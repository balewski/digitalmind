#!/bin/bash
# Disable multiple threads
export OMPI_MCA_osc_pami_allow_thread_multiple=0

#disable adaptive routing
export PAMI_IBV_ENABLE_OOO_AR=0
export PAMI_IBV_QP_SERVICE_LEVEL=0

# Reduce horovod sleep time, enable priority NCCL stream
export HOROVOD_SLEEP_INTERVAL=2
export HOROVOD_USE_PRIORITY=0

# Activate my binary version of TF
# following https://code.ornl.gov/summit/mldl-stack/tensorflow
cd /ccs/home/balewski/summit/tensorflow-1.8-p3/
source source_to_run_tensorflow1.8-p3
cd -
pwd
grank=$PMIX_RANK
lrank=$(($PMIX_RANK%6))


#APP="python ./tiramisu-tf.py --datadir_train ${1}/train/data --datadir_validation ${1}/validation/data --chkpt_dir checkpoint.fp16.lag${5} --epochs ${2} --fs local --blocks 2 2 2 4 5 --growth 32 --filter-sz 5 --loss weighted --cluster_loss_weight 0.0 --optimizer opt_type=LARC-Adam,learning_rate=${3},gradient_lag=${5} --batch 2 --dtype float16 --scale_factor ${4}"

#APP=" nvidia-smi  "
#APP=" python -u toy1-FC.py  "
APP=" python -u ../digitalMind/keras/examples-solo/toy-FC.py"

export PAMI_ENABLE_STRIPING=0

case ${lrank} in
[0])
nvidia-smi -l 2 >&L.smi-${lrank}&
export PAMI_IBV_DEVICE_NAME=mlx5_0:1
numactl --physcpubind=0-27 --membind=0 $APP

  ;;
[1])
export PAMI_IBV_DEVICE_NAME=mlx5_1:1
numactl --physcpubind=28-55 --membind=0 $APP
  ;;
[2])
export PAMI_IBV_DEVICE_NAME=mlx5_0:1
numactl --physcpubind=56-83 --membind=0 $APP
  ;;
[3])
export PAMI_IBV_DEVICE_NAME=mlx5_3:1
numactl --physcpubind=88-115 --membind=8 $APP
  ;;
[4])
export PAMI_IBV_DEVICE_NAME=mlx5_2:1
numactl --physcpubind=116-143 --membind=8 $APP
  ;;
[5])
export PAMI_IBV_DEVICE_NAME=mlx5_3:
numactl --physcpubind=144-171 --membind=8 $APP
  ;;
esac
