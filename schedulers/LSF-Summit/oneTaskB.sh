#!/bin/bash

# Activate my binary version of TF
# following https://code.ornl.gov/summit/mldl-stack/tensorflow
cd /ccs/home/balewski/summit/tensorflow-1.8-p3/
source source_to_run_tensorflow1.8-p3
cd -
python -V
pwd

# Disable multiple threads
export OMPI_MCA_osc_pami_allow_thread_multiple=0

#disable adaptive routing
export PAMI_IBV_ENABLE_OOO_AR=0
export PAMI_IBV_QP_SERVICE_LEVEL=0

# Reduce horovod sleep time, enable priority NCCL stream
export HOROVOD_SLEEP_INTERVAL=2
export HOROVOD_USE_PRIORITY=0
grank=$PMIX_RANK
lrank=$(($PMIX_RANK%6))

outPath="/gpfs/alpine/world-shared/ast153/balewski/tmp1/"
#outPath="./out/"

#APP=" nvidia-smi  "
APP=" python -u ../../keras/examples-solo/toy-FC.py"
#APP=" python -u ../../keras/examples-solo/toy-FC-horovod.py  --outPath $outPath --batch_size=196608"

# attic:  24*1k=24576, 48*4k=196608
echo "Full APP=$APP="

export PAMI_ENABLE_STRIPING=0

case ${lrank} in
[0])
nvidia-smi -l 2 >&$outPath/L.smi-${lrank}&
export PAMI_IBV_DEVICE_NAME=mlx5_0:1
numactl --physcpubind=0-27 --membind=0 $APP
  ;;
[1])
export PAMI_IBV_DEVICE_NAME=mlx5_1:1
numactl --physcpubind=28-55 --membind=0 $APP
  ;;
[2])
export PAMI_IBV_DEVICE_NAME=mlx5_0:1
numactl --physcpubind=56-83 --membind=0 $APP
  ;;
[3])
export PAMI_IBV_DEVICE_NAME=mlx5_3:1
numactl --physcpubind=88-115 --membind=8 $APP
  ;;
[4])
export PAMI_IBV_DEVICE_NAME=mlx5_2:1
numactl --physcpubind=116-143 --membind=8 $APP
  ;;
[5])
export PAMI_IBV_DEVICE_NAME=mlx5_3:1
numactl --physcpubind=144-171 --membind=8 $APP
  ;;
esac
