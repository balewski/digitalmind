#!/usr/bin/env python
"""   classify Iris petals using random forest
Workhorse:
http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html


RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
            max_depth=None, max_features='auto', max_leaf_nodes=None,
            min_impurity_split=1e-07, min_samples_leaf=1,
            min_samples_split=2, min_weight_fraction_leaf=0.0,
            n_estimators=10, n_jobs=2, oob_score=False, random_state=0,
            verbose=0, warm_start=False)

Example from
https://chrisalbon.com/machine-learning/random_forest_classifier_example_scikit.html

"""
from sklearn.datasets import load_iris
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np

print('Use random forest to classify iris petals')
iris = load_iris()
df = pd.DataFrame(iris.data, columns=iris.feature_names)

df['species'] = pd.Categorical.from_codes(iris.target, iris.target_names)
print('input data:\n',df.head(10))

# split data on training/test segments
df['is_train'] = np.random.uniform(0, 1, len(df)) <= .70

# shuffle data in place to have more interesting printout
df = df.sample(frac=1).reset_index(drop=True)

train, test = df[df['is_train']==True], df[df['is_train']==False]

features = df.columns[:4]
print('\nfeatures num=%d:\n'%(features.shape[0]),features)

# there are three species, which have been coded as 0, 1, or 2.
y, _ = pd.factorize(train['species'])

# Create a random forest Classifier. By convention, clf means 'Classifier'
clf = RandomForestClassifier(n_jobs=2, verbose=1)
clf.fit(train[features], y)
print ("\nTrained model :: ",clf)


# Apply Classifier To Test Data
preds = iris.target_names[clf.predict(test[features])]
print('\nClassifier predictions:\n',preds[:5],'\n truth:\n',test['species'].head(5))

# View the predicted probabilities of the first 10 observations
out=clf.predict_proba(test[features])[0:10]
print('\nPred proba:\n',out)

out=pd.crosstab(test['species'], preds, rownames=['actual'], colnames=['preds'])
print('\nClassifier confusion matrix\n',out)

# View Feature Importance
# a list of the features and their importance scores
out=list(zip(train[features], clf.feature_importances_))
print('\n Feature Importance\n',out)
