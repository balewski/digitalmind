#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"
 
''' 
Visualising high-dimensional datasets using PCA and t-SNE in Python

Basaed on 
https://medium.com/@luckylwk/visualising-high-dimensional-datasets-using-pca-and-t-sne-in-python-8ef87e7915b

'''

import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings


#............................
#............................


#............................
#............................


#............................
#............................


#=================================
#=================================
#  M A I N 
#=================================
#=================================

import numpy as np
from sklearn.datasets import fetch_mldata

mnist = fetch_mldata("MNIST original")
X = mnist.data / 255.0
y = mnist.target

print ('mnist inp',X.shape, y.shape)

# - - - -
#convert  matrix and vector to a Pandas DataFrame. 

import pandas as pd

feat_cols = [ 'pixel'+str(i) for i in range(X.shape[1]) ]

df = pd.DataFrame(X,columns=feat_cols)
df['label'] = y
df['label'] = df['label'].apply(lambda i: str(i))
rndperm = np.random.permutation(df.shape[0])  # oryginal input is ordered by lalbel
X, y = None, None

print ('PD: Size of the dataframe: ',df.shape)

# - - -  Display some numbers
import matplotlib.pyplot as plt

# Plot the graph
plt.gray()
fig = plt.figure( figsize=(16,7) )
for i in range(0,30):
    ax = fig.add_subplot(3,10,i+1, title='Digit: ' + str(df.loc[rndperm[i],'label']) )
    ax.matshow(df.loc[rndperm[i],feat_cols].values.reshape((28,28)).astype(float))

# - - - -   PCA : 
'''three-dimensional plots lets start with that and generate, from the original 784 dimensions, the first three principal components. And we’ll also see how much of the variation in the total dataset they actually account for.
'''
from sklearn.decomposition import PCA

pca = PCA(n_components=3)
pca_result = pca.fit_transform(df[feat_cols].values)

df['pca-one'] = pca_result[:,0]
df['pca-two'] = pca_result[:,1] 
df['pca-three'] = pca_result[:,2]

print ('PCA: Explained variation per principal component: ',pca.explained_variance_ratio_)

df2d= df.loc[rndperm[:3000],:]
df_x=df2d['pca-one']
df_y=df2d['pca-two']
df_lbl=df2d['label']

print(df_x.head(10))

#  display 1st + 2nd PCA component by digit - no library
plt.figure(facecolor='white', figsize=(6,5.5))
ax=plt.subplot(1,1,1)
img=ax.scatter(df_x,df_y, alpha=.4, s=3**2, c=df_lbl,cmap='tab10')

plt.colorbar(img).set_label('10 MNIST digits')
ax.set(title='1st+2nd PCA colored by digit',xlabel='pca-one',ylabel='pca-two')


#plt.tight_layout() ;plt.show()

# - - -   TSNE

''' 
 Since t-SNE scales quadratically in the number of objects N, its applicability is limited to data sets with only a few thousand input objects;

It is highly recommended to use another dimensionality reduction
  method (e.g. PCA for dense data or TruncatedSVD for sparse data)
  to reduce the number of dimensions to a reasonable amount (e.g. 50)
'''

import time

from sklearn.manifold import TSNE

n_sne = 1500
print('TSNE for 7k smaples takes 100 seconds on Cori login node, T~N*N')

time_start = time.time()
tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
tsne_results = tsne.fit_transform(df.loc[rndperm[:n_sne],feat_cols].values)

print ('t-SNE done1 ! Time elapsed: %.1f seconds'%(time.time()-time_start))

# Now that we have the two resulting dimensions we can again visualise them 

df_tsne = df.loc[rndperm[:n_sne],:].copy()
df_x2 = tsne_results[:,0]
df_y2 = tsne_results[:,1]
df_lbl2=df_tsne['label']

plt.figure(facecolor='white', figsize=(6,5.5))
ax=plt.subplot(1,1,1)
img=ax.scatter(df_x2,df_y2, alpha=.4, s=3**2, c=df_lbl2,cmap='tab10')

plt.colorbar(img).set_label('10 MNIST digits')
ax.set(title='784D--> 2D TSNE colored by digit',xlabel='tsne-one',ylabel='tsne-two')


# - - - -  Lets combine PCA + TSNE
print('\n\n reduce 784D to 50D w/ PCA, then do TSNE')
pca_50 = PCA(n_components=50)
pca_result_50 = pca_50.fit_transform(df[feat_cols].values)

print ('Explained variation per principal component (PCA):',np.sum(pca_50.explained_variance_ratio_))
n_sne = 5000

time_start = time.time()

tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
tsne_pca_results = tsne.fit_transform(pca_result_50[rndperm[:n_sne]])
print ('t-SNE done2 ! Time elapsed: %.1f seconds'%(time.time()-time_start))


df_tsne = None
df_tsne = df.loc[rndperm[:n_sne],:].copy()
df_x3 = tsne_pca_results[:,0]
df_y3 = tsne_pca_results[:,1]
df_lbl3=df_tsne['label']

plt.figure(facecolor='white', figsize=(6,5.5))
ax=plt.subplot(1,1,1)
img=ax.scatter(df_x3,df_y3, alpha=.4, s=3**2, c=df_lbl3,cmap='tab10')

plt.colorbar(img).set_label('10 MNIST digits')
ax.set(title='50D--> 2D TSNE colored by digit',xlabel='tsne-one',ylabel='tsne-two')

plt.tight_layout() ;plt.show()
