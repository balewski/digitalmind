#!/usr/bin/env python

# Adopted by Jan Balewski 
"""
This showcases how simple it is to build image classification networks.

It follows description from this TensorFlow tutorial:
    https://www.tensorflow.org/versions/master/tutorials/mnist/pros/index.html#deep-mnist-for-experts
"""

import os
import sys
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

import numpy as np
import tensorflow as tf
import time

TFVer=tf.__version__
print(' TF.ver=%s '%(TFVer))
assert TFVer>='1.8.0'

# Configure model options
TF_DATA_DIR = 'data1'
TF_MODEL_DIR = 'out1'
TF_TRAIN_STEPS = 20
TF_BATCH_SIZE =  100
TF_LEARNING_RATE =  0.01

N_DIGITS = 10  # Number of digits.
#----------------------------------
def conv_model(features, labels, mode):
  """2-layer convolution model."""
  # Reshape feature to 4d tensor with 2nd and 3rd dimensions being
  # image width and height final dimension being the number of color channels.
  input_x = tf.reshape(features['x'], [-1, 28, 28, 1])

  # First conv layer will compute 32 features for each 5x5 patch
  with tf.variable_scope('conv_layer1'):
    h_conv1 = tf.layers.conv2d(input_x, filters=32, kernel_size=[5, 5], 
                               padding='same', activation=tf.nn.relu)
    h_pool1 = tf.layers.max_pooling2d(
        h_conv1, pool_size=2, strides=2, padding='same')

  # Second conv layer will compute 64 features for each 5x5 patch.
  with tf.variable_scope('conv_layer2'):
    h_conv2 = tf.layers.conv2d( h_pool1, filters=64, kernel_size=[5, 5],
        padding='same',  activation=tf.nn.relu)
    h_pool2 = tf.layers.max_pooling2d(
        h_conv2, pool_size=2, strides=2, padding='same')

    # reshape tensor into a batch of vectors
    h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])

  # Densely connected layer with 1024 neurons.
  h_fc1 = tf.layers.dense(h_pool2_flat, 1024, activation=tf.nn.relu)
  h_fc1 = tf.layers.dropout( h_fc1, rate=0.5,
      training=(mode == tf.estimator.ModeKeys.TRAIN))

  # Compute logits (1 per class) and compute loss.
  logits = tf.layers.dense(h_fc1, N_DIGITS, activation=None)
  predict = tf.nn.softmax(logits)
  classes = tf.cast(tf.argmax(predict, 1), tf.uint8)


  # . . . . .  alternative returns defined below .....  
  # Compute predictions.
  predicted_classes = tf.argmax(logits, 1)
  if mode == tf.estimator.ModeKeys.PREDICT:
    predictions = {
        'class': predicted_classes,
        'prob': tf.nn.softmax(logits)
    }
    return tf.estimator.EstimatorSpec(mode, predictions=predictions,
        export_outputs={'classes':
                        tf.estimator.export.PredictOutput({"predictions": predict,
                                                           "classes": classes})})
  # Compute loss.
  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

  # Create training op.
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.GradientDescentOptimizer(
        learning_rate=TF_LEARNING_RATE)
    train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)

  # Compute evaluation metrics.
  eval_metric_ops = {
      'accuracy': tf.metrics.accuracy(
          labels=labels, predictions=predicted_classes)
  }
  return tf.estimator.EstimatorSpec(
      mode, loss=loss, eval_metric_ops=eval_metric_ops)

#----------------------------------
def main(_):
  tf.logging.set_verbosity(tf.logging.INFO)

  # prepare data - I do not want to user tensorLayers for modeling
  import tensorlayer as tl
  X_train, y_train, X_val, y_val, X_test, y_test = tl.files.load_mnist_dataset(shape=(-1,784),path=TF_DATA_DIR)

  train_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={'x': X_train}, y=y_train, batch_size=TF_BATCH_SIZE,
      num_epochs=None,  shuffle=True)
  test_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={'x': X_val}, y=y_val, num_epochs=1, shuffle=False)
  
  training_config = tf.estimator.RunConfig(
      model_dir=TF_MODEL_DIR, save_summary_steps=100, save_checkpoints_steps=1000)

  # Convolutional network
  classifier = tf.estimator.Estimator(
      model_fn=conv_model, model_dir=TF_MODEL_DIR, config=training_config)
  
  tf.logging.set_verbosity(tf.logging.ERROR)
  train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn, max_steps=TF_TRAIN_STEPS)
  eval_spec = tf.estimator.EvalSpec(input_fn=test_input_fn, steps=1)

  startT = time.time()  
  tf.estimator.train_and_evaluate(classifier, train_spec, eval_spec)
  fitTime=time.time() - startT
  print('M: train_and_evaluate done, fit %.2f min'%(fitTime/60.)) 
  
  # lossA=np.array(...)
  ### PRINT loss for all step here

  tf.logging.set_verbosity(tf.logging.INFO)

  print('Make predictions')
  k=0
  nBad=0
  for _prediction in classifier.predict(test_input_fn):
      k+=1
      if k==1: print('dump',_prediction)
  
      probA=_prediction['prob']
      digU=_prediction['class']
      
      digZ=np.argmax(probA)
      if k<5: print('probA',type(probA),probA)

      if digU!=digZ: nBad+=1
      if k<20: print('k=',k,digU,digZ,digU==digZ)
      #if k>200: break
  print('n All=',k,' nBad=',nBad)

if __name__ == '__main__':
  tf.app.run()
