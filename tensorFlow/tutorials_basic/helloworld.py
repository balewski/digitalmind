#!/usr/bin/env python
'''
HelloWorld example using TensorFlow library.

Author: Aymeric Damien
Project: https://github.com/aymericdamien/TensorFlow-Examples/
'''

from __future__ import print_function

import tensorflow as tf

print('TF ver=',tf.__version__)

# Simple hello world using TensorFlow

# Create a Constant op
# The op is added as a node to the default graph.
#
# The value returned by the constructor represents the output
# of the Constant op.
hello = tf.constant('Hello, TensorFlow!')

# Start tf session
sess = tf.Session()

# Run the op
print(sess.run(hello))


#filenames='./data_tfrStr_mnist2/mnistBlk.[01].tfrecords'
#print(sess.run()
#print(sess.run(tf.data.Dataset.list_files(filenames)))
