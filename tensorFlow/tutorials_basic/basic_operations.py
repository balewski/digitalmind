#!/usr/bin/env python
'''
Basic Operations example using TensorFlow library.

Author: Aymeric Damien
Project: https://github.com/aymericdamien/TensorFlow-Examples/
'''

from __future__ import print_function

import tensorflow as tf
print('TF ver=',tf.__version__)

# Basic constant operations
# The value returned by the constructor represents the output of the Constant op.
a = tf.constant(2)
b = tf.constant(3)
c = tf.constant([7, 5, 8, 1])

# Launch the default graph.
with tf.Session() as sess:
    print("expect a=2, b=3")
    print('a: type',a,', shape=',a.shape)
    print('c: type',c,', shape=',c.shape)
    print('a=',sess.run(a),'b=',sess.run(b),'a=',sess.run(c),)

    print("a+b= with constants: %i" % sess.run(a+b))
    print("a*b= with constants: %i" % sess.run(a*b))
    print("a*c= with constants: " , sess.run(a*c))

# Basic Operations with variable as graph input
# The value returned by the constructor represents the output of the Variable op. 
# (define as input when running session)  tf Graph input
aV = tf.placeholder(tf.int16)
bV = tf.placeholder(tf.int16)

print('\n Define some operations using TF variables')
sumV = tf.add(aV, bV)
mulV = tf.multiply(aV, bV)

# Launch the default graph.
with tf.Session() as sess:
    print('aV: type',aV,', shape=',aV.shape)
    print('aV=',sess.run(aV, feed_dict={aV: [66, 55]}))
    inpData={aV: [2, 5], bV: 3}
    print(' Run every operation with variable input')
    print('input:',inpData)

    print("Addition with vars: " , sess.run(sumV, feed_dict=inpData))
    print("Multiplication with vars: ",  sess.run(mulV, feed_dict=inpData))


# ----------------
# More in details:
# Matrix Multiplication from TensorFlow official tutorial

print('\n Create a Constant op that produces a 1x2 matrix')
# The op is added as a node to the default graph.
#
# The value returned by the constructor represents the output
# of the Constant op.
matrix1 = tf.constant([[3., 5.]])

# Create another Constant that produces a 2x1 matrix.
matrix2 = tf.constant([[7.],[11.]])


# Create a Matmul op that takes 'matrix1' and 'matrix2' as inputs.
# The returned value, 'product', represents the result of the matrix
# multiplication.
product = tf.matmul(matrix1, matrix2)

# To run the matmul op we call the session 'run()' method, passing 'product'
# which represents the output of the matmul op.  This indicates to the call
# that we want to get the output of the matmul op back.
#
# All inputs needed by the op are run automatically by the session.  They
# typically are run in parallel.
#
# The call 'run(product)' thus causes the execution of threes ops in the
# graph: the two constants and matmul.
#
# The output of the op is returned in 'result' as a numpy `ndarray` object.
with tf.Session() as sess:
    print('matrix2: type',matrix2,', shape=',matrix2.shape)
    result = sess.run(product)
    print(result)
    # ==> [[ 12.]]
