#!/usr/bin/env python
""" 
format input for  CosmoFlow3.1, works  interactive haswell node
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_Cosmo import locateTFRecDataJan, split_on_domains,write_yaml,read_yaml
from pprint import pprint
import numpy as np

import argparse, os 
def get_parser():
    parser = argparse.ArgumentParser(
        description='prepare input for  CosmoFlow',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")


    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    args = parser.parse_args()
    args.srunId=0 # pretend it is 1-node cluster
    args.prjName='cosmoFlow3'
    if args.srunId==0:
        for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

#inpSrc='cs1'  # Debbie's data
#sourceDir,allL=locateTFRecDataDebbie(inpSrc,'train')
inpSrc='cs3' # Jan data Oct'18
inpSrc='cs4' # Jan data Oct30'18
dimMask='dim128_cube'
sourceDir,allL,mainDir=locateTFRecDataJan(inpSrc,dimMask)
print('M: got len=',len(allL),' sourceDir=',sourceDir)
print('one:',allL[0])
metaF=sourceDir+'/cosmo3_dataSplit.yaml'
blob=read_yaml(metaF)
pprint(blob)

outD=split_on_domains(allL)
outD['sourceDir']={inpSrc: sourceDir,'bb4':"<DW_PERSISTENT_STRIPED_cosmo_2018_10_dim128_cube>"}
outD['parNames']=blob['namePar']
outD['shape']=blob['shape']
outD['frameCnt']=blob['frameCnt']
outD['coreStr']=blob['coreStr']
outF=args.outPath+'/cosmo3_dataSplit.yaml'
write_yaml(outD,outF)
