__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"
import os
import numpy as np

#...!...!..................
def getUZ_correlation(UmZ,lossAvr):
        print('\n - - - - getUZ_corr dims:',UmZ.shape)

        nv=UmZ.shape[1]
        nvL=range(nv)

       
        # compute cov matrix
        c=np.cov(UmZ,rowvar=False)
        #print('cov M:')
        #pprint(c)
        c1=np.diagonal(c)
        #print('c1.mean',np.mean(c1))
        c2=np.sqrt(c1)
        print('one std dev:',c2)
        avrSig=c2.mean()
        print('global  residua avr=%.3f  var=%.3f '%(avrSig, avrSig*avrSig))
  
        parNameL=['Omage_m','sigma_8','N_s']

        print('\nnames ',end='')
        [ print('     %s'%parNameL[i],end='') for i in nvL]

        Uampl=1.8
        print('\nresidua (Uampl=%.1f): '%Uampl,end='')
        [ print('U%d> %4.1f%s  '%(i,100*c2[i]/Uampl,chr(37)),end='') for i in nvL ]

        
        #c3=np.power(c2,2);    xxlossMSE=c3.mean()
        # compute MAE loss correctly from residua
        #print('true TF loss:',lossV)
        #print('inp U-Z:',UmZ)
        s1=np.abs(UmZ)
        #print('s1:',s1)
        s2=np.mean(s1,axis=1)
        #print('comp MSE loss per frame (s2):',s2)
        compMAE=np.mean(s1)
        tfLoss=lossAvr
        #print('comp MAE=%.3f  tfLoss=%.3f'%(compMAE,tfLoss))

        t1=np.power(UmZ,2); compMSE=t1.mean()
        print('\n computed: lossMSE=%.3f , lossMAE=%.3f,  true lossTF=%.3f'%( compMSE,compMAE,tfLoss))
 
        #print('\ncorrelation matrix for %d dim vector'%nv)
        r=np.corrcoef(UmZ,rowvar=False)
        #pprint(r)

        print('\nnames:  ',end='')
        [ print('   %7s '%parNameL[i],end='') for i in nvL]
        print('\ncorrM  ',end='')
        [ print('  __var%1d__ '%i,end='') for i in nvL]
        for k in nvL:
            print('\n var%1d '%k,end='')
            [ print('     %6.3f'% r[i][k],end='') for i in range(k+1)]
            #print('      #  '*(nv-k),end='')
        print()

              
