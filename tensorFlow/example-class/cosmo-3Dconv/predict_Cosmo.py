#!/usr/bin/env python
""" 
Make predictions for CosmoFlow 3 on interactive haswell node
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Deep_Cosmo import Deep_Cosmo
from Plotter_Cosmo import Plotter_Cosmo
from Util_Cosmo_py3 import getUZ_correlation

import argparse, os 
def get_parser():
    parser = argparse.ArgumentParser(
        description='train  multi-valued regression',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign',choices=['tune1','paper1'],
                         default='paper1',
                         help=" hyper-param set, aka design of the model")

    parser.add_argument("--dataSource",choices=['bb4','cs4'],
                        default='cs4',help="path to data vault: cscratch or BB")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("--modelPath",
                        default='model',help="TF model, checkpoints")


    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--frames", type=int, default=3,
                        help="how many frames should be procerssed")

    parser.add_argument("--nCpu", type=int, default=60,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=16,
                        help="fit batch_size")


    parser.add_argument("-e", "--best_epoch", type=int, default=15,
                        help="use model from this epoch")


    args = parser.parse_args()
    args.prjName='cosmoFlow3'
    #P args.srunId=int(os.environ['SLURM_PROCID'])
    args.srunId=0
    #P args.slurmPart=os.environ['SLURM_JOB_PARTITION']
    args.submitHost=os.environ['HOST']
    #args.batch_size=1 # keep output simple & slow?
    args.steps=args.frames # to match the logic
    args.epochs=0
    if args.srunId==0:
        for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
gra=Plotter_Cosmo(args)

dom='test'
#dom='val'
# Jan's new framework starts here:
deep= Deep_Cosmo()
deep.set_params(args)
deep.create_weigths()

deep.open_data_readers(args)

UV,ZV,lossAvr=deep.predict(args,dom)

getUZ_correlation(UV-ZV,lossAvr)
print('M: UVsh',UV.shape)

gra.plot_fparam(UV,tit='true Us',figId=14)
gra.plot_fparam(ZV,tit='predictions',figId=15)
#gra.plot_data_frames(segR,9,figId=7)
gra.plot_fparam_residua(UV,ZV)
gra.show(args,'pred')


if args.srunId==0:  print('Jan-end, srunId=',args.srunId)

    
    
            
