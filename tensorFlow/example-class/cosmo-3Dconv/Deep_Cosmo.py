__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#  previous authors: 
#  Debbie Bard, LBL & Pete Mendygral, Cray Inc.

import sys
import os

tfJanOpt='Pete_Apr10_py27'

if (sys.version_info > (3, 0)):
    from functools import reduce
    tfJanOpt='Nersc_TF11.x_py36'

print('Deep0: will use tfJanOpt=',tfJanOpt)

if tfJanOpt=='Pete_Apr10_py27':
    os.unsetenv('OMP_NUM_THREADS')
    os.environ['KMP_AFFINITY']  = "compact,norespect"
    os.environ['KMP_HW_SUBSET'] = "64C@2,1T"  # KNL optimized

if tfJanOpt=='Nersc_TF11.x_py36':
    os.environ['KMP_BLOCKTIME'] = "1" # KNL optimized 
    os.environ['OMP_NUM_THREADS'] = "64" 
    os.environ['KMP_HW_SUBSET'] = "64C@2,1T"  # KNL optimized    
    os.environ['TF_BIND'] = "1" 
    os.environ['TF_BIND_VERBOSE'] = "1" 

import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

#if (sys.version_info < (3, 0)):
    #import the Cray PE ML Plugin
#    import ml_comm as mc

import tensorflow as tf
import numpy as np
import time

from tensorflow.python.ops import math_ops 
from Util_Cosmo import YParams, read_yaml, readDataSet

def weight_variable(shape,name):
    W = tf.get_variable(name,shape=shape, initializer=tf.contrib.layers.xavier_initializer())
    return W

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def lrelu(x, alpha):
    return tf.nn.relu(x) - alpha * tf.nn.relu(-x)

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 

class Deep_Cosmo:
    def __init__(self):
        self.learning_rate= None ######## 
        self.tensors={}

#...!...!..................
    def set_params(self,args):
        self.batch_size=args.batch_size
        self.epochs=args.epochs
        assert args.steps >1 # later: if 0  compute as : total input size/mc.get_nranks()
        self.train_steps=args.steps  
        self.val_steps=max(2, int(args.steps/8)) 
        self.srunId=args.srunId
        if args.srunId==0: # only node0 will be printing debug info
            self.verb=args.verb
        else:
            self.verb=0 

        if self.verb:
            print('Jan submitHost:',args.submitHost,'srunId=',self.srunId)
            for xx in [ args.outPath, args.modelPath]:
                if os.path.exists(xx): continue
                print('Aborting on start, missing  dir:',xx)
                exit(1)

        # . . . load hyperparameters
        yaml_cnfg='hpar_'+args.prjName+'.yaml'
        self.hparams=YParams(os.path.abspath(yaml_cnfg),args.modelDesign , self.verb)
        # convert types for some of variables - perhaps this can be done smarter way
        self.hparams.zsAVG=np.array(self.hparams.zsAVG)
        self.hparams.zsSTD=np.array(self.hparams.zsSTD)
        #cary oversome values for convenience
        self.hparams.batch_size=args.batch_size
        #Min_after_dequeue and capacity(computed) together determines the shuffling of input data
        self.hparams.CAPACITY=self.hparams.batch_size*4 + self.hparams.MIN_AFTER_DEQUEUE

#...!...!..................
    def open_data_readers(self,args):
        # .... find all input data names as lists
        inpF='cosmo3.2_dataSplit_3parA.yaml'
        blobD=read_yaml(inpF,self.verb)
        BBname=blobD['sourceDir'][args.dataSource]
        if 'bb' in  args.dataSource:
            inpDir = os.environ[BBname]
        else:
            inpDir=BBname
        inpDir+=blobD['subDir']

        if self.verb : print('open_data_readers(%s)'%inpDir)
        data={}
        # ... instantiate input streams
        for dom in ['train', 'val','test']:
            # prepend absolute path
            inpL=blobD[dom]
            fullL=[inpDir+'/'+x for x in inpL]
            if self.verb : 
                inpF1=fullL[0]
                print('reader domain=%s numTFrec=%d, example:'%(dom,len(inpL)),inpF1)
                fd=open(inpF1,'r') # just to test readibility
                fd.close()
            data[dom]=readDataSet(self.hparams,filenames =fullL, shuffle=(dom!='test') )
        self.data=data
        

#...!...!..................
    def create_weigths(self):
        #initialize weight and bias
        self.W = {}
        self.b = {}
        self.bn_param = {}
        
        j=0
        dimIn=1
 

        # agregate all cnn params
        filtL=[]; kernL=[]
        for filt,kern in zip(self.hparams.convA_filters, self.hparams.convA_kernels ):
            filtL.append(filt)
            kernL.append(kern)

        for filt,kern in zip(self.hparams.convB_filters, self.hparams.convB_kernels ):
            filtL.append(filt)
            kernL.append(kern)

        for filt,kern in zip(filtL, kernL):
            j+=1
            cname='conv%d'%j
            wname='w%d'%j
            #print('create j=',j,cname,filt,kern,wname)
            self.W['W_'+cname] = weight_variable([kern, kern, kern, dimIn,filt ],wname)
            self.b['b_'+cname] = bias_variable([filt])
            dimIn=filt
 
        jj=0
        dimIn=self.hparams.flatten_dim
        for dimOut  in self.hparams.dense_dims:
            j+=1; jj+=1
            cname='fc%d'%jj
            wname='w%d'%j
            #print('create j=',j,cname,filt,kern,wname)
            self.W['W_'+cname] = weight_variable([ dimIn,dimOut ],wname)
            self.b['b_'+cname] = bias_variable([dimOut])
            dimIn=dimOut
 
#...!...!..................
    def assembly_model(self,inputBatch,IS_TRAINING,keep_prob,scope,reuse):
        # First convolutional layer
        tenx=inputBatch
        j=0 # global CNN-layer counter
        with tf.name_scope('convA_block'):  
            for filt,stri in zip(self.hparams.convA_filters , self.hparams.poolA_strides):
                j+=1
                cname='conv%d'%j
                #print('assemby j=',j,cname,filt,stri)
                tenx=tf.nn.conv3d(tenx,self.W['W_'+cname],strides = [1,1,1,1,1],padding = 'VALID',name=cname)
                tenx = lrelu( tenx+ self.b['b_'+cname],self.hparams.LEAK_PARAMETER)
                if self.verb>0: print(cname,tenx.shape, 'pool_stride:',stri)
                if stri>0:
                    #Jan tried and abandoned: changed AVG-pool to MAX-pool
                    tenx = tf.nn.avg_pool3d(tenx, ksize=[1,stri,stri,stri,1], strides =[1,stri,stri,stri,1] , padding = 'VALID')
                    if self.verb>0: print('pool%d'%j,tenx.shape)

        with tf.name_scope('convB_block'):  
            for filt,stri in zip(self.hparams.convB_filters, self.hparams.poolB_strides):
                j+=1
                cname='conv%d'%j
                #print('assemby j=',j,cname,filt,stri)
                tenx=tf.nn.conv3d(tenx,self.W['W_'+cname],strides = [1,stri,stri,stri,1],padding = 'VALID',name=cname)
                tenx = lrelu( tenx+ self.b['b_'+cname],self.hparams.LEAK_PARAMETER)
                if self.verb>0: print(cname,tenx.shape, 'stride:',stri)
    
 
        with tf.name_scope('fc_block'):  
            tenx=tf.reshape(tenx,[-1,self.hparams.flatten_dim]) # flatten input before passing to FC
            if self.verb>0: print('flatten',tenx.shape)
            j=0
            for dims  in self.hparams.dense_dims:
                j+=1
                cname='fc%d'%j
                #print('assemby j=',j,cname,dims)
                tenx=tf.matmul(tf.nn.dropout(tenx,keep_prob), self.W['W_'+cname])+ self.b['b_'+cname]
                if j< len(self.hparams.dense_dims):  # use linear activation for last FC layer
                    tenx = lrelu( tenx,self.hparams.LEAK_PARAMETER)
                if self.verb>0: print(cname,tenx.shape)
                

        return tenx
 
#...!...!..................
    def lossU_op(self):
        with tf.name_scope('loss'):
            X,Y=self.data['train']
            Z = self.assembly_model(inputBatch =X ,IS_TRAINING = True,keep_prob = (1.-self.hparams.dropFracFC),scope='conv_bn',reuse = None)
            #lossL1 = tf.reduce_mean(tf.abs(Y-predictions))
            lossL2=tf.losses.mean_squared_error(Y,Z)
            return lossL2

#...!...!..................
    def predictor_op(self,domain):
        if self.verb: print('\n= = = = predictor_op dom=',domain)
        X,Y=self.data[domain]
        Z = self.assembly_model(inputBatch = X,IS_TRAINING = False,keep_prob = 1,scope='conv_bn',reuse=True)
        #ok 10 lossAvr = tf.reduce_mean(tf.abs(Y-Z))
        lossAvr=tf.losses.mean_squared_error(Y,Z)
        return Y,Z,lossAvr


#...!...!..................
    def get_lars_optimizer(self,opt_type,loss,global_step,learning_rate,momentum=0.9,LARS_eta=0.002,LARS_epsilon=1./16000.):
        #set up optimizers
        if opt_type == "Adam":
            optim = tf.train.AdamOptimizer(learning_rate=learning_rate)
        elif opt_type == "RMSProp":
            optim = tf.train.RMSPropOptimizer(learning_rate=learning_rate)
        elif opt_type == "SGD":
            optim = tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=momentum)
        else:
            raise ValueError("Error, optimizer {} unsupported.".format(opt_type))

        #compute gradients
        grads_and_vars = optim.compute_gradients(loss)
        grads          = [gv[0] for gv in grads_and_vars]
        gs_and_vs      = [(g,v) for (_,v), g in zip(grads_and_vars, grads)]
        for idx, (g, v) in enumerate(gs_and_vs):
            if g is not None:
                v_norm = tf.norm(v, ord='euclidean')
                g_norm = tf.norm(g, ord='euclidean')

                lars_local_lr = tf.cond(
                    pred = math_ops.logical_and( math_ops.not_equal(v_norm, tf.constant(0.0)),
                                            math_ops.not_equal(g_norm, tf.constant(0.0)) ),
                                            true_fn = lambda: LARS_eta * v_norm / g_norm,
                                            false_fn = lambda: LARS_epsilon)

                # hardcoded LARS_mode="clip", Thorsten said "scale" is bad
                effective_lr = math_ops.minimum(lars_local_lr, 1.0)

                #multiply gradients
                gs_and_vs[idx] = (math_ops.scalar_mul(effective_lr, g), v)

        #apply gradients:
        grad_updates = optim.apply_gradients(gs_and_vs,global_step=global_step)

        # Ensure the train_tensor computes grad_updates.
        with tf.control_dependencies([loss]):
            return grad_updates

#...!...!..................
    def optimize_op(self, global_step, beta1, beta2):
        if self.verb: print('= = = = optimize_op = = = =')
        lossU_op1 = self.lossU_op()  #  put here the code for _op()
        if  self.hparams.opt_type =='ADAM':
            missing_JAN1
        elif  self.hparams.opt_type in  ['ADAM_LARC', 'ADAM_LARS','LARS']:
            #num_batches_per_epoch = (float(hp.RUNPARAM['num_train']) *hp.magic_number/self.batch_size/num_nodes)
            num_batches_per_epoch=self.train_steps
            global_step_val = math_ops.cast(global_step, tf.float32)
            num_steps_for_warmup = int(num_batches_per_epoch * self.hparams.LR_warmup_epoch_num) # JAN: drop it & from hpar

            def learning_rate_decay_schedule(global_step):
                lr = self.hparams.LEARNING_RATE
                if (self.hparams.lr_scaling_mode in [1,2] ):
                    missing_code_33
                elif (self.hparams.lr_scaling_mode in [3,4] ):
                    base_LR = lr
                    decay_steps = int(num_batches_per_epoch * self.hparams.num_epochs_per_decay)
                    if (self.hparams.lr_scaling_mode == 3):
                        lr=tf.train.polynomial_decay( base_LR, global_step, decay_steps,
                                end_learning_rate=self.hparams.poly_min_lr,
                                power=1.0,#for LARS
                                cycle=False, name="poly3_LR_decay")
                    elif (lr_scaling_mode == 4):
                        missing_code_67
                return math_ops.cast(lr, tf.float32)
            
            if self.hparams.opt_type == 'ADAM_LARC':
                with tf.name_scope('ADAM_LARC_optimizer'):
                    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
                    with tf.control_dependencies(update_ops):
                        lr = learning_rate_decay_schedule(global_step)    
                        mystery_tf = self.get_lars_optimizer("Adam",lossU_op1,global_step, lr)
            else: 
                no_code_1_jan

        lossL1Train=train_true=train_predict=lossU_op1
        return mystery_tf, lossU_op1,global_step
        # note: mystery_tf must be returned or global_step will not be advanced during the training - I have no idea why is it

 
#...!...!..................
    def train(self,args):
        global_step = tf.train.get_or_create_global_step()#

        config = tf.ConfigProto()

        ### taking config from the MKL benchmarks. 
        '''
        There were several builds of TensorFlow that Intel provided where the behavior of inter and intra_op differed from everything else.  The tell tale sign you are using such a version is that you get errors about exhausting the number of OMP threads.  The settings below are what you had to do for those versions so that you used the correct number of threads as set by the environment variable OMP_NUM_THREADS.  Mustafa's recommendation are generally the right ones for say HSW assuming you are not using one of these problematic TensorFlow builds.
        
        Cheers,        Pete
        
        module load /global/cscratch1/sd/pjm/modulefiles/cosmoflow-gb-apr10

        '''

        config.allow_soft_placement = True

        print('Deep1: will use tfJanOpt=',tfJanOpt,' TF ver:',tf.__version__)
        if tfJanOpt=='Pete_Apr10_py27':
            config.intra_op_parallelism_threads = 1 ## for March12 wheel
            config.inter_op_parallelism_threads = 1 ## for March12 wheel

        if tfJanOpt=='Nersc_TF11.x_py36':
            config.intra_op_parallelism_threads = 64 
            config.inter_op_parallelism_threads = 1 # suggested by Lawerence 


        #used to save the model
        saver = tf.train.Saver(max_to_keep=100)
        global best_validation_accuracy

        best_validation_accuracy = 1.0         #Best validation accuracy seen so far 

        #Adam parameters
        beta1 = 0.9
        beta2 = 0.999
        
        #initialize the CPE ML Plugin with one team (single thread for now) and the model size
        totsize = sum([reduce(lambda x, y: x*y, v.get_shape().as_list()) for v in tf.trainable_variables()])

        #C mc.init(self.hparams.cpe_plugin_comm_threads, 1, totsize, "tensorflow")
 
        totsteps = int(self.epochs * self.train_steps) 
        # micromanage printouts from 'mc'
        if self.srunId <2: mc_n_hid=2
        else:  mc_n_hid=0
        mc_n_out=2*self.train_steps

        ''' #C
        if (self.hparams.cpe_plugin_pipeline_enabled == 1):
            cool_down = - int(0.45 * totsteps)
            mc.config_team(0, 0, cool_down, totsteps, mc_n_hid, mc_n_out)
            beta2 = 0.95
        else:
            mc.config_team(0, 0, totsteps, totsteps, mc_n_hid, mc_n_out)

        if self.verb>0:
            print('Effective learning rate=', self.hparams.LEARNING_RATE)
        '''

        if self.verb: print('\nInstatiate tensors used by session, match inputs')
        tensors_optimizer=self.optimize_op( global_step, beta1, beta2)
        tensors_predictor=self.predictor_op('val')

        if self.verb>0:
            print('Jan mc.init totsize=',totsize)
            print('Jan revised-dims totsteps=',totsteps)
            print("+------------------------------+")
            print("| CosmoFlow                    |")
            #print("| num Ranks = {:5d}            |".format(mc.get_nranks()))
            print("|  Batch size = {:6d}        |".format( self.batch_size))
            print("| num Parameters = {:9d}   |".format(totsize))
            print("| Optimizer = " + self.hparams.opt_type + "        |")
            print("| Eff. Learning Rate= %.1e  |"%self.hparams.LEARNING_RATE)
            '''
            if (self.hparams.cpe_plugin_pipeline_enabled == 1):
                print("| CPE Plugin Pipeline Enabled  |")
            else:
                print("| CPE Plugin Pipeline Disabled |")
            '''
            print("+------------------------------+") 

        ''' #C
        #use the CPE ML Plugin to broadcast initial model parameter values
        new_vars = mc.broadcast(tf.trainable_variables(),0)
        bcast    = tf.group(*[tf.assign(v,new_vars[k]) for k,v in enumerate(tf.trainable_variables())])
        '''

        # = = = = = = = = = = = = = 
        with tf.Session(config=config) as sess:
            losses_train = []  
            losses_val = []
            losses = []
            val_accuracys = []       
            data_accuracys = []  
            train_hist={'loss':[],'loss0':[],'val_loss':[],'val_loss0':[],'rate':[],'step':[],'numNodes':1,'trainTime':-1}


            #do all parameter initializations 
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            #C sess.run(bcast)

           #restore previous model - if exist
            save_path = os.path.join(args.modelPath)
            last_model=tf.train.latest_checkpoint(save_path)
            if last_model is not None:
                if self.verb: print("Restoring model %s"%last_model)
                saver.restore(sess, last_model)

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(coord=coord)
            #C mc.barrier() #TF startup is influencing the skew in comunication time, added as suggested by Pete
            tot_step=0
            train_start_time = time.time()
            par_i_hist=max(1,int(self.val_steps/2)) # for monitoring pick not the first & not the last
            for epoch in range(self.epochs):
                loss_per_epoch_val = 0
                loss_per_epoch_train = 0
                elaTime={'train':0.,'val':0}
                sumLoss={'train':0.,'val':0}

                for i in range(self.train_steps):
                    step_start_time = time.time()
                    tot_step+=1
                    mysteryX,lossAvr_train, gstep = sess.run(tensors_optimizer)  
                    step_finish_time = time.time()
                    step_duration=step_finish_time-step_start_time

                    elaTime['train'] += step_duration
                    nRanks=1
                    samps_per_sec = nRanks * gstep* self.batch_size / (step_finish_time-train_start_time)
                    samps_per_sec_inst = nRanks * self.batch_size / step_duration

                    
                    if i==par_i_hist: 
                        train_hist['step'].append(tot_step)
                        train_hist['rate'].append(samps_per_sec_inst)
                        train_hist['loss0'].append(lossAvr_train)
                        xF=np.array([lossAvr_train],dtype=np.float32)
                        #C mc.average(xF)
                        train_hist['loss'].append(xF)

                    if self.verb  and i%2==0:
                        print("train %d steps/node, sum:%3d;  frames/Sec trainAvr:%.2f  instant:%.2f  trainAvr_loss =%.3g "%(i+1,gstep,samps_per_sec,samps_per_sec_inst,lossAvr_train))
                        if i==-1 : print('pred shape:',train_true.shape,train_predict.shape)
                    loss_per_epoch_train +=lossAvr_train

                average_start_time=time.time()

                if (epoch % self.hparams.loss_average_interval == 0):
                    global_loss=np.array([loss_per_epoch_train],dtype=np.float32)
                    #C mc.average(global_loss)
                    loss_per_epoch_train = global_loss / self.train_steps
                    losses.append(loss_per_epoch_train)
                    losses_train.append(loss_per_epoch_train)


                # = = = = = =  Validation
                val_start_time=time.time()
                for i in range(self.val_steps):
                    Y,Z,lossAvr_val=sess.run(tensors_predictor)
                    loss_per_epoch_val += lossAvr_val
                    if i==par_i_hist: 
                        train_hist['val_loss0'].append(lossAvr_val)
                        xF=np.array([lossAvr_val],dtype=np.float32)
                        #C mc.average(xF)
                        train_hist['val_loss'].append(xF)

                elaTime['val']=time.time() - val_start_time                    
                if self.verb:
                    print("End-EpochT %d, elaTime/sec: train=%.1f, valid=%.1f, epoch_frames=%d"%(epoch,elaTime['train'],elaTime['val'],self.train_steps*self.batch_size))

                average_start_time=time.time()
                if (epoch % self.hparams.loss_average_interval == 0):
                    global_loss = np.array([loss_per_epoch_val],dtype=np.float32)
                    #C mc.average(global_loss)
                    loss_per_epoch_val = global_loss / self.val_steps
                    losses_val.append(loss_per_epoch_val)

                # cumbersome logic : when to save the model
                save_start_time=time.time()
                saveIt=False
                if (epoch % self.hparams.model_save_interval == 0) or (epoch < self.hparams.model_save_interval):
                    fname='epoch_%d'%epoch
                    saveIt=True

                if loss_per_epoch_val < best_validation_accuracy:
                    best_validation_accuracy  = loss_per_epoch_val
                    fname='best'
                    saveIt=True

                if epoch==self.epochs-1:
                    fname='last'
                    saveIt=True

                if self.srunId==0 and saveIt:
                    fname='cosmoFlow3_'+fname
                    save_path = os.path.join(args.modelPath, fname)
                    saver.save(sess=sess, save_path=save_path)
                    print('model saving epoch=%d fname=%s  valLoss=%.3f'%(epoch,fname,loss_per_epoch_val))

                elaT=time.time()-train_start_time
                if self.verb>0:
                     print("End-EpochL %d  loss train: %.3g,  val: %.3g,  bestVal:%.3g, gstep=%d elaT/min=%.1f" %(epoch,loss_per_epoch_train,loss_per_epoch_val,best_validation_accuracy,gstep,elaT/60.))

        train_hist['trainTime']= elaT
        if self.verb : print('all-train-end elaTime=%.1f sec'%train_hist['trainTime'])
        self.train_hist=train_hist
        if 0: # jan-off  , it crashes the job  
            coord.request_stop()
            coord.join(threads)

        #C mc.finalize()

#...!...!..................
    def predict(self,args,dom):
        print('start predict dom=%s, batch=%d ...'%(dom,args.batch_size))
        #used the save the model for predictions
        saver = tf.train.Saver()

        outU=[]; outZ=[]
 
        tensors_predictor= self.predictor_op(dom)
        fname='cosmoFlow3_best'
        #fname='cosmoFlow3_last'
        save_path = os.path.join(args.modelPath, fname)
        print('model restored from:',save_path)
        assert os.path.exists(save_path+'.index')

        totFrames=args.steps*args.batch_size
        with tf.Session() as sess:
            saver.restore(sess=sess,save_path=save_path)
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(coord=coord)
            print('threads setup done, loop over tot=%d frames in %d steps...'%(totFrames,args.steps))
            sum1=0
            for j in range(args.steps): # predict one batch at a time
                startT1 = time.time()
                U,Z,lossAvr = sess.run(tensors_predictor)
                sum1+=lossAvr
                for i in range(args.batch_size):
                    outU.append(U[i])
                    outZ.append(Z[i])
                if self.verb>0 and (j<2 or j%10==0 ):
                    print('pred batch %d  elaT=%.2f sec,'%(j,time.time() - startT1))
                    print("  test_loss/batch: %.3f"%lossAvr,' shapes Y,Z:',U.shape,Z.shape)
                    D=Z-U
                    print('true',U[0].tolist(),' delV=',D[0].tolist())

            coord.request_stop()
            coord.join(threads)
        return np.array(outU),np.array(outZ),sum1/args.steps

