#!/usr/bin/env python
""" 
train  CosmoFlow 3 on one KNL node

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Deep_Cosmo import Deep_Cosmo
from Plotter_Cosmo import Plotter_Cosmo


import argparse, os 
def get_parser():
    parser = argparse.ArgumentParser(
        description='train  multi-valued regression',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign',choices=['tune1','paper1'],
                         default='paper1',
                         help=" hyper-param set, aka design of the model")

    parser.add_argument("--dataSource",choices=['bb4','cs4'],
                        default='bb4',help="path to data vault")

    parser.add_argument("--modelPath",
                        default='model',help="TF model, checkpoints")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-e", "--epochs", type=int, default=5,
                        help="fitting epoch")

    parser.add_argument("-b", "--batch_size", type=int, default=3,
                        help="fit batch_size")

    parser.add_argument("-s", "--steps", type=int, default=2,
                        help="batches per epoch")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='cosmoFlow3'
    args.srunId=0 #int(os.environ['SLURM_PROCID'])
    args.submitHost=os.environ['HOST']
    args.noXterm=True
    if args.srunId==0:
        for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep= Deep_Cosmo()

if args.srunId==0:
    gra=Plotter_Cosmo(args)

deep.set_params(args)
deep.create_weigths()
deep.open_data_readers(args)

deep.train(args)

if args.srunId==0:  
    print('Jan-end, srunId=',args.srunId)
    import yaml
    DL=deep.train_hist
    outF=args.outPath+'/train_history.yaml'
    ymlf = open(outF, 'w')
    yaml.dump(DL, ymlf)
    ymlf.close()
    print('  closed  yaml:',outF)

    gra.plot_train_history(deep,args)
    gra.show(args,'train')
 
    
    
            
