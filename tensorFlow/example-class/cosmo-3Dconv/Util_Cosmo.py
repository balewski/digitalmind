from __future__ import print_function

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#  previous authors: 
#  Debbie Bard, LBL & Pete Mendygral, Cray Inc.

from ruamel.yaml import YAML
from tensorflow.contrib.training import HParams
import os
import tensorflow as tf
import random
import yaml
from pprint import pprint
import numpy as np

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class YParams(HParams):
    """ Yaml file parser derivative of HParams """

    def __init__(self, yaml_fn, config_name,verb):
        super(YParams, self).__init__()
        with open(yaml_fn) as yamlfile:
            for key, val in YAML().load(yamlfile)[config_name].items():
                if verb: print('hpar:',key, val)
                self.add_hparam(key, val)

""" Yaml file parser derivative of tensorflow.contrib.training.HParams """
""" Original code: https://hanxiao.github.io/2017/12/21/Use-HParams-and-YAML-to-Better-Manage-Hyperparameters-in-Tensorflow/ """

#...!...!....................
def read_yaml(ymlFn,verb=1):
        if verb:  print('  read  yaml:',ymlFn)

        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd)
        ymlFd.close()
        if verb: print(' done, size=%d'%len(bulk))
        return bulk

#............................
def write_yaml(outD,outF) :
        print('save yaml:',outF)
        ymlf = open(outF, 'w')
        yaml.dump(outD, ymlf) #, Dumper=yaml.CDumper
        ymlf.close()

#............................  Based on Deep_Mito.py
def split_on_domains(allL):
        frac=0.15
        nTot=len(allL)
        nFrac=int(nTot*frac)
        assert nFrac>2

        #print('rawL',allL[:10])
        # shuffle list in place
        random.shuffle(allL,random.random)
        #print('shaffL',allL[:10])

        outD={}
        outD['test']=allL[:nFrac]
        outD['val']=allL[nFrac:2*nFrac]
        outD['train']=allL[2*nFrac:]

        for dom in outD:
            print(dom, ' size',len(outD[dom]))

        return outD
# - - - - - - - - - - - - - - - - - - - - - - 
def XXlocateTFRecDataJan(inputSrc, nameMask,verb=1):
    if inputSrc=='bb4':
        main_dir = os.environ['DW_PERSISTENT_STRIPED_cosmo_2018_10_dim128_cube']
    elif inputSrc=='cs4':  # Jan's data
        main_dir = "/global/cscratch1/sd/balewski/cosmoUniverse_2018_10/"
    else:
        bad_inp_dir

    source_dir = main_dir+nameMask+"/"
    for xx in [ main_dir,  source_dir]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use source_dir:',source_dir)
    allL=os.listdir(source_dir)

    outL=[]
    for x in allL:
        if 'tfrecord' not in x: continue
        if nameMask not in x: continue
        #fullN=source_dir+'/'+x
        fullN=x
        if len(outL)<1 and verb>0 :
            print('name',fullN)
        outL.append(fullN)
    if verb>0:  print('found %d tfrec for mask=%s'%(len(outL),nameMask))
    return source_dir,outL,main_dir


# - - - - - - - - - - - - - - - - - - - - - - 
def locateTFRecDataDebbie(inputSrc, domain,verb=1):
    if inputSrc=='bb':
        main_dir = os.environ['DW_PERSISTENT_STRIPED_CosmoFlow3param']
    elif inputSrc=='cs1':  # Debbie's
        main_dir = "/global/cscratch1/sd/djbard/cosmoML/data-3param"
    else:
        bad_67

    source_dir = main_dir+"/13000-2xDupe/"+domain
    for xx in [ main_dir,  source_dir]:
        if os.path.exists(xx): continue
        print(self.srunId,'Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use source_dir:',source_dir)
    allL=os.listdir(source_dir)

    outL=[]
    for x in allL:
        if 'tfrecord' not in x: continue
        #fullN=source_dir+'/'+x
        fullN=x
        if len(outL)<1 and verb>0 :
            print('name',fullN)
        outL.append(fullN)
    if verb>0:  print('found %d tfrec for dom=%s'%(len(outL),domain))
    return source_dir,outL

#=============================
def read_tfrecord(filename_queue,hparams):
    reader = tf.TFRecordReader()
    _,single_example = reader.read(filename_queue)
    parsed_record = tf.parse_single_example(
        single_example,features = {
            "3Dmap": tf.FixedLenFeature([],tf.string),
            "unitPar": tf.FixedLenFeature([],tf.string),
            "physPar": tf.FixedLenFeature([],tf.string)
        }
    )

    Xdata = tf.decode_raw(parsed_record['3Dmap'], tf.float32)
    Ydata = tf.decode_raw(parsed_record['unitPar'], tf.float32)
    AUXdata = tf.decode_raw(parsed_record['physPar'], tf.float32)

    par_frame_shape=[128,128,128]
    vox_vol=np.prod(par_frame_shape)+0.

    frame=tf.reshape(Xdata, par_frame_shape)

    #normalize
    frame/=tf.reduce_sum(frame/vox_vol)
    frame_4d=tf.expand_dims(frame,axis = 3)
    out_dim=hparams.dense_dims[-1]
    Yvect = tf.reshape(Ydata,[ out_dim])
    return frame_4d,Yvect

    '''
    xxxx
    NbodySimus /= (tf.reduce_sum(NbodySimus)/128**3+0.)
    NbodySimuAddDim = tf.expand_dims(NbodySimus,axis = 3)
    label = tf.reshape(labelDecode,[ out_dim])
    label = (label - tf.constant(hparams.zsAVG,dtype = tf.float32))/tf.constant(hparams.zsSTD,dtype = tf.float32)
    return NbodySimuAddDim,label
    '''



#=============================
def readDataSet(hparams,filenames,shuffle=True):
    #print "---readDataSet-ioCosmo-Jan------"
    #print('init readDataSet', len(filenames),shuffle)
    filename_queue = tf.train.string_input_producer(filenames,num_epochs=None,shuffle=shuffle)
    NbodySimus,label= read_tfrecord(filename_queue,hparams)

    NbodySimus_batch, label_batch = tf.train.shuffle_batch(
        [NbodySimus,label],
        batch_size = hparams.batch_size,
        num_threads = hparams.NUM_THREADS,
        capacity =hparams.CAPACITY,
        min_after_dequeue = hparams.MIN_AFTER_DEQUEUE,
        allow_smaller_final_batch=True)

    #print('init readDataSet end',NbodySimus_batch.shape,label_batch.shape)
    return  NbodySimus_batch, label_batch



'''  OLD , Debbie 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 

def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

class loadNpyData:
    def __init__(self,data,label):
        ### suggestion from James to cast as 32-bit
        self.data = data.astype(dtype = np.float32) ##data
        self.label = label.astype(dtype = np.float32) ##label
        print('shapes  data:',data.shape,' label:',label.shape)

    def convert_to(self,outF):
        filename = outF
        print('Writing ', filename)
        writer = tf.python_io.TFRecordWriter(filename)
        for index in range(len(self.data)):
            data_raw = self.data[index].tostring()
            label_raw = self.label[index].tostring()
            example = tf.train.Example(features = tf.train.Features(feature={'label_raw': _bytes_feature(label_raw),'data_raw': _bytes_feature(data_raw)}))
            writer.write(example.SerializeToString())
        writer.close()
        #return filename




# - - - - - - - - - - - - - - - - - - - - - - 


# - - - - - - - - - - - - - - - - - - - - - - 
def XXlocateNumpyData(args,verb=1):

    source_dir=args.rawPath
    for xx in [ source_dir]:
        if os.path.exists(xx): continue
        print(self.srunId,'Aborting on start, missing  dir:',xx)
        exit(1)
 
    if verb>0: print('use source_dir:',source_dir)
    dirL=os.listdir(source_dir)
    print(dirL)
    print(' found total dirs ',len(dirL))
    outL=[]

    meatN0='cosmoMeta.yaml'
    for dirN in dirL:
        if args.rawFilter not in dirN : continue
        inpPath= source_dir+'/'+dirN+'/out'
        fileL=os.listdir(inpPath)
        #print(fileL)
        #print(' found total files',len(fileL),inpPath)
        metaN=inpPath+'/'+meatN0
        if not os.path.exists(metaN): 
                print('Aborting2 , missing  meta-data:',metaN)
                exit(1)
        blob=read_yaml(metaN,0)
        #pprint(blob)

        for fN in fileL:
            #print('fN',fN)
            if args.rawSet not in fN : continue
            fullN=inpPath+'/'+fN  # print('name',fullN)
            outL.append((fullN,blob))
            #
        
    if verb>0:  print('found %d raw data for set=%s'%(len(outL),args.rawSet))
    return outL

#...!...!....................
def packTFRecord(inpL,idx,args):
    
    outF=args.tfrecPath+'/'+'%s_%d.tfrec'%(args.prjName,idx)
    print('packTFRecord ', len(inpL),idx,outF)

    dataL = []
    labelL = []
    auxL=[]
    for (fN,blob) in inpL:
        dataL.append(np.load(fN))
        labelL.append(blob['unitPar'])
    print('pack: num .npy files',len(labelL) )
    data_np=np.array(dataL)
    label_np=np.array(labelL)

    recObj=loadNpyData(data_np.reshape(-1,128,128,128,1),label_np.reshape(-1,3))
    recObj.convert_to(outF)
    print('Ms: saved',outF)
    return outF


#...!...!....................
def unpackTFRecord(tfr_fname):

    print('\n Open %s  and check the first entries'%tfr_fname)
    for serialized_example in tf.python_io.tf_record_iterator(tfr_fname):
        example = tf.train.Example()
        myLen=example.ParseFromString(serialized_example)
        print('ee2:',myLen)

        x_1 = np.fromstring(example.features.feature["data_raw"].bytes_list.value[0], dtype=np.float32)
        y_1 = np.fromstring(example.features.feature["label_raw"].bytes_list.value[0], dtype=np.float32)
        break

    # the numbers may be slightly different because of the floating point error.

    print ('tfr X:',x_1.shape,x_1[::30])
    print (' mass sum: %.3g'%np.sum(x_1))
    print ('tfr y:',y_1.shape,y_1)
    return x_1,y_1
#end-old Debbie
''' 
