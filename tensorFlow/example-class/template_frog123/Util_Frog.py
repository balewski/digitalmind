frameDim=2 # select 1,2D or 3D gauss
frameRange=128 # select num pixels per dim

import numpy as np
from scipy import signal

#............................
def my1DGauss_generator(x0,sig):
        sig3=int(3*sig)
        #print('p:',p,x0,sig)
        window = signal.gaussian(2*sig3, std=sig)
        #print('win:',window)
        
        F=[]
        ta=int(x0-sig3)
        tb=int(x0+sig3)        
        for t in range(frameRange):
                if t<=ta or t>=tb:
                        f=0
                else:
                    f= window[t-ta]
                F.append(f) 
        return np.array(F)


#............................
def func_parm_rescale(V,norm='fixMe'):
        #norm='as-is'
        if norm=='2unit':            
            assert 6==9 # fix me
            
        elif norm=='2phys':
            xU=V[:frameDim]
            sU=V[frameDim:]
            xP=(0.5*xU+0.5)* frameRange 
            sP=(0.07*sU+0.05)* frameRange 
             #print('sP',sP,' xP',xP)
            P=np.concatenate((xP,sP))
            return P
        elif norm=='as-is':
            return fV
        else:
            print('bad func transform',norm)
            bad_bad
