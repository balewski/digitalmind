__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import os, time, yaml
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()

#from pprint import pprint
import tensorflow as tf
from IO_Frog_TF1_8 import frogInput_fn, geneL
from Model_Frog import frogModel_fn

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from ruamel.yaml import YAML
from tensorflow.contrib.training import HParams
import tensorflow as tf
import numpy as np
from Util_Frog import my1DGauss_generator,func_parm_rescale,frameDim

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class YParams(HParams):
    """ Yaml file parser derivative of HParams """

    def __init__(self, yaml_fn, config_name):
        super(YParams, self).__init__()
        with open(yaml_fn) as yamlfile:
            for key, val in YAML().load(yamlfile)[config_name].items():
                print('hpar:',key, val)
                self.add_hparam(key, val)

""" Yaml file parser derivative of tensorflow.contrib.training.HParams """
""" Original code: https://hanxiao.github.io/2017/12/21/Use-HParams-and-YAML-to-Better-Manage-Hyperparameters-in-Tensorflow/ """


#............................
#............................
#............................
class Deep_Frog(object):
    """Contains all operations specific for this project"""

    def __init__(self,args, mode=None):
        self.outPath=args.outPath
        self.dataPath=args.dataPath
        self.name=args.prjName
        self.verb=args.verb

        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(1)

        print(self.__class__.__name__,' frameDim=',frameDim)
        if mode=='format': 
            self.frameId=0
            return

        # append project name w/ design name
        self.name+='_'+args.modelDesign

        self.tfLogPath=args.outPath+'/'+args.modelDesign        
        # . . . load hyperparameters
        yaml_cnfg='hpar_'+args.prjName+'.yaml'         
        self.hparams=YParams(os.path.abspath(yaml_cnfg),args.modelDesign )

  
        #self.hparams.add_hparam('modelHook', self)
        #pprint(self.hparams)

        ymlFn=self.dataPath+'/split_domain.yaml'
        print(' ..... load domains split from ',ymlFn)
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd)
        ymlFd.close()
        self.domSplitD=bulk

        for key in ['Xshape','Yshape']:
            self.hparams.add_hparam(key, bulk[key])
        if mode=='train':
            self.epochs=args.epochs
            self.batch_size=args.batch_size
            
        print(self.__class__.__name__,' my prj:',self.name)
        
#............................
    def generate_frames(self,args):
        noiseAmpl=args.noise_ampl
        out={}
        for gene in geneL: out[gene]=[]

        start = time.time()
        for i in range(args.events_per_tfrec):
            self.frameId+=1
            # generate  params normalized to unity
            U=np.random.uniform(-0.5,0.5,size=2*frameDim)
            #print('U:',U,' xshape:',xbins.shape)            
            P=func_parm_rescale(U,'2phys')
            #print('P:',P)
            auxV=np.append(P,self.frameId)

            funcV=my1DGauss_generator(P[0],P[0+frameDim])
            if frameDim==2:
                funcV2=my1DGauss_generator(P[1],P[1+frameDim])
                #print('2x1D shapes',funcV.shape)
                funcV=np.tensordot(funcV,funcV2,axes=0)
                #print('2D shape',funcV.shape)

            if noiseAmpl>1e-3:
                noiseV= np.random.uniform(-noiseAmpl,noiseAmpl,size=funcV.shape)
                atten=1-noiseAmpl
                funcV=np.add(atten*funcV,noiseV)

            if i%100==0 and self.frameId<50:
                print('tfrec i=',i,' parmU:',U,' aux:',auxV,' nDim=',funcV.ndim)
            out['X'].append(funcV)
            out['Y'].append(U)
            out['AUX'].append(auxV)

        for item in out:
                out[item]=np.array(out[item]).astype(np.float32)
        if self.verb>1 :print('%d frames generated , elaT=%.1f sec'%(i,(time.time() - start)))
        return out


#............................
    def assemble_model(self):
        # build estimator

        self.myEstimator = tf.estimator.Estimator(model_fn=frogModel_fn,
             model_dir=self.tfLogPath,params=self.hparams)
        if self.verb : print('myEstimator created')

#............................
    def assemble_data_pipes(self):
        # read data list

        hpar=self.hparams  #tmp
        tf.logging.set_verbosity(tf.logging.INFO) 

        # create training data input pipeline
        train_input_fn =frogInput_fn(self.domSplitD['train'],
                batchsize=self.batch_size,epochs=self.epochs,
                variable_scope='train_data_pipeline',
                shuffle_buffer_size=hpar.shuffle_buffer_size,
                num_parallel_reads=hpar.num_parallel_reads,
                num_parallel_calls=hpar.num_parallel_calls,
                num_prefetch=hpar.num_prefetch)

        num_steps=int(self.domSplitD['train_size']*self.epochs/self.batch_size)
        if self.verb :  print('ASD: train_input_fn  , num_step=',num_steps)

        self.train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn,
                max_steps= num_steps,hooks=[])

        if self.verb :     print('TD: train_spec  ok')
        # create validation data input pipeline
        self.valid_input_fn = frogInput_fn(self.domSplitD['val'],
                batchsize=self.batch_size,epochs=1,
                variable_scope='valid_data_pipeline')
        self.valid_spec = tf.estimator.EvalSpec(input_fn=self.valid_input_fn, hooks=[],throttle_secs=self.hparams.validation_period)
    
        if self.verb :     print('TD: valid_spec  ok')

#............................
    def train_and_evaluate(self):
        print('TD:  train ...')
        tf.logging.set_verbosity(tf.logging.INFO) # Just to have some logs to display for demonstration

        # train
        startT = time.time()

        tf.estimator.train_and_evaluate(self.myEstimator, self.train_spec, self.valid_spec)
        fitTime=time.time() - startT
        print('TD: train_and_evaluate done, fit %.2f min\n'%(fitTime/60.))

#............................
    def do_pseudo_predictions(self,m):
        print('M:make %s prediction(s), on valid-data, no true labels avaliable:'%m)
        test_input_fn=self.valid_input_fn #tmp
        j=0
        for onePred in self.myEstimator.predict(test_input_fn):
            # do something with prediction
            predDig=onePred
            if j<m : print('j=',j,onePred)
            j+=1
         
        print('TD:  predict done, nInp=',j)



#............................
    def get_correlation(self,a):
        #print('a dims:',a.shape,'\n dump a:')

        # compute cov matrix
        c=np.cov(a,rowvar=False)
        nv=c.shape[0]
        nvL=range(nv)
        #print('cov M:')
        #pprint(c)
        c1=np.diagonal(c)
        c2=np.sqrt(c1)
        print('one std dev:',c2)
        avrSig=c2.mean()
        print('global  residua avr=%.3f  var=%.3f '%(avrSig, avrSig*avrSig))
        parNameL=self.domSplitD['parmName']
        magU=self.domSplitD['parmMagU']

        print('\nnames ',end='')
        [ print('     %s'%parNameL[i],end='') for i in nvL]

        print('\nresidua/%.1f: '%magU,end='')
        [ print('U%d-> %4.1f%s  '%(i,100*c2[i]/magU,chr(37)),end='') for i in nvL ]

        lossMAE=np.absolute(c2).mean()
        c3=np.power(c2,2);    lossMSE=c3.mean()
        print('\n global lossMSE=%.3f , lossMAE=%.3f (computed)'%( lossMSE,lossMAE))


        #print('\ncorrelation matrix for %d dim vector'%nv)
        r=np.corrcoef(a,rowvar=False)
        #pprint(r)

        print('\nnames  ',end='')
        [ print('   %s '%parNameL[i],end='') for i in nvL]
        print('\ncorrM  ',end='')
        [ print('  __var%1d__ '%i,end='') for i in nvL]
        for k in nvL:
            print('\n var%1d '%k,end='')
            [ print('     %6.3f'% r[i][k],end='') for i in range(k+1)]
            #print('      #  '*(nv-k),end='')
        print()
