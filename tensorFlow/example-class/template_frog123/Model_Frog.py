__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import tensorflow as tf
import numpy as np

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class ModelFrog(object):
    def __init__(self, hparams, input_x, is_training):

        self.hpar = hparams
        self.frameDim=len(hparams.Xshape)-2
        self.is_training = is_training
        self.input_x = input_x
 
        with tf.variable_scope('training_counters', reuse=tf.AUTO_REUSE) as _:
            self.global_step = tf.train.get_or_create_global_step()

        self.build_graph()
        self.loss = None
        self.optimizer = None

    # . . . . . . . . . . . . 
    def build_graph(self): 

        print('build_graph expected shapes inp:',self.hpar.Xshape,' out:',self.hpar.Yshape,' isTrain=',self.is_training)
        _h = tf.reshape(self.input_x, self.hpar.Xshape)
        print('INP', _h.get_shape().as_list())

        dropFracFC = self.hpar.dropFracFC
        dropFracCNN = self.hpar.dropFracCNN
        activ=tf.nn.leaky_relu

        with tf.variable_scope('frog_CNNs') as _:
            kernel_sizes = self.hpar.conv_kernels
            filters = self.hpar.conv_filters
            strides = self.hpar.conv_strides
            padd='same'
                        
            for i, (_kern, _filt, _strid) in enumerate(zip(kernel_sizes, filters, strides)):
                _name = 'conv1d_%i'%i
                #print(i,'CNN add layer:',_kern, _filt, _strid,_name)
                if self.frameDim==1:
                    cnnLayer= tf.layers.conv1d
                if self.frameDim==2:
                    cnnLayer= tf.layers.conv2d
                _h =cnnLayer(_h, filters=_filt, kernel_size=_kern, strides=_strid, name=_name, activation=activ, padding=padd) 

                    
                if self.hpar.batch_norm:
                    channels_axis = len(self.hpar.Xshape)-1
                    _h = tf.layers.batch_normalization(_h, axis=channels_axis, training=self.is_training)
                if dropFracCNN > 0:
                    _h = tf.layers.dropout(_h, rate=dropFracCNN, training=self.is_training)
                print('Layer', _name, 'Output size:', _h.get_shape().as_list())
            
            # reshape tensor into a batch of vectors  (flatten it)
            dL=_h.get_shape().as_list() # dimensons of input tesnor from CNN
            dimFlat=np.prod(dL[1:]) # this is the total feaures count on output from CNN block
            print('flatten',dL,' to ',dimFlat)
            _h = tf.reshape(_h, [-1,dimFlat],name='toFlat' )
 

        with tf.variable_scope('frog_FCs'):
            for _dim in self.hpar.dense_dims:
                i+=1
                _name = 'FC_%i'%i
                _h = tf.layers.dense(_h, _dim, name=_name , activation=activ)
                if dropFracFC > 0:
                    _h = tf.layers.dropout(_h, rate=dropFracFC,training=self.is_training)
                print('Layer', _name, 'Output size:', _h.get_shape().as_list())

            # Compute multi-valued predictions
            nZ=self.hpar.Yshape[1]
            Z = tf.layers.dense(_h, nZ, activation=None, name='output_Z')
            print('Layer last Output size:', Z.get_shape().as_list(),Z.name)

        # how to concatenate
        #if self.hpar.u_net and not is_last_layer:
        #        _h = tf.concat([down.pop(), _h], axis=channels_axis)

        self.predictions=Z

    # . . . . . . . . . . . . . .        
    def define_loss(self, labels):
        with tf.name_scope('loss-MSE'):
            self.loss = tf.reduce_mean(tf.losses.mean_squared_error(labels=labels, predictions=self.predictions))


    # . . . . . . . . . . . . . . 
    def define_optimizer(self):
        with tf.variable_scope('optimizer') as _:
            self.optimizer = tf.train.AdamOptimizer(self.hpar.learning_rate).minimize(self.loss, global_step=self.global_step)


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 

def frogModel_fn(features, labels, params, mode):
    """ Build graph and return EstimatorSpec 
    Returns:
        (EstimatorSpec): Model to be run by Estimator.
    """
    is_training = (mode == tf.estimator.ModeKeys.TRAIN)

    model = ModelFrog(params, input_x=features, is_training=is_training)

    if mode is not tf.estimator.ModeKeys.PREDICT:
        # loss and optimizer are not needed for inference
        model.define_loss(labels)
        model.define_optimizer()
        
    print('next model_fn:',type(model.predictions), type(labels),mode)


    # . . . . .  alternative returns defined below .....  
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=model.predictions)
    
    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(
            mode, loss=model.loss)

    return tf.estimator.EstimatorSpec(
        predictions=model.predictions,
        loss=model.loss,
        train_op=model.optimizer,
        mode=mode)

