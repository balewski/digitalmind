#!/usr/bin/env python
""" 
Example for creating TFrecords from np.array and tran/val/test lists
INPUT: none
Output: tfrecords & lists
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os
import numpy as np
import tensorflow as tf
import yaml

from IO_Frog_TF1_8 import frog_np_to_tfrecord, frog_tfrecord_to_np
from Plotter_Frog import Plotter_Frog
from Deep_Frog import Deep_Frog 
from Util_Frog import frameDim,frameRange

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train  multi-valued regression',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("--dataPath",
                        default='data',help="path to data vault")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=21000,
                        help="events to consider, use 0 for all")

    args = parser.parse_args()
    args.prjName='frog'
    args.events_per_tfrec=1024
    args.noise_ampl=0.1

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================

args=get_parser()
gra=Plotter_Frog(args )
deep=Deep_Frog(args,mode='format')
num_tfrec=int(args.events/args.events_per_tfrec)
print('will write %d tfrecords of %d to dir=%s'%(num_tfrec,args.events_per_tfrec,args.dataPath))
tfrecNL=[]
for irec in range(num_tfrec):
    segD=deep.generate_frames(args)
    tfr_filename =  args.dataPath+'/frogRec.%d'%irec+'.tfrecords'
    tfrecNL.append(tfr_filename)
    frog_np_to_tfrecord(segD,tfr_filename, verbose=True)

# - - - -   Assemble data summary , define domain split
itL=np.random.permutation(num_tfrec).astype(np.int32)
kTest=int(0.1*num_tfrec)
assert kTest>0

splitD={'val':itL[0:kTest],'test':itL[kTest:2*kTest],'train':itL[2*kTest:]}

outD={}
for dom in splitD:
    idxL=splitD[dom]
    fnmL=[ tfrecNL[x] for x in idxL]        
    outD[dom]=fnmL
    outD[dom+'_size']=len(fnmL)*args.events_per_tfrec

splitD=outD
splitD['tfrecord_size']=args.events_per_tfrec
if frameDim==1:
    splitD['Xshape']=[-1,frameRange,1]
else:
    splitD['Xshape']=[-1,frameRange,frameRange,1]
splitD['Yshape']=[-1,2*frameDim] # pos & sig per dim
splitD['parmMagU']=0.5 # magnitude of normalized params

# generate list of parameter names
parmName=[]
for jd in range(frameDim):
    parmName.append('pos%d'%jd)
for jd in range(frameDim):
    parmName.append('sig%d'%jd)
splitD['parmName']=parmName
print('splitD',splitD)

ymlFn=args.dataPath+'/split_domain.yaml'
ymlFd = open(ymlFn, 'w')
yaml.dump(splitD, ymlFd)
ymlFd.close()
print('\ncreated: ',ymlFn)

gra.plot_data_frames(segD,9,figId=7)
gra.plot_data_fparams(segD['Y'],'U-pars',figId=6, parmName=parmName)
#gra.plot_data_fparams(segD['AUX'],'P-pars',figId=61)
gra.show(args,'form')

end_ok11

# 1-2. Check if the data is stored correctly

tfr_filename =tfrecNL[-1]  
nrec=0
print('\nM: Cross-test for ',tfr_filename)

segR=frog_tfrecord_to_np(tfr_filename)

print('read in nrec=',segR['Y'].shape[0], 'per tfrecord file')
idx=7
# the numbers may be slightly different because of the floating point error.
print ('org X:',segD['X'][idx][::30])
print ('tfr X:',segR['X'][idx][::30])
print ('org Y:',segD['Y'][idx])
print ('tfr y:',segR['Y'][idx])
