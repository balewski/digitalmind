import os
import numpy as np
from matplotlib import cm as cmap

import socket  # for hostname
from tensorflow.contrib.keras.api.keras.utils import plot_model


import time

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_Frog(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args):
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt

        print(self.__class__.__name__,':','Graphics started')
 
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        self.maxU=0.7

    #............................
    def show(self,args,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()                
                figName='%s/%s_%s_f%d'%(self.outPath,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()

      
#............................
    def plot_train_history(self,args,figId=10):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,6))
        prjName=args.prjName+'_'+args.modelDesign
        nrow,ncol=2,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2 )
        ax2 = self.plt.subplot2grid((nrow,ncol), (1,1), colspan=2 ) 
        ax3 = self.plt.subplot(nrow, ncol, 1)
       
        DL=self.train_hist
        STP=self.train_hist['step']
        endLoss=DL['loss'][-1]
 
        tit1='%s, train %.1f min, end-loss=%.3g'%(prjName,DL['titFitSec']/60.,endLoss)
        ax1.set(ylabel='loss ',title=tit1,xlabel='steps') 
        
        ax1.plot(STP,DL['loss'],'.-.',label='loss' )
        ax1.legend(loc='best')
        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')

        # histogram of timing
        rateL=self.train_hist['rate'][1:] # skip 1st data point
        ax3.hist(rateL,bins=30)

        zmu=np.array(rateL)
        resM=zmu.mean()
        resS=zmu.std()
        ax3.set(title='avr %.1f+/-%.1f batch/sec'%(resM,resS), xlabel='global_steps/sec')


#............................
    def ana_event_accumulator(self,path):
        from tensorboard.backend.event_processing import event_accumulator        
        print('\n Open event_accumulator in %s '%path)

        size_guide={
            event_accumulator.COMPRESSED_HISTOGRAMS: 500,
            event_accumulator.IMAGES: 4,
            event_accumulator.AUDIO: 4,
            event_accumulator.SCALARS: 0,
            event_accumulator.HISTOGRAMS: 1,}
        ea = event_accumulator.EventAccumulator(path ,size_guidance=size_guide)

        ea.Reload()
        tagsD=ea.Tags()
        print('my Tags:', type(tagsD))
        print(tagsD)

        recLoss=ea.Scalars('loss')
        recRate=ea.Scalars('global_step/sec')
        #print('rr',recRate)
        #rr1=recRate[0] #ScalarEvent(wall_time=156.572, step=101, value=8.90277)
        #print(rr1)
        
        hist={'step':[], 'loss':[],'rate':[]}
        totSec=0

        for x,x1 in zip(recLoss,recRate):
            t,st,lo=x
            _,_,rt=x1
            if len(hist['step']) >0:                
                totSec+=(st-st0)/rt
            st0=st
            hist['step'].append(st)
            hist['loss'].append(lo)
            hist['rate'].append(rt)
        
        #step_size=100  # tmp, fix it later

        print('last scalers: step=',st,' loss=',lo,' rate=',rt, ' tot_train_time =%.1f min'%(totSec/60.))
        hist['titFitSec']=totSec
        self.train_hist=hist

#............................
    def plot_data_frames(self,dataA,nFr,idxL=[],figId=7):
 
        X=dataA['X']
        Y=dataA['Y']
        Aux=dataA['AUX']
        
        if len(idxL)==0:
            mxFr=X.shape[0]
            idxL=np.random.randint(mxFr,size=(nFr))

        nFr=min(len(idxL),nFr)
        frameDim=X[0].ndim
        nrow,ncol=3,3
        print('plot input for trace idx=',idxL,' frameDim=',frameDim)

        inY=6
        if frameDim==2: inY=9
        fig=self.plt.figure(figId,facecolor='white', figsize=(9,inY))
        self.figL.append(figId)

        j=0
        for it in idxL[:nFr]:
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j)
            j+=1
            #print('ee',it) #,Aux[it])
            id=Aux[it][-1]
            amplV=X[it]
            if frameDim==1:
                ax.plot(amplV)
            else:
                ax.imshow(amplV, cmap = cmap.rainbow)
 

            tit='id%d'%(id)
            ptxtL=[ '%.2f'%x for x in Y[it]]
            tit+=' U:'+','.join(ptxtL)
            #tit='%.2f'%Aux[it][1]
            ax.set(title=tit[:35]+'..',ylabel='ampl', xlabel='time bins')
            #ax.legend(loc='best') #upper right')

            ax.grid()

#............................
    def plot_data_fparams(self,U,tit,figId=6, parmName=[]):
        nPar=U.shape[1]
        if len(parmName)==0:
            parmName=[ 'par_%d'%i for i in range(nPar) ]
        nrow,ncol=2,int(nPar/2+1.5) # displays 2 pars per plot

        fig=self.plt.figure(figId,facecolor='white', figsize=(3.*ncol,2.6*nrow))
        self.figL.append(figId)
        mm=self.maxU
        binsX= np.linspace(-mm,mm,30)

        j=1
        for i in range(0,nPar,2):
            iPar=min(i,U.shape[1]-2) # account for odd num of params
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            self.plt.colorbar(img, ax=ax)
            ax.set(title=tit, xlabel=parmName[iPar], ylabel=parmName[iPar+1])
            ax.grid()

        for i in range(0,nPar):
            y1=U[:,i]
            ax=self.plt.subplot(nrow,ncol,j) ; j+=1
            ax.hist(y1,bins=30)
            ax.set(xlabel=parmName[i])
            ax.set_xlim(-mm,mm)
            ax.grid()
  
#............................
    def plot_fparam_residua(self,U,Z,figId=9,tit=''):
        nPar=Z.shape[1]
        nrow,ncol=3,nPar
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        self.figL.append(figId)
        inSz=2.5
        self.plt.figure(figId,facecolor='white', figsize=(inSz*nPar*1.2,nrow*inSz))

        assert U.shape[1]==Z.shape[1]
        self.parSum=[]

        j=0
        mm=self.maxU
        binsU= np.linspace(-mm*1.5,mm*1.5,90)
        binsX= np.linspace(-mm,mm,30)
        binsY= np.linspace(-mm/2,mm/2,30)

        for iPar in range(0,nPar):
            ax1 = self.plt.subplot(nrow,ncol, j+1)
            ax2 = self.plt.subplot(nrow,ncol, j+1+ncol*2)
            ax3 = self.plt.subplot(nrow,ncol, j+1+ncol*1)
            ax1.grid(); ax2.grid(); ax3.grid()

            u=U[:,iPar]
            z=Z[:,iPar]
            #j rho,sigRho=get_correlation(u,z)

            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            # beutification
            self.plt.colorbar(img, ax=ax1)
            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=1,transform=ax1.transAxes) #diagonal
            # 

            ax1.set(title='corr iPar=%d'%iPar, xlabel='pred Z_%d'%iPar, ylabel='true U_%d'%iPar)
            #j ax1.text(0.2,0.1,'rho=%.3f +/- %.3f'%(rho,sigRho),transform=ax1.transAxes)

            if iPar==0: ax1.text(0.1,0.9,tit,transform=ax1.transAxes)
            if iPar==1: ax1.text(0.1,0.9,'n=%d'%(u.shape[0]),transform=ax1.transAxes)


            # .... compute residua 
            zmu=z-u
            resM=zmu.mean()
            resS=zmu.std()
            #j sum={'resM':resM,'res-std-dev':resS,'rho':rho,'sigRho':sigRho}
            #j self.parSum.append(sum)

            # ......  2D residua
            zsum,xbins,ybins,img = ax3.hist2d(z,zmu,bins=[binsX,binsY], cmin=1,
                                   cmap = self.plt.cm.rainbow)
            self.plt.colorbar(img, ax=ax3)
            ax3.set( xlabel='pred Z_%d'%iPar, ylabel='residue Z-U')
            ax3.axhline(0, color='green', linestyle='--')
            ax3.set_xlim(-mm,mm) ; ax3.set_ylim(-mm/2,mm/2)

            # ..... 1D residua
            ax2.hist(zmu,bins=binsU)

            ax2.set(title='avr res%d: %.2f+/-%.2f'%(iPar,resM,resS), xlabel='residue: Z_%d - U_%d'%(iPar,iPar), ylabel='traces')

            ax2.set_xlim(-mm/2,mm/2)

            j+=1

        return
                                
#............................
    def plot_fparam(self,Z,figId=16):
        nPar=Z.shape[1]
        nrow,ncol=1,int(nPar/2+0.5) # displays 2 pars per plot
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        if figId in self.figL: # append this plot to training history
            self.plt.figure(figId)
            nrow,ncol=1,3
        else:
            self.figL.append(figId)
            self.plt.figure(figId,facecolor='white', figsize=(3.0*ncol,2.5))

        mm=self.maxU
        binsX= np.linspace(-mm*1.2,mm*1.2,30)

        # plot correlation between latent space variables - should be none
        j=0
        for iPar in range(0,ncol):
            iPar2=(iPar+ncol)%nPar
            y1=Z[:,iPar] ; y2=Z[:,iPar2]
            ax=self.plt.subplot(nrow,ncol,j+1)
            j+=1
            #img=ax.scatter(y1,y2, alpha=.4, s=8, cmap='cool')
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            # beautification
            self.plt.colorbar(img, ax=ax)
            ax.set(title='pred Z %d-%d'%(iPar,iPar2), xlabel='Z_%d'%iPar, ylabel='Z_%d'%(iPar2))

        return
