#!/usr/bin/env python
""" 
Example for using TF estimator and pipeline
read input : tfrecord + trained model
 make only predictions

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Deep_Frog import Deep_Frog 
from Plotter_Frog import Plotter_Frog 
from IO_Frog_TF1_8 import frog_tfrecord_to_np,Xgene,Ygene
import tensorflow as tf
import numpy as np

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train    multi-valued regression',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign',choices=['hset2a','hset2b'],
                         default='hset2a',
                         help=" hyper-param set, aka design of the model")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")


    args = parser.parse_args()
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    args.prjName='frog'
    
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

gra=Plotter_Frog(args)
deep=Deep_Frog(args,mode='predict')
deep.assemble_model()

segR=frog_tfrecord_to_np(deep.domSplitD['test'][0])

test_input_fn =tf.estimator.inputs.numpy_input_fn(x=segR[Xgene], num_epochs=1, 
                                                  shuffle=False)
Ustream=segR[Ygene]
Zstream=[]
j=0; nBad=0
for Z in deep.myEstimator.predict(test_input_fn):
    Zstream.append(Z)
    if j==0: print('j=',j,Z)
    U=Ustream[j]
    if j<0: print(j,'U,Z=',U,Z,U-Z)
    #if digU!=digZ: nBad+=1
    j+=1
Zstream=np.array(Zstream)

deep.get_correlation(Ustream-Zstream)
gra.plot_fparam_residua(Ustream,Zstream)
gra.plot_fparam(Ustream,figId=15)
gra.plot_fparam(Zstream,figId=16)
#gra.plot_data_frames(segR,9,figId=7)

gra.show(args,'pred')    
