__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import tensorflow as tf
import numpy as np

# pylint: disable-msg=C0103
# pylint: disable=too-many-instance-attributes

''' 
this template was oryginaly assembled by Mustafa,
__author__ = "Mustafa Mustafa"
__email__  = "mmustafa@lbl.gov"
Jan B. has adopted it further.
'''
geneL=['X','Y','AUX']
Xgene=geneL[0]
Ygene=geneL[1]

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def frog_tfrecord_to_np(tfr_fname):
    # note - wild cards are not accepted by 
    print('\n Open %s and  convert it into in-memory np.array'%tfr_fname)

    out={}
    for gene in geneL: out[gene]=[]

    XL=[]; YL=[]; AuxL=[]
    for ser_item in tf.python_io.tf_record_iterator(tfr_fname):
        tftUtil = tf.train.Example()
        myLen=tftUtil.ParseFromString(ser_item)
        for gene in geneL:
            _x = np.fromstring(tftUtil.features.feature[gene].bytes_list.value[0], dtype=np.float32)
            out[gene].append(_x)

    for gene in geneL:
        out[gene]=np.array(out[gene])

    print('frog Input2nparray: got dataX:',out['X'].shape,' dataY:',out['Y'].shape,' one rec len/kB=%.2f',myLen/1014)
    return out

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def frog_np_to_tfrecord(dataA, result_tf_file, verbose=True):
    objNL=list(dataA.keys())
    nEve=dataA[objNL[0]].shape[0]
    #print('inp nEve=',nEve,objNL,' outFN=',result_tf_file)
    writer = tf.python_io.TFRecordWriter(result_tf_file)

    def _bytes_feature(value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    for idx in range(nEve):
        features = {}
        for objN in objNL:
            features[objN] = _bytes_feature(dataA[objN][idx].tostring())

        example = tf.train.Example(features=tf.train.Features(feature=features))
        serialized = example.SerializeToString()
        writer.write(serialized)
    writer.close()


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def frogInput_fn(filenames, batchsize, epochs, variable_scope,
                   shuffle_buffer_size=4, num_parallel_reads=4,
                   num_parallel_calls=4, num_prefetch=4):
 
    """ creates a tf.data.Dataset and feeds and augments data from tfrecords
    Returns:
        data input function input_fn
    """
    # . . . . . . . . . . . . . .                
    def parse_fn(exampleInp):
        "Parse TFExample records and perform data augmentation"
        example_fmt = {}
        for gene in geneL:
            example_fmt[gene] = tf.FixedLenFeature([], tf.string)
        parsed_record = tf.parse_single_example(exampleInp, example_fmt)

    
        Xdata = tf.decode_raw(parsed_record[Xgene], tf.float32)
        Ydata = tf.decode_raw(parsed_record[Ygene], tf.float32)        

        return Xdata, Ydata
 
    # . . . . . . . . . . . . . .        
    def input_fn():
        """ create input_fn for Estimator training
        Returns: tf.Tensors of features and labels
        """
        with tf.variable_scope(variable_scope) as _:
            fnameList=tf.data.Dataset.list_files(filenames, shuffle=False)
            dataset = tf.data.TFRecordDataset(fnameList,compression_type=None,
                         buffer_size=2*shuffle_buffer_size,
                         num_parallel_reads=num_parallel_reads)
            dataset = dataset.shuffle(shuffle_buffer_size)
            dataset = dataset.repeat(epochs)
            dataset = dataset.map(map_func=parse_fn, 
                                  num_parallel_calls=num_parallel_calls)
            dataset = dataset.prefetch(num_prefetch)
            dataset = dataset.batch(batchsize)

            data_it = dataset.make_one_shot_iterator()
            X, Y = data_it.get_next()
            print('frogInput_fn :',X,Y)
        return X, Y

    return input_fn


