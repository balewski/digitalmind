#!/usr/bin/env python
#import numpy as np
#import tensorflow as tf
from tensorboard.backend.event_processing import event_accumulator

#eventName='out/hset2a/events.out.tfevents.1533757187.cori10'
eventName='out/hset2a/' # no need to kmnow the Event-name details


print('\n Open %s  and access some scaler data '%eventName)

size_guide={ # see below regarding this argument
   event_accumulator.COMPRESSED_HISTOGRAMS: 500,
   event_accumulator.IMAGES: 4,
   event_accumulator.AUDIO: 4,
   event_accumulator.SCALARS: 0,
   event_accumulator.HISTOGRAMS: 1,}
'''
size_guidance: Information on how much data the EventAccumulator should
  store in memory. The DEFAULT_SIZE_GUIDANCE tries not to store too much
  so as to avoid OOMing the client. The size_guidance should be a map
  from a `tagType` string to an integer representing the number of
  items to keep per tag for items of that `tagType`. If the size is 0,
  all events are stored.
'''

ea = event_accumulator.EventAccumulator(eventName ,size_guidance=size_guide)

ea.Reload()
tagsD=ea.Tags()
print('my Tags:', type(tagsD))
from pprint import pprint
pprint(tagsD)

recL=ea.Scalars('loss')
for x in recL: 
    t,s,v=x
    print(t,s,v)
