#!/usr/bin/env python
import os
import numpy as np
import tensorflow as tf

__author__ = "Mustafa Mustafa"
__email__  = "mmustafa@lbl.gov"
def _bytes_feature(value):
	return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def np_to_tfrecords(X, Y, result_tf_file, verbose=True):
    print('inp', type(X), type(Y),' outFN=',result_tf_file)
    writer = tf.python_io.TFRecordWriter(result_tf_file+'.tfrecords')

    for idx in range(X.shape[0]):
        x = X[idx]
        y = Y[idx]
        
        features = {}
        features['X'] = _bytes_feature(x.tostring())
        features['Y'] = _bytes_feature(y.tostring())

        example = tf.train.Example(features=tf.train.Features(feature=features))
        serialized = example.SerializeToString()
        writer.write(serialized)
    writer.close()


        
#################################    
##      Test and Use Cases     ##
#################################


# create training data input pipeline
# prepare data - I do not want to user tensorLayers for modeling
import tensorlayer as tl
TF_DATA_DIR = 'tmp1'
X_train, y_train, X_val, y_val, X_test, y_test = tl.files.load_mnist_dataset(shape=(-1,784),path=TF_DATA_DIR)

y_train=np.reshape(y_train,(-1,1)).astype(np.int32)
print('got X:',X_train.shape,X_train.dtype,' Y:',y_train.shape, y_train.dtype)
xx= X_train
yy =y_train
    
# 1-1. Saving a dataset with input and label (supervised learning)
block_size=6400
num_blocks=int(xx.shape[0]/block_size)
outPath='data_tfrStr_mnist2'
assert  os.path.exists(outPath)

print('will write %d blocks of %d to dir=%s'%(num_blocks,block_size,outPath))
for ib in range(num_blocks):
    xRec=xx[ib*block_size : (ib+1)*block_size]
    yRec=yy[ib*block_size : (ib+1)*block_size]
    tfr_filename =  outPath+'/mnistBlk.%d'%ib
    np_to_tfrecords(xRec, yRec,tfr_filename, verbose=True)

# 1-2. Check if the data is stored correctly

ib=0
tfr_filename =  outPath+'/mnistBlk.%d'%ib
nrec=0
print('\nOpen the saved file and check the first entries, ',tfr_filename)
for serialized_example in tf.python_io.tf_record_iterator(tfr_filename+'.tfrecords'):
    example = tf.train.Example()
    example.ParseFromString(serialized_example)
    myLen=example.ParseFromString(serialized_example)

    x_1 = np.fromstring(example.features.feature['X'].bytes_list.value[0], dtype=np.float32)
    y_1 = np.fromstring(example.features.feature['Y'].bytes_list.value[0], dtype=np.int32)
    nrec+=1

print('read in nrec=',nrec, 'per tfrecord, one rec len/B=',myLen)
    
# the numbers may be slightly different because of the floating point error.
absId=ib
print ('org X:',xx[absId][::30])
print ('tfr X:',x_1[::30])
print ('org Y:',yy[absId])
print ('tfr y:',y_1)

