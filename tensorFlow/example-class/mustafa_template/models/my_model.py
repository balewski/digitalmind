__author__ = "Mustafa Mustafa"
__email__  = "mmustafa@lbl.gov"

import tensorflow as tf

# pylint: disable-msg=C0103
# pylint: disable=too-many-instance-attributes

class MyModel(object):
    def __init__(self, params, input_x=None, is_training=True):

        self._params = params
        self._is_training = is_training
        self._data_format = params.data_format
        self.N_DIGITS=10

        #print('myMod0:',is_training, input_x)
        #print('myMod1 hyper-params:',params)

        assert input_x is not None

        self.input_x = input_x
 
        with tf.variable_scope('training_counters', reuse=tf.AUTO_REUSE) as _:
            self.global_step = tf.train.get_or_create_global_step()

        self.build_graph_v1()
        self.loss = None
        self.optimizer = None


    def build_graph_v1(self): # self._params decide on the shape
        """ network """
        
        conv_defaults = {'activation': tf.nn.relu,
                         'kernel_initializer': 'he_normal',
                         'padding': 'same',
                         'data_format':'channels_last'}
        # Jan: I could not make conv_defaults to work  -ignored for now

        print('BG0a:','build_graph_v1 called:',self.input_x)        
        _h = tf.reshape(self.input_x, self._params.input_shape)
 
        dropFrac = self._params.dropFrac
        with tf.variable_scope('jan_CNNs') as _:

            kernel_sizes = self._params.conv_kernals
            filters = self._params.conv_filters
            strides = self._params.conv_strides
            
            
            down = []
            for i, (_ks, _fs, _st) in enumerate(zip(kernel_sizes, filters, strides)):
                _name = 'conv2d_%i'%i
                print('BLL add layer',i,_ks, _fs, _st,_name)
                #print('BLLT',[type(x) for x in [i,_ks, _fs, _st,_name]])
                _h = tf.layers.conv2d(_h, filters=_fs, kernel_size=_ks, strides=_st, name=_name, activation=tf.nn.relu, padding='same') #, **conv_defaults)
                if self._params.batch_norm:
                    channels_axis = 3
                    _h = tf.layers.batch_normalization(_h, axis=channels_axis, training=self._is_training)
                if dropFrac > 0:
                    _h = tf.layers.dropout(_h, rate=dropFrac, training=self._is_training)
                down.append(_h)

                print('Layer', _name, 'Output size:', _h.get_shape().as_list())

            down.pop() # Jan: not sure why I need it?
            
            # how to concatenate
            #if self._params.u_net and not is_last_layer:
            #        _h = tf.concat([down.pop(), _h], axis=channels_axis)
        with tf.variable_scope('jan_FC'):
            # reshape tensor into a batch of vectors
            dL=_h.get_shape().as_list() # dimensons of input tesnor from CNN
            h_pool2_flat = tf.reshape(_h, [-1, dL[1]*dL[2]*dL[3]] )
            # Densely connected layer with 1024 neurons.
            h_fc1 = tf.layers.dense(h_pool2_flat, self._params.dense_dim , activation=tf.nn.relu)
            if dropFrac > 0:
                h_fc1 = tf.layers.dropout( h_fc1, rate=dropFrac,training=self._is_training)

            # Compute logits (1 per class) and compute loss.
            logits = tf.layers.dense(h_fc1, self.N_DIGITS, activation=None)

            # set self.predictions to the network output
            print('Mym_BG1: pred=',logits.shape)
            digit=tf.argmax(logits, 1)
            probs= tf.nn.softmax(logits)

        self.predictions=dict(logits = logits, digit=digit,probs=probs)


    def build_graph_v0(self):  # all dims are hardcoded - no use of  self._params
        """ network """
        print('BG0:','build_grap called:',self.input_x)        
        input_x = tf.reshape(self.input_x, [-1, 28, 28, 1])
 
        # First conv layer will compute 32 features for each 5x5 patch
        with tf.variable_scope('conv_layer1'):
            h_conv1 = tf.layers.conv2d(input_x, filters=32, kernel_size=[5, 5], 
                               padding='same', activation=tf.nn.relu)
            h_pool1 = tf.layers.max_pooling2d(
                h_conv1, pool_size=2, strides=2, padding='same')

        # Second conv layer will compute 64 features for each 5x5 patch.
        with tf.variable_scope('conv_layer2'):
            h_conv2 = tf.layers.conv2d( h_pool1, filters=64, kernel_size=[5, 5],
                                        padding='same',  activation=tf.nn.relu)
            h_pool2 = tf.layers.max_pooling2d(
                h_conv2, pool_size=2, strides=2, padding='same')

            # reshape tensor into a batch of vectors
            h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])

        # Densely connected layer with 1024 neurons.
        h_fc1 = tf.layers.dense(h_pool2_flat, 1024, activation=tf.nn.relu)
        h_fc1 = tf.layers.dropout( h_fc1, rate=0.5,
                                   training=self._is_training)

        # Compute logits (1 per class) and compute loss.
        logits = tf.layers.dense(h_fc1, self.N_DIGITS, activation=None)

        # set self.predictions to the network output
        print('Mym_BG1: pred=',logits.shape)
        digit=tf.argmax(logits, 1)
        probs= tf.nn.softmax(logits)
        self.predictions=dict(logits = logits, digit=digit,probs=probs)

        
    def define_loss(self, labels):
        with tf.name_scope('loss-jan2'):
            self.loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=self.predictions['logits'])


    def define_optimizer(self):
        with tf.variable_scope('optimizer') as _:
            self.optimizer = tf.train.AdamOptimizer(self._params.learning_rate).minimize(self.loss, global_step=self.global_step)
