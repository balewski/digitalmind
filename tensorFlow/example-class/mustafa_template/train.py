#!/usr/bin/env python
__author__ = "Mustafa Mustafa"
__email__  = "mmustafa@lbl.gov"

import os, time
import tensorflow as tf
from models.my_model import MyModel
from data.data_pipeline import get_tfrecords_dataset_input_fn
from hparams.yparams import YParams

def model_fn(features, labels, params, mode):
    """ Build graph and return EstimatorSpec 
    Returns:
        (EstimatorSpec): Model to be run by Estimator.
    """
    is_training = (mode == tf.estimator.ModeKeys.TRAIN)

    model = MyModel(params, input_x=features, is_training=is_training)

    if mode is not tf.estimator.ModeKeys.PREDICT:
        # loss and optimizer are not needed for inference
        model.define_loss(labels)
        model.define_optimizer()
        

    print('MMF:',type(model.predictions), type(labels),mode)


    # . . . . .  alternative returns defined below .....  
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'predDig': model.predictions['digit'],
            'prob': model.predictions['probs']
        }
        return tf.estimator.EstimatorSpec(mode, predictions=predictions)
    
    # Compute evaluation metrics.
    accuracy = tf.metrics.accuracy(labels=labels,
                               predictions=model.predictions['digit'],
                               name='acc_op')    
    metricsD = {'acc1jan': accuracy} # name used by eval-data ?!
    tf.summary.scalar('acc1jan', accuracy[1]) # namje used by test-data ?!

    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(
            mode, loss=model.loss, eval_metric_ops=metricsD)

    #print('MMF2:',predictions)
    return tf.estimator.EstimatorSpec(
        predictions=model.predictions['logits'],
        loss=model.loss,
        train_op=model.optimizer,
        eval_metric_ops=metricsD,
        mode=mode)

#--------------------
def main(argv):
    """ Training loop """

    yaml_fn='hparams/my_model_configs.yaml'    
    hconfig_name='hset_mnist2'

    # load hyperparameters
    params = YParams(os.path.abspath(yaml_fn), hconfig_name)
    print('M: hparams ok:',params)

    # build estimator
    myEstimator = tf.estimator.Estimator(model_fn=model_fn,
                                       model_dir=params.experiment_dir,
                                       params=params)

    # create training data input pipeline
    train_input_fn = get_tfrecords_dataset_input_fn(params.train_data_files,
                                                    batchsize=params.batchsize,
                                                    epochs=params.epochs,
                                                    variable_scope='train_data_pipeline',
                                                    augment=params.augment_data)

    max_steps = (params.dataset_size//params.batchsize)*params.epochs
    print('M: train_input_fn  ok, max_step=',max_steps)

    train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn,
                                        max_steps=max_steps,
                                        hooks=[])
    print('M: train_spec  ok')
    # create validation data input pipeline
    valid_input_fn = get_tfrecords_dataset_input_fn(params.valid_data_files,
                                                    batchsize=params.batchsize,
                                                    epochs=params.epochs,
                                                    variable_scope='valid_data_pipeline',
                                                    augment=False)

    valid_spec = tf.estimator.EvalSpec(input_fn=valid_input_fn, hooks=[])

    
    print('M: valid_spec  ok, train ...')
    tf.logging.set_verbosity(tf.logging.INFO) # Just to have some logs to display for demonstration

    # train
    startT = time.time()

    tf.estimator.train_and_evaluate(myEstimator, train_spec, valid_spec)
    fitTime=time.time() - startT
    print('M: train_and_evaluate done, fit %.2f min\n'%(fitTime/60.))

    s = tf.summary
    #ok print('Ms d',s.__dict__)
    #bad print('Ms f:',s.tensor_summary())
    #bad print('Ms s:',s.get_summary_description())
    # bad print('Ms ds[',s['tensor_summary'].__dict__)

    print('M:make 1 prediction, on valid-data, no true labels:')
    test_input_fn=valid_input_fn
    j=0
    nBad=0
    for onePred in myEstimator.predict(test_input_fn):
        # do something with prediction
        if j<3: print('j=',j,onePred)
        predDig=onePred['predDig']
        
        break
        # true label is not passed by estimator.predict(.)
        trueDig=_prediction['trueDig'][0]
        
        if trueDig!=predDig : 
            nBad+=1
            if nBad <10: print('bad j=',j,trueDig,predDig)
        j+=1
        #if j> 5: break


    print('done, j=%d nBad=%d'%(j,nBad))
if __name__ == '__main__':
    tf.app.run()
