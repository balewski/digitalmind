#!/usr/bin/env python
import numpy as np
import tensorflow as tf

tfr_fname='data_tfrStr_mnist2/mnistBlk.0.tfrecords'
print('\n Open %s  and check the first entries'%tfr_fname)
for serialized_example in tf.python_io.tf_record_iterator(tfr_fname):
    example = tf.train.Example()
    myLen=example.ParseFromString(serialized_example)
    print('ee2:',myLen)

    x_1 = np.fromstring(example.features.feature['X'].bytes_list.value[0], dtype=np.float32)
    y_1 = np.fromstring(example.features.feature['Y'].bytes_list.value[0], dtype=np.int32)  
    break
    
# the numbers may be slightly different because of the floating point error.

print ('tfr X:',x_1[::30])
print ('tfr y:',y_1)


print('\n Open the saved file convert it into in-memory np.array')
j=0
dataXL=[]; dataYL=[]
for ser_item in tf.python_io.tf_record_iterator(tfr_fname):
    tftUtil = tf.train.Example()
    myLen=tftUtil.ParseFromString(ser_item)
    x1 = np.fromstring(tftUtil.features.feature['X'].bytes_list.value[0], dtype=np.float32)
    y1 = np.fromstring(tftUtil.features.feature['Y'].bytes_list.value[0], dtype=np.int32)
    dataXL.append(x1)
    dataYL.append(y1)
    j+=1
    
    #if j>10: break
    
dataX=np.array(dataXL)
dataY=np.array(dataYL)

print('dataX:',dataX.shape)
print('dataY:',dataY.shape)
