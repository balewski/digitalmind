#!/usr/bin/env python
__author__ = "Mustafa Mustafa"
__email__  = "mmustafa@lbl.gov"

import tensorflow as tf
import numpy as np
import os

from hparams.yparams import YParams
from train import model_fn
from data.data_pipeline import get_tfrecords_dataset_input_fn

def main(argv):
    
    yaml_fn='hparams/my_model_configs.yaml'
    config_name='hset_mustafa'
    config_name='hset_mnist2'

    # load hyperparameters
    params = YParams(os.path.abspath(yaml_fn), config_name)
    print('M: hparams ok:',params)


    # build estimator
    estimator = tf.estimator.Estimator(model_fn=model_fn,
                                       model_dir=params.experiment_dir,
                                       params=params)

    # load test data here:
    #test_data = np.load('path/to/test/data.npy')
    #test_data_input_fn = tf.estimator.inputs.numpy_input_fn(test_data, shuffle=False)
    # OR 
    # create test data input pipeline

    
    tfr_fname='data_tfrStr_mnist2/mnistBlk.5.tfrecords' # doe NOT work on list
    tfr_fname=params.valid_data_files[1]
    # note - wild cards are not accepted by 
    print('\n Open %s and  convert it into in-memory np.array'%tfr_fname)

    dataXL=[]; dataYL=[]
    for ser_item in tf.python_io.tf_record_iterator(tfr_fname):
        tftUtil = tf.train.Example()
        myLen=tftUtil.ParseFromString(ser_item)
        x1 = np.fromstring(tftUtil.features.feature['X'].bytes_list.value[0], dtype=np.float32)
        y1 = np.fromstring(tftUtil.features.feature['Y'].bytes_list.value[0], dtype=np.int32)
        dataXL.append(x1)
        dataYL.append(y1)
    dataX=np.array(dataXL)
    dataY=np.array(dataYL)

    print('got dataX:',dataX.shape,' dataY:',dataY.shape,' one rec len=',myLen)

    test_input_fn =tf.estimator.inputs.numpy_input_fn(
        x=dataX, num_epochs=1, shuffle=False)

    j=0; nBad=0
    for _prediction in estimator.predict(test_input_fn):
        digZ=_prediction['predDig']
        if j==0: print('j=',j,_prediction)
        digU=dataY[j]
        if j<5: print(j,'digZ=',digZ,digU)
        if digU!=digZ: nBad+=1
        j+=1


    acc=1-nBad/j
    print('n All=',j,' nBad=',nBad,' acc=%.3f'%acc)


if __name__ == '__main__':
    tf.app.run()


''' Alternative solution allowing estimator.predict(.) to pass labels
hideen inside features.

In data-pipeline:

 def parse_fn(example):
 ...
 return dict(X=X,Y=label), label

Model:
  def build_graph(self):
  input_x = tf.reshape(self.input_x['X'], [-1, 28, 28, 1])


model_fn:
can now return 
            'trueDig':features['Y']
inside
return tf.estimator.EstimatorSpec(mode, predictions=predictions)

        this hack w/ double passing of lables was suggested in the post below
        to allow access to lables also in estimator.predict(..) who would toss
        the actual lables away
        https://github.com/tensorflow/tensorflow/issues/17824
        An alternative solution with reloading checkpoint is discussed here 
        this one is to much work, does not match the style of estimator
        https://stackoverflow.com/questions/47349426/how-to-have-predictions-and-labels-returned-with-tf-estimator-either-with-predi
'''
