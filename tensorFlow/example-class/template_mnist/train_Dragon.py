#!/usr/bin/env python
""" 
Example for using TF estimator and pipeline
read input : tfrecord
train net
write net + weights as tensorboard
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Dragon import Plotter_Dragon
from Deep_Dragon import Deep_Dragon 

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train  TF on MNIST',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign',choices=['hset2a','hset2b'],
                         default='hset2a',
                         help=" hyper-param set, aka design of the model")

    parser.add_argument("--dataPath",
                        default='data',help="path to data vault")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("--project",
                        default='dragon',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-e", "--epochs", type=int, default=8,
                        help="fitting epoch")

    parser.add_argument("-b", "--batch_size", type=int, default=500,
                        help="fit batch_size")

    args = parser.parse_args()
    args.arrIdx=33 # not used
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

gra=Plotter_Dragon(args )
deep=Deep_Dragon(args,mode='train')
deep.assemble_model()
deep.assemble_data_pipes()

deep.train_and_evaluate()

deep.do_predictions(3) # num events

gra.ana_event_accumulator(deep.tfLogPath) # can be destructive?  Overwriting the graph with the newest event.

gra.plot_train_history(args)

gra.display_all(args,'train')
