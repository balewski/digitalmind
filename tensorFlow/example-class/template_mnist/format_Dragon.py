#!/usr/bin/env python
""" 
Example for creating TFrecords from np.array and tran/val/test lists
INPUT: none
Output: tfrecords & lists
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os
import numpy as np
import tensorflow as tf
import yaml

from IO_Dragon import dragon_np_to_tfrecord, dragon_tfrecord_to_np

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train  TF on MNIST',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("--dataPath",
                        default='data',help="path to data vault")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events to consider, use 0 for all")

    args = parser.parse_args()
    #args.dom_split_name=None
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================

args=get_parser()

# prepare data - I do not want to user tensorLayers for modeling
import tensorlayer as tl
TMP_DIR = '/tmp/jan123'
X1,Y1,X2,Y2,X3,Y3 = tl.files.load_mnist_dataset(shape=(-1,784),path=TMP_DIR)
print(type(X1))
X=np.vstack((X1,X2,X3)) # merge all 3 pieces
Y1=np.reshape(Y1,(-1,1))
Y2=np.reshape(Y2,(-1,1))
Y3=np.reshape(Y3,(-1,1))
Y=np.vstack((Y1,Y2,Y3))
#y_train=.astype(np.int32)
print('got X:',X.shape,X.dtype,' Y:',Y.shape, Y.dtype)

# 1-1. Saving a dataset with input and label (supervised learning)
tfrec_size=7000
num_tfrec=int(X.shape[0]/tfrec_size)

tfrecL=[]
print('will write %d TFRecords of %d to dir=%s'%(num_tfrec,tfrec_size,args.dataPath))
for ib in range(num_tfrec):
    kb1=ib*tfrec_size; kb2=kb1+tfrec_size    
    xRec=X[ kb1: kb2]
    yRec=Y[ kb1: kb2]
    tfr_filename =  args.dataPath+'/mnistRec.%d'%ib+'.tfrecords'
    tfrecL.append(tfr_filename)
    dragon_np_to_tfrecord(xRec, yRec,tfr_filename, verbose=True)
    #exit(1)

# - - - -   Assemble data summary , define domain split
itL=np.random.permutation(num_tfrec).astype(np.int32)
splitD={'train':itL[2:],'val':itL[0:1],'test':itL[1:2]}

outD={}
for dom in splitD:
    idxL=splitD[dom]
    fnmL=[ tfrecL[x] for x in idxL]        
    outD[dom]=fnmL
    outD[dom+'_size']=len(fnmL)*tfrec_size

splitD=outD
splitD['tfrecord_size']=tfrec_size
#Xsh=list(X.shape); Xsh[0]=-1
splitD['Xshape']=[-1,28,28,1]
splitD['Yshape']=[-1,10] # will be 1-hot encoded

#print('splitD',splitD)

ymlFn=args.dataPath+'/split_domain.yaml'
ymlFd = open(ymlFn, 'w')
yaml.dump(splitD, ymlFd)
ymlFd.close()
print('\ncreated: ',ymlFn)

# 1-2. Check if the data is stored correctly
ib=0
tfr_filename =tfrecL[ib]  
nrec=0
print('\nM: Cross-test for ',tfr_filename)
dataX,dataY=dragon_tfrecord_to_np(tfr_filename)

print('read in nrec=',dataY.shape[0], 'per tfrecord file')
    
# the numbers may be slightly different because of the floating point error.
absId=ib
print ('org X:',X[absId][::30])
print ('tfr X:',dataX[absId][::30])
print ('org Y:',Y[absId])
print ('tfr y:',dataY[absId])

