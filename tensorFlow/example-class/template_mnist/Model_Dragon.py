__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import tensorflow as tf

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class ModelDragon(object):
    def __init__(self, hparams, input_x, is_training):

        self.hpar = hparams
        self.is_training = is_training
        self.input_x = input_x
 
        with tf.variable_scope('training_counters', reuse=tf.AUTO_REUSE) as _:
            self.global_step = tf.train.get_or_create_global_step()

        self.build_graph()
        self.loss = None
        self.optimizer = None

    # . . . . . . . . . . . . 
    def build_graph(self): 
        
        print('build_graph shapes inp:',self.hpar.Xshape,' out:',self.hpar.Yshape)
        _h = tf.reshape(self.input_x, self.hpar.Xshape)
 
        dropFrac = self.hpar.dropFrac

        with tf.variable_scope('jan_CNNs') as _:
            kernel_sizes = self.hpar.conv_kernals
            filters = self.hpar.conv_filters
            strides = self.hpar.conv_strides
            padd='same'
            activ=tf.nn.relu
                        
            for i, (_kern, _filt, _strid) in enumerate(zip(kernel_sizes, filters, strides)):
                _name = 'conv_%i'%i
                #print('BLL add layer',i,_ks, _fs, _st,_name)
                _h = tf.layers.conv2d(_h, filters=_filt, kernel_size=_kern, strides=_strid, name=_name, activation=activ, padding=padd) 
                if self.hpar.batch_norm:
                    channels_axis = 3
                    _h = tf.layers.batch_normalization(_h, axis=channels_axis, training=self.is_training)
                if dropFrac > 0:
                    _h = tf.layers.dropout(_h, rate=dropFrac, training=self.is_training)
                print('Layer', _name, 'Output size:', _h.get_shape().as_list())
            
            # how to concatenate
            #if self.hpar.u_net and not is_last_layer:
            #        _h = tf.concat([down.pop(), _h], axis=channels_axis)

        dL=_h.get_shape().as_list() # dimensons of input tesnor from CNN
        dimFlat=1;
        for x in dL[1:]: dimFlat=dimFlat*x
        print('flatten',dL,' to ',dimFlat, type(dimFlat))

        with tf.variable_scope('jan_FCs'):
            # reshape tensor into a batch of vectors
            h_flat = tf.reshape(_h, [-1,dimFlat] )
            #h_flat=tf.contrib.layers.flatten(_h)
            h_fc1 = tf.layers.dense(h_flat, self.hpar.dense_dim , activation=tf.nn.relu,name='dense_big')
            if dropFrac > 0:
                h_fc1 = tf.layers.dropout( h_fc1, rate=dropFrac,training=self.is_training)

            # Compute logits (1 per class) and compute loss.
            N_DIGITS=self.hpar.Yshape[1]
            logits = tf.layers.dense(h_fc1, N_DIGITS, activation=None,name='dense_last')

            # set self.predictions to the network output
            print('Mym_BG1: pred=',logits.shape)
            digit=tf.argmax(logits, 1)
            probs= tf.nn.softmax(logits)

        self.predictions=dict(logits = logits, digit=digit,probs=probs)

    # . . . . . . . . . . . . . .        
    def define_loss(self, labels):
        with tf.name_scope('loss-jan2'):
            self.loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=self.predictions['logits'])

    # . . . . . . . . . . . . . . 
    def define_optimizer(self):
        with tf.variable_scope('optimizer') as _:
            self.optimizer = tf.train.AdamOptimizer(self.hpar.learning_rate).minimize(self.loss, global_step=self.global_step)


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 

def dragonModel_fn(features, labels, params, mode):
    """ Build graph and return EstimatorSpec 
    Returns:
        (EstimatorSpec): Model to be run by Estimator.
    """
    is_training = (mode == tf.estimator.ModeKeys.TRAIN)

    model = ModelDragon(params, input_x=features, is_training=is_training)

    if mode is not tf.estimator.ModeKeys.PREDICT:
        # loss and optimizer are not needed for inference
        model.define_loss(labels)
        model.define_optimizer()
        
    print('MMF:',type(model.predictions), type(labels),mode)


    # . . . . .  alternative returns defined below .....  
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'predDig': model.predictions['digit'],
            'prob': model.predictions['probs']
        }
        return tf.estimator.EstimatorSpec(mode, predictions=predictions)
    
    # Compute evaluation metrics.
    accuracy = tf.metrics.accuracy(labels=labels,
                               predictions=model.predictions['digit'],
                               name='acc_op')    
    print('HHH',accuracy)

    tf.summary.scalar('acc', accuracy[1]) # name used by train-data ?
    tf.summary.scalar('acc_val', accuracy[0]) # name used by valid-data ?
    metricsD = {'acc':accuracy} # name used plot valid-data ?!
    
    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(
            mode, loss=model.loss, eval_metric_ops=metricsD)

    #print('MMF2:',predictions)
    return tf.estimator.EstimatorSpec(
        predictions=model.predictions['logits'],
        loss=model.loss,
        train_op=model.optimizer,
        eval_metric_ops=metricsD,
        mode=mode)

