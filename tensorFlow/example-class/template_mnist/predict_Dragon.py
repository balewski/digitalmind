#!/usr/bin/env python
""" 
Example for using TF estimator and pipeline
read input : tfrecord + trained model
 make only predictions

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Deep_Dragon import Deep_Dragon 
from Plotter_Dragon import Plotter_Dragon 
from IO_Dragon import dragon_tfrecord_to_np
import tensorflow as tf

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train  TF on MNIST',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign',choices=['hset2a','hset2b'],
                         default='hset2a',
                         help=" hyper-param set, aka design of the model")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("--project",
                        default='dragon',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")


    args = parser.parse_args()
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

gra=Plotter_Dragon(args)
deep=Deep_Dragon(args,mode='predict')
deep.assemble_model()

dataX,dataY=dragon_tfrecord_to_np(deep.domSplitD['test'][0])

test_input_fn =tf.estimator.inputs.numpy_input_fn(x=dataX, num_epochs=1, 
                                                  shuffle=False)

j=0; nBad=0
for _prediction in deep.myEstimator.predict(test_input_fn):
    digZ=_prediction['predDig']
    if j==0: print('j=',j,_prediction)
    digU=dataY[j]
    if j<5: print(j,'digZ=',digZ,digU)
    if digU!=digZ: nBad+=1
    j+=1

    
acc=1-nBad/j
print('n All=',j,' nBad=',nBad,' acc=%.3f'%acc)
