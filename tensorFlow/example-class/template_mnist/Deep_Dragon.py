__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import os, time, yaml
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()

from pprint import pprint
import tensorflow as tf
from IO_Dragon import dragonInput_fn
from Model_Dragon import dragonModel_fn

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from ruamel.yaml import YAML
from tensorflow.contrib.training import HParams
import tensorflow as tf

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class YParams(HParams):
    """ Yaml file parser derivative of HParams """

    def __init__(self, yaml_fn, config_name):
        super(YParams, self).__init__()
        with open(yaml_fn) as yamlfile:
            for key, val in YAML().load(yamlfile)[config_name].items():
                print('hpar:',key, val)
                self.add_hparam(key, val)

""" Yaml file parser derivative of tensorflow.contrib.training.HParams """
""" Original code: https://hanxiao.github.io/2017/12/21/Use-HParams-and-YAML-to-Better-Manage-Hyperparameters-in-Tensorflow/ """


#............................
#............................
#............................
class Deep_Dragon(object):
    """Contains all operations specific for this project"""

    def __init__(self,args, mode=None):
        self.outPath=args.outPath
        self.dataPath=args.dataPath

        self.name=args.prjName+'_'+args.modelDesign
        
        self.verb=args.verb

        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(1)

        if mode=='format': return

        self.tfLogPath=args.outPath+'/'+args.modelDesign        
        # . . . load hyperparameters
        yaml_cnfg='hpar_'+args.prjName+'.yaml' 
        self.hparams=YParams(os.path.abspath(yaml_cnfg),args.modelDesign )

        #self.hparams.add_hparam('modelHook', self)
        #pprint(self.hparams)


        ymlFn=self.dataPath+'/split_domain.yaml'
        print(' ..... load domains split from ',ymlFn)
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd)
        ymlFd.close()
        self.domSplitD=bulk

        for key in ['Xshape','Yshape']:
            self.hparams.add_hparam(key, bulk[key])
        if mode=='train':
            self.epochs=args.epochs
            self.batch_size=args.batch_size
            

        print(self.__class__.__name__,'TF ver:', tf.__version__,', prj:',self.name)
        
#............................
    def assemble_model(self):
        # build estimator

        self.myEstimator = tf.estimator.Estimator(model_fn=dragonModel_fn,
             model_dir=self.tfLogPath,params=self.hparams)
        if self.verb : print('myEstimator created')

#............................
    def assemble_data_pipes(self):
        # read data list

        hpar=self.hparams  #tmp
        tf.logging.set_verbosity(tf.logging.INFO) 

        # create training data input pipeline
        train_input_fn =dragonInput_fn(self.domSplitD['train'],
                batchsize=self.batch_size,epochs=self.epochs,
                variable_scope='train_data_pipeline',augment=hpar.augment_flag,
                shuffle_buffer_size=hpar.shuffle_buffer_size,
                num_parallel_reads=hpar.num_parallel_reads,
                num_parallel_calls=hpar.num_parallel_calls,
                num_prefetch=hpar.num_prefetch)

        num_steps=int(self.domSplitD['train_size']*self.epochs/self.batch_size)
        if self.verb :  print('ASD: train_input_fn  , num_step=',num_steps)

        self.train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn,
                max_steps= num_steps,hooks=[])

        if self.verb :     print('TD: train_spec  ok')
        # create validation data input pipeline
        self.valid_input_fn = dragonInput_fn(self.domSplitD['val'],
                batchsize=self.batch_size,epochs=1,
                variable_scope='valid_data_pipeline', augment=False)

        self.valid_spec = tf.estimator.EvalSpec(input_fn=self.valid_input_fn, hooks=[],throttle_secs=self.hparams.validation_period)
    
        if self.verb :     print('TD: valid_spec  ok')

#............................
    def train_and_evaluate(self):
        print('TD:  train ...')
        tf.logging.set_verbosity(tf.logging.INFO) # Just to have some logs to display for demonstration

        # train
        startT = time.time()

        tf.estimator.train_and_evaluate(self.myEstimator, self.train_spec, self.valid_spec)
        fitTime=time.time() - startT
        print('TD: train_and_evaluate done, fit %.2f min\n'%(fitTime/60.))

#............................
    def do_predictions(self,m):
        print('M:make %s prediction(s), on valid-data, no true labels avaliable:'%m)
        test_input_fn=self.valid_input_fn #tmp
        j=0
        for onePred in self.myEstimator.predict(test_input_fn):
            # do something with prediction
            predDig=onePred['predDig']
            if j<m : print('j=',j,onePred)
            j+=1
         
        print('TD:  predict done, nInp=',j)



