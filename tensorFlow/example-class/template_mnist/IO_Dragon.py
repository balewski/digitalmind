__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import tensorflow as tf
import numpy as np

# pylint: disable-msg=C0103
# pylint: disable=too-many-instance-attributes

''' 
this template was oryginaly assembled by Mustafa,
__author__ = "Mustafa Mustafa"
__email__  = "mmustafa@lbl.gov"
Jan B. has adopted it further.
'''

Yname='X'
Xname='Y'

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def dragon_tfrecord_to_np(tfr_fname):
    # note - wild cards are not accepted by 
    print('\n Open %s and  convert it into in-memory np.array'%tfr_fname)

    dataXL=[]; dataYL=[]
    for ser_item in tf.python_io.tf_record_iterator(tfr_fname):
        tftUtil = tf.train.Example()
        myLen=tftUtil.ParseFromString(ser_item)
        x1 = np.fromstring(tftUtil.features.feature[Xname].bytes_list.value[0], dtype=np.float32)
        y1 = np.fromstring(tftUtil.features.feature[Yname].bytes_list.value[0], dtype=np.int32)
        dataXL.append(x1)
        dataYL.append(y1)
    dataX=np.array(dataXL)
    dataY=np.array(dataYL)

    print('dragonInput2nparray: got dataX:',dataX.shape,' dataY:',dataY.shape,' one rec len/kB=%.2',myLen/1014)
    return dataX,dataY

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def dragon_np_to_tfrecord(X, Y, result_tf_file, verbose=True):
    print('inp', type(X), type(Y),' outFN=',result_tf_file)
    writer = tf.python_io.TFRecordWriter(result_tf_file)

    def _bytes_feature(value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    for idx in range(X.shape[0]):
        x = X[idx]
        y = Y[idx]
        
        features = {}
        features[Xname] = _bytes_feature(x.tostring())
        features[Yname] = _bytes_feature(y.tostring())

        example = tf.train.Example(features=tf.train.Features(feature=features))
        serialized = example.SerializeToString()
        writer.write(serialized)
    writer.close()


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def dragonInput_fn(filenames, batchsize, epochs, variable_scope, augment=False,
                   shuffle_buffer_size=4, num_parallel_reads=4,
                   num_parallel_calls=4, num_prefetch=4):
 
    """ creates a tf.data.Dataset and feeds and augments data from tfrecords
    Returns:
        data input function input_fn
    """
    # . . . . . . . . . . . . . .                
    def parse_fn(example):
        "Parse TFExample records and perform data augmentation"
        example_fmt = {Xname: tf.FixedLenFeature([], tf.string, ""),
                       Yname: tf.FixedLenFeature([], tf.string, "")}

        parsed_record = tf.parse_single_example(example, example_fmt)
         
        X = tf.decode_raw(parsed_record[Xname], tf.float32)
        label = tf.decode_raw(parsed_record[Yname], tf.int32)        
        print('TFR X.shape:',X.get_shape(),' Y.shape',label.shape)

        # generic augmentation for images
        if augment is True:
            print("Augmenting data ...")
            # rotation
            rot_k = np.random.choice([0, 1, 2, 3])
            if rot_k:
                X = tf.image.rot90(X, k=rot_k)
        else:
            print("Not augmenting data...")
        
        return X, label
 
    # . . . . . . . . . . . . . .        
    def input_fn():
        """ create input_fn for Estimator training
        Returns: tf.Tensors of features and labels
        """
        with tf.variable_scope(variable_scope) as _:
            fnameList=tf.data.Dataset.list_files(filenames, shuffle=False)
            dataset = tf.data.TFRecordDataset(fnameList,compression_type=None,
                         buffer_size=2*shuffle_buffer_size,
                         num_parallel_reads=num_parallel_reads)
            dataset = dataset.shuffle(shuffle_buffer_size)
            dataset = dataset.repeat(epochs)
            dataset = dataset.map(map_func=parse_fn, 
                                  num_parallel_calls=num_parallel_calls)
            dataset = dataset.prefetch(num_prefetch)
            dataset = dataset.batch(batchsize)

            data_it = dataset.make_one_shot_iterator()
            X, Y = data_it.get_next()
            print('RRR1',X,Y)
        return X, Y

    return input_fn


