import os
import numpy as np
from matplotlib import cm as cmap

import socket  # for hostname
from tensorflow.contrib.keras.api.keras.utils import plot_model


import time

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_Dragon(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args):
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt

        print(self.__class__.__name__,':','Graphics started')
 
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        self.maxU=1.3

    #............................
    def display_all(self,args,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()                
                figName='%s/idx%d_%s_%s_f%d'%(self.outPath,args.arrIdx,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()

      
#............................
    def plot_train_history(self,args,figId=10):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,6))
        prjName=args.prjName+'_'+args.modelDesign
        nrow,ncol=2,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2 )
        ax2 = self.plt.subplot2grid((nrow,ncol), (1,1), colspan=2 ) 
        ax3 = self.plt.subplot(nrow, ncol, 1)
       
        DL=self.train_hist
        STP=self.train_hist['step']
        endLoss=DL['loss'][-1]
        endValAcc=DL['acc_val'][-1]
        tit1='%s, train, end-loss=%.3g'%(prjName,endLoss)
        tit2='%s, train, end-val-acc=%.3g'%(prjName,endValAcc)
        ax1.set(ylabel='loss ',title=tit1,xlabel='steps') 
        ax2.set(ylabel='accuracy ',title=tit2,xlabel='steps') 
        
        ax1.plot(STP,DL['loss'],'.-.',label='loss' )
        ax2.plot(STP,DL['acc'],'.--',label='acc' )
        ax2.plot(STP,DL['acc_val'],'.--',label='acc_val' )
        ax1.set(ylabel='accuracy')
        
        ax1.legend(loc='best')
        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')

        ax2.legend(loc='best')
        #ax2.set_yscale('log')
        ax2.grid(color='brown', linestyle='--',which='both')        


        # histogram of timing
        rateL=self.train_hist['rate'][1:] # skip 1st data point
        ax3.hist(rateL,bins=30)

        zmu=np.array(rateL)
        resM=zmu.mean()
        resS=zmu.std()
        ax3.set(title='avr %.1f+/-%.1f steps/sec'%(resM,resS), xlabel='global_steps/sec')



#............................
    def ana_event_accumulator(self,path):
        from tensorboard.backend.event_processing import event_accumulator        
        print('\n Open event_accumulator in %s '%path)

        size_guide={
            event_accumulator.COMPRESSED_HISTOGRAMS: 500,
            event_accumulator.IMAGES: 4,
            event_accumulator.AUDIO: 4,
            event_accumulator.SCALARS: 0,
            event_accumulator.HISTOGRAMS: 1,}
        ea = event_accumulator.EventAccumulator(path ,size_guidance=size_guide)

        ea.Reload()
        tagsD=ea.Tags()
        print('my Tags:', type(tagsD))
        print(tagsD)

        recLoss=ea.Scalars('loss')
        recAcc0=ea.Scalars('acc')
        recAcc1=ea.Scalars('acc_val')
        recRate=ea.Scalars('global_step/sec')
        #print('rr',recRate)
        #rr1=recRate[0] #ScalarEvent(wall_time=156.572, step=101, value=8.90277)
        #print(rr1)
        
        hist={'step':[], 'loss':[],'acc':[],'acc_val':[],'rate':[]}
        for x,x1,x2,x3 in zip(recAcc0,recAcc1,recLoss,recRate): 
            t,s,v=x
            _,_,v1=x1
            _,_,v2=x2
            #print(type(x3))
            v3=x3.value
            hist['step'].append(s)
            hist['loss'].append(v2)
            hist['acc'].append(v)
            hist['acc_val'].append(v1)
            hist['rate'].append(v3)
            #print(t,s,v,v1,v2,v3)
        self.train_hist=hist
