#!/usr/bin/env python
""" 
Example:  MNIST data  no training, 
reads model
generic TF w/ raw tensors

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Deep_MNIST import Deep_MNIST_RAW

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='predict MNIST from saved model',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--project",
                        default='mnist_cnn',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath",
                        default='data/',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=500,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xDir in [args.dataPath, args.outPath]:
        if not os.path.exists(xDir):
            print('Aborting on start, missing  dir:',xDir)
            exit(22)
    return args

# - - - - - - - - -   actual  code - - - - - - - - - 
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.INFO)

print('libs imported, TF.ver=%s elaT=%.1f sec'%(tf.__version__,(time.time() - startT0)))


#-------------------
def plot_images(X,Udig,Zdig,figId=1):
    fig=plt.figure(figId,facecolor='white', figsize=(10,2.5))
    print('plot images',type(X))
    nrow,ncol=1,4
    mm=min(nrow*ncol,len(X))
    for i in range(mm):
        ax=plt.subplot( nrow,ncol,i+1)
        x=np.reshape(X[i],(-1,28)) # un-flatten images to 2D
        ax.imshow(x, cmap=plt.get_cmap('gray'))
 
        ax.set(title='truth=%d   pred=%d'%(Udig[i],Zdig[i]))
    # show the plot in main


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_MNIST_RAW(args)

deep.make_tensors()
deep.build_model()

if args.noXterm:
    print('diasable Xterm')
    import matplotlib as mpl
    mpl.use('Agg')  # to plot w/ X-server
import matplotlib.pyplot as plt

print(' Import MNIST data ...')
from tensorflow.examples.tutorials.mnist import input_data
mnistDS = input_data.read_data_sets(args.dataPath, one_hot=True)


print(' Initialize TF variables ')
init = tf.global_variables_initializer()

# 'Saver' op to save and restore all the variables
saver = tf.train.Saver()


# Running a new session
print("\nStarting 2nd session...")
with tf.Session() as sess:
    # Initialize variables
    sess.run(init)

    # Restore model weights from previously saved model
    saver.restore(sess,  args.outPath)
    print("Model restored from file: %s" %args.outPath)
    
    numTest=args.events
    imageL=mnistDS.test.images[:numTest]
    Zhot=deep.predict(sess, imageL)
    Uhot= mnistDS.test.labels[:numTest] #truth
    
    Zdig=np.argmax(Zhot,axis=-1)
    Udig=np.argmax(Uhot,axis=-1)
    Mlist=Udig==Zdig  # good predictions are set to True
    nGood=Mlist.sum()

    j=0
    badIdL=[]
    for u,z,ok in zip(Udig,Zdig,Mlist):
        if ok : 
            c=''
        else:
            c='bad'
            badIdL.append(j)
        if j<20: print(j,u,z,c)
        j+=1
        if len(badIdL) >10: break
        
    print("Accuracy=%.4f  based on %d images\n"%(nGood/Mlist.shape[0],numTest))
    print('indices of bad images:',badIdL)
    print('Udig(bad):',Udig[badIdL])

    plot_images(imageL[badIdL],Udig[badIdL],Zdig[badIdL])
    plt.show()

