#!/usr/bin/env python

""" 
Example:  MNIST data  CNN, saved model
generic TF w/ raw tensors
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import argparse, time
import numpy as np
import tensorflow as tf
from Deep_MNIST import Deep_MNIST_RAW

def get_parser():
    parser = argparse.ArgumentParser(
        description='train on  MNIST  using CNN',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath",
                        default='data/',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument("--project",
                        default='mnist_cnn',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-s", "--steps", type=int, default=80,
                        help="steps of batches")
    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="batch_size for training")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "--noXterm", dest='noXterm', action='store_true',
                         default=False,help="disable X-term for batch mode")

    parser.add_argument("--nCpu", type=int, default=8,
                        help="num CPUs used when fitting, use 0 for all resources")

    args = parser.parse_args()
    args.report_step=10 # not worth the input capability
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
  
    return args


#-------------------
def plot_images(X,Y,figId=1):
    fig=plt.figure(figId,facecolor='white', figsize=(10,2.5))
    print('plot images',type(X))
    nrow,ncol=1,4
    for i in range(nrow*ncol):
        ax=plt.subplot( nrow,ncol,i+1)
        x=np.reshape(X[i],(-1,28)) # un-flatten images to 2D
        ax.imshow(x, cmap=plt.get_cmap('gray'))
        dig=np.argmax(Y[i])
        ax.set(title='digit=%d'%dig)
    # show the plot in main


#----------------------------------
def plot_fitHir(DL,tit='fit summary',figId=2):
    # Two subplots, unpack the axes array immediately
    f, (ax1, ax2) = plt.subplots(figId,  sharex=True,
                                 facecolor='white', figsize=(8,5))

    stepL=DL['step']
    ax1.set(ylabel='loss',title=tit)
    ax1.plot(stepL,DL['loss'],'.-',label='train')
 
    ax1.legend(loc='best')
    ax1.set_yscale('log')
    ax1.grid(color='brown', linestyle='--',which='both')

    ax2.set(xlabel='steps',ylabel='accuracy')
    ax2.plot(stepL,DL['acc'],'.-',label='train')
    
    ax2.legend(loc='upper left')
    ax2.grid(color='brown', linestyle='--',which='both')
 
    f.subplots_adjust(hspace=0.05)
    figName=args.outPath+'/jan_fit%d'%figId
 
    plt.savefig(figName+'.pdf')
    # show the plot in main


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_MNIST_RAW(args)
deep.make_tensors()
deep.build_model()

if args.noXterm:
    print('diasable Xterm')
    import matplotlib as mpl
    mpl.use('Agg')  # to plot w/ X-server
import matplotlib.pyplot as plt


# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnistDS = input_data.read_data_sets(args.dataPath, one_hot=True)
plot_images( mnistDS.test.images, mnistDS.test.labels)


# CPUs are used via a "device" which is just a threadpool
if args.nCpu>0:
    tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
    print('M: restrict CPU count to ',args.nCpu)

print(' Initialize TF variables ')
init = tf.global_variables_initializer()

# 'Saver' op to save and restore all the variables
saver = tf.train.Saver()

print(' Start training ...', type(mnistDS))
with tf.Session() as sess:
    
    # Run the initializer
    sess.run(init)

    startT = time.time()
    hir=deep.train_model(sess,mnistDS,args)
    fitTime=time.time() - startT

    # Save model weights to disk
    save_path = saver.save(sess, args.outPath)
    print("Model saved in file: %s" % save_path)

    # Calculate accuracy for 256 MNIST test images
    numTest=1000
    acc=deep.avr_accuracy(sess,mnistDS,numTest)
    print("Testing Accuracy for numTest=%d:"%numTest, acc)

plot_fitHir(hir,tit='mnist, fit %.1f min, nCpu=%d, drop=%.1f'%(fitTime/60.,args.nCpu,args.dropFrac))
plt.tight_layout()

plt.show()
