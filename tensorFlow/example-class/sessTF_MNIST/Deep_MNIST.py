import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings
startT0 = time.time()
import tensorflow as tf
print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


#............................
#............................
#............................
class Deep_MNIST_RAW(object):
    """Contains all operations specific for this project"""

    def __init__(self,args):
        self.outPath=args.outPath
        self.dataPath=args.dataPath
        self.name=args.prjName
        self.verb=args.verb
        #self.data={} # domains:train/val/test or segments:0/1/.../K

        # Network Parameters
        self.num_input = 784 # MNIST data input (img shape: 28*28)
        self.num_classes = 10 # MNIST total classes (0-9 digits)
        self.learning_rate = 0.001
        

        self.tensor={} 

        print(self.__class__.__name__,'TF ver:', tf.__version__,', prj:',self.name)
        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            add_missing_dir999



    #----------------------------------
    # Create some wrappers for simplicity
    def conv2d(self,x, W, b, strides=1):
        # Conv2D wrapper, with bias and relu activation
        x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
        x = tf.nn.bias_add(x, b)
        x = tf.nn.relu(x)
        print('my_conv2d out x:',x.name,x.shape,'; W:',W.name,W.shape,'; b:',b.name,b.shape)
        return x

    #----------------------------------
    def maxpool2d(self,x, k=2):
        # MaxPool2D wrapper
        return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')

    #----------------------------------
    # Create variables
    def make_tensors(self):

        # tf Graph tensors
        self.tensor['X'] = tf.placeholder(tf.float32, [None, self.num_input])
        self.tensor['Y'] = tf.placeholder(tf.float32, [None, self.num_classes])
        self.tensor['keepFrac'] = tf.placeholder(tf.float32) # 1-dropout

        # Store layers weight & bias
        self.tensor['weights'] = {
            # 5x5 conv, 1 input, 32 outputs
            'wc1': tf.Variable(tf.random_normal([5, 5, 1, 32])),
            # 5x5 conv, 32 inputs, 64 outputs
            'wc2': tf.Variable(tf.random_normal([5, 5, 32, 64])),
            # fully connected, 7*7*64 inputs, 1024 outputs
            'wd1': tf.Variable(tf.random_normal([7*7*64, 1024])),
            # 1024 inputs, 10 outputs (class prediction)
            'out': tf.Variable(tf.random_normal([1024, self.num_classes]))
        }

        self.tensor['biases'] = {
            'bc1': tf.Variable(tf.random_normal([32])),
            'bc2': tf.Variable(tf.random_normal([64])),
            'bd1': tf.Variable(tf.random_normal([1024])),
            'out': tf.Variable(tf.random_normal([self.num_classes]))
        }
        print('make_weights  done')
  

    #----------------------------------
    # Create model
    def build_model(self):
        x=self.tensor['X']
        weights=self.tensor['weights']
        biases=self.tensor['biases']
        Y=self.tensor['Y']
        keepFrac=self.tensor['keepFrac']

        # MNIST data input is a 1-D vector of 784 features (28*28 pixels)
        # Reshape to match picture format [Height x Width x Channel]
        # Tensor input become 4-D: [Batch Size, Height, Width, Channel]
        x = tf.reshape(x, shape=[-1, 28, 28, 1])
        
        # Convolution Layer
        conv1 = self.conv2d(x, weights['wc1'], biases['bc1'])
        # Max Pooling (down-sampling)
        conv1 = self.maxpool2d(conv1, k=2)
        
        # Convolution Layer
        conv2 = self.conv2d(conv1, weights['wc2'], biases['bc2'])
        # Max Pooling (down-sampling)
        conv2 = self.maxpool2d(conv2, k=2)

        # Fully connected layer
        # Reshape conv2 output to fit fully connected layer input
        fc1 = tf.reshape(conv2, [-1, weights['wd1'].get_shape().as_list()[0]])
        fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
        fc1 = tf.nn.relu(fc1)
        # Apply Dropout
        fc1 = tf.nn.dropout(fc1, keepFrac)

        # Output, class prediction
        logits = tf.add(tf.matmul(fc1, weights['out']), biases['out'])
        
        prediction = tf.nn.softmax(logits)
        self.tensor['prediction']=prediction

        # Define loss and optimizer
        loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits( logits=logits, labels=Y))
        self.tensor['loss']=loss_op
        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        train_op = optimizer.minimize(loss_op)
        self.tensor['optimizer']=train_op

        # Evaluation step  
        correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_pred, "float"))
        #accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
        self.tensor['accuracy']=accuracy


    #---------------------------------- 
    def train_model(self,sess,mnistDS,args): # train model &  preserve history
        keepFrac=1-args.dropFrac
        hir={'loss':[],'acc':[],'step':[]}
        loss_op=self.tensor['loss']
        accuracy=self.tensor['accuracy']

        for step in range(args.steps):
            batch_x, batch_y = mnistDS.train.next_batch(args.batch_size)
            # Run optimization op (backprop)
            feed_dict={self.tensor['X']: batch_x, self.tensor['Y']: batch_y, 
                        self.tensor['keepFrac']: keepFrac}
            sess.run(self.tensor['optimizer'], feed_dict=feed_dict)
            if step % args.report_step ==  0:
                # Calculate batch loss and accuracy
                feed_dict[self.tensor['keepFrac']]=1.0
                loss, acc = sess.run([loss_op, accuracy], feed_dict=feed_dict)
                hir['step'].append(step) # archive history for plotting
                hir['loss'].append(loss)
                hir['acc'].append(acc)

                print("Step " + str(step) + ", Minibatch Loss= " + \
                      "{:.4f}".format(loss) + ", Training Accuracy= " + \
                      "{:.3f}".format(acc))

        print("Optimization Finished!")
        return hir
   
    #----------------------------------
    def predict(self,sess,data):
        # disable dropout
        feed_dict={self.tensor['X']: data, self.tensor['keepFrac']: 1.0}
        Z=sess.run(self.tensor['prediction'],feed_dict=feed_dict )
        return Z

    #----------------------------------
    def avr_accuracy(self,sess,mnistDS,numTest):
        # disable dropout
        feed_dict={self.tensor['X']: mnistDS.test.images[:numTest], 
                   self.tensor['Y']: mnistDS.test.labels[:numTest], 
                   self.tensor['keepFrac']: 1.0}
        acc=sess.run(self.tensor['accuracy'],feed_dict=feed_dict )
        return acc
