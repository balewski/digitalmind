#!/usr/bin/env python

""" 
generic TF w/ layers, train & save model

Example:  MNIST data set w/ CNN
Based on : https://www.tensorflow.org/tutorials/layers

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Recognize MNIST  using CNN',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath",
                        default='/junk/mnist_data2/',help="path to input/output")
    parser.add_argument("--outPath",
                        default='/junk/mnist_out2/',help="output path for plots and tables")

    parser.add_argument("--project",
                        default='mnist_cnn',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="fitting epoch")
    parser.add_argument("-b", "--batch_size", type=int, default=500,
                        help="batch_size for training")
    parser.add_argument("-n", "--events", type=int, default=5000,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true',
                         default=False,help="disable X-term for batch mode")
    parser.add_argument( "--noPlot", dest='noPlot', action='store_true',
                         default=False,help="disable any plotting, no matplotlib")

    parser.add_argument("--nCpu", type=int, default=8,
                        help="num CPUs used when fitting, use 0 for all resources")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xDir in [args.dataPath, args.outPath]:
        if not os.path.exists(xDir):
            print('Aborting on start, missing  dir:',xDir)
            exit(22)

    
    return args

# - - - - - - - - -   actual  code - - - - - - - - - 
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np
import tensorflow as tf
import tensorlayer as tl

tf.logging.set_verbosity(tf.logging.INFO)
tl.logging.set_verbosity(tl.logging.DEBUG)

print('libs imported, TF.ver=%s elaT=%.1f sec'%(tf.__version__,(time.time() - start)))


#-------------------
def plot_images(X,Y):
    print('plot images',type(X))
    for i in range(4):
        ax=plt.subplot(2,2,i+1)
        x=np.reshape(X[i],(-1,28)) # un-flatten images to 2D
        ax.imshow(x, cmap=plt.get_cmap('gray'))
        ax.set(title='digit=%d'%Y[i])
    # show the plot
    plt.tight_layout()
    plt.show()

#----------------------------------
def plot_fitHir(DL,tit='fit summary'):
    # LD -->DL
    #DL=dict(zip(LD[0],zip(*[d.values() for d in LD])))
    #print(DL)

    # Two subplots, unpack the axes array immediately
    f, (ax1, ax2) = plt.subplots(2,  sharex=True,
                                 facecolor='white', figsize=(8,6))

    ax1.set(ylabel='loss',title=tit)
    ax1.plot(DL['loss'],'.-',label='train')
    ax1.plot(DL['val_loss'],'.-',label='valid')
    ax1.legend(loc='best')
    #ax1.set_yscale('log')
    ax1.grid(color='brown', linestyle='--',which='both')

    ax2.set(xlabel='epochs',ylabel='accuracy')
    ax2.plot(DL['acc'],'.-',label='train')
    ax2.plot(DL['val_acc'],'.-',label='valid')
    ax2.legend(loc='upper left')
    ax2.grid(color='brown', linestyle='--',which='both')
 
    f.subplots_adjust(hspace=0.05)
    figName='jan_fit'
 
    plt.savefig(figName+'.pdf')

    plt.show()


#-------------------
def build_model(x,y_):
    fc_dim=15
    
    # - - - Assembling model
    # define the network
    network = tl.layers.InputLayer(x, name='input')
    network = tl.layers.DropoutLayer(network, keep=0.8, name='drop1')
    network = tl.layers.DenseLayer(network, fc_dim, tf.nn.relu, name='relu1')
    network = tl.layers.DropoutLayer(network, keep=0.5, name='drop2')
    network = tl.layers.DenseLayer(network, fc_dim, tf.nn.relu, name='relu2')
    network = tl.layers.DropoutLayer(network, keep=0.5, name='drop3')
    # the softmax is implemented internally in tl.cost.cross_entropy(y, y_) to
    # speed up computation, so we use identity here.
    # see tf.nn.sparse_softmax_cross_entropy_with_logits()
    network = tl.layers.DenseLayer(network, n_units=10, act=None, name='output')
    
    # define cost function and metric.
    y = network.outputs
    cost = tl.cost.cross_entropy(y, y_, name='cost')
    correct_prediction = tf.equal(tf.argmax(y, 1), y_)
    acc = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    y_op = tf.argmax(tf.nn.softmax(y), 1)

    # define the optimizer
    train_params = network.all_params
    train_op = tf.train.AdamOptimizer(learning_rate=0.0001).minimize(cost, var_list=train_params)

    return network,train_op,cost,acc


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

if not args.noPlot:
    if args.noXterm:
        print('diasable Xterm')
        import matplotlib as mpl
        mpl.use('Agg')  # to plot w/ X-server
    import matplotlib.pyplot as plt
else:
    print('disabled any plotting')

# CPUs are used via a "device" which is just a threadpool
if args.nCpu>0:
    tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
    print('M: restrict CPU count to ',args.nCpu)


# start interactive session only to allow printig of tensor values using eval()
sess = tf.InteractiveSession()

# The data, shuffled?? and split between train and test sets.
# prepare data
# http://tensorlayer.readthedocs.io/en/latest/modules/files.html
X, Y, X_val, Y_val, X_test, Y_test = tl.files.load_mnist_dataset(shape=(-1, 784),path=args.dataPath)
# define placeholder
pX = tf.placeholder(tf.float32, shape=[None, 784], name='x')
pY = tf.placeholder(tf.int64, shape=[None], name='y_')

if  args.events>0 :
    neve=args.events
    if neve < X.shape[0]:  
        X=X[:neve]; Y=Y[:neve]
        X_val=X_val[:neve]; Y_val=Y_val[:neve]        
        X_test=X_test[:neve]; Y_test=Y_test[:neve]        
        print('shrink input to ',neve,' images')
print('got input train shape X:',X.shape,type(X),' dig Y:',Y.shape,type(Y),' val dig Y:',Y_val.shape)


#plot_images(X,Y) # plot 4 images as gray scale, stop the code here

print('\nM: ...... Create the model .....')
model,train_op,cost,acc=build_model(pX,pY)

# initialize all variables in the session
tl.layers.initialize_global_variables(sess) #depreciated
#tf.global_variables_initializer() # fails
# print network information
model.print_params()
model.print_layers()

print('\nM: ....... train the network  ....')
tl.utils.fit(sess, model, train_op, cost, X, Y, pX, pY, 
             acc=acc, batch_size=args.batch_size, n_epoch=args.epochs, print_freq=10,
             X_val=X_val, y_val=Y_val, eval_train=False)


print('\nM: ....... prediction.....')
tl.utils.test(sess, model, acc, X_test, Y_test, pX, pY,
              batch_size=None, cost=cost)

outMF=args.outPath+'model1.npz'
print('\nM: ....... save the network to .npz file ...',outMF)
tl.files.save_npz(model.all_params, name=outMF)
sess.close()
