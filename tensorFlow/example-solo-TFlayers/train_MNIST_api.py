#!/usr/bin/env python

""" 
generic TF w/ API 

Example:  MNIST data set w/ CNN
Based on : https://www.tensorflow.org/tutorials/layers

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Recognize MNIST  using CNN',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--project",
                        default='mnist_cnn',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-e", "--epochs", type=int, default=0,
                        help="fitting epoch")
    parser.add_argument("-b", "--batch_size", type=int, default=20,
                        help="batch_size for training")
    parser.add_argument("-n", "--events", type=int, default=5000,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "--noXterm", dest='noXterm', action='store_true',
                         default=False,help="disable X-term for batch mode")
    parser.add_argument( "--noPlot", dest='noPlot', action='store_true',
                         default=False,help="disable any plotting, no matplotlib")

    parser.add_argument("--nCpu", type=int, default=8,
                        help="num CPUs used when fitting, use 0 for all resources")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
 
    return args

# - - - - - - - - -   actual  code - - - - - - - - - 
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.INFO)

print('libs imported, TF.ver=%s elaT=%.1f sec'%(tf.__version__,(time.time() - start)))


#-------------------
def plot_images(X,Y):
    print('plot images',type(X))
    for i in range(4):
        ax=plt.subplot(2,2,i+1)
        x=np.reshape(X[i],(-1,28)) # un-flatten images to 2D
        ax.imshow(x, cmap=plt.get_cmap('gray'))
        ax.set(title='digit=%d'%Y[i])
    # show the plot
    plt.tight_layout()
    plt.show()

#----------------------------------
def plot_fitHir(DL,tit='fit summary'):
    # LD -->DL
    #DL=dict(zip(LD[0],zip(*[d.values() for d in LD])))
    #print(DL)

    # Two subplots, unpack the axes array immediately
    f, (ax1, ax2) = plt.subplots(2,  sharex=True,
                                 facecolor='white', figsize=(8,6))

    ax1.set(ylabel='loss',title=tit)
    ax1.plot(DL['loss'],'.-',label='train')
    ax1.plot(DL['val_loss'],'.-',label='valid')
    ax1.legend(loc='best')
    #ax1.set_yscale('log')
    ax1.grid(color='brown', linestyle='--',which='both')

    ax2.set(xlabel='epochs',ylabel='accuracy')
    ax2.plot(DL['acc'],'.-',label='train')
    ax2.plot(DL['val_acc'],'.-',label='valid')
    ax2.legend(loc='upper left')
    ax2.grid(color='brown', linestyle='--',which='both')
 
    f.subplots_adjust(hspace=0.05)
    figName='jan_fit'
 
    plt.savefig(figName+'.pdf')

    plt.show()


#==================================
# cnn_model_fn function, which conforms to the interface expected
# by TensorFlow's Estimator API 
'''
   features, # This is batch_features from input_fn
   labels,   # This is batch_labels from input_fn
   mode,     # An instance of tf.estimator.ModeKeys
   params):  # Additional configuration
the three different modes are:PREDICT, TRAIN, EVAL
'''

def my_model_fn(features, labels, mode, params):
  print("Model function for CNN start, mode:",mode,', shapes:',features["x"].shape)
  if mode == tf.estimator.ModeKeys.TRAIN:  print('Model see params',params, labels.shape)

  # Input Layer
  input_layer = tf.reshape(features["x"], [-1, 28, 28, 1])

  # Convolutional Layer #1
  conv1 = tf.layers.conv2d(  inputs=input_layer, filters=32,
      kernel_size=[5, 5], padding="same", activation=tf.nn.relu)

  # Pooling Layer #1
  pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

  # Convolutional Layer #2 and Pooling Layer #2
  conv2 = tf.layers.conv2d( inputs=pool1, filters=64,
      kernel_size=[5, 5], padding="same", activation=tf.nn.relu)
  pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

  # Dense Layer
  pool2_flat = tf.reshape(pool2, [-1, 7 * 7 * 64])
  dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)
  dropout = tf.layers.dropout(
      inputs=dense, rate=params['dropFrac'], training=mode == tf.estimator.ModeKeys.TRAIN)

  # Logits Layer
  logits = tf.layers.dense(inputs=dropout, units=10)

  # Let's convert these raw values into two different formats that our model function can return
  predictions = {# Generate predictions (for PREDICT and EVAL mode)      
      "class_id": tf.argmax(input=logits, axis=1, name="dig09"),
      # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
      # `logging_hook`.
      "probabilities": tf.nn.softmax(logits, name="softmax_tensor") # dict:name pair is given for logging
  }

  if mode == tf.estimator.ModeKeys.PREDICT:
    return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

  # Calculate Loss (for both TRAIN and EVAL modes)
  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

  # Configure the Training Op (for TRAIN mode)
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize( loss=loss, global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

  # Add evaluation metrics (for EVAL mode)
  eval_metric_ops = {
      "accuracy": tf.metrics.accuracy(
          labels=labels, predictions=predictions["classes"])}
  return tf.estimator.EstimatorSpec(
      mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

if not args.noPlot:
    if args.noXterm:
        print('diasable Xterm')
        import matplotlib as mpl
        mpl.use('Agg')  # to plot w/ X-server
    import matplotlib.pyplot as plt
else:
    print('disabled any plotting')

# CPUs are used via a "device" which is just a threadpool
if args.nCpu>0:
    tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
    print('M: restrict CPU count to ',args.nCpu)


# start interactive session only to allow printig of tensor values using eval()
sess = tf.InteractiveSession()

# The data, shuffled and split between train and test sets.
# ....... Load training and eval data
mnist = tf.contrib.learn.datasets.load_dataset("mnist")

mnT=mnist.train; mnV=mnist.validation; mnT=mnist.test
X=mnT.images; Y=np.asarray(mnT.labels, dtype=np.int32)  # needs type change to int32
X_val=mnV.images; Y_val=mnV.labels
X_test=mnT.images; Y_test=mnT.labels
# the objects above are just np-array, not Tensors. Images are flatten to 784 1D vectors

if  args.events>0 :
    neve=args.events
    if neve < X.shape[0]:  
        X=X[:neve]; Y=Y[:neve]
        X_val=X_val[:neve]; Y_val=Y_val[:neve]        
        X_test=X_test[:neve]; Y_test=Y_test[:neve]        
        print('shrink input to ',neve,' images')
print('got input train shape X:',X.shape,type(X),' dig Y:',Y.shape,type(Y),' val dig Y:',Y_val.shape)

# Note, at this moment validation data are not used

# the explicite 1-hot conversion is not needed any more
Y_onehot = tf.one_hot(Y, depth=10)
print(' 1-hot Y:',Y_onehot.shape,type(Y_onehot))
i=66
print(' dump image i=%d dig=%d'%(i,Y[i]),' 1hot:',Y_onehot[i].eval()); 
del Y_onehot

#plot_images(X,Y) # plot 4 images as gray scale, stop the code here

outPath="/global/homes/b/balewski/prj/mnist_test2"
outPath="/junk/mnist_test2"
if not os.path.exists(outPath):
    print('Aborting on start, missing output dir:',outPath)
    exit(22)

print('\n ...... Create the Estimator ......, saved to',outPath)
myEstim = tf.estimator.Estimator(
    model_fn=my_model_fn, model_dir=outPath, # checkpoints saved there
    params={
        'hidden_units': [10, 10],
        'abc': 3,
        'dropFrac':args.dropFrac
    } )

print('\n ..... Init  Training ......') # provides input data for training as minibatches. 
train_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": X},  y=Y, shuffle=True,
      batch_size=args.batch_size,  num_epochs=args.epochs)

print('\n ......Train the model .....')
myEstim.train( input_fn=train_input_fn)

print('\n ......Make predictions and print results .....')
test_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": X_test}, shuffle=False)

myPred=myEstim.predict( input_fn=test_input_fn) # returns generator!!!

print('myPred',myPred, ', inp size',X_test.shape)
i=0
for z in myPred: # iterate the generator
    print(i,'pred z=',z["class_id"],Y_test[i],'<=truth')
    i+=1
    if i>10 : break

myPredL=list(myPred) # alternative execution of generater
print('myPredL len',len(myPredL), ', inp size',X_test.shape)
i=0
for z in myPredL: # iterate the generator
    print(i,'pred z=',z["class_id"],Y_test[i],'<=truth')
    i+=1
    if i>10 : break

print('\n ..... saved model ....')
def serving_input_receiver_fn():
  inputs = {
    INPUT_FEATURE: tf.placeholder(tf.float32, [None, 28, 28, 1]),
  }
  return tf.estimator.export.ServingInputReceiver(inputs, inputs)
model_path='./janModel/'
myEstim.export_savedmodel(
        model_path,
        serving_input_receiver_fn=serving_input_receiver_fn)

okok3
print(myEstim.signature_def[tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY])

feature_spec = {"input_image": tf.placeholder(dtype=tf.float32, shape=[None, 784])}

input_receiver_fn = tf.estimator.export.build_raw_serving_input_receiver_fn(test_input_fn)
input_receiver_fn = test_input_fn
model_path='./janModel/'
myEstim.export_savedmodel(model_path, input_receiver_fn, as_text=True)

#You can list the signature of your saved model by calling:

print(myEstim.signature_def[tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY])

#That should list the expected input/output Tensors.
okok2



# Save the variables to disk.
save_path = saver.save(sess, "/tmp/model.ckpt")
print("Model saved in path: %s" % save_path)

okok1
tf.saved_model.simple_save(sess, outPath, test_input_fn, None)
print('\n ..... predictions from saved model ....')
from tensorflow.contrib import predictor
myPred2_fn = predictor.from_saved_model(outPath)
myPred2=myPred2_fn(test_input_fn)
myPred2L=list(myPred2)
print('myPred2L len',len(myPred2L), ', inp size',X_test.shape)

okok
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

fname=args.prjName+'_graph.svg'
plot_model(model, to_file=fname, show_layer_names=True, show_shapes=True)
print('Graph saved as ',fname)


startTm = time.time()
hir=model.fit(X, Yhot, shuffle=True, verbose=args.verb,
              validation_data=(X_test, Yhot_test),
              batch_size=args.batch_size, epochs=args.epochs)

if args.tf_timeline:
    # Create the Timeline object, and write it to a json
    tl = timeline.Timeline(run_metadata.step_stats)
    ctf = tl.generate_chrome_trace_format()
    tlF=args.prjName+'.timeline.json'
    with open(tlF, 'w') as f: f.write(ctf)
    print('Saved TF timeline as',tlF,', point Chrome browser to : chrome://tracing/')


#evaluate performance for the last epoch
[loss,acc]=model.evaluate(X_test, Yhot_test) #, show_accuracy=True)
fitTime=time.time() - start
print('\n End Validation Accuracy:%.3f'%acc, ', Loss:%.3f'%loss,', fit time=%.1f sec'%(fitTime))

if not args.noPlot:
    plot_fitHir(hir.history,tit='mnist-LSTM, fit %.1f min, nCpu=%d, drop=%.1f'%(fitTime/60.,args.nCpu,args.dropFrac))

