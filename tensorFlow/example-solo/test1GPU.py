#!/usr/bin/env python

""" 
test if GPU is visible for TF

Based on 
https://www.tensorflow.org/guide/using_gpu


CORI-GPU instruction
ssh cori
module load escori
 salloc  -C gpu --gres=gpu:2 -Adasrepo -t2:00:00 --reservation=N9_COE
module load python3
source activate py3_TF_GPU
module load cuda
module load nccl
cd ~/digitalMind/tensorFlow/example-solo
srun nvidia-smi 
srun ./test1_GPU.py

"""
import tensorflow as tf
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings


# Creates a graph.
a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
c = tf.matmul(a, b)
# Creates a session with log_device_placement set to True.
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
# Runs the op.
print(sess.run(c))


'''
Expected output
Device mapping:
/job:localhost/replica:0/task:0/device:GPU:0 -> device: 0, name: Tesla K40c, pci bus
id: 0000:05:00.0
b: /job:localhost/replica:0/task:0/device:GPU:0
a: /job:localhost/replica:0/task:0/device:GPU:0
MatMul: /job:localhost/replica:0/task:0/device:GPU:0
[[ 22.  28.]
 [ 49.  64.]]
'''
