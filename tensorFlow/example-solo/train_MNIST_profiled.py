#!/usr/bin/env python

""" 
Example:  MNIST data  CNN, saved model
generic TF w/ raw tensors

PROFILING lines marked with #PROFI
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train on  MNIST  using CNN',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath",
                        default='data/',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument("--project",
                        default='mnist_cnn',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-s", "--steps", type=int, default=30,
                        help="steps of batches")
    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="batch_size for training")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( '-X', "--noXterm", dest='noXterm', action='store_true',
                         default=False,help="disable X-term for batch mode")

    parser.add_argument("--nCpu", type=int, default=8,
                        help="num CPUs used when fitting, use 0 for all resources")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xDir in [args.dataPath, args.outPath]:
        if not os.path.exists(xDir):
            print('Aborting on start, missing  dir:',xDir)
            exit(22)

    return args

# - - - - - - - - -   actual  code - - - - - - - - - 
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.INFO)

print('libs imported, TF.ver=%s elaT=%.1f sec'%(tf.__version__,(time.time() - startT0)))


#-------------------
def plot_images(X,Y,figId=1):
    fig=plt.figure(figId,facecolor='white', figsize=(10,2.5))
    print('plot images',type(X))
    nrow,ncol=1,4
    for i in range(nrow*ncol):
        ax=plt.subplot( nrow,ncol,i+1)
        x=np.reshape(X[i],(-1,28)) # un-flatten images to 2D
        ax.imshow(x, cmap=plt.get_cmap('gray'))
        dig=np.argmax(Y[i])
        ax.set(title='digit=%d'%dig)
    # show the plot in main


#----------------------------------
def plot_fitHir(DL,tit='fit summary',figId=2):
    # LD -->DL
    #DL=dict(zip(LD[0],zip(*[d.values() for d in LD])))
    #print(DL)

    # Two subplots, unpack the axes array immediately
    f, (ax1, ax2) = plt.subplots(figId,  sharex=True,
                                 facecolor='white', figsize=(8,6))

    stepL=DL['step']
    ax1.set(ylabel='loss',title=tit)
    ax1.plot(stepL,DL['loss'],'.-',label='train')
 
    ax1.legend(loc='best')
    ax1.set_yscale('log')
    ax1.grid(color='brown', linestyle='--',which='both')

    ax2.set(xlabel='steps',ylabel='accuracy')
    ax2.plot(stepL,DL['acc'],'.-',label='train')
    
    ax2.legend(loc='upper left')
    ax2.grid(color='brown', linestyle='--',which='both')
 
    f.subplots_adjust(hspace=0.05)
    figName=args.outPath+'/jan_fit%d'%figId
 
    plt.savefig(figName+'.pdf')
    # show the plot in main


#----------------------------------
# Create some wrappers for simplicity
def conv2d(x, W, b, strides=1):
    # Conv2D wrapper, with bias and relu activation
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    x = tf.nn.bias_add(x, b)
    x = tf.nn.relu(x)
    print('my_conv2d out x:',x.name,x.shape,'; W:',W.name,W.shape,'; b:',b.name,b.shape)
    return x


def maxpool2d(x, k=2):
    # MaxPool2D wrapper
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],
                          padding='SAME')


#----------------------------------
# Create variables
def make_weights():
    # Store layers weight & bias
    weightsL = {
        # 5x5 conv, 1 input, 32 outputs
        'wc1': tf.Variable(tf.random_normal([5, 5, 1, 32])),
        # 5x5 conv, 32 inputs, 64 outputs
        'wc2': tf.Variable(tf.random_normal([5, 5, 32, 64]),name='wc2j'),
        # fully connected, 7*7*64 inputs, 1024 outputs
        'wd1': tf.Variable(tf.random_normal([7*7*64, 1024])),
        # 1024 inputs, 10 outputs (class prediction)
        'out': tf.Variable(tf.random_normal([1024, num_classes]))
    }

    biasesL = {
        'bc1': tf.Variable(tf.random_normal([32])),
        'bc2': tf.Variable(tf.random_normal([64]),name='bc2j'),
        'bd1': tf.Variable(tf.random_normal([1024])),
        'out': tf.Variable(tf.random_normal([num_classes]))
    }
    print('make_weights nW=',len(weightsL), ' nB=',len(biasesL))
    return weightsL,biasesL

#----------------------------------
# Create model
def build_model(x, weights, biases, keepFrac):
    # MNIST data input is a 1-D vector of 784 features (28*28 pixels)
    # Reshape to match picture format [Height x Width x Channel]
    # Tensor input become 4-D: [Batch Size, Height, Width, Channel]
    x = tf.reshape(x, shape=[-1, 28, 28, 1])

    with tf.name_scope('convA'):
        # Convolution Layer
        conv1 = conv2d(x, weights['wc1'], biases['bc1'])
        # Max Pooling (down-sampling)
        conv1 = maxpool2d(conv1, k=2)
        
    # Convolution Layer
    conv2 = conv2d(conv1, weights['wc2'], biases['bc2'])
    # Max Pooling (down-sampling)
    conv2 = maxpool2d(conv2, k=2)

    # Fully connected layer
    # Reshape conv2 output to fit fully connected layer input
    fc1 = tf.reshape(conv2, [-1, weights['wd1'].get_shape().as_list()[0]])
    fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
    fc1 = tf.nn.relu(fc1)
    # Apply Dropout
    fc1 = tf.nn.dropout(fc1, keepFrac)

    # Output, class prediction
    out = tf.add(tf.matmul(fc1, weights['out']), biases['out'])
    return out


#----------------------------------
# train model, preserve history
def train_model(sess,mnist,report_step, profiL):
    run_metadata, options = profiL  #PROFI
    hir={'loss':[],'acc':[],'step':[]}
    for step in range(args.steps):
        batch_x, batch_y = mnist.train.next_batch(args.batch_size)
        feed_dict={X: batch_x, Y: batch_y, keepFrac: (1-args.dropFrac)}
        
        # Run optimization op (backprop)
        sess.run(train_op, feed_dict=feed_dict, options=options, run_metadata=run_metadata)  #PROFI
        profiler.add_step(step, run_metadata)  #PROFI
         
        if step % report_step ==  0:
            # Calculate batch loss and accuracy
            loss, acc = sess.run([loss_op, accuracy], feed_dict={X: batch_x,
                                                                 Y: batch_y,
                                                                 keepFrac: 1.0})
            hir['step'].append(step) # archive history for plotting
            hir['loss'].append(loss)
            hir['acc'].append(acc)

            print("Step " + str(step) + ", Minibatch Loss= " + \
                  "{:.4f}".format(loss) + ", Training Accuracy= " + \
                  "{:.3f}".format(acc))

    print("Optimization Finished!")
    return hir


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

if args.noXterm:
    print('diasable Xterm')
    import matplotlib as mpl
    mpl.use('Agg')  # to plot w/ X-server
import matplotlib.pyplot as plt

# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnistDS = input_data.read_data_sets(args.dataPath, one_hot=True)
plot_images( mnistDS.test.images, mnistDS.test.labels)


# CPUs are used via a "device" which is just a threadpool
if args.nCpu>0:
    tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
    print('M: restrict CPU count to ',args.nCpu)

# Training Parameters
learning_rate = 0.001
report_step = 10

# Network Parameters
num_input = 784 # MNIST data input (img shape: 28*28)
num_classes = 10 # MNIST total classes (0-9 digits)

# tf Graph input
X = tf.placeholder(tf.float32, [None, num_input])
Y = tf.placeholder(tf.float32, [None, num_classes])
keepFrac = tf.placeholder(tf.float32) # 1-dropout

weightsL,biasesL =make_weights()

print(' Construct model')
logits = build_model(X, weightsL, biasesL, keepFrac)
prediction = tf.nn.softmax(logits)

# Define loss and optimizer
loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits( logits=logits, labels=Y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss_op)

# Evaluation graph
correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

print(' Initialize TF variables ')
init = tf.global_variables_initializer()

# 'Saver' op to save and restore all the variables
saver = tf.train.Saver()

# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnistDS = input_data.read_data_sets(args.dataPath, one_hot=True)
plot_images( mnistDS.test.images, mnistDS.test.labels)

print(' Start training ...', type(mnistDS))
with tf.Session() as sess:
    
    # Run the initializer
    sess.run(init)

    print("\n\n= = = =   PROFI-1- will print model details")
    param_stats = tf.contrib.tfprof.model_analyzer.print_model_analysis(
        tf.get_default_graph(),
        tfprof_options=tf.contrib.tfprof.model_analyzer.TRAINABLE_VARS_PARAMS_STAT_OPTIONS)
    print('PROFI tot_params: %d, tot_tensors:%d\n' % (param_stats.total_parameters,len(param_stats.children)))
    for x in param_stats.children:
        #xx=param_stats.children[x]
        print('tensor', x) # it is 'message'
        break
        ''' has 4 fileds I do not know how to access
        tensor name: "Variable"
        parameters: 800
        total_parameters: 800
        total_definition_count: 1
        '''
    # Profiler is created here.
    profiler = tf.profiler.Profiler(sess.graph)
    options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    run_metadata = tf.RunMetadata()
    profiL=[run_metadata,options]
    
    # PROFI-end

    startT = time.time()
    hir=train_model(sess,mnistDS,report_step,profiL)  # PROFI
    fitTime=time.time() - startT

    print("\n\n= = = =   PROFI-2 compute flops per layer, works only after metadata are passed ")
    # e.g. by sess.run(.... options=options, run_metadata=run_metadata)
    tf.contrib.tfprof.model_analyzer.print_model_analysis(
        tf.get_default_graph(), tfprof_options=tf.contrib.tfprof.model_analyzer.FLOAT_OPS_OPTIONS)
    
    outF=args.outPath+'/'+args.prjName+'_profile.txt'
    print("\n\n= = = =   PROFI-3 save timing in",outF)
    option_builder = tf.profiler.ProfileOptionBuilder
    opts = (option_builder(option_builder.time_and_memory()).
            with_step(-1). # with -1, should compute the average of all registered steps.
            with_file_output(outF).select(['micros','bytes','occurrence']).order_by('micros').build())
    # Profiling infos about ops are saved in 'test-%s.txt' % FLAGS.out
    profiler.profile_operations(options=opts)
    
    
    # Save model weights to disk
    save_path = saver.save(sess, args.outPath)
    print("Model saved in file: %s" % save_path)

    # Calculate accuracy for 256 MNIST test images
    numTest=1000
    print("Testing Accuracy for numTest=%d:"%numTest, \
          sess.run(accuracy, feed_dict={X: mnistDS.test.images[:numTest],
                                        Y: mnistDS.test.labels[:numTest],
                                        keepFrac: 1.0}))

exit(33)
plot_fitHir(hir,tit='mnist, fit %.1f min, nCpu=%d, drop=%.1f'%(fitTime/60.,args.nCpu,args.dropFrac))
plt.tight_layout()

plt.show()
