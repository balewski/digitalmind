#!/usr/bin/env python
import tensorflow as tf
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

# conclusion: tf.name_scope() ignores variables created with tf.get_variable() because it assumes that you know which variable and in which scope you wanted to use.

def scopingTest(fn, scope1, scope2, vals):
    with fn(scope1):
        a = tf.Variable(vals[0], name='a_Var')
        b = tf.get_variable('b_get_var', initializer=vals[1])
        c = tf.constant(vals[2], name='c_const')

        with fn(scope2):
            d = tf.add(a * b, c, name='prod')

        print ('\n  dump-->   '.join([scope1, a.name, b.name, c.name, d.name]), '\n')
    return d

print('start')
d1 = scopingTest(tf.variable_scope, 'scope_vars', 'res1', [1, 2, 3])
d2 = scopingTest(tf.name_scope,     'scope_name', 'res2', [1, 2, 3])



with tf.Session() as sess:
    writer = tf.summary.FileWriter('out', sess.graph)
    sess.run(tf.global_variables_initializer())
    
    sess.run([d1, d2]) 
#    sess.run(d3)
    
    writer.close()
print('end,  I executes the graph to get values of the resulting values and save event-files to investigate them in TensorBoard.')
