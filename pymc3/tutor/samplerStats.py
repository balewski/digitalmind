#!/usr/bin/env python
'''
http://docs.pymc.io/notebooks/sampler-stats.html

it is often helpful to take a closer look at what the sampler is doing. For this purpose some samplers export statistics for each generated sample.
'''
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb
import pandas as pd
import pymc3 as pm

# As a minimal example we sample from a standard normal distribution:

model = pm.Model()
with model: # provide context
    mu1 = pm.Normal("mu1", mu=1, sd=2, shape=10)


with model:
    step = pm.NUTS()
    trace = pm.sample(2000, tune=1000, init=None, step=step, cores=2, chains=2)

#NUTS provides the following statistics:
print('trace=',trace.stat_names)

print('end1')

varName='step_size_bar'

plt.plot(trace[varName])
plt.title(varName)

#The get_sampler_stats method provides more control over which values should be returned

sizes1, sizes2 = trace.get_sampler_stats('depth', combine=False)
fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, sharey=True)
ax1.plot(sizes1)
ax2.plot(sizes2)
ax1.set_title('depth')


plt.subplots(1, 1)
varName='mean_tree_accept'
accept = trace.get_sampler_stats(varName, burn=1000)
ax=sb.distplot(accept, kde=False)
ax.set_title(varName)


print('my mean',accept.mean())
# Find the index of all diverging transitions:
print('diverging:',trace['diverging'].nonzero())


# It is often useful to compare the overall distribution of the energy levels with the change of energy between successive samples. Ideally, they should be very similar:


plt.subplots(1, 1)
energy = trace['energy']
energy_diff = np.diff(energy)
sb.distplot(energy - energy.mean(), label='energy')
sb.distplot(energy_diff, label='energy diff')
ax.set_title('energy levels')
plt.legend()

#If the overall distribution of energy levels has longer tails, the efficiency of the sampler will deteriorate quickly.




plt.show()

