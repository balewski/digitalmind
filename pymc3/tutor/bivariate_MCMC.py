#!/usr/bin/env python
'''
Markov Ch. MC event generator.
Alice: takes Bob's price during the previous hour, multiply by 0.6, add 90$, add Gaussian noise from  N(0,20)N(0,20) .

Bob: takes Alice's price during the current hour, multiply by 1.2 and subtract 20$, add Gaussian noise from  N(0,10)N(0,10) .

'''
import numpy as np
import pandas as pd
import numpy.random as rnd
import seaborn as sns
import seaborn
import warnings
import matplotlib.pyplot as plt

warnings.filterwarnings('ignore')

from matplotlib import animation
import pymc3 as pm

# Implement the run_simulation function according to the description above.
def run_simulation(alice_start_price=300.0, bob_start_price=300.0, seed=42, num_hours=2000, burnin=1000):
    """Simulates an evolution of prices set by Bob and Alice.
    """
    np.random.seed(seed)

    alice_prices = [alice_start_price]
    bob_prices = [bob_start_price]
    
    #### YOUR CODE HERE ####
    for t in range(num_hours):
        A0=alice_prices[-1]
        B0=bob_prices[-1]
        A=B0*0.6 +90 + np.random.normal(0,20)
        B=A*1.2 -20 + np.random.normal(0,10)
        alice_prices.append(A)
        bob_prices.append(B)
        #print(t,A,B) 
    ### END OF YOUR CODE ###
    
    return alice_prices[burnin:], bob_prices[burnin:]

alice_prices, bob_prices = run_simulation(alice_start_price=300, bob_start_price=300, seed=42, num_hours=3, burnin=1)
if len(alice_prices) != 3:
    raise RuntimeError('Make sure that the function returns `num_hours` data points.')

print('ali-pr',alice_prices)

#1.1 plot
alice_prices, bob_prices = run_simulation()
plt.plot(alice_prices, bob_prices)
plt.xlabel('alice_prices')
plt.ylabel('bob_prices')


#1.2
print('averages: alice=',np.mean(alice_prices),' bob=',np.mean(bob_prices))
corr=np.corrcoef(alice_prices, bob_prices)
print('corr',corr)

#1.3 Let's look at the 2-d histogram of prices, computed using kernel density estimation.

data = np.array(run_simulation())
axg=sns.jointplot(data[0, :], data[1, :], stat_func=None, kind='kde')
print('axg',axg)


plt.show()
