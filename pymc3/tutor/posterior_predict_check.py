#!/usr/bin/env python
'''
http://docs.pymc.io/notebooks/posterior_predictive.html

PPCs are a great way to validate a model. The idea is to generate data sets from the model using parameter settings from draws from the posterior.

'''

import numpy as np
import pymc3 as pm
import seaborn as sns
import matplotlib.pyplot as plt
from collections import defaultdict
#Here we will implement a general routine to draw samples from the observed nodes of a model.


#1 ) Lets generate a very simple model:
data = np.random.randn(100)

with pm.Model() as model:
    mu = pm.Normal('mu', mu=0.5, sd=1, testval=0)
    sd = pm.HalfNormal('sd', sd=2)
    n = pm.Normal('n', mu=mu, sd=sd, observed=data)

    trace = pm.sample(5000, cores=2, chains=2)

ax=pm.traceplot(trace)


#This function will randomly draw 500 samples of parameters from the trace. Then, for each sample, it will draw 100 random numbers from a normal distribution specified by the values of mu and std in that sample.

ppc = pm.sample_ppc(trace, samples=500, model=model, size=100)

print('ppc output',np.asarray(ppc['n']).shape)

# how close are the inferred means to the actual sample mean:

ax = plt.subplot()
sns.distplot([n.mean() for n in ppc['n']], kde=False, ax=ax)
ax.axvline(data.mean())
ax.set(title='Posterior predictive of the mean', xlabel='mean(x)', ylabel='Frequency');

plt.show() 
