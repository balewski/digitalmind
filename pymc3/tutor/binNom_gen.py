#!/usr/bin/env python
'''
Markov Ch. MC event generator.
Alice: takes Bob's price during the previous hour, multiply by 0.6, add 90$, add Gaussian noise from  N(0,20)N(0,20) .

Bob: takes Alice's price during the current hour, multiply by 1.2 and subtract 20$, add Gaussian noise from  N(0,10)N(0,10) .

'''
import numpy as np
import pandas as pd
import numpy.random as rnd
import seaborn as sns
import seaborn
import warnings
import matplotlib.pyplot as plt

warnings.filterwarnings('ignore')

from matplotlib import animation
import pymc3 as pm
# 2 Animation of MCMC

from IPython.display import HTML


#1 generate data to be binminal

def invlogit(x):
    return np.exp(x) / (1 + np.exp(x))

n_exp = 400
coeff = 5.  # sigma multiplier
offset=-2.
predicL = np.random.normal(size=n_exp) # X=0, sig=1
print('pred=',predicL[:20])

outcL = np.random.binomial(1, invlogit(offset+coeff * predicL))

print('out=',outcL[:20])

axg=sns.jointplot(predicL, outcL, stat_func=None, kind='kde')
print('axg',axg)

fig = plt.figure(figsize=(3, 3))
plt.plot(predicL, outcL)
plt.xlabel('prediction')
plt.ylabel('observation')

# create pandas data frame
df2 = pd.DataFrame({ 'xwidth' : predicL, 'outBino': outcL})
print(df2.head())

#3  build model
n_gen=2000
with pm.Model() as logistic_model:
    # Train Bayesian logistic regression model on the following feature:
    # outBino ~ xwidth
    # Save the output of pm.sample to a variable: this is the trace of the sampling procedure and will be used
    # to estimate the statistics of the posterior distribution.    
    #### YOUR CODE HERE ####    
    pm.glm.GLM.from_formula('outBino ~  xwidth', df2, family=pm.glm.families.Binomial())
    start = pm.find_MAP() #maximum aposterior estimator
    print('start: MAP estamate based on data',start)
    #step = pm.Metropolis()
    step = pm.NUTS()  # very advanced generator
    traces = pm.sample(n_gen, step=step, start=start, njobs=1, chains=1)
    # sampling 1 chains in 1 job - not sure what it means.
    ### END OF YOUR CODE ###


trace1=traces[7]
print('one trace size',len(trace1),' num traces=',len(traces))
for x in trace1:
   print('name=',x, type(x),'val=',trace1[x])
   
#4-------------------------------

# You will need the following function to visualize the sampling process.
# You don't need to change it.
def plot_traces(traces, burnin=200):
    ''' 
    Convenience function:
    Plot traces with overlaid means and values
    '''
    
    ax = pm.traceplot(traces[burnin:], figsize=(12,len(traces.varnames)*1.5),
        lines={k: v['mean'] for k, v in pm.df_summary(traces[burnin:]).iterrows()})

    for i, mn in enumerate(pm.df_summary(traces[burnin:])['mean']):
        ax[i,0].annotate('{:.2f}'.format(mn), xy=(mn,0), xycoords='data'
                    ,xytext=(5,10), textcoords='offset points', rotation=90
                    ,va='bottom', fontsize='large', color='#AA0022')
        

plot_traces(traces, burnin=900)

# 5 (recall that credible intervals are Bayesian and confidence intervals are frequentist)

burnin = 100
b= traces['xwidth'][burnin:]
print(b)

lb, ub = np.percentile(b, 2.5), np.percentile(b, 97.5)
print("P(%.3f < xwidth < %.3f) = 0.95" % (lb, ub))

b= traces['Intercept'][burnin:]
print(b)

lb, ub = np.percentile(b, 2.5), np.percentile(b, 97.5)
print("P(%.3f < Intercept < %.3f) = 0.95" % (lb, ub))


plt.tight_layout()
plt.show()
