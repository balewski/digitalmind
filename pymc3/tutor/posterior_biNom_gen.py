#!/usr/bin/env python
'''
http://docs.pymc.io/notebooks/posterior_predictive.html

Here we’re building a logistic regression model. Note that since we’re dealing the full posterior, we’re also getting uncertainty in our predictions for free.
'''

import numpy as np
import pymc3 as pm
import seaborn as sns
import matplotlib.pyplot as plt
from collections import defaultdict

# Use a theano shared variable to be able to exchange the data the model runs on
from theano import shared

def invlogit(x):
    return np.exp(x) / (1 + np.exp(x))

n = 4000
n_oos = 50
coeff = 1.

predictors = np.random.normal(size=n)
# Turn predictor into a shared var so that we can change it later
predictors_shared = shared(predictors)
print('pred=',predictors[:20])

outcomes = np.random.binomial(1, invlogit(coeff * predictors))

print('out=',outcomes[:20])


predictors_oos = np.random.normal(size=50)
outcomes_oos = np.random.binomial(1, invlogit(coeff * predictors_oos))
def tinvlogit(x):
    import theano.tensor as t
    return t.exp(x) / (1 + t.exp(x))

with pm.Model() as model:
    coeff = pm.Normal('coeff', mu=0, sd=1)
    p = tinvlogit(coeff * predictors_shared)

    o = pm.Bernoulli('o', p, observed=outcomes)

    trace = pm.sample(5000, n_init=5000, cores=2, chains=2)


# Changing values here will also change values in the model
predictors_shared.set_value(predictors_oos)

# Simply running PPC will use the updated values and do prediction
ppc = pm.sample_ppc(trace, model=model, samples=500)

# Mean predicted values plus error bars to give sense of uncertainty in prediction

plt.errorbar(x=predictors_oos, y=np.asarray(ppc['o']).mean(axis=0), yerr=np.asarray(ppc['o']).std(axis=0), linestyle='', marker='o')
plt.plot(predictors_oos, outcomes_oos, 'o')
plt.ylim(-.05, 1.05)
plt.xlabel('predictor')
plt.ylabel('outcome')
plt.show()
