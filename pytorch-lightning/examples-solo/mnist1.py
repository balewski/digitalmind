#!/usr/bin/env python3

''' minimal MNIST example from
https://pytorch-lightning.readthedocs.io/en/latest/lightning_module.html#minimal-example
'''

import pytorch_lightning as pl
import torch
from torch.nn import functional as F

from torch.utils.data import DataLoader
from torchvision.datasets import MNIST
import os
from torchvision import datasets, transforms

class LitModel(pl.LightningModule):

     def __init__(self):
         super().__init__()
         # Define computations here
         self.lyr1 = torch.nn.Linear(28 * 28, 10)

     def forward(self, x):
         # Use for inference only (separate from training_step)
         return torch.relu(self.lyr1(x.view(x.size(0), -1)))

     def training_step(self, batch, batch_idx):
         # the full training loop
         x, y = batch
         y_hat = self(x)
         loss = F.cross_entropy(y_hat, y)
         return loss
     
     # not implemented in this example
     # def validation_step():
     # def test_step():

     def configure_optimizers(self):
         # define optimizers and LR schedulers
         return torch.optim.Adam(self.parameters(), lr=0.02)


# = = = = = = =  M A I N = = = = = =

train_loader = DataLoader(MNIST(os.getcwd(), download=True, transform=transforms.ToTensor()))
trainer = pl.Trainer(max_epochs=2, profiler="simple")#,gpus=1)
model = LitModel()

trainer.fit(model, train_loader)
