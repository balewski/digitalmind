#!/usr/bin/env python3

''' minimal MNIST example from
https://pytorch-lightning.readthedocs.io/en/latest/lightning_module.html#minimal-example

Train on 2 GPUs/node x 2 nodes --> total 4 GPUs

  salloc -C gpu -N 2 -t 4:00:00 -c 10 --gres=gpu:2 --ntasks-per-node=2 
 srun -n4 -l python -u  ./mnist_multi.py 

'''

import pytorch_lightning as pl
import torch
from torch.nn import functional as F

from torch.utils.data import DataLoader
from torchvision.datasets import MNIST
import os
from torchvision import datasets, transforms

class LitModel(pl.LightningModule):
#....!....!..................5
     def __init__(self):
          super().__init__()
          # Define computations here
          self.lyr1 = torch.nn.Linear(28 * 28, 10)

     def forward(self, x):
          # Use for inference only (separate from training_step)
          return torch.relu(self.lyr1(x.view(x.size(0), -1)))

     
class LitClassifier(pl.LightningModule):
#....!....!..................5
     def __init__(self, model):
          super().__init__()
          self.model = model

     def training_step(self, batch, batch_idx):
          x, y = batch
          y_hat = self.model(x)
          loss = F.cross_entropy(y_hat, y)
          return loss
    
     def configure_optimizers(self):
          # define optimizers and LR schedulers
          return torch.optim.Adam(self.parameters(), lr=0.02)
     
           

# = = = = = = =  M A I N = = = = = =

rank = int(os.environ['SLURM_PROCID'])
world_size = int(os.environ['SLURM_NTASKS'])
verb=rank==0
print('M:myRank=',rank,'world_size =',world_size,'verb=',verb )

train_loader = DataLoader(MNIST(os.getcwd(), download=True, transform=transforms.ToTensor()))

model = LitModel()
classifier=LitClassifier(model)

trainer = pl.Trainer(max_epochs=2, gpus=2, distributed_backend='ddp', num_nodes=2,progress_bar_refresh_rate=verb, profiler="simple")


verb=trainer.global_rank==0

trainer.fit(classifier, train_loader)
