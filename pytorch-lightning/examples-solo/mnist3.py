#!/usr/bin/env python3

''' minimal MNIST example from
https://pytorch-lightning.readthedocs.io/en/latest/lightning_module.html#minimal-example

The model is kept separately from training
*) add Training epoch-level metrics
*) override training_epoch_end yourself.

'''

import pytorch_lightning as pl
import torch
from torch.nn import functional as F

from torch.utils.data import DataLoader
from torchvision.datasets import MNIST
import os
from torchvision import datasets, transforms

class LitModel(pl.LightningModule):

     def __init__(self):
         super().__init__()
         # Define computations here
         self.lyr1 = torch.nn.Linear(28 * 28, 10)

     def forward(self, x):
         # Use for inference only (separate from training_step)
         return torch.relu(self.lyr1(x.view(x.size(0), -1)))


class LitClassifier(pl.LightningModule):

     def __init__(self, model):
         super().__init__()
         self.model = model
         self.stepCnt=0
         
     def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.model(x)
        loss = F.cross_entropy(y_hat, y)
        self.log('val_loss', loss)
        
     def training_step(self, batch, batch_idx):
         x, y = batch
         y_hat = self.model(x)
         loss = F.cross_entropy(y_hat, y)

         # logs metrics for each training_step,
         # and the average across the epoch, to the progress bar and logger
         self.log('train_lossJan', loss, on_step=False, on_epoch=True, prog_bar=True, logger=True)

         self.stepCnt+=1
         #preds=self.model.forward(x)
         return {'loss': loss, 'stepCnt': self.stepCnt}
    
     def training_epoch_end(self, training_step_outputs):
         for pred in training_step_outputs:
              print('EOE',pred)
              # do something
        
     def configure_optimizers(self):
         # define optimizers and LR schedulers
         return torch.optim.Adam(self.parameters(), lr=0.02)

# = = = = = = =  M A I N = = = = = =



train_loader = DataLoader(MNIST(os.getcwd(), download=True, transform=transforms.ToTensor()))
model = LitModel()

classifier=LitClassifier(model)

trainer = pl.Trainer( max_epochs=2)#gpus=1)

trainer.fit(classifier, train_loader)
