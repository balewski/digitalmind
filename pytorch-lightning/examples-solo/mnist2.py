#!/usr/bin/env python3

''' minimal MNIST example from
https://pytorch-lightning.readthedocs.io/en/latest/lightning_module.html#minimal-example

The model is kept separately from training

'''

import pytorch_lightning as pl
import torch
from torch.nn import functional as F

from torch.utils.data import DataLoader
from torchvision.datasets import MNIST
import os
from torchvision import datasets, transforms

class LitModel(pl.LightningModule):
#....!....!..................5
     def __init__(self):
          super().__init__()
          # Define computations here
          self.lyr1 = torch.nn.Linear(28 * 28, 10)

     def forward(self, x):
          # Use for inference only (separate from training_step)
          return torch.relu(self.lyr1(x.view(x.size(0), -1)))

     
class LitClassifier(pl.LightningModule):
#....!....!..................5
     def __init__(self, model):
          super().__init__()
          self.model = model

     def training_step(self, batch, batch_idx):
          x, y = batch
          y_hat = self.model(x)
          loss = F.cross_entropy(y_hat, y)
          self.log('my_loss', loss, on_step=True, on_epoch=True, prog_bar=False, logger=True)
          return loss
    
     def configure_optimizers(self):
          # define optimizers and LR schedulers
          return torch.optim.Adam(self.parameters(), lr=0.02)
     
     def get_progress_bar_dict(self):
          # don't show the version number
          items = super().get_progress_bar_dict()
          items.pop("v_num", None)
          return items
          

# = = = = = = =  M A I N = = = = = =



train_loader = DataLoader(MNIST(os.getcwd(), download=True, transform=transforms.ToTensor()))

model = LitModel()
classifier=LitClassifier(model)

# disable progress bar
#trainer = Trainer(progress_bar_refresh_rate=0)

trainer = pl.Trainer(gpus=1)
print('M:myRank=',trainer.global_rank)

trainer.fit(classifier, train_loader)
