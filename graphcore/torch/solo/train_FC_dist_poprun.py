#!/usr/bin/env python3

'''

The weights initialisation: the seed is set differently on each instance, replicas will be initialised with different weights (since they are initialised randomly by default) which is problematic. One way to be sure that all instances get the same weights is to broadcast them with horovod at initialisation (it will require pip install horovod):

in Trainer.py:

import horovod.torch as hvd
hvd.init()
...
# In Trainer __init__()
# after instantiating the optimizers, and before creating the poptorch models (~line 129):

hvd.broadcast_parameters(modelWloss.state_dict(), root_rank=0)

= = = = = = 
 example of simple FC +regression
 a noisy Y is added to X so training  converges 
 uses DataLoader  which serves all input samples (,auto_distributed_partitioning=False)
 code keeps constant:
  * global number of samples
  * global BS , localBS=globalBS/world_size
  

This is a single IPU code.
The data-parallel IPU traing should work using NCCL

gc-monitor
cd /digitalMind/graphcore/torch/solo/
./train_FC-dist_poprun.py

Alternative,  (instance== MPI rank, replica==model)
run the same code using poprun on a single IPU
$  poprun  --num-instances=1 --num-replicas=1 --ipus-per-replica 1 train_FC-dist_poprun.py

m=2
poprun --num-instances=$m  --num-replicas=$m   train_FC_dist_poprun.py

- - -   M2000 - - - - 
Scaling:  globalBS=const, global samples=const, LR=const
local BS=globalBS/world_size, localSamples=global samples/world_size


'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import os, time, sys

import torch
import poptorch
import socket  # for hostname

import popdist.poptorch

from Util_CNN import MyModel,  MyModelWithLoss, MyDataset,train_one_epoch, create_dataset



#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':

    host=socket.gethostname()
    device_id = popdist.popdist_core.getDeviceId()
    locReplicas = int(popdist.getNumLocalReplicas())
    total_replicas= int(popdist.getNumTotalReplicas())
    rank = popdist.getInstanceIndex() # index of the current instance 
    world_size = popdist.getNumInstances() # total number of instances
    replicas_per_rank=total_replicas//world_size
    ipus_per_replica=popdist.getNumIpusPerReplica()
    total_ipus=total_replicas*ipus_per_replica
    print("I am rank=%d of world=%d on host=%s, locReplias=%d devId %d totReplias=%d totIpus=%d repl/ranl=%d ipu/repl=%d"% (rank, world_size, host,locReplicas,device_id,total_replicas,total_ipus,replicas_per_rank,ipus_per_replica))
    verb=rank==0

    doCaching=1          # compile graph once, keep all previous graphs usable
    epochs=8
    localBS=1024// world_size  # per replica   
    globNSamp=1024*80
    #globNSamp=1024*2000*10; epochs=3  # reference job

    # 1 instance may manage many replicas
    replica_steps_per_iter=5  # per replica
    num_cpuWorker=2  # per instance

    globBS=localBS*total_replicas  #  only for printout
    loaderBS=localBS*replica_steps_per_iter*replicas_per_rank # only for printout
    replNSamp=globNSamp//total_replicas
    
    steps=replNSamp//localBS 

    instNSamp=globNSamp//world_size

    print('M:myRank=instance=',rank,'world_size =',world_size,'verb=',verb,'host=',host, 'verb=',verb,"IPU id:", device_id )
    if verb:
        print('imported PyTorch ver:',torch.__version__)
        print("replicas per rank:", locReplicas)
        print("Total replicas number:", total_ipus)
        print(' model caching=',doCaching)
        print('Train for epochs=%d, localBS=%d, loaderBS=%d steps=%d NSamp/replica=%d  instNSamp=%d  globNSamp=%d'%(epochs,localBS,loaderBS,steps,replNSamp,instNSamp,globNSamp) )
        goalLoaderNSamp=world_size * localBS * replicas_per_rank * replica_steps_per_iter
        #print('aa', globNSamp , goalLoaderNSamp) 
        assert globNSamp >= goalLoaderNSamp # too little global samples
        assert globNSamp % ( goalLoaderNSamp)==0 # OR some data will be lost

    # Initialize model
    inp_dim=80
    fc_dim=20
    out_dim=10
    learning_rate = 0.0002

    model = MyModel(inp_dim,fc_dim,out_dim)
    if verb: print('M: torchsummary.summary(model):'); print(model)
    modelWloss = MyModelWithLoss(model)

    if world_size>1:
        import horovod.torch as hvd
        hvd.init()
        print('M:horovod started, myRank=%d of %d'%(hvd.rank(),hvd.size()))
        hvd.broadcast_parameters(model.state_dict(), root_rank=0)
        print('M:init wieights synch before creating the poptorch models ')
    else:
        hvd=None

    # Initialize optimizer
    optimizer = poptorch.optim.AdamW(model.parameters(), lr=learning_rate)
    print('isConstant(LR)=',optimizer.variable_attrs.isConstant("lr"))
    
    # - - - -  DATA  PERP - - - - -
    if verb: print('\nM: generate data per instance instNSamp=%d  instance=rank=%d of %d'%(instNSamp,rank,world_size))
    X,Y=create_dataset(instNSamp,inp_dim,out_dim,rank)

    if verb: print('\nCreate pth-Dataset instance')
    trainDst=MyDataset(X,Y)
    if verb: print('Create pt-DataLoader instance ')

    opts = popdist.poptorch.Options()
    opts.deviceIterations(replica_steps_per_iter) # Device "step"
    opts.randomSeed(42+device_id) #I want it different droput per replica, must sync initial weights
    if doCaching:  
        opts.enableExecutableCaching('./exec_cache')

    if verb: print('M:make data-loader... ')
    
    #GC: we can deal 'manually' with the dataset partitioning by disabling the automatic dataset partitioning of the Dataloader 
    #     each instance only create a partition of size num_eve/M (M being the number of instance
    #     each instance will further re-distrubut the data over N/M replicas (if N>M)
    trainLdr = poptorch.DataLoader(opts,trainDst, batch_size=localBS, num_workers=num_cpuWorker, shuffle=True,auto_distributed_partitioning=False)


    if verb:
        print('\nM: dump one batch of training data, instance loader size=%d  replica steps/iter=%d'%(len(trainLdr),replica_steps_per_iter))
        xx, yy = next(iter(trainLdr))
        print('X:',xx.shape)#,'Y:',yy.shape)
        print('Y:',yy.shape)#,yy[:,0])
        loaderNSamp=xx.shape[0]*len(trainLdr)
        print('loader nSamp',loaderNSamp)
        assert loaderBS==xx.shape[0]
        assert loaderNSamp*world_size==globNSamp

    # - - - -  DATA  READY - - - - - 
    # Create model for training which will run on IPU.
    training_model = poptorch.trainingModel(modelWloss, options=trainLdr.options, optimizer=optimizer)  

    #input_size=(inp_dim,)
    #summary(model,input_size) # will print  <== make it work?

    if verb:
        #print('optimizer=', optimizer)
        print('\nTrain for epochs=%d, localBS=%d globBS=%d  LR=%.3g,  sampl/replica=%dK of total %dK, world_size=%d'%(epochs,localBS,globBS,learning_rate,replNSamp//1024,globNSamp//1024,world_size) )

    startT0=time.time()
    startT1=startT0
    for epoch in range(epochs):
        t1=time.time()
        loss = train_one_epoch( training_model, trainLdr,hvd)
        t2=time.time()
        if epoch%1==0 and verb: print('  M: epoch=%d   loss=%.3g, this epoch T=%.2f sec, rank=%d LR=%.2g'%(epoch ,loss,t2-t1,rank,optimizer.param_groups[0]['lr']))
        if epoch ==2000:
            print('M: disable LR to switch to pseudo-inference mode ')
            optimizer.param_groups[0]['lr'] =0.
            training_model.setOptimizer(optimizer)
            
    if verb: print('\nM: done training, elapsed T=%.2f sec'%((time.time()-startT0)))
