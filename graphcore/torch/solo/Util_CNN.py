import os, time, sys
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset
#-------------------
#-------------------
#-------------------
class MyModel(torch.nn.Module):
    def __init__(self,inp_dim,fc_dim,out_dim):
        super().__init__()
        self.fc1 = nn.Linear(inp_dim,fc_dim)
        self.fc2 = nn.Linear(fc_dim,fc_dim)
        self.fc3 = nn.Linear(fc_dim,out_dim)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.dropout(x, p=0.05, training=self.training)
        x = self.fc3(x)
        y = torch.tanh(x)
        return y

#-------------------
#-------------------
#-------------------
class MyModelWithLoss(torch.nn.Module): # GC wrapper class
    def __init__(self, model):
        super().__init__()
        self.model = model
        self.loss = nn.MSELoss()

    def forward(self, x,ytrue):
        ypred = self.model(x)
        return ypred, self.loss(ypred,ytrue)

    #Note, forward(.) output can be conditioned on self.training but NOT on ytrue==None (despit the latter may work in some simple cases)
    

    
#-------------------
#-------------------
#-------------------
class MyDataset(Dataset): # from input np-arrays
    def __init__(self, npX, npY):
        self.X=npX
        self.Y=npY
        self.num_eve=self.Y.shape[0]

    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.X[idx]
        y=self.Y[idx]
        sample =(x,y)
        return sample

#= = = = = = = = = = =  UTIL FUNCTIONS

#...!...!..................
def train_one_epoch(model, dataLoader, hvd=None):
    model.train()
    loss=0
    for i, (data, target) in enumerate(dataLoader):        
        _, loss_op = model(data, target)
        loss += loss_op.numpy()
    loss /= len(dataLoader)
    #print('b',loss,len(dataLoader),data.size()[0],loss_op.numpy().shape)

    if hvd!=None:
        loss = np.mean(hvd.allgather_object(loss))
    return loss


#...!...!..................
def create_dataset(num_train,inp_dim,out_dim,rank,verb=1):
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-1,1, size=(num_train,out_dim)).astype('float32')
    if verb: print('DS: done Y shape',Y.shape,Y.dtype)

    X=np.random.uniform(-0.8,0.8, size=(num_train,inp_dim)).astype('float32')
    if verb: print('DS: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT),'rank=',rank)

    # inject Y to X so training can converge
    X[:,:out_dim]=Y*0.8+np.random.normal(loc=0, scale=0.02, size=(num_train,out_dim)).astype('float32')

    #GC: return torch vectors as they will need to be casted somewhere because torch use this type of Tensors
    return torch.from_numpy(X), torch.from_numpy(Y)


#...!...!..................
def save_checkpoint( checkpoint_path, model,optimizer,epoch):
    torch.save({ 'epoch': epoch, 'model_state': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict()}, checkpoint_path)

#...!...!..................
def restore_checkpoint( checkpoint_path,model,optimizer=None):
    checkpoint = torch.load(checkpoint_path)
    model=model.load_state_dict(checkpoint['model_state'])
    if optimizer!=None:
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    startEpoch = checkpoint['epoch'] + 1
    return startEpoch
