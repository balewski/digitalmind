#!/usr/bin/env python3

'''
 example of simple FC +regression
 a noisy Y is added to X so training  converges 
 uses DataLoader (aka generator) for input

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time, sys
import copy

startT0 = time.time()
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader


print('imported PyTorch ver:',torch.__version__,' elaT=%.1f sec,'%(time.time() - startT0))


#-------------------
#-------------------
#-------------------
class JanModel(nn.Module):  # For MNIST, CNN+FC
    def __init__(self,inp_dim,fc_dim,out_dim):
        super(JanModel, self).__init__()
        self.fc1 = nn.Linear(inp_dim,fc_dim)
        self.fc2 = nn.Linear(fc_dim,fc_dim)
        self.fc3 = nn.Linear(fc_dim,out_dim)
        
    def forward(self, x):
        #print('J: in',x.shape)        
        x = F.relu(self.fc1(x)) #; print('J: fc1',x.shape)        
        x = F.relu(self.fc2(x))
        x = self.fc3(x)         #; print('J: fc3',x.shape)        
        x=torch.tanh(x)         #; print('J: y',x.shape)        
        return x

#-------------------
#-------------------
#-------------------
class JanDataset(Dataset): # from input np-arrays
    def __init__(self, npX, npY):
        self.X=npX
        self.Y=npY
        self.num_eve=Y.shape[0]

    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.X[idx]
        y=self.Y[idx]
        sample =(x,y)
        return sample

    
# UTIL FUNCTIONS

# - - - - - - - -
def model_train( model, device,train_loader, loss_func, optimizer):
    model.train()
    train_loss=0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        lossOp = loss_func(output, target)
        #print('qq',lossOp,len(train_loader.dataset),len(train_loader)); ok55
        lossOp.backward()
        optimizer.step()
        train_loss +=lossOp.item()
    train_loss /= len(train_loader)
    return train_loss


# - - - - - - - -
def model_infer( model, device, test_loader,loss_func):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            lossOp=loss_func(output, target)
            #print('qq',lossOp,len(test_loader.dataset),len(test_loader)); ok55
            test_loss += lossOp.item()
    test_loss /= len(test_loader)
    return test_loss,data,target, output

#=================================
#=================================
#  M A I N 
#=================================
#=================================
inp_dim=280
fc_dim=140
out_dim=3

epochs=23
batch_size=4
steps=1000
num_eve=steps*batch_size
num_workers=4 # to load the data in parallel 
device='cpu'

# Initialize model 
model = JanModel(inp_dim,fc_dim,out_dim)

# Initialize optimizer
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# define loss function
loss_fn = nn.MSELoss()# Mean Squared Loss

# - - - -  DATA  PERP - - - - - 
print('\nM: generate data and train, num_eve=',num_eve)
Y=np.random.uniform(-0.8,0.8, size=(num_eve,out_dim)).astype('f')
print('M: done Y shape',Y.shape, Y.dtype,', one:',Y[0])
X=np.random.uniform(-2,2, size=(num_eve,inp_dim)).astype('f')
print('M: done X shape',X.shape)

# inject Y to X so the training will converge
X[:,:out_dim]=Y*0.3+np.random.normal(loc=0, scale=0.001, size=(num_eve,out_dim))

print('\nCreate pth-Dataset instance & test it')
trainDst=JanDataset(X,Y)
print('\nCreate pth-DataLoader instance & test it')
trainLdr = torch.utils.data.DataLoader(trainDst, batch_size=batch_size,
                        shuffle=True, num_workers=num_workers)

print('\n print one batch of training data ')
xx, yy = next(iter(trainLdr))
print('X:',xx.shape,'Y:',yy.shape)
print('Y[:,]',yy[:,0])
# - - - -  DATA ARE READY - - - - - 

print('\n= = = = Prepare for the treaining = = =\n')
print('\n\nM: torchsummary.summary(model):'); print(model)
from torchsummary import summary
inp_size=(inp_dim,) # input_size=(channels, H, W)) if CNN is first
summary(model,inp_size)

model=model.to(device) # re-cast model on device, data will be cast later

print('optimizer=',optimizer)
print('Train for epochs=',epochs)

startT=time.time()
for epoch in range(epochs):
    loss=model_train( model, device, trainLdr, loss_fn, optimizer)
    currLR=optimizer.param_groups[0]['lr']
    #scheduler.step(loss)  # Decay Learning Rate
    if epoch%1==0: print('M: epoch=%d LR=%.3g loss=%.3g, elaT=%.1f sec'%(epoch,currLR ,loss,time.time()-startT))

print('\nInfer for train data:')

loss,X1,Y1,Z1=model_infer( model, device, trainLdr,loss_fn)
print('infer : Average loss: %.4f  events=%d '% (loss,  len(trainLdr.dataset)))
print('sample predictions:')
for i in range(batch_size):
    print('y=',Y1[i].numpy(),'   z=',Z1[i].numpy(),'   diff=',(Y1[i]-Z1[i]).numpy())
