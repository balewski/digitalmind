#!/usr/bin/env python3

'''
 example of simple FC +regression
 a noisy Y is added to X so training  converges 
 uses DataLoader (aka generator) for input


To execute on AzureVM
export MYDATA=/datadrive/balewski/ ; export MYMIND=/home/balewski/digitalMind/ ; export MYVIS=/datadrive/balewski/popVisLog

gc-docker  -- -u balewski  -it  --shm-size 8G  --volume $MYVIS:/popVisLog --volume $MYDATA:/data --volume $MYMIND:/digitalMind balewski/torch_poplar-1.4.0:v3 bash

cd /digitalMind/graphcore/torch/solo/

- - - 

Scaling:  globalBS=const, global samples=const, LR=const
local BS=globalBS/world_size, localSamples=global samples/world_size

Reference convergence data:

**** num_ipu=1 ****
M: epoch=0  loss=0.0999, this epoch T=19.12 sec
M: epoch=1  loss=0.0072, this epoch T=0.34 sec
...
M: epoch=9  loss=0.00441, this epoch T=0.23 sec


**** num_ipu=4 ****
M: epoch=0  loss=0.106, this epoch T=18.58 sec
M: epoch=1  loss=0.00801, this epoch T=0.22 sec
...
M: epoch=9  loss=0.00444, this epoch T=0.23 sec

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time, sys
import copy

startT0 = time.time()
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset
from torchsummary import summary
import poptorch 

print('imported PyTorch ver:',torch.__version__,' elaT=%.1f sec,'%(time.time() - startT0))


#-------------------
#-------------------
#-------------------
class JanModel(nn.Module):  # For MNIST, CNN+FC
    def __init__(self,inp_dim,fc_dim,out_dim):
        super(JanModel, self).__init__()
        self.fc1 = nn.Linear(inp_dim,fc_dim)
        self.fc2 = nn.Linear(fc_dim,fc_dim)
        self.fc3 = nn.Linear(fc_dim,out_dim)
        self.loss = nn.MSELoss
        
    def forward(self, x, target=None):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x) 
        x = torch.tanh(x)
        # loss must be computed here to be executed on IPU
        if self.training:
            return x, self.loss()(x, target)
        return x

#-------------------
#-------------------
#-------------------
class JanDataset(Dataset): # from input np-arrays
    def __init__(self, npX, npY):
        self.X=npX
        self.Y=npY
        self.num_eve=Y.shape[0]

    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.X[idx]
        y=self.Y[idx]
        sample =(x,y)
        return sample

    
# UTIL FUNCTIONS

# - - - - - - - -
def model_train(model, train_loader, num_ipu):
    model.train()
    train_loss=0
    sumi = 0 
    for _, (data, target) in enumerate(train_loader):
        init = time.time()
        _, loss_op = model(data, target)
        #print("Tput: " + str(data.size()[0]/(time.time()-init)))
        #print("Data size: " + str(data.size()[0]))
        sumi += data.size()[0]
        train_loss += np.sum(loss_op.numpy())
    train_loss /= len(train_loader) * num_ipu
    return train_loss

#...!...!..................
def create_dataset(num_train,inp_dim,out_dim):
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-1,1, size=(num_train,out_dim)).astype('float32')
    print('M: done Y shape',Y.shape,Y.dtype,Y[0])

    X=np.random.uniform(-0.8,0.8, size=(num_train,inp_dim)).astype('float32')
    print('M: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))

    # inject Y to X so training can converge
    X[:,:out_dim]=Y*0.8+np.random.normal(loc=0, scale=0.02, size=(num_train,out_dim)).astype('float32')

    # return torch vectors as they will need to be casted somewhere because torch use this type of Tensors
    return torch.from_numpy(X), torch.from_numpy(Y)


#=================================
#=================================
#  M A I N 
#=================================
#=================================

num_ipu=4
epochs=2
learning_rate = 0.02

inp_dim=80
fc_dim=20
out_dim=10
ipu_step_iter=5


globBS=4*1024
localBS=globBS//num_ipu  # local batch size
steps=10*ipu_step_iter
num_eve=steps*globBS


if __name__ == '__main__':
    # Initialize model 
    model = JanModel(inp_dim,fc_dim,out_dim)
    print('\n\nM: torchsummary.summary(model):'); print(model)
    
    # Initialize optimizer
    #1optimizer = poptorch.optim.AdamW(model.parameters(), lr=learning_rate, weight_decay=0)
    optimizer = poptorch.optim.AdamW(model.parameters(), lr=learning_rate)

    # - - - -  DATA  PERP - - - - -
    print('\nM: generate data and train, num_eve=',num_eve)
    X,Y=create_dataset(num_eve,inp_dim,out_dim)

    print('\nCreate pth-Dataset instance')
    trainDst=JanDataset(X,Y)
    print('\nCreate pt-DataLoader instance & test it')

    #GC
    opts = poptorch.Options()
    opts.deviceIterations(ipu_step_iter) # Device "step"
    opts.replicationFactor(num_ipu) # How many IPUs to replicate over.
    opts.randomSeed(42) # force the same sequence on each IPU?
    trainLdr = poptorch.DataLoader(opts,trainDst, batch_size=localBS, num_workers=40, shuffle=True)

    # it can speed up some models
    loader = poptorch.AsynchronousDataAccessor(trainLdr)

    print('\n print one batch of training data ')
    xx, yy = next(iter(loader))
    print('X:',xx.shape,'Y:',yy.shape)
    print('Y[:,]',yy[:,0])

    # - - - -  DATA  READY - - - - - 
    # Create model for training which will run on IPU.
    training_model = poptorch.trainingModel(model, options=trainLdr.options, optimizer=optimizer)

    #input_size=(inp_dim,)
    #summary(model,input_size) # will print  <== make it work?

    print('Train for epochs=%d, local BS=%d world_size=%d, LR=%.3g,  local samples=%d'%(epochs,localBS,num_ipu,learning_rate,num_eve) )

    print('optimizer=', optimizer)
    print('Train for epochs=',epochs,' num_ipu=',num_ipu)

    startT0=time.time()
    startT1=startT0
    for epoch in range(epochs):
        loss = model_train( training_model, loader, num_ipu)
        startT2=time.time()
        if epoch%1==0 : print('M: epoch=%d  loss=%.3g, this epoch T=%.2f sec'%(epoch ,loss,startT2-startT1))
        startT1=startT2

    print('\nM: done training, elapsed T=%.2f sec, num_ipu=%d'%((time.time()-startT0),num_ipu))
