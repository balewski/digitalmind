#!/usr/bin/env python3

'''
 example of simple FC +regression
 model is used in both: training & inference mode to allow LR-schedule to be feed w/ valiadation loss computed on IPU
a noisy Y is added to X so training  converges 
 uses DataLoader (aka generator) for input

model is precompiled - it takes 20 sec, so the training time excludes this step

run the same code using poprun on a single IPU


$  poprun --num-instances=2 --num-replicas=2  --ipus-per-replica 1 trainVal_FC-dist_poprun.py

Tested for up to 16 IPUs and works w/ caching

= = = = = = 
Use of caching -  compile graph once, keep all previous versions usable
*) enable doCaching=1
*) first time graph compilation  takes [00:14<00:00], saves file to dir:
exec_cache/1234.popart: size 277MB
*) 2nd time graph compilation  takes ~2 sec
*) if you change: model,BS,or replica_steps_per_iter popart detects it and it will recompile the graph
*) all previously cached models are still available in exec_cache/
*) works also for multi-replica training w/ poprun


The weights initialisation: the seed is set differently on each instance, replicas will be initialised with different weights (since they are initialised randomly by default) which is problematic. One way to be sure that all instances get the same weights is to broadcast them with horovod at initialisation (it will require pip install horovod):

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import os, time, sys

import torch
import poptorch
import socket  # for hostname
from torchsummary import summary

import popdist.poptorch

import horovod.torch as hvd
from Util_CNN import MyModel,  MyModelWithLoss, MyDataset,train_one_epoch, create_dataset

#...!...!.................. # strange code, doe snot avearge loss???
def val_one_epoch(model2, dloader):
    model2.eval()
    loss=0
    for _, (data, target) in enumerate(dloader):
        _, loss_op = model2(data, target)
        loss += loss_op.numpy()
        #loss += np.sum(loss_op.numpy())
    loss /= len(dloader)
    t2 = time.time(); t12=t2-t1;
    #print('EO:dims',loss_op.shape,len(dloader),loss,model2.training);ok01
    return loss,t12




#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':

    doValid=1   # enable validation pass

    doCaching=1          # compile graph once, keep all previous graphs usable
    doPrecompileGraph=0  # separate graph compilation from graph execution
    doSaveLoadMode=0     # 1 crash: LowerToPopart::loadExecutableFromFil   

    epochs=5
    localBS=1024  # per replica   
    globNSamp=1024*100*8  # quick testing
    #globNSamp=1024*2000*10  # reference job last ~30 sec/epoch

    # 1 instance may manage many replicas
    replica_steps_per_iter=50  # per replica
    num_cpuWorker=8  # per instance
    
    host=socket.gethostname()
    device_id = popdist.popdist_core.getDeviceId()
    locReplicas = int(popdist.getNumLocalReplicas())
    total_replicas= int(popdist.getNumTotalReplicas())
    rank = popdist.getInstanceIndex() # index of the current instance 
    world_size = popdist.getNumInstances() # total number of instances
    replicas_per_rank=total_replicas//world_size
    ipus_per_replica=popdist.getNumIpusPerReplica()
    total_ipus=total_replicas*ipus_per_replica
    print("I am rank=%d of world=%d on host=%s, locReplias=%d devId %d totReplias=%d totIpus=%d repl/ranl=%d ipu/repl=%d"% (rank, world_size, host,locReplicas,device_id,total_replicas,total_ipus,replicas_per_rank,ipus_per_replica))
    verb=rank==0

    globBS=localBS*total_replicas  #  only for printout
    loaderBS=localBS*replica_steps_per_iter*replicas_per_rank # only for printout
    replNSamp=globNSamp//total_replicas

    steps=replNSamp//localBS  # only for printout
    instNSamp=globNSamp//world_size

    print('M:myRank=instance=',rank,'world_size =',world_size,'verb=',verb,'host=',host, 'verb=',verb,"IPU id:", device_id )
    if verb:
        print('imported PyTorch',torch.__version__,' PopTorch',poptorch.__version__)
        print("replicas per rank:", locReplicas)
        print("Total replicas number:", total_ipus)
        print('Train for epochs=%d, localBS=%d, loaderBS=%d steps=%d NSamp/replica=%d  instNSamp=%d  globNSamp=%d'%(epochs,localBS,loaderBS,steps,replNSamp,instNSamp,globNSamp) )
        goalLoaderNSamp=world_size * localBS * replicas_per_rank * replica_steps_per_iter
        print('sample sizes', globNSamp , goalLoaderNSamp) 
        assert globNSamp >= goalLoaderNSamp # too little global samples
        assert globNSamp % ( goalLoaderNSamp)==0 # OR some data will be lost

    # Initialize model
    inp_dim=80
    fc_dim=20
    out_dim=10
    learning_rate = 0.02

    # Initialize model 
    myModel = MyModel(inp_dim,fc_dim,out_dim)
    
    if verb:
        print('M: torchsummary.summary(model):'); print(myModel)
        if 1:
            input_size=(inp_dim,)
            summary(myModel,input_size) # will print  
    modelWloss = MyModelWithLoss(myModel)

    if world_size>1:
        hvd.init()
        print('M:horovod started, num ranks=%d, myRank=%d'%(hvd.size(),hvd.rank()))
        hvd.broadcast_parameters(modelWloss.state_dict(), root_rank=0)
        print('M:init wieights synch before eating the poptorch models ')


        
    # - - - -  DATA  PERP - - - - -
    if verb: print('\nM: generate data per instance instNSamp=%d  instance=rank=%d of %d'%(instNSamp,rank,world_size))
    X,Y=create_dataset(instNSamp,inp_dim,out_dim,rank) 
    if doValid:
        Xval,Yval=create_dataset(instNSamp,inp_dim,out_dim,rank) 

    if verb: print('\nCreate pth-Dataset instance')
    trainDst=MyDataset(X,Y)
    if doValid:
        valDst=MyDataset(Xval,Yval)
    if verb: print('Create DataLoaders, validation=',doValid,', model caching=',doCaching)

    opts = popdist.poptorch.Options()
    if doCaching:  
        opts.enableExecutableCaching('./exec_cache')
    opts.deviceIterations(replica_steps_per_iter) # Device "step"
    if world_size>1:
        opts.randomSeed(100+ device_id) #Why??? want droput seed to differ for each replica
    
    if verb: print('M: GC data-loader... ')
    # do 'manual' data partition
    trainLdr = poptorch.DataLoader(opts,trainDst, batch_size=localBS, num_workers=num_cpuWorker, shuffle=True,auto_distributed_partitioning=False)
    if doValid:
        valLdr = poptorch.DataLoader(opts,valDst, batch_size=localBS, num_workers=num_cpuWorker*2, shuffle=False, auto_distributed_partitioning=False)

    xx, yy = next(iter(trainLdr))  # needed on all ranks for compilations
    if verb:
        print('\nM: dump one batch of training data, instance loader size=niter=%d  replica steps/iter=%d'%(len(trainLdr),replica_steps_per_iter))
        print('per iter X:',xx.shape)#,'Y:',yy.shape)
        print('Y:',yy.shape)#,yy[:,0])
        loaderNSamp=xx.shape[0]*len(trainLdr)
        print('loader nSamp %d total'%loaderNSamp)
        assert loaderBS==xx.shape[0]
        #print('zz',loaderNSamp,world_size,globNSamp)
        assert loaderNSamp*replicas_per_rank*world_size==globNSamp # crashes for 2-2 , fix it later

    # - - - -  DATA  READY - - - - - 


    # Initialize optimizer
    # https://docs.graphcore.ai/projects/poptorch-user-guide/en/latest/reference.html#poptorch.optim.LAMB : Layer-wise Adaptive Moments (LAMB)
    optimizer = poptorch.optim.AdamW(myModel.parameters(), lr=learning_rate)
    #optimizer = poptorch.optim.LAMB(myModel.parameters(), lr=learning_rate)

    
    # Create models for training and for validation        
    model4train = poptorch.trainingModel(modelWloss, options=opts, optimizer=optimizer)
    if doValid:
        model4infer = poptorch.inferenceModel(modelWloss, options=opts)
        fix-it_see-checkpoint-predict
    
    if doSaveLoadMode:
        compF='my_model.poptorch'
        if rank==0:
            print('M:pre-compile model & save to %s ...'%compF)
            t1 = time.time()
            model4train.compileAndExport(compF,xx,yy)
            compTrain_sec = time.time()-t1
            print('M: compTrain=%.1f sec, save=%s'%(compTrain_sec,compF))

        modelWloss=None  # clear the pointer
        model4train=None
        # missing brarrier to wait for rank 1 to compile
        
        if 1:
            print('M:load pre-compile graph from %s ...'%compF)
            # https://docs.graphcore.ai/projects/poptorch-user-guide/en/latest/reference.html#poptorch.load
            t1 = time.time()
            model4train=poptorch.load(compF)
            compTrain_sec = time.time()-t1
            print('M: load train=%.1f sec, from%s'%(compTrain_sec,compF))
            modelWloss=fixMe11  # restory PyTorch model from saved file
            input_size=(inp_dim,)
            print('restored model for rank=',rank)
            summary(model,input_size) # will print  

    if doPrecompileGraph:
        print('M:pre-compile graph on rank %d  w/o saving ...'%rank)
        # https://docs.graphcore.ai/projects/poptorch-user-guide/en/latest/reference.html#poptorch.PoplarExecutor.compile
        t1 = time.time()
        model4train.compile(xx,yy)
        model4train.detachFromDevice()
        
        t2 = time.time()
        if doValid :
            model4infer.compile(xx,yy)
            model4infer.detachFromDevice()  # to allow treining pass being 1st
        t3 = time.time()
        
        txt='compile graph:  train=%.1f sec , infer=%.1f sec'%(t2-t1, t3-t2)            
        compTrain_sec = time.time()-t1
        print('M: '+txt)

    if verb:
        print('\nTrain for epochs=%d, localBS=%d globBS=%d  LR=%.3g,  sampl/replica=%dK of total %dK  doValid=%r'%(epochs,localBS,globBS,learning_rate,replNSamp//1024,globNSamp//1024,doValid),', modelCaching=',doCaching)

    startT0=time.time()
    startT1=startT0
    for epoch in range(epochs):
        t1 = time.time()
        train_loss=train_one_epoch(model4train, trainLdr)
        train_sec=time.time() -t1
        
        # WARN  Horovod should be used here to average losses between ranks
        
        if doValid: # enable validation pass
            t1 = time.time()
            model4train.detachFromDevice() #GC needed for model swap
            if model4infer._executable:  model4infer.attachToDevice() 
            attaInfer_sec = time.time()-t1

            val_loss,val_sec=val_one_epoch(model4infer, valLdr)

            t1 = time.time()
            model4infer.detachFromDevice() #GC needed for model swap back
            if model4train._executable:  model4train.attachToDevice() 
            attaTrain_sec = time.time()-t1
        else:
            val_loss=-1; val_sec=0; attaInfer_sec = 0; attaTrain_sec = 0
            
        startT2=time.time()
        if epoch%1==0 and verb :
            print('M: epoch=%d  LOSS train=%.3g val=%.3g, this epoch T=%.2f sec, rank=%d'%(epoch ,train_loss, val_loss,startT2-startT1,rank))
            print('M: epoch=%d  TIME(sec)  train=%.1f val=%.1f, attachInferM=%.2f   attachTrainM=%.2f\n'%(epoch ,train_sec, val_sec,attaInfer_sec,attaTrain_sec))
        startT1=startT2

    if verb: print('\nM: done training, elapsed T=%.2f sec, rank=%d'%(time.time()-startT0,rank))
