#!/usr/bin/env python3

'''
example of simple FC 
reads model
predicts

Single IPU

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import os, time, sys

import torch
import torch.nn as nn

import poptorch
import socket  # for hostname
import numpy as np
import popdist.poptorch

from Util_CNN import MyModel,  MyModelWithLoss, MyDataset, create_dataset, restore_checkpoint



#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':

    host=socket.gethostname()
    device_id = popdist.popdist_core.getDeviceId()
    locReplicas = int(popdist.getNumLocalReplicas())
    total_replicas= int(popdist.getNumTotalReplicas())
    rank = popdist.getInstanceIndex() # index of the current instance 
    world_size = popdist.getNumInstances() # total number of instances
    replicas_per_rank=total_replicas//world_size
    ipus_per_replica=popdist.getNumIpusPerReplica()
    total_ipus=total_replicas*ipus_per_replica
    print("I am rank=%d of world=%d on host=%s, locReplias=%d devId %d totReplias=%d totIpus=%d repl/ranl=%d ipu/repl=%d"% (rank, world_size, host,locReplicas,device_id,total_replicas,total_ipus,replicas_per_rank,ipus_per_replica))
    verb=rank==0

    doCaching=1          # compile graph once, keep all previous graphs usable
    localBS=1024// world_size  # per replica   
    globNSamp=1024*80
    outPath='out/' 
    
    # 1 instance may manage many replicas
    replica_steps_per_iter=5  # per replica
    num_cpuWorker=2  # per instance

    globBS=localBS*total_replicas  #  only for printout
    loaderBS=localBS*replica_steps_per_iter*replicas_per_rank # only for printout
    replNSamp=globNSamp//total_replicas
    
    steps=replNSamp//localBS 

    instNSamp=globNSamp//world_size

    print('M:myRank=instance=',rank,'world_size =',world_size,'verb=',verb,'host=',host, 'verb=',verb,"IPU id:", device_id )
    if verb:
        print('imported PyTorch ver:',torch.__version__)
        print("replicas per rank:", locReplicas)
        print("Total replicas number:", total_ipus)
        print(' model caching=',doCaching)
        print('Predict localBS=%d, loaderBS=%d steps=%d NSamp/replica=%d  instNSamp=%d  globNSamp=%d'%(localBS,loaderBS,steps,replNSamp,instNSamp,globNSamp) )
        goalLoaderNSamp=world_size * localBS * replicas_per_rank * replica_steps_per_iter
        #print('aa', globNSamp , goalLoaderNSamp) 
        assert globNSamp >= goalLoaderNSamp # too little global samples
        assert globNSamp % ( goalLoaderNSamp)==0 # OR some data will be lost

    # Initialize model
    inp_dim=80
    fc_dim=20
    out_dim=8
    learning_rate = 0.0002

    model = MyModel(inp_dim,fc_dim,out_dim)
    if verb: print('M: torchsummary.summary(model):'); print(model)
    modelWloss = MyModelWithLoss(model)
    
    # - - - -  DATA  PERP - - - - -
    if verb: print('\nM: generate data per instance instNSamp=%d  instance=rank=%d of %d'%(instNSamp,rank,world_size))
    X,Y=create_dataset(instNSamp,inp_dim,out_dim,rank)

    if verb: print('\nCreate pth-Dataset instance')
    trainDst=MyDataset(X,Y)
    if verb: print('Create pt-DataLoader instance ')

    popOpts = popdist.poptorch.Options()
    popOpts.deviceIterations(replica_steps_per_iter) # Device "step"
    if doCaching:  
        popOpts.enableExecutableCaching('./exec_cache')

    if verb: print('M:make data-loader... ')
    
    trainLdr = poptorch.DataLoader(popOpts,trainDst, batch_size=localBS, num_workers=num_cpuWorker, shuffle=True,auto_distributed_partitioning=False)

    
    if verb:
        print('\nM: dump one batch of training data, instance loader size=%d  replica steps/iter=%d'%(len(trainLdr),replica_steps_per_iter))
        xx, yy = next(iter(trainLdr))
        print('X:',xx.shape)#,'Y:',yy.shape)
        print('Y:',yy.shape)#,yy[:,0])
        loaderNSamp=xx.shape[0]*len(trainLdr)
        print('loader nSamp',loaderNSamp)
        assert loaderBS==xx.shape[0]
        assert loaderNSamp*world_size==globNSamp

    # - - - -  DATA  READY - - - - - 
  
    if verb:
        print('\nPredict localBS=%d globBS=%d  LR=%.3g,  sampl/replica=%dK of total %dK, world_size=%d'%(localBS,globBS,learning_rate,replNSamp//1024,globNSamp//1024,world_size) )

    outF='out/fc_check_epoch9.state.pth'  # partial training
    outF='out/fc_check_epoch14.state.pth'  # good training
    print("\n-----------  restore model for inference, state= ",outF)
    startEpoch=restore_checkpoint( outF, modelWloss)
    from torchsummary import summary
    input_size=(inp_dim,)
    summary(model,input_size) # will print 

    model4infer = poptorch.inferenceModel(modelWloss.eval(), options=popOpts)
    cpuLossF=nn.MSELoss(reduce=False)#returns a loss per element

    for j, (data, target) in enumerate(trainLdr):
        pred, loss_op = model4infer(data, target)
        loss=np.mean(loss_op.numpy())
        #print(j,'=j, type: target=',type(target),target.shape,'pred',type(pred),pred.shape)

        cpuLoss2D=cpuLossF(pred,target).numpy()
        #print(j,'=j, type: cpuLoss2D=',type(cpuLoss2D),cpuLoss2D.shape)
        cpuLossV=np.mean(cpuLoss2D,axis=1)
        #print(j,'=j, type: cpuLossV=',type(cpuLossV),cpuLossV.shape)
        cpuLoss=np.mean(cpuLossV)
        
        print('pred j=%d   ipuLoss=%.4f, cpuLoss=%.4f  Shapes: pred=%s, loss=%s, cpuLossV=%s'%(j,loss,cpuLoss,str(pred.shape),str(loss.shape),str(cpuLossV.shape)))

    print('M:done0')
    exit(0)
    i=0
    print('\nM:print one sample, i=',i)
    np.set_printoptions(precision=3)
    if 1:
        deltaV=np.array(target[i]-pred[i])
        print('target=',target[i])
        print('pred=  ',pred[i])
        print('delta= ',deltaV)
        print('sqr(L)=',np.sqrt(cpuLoss2D[i]))
        print('cpu L[i]= ',cpuLoss2D[i])
                
        print('cpuLoss=',cpuLossV[i],' sqrt(loss)=',np.sqrt(cpuLossV[i]),', avr std=',np.sqrt(np.mean(deltaV**2)))  # note: different from std(deltaV)
    print('M:done')

