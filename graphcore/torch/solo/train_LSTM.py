#!/usr/bin/env python3

'''
 example of simple LSTM+FC  +regression
 a noisy Y is added to X so training  converges 
 uses DataLoader (aka generator) for input

This is a single IPU code.


'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time, sys
import copy

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset
from torchsummary import summary
import poptorch
import socket  # for hostname

import popdist
import popdist.poptorch
from torch.autograd import Variable  #can be differentiated

from train_FC_dist_poprun import MyModelWithLoss

#-------------------
#-------------------
#-------------------
class JanModel2(nn.Module):
#...!...!..................
    def __init__(self, input_size, output_size, hidden_size,num_layers,fc_dim):
        super(JanModel2, self).__init__()

        self.num_layers = num_layers #number of layers
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(input_size=input_size, hidden_size=hidden_size,
                            num_layers=num_layers, batch_first=True)
        self.fc1 = nn.Linear(hidden_size,fc_dim )
        self.fc2 = nn.Linear(fc_dim, output_size)
        self.relu = nn.ReLU()
        self.loss = nn.MSELoss

    def forward(self, x): 
        #x=shape (BS,seq_len,inp_chan)
        #print('J: in',x.shape)
        bs=x.size(0)
        h_0 = Variable(torch.zeros(self.num_layers, bs, self.hidden_size)) #hidden state
        c_0 = Variable(torch.zeros(self.num_layers, bs, self.hidden_size)) #internal state
        # Propagate input through LSTM
        output, (hn, cn) = self.lstm(x, (h_0, c_0)) #lstm with input, hidden, and internal state
        hn = hn.view(-1, self.hidden_size) #reshaping the data for Dense layer next
        #print('J: hn',hn.shape,h_0.shape)
        out = self.relu(hn)
        out = self.fc1(out) #first Dense
        out = self.relu(out) #relu
        y = self.fc2(out) #Final Output
        return y


#-------------------
#-------------------
#-------------------
class JanDataset(Dataset): # from input np-arrays
    def __init__(self, npX, npY):
        self.X=npX
        self.Y=npY
        self.num_eve=Y.shape[0]

    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.X[idx]
        y=self.Y[idx]
        sample =(x,y)
        return sample

    
#= = = = = = = = =  UTIL FUNCTIONS  = = = = = = = = = = = = 

#...!...!..................
def model_train_1replica(model, train_loader):
    model.train()
    train_loss=0
    for _, (data, target) in enumerate(train_loader):        
        _, loss_op = model(data, target)
        # sumi += data.size()[0]
        train_loss += np.sum(loss_op.numpy())
    train_loss /= len(train_loader)
    return train_loss


#...!...!..................
def model_infer_1replica(model, dloader):
    model.eval()
    loss=0
    with torch.no_grad():
        for _, (data, target) in enumerate(dloader):
            pred, loss_op = model(data, target)
            loss += np.sum(loss_op.numpy())
            loss /= len(train_loader)
    return loss,data,target,pred

#...!...!..................
def create_waveform_dataset(num_train,seq_len,inp_chan,out_chan):
    ''' The input data: 
    X: chan=0: f(t): sine-wave snippet with random phase0 & ampl
    X: chan>0: white noise
    Y: f(tend+phi0), 2*f(tend+phi1), -3*f(tend+phi2)
    '''
    #assert inp_chan==1 # for now
    assert out_chan in [1,2,3]
    startT = time.time()
    seq2=2*seq_len
    t=np.linspace(0,4*np.pi,num=seq2)
    t0=np.random.uniform(2*np.pi,size=num_train)
    A=np.random.uniform(0.1,1.2,size=num_train)

    X=np.zeros((num_train,seq2,inp_chan),dtype=np.float32)
    Y=np.zeros((num_train,out_dim),dtype=np.float32)
    for i in range(num_train):
        X[i,:,0]=np.sin(t+t0[i])*A[i]
    
    Y[:,0]=X[:,int(seq_len*1.1),0]
    if out_chan>1: Y[:,1]=2*X[:,int(seq_len*1.2),0]
    if out_chan>2: Y[:,2]=-3*X[:,int(seq_len*1.3),0]

    # clip not needed values of X
    X=X[:,:seq_len]
    print('DS: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))
    print('DS: done Y shape',Y.shape,Y.dtype,Y[0])

    #GC: return torch vectors as they will need to be casted somewhere because torch use this type of Tensors
    return torch.from_numpy(X), torch.from_numpy(Y)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':

    host=socket.gethostname()
    device_id = popdist.popdist_core.getDeviceId()
    locReplicas = int(popdist.getNumLocalReplicas())
    total_replicas= int(popdist.getNumTotalReplicas())
    rank = popdist.getInstanceIndex() # index of the current instance 
    world_size = popdist.getNumInstances() # total number of instances
    replicas_per_rank=total_replicas//world_size
    ipus_per_replica=popdist.getNumIpusPerReplica()
    total_ipus=total_replicas*ipus_per_replica
    print("I am rank=%d of world=%d on host=%s, locReplias=%d devId %d totReplias=%d totIpus=%d repl/ranl=%d ipu/repl=%d"% (rank, world_size, host,locReplicas,device_id,total_replicas,total_ipus,replicas_per_rank,ipus_per_replica))
    verb=rank==0

    epochs=10
    learning_rate = 0.02


    seq_len=280
    #seq_len=1600 # like neuron-inverter
    inp_chan=2
    num_lstm_layers = 1 #number of stacked lstm layers
    fc_dim=8
    out_dim=3

    ipu_step_iter=5
    num_cpuWorker=8*1

    #globBS=256*4  # 23 sec for compilation
    globBS= 4 #quick compile
    localBS=globBS//total_replicas  # local batch size
    steps=50*ipu_step_iter
    #num_eve=steps*localBS # this is waht Jan wants
    num_eve=steps*globBS  # this is wat Alexandre suggested
    print('M:myRank=',rank,'world_size =',world_size,'verb=',verb,'host=',host, 'verb=',verb,"IPU id:", device_id )
    if verb:
        print('imported PyTorch ver:',torch.__version__)
        print("replicas per rank:", locReplicas)
        print("Total replicas number:", total_ipus)
        print('Train for epochs=%d, local BS=%d local samples=%d'%(epochs,localBS,num_eve) )

    # Initialize model 
    myModel = JanModel2(input_size=inp_chan, output_size=out_dim, hidden_size=12,num_layers=num_lstm_layers,fc_dim=fc_dim)
    
    if verb: print('M: torchsummary.summary(model):'); print(myModel)
    modelWloss = MyModelWithLoss(myModel)

    # Initialize optimizer
    optimizer = poptorch.optim.AdamW(modelWloss.parameters(), lr=learning_rate)

    # - - - -  DATA  PERP - - - - -
    print('\nM: generate data and train, num_eve=%d  myRank=%d'%(num_eve,rank))
    X,Y=create_waveform_dataset(num_eve,seq_len,inp_chan,out_dim)

    if verb: print('\nCreate pth-Dataset instance')
    trainDst=JanDataset(X,Y)
    if verb: print('Create pt-DataLoader instance & test it')

    opts = popdist.poptorch.Options()
    opts.deviceIterations(ipu_step_iter) # Device "step"
    opts.randomSeed(42) # force the same sequence on each IPU?
    if verb: print('M:make data-loader... ')
    trainLdr = poptorch.DataLoader(opts,trainDst, batch_size=localBS, num_workers=num_cpuWorker, shuffle=True)
    localSamp=len(trainLdr)*localBS*ipu_step_iter

    if verb:
        print('\n print one batch of training data, loader size=%d  replica step/iter=%d'%(len(trainLdr),ipu_step_iter))
        xx, yy = next(iter(trainLdr))
        print('X:',xx.shape)#,'Y:',yy.shape)
        print('Y:',yy.shape)#,yy[:,0])
        assert localBS*ipu_step_iter* replicas_per_rank==xx.shape[0]

    # - - - -  DATA  READY - - - - - 
    # Create model for training which will run on IPU.
    training_model = poptorch.trainingModel(modelWloss, options=trainLdr.options, optimizer=optimizer)  


    if verb:
        #print('optimizer=', optimizer)
        print('\nTrain for epochs=%d, local BS=%d  LR=%.3g,  local samples=%dK of total %dK'%(epochs,localBS,learning_rate,localSamp//1024,num_eve//1024) )

        
    startT0=time.time()
    startT1=startT0
    for epoch in range(epochs):
        loss = model_train_1replica( training_model, trainLdr)
        startT2=time.time()
        if epoch%1==0 and verb : print('M: epoch=%d  loss=%.3g, this epoch T=%.2f sec, rank=%d'%(epoch ,loss,startT2-startT1,rank))
        startT1=startT2

    if verb: print('\nM: done training, elapsed T=%.2f sec'%((time.time()-startT0)))

    exit(0)
    print('\nInfer for train data: WILL HANG')
    loss,X1,Y1,Z1=model_infer_1replica( training_model, trainLdr)
    print('infer : Average loss: %.4f  events=%d '% (loss,  len(trainLdr.dataset)))
    print('sample predictions:')
    for i in range(batch_size):
        print('y=',Y1[i].numpy(),'   diff=',(Y1[i]-Z1[i]).numpy()) #,'   z=',Z1[i].numpy()

