#!/usr/bin/env python3
"""
Testing poprun --num-replicas=N --num-instances=M --ipus-per-replica K
https://docs.graphcore.ai/projects/poprun-user-guide/en/latest/configuration.html
"""

import poptorch
import socket  # for hostname
import popdist
import popdist.poptorch



#=================================                                                                                  
#=================================                                                                                  #  M A I N                                                                                                         
#=================================                                                                                  
#=================================                                                                                   
if __name__ == '__main__':

    host=socket.gethostname()
    device_id = popdist.popdist_core.getDeviceId()
    locReplicas = int(popdist.getNumLocalReplicas())
    total_repli = int(popdist.getNumTotalReplicas()) 
    rank = popdist.getInstanceIndex() # index of the current instance 
    world_size = popdist.getNumInstances() # total number of instances
    ipus_per_repli=popdist.getNumIpusPerReplica()
    total_ipus=total_repli*ipus_per_repli
    localBS=10
    instBS=localBS*world_size
    #globBS=
    print("I am rank=%d of world=%d on host=%s, locRepli=%d devId %d totRepli=%d totIpu=%d ipu/repl=%d"% (rank, world_size, host,locReplicas,device_id,total_repli,total_ipus,ipus_per_repli))
    
    #if rank==0: print('done')


