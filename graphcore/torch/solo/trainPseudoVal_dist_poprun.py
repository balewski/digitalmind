#!/usr/bin/env python3

'''
Use Horovod for sync
Use mdoel4train w/ LR=0 as pseudo-inference (ignore non-zero drop fract)

= = = = = = 
 example of simple FC +regression
 a noisy Y is added to X so training  converges 
 uses DataLoader  which serves all input samples (,auto_distributed_partitioning=False)
 code keeps constant:
  * global number of samples
  * local BS (per replica)
  
This multi-IPU code.

This code depends on  train_FC-dist_poprun.py

m=2
poprun --num-instances=$m  --num-replicas=$m  trainPseudoVal_dist_poprun.py 


'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time, sys

import torch
import poptorch
import socket  # for hostname

import popdist.poptorch

from Util_CNN import MyModel,  MyModelWithLoss, MyDataset,train_one_epoch, create_dataset
    

# UTIL FUNCTIONS

#...!...!..................


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':

    host=socket.gethostname()
    device_id = popdist.popdist_core.getDeviceId()
    locReplicas = int(popdist.getNumLocalReplicas())
    total_replicas= int(popdist.getNumTotalReplicas())
    rank = popdist.getInstanceIndex() # index of the current instance 
    world_size = popdist.getNumInstances() # total number of instances
    replicas_per_rank=total_replicas//world_size
    ipus_per_replica=popdist.getNumIpusPerReplica()
    total_ipus=total_replicas*ipus_per_replica
    print("I am rank=%d of world=%d on host=%s, locReplias=%d devId %d totReplias=%d totIpus=%d repl/ranl=%d ipu/repl=%d"% (rank, world_size, host,locReplicas,device_id,total_replicas,total_ipus,replicas_per_rank,ipus_per_replica))
    verb=rank==0

    
    doValid=1   # enable pseudo-validation pass
    doCaching=1        # compile graph once, keep all previous graphs usable
    epochs=15
    localBS=1024/world_size  # per replica   
    globNSamp=1024*80

    # 1 instance may manage many replicas
    replica_steps_per_iter=5  # per replica
    num_cpuWorker=8  # per instance

    # Initialize model
    inp_dim=80
    fc_dim=20
    out_dim=10
    learning_rate = 0.0002


    globBS=localBS*total_replicas  #  only for printout
    loaderBS=localBS*replica_steps_per_iter*replicas_per_rank # only for printout
    replNSamp=globNSamp//total_replicas
    
    steps=replNSamp//localBS 

    instNSamp=globNSamp//world_size

    print('M:myRank=instance=',rank,'world_size =',world_size,'verb=',verb,'host=',host, 'verb=',verb,"IPU id:", device_id )
    if verb:
        print('imported PyTorch ver:',torch.__version__)
        print("replicas per rank:", locReplicas)
        print("Total replicas number:", total_ipus)
        print(' model caching=',doCaching)
        print('Train for epochs=%d, localBS=%d, loaderBS=%d steps=%d NSamp/replica=%d  instNSamp=%d  globNSamp=%d'%(epochs,localBS,loaderBS,steps,replNSamp,instNSamp,globNSamp) )
        goalLoaderNSamp=world_size * localBS * replicas_per_rank * replica_steps_per_iter
        #print('aa', globNSamp , goalLoaderNSamp) 
        assert globNSamp >= goalLoaderNSamp # too little global samples
        assert globNSamp % ( goalLoaderNSamp)==0 # OR some data will be lost


    opts = popdist.poptorch.Options()
    opts.deviceIterations(replica_steps_per_iter) # Device "step"
    opts.randomSeed(42+device_id) #I want it different droput per replica, must sync initial weights
    if doCaching:  
        opts.enableExecutableCaching('./exec_cache')


    model = MyModel(inp_dim,fc_dim,out_dim)
    
    # Initialize optimizer
    optimizer = poptorch.optim.AdamW(model.parameters(), lr=learning_rate)
    fakeOptimizer = poptorch.optim.AdamW(model.parameters(), lr=0., betas=(0.999,0.999), weight_decay=0) # it will not modify weights

    if verb:
      for x in ['lr','betas','weight_decay']:
        optimizer.variable_attrs.markAsVariable(x)
        fakeOptimizer.variable_attrs.markAsVariable(x)
        print('optim attr:',x,' isConst:',optimizer.variable_attrs.isConstant("x"))


    if verb: print('M: torchsummary.summary(model):'); print(model)

    # GC -speciffic
    modelWloss = MyModelWithLoss(model)
    
    if world_size>1:
        import horovod.torch as hvd
        hvd.init()
        if verb: print('M:horovod started, num ranks=%d, myRank=%d'%(hvd.size(),hvd.rank()))
        hvd.broadcast_parameters(model.state_dict(), root_rank=0)
        if verb: print('M:init wieights synch before eating the poptorch models ')
    else:
        hvd=None

    # Create model for training which will run on one IPU.
    model4train = poptorch.trainingModel(modelWloss, options=opts, optimizer=optimizer)  

    
    # - - - -  DATA  PERP - - - - -
    if verb: print('\nM: generate data per instance instNSamp=%d  instance=rank=%d of %d'%(instNSamp,rank,world_size))
    if verb: print('M:make data-loader, validation=',doValid,', model caching=',doCaching)
    if verb: print('\nCreate train Dataset instance')
    
    X1,Y1=create_dataset(instNSamp,inp_dim,out_dim,rank, verb=verb)
    trainDataset=MyDataset(X1,Y1)
    #GC: we can deal 'manually' with the dataset partitioning by disabling the automatic dataset partitioning of the Dataloader 
    #     each instance only create a partition of size num_eve/M (M being the number of instance
    #     each instance will further re-distrubut the data over N/M replicas (if N>M)
    if verb: print('Create train DataLoader instance ')
    trainLoader = poptorch.DataLoader(opts,trainDataset, batch_size=localBS, num_workers=num_cpuWorker, shuffle=True,auto_distributed_partitioning=False)

    if doValid:
        if verb: print('\nCreate traival DataLoader')
        X2,Y2=create_dataset(instNSamp,inp_dim,out_dim,rank, verb=verb) 
        valDataset=MyDataset(X2,Y2)
        valLoader = poptorch.DataLoader(opts,valDataset, batch_size=localBS, num_workers=num_cpuWorker, shuffle=False, auto_distributed_partitioning=False)
        next(iter(valLoader)) # needed to un-hang validation pass
 
    if verb :
        xLoader=trainLoader
        print('\nM: dump one batch of training data, instance loader size=%d  replica steps/iter=%d'%(len(xLoader),replica_steps_per_iter))
        xx, yy = next(iter(xLoader))
        print('X:',xx.shape)#,'Y:',yy.shape)
        print('Y:',yy.shape)#,yy[:,0])
        loaderNSamp=xx.shape[0]*len(xLoader)
        print('loader nSamp',loaderNSamp)

    # - - - -  DATA  READY - - - - - 
 
    if verb:
        print('\nTrain for epochs=%d, localBS=%d globBS=%d  LR=%.3g,  sampl/replica=%dK of total %dK, world_size=%d'%(epochs,localBS,globBS,learning_rate,replNSamp//1024,globNSamp//1024,world_size) )
        if doValid:
            print(' local samples: train=%d  valid=%d'%(len(trainDataset),len(valDataset)))

    startT=time.time()
    for epoch in range(epochs):
        t1 = time.time()
        loss_train = train_one_epoch( model4train, trainLoader,hvd)
        t2 = time.time()
        if doValid :  # disable weights update           
            model4train.setOptimizer(fakeOptimizer) # AdamW w/ LR-0
            loss_val = train_one_epoch( model4train, valLoader,hvd)            
            model4train.setOptimizer(optimizer)
        else:
            loss_val =0
        t3 = time.time()

        #print('M: epoch=%d   loss=%.5g,  rank=%d'%(epoch ,loss_train,rank))
            
        if epoch%1==0 and verb : print('  M: epoch=%d   Loss: train=%.3g  val=%.3g, this epoch T=%.2f sec, rank=%d LR=%.2e'%(epoch ,loss_train,loss_val,t3-t1,rank,optimizer.param_groups[0]['lr']))
        if doValid and verb>1:
            print('M: time split:  %.2f %.2f %.2f'%(t2-t1,t3-t2,t3-t1))
            
    if verb: print('\nM: done training, elapsed T=%.2f sec'%((time.time()-startT)))
