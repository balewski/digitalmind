#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

#outPath=/global/cscratch1/sd/balewski/test1/squareRegrData4/
outPath=data1
echo path=$outPath
mkdir -p $outPath

numEve=10000

for k in `seq 1 5`; do
    ./create_SquareRegrData.py --noise 0.10 --events $numEve --name data_c$k --dataPath $outPath -X
    #exit
done

