#!/usr/bin/env python3
"""
Runs on CPU,  1 GPU, or multipl GPUs
Uses TCP/IP for communication

 read input hd5 tensors
uses self.layer = nn.ModuleList()
uses  LambdaLayer, ReduceLROnPlateau
train net
write net + weights as HD5

SEE ALSO README !!!

Testing:
Interactive 2x2 GPU:
   salloc -N2  -C gpu  -c 10  --gres=gpu:2 --ntasks-per-node=2  -t4:00:00 

   line=`ifconfig |grep 'ib1:' -A1 |tail -n1`
   headIp=`echo $line | cut -f2 -d\ `
   export MASTER_ADDR=$headIp
   export MASTER_PORT=8888
   echo headIP=$headIp

   run on 1-8 GPUs preserving total samples and global BS:

srun -n1 -l python -u ./train_SquareRegr.py --device cuda -n40000 --localBS  512 -e 30
srun -n2 -l python -u ./train_SquareRegr.py --device cuda -n20000 --localBS  256  -e 30
srun -n4 -l python -u ./train_SquareRegr.py --device cuda -n10000 --localBS  128  -e 30
srun -n8 -l python -u ./train_SquareRegr.py --device cuda -n5000 --localBS  64  -e 30

1 GPU, 7 k fr/sec, loss 4.4e-4 early stop epoch 15 , wall time 1.4 min
2 GPU, 14 k fr/sec, loss 5.8e-4 early stop epoch 15 , wall time 0.71 min
4 GPU, 19 k fr/sec, loss 5.7e-4 early stop epoch 13, wall time 0.31 min
8 GPU, 25 k fr/sec, loss 1.6e-3 early stop epoch 12


Predict:
      srun -n1 -l python -u ./predict_SquareRegr.py --device cuda ??

GRAPHCORE:

  export MYDATA=/datadrive/balewski/ ; export MYMIND=/home/balewski/digitalMind/ ; export MYVIS=/datadrive/balewski/popVisLog 
  gc-docker  -- -u balewski  -it --shm-size 8G --volume $MYVIS:/popVisLog --volume $MYDATA:/data --volume $MYMIND:/digitalMind balewski/torch_poplar-1.4.0:v3 bash

cd /digitalMind/graphcore/torch/squareRegression_dist_iofree

CPU-based training
./train_SquareRegr.py  -n5000 --localBS  64  -e 30
1.1k fr/sec, loss = 8.6e-4, early stop at 20 epochs

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from pprint import pprint 

import sys,os
sys.path.append(os.path.relpath("toolbox/"))
from Plotter_SquareRegr import Plotter_SquareRegr
from Deep_SquareRegr import Deep_SquareRegr
from Util_IOfunc import  read_yaml,write_yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--design', dest='modelDesign',  default='cnn1', help=" model design of the ML-network")
    parser.add_argument("--dataTag",  default='december5',help="data config name")
    parser.add_argument('--device',  default='cpu', choices=['cpu','cuda'],  help=' hardware to be used, multi-gpus not supported')
    parser.add_argument("--seedWeights", default=None, help="seed weights only, after model is created") # not tested

    parser.add_argument("--designPath",help="location of hpar.yaml defining the model",  default='./')
    parser.add_argument("--dataPath",  default='data1/',help="training data path")

    parser.add_argument("--outPath", default='out',help="output path for plots and tables")

    parser.add_argument("-n", "--localSamples", type=int, default=2000, help="samples to read")

    parser.add_argument("-e", "--epochs", type=int, default=6, help="fitting epoch")
    parser.add_argument("-b", "--localBS", type=int, default=None, help="change localBS, default --> hparams")
    parser.add_argument("--dropFrac", type=float, default=None, help="drop fraction at all FC layers, default --> hparams")
    parser.add_argument("-H", "--historyPeriod", type=int, default=5, help="in epochs, compute & save train+val loss.")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',  action='store_true', default=False, help="disables X-term for batch mode")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],  help="increase output verbosity", default=1, dest='verb')

    args = parser.parse_args()
    args.prjName='squareRegr'
    if args.device!='cuda':
        for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.seedWeights=='same' :  args.seedWeights=args.outPath

deep=Deep_SquareRegr.trainer(args)
deep.init_dataLoaders()

if deep.myRank==0 : 
    plot=Plotter_SquareRegr(args ,deep.metaD )
    dom='val'
    xx, yy, _ = next(iter(deep.dataLoader[dom]))
    dataA={dom+'_image_X':xx,dom+'_unit_Y':yy}
    plot.frames(dataA,dom)
    #plot.display_all('train')
    
deep.build_model()

if args.seedWeights: deep.load_model_state()
deep.train_model()
if deep.myRank!=0:  exit(0)  # stop all other ranks here

deep.save_model_full() 
write_yaml(deep.sumRec, deep.outPath+'/'+deep.prjName2+'.sum_train.yaml')

if args.verb>1: pprint(deep.sumRec)

plot.train_history(deep,args) 
plot.display_all('train')
