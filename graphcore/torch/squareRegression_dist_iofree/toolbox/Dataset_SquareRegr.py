__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
reads all data at once and serves them from RAM
- optimized for mult-GPU training
- only used block of data  from each H5-file
- reads data from multiple files

    Shuffle: only  all samples after compleated read

'''

import time,  os
import random
import h5py
import numpy as np

import copy
from torch.utils.data import Dataset, DataLoader


#-------------------
#-------------------
#-------------------
class Dataset_SquareRegr(Dataset):
    def __init__(self, conf0,verb=1):
        self.conf=copy.deepcopy(conf0)  # the input conf0 is reused later in the upper level code
        self.verb=verb
        dataL=self.conf['dataList']
        numH5=len(dataL)
        assert numH5>=1
        self.conf['numSamplesPerH5']=self.conf['numLocalSamples']//numH5
        self.numLocFrames=self.conf['numSamplesPerH5']*numH5 # can be < numLocalSamples
        assert self.conf['numSamplesPerH5']>=1
        self.cnt=0
        if self.verb and 0:
            print('\nDS-cnst name=%s  shuffle=%r BS=%d steps=%d myRank=%d numSampl/cell=%d'%(self.conf['name'],self.conf['shuffle'],self.localBS,self.__len__(),self.conf['myRank'],self.conf['numSamplesPerH5']),'H5-path=',self.conf['dataPath'])
        assert self.numLocFrames>0
        assert self.conf['myRank']>=0

        if self.verb>1:
            print('DS: use %d dataL:'%(len(dataL)),dataL)

        # malloc storage for the data, to avoid concatenation cost
        self.data_frames=np.zeros((self.numLocFrames,)+tuple(self.conf['inputShape']),dtype='float32')
        self.data_parU=np.zeros((self.numLocFrames,self.conf['outputSize']),dtype='float32')
        if self.conf['x_y_aux']:
            self.data_parP=np.zeros((self.numLocFrames,self.conf['outputSize']),dtype='float32')

        idxA=np.arange(self.numLocFrames)
        # RAM-efficient pre-shuffling of target indexes
        if self.conf['shuffle']:
            np.random.shuffle(idxA)
            if self.verb: print('IG:pre-shufle all local %d frames'%self.numLocFrames)

        # prime this generator  ... takes sub-sec/file
        startTm0 = time.time()
        for ic in range(numH5):
            numSamp=self.conf['numSamplesPerH5']
            idxOff=ic*numSamp
            goalIdxL=idxA[idxOff:idxOff+numSamp]
            self.openH5(dataL[ic],goalIdxL,ic)

        startTm1 = time.time()

        if self.verb :
            print(' DS:load-end of all HD5, read time=%.2f(sec) name=%s numH5=%d, numLocSamp=%d, numLocSamp/cell=%d '%(startTm1 - startTm0,self.conf['name'],numH5,self.numLocFrames,self.conf['numSamplesPerH5']))
            print(' DS:Xall',self.data_frames.shape,self.data_frames.dtype)
            print(' DS:Uall',self.data_parU.shape,self.data_parU.dtype)
            if self.conf['x_y_aux']:
                print(' DS:Pall',self.data_parP.shape,self.data_parP.dtype)

#...!...!..................
    def sanity(self,localBS):
        stepPerEpoch=int(np.floor( self.numLocFrames/ localBS))
        if  stepPerEpoch <1:
            print('\nDS:ABORT, Have you requested too few samples per rank?, numLocFrames=%d, BS=%d  name=%s'%(self.numLocFrames, localBS,self.conf['name']))
            exit(67)
        # all looks good
        
#...!...!..................
    def openH5(self,fileH5N,goalIdxL,ic):
        fnameTmpl=self.conf['dataPath']+self.conf['h5nameTemplate']
        inpF=fnameTmpl.replace('*',fileH5N)
        numSamp=self.conf['numSamplesPerH5']
        dom=self.conf['domain']

        pr=self.verb>0 and (ic<10 or ic%20==0)
        if self.verb>1 : print('DS:fileH5 %s name=%s, idxOff[0..3]='%(fileH5N,self.conf['name']),goalIdxL[:3],'inpF=',inpF)
        if not os.path.exists(inpF):
            print('FAILD, missing HD5',inpF)
            exit(22)

        doAux= self.conf['x_y_aux']
        startTm0 = time.time()

        if pr :
            print('DS:read numSamp=%d data from %d hdf5:%s'%(numSamp,ic,inpF),', doAux=%r '%(doAux))

        # = = = READING HD5  start
        h5f = h5py.File(inpF, 'r')
        Xshape=h5f[dom+'_image_X'].shape
        totSamp=Xshape[0]
        maxShard=totSamp//numSamp
        if  maxShard <1:
            print('\nABORT, Have you requested too many samples per rank?, one fileH5 Xshape:',Xshape,'name=',self.conf['name'])
            exit(66)
        # chosen shard is rank dependent, wraps up if not sufficient number of ranks
        myShard=self.conf['myRank'] %maxShard
        sampIdxOff=myShard*numSamp
        if pr: print('DS:file myShard=%d, maxShard=%d, sampIdxOff=%d, file:'%(myShard,maxShard,sampIdxOff),'X-fileH5:',Xshape)

        # data reading starts
        self.data_frames[goalIdxL]=h5f[dom+'_image_X'][sampIdxOff:sampIdxOff+numSamp]
        self.data_parU[goalIdxL]=h5f[dom+'_unit_Y'][sampIdxOff:sampIdxOff+numSamp]
        if doAux:
            self.data_parP[goalIdxL]=h5f[dom+'_phys_Y'][sampIdxOff:sampIdxOff+numSamp]
        h5f.close()
        # = = = READING HD5  done

        if pr :
            startTm1 = time.time()
            print(' hd5 read time=%.2f(sec) dom=%s fileH5=%s'%(startTm1 - startTm0,dom,fileH5N))

        # .......................................................
        #.... data embeddings, transformation should go here ....


        #.... end of embeddings ........
        # .......................................................


    def __len__(self):        
        return self.numLocFrames


    def __getitem__(self, idx):
        # print('DSI:',idx,self.conf['name'],self.cnt); self.cnt+=1
        #assert idx>=0
        #assert idx< self.numLocFrames
        X=self.data_frames[idx]
        Y=self.data_parU[idx]
        if self.conf['x_y_aux']: # predictions for Roy
            AUX=self.data_parP[idx]
            return (X,Y,AUX)
        return (X,Y,Y)


