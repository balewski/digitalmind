__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import time,  os
import ruamel.yaml  as yaml
from scipy.stats import stats # for Pearson correlation

#...!...!..................
def get_rho_correl(aV,bV):
        n=len(aV)
        assert n >2
        assert n == len(bV)
        rho,p_val=stats.pearsonr(aV,bV)
        #print(n,rho,p_val)
        # based on  https://stats.stackexchange.com/questions/226380/derivation-of-the-standard-error-for-pearsons-correlation-coefficient

        sigRho=np.sqrt( (1-rho*rho)/(n-2))
        #print('rho=',rho,'sigRho=',sigRho)
        return rho,sigRho

#...!...!..................
def get_UmZ_correl(UmZ,metaD):
        #print('a dims:',a.shape,'\n dump a:')

        # compute cov matrix
        c=np.cov(UmZ,rowvar=False)
        nv=c.shape[0]
        nvL=range(nv)
        #print('cov M:')
        #pprint(c)
        c1=np.diagonal(c)
        c2=np.sqrt(c1)
        print('one std dev:',c2)
        avrSig=c2.mean()
        print('global  residua avr=%.3f  var=%.3f '%(avrSig, avrSig*avrSig))

        outputNameL=metaD['dataInfo']['outputName']
        print('\nnames ',end='')
        [ print('     %s'%outputNameL[i],end='') for i in nvL]

        print('\nresidu: ',end='')
        [ print(' U%d-> %4.1f%s  '%(i,100*c2[i],chr(37)),end='') for i in nvL ]

        lossMAE=np.absolute(c2).mean()
        c3=np.power(c2,2);    lossMSE=c3.mean()
        print('\n global lossMSE=%.4f , lossMAE=%.3f (computed)'%( lossMSE,lossMAE))

        #print('\ncorrelation matrix for %d dim vector'%nv)
        r=np.corrcoef(UmZ,rowvar=False)
        #pprint(r)

        print('\nnames  ',end='')
        [ print('   %s '%outputNameL[i],end='') for i in nvL]
        print('\ncorrM  ',end='')
        [ print('  __var%1d__ '%i,end='') for i in nvL]
        for k in nvL:
            print('\n var%1d '%k,end='')
            [ print('     %6.3f'% r[i][k],end='') for i in range(k+1)]
            #print('      #  '*(nv-k),end='')
        print()
        return  lossMSE


#...!...!..................
def insert_ghost(base_a,pos_x,pos_y,frame):
        off=int(base_a/4)
        for i in range(base_a):
           #if i<10: print(i,pos_y,pos_x+i)
           # remember, plotter flips the Y-axis
           frame[pos_y+off,pos_x+i]=1  # bottom
           frame[pos_y+i,pos_x+off]=1  # left
           frame[pos_y+i,pos_x+base_a-off]=1  # right
           frame[pos_y+base_a-off,pos_x+i]=1  # top

#...!...!..................
def insert_symbol(base_a,pos_x,pos_y,frame):
        for i in range(base_a):
           #if i<10: print(i,pos_y,pos_x+i)
           # remember, plotter flips the Y-axis
           frame[pos_y,pos_x+i]=1  # bottom
           frame[pos_y+i,pos_x]=1  # left
           frame[pos_y+i,pos_x+base_a-1]=1  # right
           frame[pos_y+base_a-1,pos_x+i]=1  # top

#...!...!..................
def produce_oneFrame(metaD):
        nPar=len(metaD['outputName'])
        mx_x=metaD['phys_range'][0][1]
        mx_y=metaD['phys_range'][1][1]
        base_a=metaD['square_base']

        # generate  params normalized to unity
        U=np.random.uniform(-1,1,size=nPar)
        
        u2p=np.array(metaD['unit_2_phys'])
        #print('u2p',u2p)
        P=U*u2p[:,0] + u2p[:,1]
        pos_x=int(P[0])
        pos_y=int(P[1])
        #print('P=',P)
        
        frame=np.zeros( (mx_y,mx_x) )
        insert_symbol(base_a,int(pos_x),int(pos_y),frame)
        insert_ghost(base_a,int(mx_x-pos_x),int(mx_y-pos_y),frame)

        # add pepper-noise
        frame+= np.random.uniform(size=(mx_y,mx_x)) <metaD['pepper_noise']
        
        return U,P,frame

#...!...!..................
def create_frames(parD,args):        
        
        # fill meta-data record
        metaD={}
        mx_x=parD['const']['max_x']
        mx_y=parD['const']['max_y']
        metaD['phys_range']=[[0,mx_x], [0,mx_y]]
        metaD['outputName']=parD['outputName']
        metaD['outSize']=len(parD['outputName'])
        metaD['unit_2_phys']=[]
        base_a=parD['const']['base_a']
        metaD['square_base']=base_a
        metaD['pepper_noise']=args.noise
        for mxv in [mx_x, mx_y]:
                fact=(mxv - 2.05 * base_a)/2.
                off=(mxv - 0.05 * base_a)/2.
                metaD['unit_2_phys'].append([fact,off])
        print('unit_2_phys:',metaD['unit_2_phys'])
        print(metaD)
        
        
        start = time.time()
        # data split 1/1/8
        ntest=args.events//10
        domD={'test':ntest,'val':ntest,'train':ntest*8}
        print('domD',domD)
        out={}
        for dom in domD:
            out.update({dom+'_image_X':[],dom+'_phys_Y':[],dom+'_unit_Y':[]})
            for i in range(domD[dom]):
                uy,py,x=produce_oneFrame(metaD)
                if i%500==0:
                    print(dom,'gen i=',i,' Y unit, phys:',uy,py)
                out[dom+'_unit_Y'].append(uy)
                out[dom+'_phys_Y'].append(py)
                out[dom+'_image_X'].append(x)

        for item in out:
            out[item]=np.array(out[item]).astype(np.float32)
 
        print('%d frames produced, elaT=%.1f sec'%(args.events,(time.time() - start)))
        return out, metaD
