import tensorflow.compat.v1 as tf
import time
import numpy as np

#...!...!..................
def save_stepRecords(stepRecord,csvF):
    speedA=[]
    step0=-1
    fd=open(csvF,'w')
    fd.write('elaT,stepCnt,loss\n')
    for [elaT,step,loss] in stepRecord:
        fd.write('%.6g,%d,%.3g\n'%(elaT,step,loss))            
        if step0>0:
            speed=(step-step0)/(elaT-elaT0)
            speedA.append(speed)
        if step>0:
            step0=step
            elaT0=elaT
    fd.close()
    print('saved: ',csvF)
    
    sA=np.array(speedA)
    if sA.shape[0]<2 : return
        
    resM=float(sA.mean())
    resS=float(sA.std())
    tit2=' avr:%.1f rms:%.1f (steps/sec)'%(resM,resS)
    
    print('SSR_END speed %s  saved to: %s'%(tit2,csvF))

    return
    resM*=args.batch_size
    resS*=args.batch_size
    tit3=' avr:%.1f rms:%.1f (samples/sec)'%(resM,resS)
    print('SSR_END speed %s  num rec=%d \n'%(tit3,sA.shape[0]))
        



# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - -
#based on:
#  https://stackoverflow.com/questions/52512282/setting-variables-in-a-tf-estimator-training-hook
class TfTrainHook(tf.train.SessionRunHook):
    def __init__(self, name, mode,frequency): #1 variable_name
        #1 variable name should be like: parent/scope/some/path/variable_name:0
        self.variable_name = "mean_squared_error/value:0"
        
        self.frequency = max(1,frequency)
        self.stepRecord=[]
        self.mode=mode
        self.prjName=name

#...!...!..................
    def begin(self):
        self.global_step_tensor = tf.train.get_global_step()
        print('\nTTH:begin',time.time(),self.mode,self.frequency)
        self.time0=time.time()

#...!...!..................
    def after_create_session(self, sess, coord):
        #op = sess.graph.get_operations()
        #print('dump ops',[m.values() for m in op])
        
        self.variable_tensor = sess.graph.get_tensor_by_name(self.variable_name)

        timeRec=[time.time()-self.time0,-2,-2]
        self.stepRecord.append(timeRec)
        print('\nTTH:after_create_session',time.time())

#...!...!..................        
    def after_run(self, run_context, run_values): # for *every* step
        global_step = run_context.session.run(self.global_step_tensor)
        loss=run_context.session.run(self.variable_tensor)
        if global_step<5:
            print('TTH:after_run',time.time(),global_step,loss,self.mode)
        if global_step % self.frequency == 0:
            timeRec=[time.time()-self.time0,global_step,loss]
            self.stepRecord.append(timeRec)
            #print('\nVUH:after_run',time.time(),'timeRec',timeRec)
            #new_variable_value = complicated_algorithm(...)
            #assign_op = self.variable.assign(new_variable_value)
            #run_context.session.run(assign_op)
#...!...!..................
    def end(self,session):
        print('\nTTH:end',time.time(),'end-rec:',self.stepRecord[-1],' num rec=', len(self.stepRecord))
        csvF='%s-%s.stepRecord.csv'%(self.prjName,self.mode)

        save_stepRecords(self.stepRecord,csvF)
        totTime=self.stepRecord[-1][0]
        initT=self.stepRecord[1][0]
        print('\nTTH_END graph init time=%.2f (min),  totalTime %.2f (min)'%(initT/60.,totTime/60.))


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - -

class IpuTrainingHook(tf.estimator.SessionRunHook):
    """Logs loss"""
    
    def __init__(self, name, mode):
        super().__init__()
        from tensorflow.python import ipu
        self._outfeed = ipu.ipu_outfeed_queue.IPUOutfeedQueue(feed_name="logging")
        self.stepRecord=[]
        self.mode=mode
        self.prjName=name
        self.myGlobStep=-1

    def enqueue(self, tensor):
        return self._outfeed.enqueue(tensor)

    def begin(self):
        self.tensor = self._outfeed.dequeue()
        print('\nITH:begin',time.time(),self.mode)
        self.myGlobStep=0
        self.time0=time.time()

#...!...!..................
    def after_create_session(self, sess, coord):
        timeRec=[time.time()-self.time0,-2,-2]
        self.stepRecord.append(timeRec)
        print('\nITH:after_create_session',time.time())

    def after_run(self, run_context, run_values):
        loss_valueL = run_context.session.run(self.tensor)['loss'] #np-array
        nStep=len(loss_valueL)
        assert nStep>0
        loss=loss_valueL[-1]
        self.myGlobStep+=nStep
        if self.myGlobStep<1000 or self.myGlobStep%6000==0:  # will be called only once per IPU-loop
            print('ITH:',time.time(),self.myGlobStep,len(loss_valueL),loss)
        
        timeRec=[time.time()-self.time0,self.myGlobStep,loss]
        self.stepRecord.append(timeRec)
        
#...!...!..................
    def end(self,session):
        print('\nTTH:end',time.time(),'end-rec:',self.stepRecord[-1],' num rec=', len(self.stepRecord))
        csvF='%s-%s.stepRecord.csv'%(self.prjName,self.mode)

        save_stepRecords(self.stepRecord,csvF)
        totTime=self.stepRecord[-1][0]
        initT=self.stepRecord[1][0]
        print('\nITH_END graph init time=%.2f (min),  totalTime %.2f (min)'%(initT/60.,totTime/60.))

        
