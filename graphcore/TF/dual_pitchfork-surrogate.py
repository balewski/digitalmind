#!/usr/bin/env python

'''
simpliffied inplementation of pitchforkOracle :
- instantiates model from HPAR (yaml)
- fake data created in CPU RAM
- no IO cost included
- no validation cost included
- does inference at the end - uncomment the code
- two types of data generators: int16 and float32 can be examined
- custom hook used to monitore speed


Based on ipu_estimator_cnn.py from Brad, uses estimator


*******Refference speed  *******
data shape: (?,8000,11) and BS=8
1 IPU:  480 steps/sec (at peak, varies a lot)
1 GPU V100: 230 steps/sec

Galileo Azure, CPU: 21 steps/sec
Cori-login, CPU: 34 step/sec

'''

import argparse
import tensorflow.compat.v1 as tf
import time
import numpy as np
import os, time
import ruamel.yaml  as yaml

from tensorflow.keras.layers import Dense, Input, Dropout, BatchNormalization, Conv1D, LeakyReLU, MaxPool1D, Flatten, Lambda


from CustomHook1 import CustomUpdaterHook

#...!...!..................
def model_fn(features, labels, mode, params):
    # - - - Assembling model
    hpar=params
    print('build_model hpar:',hpar)

    # CNN params

    cnn_ker=hpar['conv_kernel']
    # hpar.pool_len -  how much time_bins get reduced per pooling
    # hpar.conv_repeat - how many times repeat CNN before maxPool

    # FC params
    dropFrac=hpar['dropFrac']

    # - - - Assembling model 
    h=features
    print('xa dtype:',tf.keras.backend.dtype(h))

    if hpar['inp_batch_norm']:
        h=BatchNormalization(name='inp_BN_cast')(h) #,dtype=np.float16

    #print('cnn-inp dtype:',tf.keras.backend.dtype(h))

    # .....  CNN-1D layers
    k=0
    for dim in hpar['conv_filter']:
        for j in range(hpar['conv_repeat']):
            k+=1
            h= Conv1D(dim,cnn_ker,activation='linear',padding='valid' ,name='cnn%d_d%d_k%d'%(k,dim,cnn_ker))(h)
            h =LeakyReLU(name='aca%d'%(k))(h)
        h= MaxPool1D(pool_size=hpar['pool_len'], name='pool_%d'%(k))(h)


    h=Flatten(name='to_1d')(h)
    flattenSize=int(h.get_shape()[1])
    if mode == tf.estimator.ModeKeys.TRAIN and  dropFrac>0:
        h = Dropout(dropFrac,name='fdrop')(h)

    # .... FC  layers   
    for i,dim in enumerate(hpar['fc_dims']):
        h = Dense(dim,activation='linear',name='fc%d'%i)(h)
        h =LeakyReLU(name='acb%d'%(i))(h)
        if dropFrac>0:  h = Dropout(dropFrac,name='drop%d'%i)(h)


    #y= Dense(labels, activation=hpar['lastAct'])(h) # crash on IPU
    y= Dense(hpar['out_dim'], activation=hpar['lastAct'])(h) # hack, ignore 'lables'
    if hpar['lastAct']=='tanh':
        y = Lambda(lambda val: val*hpar['outAmpl'], name='scaleAmpl_%.1f'%hpar['outAmpl'])(y)
    totTrainVars=np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])/1024
    print('build_fn w/ outputs :',y.get_shape(),'mode=',mode,'totTrainVars=%.1fk'%(totTrainVars))

    # Optimizer, learning rate, and epsilon
    #[optName, initLR, optPar2]=hpar['optimizer']
    lossName = hpar['lossName']
    # select the correct loss function
    if lossName == 'mae' :
        loss = tf.metrics.mean_absolute_error(labels , y)
    elif lossName == 'mse' :
        loss = tf.losses.mean_squared_error(labels , y)
    else: crash_bad_loss


    print('WWW',loss)
    if mode == tf.estimator.ModeKeys.EVAL:
        predictions = h
        eval_metric_ops = {
            "accuracy": tf.metrics.accuracy(labels=labels, predictions=predictions),}  # not used, junk
        return tf.estimator.EstimatorSpec(mode, loss=loss, eval_metric_ops=eval_metric_ops)
    elif mode == tf.estimator.ModeKeys.TRAIN:
        # Optimizer, learning rate, and epsilon
        [optName, initLR, optPar2]=hpar['optimizer']
        if optName=='adam' :
            optimizer=tf.train.AdamOptimizer(learning_rate=initLR, epsilon=optPar2)
        elif optName=='nadam' :
            #optimizer=tf.train.NadamOptimizer(learning_rate=initLR, epsilon=optPar2)
            optimizer=tf.keras.optimizers.Nadam(learning_rate=initLR, epsilon=optPar2)
            # neither works w/ TF
        elif optName=='adadelta' :
            optimizer = tf.train.AdadeltaOptimizer(learning_rate=initLR,rho=optPar2)
        else:
            crash_21
            
        # this is hack - using global variable 'args'
        if args.useIPU:
            train_op = optimizer.minimize(loss=loss)
        else: 
            train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
    else:
        raise NotImplementedError(mode)

    
#...!...!..................
def create_gpu_estimator(args,hpar):
    config = tf.estimator.RunConfig(
        log_step_count_steps=args.log_interval,
        save_summary_steps=args.summary_interval,
        model_dir=args.model_dir,
    )

    return tf.estimator.Estimator(
        config=config,
        model_fn=model_fn,
        params=hpar,
    )

#...!...!..................
def create_ipu_estimator(args,hpar):
    from tensorflow.python import ipu
    ipu_options = ipu.utils.create_ipu_config()

    ipu.utils.auto_select_ipus(ipu_options, num_ipus=1)

    ipu_run_config = ipu.ipu_run_config.IPURunConfig(
        iterations_per_loop=args.batches_per_ipu_iter,
        ipu_options=ipu_options,
    )

    config = ipu.ipu_run_config.RunConfig(
        ipu_run_config=ipu_run_config,
        log_step_count_steps=args.log_interval,
        save_summary_steps=args.summary_interval,
        model_dir=args.model_dir,
    )

    return ipu.ipu_estimator.IPUEstimator(
        config=config,
        model_fn=model_fn,
        params=hpar,
    )

#...!...!..................
def train(estimator, args, x_train, y_train):
    """Train a model and save checkpoints to the given `args.model_dir`."""
    def input_fn():
        # If using Dataset.from_tensor_slices, the data will be embedded
        # into the graph as constants, which makes the training graph very
        # large and impractical. So use Dataset.from_generator here instead.

        def generator(): return zip(x_train, y_train)

        prefetch_buffer_size=x_train.shape[0]//10
        shuffle_buffer_size=prefetch_buffer_size
        print('Train DC BS=%d, shuffle_buf=%d'%(args.batch_size,shuffle_buffer_size))
        types = (x_train.dtype, y_train.dtype)
        shapes = (x_train.shape[1:], y_train.shape[1:])

        dataset = tf.data.Dataset.from_generator(generator, types, shapes)
        dataset = dataset.prefetch(prefetch_buffer_size).cache()
        dataset = dataset.repeat()
        dataset = dataset.shuffle(shuffle_buffer_size)
        dataset = dataset.batch(args.batch_size, drop_remainder=True)

        # return dataset
        return dataset

    # Training progress is logged as INFO, so enable that logging level
    tf.logging.set_verbosity(tf.logging.INFO)
    myRunHook=CustomUpdaterHook('pitch1',args.log_interval//4, args.useIPU, 'train')

    # To evaluate N epochs each of n data points, with batch size BS, do Nn / BS steps.
    num_train_examples = int(args.epochs * len(x_train))
    steps = num_train_examples // args.batch_size

    if args.useIPU:
        # IPUEstimator requires no remainder; steps must be divisible by batches_per_step
        steps += (args.batches_per_ipu_iter - steps % args.batches_per_ipu_iter)

    t0 = time.time()
    estimator.train(input_fn=input_fn, steps=steps, hooks=[myRunHook])
    duration = time.time() - t0
    speed=duration/args.epochs
    print("\ncompile+train total time=%.1fs for %d epochs, avr time %.1f sec/epoch\n"%(duration,args.epochs,speed))

    
#...!...!..................
def test(estimator, args, x_test, y_test):
    """Test the model by loading weights from the final checkpoint in the
    given `args.model_dir`."""

    def input_fn():
        dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
        dataset = dataset.prefetch(len(x_test)).cache()
        dataset = dataset.batch(args.batch_size, drop_remainder=True)
        return dataset

    num_test_examples = len(x_test)
    steps = num_test_examples // args.batch_size
    if args.useIPU:
        # IPUEstimator requires no remainder; steps must be divisible by batches_per_ipu_iter
        steps -= steps % args.batches_per_ipu_iter

    print(f"Evaluating on {steps * args.batch_size} examples")

    t0 = time.time()
    metrics = estimator.evaluate(input_fn=input_fn, steps=steps)
    t1 = time.time()

    test_loss = metrics["loss"]
    test_accuracy = metrics["accuracy"] # is junk now
    duration_seconds = t1 - t0
    print("Test loss: {:g}".format(test_loss))
    print(f"Took {duration_seconds:.2f} seconds to compile and run")

#-------------------
def create_datasetInt(num_train,hpar):
    def generator():
        return zip(X, Y) # The input data and labels

    startT = time.time()
    Y=np.random.uniform(-0.2,0.8, size=(num_train,hpar['out_dim'])).astype('float32')
    print('CD: done Y shape',Y.shape,Y.dtype)
    label_shape = [None, Y.shape[1]]
    
    X=np.random.randint(-20000,20000, size=(num_train,hpar['time_dim'],hpar['inp_dim']),dtype=np.int16)
    data_shape = [None, X.shape[1]]

    # inject Y to X so training can converge
    X[:,:hpar['out_dim'],0]=Y*15000+np.random.normal(loc=0, scale=5000, size=(num_train,hpar['out_dim']))

    # hack #1, bc TF has no dtype=int16
    X=X.astype(np.float32)# why do I need convert int16 to float ?
    print('CDI: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))

    if 0: #hack #2
        t0 = time.time()
        X/=20000. # scale values to [-1,1] range, approximately
        print('X/20k cost: elaT=%.1f sec,'%(time.time() - t0))
    print('X w/ y example values:',X[:3,:hpar['out_dim'],0],X[0,0,0].dtype)
    return X,Y

    
#-------------------
def create_datasetFloat(num_train,hpar):
    def generator():
        return zip(X, Y)
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-0.2,0.8, size=(num_train,hpar['out_dim'])).astype('float32')
    print('CD: done Y shape',Y.shape,Y.dtype)
    label_shape = [None, Y.shape[1]]

    X=np.random.uniform(-0.8,0.8, size=(num_train,hpar['time_dim'],hpar['inp_dim'])).astype('float32')
    data_shape = [None, X.shape[1]]

    # inject Y to X so training can converge
    X[:,:hpar['out_dim'],0]=Y*0.3+np.random.normal(loc=0, scale=0.04, size=(num_train,hpar['out_dim'])).astype('float32')
    X=X.astype(np.float32)
    print('CDF: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))
    return X,Y


#...!...!..................
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--design', dest='modelDesign', default='603282_69_9_bbp153',help=" HPAR defining ML model")
       
    parser.add_argument( "--ipu", dest='useIPU', action='store_true', default=False, help="switch to IPU HW")
      
    parser.add_argument("--batch_size",type=int, default=8,help="The batch size.")
    parser.add_argument("--inp_features",type=int, default=11,help="No. of input features per sample shape (?,8000,inpFeat)")
    
    parser.add_argument("--epochs", type=float, default=20, help="Total number of epochs to train for.")

    parser.add_argument("--test-only",action="store_true", help="Skip training and test using latest checkpoint from model_dir.")

    parser.add_argument( "--log-interval",type=int,default=1000, help="(steps) Interval at which to log progress.")

    parser.add_argument( "--summary-interval",type=int, default=300, help="Interval at which to write summaries.")

    parser.add_argument("--model-dir", default=None, help="Directory where checkpoints and summaries are stored.")

    return parser.parse_args()

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == "__main__":
    # Parse args
    args = parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    if args.model_dir !=None and os.path.exists(args.model_dir):
            print('Aborting on start:   rm -rf',args.model_dir)
            exit(0)
    
    ymlFn='hpar_cellSpike_%s.yaml'%args.modelDesign
    ymlFd = open(ymlFn, 'r')
    hparD=yaml.load( ymlFd, Loader=yaml.CLoader)
    ymlFd.close()
    print(hparD)

    # create data
    hparD['time_dim']=8000
    hparD['inp_dim']=args.inp_features
    hparD['out_dim']=30

    # pick  the type of input data generator !
    create_dataset=create_datasetFloat
    #create_dataset=create_datasetInt
    
    numFakeDataFiles=2
    num_train=12*1024*numFakeDataFiles
    steps=num_train//args.batch_size

    print('\nM: generate data, num_train samples=',num_train,'train steps=',steps)

    X,Y= create_dataset(num_train,hparD)

    print('\nM: build model from hpar')
    # Make estimator
    if args.useIPU:
        args.batches_per_ipu_iter=1000
        #help="The number of batches per execution loop on IPU."
        estimator = create_ipu_estimator(args,hparD)
    else:
        estimator = create_gpu_estimator(args,hparD)
        
    if not args.test_only:
        print("M: Training...")
        t0 = time.time()
        train(estimator, args, X, Y)
        duration = time.time() - t0
        speed=duration/args.epochs
        print("total time=%.1fs for %d epochs, avr time %.1f sec/epoch"%(duration,args.epochs,speed))

    print('M:train-done'); exit(0)
    num_test=num_train//10
    X2,Y2=create_dataset(num_test,hparD)

    print("M:Testing...")
    test(estimator, args, X2,Y2)

    print('M: done')
