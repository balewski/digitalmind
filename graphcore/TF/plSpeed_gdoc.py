#!/usr/bin/env python
""" 
 make plot from summary data in a gSheet table
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time
import csv
import matplotlib.pyplot as plt

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-t", "--tableName",help="input table copied from gDoc", default='speed-pich-surr-1.csv')


    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='plSpeed'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args



#...!...!..................
def read_one_csv(fname,delim=','):
    print('read_one_csv:',fname)
    tabL=[]
    with open(fname) as csvfile:
        drd = csv.DictReader(csvfile, delimiter=delim)
        print('see %d columns'%len(drd.fieldnames),drd.fieldnames)
        for row in drd:
            tabL.append(row)

        print('got %d rows \n'%(len(tabL)))
    #print('LAST:',row)
    return tabL,drd.fieldnames

#...!...!..................
def myPlot1(xV,yVV,labD,tit='',ytit='',figId=10):
    nrow,ncol=1,1
    #  grid is (yN,xN) - y=0 is at the top,  so dumm
    plt.figure(figId,facecolor='white', figsize=(5,3))

    ax=plt.subplot(nrow,ncol,1)

    ax.plot(xV, yVV[:,0],"*-",label=labD[0])
    ax.plot(xV, yVV[:,1],"o-",label=labD[1])
    ax.plot(xV, yVV[:,2],"x-",label=labD[2])
    ax.grid(True)
    
    ax.set(xlabel='batch size',ylabel=ytit,title=tit)
    #ax.legend(loc='center right')
    ax.legend(loc='best')
    if figId==10:
        ax.set_yscale('log')
        ax.set_xscale('log')
    else:
        ax.set_ylim(0,)
        ax.set_xlim(0,70)
    plt.tight_layout()
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
tabL,keyL=read_one_csv(args.tableName,'\t')

# repack data, idx
idxD={1:'GPU V100',0:'IPU Graphcore', 2:'CPUs Haswell'}
frameMB=0.352/1024.  # now it is GB

bsV=[]
speedS=[]  # speed in steps/sec
speedF=[]  # speed in frames/sec
speedM=[]  # speed in MB/sec
for row in tabL:
    bs=float(row['batch size'])
    bsV.append(bs)
    rec=[]; rec2=[]; rec3=[];
    for i in range(3):
        try:
            val=float(row[idxD[i]])
            val2=val*bs
            val3=val2*frameMB
        except:
            val3=val2=val=None
        rec.append(val)
        rec2.append(val2)
        rec3.append(val3)
    print(bsV[-1],rec)
    speedS.append(rec)
    speedF.append(rec2)
    speedM.append(rec3)

bsV=np.array(bsV)
speedS=np.array(speedS)
speedF=np.array(speedF)
speedM=np.array(speedM)
print('shapes:',bsV.shape,speedS.shape)
myPlot1(bsV,speedS,idxD,ytit='steps/sec', figId=10)

myPlot1(bsV,speedF,idxD,ytit='samples/sec', figId=11)
myPlot1(bsV,speedM,idxD,ytit='ingested GB/sec', figId=12)


plt.show()
