#!/usr/bin/env python

'''
 example of simple FC 


Cori-Gpu HW
https://docs-dev.nersc.gov/cgpu/hardware/
'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time
import numpy as np


import tensorflow.compat.v1 as tf
from tensorflow.keras.layers import Dense, Input, Dropout
from tensorflow.keras.models import Model, load_model

print('deep-libs imported TF ver:',tf.__version__)


#-------------------
def config_ipu():
    # Configure the IPU system
    from tensorflow.python import ipu
    cfg = ipu.utils.create_ipu_config()
    cfg = ipu.utils.auto_select_ipus(cfg, 1)
    ipu.utils.configure_ipu_system(cfg)
    return ipu.ipu_strategy.IPUStrategy()

    
#-------------------
def build_model(inp_dim,out_dim,fc_dim):
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' outs:',out_dim)
    h = Dense(fc_dim, activation='relu',name='calcium')(x_img)
    h=Dropout(0.1)(h)
    h=Dense(fc_dim*10, activation='relu',name='water')(h)
    h=Dense(fc_dim, activation='relu',name='sodium')(h)
    ho=Dense(out_dim, activation='tanh',name='potas')(h)

    print('build_model w/ outputs :',ho.get_shape())
    model=Model(inputs=x_img, outputs=ho)
    
    return model

#-------------------
def estimator_model(features,labels):
    # - - - Assembling model 
    h = Dense(fc_dim, activation='relu',name='calcium')(features)
    h=Dropout(0.1)(h)
    h=Dense(fc_dim*10, activation='relu',name='water')(h)
    h=Dense(fc_dim, activation='relu',name='sodium')(h)
    ho=Dense(out_dim, activation='tanh',name='potas')(h)
    print('build_model w/ outputs :',ho.get_shape())
    loss = tf.losses.mean_squared_error(labels, ho)    
    trainable_vars = [v for v in tf.trainable_variables()]
    optimizer = tf.train.AdamOptimizer()
    if GPU_ESTIMATOR == 1:
        train_op = optimizer.minimize(loss=loss, var_list=trainable_vars, global_step=tf.train.get_global_step())
    else:
        train_op = optimizer.minimize(loss=loss, var_list=trainable_vars)
    
    return tf.estimator.EstimatorSpec(mode=tf.estimator.ModeKeys.TRAIN, loss=loss, train_op=train_op)


#-------------------
def create_estimator(steps, ipu=True):
    if ipu == True:
        from tensorflow.python import ipu
        ipu_options = ipu.utils.create_ipu_config(
            profiling=False,
            use_poplar_text_report=False,
        )

        ipu.utils.auto_select_ipus(ipu_options, num_ipus=1)

        ipu_run_config = ipu.ipu_run_config.IPURunConfig(
            iterations_per_loop=steps,
            ipu_options=ipu_options,
        )

        config = ipu.ipu_run_config.RunConfig(
            ipu_run_config=ipu_run_config,
            log_step_count_steps=steps
        )

        return ipu.ipu_estimator.IPUEstimator(
            config=config,
            model_fn=estimator_model,)
    else:
        #print('aa',steps); ok77
        config = tf.estimator.RunConfig(
            log_step_count_steps=steps,
        )

        return tf.estimator.Estimator(
            config=config,
            model_fn=estimator_model,)


#-------------------
def create_dataset(num_train,inp_dim,out_dim,BS):
    def generator():
        return zip(X, Y)
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-0.2,0.8, size=(num_train,out_dim)).astype('float32')
    print('M: done Y shape',Y.shape,Y.dtype)
    label_shape = [None, Y.shape[1]]
    
    X=np.random.uniform(-0.8,0.8, size=(num_train,inp_dim)).astype('float32')
    print('M: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))
    data_shape = [None, X.shape[1]]
    
    # inject Y to X so training can converge
    X[:,:out_dim]=Y*0.3+np.random.normal(loc=0, scale=0.04, size=(num_train,out_dim)).astype('float32')

    shuffle_buffer_size=num_train//10
    print('TF DS BS=%d, shuffle_buf=%d'%(BS,shuffle_buffer_size))
    # train_ds = tf.data.Dataset.from_tensor_slices(
    #     (X,Y)).shuffle(shuffle_buffer_size).batch(BS)

    types = (tf.float32, tf.float32)
    shapes = (X.shape[1:], Y.shape[1:])
    train_ds = tf.data.Dataset.from_generator(generator, types, shapes)
    train_ds = train_ds.batch(batch_size, drop_remainder=True)
    train_ds = train_ds.shuffle(shuffle_buffer_size)
    train_ds = train_ds.prefetch(len(X)).cache()
    train_ds = train_ds.repeat()

    return train_ds

#=================================
#=================================
#  M A I N 
#=================================
#=================================
mFact=4  # model size expansion factor
nFact=20 # number of events expansion factor

inp_dim=8000*mFact
fc_dim=15*mFact*mFact
out_dim=3*mFact

epochs=6
batch_size=4
steps=150*nFact
num_train=steps*batch_size

print('\nM: generate data and train, num_train=',num_train,', mFact=%d, nFact=%d'%(mFact,nFact))

KERAS = 0
GPU_ESTIMATOR = 0

ds = create_dataset(num_train,inp_dim,out_dim,batch_size)
input_fn = lambda: create_dataset(num_train,inp_dim,out_dim,batch_size)
tf.logging.set_verbosity(tf.logging.INFO)

if KERAS: # this line makes it work on CPU/GPU
    model=build_model(inp_dim,out_dim,fc_dim)
    print('\nBuild multi-out model, layers=%d , params=%.1f K'%(len(model.layers),model.count_params()/1000.))
    model.compile(optimizer='adam', loss='mse')
    model.summary() # will print
    print('...fit on CPU using Keras')
    t0 = time.time()
    model.fit(ds, steps_per_epoch=steps, epochs=epochs,verbose=1)
elif GPU_ESTIMATOR:
    gpu_estimator = create_estimator(steps, ipu=False)
    t0 = time.time()
    gpu_estimator.train(input_fn=input_fn, steps=steps*epochs)
    duration = time.time() - t0
else:
    ipu_estimator = create_estimator(steps, ipu=True)
    t0 = time.time()
    ipu_estimator.train(input_fn=input_fn, steps=steps*epochs)
    duration = time.time() - t0
print('M:done')
duration = time.time() - t0
speed=duration/epochs
print("total time=%.1fs for %d epochs, avr time %.1f sec/epoch"%(duration,epochs,speed))

