#!/usr/bin/env python3
""" 
 make plot from per-step data stored as CSV file
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time
import csv

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-t", "--csvName",help="input per-step data recored during the training", default='fc101-train.stepRecord.csv')


    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='plSpeed'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args



#...!...!..................
def read_one_csv(fname,delim=','):
    print('read_one_csv:',fname)
    tabL=[]
    with open(fname) as csvfile:
        drd = csv.DictReader(csvfile, delimiter=delim)
        print('see %d columns'%len(drd.fieldnames),drd.fieldnames)
        for row in drd:
            tabL.append(row)

        print('got %d rows \n'%(len(tabL)))
    #print('LAST:',row)
    return tabL,drd.fieldnames

#...!...!..................
def myPlot1(xV,ysV, ylV,tit='',xtit='',figId=10):
    nrow,ncol=1,1
    #  grid is (yN,xN) - y=0 is at the top,  so dumm
    plt.figure(figId,facecolor='white', figsize=(5,3))

    ax=plt.subplot(nrow,ncol,1)
    col1='r'
    ax.plot(xV, ysV,".-", linewidth=0.5,markersize=3, color=col1,label='speed')
    #ax.grid(True)
    ax.set(xlabel=xtit,title=tit)
    ax.set_ylabel('speed (steps/sec)', color=col1)
    for tl in ax.get_yticklabels():  tl.set_color(col1)
    ax.legend(loc='upper center')

    ax.set_ylim(0.,1.05*max(ysV))
    
    if ylV[0] <0: # loss was not avaliable
        plt.tight_layout()
        return

    ax2 = ax.twinx()
    col2='b'
    # ,linestyle=''
    ax2.plot(xV, ylV,marker=".",markersize=4,linewidth=0.5, color=col2,label='loss')
    ax2.set_ylabel('loss', color=col2)
    #ax2.set_yscale('log')
    for tl in ax2.get_yticklabels():  tl.set_color(col2)
    ax2.legend(loc='lower center')
    ax2.grid()
    plt.tight_layout()

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
tabL,keyL=read_one_csv(args.csvName)

xV=[] # steps/k
tV=[] # time (min)
speedS=[] # steps/sec
lossV=[] # train-loss

# ['elaT', 'stepCnt', 'loss']
i=-1
for row in tabL:
   step=int(row['stepCnt'])
   elaT=float(row['elaT'])
   loss=float(row['loss'])
   i+=1

   if i>1 :    speed=(step-step0)/(elaT-elaT0)
   step0=step
   elaT0=elaT
   #print(i,elaT,elaT0)
   if i<2 : continue
   
   xV.append(step/1000.)
   tV.append(elaT/60.)
   speedS.append(speed)
   lossV.append(loss)

#print('tV',tV)
#print('xV',lossV)
xV=np.array(xV)
tV=np.array(tV)
speedS=np.array(speedS)
lossV=np.array(lossV)

#print('shapes:',xV.shape)
resM=float(speedS.mean())
resS=float(speedS.std())
tit2=' avr:%.2f rms:%.1f (steps/sec)'%(resM,resS)
print('M:',args.csvName,tit2,tV.shape)
#1myPlot1(xV,speedS,lossV,tit=args.csvName,xtit='step counter (k)', figId=10)
myPlot1(tV,speedS,lossV,tit=args.csvName,xtit='wall time (min)', figId=11)



plt.show()
