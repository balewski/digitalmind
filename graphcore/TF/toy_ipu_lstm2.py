#!/usr/bin/env python3

'''
 minimal example construction model using IPU LSTM API
- uses gcprofiler
- allows for variable precision: F16.32, F16

To activate Graphcore software stack:
. ./mySetup_graphcore.source 
Requires: Poplar ver 1.2

To run inside Docker do
 gc-docker  -- -u balewski  -it --volume /home/balewski/digitalMind/graphcore:/digitalMind balewski/graphcore:v1.2 bash
cd /digitalMind


Observed speed  for 
lstm0-out (600, 16, 40)
lstm1-out (600, 16, 20)



 gc-docker  -- -u balewski  -it --volume /home/balewski/digitalMind/graphcore:/digitalMind balewski/graphcore:v1.2 bash
cd /digitalMind


Observed speed  for partials=half
INFO:tensorflow:global_step/sec: 28.993

'''



import tensorflow.compat.v1 as tf

import numpy as np

from tensorflow.keras.layers import Dense,Flatten
from tensorflow.keras.layers import LSTM
from tensorflow.python import ipu
from tensorflow.compiler.plugin.poplar.ops import gen_ipu_ops


import argparse
import time
# target lstm input (BS, 1600, 4) based on new dataset dimensions


class ProfilerHook(tf.train.SessionRunHook):
    def begin(self):
        with tf.device("cpu"):
            self._report = gen_ipu_ops.ipu_event_trace()
    def end(self, session):
        raw_reports = session.run(self._report)
        save_tf_report(raw_reports)

#...!...!..................
def model_fn(features, labels, mode, params):
    # - - - Assembling model 
    fc_dim=params['fc_dim']
    lstm0_dim=params['lstm0_dim']
    lstm1_dim=params['lstm1_dim']

    h=features
    print('MF: inp',h.get_shape(),',lstm_dims=',lstm0_dim,lstm1_dim)
    if not args.useKeras:
        h=tf.transpose(h,perm=[1,0,2])
        print('MF: use Poplar LSTM, inp',h.get_shape())
        if lstm0_dim>0: #additional many-to-many LSTM layer
            lstm0 = ipu.rnn_ops.PopnnLSTM(lstm0_dim, dtype=tf.float16)
            h, output_state = lstm0(h, training=True)        
            print('MF: lstm0-out',h.get_shape())
        lstm1 = ipu.rnn_ops.PopnnLSTM(lstm1_dim, dtype=tf.float16)
        h, output_state = lstm1(h, training=True) # used as many-to-one         
        print('MF: lstm1-out',h.get_shape())
        h = output_state.h # need to set h = output_state.h which contains the output from the last sequence in the LSTM
        
        
    if args.useKeras:
        print('use Keras LSTM')
        if lstm0_dim>0: #additional many-to-many LSTM layer
            h= LSTM(lstm0_dim,return_sequences=True, dtype=tf.float16) (h)
        h= LSTM(lstm1_dim, dtype=tf.float16) (h)  # many-to_one
        print('MF: lstm1-out',h.get_shape())
        
    print('MF: dens-inp',h.get_shape())        
    h = Dense(fc_dim, dtype=tf.float16)(h)
    print('MF: dens-out',h.get_shape())
    h = Dense(fc_dim*2, dtype=tf.float16)(h)
    ho=Dense(params['out_dim'], activation='tanh', dtype=tf.float16)(h)
    print('MF: end-out',ho.get_shape())
    totTrainVars=np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])/1024
    print('build_fn w/ outputs :',ho.get_shape(),'mode=',mode,'totTrainVars=%.1fk'%(totTrainVars))
    loss = tf.losses.mean_squared_error(labels, ho)

    optimizer = tf.train.GradientDescentOptimizer(params["learning_rate"])
    train_op = optimizer.minimize(loss=loss)
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
 

#...!...!..................
def create_ipu_estimator(args,hpar):
    from tensorflow.python import ipu
    ipu_options = ipu.utils.create_ipu_config(profiling=args.profile, profile_execution=args.profile, max_report_size=1727937556, enable_poplar_serialized_graph=args.profile)

    ipu.utils.auto_select_ipus(ipu_options, num_ipus=1)

    ipu_options = ipu.utils.set_convolution_options(ipu_options, {
            "partialsType": str(args.partials)
        })

    ipu_run_config = ipu.ipu_run_config.IPURunConfig(
        iterations_per_loop=args.batches_per_step,
        ipu_options=ipu_options,
    )

    config = ipu.ipu_run_config.RunConfig(
        ipu_run_config=ipu_run_config,
        log_step_count_steps=args.log_interval,
        save_summary_steps=args.summary_interval,
        model_dir=args.model_dir,
    )

    return ipu.ipu_estimator.IPUEstimator(
        config=config,
        model_fn=model_fn,
        params=hpar,
    )

#...!...!..................
def train(estimator, args, x_train, y_train):
    """Train a model and save checkpoints to the given `args.model_dir`."""
    def input_fn():
        # If using Dataset.from_tensor_slices, the data will be embedded
        # into the graph as constants, which makes the training graph very
        # large and impractical. So use Dataset.from_generator here instead.

        def generator(): return zip(x_train, y_train)

        prefetch_buffer_size=x_train.shape[0]//10
        shuffle_buffer_size=prefetch_buffer_size
        print('Train DC BS=%d, shuffle_buf=%d'%(args.batch_size,shuffle_buffer_size))
        types = (x_train.dtype, y_train.dtype)
        shapes = (x_train.shape[1:], y_train.shape[1:])

        dataset = tf.data.Dataset.from_generator(generator, types, shapes)
        dataset = dataset.prefetch(prefetch_buffer_size).cache()
        dataset = dataset.repeat()
        dataset = dataset.shuffle(shuffle_buffer_size)
        dataset = dataset.batch(args.batch_size, drop_remainder=True)

        # return dataset
        return dataset

    # Training progress is logged as INFO, so enable that logging level
    tf.logging.set_verbosity(tf.logging.INFO)

    # To evaluate N epochs each of n data points, with batch size BS, do Nn / BS steps.
    num_train_examples = int(args.epochs * len(x_train))
    steps = num_train_examples // args.batch_size

    # IPUEstimator requires no remainder; steps must be divisible by batches_per_step
    steps += (args.batches_per_step - steps % args.batches_per_step)

    t0 = time.time()
    if args.profile == True:
        from gcprofile import save_tf_report
        ph = ProfilerHook()
        hooks = [ph]
        estimator.train(input_fn=input_fn, steps=100, hooks=hooks)
    else:
        estimator.train(input_fn=input_fn, steps=steps)
    duration = time.time() - t0
    speed=duration/args.epochs
    print("\ncompile+train total time=%.1fs for %d epochs, avr time %.1f sec/epoch\n"%(duration,args.epochs,speed))
 

#-------------------
def create_dataset(num_sampl,hpar):
    def generator():
        return zip(X, Y)
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-1.,1., size=(num_sampl,hpar['out_dim'])).astype('float16')
    print('CD: done Y shape',Y.shape,Y.dtype)
    label_shape = [None, Y.shape[1]]

    X=np.random.uniform(-0.8,0.8, size=(num_sampl,hpar['time_dim'],hpar['rgb_dim'])).astype('float16')
    data_shape = [None, X.shape[1]]

    #inject Y to X periodicaly flipping amplitude -better for LSTMs
    #List of prime numbers up to 100 used for unique periods
    primL=[3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
    assert len(primL) >=hpar['out_dim']
    for ip in range(hpar['out_dim']):
        yV=Y[:,ip]
        period=primL[ip]
        for it in range(hpar['time_dim']):
            if it%period!=0 : continue
            sign=1 if it%(2*period)==0 else -1
            #print('ip',ip,it,sign)
            X[:,it,0]=sign*yV*0.9 + np.random.normal(loc=0, scale=0.04, size=(num_sampl))
    
    print('\ndata-gen: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT),' Y:',Y.shape,Y.dtype)
    return X,Y

#-------------------


#...!...!..................
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument( "--keras", dest='useKeras', action='store_true', default=False, help="switch keras implementation of LSTM")
          
    parser.add_argument("--batch_size",type=int, default=16,help="The batch size.")
    parser.add_argument("--epochs", type=float, default=50, help="Total number of epochs to train for.")

    parser.add_argument( "--log-interval",type=int,default=1000, help="(steps) Interval at which to log progress.")

    parser.add_argument( "--summary-interval",type=int, default=300, help="Interval at which to write summaries.")

    parser.add_argument("--model-dir", default=None, help="Directory where checkpoints and summaries are stored.")

    parser.add_argument("--profile", type=bool, default=False, help="Flag to enable IPU profiling")

    parser.add_argument("--partials", type=str, default="half", help="select between fp16.16 (half) or fp16.32 (float)")

    return parser.parse_args()

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == "__main__":
    # Parse args
    args = parse_args()
    args.learning_rate=0.01
    args.batches_per_step=100
    #help="The number of batches per execution loop on IPU."
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    hparD={"learning_rate": args.learning_rate,
           "fc_dim":100,
           "lstm0_dim":30 , "lstm1_dim":20 # note, lstm0 can be 0
    }
    if args.profile == True:
        args.batches_per_step=1
    
    #hparD['time_dim']=1600;  hparD['rgb_dim']=4  # create data for MK-II (it compiles)
    hparD['time_dim']=900;  hparD['rgb_dim']=4  # create data for MK-I (it compiles)


    hparD['out_dim']=2
    num_train=10*1024
    X,Y=create_dataset(num_train,hparD)
    
    # Make estimator
    estimator = create_ipu_estimator(args,hparD)
    print("M: Training...")
    train(estimator, args, X, Y)

    print('M:train-done, end')
