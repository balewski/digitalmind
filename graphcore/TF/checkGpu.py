#!/usr/bin/env python
from pprint import pprint

import tensorflow as tf
print(tf.test.gpu_device_name())


from tensorflow.python.client import device_lib
Ltf=device_lib.list_local_devices()
print('Ltf, devCnt=',len(Ltf))
i=0
for d in Ltf:
    print('\nTF device:',i,d.name); i+=1
    name=d.name
    if 'XLA' in name : continue
    if 'CPU' in name:
        print(d)
        continue
    loc=d.locality
    phys=d.physical_device_desc

    print(name,'bus:',loc.bus_id,'numa:',loc.numa_node,'phys',phys)
    pprint(d)
    #ok88
    continue

    
#assert tf.test.is_gpu_available()
#assert tf.test.is_built_with_cuda()

Lpd=tf.config.list_physical_devices('IPU')
print('Lpd, devCnt=',len(Lpd))
pprint(Lpd)

from tensorflow.python.keras import backend as K
Lkr=K._get_available_gpus()
print('Lkr, devCnt=',len(Lkr))
pprint(Lkr)


