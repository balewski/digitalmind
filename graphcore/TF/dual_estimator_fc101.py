#!/usr/bin/env python3

'''
 example from Brad
# Copyright 2019 Graphcore Ltd.

Extended by Jan Balewski:
- runs on GPU or IPU
- instantiates Dense-only, hardcoded model
- fake data created in CPU RAM
- custom speed monitoring via callback hook
- no IO cost included
- no validation cost included
- does inference at the end - uncomment the code
- custom hook used to monitore speed

To draw speed+loss:
./plSpeed_csv.py  --csvName fc101-ipu-train.stepRecord.csv

Cori-Gpu HW
https://docs-dev.nersc.gov/cgpu/hardware/
module load esslurm
module load tensorflow/gpu-2.1.0-py37
salloc  -C gpu -n1 -c 10 --gres=gpu:1 --ntasks-per-node=1 -Adasrepo -t4:00:00 

 ./dual_estimator_fc101.py --epochs 1 --ipu

*******Refference speed for BS=16 *******
1 IPU:      644 +/- 190 steps/sec (at peak, varies a lot)
1 GPU V100: 512 +/-55   steps/sec   and 1.7 cpu w-load
 
Galileo Azure, CPU: ?130 steps/sec
Skylake, CPU: 143 +/- 4 step/sec  and 6 cpu w-load
cori-login:   124 +/- 12 step/sec and 16 cpu w-load
'''

import argparse
import tensorflow.compat.v1 as tf
import time
import numpy as np

from tensorflow.keras.layers import Dense, Dropout

#...!...!..................
def model_fn(features, labels, mode, params):
    # - - - Assembling model 
    fc_dim=params['fc_dim']
    h = Dense(fc_dim, activation='relu',name='calcium')(features)
    h=Dropout(0.1)(h)
    h=Dense(fc_dim*10, activation='relu',name='water')(h)
    h=Dense(fc_dim, activation='relu',name='sodium')(h)
    ho=Dense(params['out_dim'], activation='tanh',name='potas')(h)
    totTrainVars=np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])/1024
    print('build_fn w/ outputs :',ho.get_shape(),'mode=',mode,'totTrainVars=%.1fk'%(totTrainVars))
    loss = tf.losses.mean_squared_error(labels, ho)
    print('Loss tensor name=',loss,mode)

    if mode == tf.estimator.ModeKeys.EVAL:
        predictions = ho
        eval_metric_ops = {
            "accuracy": tf.metrics.accuracy(labels=labels, predictions=predictions),}  # not used, junk
        return tf.estimator.EstimatorSpec(mode, loss=loss, eval_metric_ops=eval_metric_ops)
    elif mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientDescentOptimizer(params["learning_rate"])
        # this is hack - using global variable 'args'
        if args.useIPU:
            train_op = optimizer.minimize(loss=loss)
            from TrainHooks  import IpuTrainingHook
            myTrainHook = IpuTrainingHook('fc101-ipu', 'train')
            logging_op = myTrainHook.enqueue({"loss": loss}) # Add loss to outfeed.  You could add accuracy here too
            train_op = tf.group([train_op, logging_op]) # Ensure logging op is attached to graph and executed
            # tf.group is important because otherwise the logging hook is pruned from the graph and causes this segfault error. 
        else: # regular TF
            train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
            from TrainHooks import TfTrainHook
            myTrainHook=TfTrainHook('fc101-gpu', 'train',args.log_interval//4)

        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op, training_hooks=[myTrainHook])
    else:
        raise NotImplementedError(mode)

    
#...!...!..................
def create_gpu_estimator(args,hpar):
    config = tf.estimator.RunConfig(
        log_step_count_steps=args.log_interval,
        save_summary_steps=args.summary_interval,
        model_dir=args.model_dir,
    )

    return tf.estimator.Estimator(
        config=config,
        model_fn=model_fn,
        params=hpar,
    )

#...!...!..................
def create_ipu_estimator(args,hpar):
    from tensorflow.python import ipu
    ipu_options = ipu.utils.create_ipu_config()

    ipu.utils.auto_select_ipus(ipu_options, num_ipus=1)

    ipu_run_config = ipu.ipu_run_config.IPURunConfig(
        iterations_per_loop=args.batches_per_step,
        ipu_options=ipu_options,
    )

    config = ipu.ipu_run_config.RunConfig(
        ipu_run_config=ipu_run_config,
        log_step_count_steps=args.log_interval,
        save_summary_steps=args.summary_interval,
        model_dir=args.model_dir,
    )

    return ipu.ipu_estimator.IPUEstimator(
        config=config,
        model_fn=model_fn,
        params=hpar,
    )

#...!...!..................
def train(estimator, args, x_train, y_train):
    """Train a model and save checkpoints to the given `args.model_dir`."""
    def input_fn():
        # If using Dataset.from_tensor_slices, the data will be embedded
        # into the graph as constants, which makes the training graph very
        # large and impractical. So use Dataset.from_generator here instead.

        def generator(): return zip(x_train, y_train)

        prefetch_buffer_size=x_train.shape[0]//10
        shuffle_buffer_size=prefetch_buffer_size
        print('Train DC BS=%d, shuffle_buf=%d'%(args.batch_size,shuffle_buffer_size))
        types = (x_train.dtype, y_train.dtype)
        shapes = (x_train.shape[1:], y_train.shape[1:])
        print('Train shapes, X,Y',shapes)

        dataset = tf.data.Dataset.from_generator(generator, types, shapes)
        dataset = dataset.prefetch(prefetch_buffer_size).cache()
        dataset = dataset.repeat()
        dataset = dataset.shuffle(shuffle_buffer_size)
        dataset = dataset.batch(args.batch_size, drop_remainder=True)

        # return dataset
        return dataset

    # Training progress is logged as INFO, so enable that logging level
    tf.logging.set_verbosity(tf.logging.INFO)

    # To evaluate N epochs each of n data points, with batch size BS, do Nn / BS steps.
    num_train_examples = int(args.epochs * len(x_train))
    steps = num_train_examples // args.batch_size

    if args.useIPU:
        # IPUEstimator requires no remainder; steps must be divisible by batches_per_step
        steps += (args.batches_per_step - steps % args.batches_per_step)
        
    #myRunHook=CustomUpdaterHook('fc101',args.log_interval//4, args.useIPU, 'train')
    t0 = time.time()
    estimator.train(input_fn=input_fn, steps=steps)
    duration = time.time() - t0
    speed=duration/args.epochs
    print("\ncompile+train total time=%.1fs for %d epochs, avr time %.1f sec/epoch\n"%(duration,args.epochs,speed))
 
#...!...!..................
def test(estimator, args, x_test, y_test):
    """Test the model by loading weights from the final checkpoint in the
    given `args.model_dir`."""

    def input_fn():
        dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
        dataset = dataset.prefetch(len(x_test)).cache()
        dataset = dataset.batch(args.batch_size, drop_remainder=True)
        return dataset

    num_test_examples = len(x_test)
    steps = num_test_examples // args.batch_size
    if args.useIPU:
        # IPUEstimator requires no remainder; steps must be divisible by batches_per_step
        steps -= steps % args.batches_per_step

    print(f"Evaluating on {steps * args.batch_size} examples")
    
    #2 myRunHook=CustomUpdaterHook(args.log_interval//50, args.useIPU, 'test')
    t0 = time.time()
    metrics = estimator.evaluate(input_fn=input_fn, steps=steps)#2 , hooks=[myRunHook])
    t1 = time.time()

    test_loss = metrics["loss"]
    test_accuracy = metrics["accuracy"] # is junk now
    duration_seconds = t1 - t0
    print("Test loss: {:g}".format(test_loss))
    print(f"Took {duration_seconds:.2f} seconds to compile and run")
    
#-------------------
def create_dataset(num_train,inp_dim,out_dim):
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-1,1, size=(num_train,out_dim)).astype('float32')
    print('M: done Y shape',Y.shape,Y.dtype)
    #label_shape = [None, Y.shape[1]]
    
    X=np.random.uniform(-0.8,0.8, size=(num_train,inp_dim)).astype('float32')
    print('M: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))
    #data_shape = [None, X.shape[1]]
    
    # inject Y to X so training can converge
    X[:,:out_dim]=Y*0.8+np.random.normal(loc=0, scale=0.04, size=(num_train,out_dim)).astype('float32')

    return X,Y


#...!...!..................
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument( "--ipu", dest='useIPU', action='store_true', default=False, help="switch to IPU HW")
          
    parser.add_argument("--batch_size",type=int, default=16,help="The batch size.")
    parser.add_argument("--epochs", type=int, default=10, help="Total number of epochs to train for.")

    parser.add_argument("--test-only",action="store_true", help="Skip training and test using latest checkpoint from model_dir.")

    parser.add_argument( "--log-interval",type=int,default=1000, help="(steps) Interval at which to log progress.")

    parser.add_argument( "--summary-interval",type=int, default=300, help="Interval at which to write summaries.")

    parser.add_argument("--model-dir", default=None, help="Directory where checkpoints and summaries are stored.")

    return parser.parse_args()

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == "__main__":
    # Parse args
    args = parse_args()
    args.learning_rate=0.01

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    # create data
    out_dim=12
    inp_dim=8000*4
    num_train=48*1024
    X,Y=create_dataset(num_train,inp_dim,out_dim)
    hparD={"learning_rate": args.learning_rate,
           "out_dim":out_dim, "fc_dim":240 }

    # Make estimator
    if args.useIPU:
        args.batches_per_step=100
        #help="The number of batches per execution loop on IPU."
        estimator = create_ipu_estimator(args,hparD)
    else:
        estimator = create_gpu_estimator(args,hparD)
         
    if not args.test_only:
        print("M: Training...")
        train(estimator, args, X, Y)

    print('M:train-done') ; exit(0)
    num_test=num_train//10
    X2,Y2=create_dataset(num_test,inp_dim,out_dim)
    
    print("M:predicting (aka Testing)...")
    test(estimator, args, X2,Y2)

    print('M: done')
