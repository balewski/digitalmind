#!/usr/bin/env python3

'''
 example from Brad
# Copyright 2019 Graphcore Ltd.

Extended by Jan Balewski:
- runs on GPU or IPU
- instantiates LSTM+Dense
- allow choice: keras.lstm or poplar.lstm
- LSTM cell count is hardcoded in main(): hparD:  "lstm0_dim":40 , "lstm1_dim":20
- fake data created in CPU RAM
- custom speed monitoring via callback hook
- no IO cost included
- no validation cost included
- does inference at the end - uncomment the code
- custom hook used to monitore speed & loss, use: plSpeed_csv.py to display

To display speed+loss:
./plSpeed_csv.py  --csvName lstm102-ipu-train.stepRecord.csv 


Cori-Gpu HW
https://docs-dev.nersc.gov/cgpu/hardware/
module load esslurm
module load tensorflow/gpu-2.1.0-py37
salloc  -C gpu -n1 -c 10 --gres=gpu:1 --ntasks-per-node=1 -Adasrepo -t4:00:00 
OR 1-NODE
salloc -C gpu -n 8 --ntasks-per-node=8 -c 10 -N1  --gres=gpu:8 -Adasrepo -t4:00:00


srun -n1  ./dual_estimator_lstm102.py --keras
OR with GPU monitor
srun -n1 -c10  bash -c '  nvidia-smi -l 3 >&L.smi & python -u ./dual_estimator_lstm102.py --keras '

****** Reference data & model *****
data-gen: X shape (40960, 900, 4) float32,  Y: (40960, 5) float32

 used hparD= {'learning_rate': 0.01, 'batch_size': 32, 'dropFrac': 0.05, 'fc_dim': 100, 'lstm0_dim': 30, 'lstm1_dim': 20, 'time_dim': 900, 'rgb_dim': 4, 'out_dim': 5, 'xType': 'float32', 'yType': 'float32'}

*******Refference speed *******
1 IPU:      35.3 steps/sec, BS=32, 60 epochs=60min

Software: Poplar version: 1.2.100, Poplar Tensorflow version: sdk/tensorflow/1.1.11-254-g9d1adc8fa1)

- - -
Software: gpu-tensorflow/2.1.0-py37
1 GPU V100: 1.1 steps/sec, BS=32, 2 epochs=40min
GPU load was: 20%-30%

1 GPU V100: 1.06 steps/sec, BS=256, 30 epochs=~80min
GPU load was: 35%-45%

1 GPU V100: 1.06 steps/sec, BS=1024, 60 epochs=~40min
GPU load was: 40%-55%

Q: how to get prediction values: Z2
Z=test(estimator, args, X,Y)
D=Y-Z <--- residua

'''

import argparse
import tensorflow.compat.v1 as tf
import time
import numpy as np

from tensorflow.keras.layers import Dense, Dropout

# https://docs.graphcore.ai/projects/tensorflow-user-guide/en/1.2.100/api.html#tensorflow.python.ipu.rnn_ops.PopnnLSTM

#...!...!..................
def model_fn(features, labels, mode, params):
    # - - - Assembling model 
    fc_dim=params['fc_dim']
    lstm0_dim=params['lstm0_dim']
    lstm1_dim=params['lstm1_dim']
    dropFrac=params['dropFrac']

    h=features
    print('MF: inp',h.get_shape(),',lstm_dims=',lstm0_dim,lstm1_dim)
    if not args.useKeras:
        from tensorflow.python import ipu
        h=tf.transpose(h,perm=[1,0,2])
        print('MF: use Poplar LSTM, inp',h.get_shape())
        if lstm0_dim>0: #additional many-to-many LSTM layer
            lstm0 = ipu.rnn_ops.PopnnLSTM(lstm0_dim)
            h, output_state = lstm0(h, training=True)
            print('MF: lstm0-out',h.get_shape())
        lstm1 = ipu.rnn_ops.PopnnLSTM(lstm1_dim)
        h, output_state = lstm1(h, training=True) # used as many-to-one         
        print('MF: lstm1-out',h.get_shape())
        h = output_state.h # need to set h = output_state.h which contains the output from the last sequence in the LSTM


    if args.useKeras:
        from tensorflow.keras.layers import LSTM
        print('use Keras LSTM')
        if lstm0_dim>0: #additional many-to-many LSTM layer
            h= LSTM(lstm0_dim,return_sequences=True) (h)
        h= LSTM(lstm1_dim) (h)  # many-to_one
        print('MF: lstm1-out',h.get_shape())

    print('MF: dens-inp',h.get_shape())
    h = Dense(fc_dim, activation='relu')(h)
    print('MF: dens-out',h.get_shape())
    h=Dropout(dropFrac)(h)
    h = Dense(fc_dim*2, activation='relu')(h)
    h=Dropout(dropFrac)(h)
    ho=Dense(params['out_dim'], activation='tanh')(h)
    print('MF: end-out',ho.get_shape())
    
    totTrainVars=np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])/1024
    print('build_fn w/ outputs :',ho.get_shape(),'mode=',mode,'totTrainVars=%.1fk'%(totTrainVars))
    loss = tf.losses.mean_squared_error(labels, ho)
    print('Loss tensor name=',loss,mode)

    if mode == tf.estimator.ModeKeys.EVAL:
        predictions = ho
        eval_metric_ops = {
            "accuracy": tf.metrics.accuracy(labels=labels, predictions=predictions),}  # not used, junk
        return tf.estimator.EstimatorSpec(mode, loss=loss, eval_metric_ops=eval_metric_ops)
    elif mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientDescentOptimizer(params["learning_rate"])
        # this is a hack - using global variable 'args'
        csvCore='lstm102'
        if args.useIPU:
            if args.useKeras:
                csvCore+='-keras-ipu'
            else:
                csvCore+='-poplar-ipu'
            train_op = optimizer.minimize(loss=loss)
            from TrainHooks  import IpuTrainingHook
            myTrainHook = IpuTrainingHook(csvCore, 'train')
            logging_op = myTrainHook.enqueue({"loss": loss}) # Add loss to outfeed.  You could add accuracy here too
            train_op = tf.group([train_op, logging_op]) # Ensure logging op is attached to graph and executed
            # tf.group is important because otherwise the logging hook is pruned from the graph and causes this segfault error. 
        else: # regular TF
            csvCore+='-gpu'
            train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
            from TrainHooks import TfTrainHook
            myTrainHook=TfTrainHook(csvCore, 'train',args.log_interval//4)

        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op, training_hooks=[myTrainHook])
    else:
        raise NotImplementedError(mode)

    
#...!...!..................
def create_gpu_estimator(args,hpar):
    config = tf.estimator.RunConfig(
        log_step_count_steps=args.log_interval,
        save_summary_steps=args.summary_interval,
        model_dir=args.model_dir,
    )

    return tf.estimator.Estimator(
        config=config,
        model_fn=model_fn,
        params=hpar,
    )

#...!...!..................
def create_ipu_estimator(args,hpar):
    from tensorflow.python import ipu
    ipu_options = ipu.utils.create_ipu_config()

    ipu.utils.auto_select_ipus(ipu_options, num_ipus=1)

    ipu_run_config = ipu.ipu_run_config.IPURunConfig(
        iterations_per_loop=args.batches_per_step,
        ipu_options=ipu_options,
    )

    config = ipu.ipu_run_config.RunConfig(
        ipu_run_config=ipu_run_config,
        log_step_count_steps=args.log_interval,
        save_summary_steps=args.summary_interval,
        model_dir=args.model_dir,
    )

    return ipu.ipu_estimator.IPUEstimator(
        config=config,
        model_fn=model_fn,
        params=hpar,
    )

#...!...!..................
def train(estimator, args, x_train, y_train):
    """Train a model and save checkpoints to the given `args.model_dir`."""
    def input_fn():
        # If using Dataset.from_tensor_slices, the data will be embedded
        # into the graph as constants, which makes the training graph very
        # large and impractical. So use Dataset.from_generator here instead.

        def generator(): return zip(x_train, y_train)

        prefetch_buffer_size=x_train.shape[0]//10
        shuffle_buffer_size=prefetch_buffer_size
        print('Train DC BS=%d, shuffle_buf=%d'%(args.batch_size,shuffle_buffer_size))
        types = (x_train.dtype, y_train.dtype)
        shapes = (x_train.shape[1:], y_train.shape[1:])
        print('Train shapes, X,Y',shapes)

        dataset = tf.data.Dataset.from_generator(generator, types, shapes)
        dataset = dataset.prefetch(prefetch_buffer_size).cache()
        dataset = dataset.repeat()
        dataset = dataset.shuffle(shuffle_buffer_size)
        dataset = dataset.batch(args.batch_size, drop_remainder=True)
        return dataset

    # Training progress is logged as INFO, so enable that logging level
    tf.logging.set_verbosity(tf.logging.INFO)

    # To evaluate N epochs each of n data points, with batch size BS, do Nn / BS steps.
    num_train_examples = int(args.epochs * len(x_train))
    steps = num_train_examples // args.batch_size

    if args.useIPU:
        # IPUEstimator requires no remainder; steps must be divisible by batches_per_step
        steps += (args.batches_per_step - steps % args.batches_per_step)
        
    t0 = time.time()
    estimator.train(input_fn=input_fn, steps=steps)
    duration = time.time() - t0
    speed=duration/args.epochs
    print("\ncompile+train total time=%.1fs for %d epochs, avr time %.1f sec/epoch\n"%(duration,args.epochs,speed))

    
#...!...!..................
def test(estimator, args, x_test, y_test):
    """Test the model by loading weights from the final checkpoint in the
    given `args.model_dir`."""

    def input_fn():
        dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
        dataset = dataset.prefetch(len(x_test)).cache()
        dataset = dataset.batch(args.batch_size, drop_remainder=True)
        return dataset

    num_test_examples = len(x_test)
    steps = num_test_examples // args.batch_size
    if args.useIPU:
        steps -= steps % args.batches_per_step

    print(f"Evaluating on {steps * args.batch_size} examples")
    
    t0 = time.time()
    metrics = estimator.evaluate(input_fn=input_fn, steps=steps)
    t1 = time.time()

    test_loss = metrics["loss"]
    test_accuracy = metrics["accuracy"] # is junk now
    duration = t1 - t0
    speed=duration/args.epochs
    print("Test loss: {:g}".format(test_loss))
    print("\ncompile+train total time=%.1fs for %d epochs, avr time %.1f sec/epoch\n"%(duration,args.epochs,speed))
    
#-------------------
def create_dataset(num_sampl,hpar):
    def generator():
        return zip(X, Y)
    
    # The input data and labels
    startT = time.time()
    
    Y=np.random.uniform(-1,1, size=(num_sampl,hpar['out_dim'])).astype(hpar['yType'])

    X=np.random.uniform(-0.8,0.8, size=(num_sampl,hpar['time_dim'],hpar['rgb_dim'])).astype(hpar['xType'])

    #inject Y to X periodicaly flipping amplitude -better for LSTMs
    #List of prime numbers up to 100  used for unique periods
    primL=[3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
    assert len(primL) >=hpar['out_dim']
    for ip in range(hpar['out_dim']):
        yV=Y[:,ip]
        period=primL[ip]
        for it in range(hpar['time_dim']):
            if it%period!=0 : continue
            sign=1 if it%(2*period)==0 else -1
            #print('ip',ip,it,sign)
            X[:,it,0]=sign*yV*0.9 + np.random.normal(loc=0, scale=0.04, size=(num_sampl))
        
    
    print('\ndata-gen: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT),' Y:',Y.shape,Y.dtype)
    return X,Y



#...!...!..................
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument( "--keras", dest='useKeras', action='store_true', default=False, help="switch keras implementation of LSTM")

    parser.add_argument( "--ipu", dest='useIPU', action='store_true', default=False, help="switch to IPU HW")
          
    parser.add_argument("--batch_size",type=int, default=32,help="The batch size.")
    parser.add_argument("--epochs", type=int, default=90, help="Total number of epochs to train for.")

    parser.add_argument("--test-only",action="store_true", help="Skip training and test using latest checkpoint from model_dir.")

    parser.add_argument( "--log-interval",type=int,default=1000, help="(steps) Interval at which to log progress.")

    parser.add_argument( "--summary-interval",type=int, default=300, help="Interval at which to write summaries.")

    parser.add_argument("--model-dir", default=None, help="Directory where checkpoints and summaries are stored.")

    return parser.parse_args()

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == "__main__":
    # Parse args
    args = parse_args()
    args.learning_rate=0.01
 
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    if not args.useIPU: assert args.useKeras  # you run on Cori

    #  data for MK-I (it compiles)
    hparD={"learning_rate": args.learning_rate,
           "batch_size": args.batch_size,
           "dropFrac": 0.05,
           "fc_dim":100,
           "lstm0_dim":30 , "lstm1_dim":20, # note, lstm0 can be 0
           'time_dim':900,'rgb_dim':4,  
           'out_dim':5, 
           'xType':'float32','yType':'float32',           
    }
        
    # create data
    num_train=40*1024
    
    X,Y=create_dataset(num_train,hparD)
    stepsPerEpoch=X.shape[0]//args.batch_size
    print('M: One epoch is %d steps, BS=%d, totSteps=%d\n'%(stepsPerEpoch,args.batch_size,stepsPerEpoch*args.epochs))
    # Make estimator
    if args.useIPU:
        args.batches_per_step=100 #help="The number of batches per execution loop on IPU."
        estimator = create_ipu_estimator(args,hparD)
    else:
        estimator = create_gpu_estimator(args,hparD)
         
    if not args.test_only:
        print("M: Training...")
        train(estimator, args, X, Y)

    print('M: used hparD=',hparD)
    print('M:train-done') ; exit(0)
    num_test=num_train//10
    X2,Y2=create_dataset(num_test,hparD)
    
    print("M:predicting (aka Testing)...")
    test(estimator, args, X2,Y2)

    print('M: done')
