#!/usr/bin/env python3

'''
 example of simple FC 

Setup  mFact=4
M: done Y shape (12000, 12) float32
M: done X shape (12000, 32000) float32
 BS=4, shuffle_buf=1200
Trainable params: 8,837,772

*****Performance *****
Nersc, Skylake, 20-cpu Skylake:
CPU-only 8 cpus:  28 sec/epoch
GPU (V-100) +1.5 CPU :  5 sec/epoch

Azure, VM Standard_ND40s_v3
CPU-only: 23 sec/epoch
1 IPU:  9? sec/epoch 

Cori-Gpu HW
https://docs-dev.nersc.gov/cgpu/hardware/
module load esslurm
module load tensorflow/gpu-2.1.0-py37
salloc  -C gpu -n1 -c 10 --gres=gpu:1 --ntasks-per-node=1 -Adasrepo -t4:00:00 

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time
import numpy as np

import tensorflow as tf
from tensorflow.keras.layers import Dense, Input, Dropout
from tensorflow.keras.models import Model, load_model

print('deep-libs imported TF ver:',tf.__version__)


#-------------------
def config_ipu():
    # Configure the IPU system
    from tensorflow.python import ipu
    cfg = ipu.utils.create_ipu_config()
    cfg = ipu.utils.auto_select_ipus(cfg, 1)
    ipu.utils.configure_ipu_system(cfg)
    return ipu.ipu_strategy.IPUStrategy()

    
#-------------------
def build_model(inp_dim,out_dim,fc_dim):
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' outs:',out_dim)
    h = Dense(fc_dim, activation='relu',name='calcium')(x_img)
    h=Dropout(0.1)(h)
    h=Dense(fc_dim*10, activation='relu',name='water')(h)
    h=Dense(fc_dim, activation='relu',name='sodium')(h)
    ho=Dense(out_dim, activation='tanh',name='potas')(h)

    print('build_model w/ outputs :',ho.get_shape())
    model=Model(inputs=x_img, outputs=ho)
    
    return model

#-------------------
def create_dataset(num_train,inp_dim,out_dim,BS):
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-0.2,0.8, size=(num_train,out_dim)).astype('float32')
    print('M: done Y shape',Y.shape,Y.dtype)
    label_shape = [None, Y.shape[1]]
    
    X=np.random.uniform(-0.8,0.8, size=(num_train,inp_dim)).astype('float32')
    print('M: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))
    data_shape = [None, X.shape[1]]
    
    # inject Y to X so training can converge
    X[:,:out_dim]=Y*0.3+np.random.normal(loc=0, scale=0.04, size=(num_train,out_dim)).astype('float32')

    shuffle_buffer_size=num_train//10
    print('TF DS BS=%d, shuffle_buf=%d'%(BS,shuffle_buffer_size))
    train_ds = tf.data.Dataset.from_tensor_slices(
        (X,Y)).shuffle(shuffle_buffer_size).batch(BS)
    train_ds = train_ds.map(lambda d, l:
                            (tf.cast(d, tf.float32), tf.cast(l, tf.float32)))
    #d = d.map(parser_fn, num_parallel_calls=num_map_threads)
    return train_ds.repeat()


#=================================
#=================================
#  M A I N 
#=================================
#=================================
mFact=4  # model size expansion factor
nFact=20 # number of events expansion factor

inp_dim=8000*mFact
fc_dim=15*mFact*mFact
out_dim=3*mFact

epochs=6
batch_size=4
steps=150*nFact
num_train=steps*batch_size

print('\nM: generate data and train, num_train=',num_train,', mFact=%d, nFact=%d'%(mFact,nFact))


# next 2 lines redirect computation to IPU
#strategy = config_ipu()
#with strategy.scope():

if 1: # this line makes it work on CPU/GPU
    model=build_model(inp_dim,out_dim,fc_dim)
    print('\nBuild multi-out model, layers=%d , params=%.1f K'%(len(model.layers),model.count_params()/1000.))

    ds = create_dataset(num_train,inp_dim,out_dim,batch_size)
    model.compile(optimizer='adam', loss='mse')
    model.summary() # will print

    #...fit on CPU using Keras
    model.fit(ds, steps_per_epoch=steps, epochs=epochs,verbose=1)

print('M:done')
