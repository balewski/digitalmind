__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import time,  os
import h5py
import ruamel.yaml  as yaml
from scipy.stats import stats # for Pearson correlation

#...!...!..................
def read_yaml(ymlFn):
        print('  read  yaml:',ymlFn)#,end='')
        start = time.time()
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.safe_load( ymlFd) #, Loader=yaml.Loader)
        ymlFd.close()
        print(' done, size=%d'%len(bulk),'  elaT=%.1f sec'%(time.time() - start))
        return bulk

#...!...!..................
def write_yaml(rec,ymlFn,verb=1):
        start = time.time()
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1048576
        if verb:
                print('  closed  yaml:',ymlFn,' size=%.2f MB'%xx,'  elaT=%.1f sec'%(time.time() - start))

#...!...!..................
def write_data_hdf5(dataD,outF):
        h5f = h5py.File(outF, 'w')
        print('save data as hdf5:',outF)
        for item in dataD:
           rec=dataD[item]
           h5f.create_dataset(item, data=rec)
        print('h5-write :',item, rec.shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB, frames=%d'%(xx,rec.shape[0]))

#...!...!..................
def read_data_hdf5(inpF):
        print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD
