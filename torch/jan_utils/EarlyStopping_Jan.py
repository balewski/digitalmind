# Author: Bjarte Mehus Sunde
# modiffied by Jan Balewski
# source: https://github.com/Bjarten/early-stopping-pytorch/blob/master/pytorchtools.py
import numpy as np
import torch

# fixes: change (-loss) to loss , use automatic direction???

class EarlyStopping:
    """Early stops the training if validation loss doesn't improve after a given patience."""
    def __init__(self, patience=7, verbose=False, delta=0, warmup=5, chkptFile=None):
        """
        Args:
            patience (int): How long to wait after last time validation loss improved.
            verbose (bool): If True, prints a message for each validation loss improvement. 
            delta (float): Minimum change in the monitored quantity to qualify as an improvement.
            warmup (int): initial sleep period to avoid frequent chkpt
            chktpFile (string): full chkpt file name
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf
        self.delta = delta
        self.warmup=warmup
        self.chkptFile=chkptFile
        self.epochCnt=0
        print('EarlyStopping cstr, chkptFile=',chkptFile,', patience=%d, delta=%.3g'%(patience,delta))

    def __call__(self, val_loss, model):
        score = -val_loss
        self.epochCnt+=1
        if self.best_score is None:  # jan- drop it later
            self.best_score = score
            self.save_checkpoint(val_loss, model)
        elif score < self.best_score - self.delta:
            self.counter += 1
            #print('  EarlyStopping: patience=%d,  worse loss, old:  %.3g  --> new %.3g '%(self.counter,self.val_loss_min,val_loss))
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
            self.counter = 0
        return self.early_stop

    def save_checkpoint(self, val_loss, model):
        '''Saves model when validation loss decrease.'''
        if self.verbose:
            print('  EarlyStopping: patience=%d,  val-loss decreased:  %.4g  -->  %.4g '%(self.counter,self.val_loss_min,val_loss))
        if self.epochCnt > self.warmup and self.chkptFile!=None:
            print('  EarlyStopping: saving state ...',self.chkptFile)
            torch.save(model.state_dict(), self.chkptFile)
        self.val_loss_min = val_loss
