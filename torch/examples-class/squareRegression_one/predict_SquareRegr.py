#!/usr/bin/env python
""" 
read input hd5 tensors
read trained net : model+weights
read test data from HD5
infere for  test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"
import numpy as np
import  time
import sys,os
sys.path.append(os.path.relpath("toolbox/"))
from Plotter_SquareRegr import Plotter_SquareRegr
from Deep_SquareRegr import Deep_SquareRegr

from Util_Torch import model_infer
from Util_SquareRegr import  get_UmZ_correl

sys.path.append(os.path.relpath("../../jan_utils/"))
from Util_IO import read_yaml, write_yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='predict Cell-HH data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('--design', dest='modelDesign',  default='fc1',
                        choices=['fc1','cnn1'], help=" model design of the network")

    parser.add_argument('--device',  default='cpu', choices=['cpu','cuda'], 
                        help=' hardware to be used, multi-gpus not supported')

    parser.add_argument("-d", "--dataPath",help="path to input",
                        default='data0')

    parser.add_argument("--seedWeights", default='same',
                        help="seed weights only, after model is created")

    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='squareRegr'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.seedWeights=='same' :  args.seedWeights=args.outPath

deep=Deep_SquareRegr.predictor(args)
gra=Plotter_SquareRegr(args,deep.metaD  )

dom='test'
#dom='val'
deep.load_input_hdf5([dom])
deep.seed_dataLoaders()

#1gra.plot_frames(deep.data[dom]); gra.display_all(args,'predict')  

deep.build_model()
deep.load_model_state()

print('sample predictions:')
loss,X1,Y1,Z1=model_infer( deep,dom,outType=1) 
print('quick infer : Average loss: %.4f  events=%d '% (loss,  Z1.shape[0]))
for i in range(Z1.shape[0]):
    print('y=',Y1[i],'   z=',Z1[i],'   diff=',Y1[i]-Z1[i])
    if i>3: break
dataA={'image_X':X1,'unit_Y':Y1,'unit_Z':Z1}
gra.plot_frames(dataA) #; gra.display_all(args,'predict')  

print('\nfull test-set predictions:')
startT=time.time()
loss,Y2,Z2=model_infer( deep,dom,outType=2)
predTime=time.time()-startT
print(' infer : Average loss: %.4f  events=%d , elaT=%.2f min'% (loss,  Z2.shape[0],predTime/60.))

lossMSE=get_UmZ_correl(Y2-Z2,deep.metaD)
sumRec={}
sumRec[dom+'LossMSE']=float(lossMSE)
sumRec['predTime']=predTime
sumRec['numSamples']=Y2.shape[0]
sumRec['design']=deep.modelDesign
write_yaml(sumRec, deep.outPath+'/'+deep.prjName+'.sum_pred.yaml')

tit='dom=%s '%('val')
gra.plot_fparam_residu(Y2,Z2, figId=9,tit=tit, tit2='loss=%.3f'%loss)
gra.plot_data_fparams(Z2,'pred Z',figId=8)
gra.plot_data_fparams(Y2,'true Y',figId=7)

gra.display_all('predict')  
