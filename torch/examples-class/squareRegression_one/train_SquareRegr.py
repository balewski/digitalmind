#!/usr/bin/env python
"""
Runs on CPU or 1 GPU

 read input hd5 tensors
uses self.layer = nn.ModuleList()
uses  LambdaLayer, ReduceLROnPlateau
train net
write net + weights as HD5


SEE ALSO README !!!

For predictions must run on GPU (or 20 epochs)  1st to create (once) the 'state_best.pth' file

Train on CPU , uses w=16
 ./train_SquareRegr.py -e 20

CPU speed:  1.2k frames/sec, 10 sec/epoch

Train  one 1 GPU
 srun -n1 ./train_SquareRegr.py --device cuda -e 100

speed 7.7k framse/sec
val_loss=0.0425


"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from pprint import pprint 

import sys,os
sys.path.append(os.path.relpath("toolbox/"))
from Plotter_SquareRegr import Plotter_SquareRegr
from Deep_SquareRegr import Deep_SquareRegr

sys.path.append(os.path.relpath("../../jan_utils/"))
from Util_IO import write_yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--design', dest='modelDesign',  default='fc1', choices=['fc1','cnn1'], help=" model design of the network")

    parser.add_argument('--device',  default='cpu', choices=['cpu','cuda'],  help=' hardware to be used, multi-gpus not supported')
    parser.add_argument("--seedWeights", default=None, help="seed weights only, after model is created")

    parser.add_argument("-dataPath",help="output path",  default='data0')
    parser.add_argument("--outPath", default='out',help="output path for plots and tables")

    parser.add_argument("-n", "--events", type=int, default=0, help="events for training, use 0 for all")

    parser.add_argument("-e", "--epochs", type=int, default=40,
                        help="fitting epoch")
    parser.add_argument("-b", "--batch_size", type=int, default=None,
                        help="fit batch_size, default --> hparams")
    parser.add_argument("--dropFrac", type=float, default=None,
                        help="drop fraction at all FC layers, default --> hparams")
    parser.add_argument("-H", "--historyPeriod", type=int, default=5,
                        help="in epochs, compute & save train+val loss.")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disables X-term for batch mode")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    args = parser.parse_args()
    args.prjName='squareRegr'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.seedWeights=='same' :  args.seedWeights=args.outPath

deep=Deep_SquareRegr.trainer(args)
gra=Plotter_SquareRegr(args ,deep.metaD  )
deep.load_input_hdf5(['train','val'])
deep.seed_dataLoaders()

#1gra.plot_frames(deep.data['val'])
    
deep.build_model()

if args.seedWeights: deep.load_model_state()
deep.train_model()

deep.save_model_full() 
write_yaml(deep.sumRec, deep.outPath+'/'+deep.prjName2+'.sum_train.yaml')

if args.verb>1: pprint(deep.sumRec)

gra.plot_train_history(deep,args) 
gra.display_all('train')
