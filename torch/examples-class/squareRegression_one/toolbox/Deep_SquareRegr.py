__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, sys, time
import socket # for hostname
start = time.time()

import numpy as np
import h5py

import torch
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau

from Util_Torch import SquareRegr_Dataset, model_train, model_infer, save_model_full

sys.path.append(os.path.relpath("../../jan_utils/"))
from Util_IO import write_yaml, read_yaml
from EarlyStopping_Jan import EarlyStopping

print('deep-libs imported elaT=%.1f sec'%(time.time() - start))

'''  MISSING:
- count epochs form1, propagate  epoch after restart
- EarlyStop : save state_best.pth after  after reaching first plataou, patince=5 is set in hpar.yaml
- train on GPU, predict on CPU

'''

#............................
#............................
#............................
class Deep_SquareRegr(object):

#...!...!..................
    def __init__(self,**kwargs):
        for k, v in kwargs.items():            
            self.__setattr__(k, v)

        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

        self.data={}
 
# * * * *   alternative constructors   * * * * 
#...!...!..................
    @classmethod 
    def trainer(cls, args):
        print('Cnst:train')
        obj=cls(**vars(args))
        obj.load_metaData()
        obj.load_hyperParams()

        # overwrite some hpar if provided from command line
        if obj.dropFrac!=None:
            obj.hparams['dropFrac']=obj.dropFrac
        if obj.batch_size!=None:
            obj.hparams['batch_size']=obj.batch_size

        # sanity checks
        assert obj.hparams['dropFrac']>=0
        return obj
#...!...!..................
    @classmethod 
    def predictor(cls, args):
        print('Cnst:pred')
        obj=cls(**vars(args))
        obj.load_metaData()
        obj.load_hyperParams()
        #obj.hparams['batch_size']=8 #tmp
                
        return obj        
# * * * *   alternative constructors   END * * * *

#...!...!..................        
    def load_metaData(self):
        metaF=self.dataPath+'/meta.'+self.prjName+'.yaml'
        self.metaD=read_yaml(metaF)
        print('read metaData:',metaF,'items:',len(self.metaD))
        # update prjName if not 'format'
        self.prjName2=self.prjName+'_'+self.modelDesign
        #self.inpDir=self.dataPath
        
#...!...!..................        
    def load_hyperParams(self):
        hparF='./hpar.'+self.prjName2+'.yaml'
        bulk=read_yaml(hparF)
        #print('input hpar',bulk)
        self.hparams=bulk
        print('read hyperParams:',hparF,'items:',len(self.hparams))
        
#...!...!..................
    def load_input_hdf5(self,domL):
        for dom in domL:
            inpF=self.dataPath+'/'+self.prjName+'_%s.hd5'%dom
            print('load hdf5:',inpF)
            h5f = h5py.File(inpF, 'r')
            self.data[dom]={}
            for xy in h5f.keys(): 
                npA=h5f[xy][:]
                if self.events>0:
                    mxe=self.events 
                    if dom!='train':  mxe=int(mxe/5) 
                    pres=int(npA.shape[0]/ mxe)
                    if pres>1: npA=npA[::pres]
                    if xy=='X': print('reduced ',dom,xy,' to %d events'%npA.shape[0])
                self.data[dom][xy] = npA 
                print(' done',dom,xy,self.data[dom][xy].shape)
            h5f.close()
            
        print('load_input_hdf5 done, elaT=%.1f sec'%(time.time() - start))

        # add input/output dimemnsions to hyper-params
        if 'inp_dims' not in self.hparams:
            self.hparams['inp_dims']=list(self.data[dom]['image_X'].shape[1:])
            self.hparams['out_dims']=self.data[dom]['unit_Y'].shape[1]

#...!...!..................
    def seed_dataLoaders(self):
        self.dataLoader={}
        batch_size=self.hparams['batch_size']
        for dom in self.data:
            domDst = SquareRegr_Dataset(self.data[dom])
            domLdr = DataLoader(domDst,shuffle=True,
                        batch_size=self.hparams['batch_size'],
                        num_workers=self.hparams['num_workers'])
            print('add dataLoader for dom=',dom,', num eve=',len(domDst))
            self.dataLoader[dom]=domLdr

        if 'train' in self.data:
            # add second shorter test8-loader for end-of-epoch loss calculation
            dom1='train'
            assert self.data[dom1]['unit_Y'].shape[0]>8
            domDst = SquareRegr_Dataset(self.data[dom1],presc=8)
            domLdr = DataLoader(domDst,shuffle=True,
                        batch_size=batch_size,
                        num_workers=self.hparams['num_workers'])
            self.dataLoader['train8']=domLdr

        print('seeded dataLoaders: ',self.dataLoader.keys(),', BS=',batch_size )
        if self.verb<=1: return
        
        print('\n print a fraction one batch of training data ')
        xx, yy = next(iter(self.dataLoader[dom]))
        print(dom,'batch, X,Y;',xx.shape,yy.shape)
        print('Y[:10]',yy[:10])

            
#...!...!..................
    def build_model(self):
        print('build_model START, design',self.modelDesign)

        if 'fc' in self.modelDesign:
            from Util_Torch import FConly_Model
            model=FConly_Model(self.hparams)

        if 'cnn' in self.modelDesign:
            from Util_Torch import CNNandFC_Model
            model=CNNandFC_Model(self.hparams)

        print(model)
        rec2=model.summary();    print(rec2)
        
        # create summary record
        rec={'hyperParams':self.hparams,
             'modelDesign' : self.modelDesign,
             'hostName' : socket.gethostname()
        }
        rec.update(rec2)

        rec3={}
        for dom in self.dataLoader:
            rec3[dom]={'steps': len(self.dataLoader[dom]),'events': len(self.dataLoader[dom].dataset)}
        #print('rec3',rec3)
        rec['dataset']=rec3
        
        self.sumRec=rec
        self.model=model

        # define loss function
        if self.hparams['lossName']=='mse':
            self.loss_func = torch.nn.MSELoss()# Mean Squared Loss
        print('loss=',self.loss_func)

        if self.verb<=1: return

        # only printout code below
        from torchsummary import summary
        inpShp=self.hparams['inp_dims'] # data must be opened 1st

        print('\n\nM: torchsummary.summary(model), inpSh=',inpShp)
        
        self.model.verb=1
        summary(self.model,(inpShp[0],inpShp[1]), device='cpu')
        self.model.verb=0

        return

#...!...!..................
    def train_model(self):
        print('train_model history period=%d  START ...'%self.historyPeriod)
        hpar=self.hparams
        self.model=self.model.to(self.device) # re-cast model on device, data will be casted later
        
            
        # Initialize optimizer
        optName, initLR=hpar['optimizer']
        currLR=initLR

        if optName=='adam' :
            self.optimizer=torch.optim.Adam(self.model.parameters(),lr=initLR)
        elif optName=='adadelta' :
            fix_me11
        else:
            print('invalid Opt:',optName); exit(99)
        print('opt:',self.optimizer)

        lrSched=None
        if 'ReduceLROnPlateau' in hpar:
            patx,facx,coolx=hpar['ReduceLROnPlateau']
            lrSched=ReduceLROnPlateau(self.optimizer, mode='min', factor=facx, patience=patx, cooldown=coolx, verbose=True)
            print('enable ReduceLROnPlateau:',patx,facx,coolx)

        earlyStop=None
        if 'EarlyStopping' in hpar:
            paty, dely, wary, chkpty= hpar['EarlyStopping']
            earlyStop=EarlyStopping(patience=paty, verbose=True, warmup=wary, chkptFile=self.outPath+'/'+self.prjName2+'.state_best.pth')
            print('enable EarlyStopping:',paty, dely, wary, chkpty)
            

        # set monitoring variables for traing
        self.sumRec['earlyStopOccured']=0
        histL=[]
        startT0=time.time()
        train_sum=0; kEpoch=0; startTM=time.time()
        print('start training over %d epochs'%self.epochs)
        for epoch in range( self.epochs ):
            kEpoch+=1            
            doMon= (epoch<4) or (epoch%self.historyPeriod==0) or  (epoch==self.epochs-1)
            startT1=time.time()
            wrk_loss=model_train( self)
            train_sum+=time.time() - startT1
            val_loss=model_infer( self, 'val')

            if  lrSched:
                currLR=self.optimizer.param_groups[0]['lr']
                lrSched.step(val_loss) # Decay Learning Rate

            if  earlyStop!=None and earlyStop(val_loss, self.model) :
                self.sumRec['earlyStopOccured']=1
                print("Early stopping, best val_loss=%.3g"%(-earlyStop.best_score))
                doMon=True

            if not doMon : continue
            # those operations cost more time , are mainly for monitoring purposes
            train_loss=model_infer(self,'train8')

            elaT=time.time() - startT0
            avr_sum=(time.time() - startTM)/kEpoch
            train_sum/=kEpoch
            
            print('epoch=%d loss wrk=%.3g, train=%.3g, val=%.3g, LR=%.3g, elaT=%d sec'%(epoch,wrk_loss,train_loss,val_loss,currLR,elaT))
            rec={'epoch':epoch,'train_loss':train_loss,'val_loss':val_loss,
                 'wrk_loss':wrk_loss,'lr':currLR,
                 'run_time':{'elapsed':elaT,'train_epoch':train_sum,'avr_epoch':avr_sum}
            }
            histL.append(rec)
            if self.sumRec['earlyStopOccured']==1: break
            # clear some of counters
            startTM=time.time()
            kEpoch=0
            train_sum=0

        #. . . .  end of epochs . . . . . .
        trainTime=time.time() - startT0
        trainEpochs=histL[-1]['epoch'] - histL[0]['epoch']
        trainFrames=trainEpochs * self.sumRec['dataset']['train']['events']

        self.sumRec['history']=histL
        self.sumRec['device']=self.device
        self.sumRec['trainTime']=trainTime
        self.sumRec['trainEpochs']=trainEpochs
        self.sumRec['trainFrames']=trainFrames
               
        end_loss=histL[-1]['val_loss']
        
        print('\n End Val Loss:%.3f  train: epochs=%d time/min=%.1f frames=%.1fk'%(end_loss,trainEpochs,trainTime/60.,trainFrames/1000.))
 
        return    
 
    #............................
    def save_model_full(self):
        outF=self.outPath+'/'+self.prjName2+'.model.tar'
        epochs=self.sumRec['trainEpochs']
        save_model_full(outF,self.model,self.optimizer,epochs)

    #............................
    def load_model_state(self):
        start = time.time()
        stateF=self.seedWeights+'/'+self.prjName2+'.state_best.pth'
        #stateF=self.seedWeights+'/'+self.prjName2+'.model.tar'
        print('load  weights  from',stateF,end='... ')
        if not os.path.exists(stateF): 
            print('Aborting on start, missing  model:',stateF)
            exit(99)

        self.model.load_state_dict(torch.load(stateF))
	self.model=self.model.to(self.device) # re-cast model on device, data will be casted later
        print('loaded, elaT=%.2f sec'%(time.time() - start))

  
