__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torch.optim  import lr_scheduler

import h5py

import numpy as np
import os, sys, copy,time

sys.path.append(os.path.relpath("../../jan_utils/"))
from LambdaLayer import LambdaLayer

#-------------------
#-------------------
#-------------------
#-------------------
class CNNandFC_Model(nn.Module):
    ''' Automatically   compute the size of the output of CNN+Pool portion of the model,
    needed as input to the first FC layer 
    '''
#...!...!..................
    def __init__(self,hpar):
        super(CNNandFC_Model, self).__init__()
        print('CNNandFC_Model hpar=',hpar)
        inp_chan=1
        self.inp_shape=(inp_chan,)+tuple(hpar['inp_dims'])
        self.verb=0
        print('CNNandFC_Model inp_shape=',self.inp_shape,', verb=%d'%(self.verb))
        
        self.cnn_block = nn.ModuleList()
        self.fc_block  = nn.ModuleList()

        # .....  CNN-1D layers
        k=0 #tmp
        cnn_stride=1
        for out_chan,cnnker,plker in zip(hpar['conv_filter'],hpar['conv_kernel'],hpar['pool_size']):
            for j in range(hpar['conv_repeat']):
                k+=1
                #print('BM: k,j=',k,j, 'inp_chan',inp_chan,'out_chan,cnnker,plker',out_chan,cnnker,plker)
                # class _ConvNd( in_channels, out_channels, kernel_size, stride,
                # CLASS torch.nn.MaxPool2d(kernel_size, stride=None,                
                self.cnn_block.append( nn.Conv2d(inp_chan, out_chan, cnnker, cnn_stride))
                self.cnn_block.append( nn.MaxPool2d(plker))
                self.cnn_block.append( nn.ReLU())
                inp_chan=out_chan

        # compute FC input size
        with torch.no_grad():
            # process 2 examples through the CNN portion of model
            x1=torch.tensor(np.zeros((2,)+self.inp_shape), dtype=torch.float32)
            y1=self.forwardCnnOnly(x1)
            self.flat_dim=np.prod(y1.shape[1:]) 
            print('myNet flat_dim=',self.flat_dim)

        # .... add FC  layers
        inp_dim=self.flat_dim
        for i,dim in enumerate(hpar['fc_dims']):
            self.fc_block.append( nn.Linear(inp_dim,dim))
            inp_dim=dim
            self.fc_block.append( nn.ReLU())
            if hpar['dropFrac']>0 : self.fc_block.append( nn.Dropout(p= hpar['dropFrac']))

        #.... the last FC layer will have different activation and no Dropout
        self.fc_block.append(nn.Linear(inp_dim,hpar['out_dims']))
        if hpar['lastAct']=='tanh' :
            self.fc_block.append(nn.Tanh())
            self.fc_block.append(LambdaLayer(lambda x: x*hpar['outAmpl']))

#...!...!..................
    def forwardCnnOnly(self, x):
        # flatten 2D image 
        x=x.view((-1,)+self.inp_shape )
        if self.verb: print('J: inp2cnn',x.shape)
        for i,lyr in enumerate(self.cnn_block):
            x=lyr(x)
            if self.verb: print('Jcnn: ',i,x.shape)
        return x
        
#...!...!..................
    def forward(self, x):
        if self.verb: print('J: inF',x.shape,'numLayers CNN=',len(self.cnn_block),'FC=',len(self.fc_block))
        x=self.forwardCnnOnly(x)
        x = x.view(-1,self.flat_dim)
        for i,lyr in enumerate(self.fc_block):
            x=lyr(x)
            if self.verb: print('Jfc: ',i,x.shape)
        if self.verb: print('J: y',x.shape)
        return x

#...!...!..................
    def summary(self):
        numLayer=sum(1 for p in self.parameters())
        numParams=sum(p.numel() for p in self.parameters())
        return {'modelWeightCnt':numParams,'trainedLayerCnt':numLayer,'modelClass':self.__class__.__name__}

    

#-------------------
#-------------------
#-------------------
class FConly_Model(nn.Module):
#...!...!..................
    def __init__(self,hpar):
        super(FConly_Model, self).__init__()
        print('FConly_Model hpar=',hpar)
        self.inpPix=np.prod(hpar['inp_dims'])
        self.verb=0
        print('FC_Model inpPix=%d verb=%d'%(self.inpPix,self.verb))
        self.layer = nn.ModuleList()
        
        # .... add FC  layers
        inp_dim=self.inpPix
        for i,dim in enumerate(hpar['fc_dims']):
            self.layer.append( nn.Linear(inp_dim,dim))
            inp_dim=dim
            self.layer.append( nn.ReLU())
            if hpar['dropFrac']>0 : self.layer.append( nn.Dropout(p= hpar['dropFrac']))
        #the last layer will have different activation and no Dropout
        self.layer.append(nn.Linear(inp_dim,hpar['out_dims']))
        if hpar['lastAct']=='tanh' :
            self.layer.append(nn.Tanh())
            self.layer.append(LambdaLayer(lambda x: x*hpar['outAmpl']))

#...!...!..................
    def forward(self, x):
        if self.verb: print('J: in',x.shape,'numLayers=',len(self.layer))
        # flatten 2D image 
        x=x.view(-1,self.inpPix )
        if self.verb: print('J: fl',x.shape)

        for i,lyr in enumerate(self.layer):
            x=lyr(x)
            if self.verb: print('J: ',i,x.shape)
        if self.verb: print('J: y',x.shape)
        return x

#...!...!..................
    def summary(self):
        numLayer=sum(1 for p in self.parameters())
        numParams=sum(p.numel() for p in self.parameters())
        return {'modelWeightCnt':numParams,'trainedLayerCnt':numLayer,'modelClass':self.__class__.__name__}

    
#-------------------
#-------------------
#-------------------
class ResistorTree_Dataset(Dataset):
    def __init__(self, conf0, verb=1):
        self.verb=verb
        self.conf=copy.deepcopy(conf0)
        assert len(conf0['dataFname'])==1  # not ready for mutiple input files
        inpF=conf0['sourceDir']+conf0['dataFname'][0]
        pres=conf0['prescale']
        print('load hdf5:',inpF, ',prescale=',pres)
        startT=time.time()
        h5f = h5py.File(inpF, 'r')
        self.data={}
        for xy in h5f.keys(): 
            npA=h5f[xy][:]
            if pres>1:
                npA=npA[::pres]
                if xy=='Volts':
                    print('reduced ',conf0['name'],xy,' to %d events'%npA.shape[0])
            self.data[xy] = npA 
            print(' done',conf0['name'],xy,self.data[xy].shape)
        h5f.close()
        self.num_eve=self.data['Volts'].shape[0]
        print('load_input_hdf5 done, elaT=%.1f sec'%(time.time() - startT))
 
    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.data['Volts'][idx]
        y=self.data['RxUnit'][idx]
        sample =(x,y)
        return sample

    def XY_shapes(self):
        return [ self.data['Volts'].shape[1] , self.data['RxUnit'].shape[1] ]

    
# = = = = = = = = = = = = = = = 
def model_train(deep):
    model=deep.model
    device=deep.device
    train_loader=deep.dataLoader['train']  # hardcoded
    optimizer=deep.optimizer
    model.train()
    train_loss=0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        lossOp = deep.loss_func(output, target)
        #print('qq',lossOp,len(train_loader.dataset),len(train_loader)); ok55
        lossOp.backward()
        optimizer.step()
        train_loss +=lossOp.item()
    train_loss /= len(train_loader)
    return train_loss


# = = = = = = = = = = = = = = = 
def model_infer(deep,dom, outType=0):
    ''' outType = 
    0:training mode, output only loss
    1:prediction for only 1 batch, output alL: loss,X,Y,U)
    
    '''
    model=deep.model
    device=deep.device
    test_loader=deep.dataLoader[dom] 
    model.eval()
    test_loss = 0

    if outType==2:
        num_eve=len(test_loader.dataset)
        numPar=deep.metaD['numPar']
        print('predict for num_eve=',num_eve,', numPar=',numPar)
        # clever list-->numpy conversion, Thorsten's idea
        Uall=np.zeros([num_eve,numPar],dtype=np.float32) 
        Zall=np.zeros([num_eve,numPar],dtype=np.float32) 
        nEve=0
        
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            lossOp=deep.loss_func(output, target)
            #print('qq',lossOp,len(test_loader.dataset),len(test_loader)); ok55
            test_loss += lossOp.item()
            if outType==1: return test_loss,np.array(data),np.array(target), np.array(output)
            if outType==2:
                nEve2=nEve+target.shape[0]
                #print('nn',nEve,nEve2)
                Uall[nEve:nEve2,:]=target[:]
                Zall[nEve:nEve2,:]=output[:]
                nEve=nEve2
    test_loss /= len(test_loader)
    if outType==0:
        return test_loss
    if outType==2:
        return test_loss,Uall,Zall
        

# = = = = = = = = = = = = = = = 
def save_model_full(outF,model,optimizer,currEpoch):
    torch.save({'epoch': currEpoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
             }, outF)
    xx=os.path.getsize(outF)/1048576
    print('  closed PyTorch chkpt:',outF,' size=%.2f MB'%xx)

    
# = = = = = = = = = = = = = = = 
def load_model_full(inpF,model,optimizer):
    print(' load PyTorch chkpt:',inpF)
    checkpoint = torch.load(inpF)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    lastEpoch = checkpoint['epoch']
    return lastEpoch

