__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np

from matplotlib import cm as cmap

from Plotter_Backbone import Plotter_Backbone

#............................
#............................
#............................
class Plotter_ResistorTree(Plotter_Backbone):
    def __init__(self, args,metaD):
        Plotter_Backbone.__init__(self,args)

        self.maxU=1.4
        self.metaD=metaD

#............................
    def plot_frames(self,dataA,nFr=4,idxL=[],figId=7):
        figId=self.smart_append(figId)
        XV=dataA['image_X']
        UV=dataA['unit_Y']
        showPred=0
        if 'unit_Z' in dataA:
            ZV=dataA['unit_Z']
            showPred=1
            
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,6))

        nPar=UV.shape[1]
        
        # prep conversion from unit-->phys parameters                
        u2p=np.array(self.metaD['unit_2_phys'])
        print('u2p',u2p)

        if len(idxL)==0:
            mxFr=XV.shape[0]
            idxL=np.random.choice(mxFr,size=(nFr), replace=False)
            
        nFr=min(len(idxL),nFr)
        nrow,ncol=2,2
        print('plot input for trace idx=',idxL, 'showPred=',showPred)
        j=0
        for it in idxL[:nFr]:
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j)
            j+=1
            frame=XV[it]
            U=UV[it]
            P=U*u2p[:,0] + u2p[:,1]
            ax.imshow(frame, vmax=1.1,cmap="YlGnBu", interpolation='nearest',origin='lower')
            ax.scatter(P[0],P[1],c='r') 
            ax.set(title='frame=%d, phys_Y=(%.1f,%.1f)'%(it,P[0],P[1]))

            if showPred:
                Z=ZV[it]
                pP=Z*u2p[:,0] + u2p[:,1]
                ax.scatter(pP[0],pP[1],c='b',marker='X')#,linewidth=0.3)


#............................
    def train_history(self,deep,args,figId=10):

        # pre-process training history
        trecL=deep.sumRec['history']
        print('plot_train_history: got %d train records'%len(trecL))

        batch_size=deep.sumRec['hyperParams']['batch_size']
        trainSteps=deep.sumRec['dataset']['train']['steps']
        trainEve=deep.sumRec['dataset']['train']['events']
        train8Eve=deep.sumRec['dataset']['train8']['events']
        valEve=deep.sumRec['dataset']['val']['events']
        
        trecEnd=trecL[-1]
        nEpochs=trecEnd['epoch']
        valLoss=trecEnd['val_loss']
        elapsedT=trecEnd['run_time']['elapsed']

        epoV=[] # epochs for which we have data
        VD={'train_fit':([],':bx',' n=%d'%trainEve),
            'train':([],'--bo',' n=%d'%train8Eve),
            'val':([],'--rd',' n=%d'%valEve),
        }

        hasLR=False
        if 'lr' in trecEnd:
            hasLR=True ;  lrV=[]
        
        speedD={'train_only':[],'agreg_epoch':[]}
        for rec in trecL:
            epoV.append(rec['epoch'])
            VD['train_fit'][0].append(rec['wrk_loss'])
            VD['train'][0].append(rec['train_loss'])
            VD['val'][0].append(rec['val_loss'])
            speedD['train_only'].append( trainEve/rec['run_time']['train_epoch'])
            speedD['agreg_epoch'].append( trainEve/rec['run_time']['avr_epoch'])
            #delTD['elaT'].append( rec['run_time']['elapsed']) # not used
            if hasLR:   lrV.append(rec['lr'])
            
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,4))

        nrow,ncol=3,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2, rowspan=2 )
        tit1='%s, train %.2f min, end-val=%.3g'%(deep.prjName2,elapsedT/60.,valLoss)
        ax1.set(ylabel='loss = %s'%deep.hparams['lossName'],title=tit1,xlabel='epochs, earlyStopOccured=%d'%deep.sumRec['earlyStopOccured'])
        for key in VD:
            ax1.plot(epoV,VD[key][0],VD[key][1],label=key+VD[key][2])

        ax1.legend(loc='lower left')
        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')

 
        # plot time per frame
        ax2  = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=1, rowspan=2 )
        tmpD={}        
        for key in ['agreg_epoch','train_only']:  # start with larger values
            ax2.hist(speedD[key],bins=10,label=key, alpha=0.7)
            zmu=np.array(speedD[key])
            resM=float(zmu.mean())
            resS=float(zmu.std())
            tmpD[key]=( resM, resS )
            print('avr speed key=%s  %.1f +/- %.1f frame/sec'%(key,resM, resS ))
        overHead=tmpD['train_only'][0]/ tmpD['agreg_epoch'][0]
        print('Cpu overhead due to validation=%.2f'%overHead)
        ax2.legend(loc='upper right')
       
        ax2.set(title='agreg avr %.1f+/-%.1f'%tmpD['agreg_epoch'], xlabel='global frames/sec ')
        x0=0.2
        ax2.text(x0,0.36,'val overhead=%.2f'%overHead,transform=ax2.transAxes)
        ax2.text(x0,0.44,'device=%s'%deep.sumRec['device'],transform=ax2.transAxes)
        ax2.text(x0,0.52,'steps=%d'%trainSteps,transform=ax2.transAxes)
        ax2.text(x0,0.60,'epochs=%d'%nEpochs,transform=ax2.transAxes)
        ax2.text(x0,0.68,'train BS=%d'%batch_size,transform=ax2.transAxes)

        if  not hasLR : return
        ax3 = self.plt.subplot2grid((nrow,ncol), (2,1), colspan=2,sharex=ax1 )

        ax3.plot(epoV,lrV,'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate')
            
#............................
    def plot_fparam_residu(self,U,Z, figId=9,tit='',tit2=''):

        colMap=cmap.rainbow
        parName=self.metaD['parName']
        nPar=self.metaD['numPar']

        nrow,ncol=3,nPar
        breakPar=0
        if nPar>15:
            breakPar=int(np.ceil(nPar/2.) )
            nrow,ncol=4,breakPar

        #  grid is (yN,xN) - y=0 is at the top,  so dum
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(ncol*3.2,nrow*2.6))
        assert U.shape[1]==Z.shape[1]

        j=0
        for iPar in range(0,nPar):
            j=iPar
            jOff=1
            if iPar >=breakPar:
                jOff=2*breakPar
                j=iPar-breakPar+1
            #print('dd',iPar,j,jOff)
            ax1 = self.plt.subplot(nrow,ncol, j+jOff); ax1.grid()
            ax2 = self.plt.subplot(nrow,ncol, j+ncol*1+jOff); ax2.grid()

            u=U[:,iPar]
            z=Z[:,iPar]
            unitStr='(a.u.)'
            mm2=self.maxU
            mm1=-mm2
            mm3=self.maxU/1.  # adjust here of you want narrow range for 1D residues
            binsX=np.linspace(mm1,mm2,30)
            rho,sigRho=get_rho_correl(u,z)
            
            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX, cmin=1,
                                              cmap = colMap)
            # beutification
            self.plt.colorbar(img, ax=ax1)
            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=1,transform=ax1.transAxes) #diagonal
            #

            ax1.set(title='%d:%s'%(iPar,parName[iPar]), xlabel='pred %s'%unitStr, ylabel='true')
            ax1.text(0.2,0.1,'rho=%.3f +/- %.3f'%(rho,sigRho),transform=ax1.transAxes)

            x0=0.07
            if iPar==0: ax1.text(x0,0.82,tit,transform=ax1.transAxes)
            if iPar==1: ax1.text(x0,0.92,'n=%d'%(u.shape[0]),transform=ax1.transAxes)
            if iPar==0: ax1.text(x0,0.92,tit2,transform=ax1.transAxes)

            # .... compute residue
            zmu=z-u
            resM=zmu.mean()
            resS=zmu.std()

            # ..... 1D residue
            binsU= np.linspace(-mm3,mm3,50)
            ax2.hist(zmu,bins=binsU)

            ax2.set(title='res: %.2g+/-%.2g'%(resM,resS), xlabel='pred-true %s'%unitStr, ylabel='traces')
            ax2.text(0.15,0.8,parName[iPar],transform=ax2.transAxes)

            if breakPar>0: continue
            # ......  2D residue
            ax3 = self.plt.subplot(nrow,ncol, j+ncol*2+jOff); ax3.grid()
            zsum,xbins,ybins,img = ax3.hist2d(z,zmu,bins=[binsX,binsX/2], cmin=1,cmap = colMap)
            self.plt.colorbar(img, ax=ax3)
            ax3.set( xlabel='pred %s'%unitStr, ylabel='pred-true %s'%unitStr)
            ax3.axhline(0, color='green', linestyle='--')
            ax3.set_xlim(mm1,mm2) ; ax3.set_ylim(mm1,mm2)


#............................
    def plot_data_fparams(self,U,tit,figId=6):
        figId=self.smart_append(figId)
        parName=self.metaD['parName']
        nPar=self.metaD['numPar']
        
        nrow,ncol=2,int(nPar/2+1.5) # displays 2 pars per plot
 
        fig=self.plt.figure(figId,facecolor='white', figsize=(3.2*ncol,2.5*nrow))

        mm=self.maxU
        binsX= np.linspace(-mm,mm,30)

        j=1
        for i in range(0,nPar,2):
            iPar=min(i,U.shape[1]-2) # account for odd num of params
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            self.plt.colorbar(img, ax=ax)
            ax.set(title=tit, xlabel='%d: %s'%(iPar,parName[iPar]), ylabel='%d: %s'%(iPar+1,parName[iPar+1]))
            ax.grid()

        for i in range(0,nPar):
            y1=U[:,i]
            ax=self.plt.subplot(nrow,ncol,j) ; j+=1
            ax.hist(y1,bins=50)
            ax.set(xlabel='%d: %s'%(i,parName[i]))
            ax.set_xlim(-mm,mm)
            ax.grid()
