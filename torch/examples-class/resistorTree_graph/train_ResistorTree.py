#!/usr/bin/env python
"""
 read input hd5 tensors
uses self.layer = nn.ModuleList()
uses  LambdaLayer, ReduceLROnPlateau
train net
write net + weights as HD5

# run on 1 Cori w/ GPU-load monitor
module load  pytorch/v1.2.0-gpu
module load cuda

./train_ResistorTree.py --dataSplit   dataSplit_circR9_15n.yaml --design  fc1

50 epochs suffice for convergence 
epoch=49 loss wrk=0.0158, train=0.00582, val=0.00609, LR=0.0003, elaT=64 sec


salloc  -C gpu -n10 --gres=gpu:1 -Adasrepo -t4:00:00 --exclusive
 srun -n1 bash -c '  nvidia-smi '

 salloc  -C gpu -c80 -N1 --gres=gpu:8 -Adasrepo -t4:00:00 --exclusive

Run job on GPU interactively

srun -n1 -c10  bash -c '  nvidia-smi -l 3 >&L.smi & python -u  ./train_graphRegr.py '

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from pprint import pprint 
from Plotter_ResistorTree import Plotter_ResistorTree
from Deep_ResistorTree import Deep_ResistorTree

import sys,os
sys.path.append(os.path.relpath("../../jan_utils/"))
from Util_IO import write_yaml

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--design', dest='modelDesign',  default='fc1',
                        choices=['fc1','cnn1','abc'], help=" model design of the network")

    parser.add_argument('--device',  default='cpu', choices=['cpu','cuda'], 
                        help=' hardware to be used, multi-gpus not supported')
    parser.add_argument("--seedWeights", default=None,
                        help="seed weights only, after model is created")

    parser.add_argument("--dataSplit", default='dataSplit_circR9_15n.yaml',
                        help="how to split data onto train/val/test")

    parser.add_argument("-o","--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    parser.add_argument("-e", "--epochs", type=int, default=3,
                        help="fitting epoch")
    parser.add_argument("-b", "--batch_size", type=int, default=None,
                        help="fit batch_size, default --> hparams")
    parser.add_argument("--dropFrac", type=float, default=None,
                        help="drop fraction at all FC layers, default --> hparams")
    parser.add_argument("-H", "--historyPeriod", type=int, default=5,
                        help="in epochs, compute & save train+val loss.")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disables X-term for batch mode")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    args = parser.parse_args()
    args.prjName='resistorTree'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.seedWeights=='same' :  args.seedWeights=args.outPath

deep=Deep_ResistorTree.trainer(args)
plot=Plotter_ResistorTree(args ,deep.metaD  )
deep.seed_dataLoaders()

#plot_frames(deep.data['val'])
    
deep.build_model()

if args.seedWeights: deep.load_model_state()
deep.train_model()

deep.save_model_full() 
write_yaml(deep.sumRec, deep.outPath+'/'+deep.prjName2+'.sum_train.yaml')

if args.verb>1: pprint(deep.sumRec)

plot.train_history(deep,args) 
plot.display_all('train')
