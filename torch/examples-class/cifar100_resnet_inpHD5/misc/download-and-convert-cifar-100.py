#!/usr/bin/env python
'''
Oryginal from
https://github.com/guillaume-chevalier/caffe-cifar-10-and-cifar-100-datasets-preprocessed-to-HDF5

Each image comes with a "fine" label (the class to which it belongs) and a "coarse" label (the superclass to which it belongs).

Note, I have chose to export both labels for cifar10:
"fine" label (the class to which it belongs) and a "coarse" label (the superclass to which it belongs).
See:
https://www.cs.toronto.edu/~kriz/cifar.html
'''

import copy
import os
from subprocess import call

import numpy as np
import sklearn

import sklearn.linear_model

import h5py

#...!...!..................
def unpickle(file):
    import pickle
    fo = open(file, 'rb')
    dict = pickle.load(fo, encoding='latin1')
    fo.close()
    return dict

#...!...!..................
def shuffle_data(data, labels):
    #data, _, labels, _ = sklearn.cross_validation.train_test_split(
    #    data, labels, test_size=0.0, random_state=42
    #)
    return data, labels

#...!...!..................
def load_data_100(dom):
    d = unpickle(
        os.path.join(cifar_python_directory, dom)
    )
    print('ddd',d.keys())
  
    i=1
    for k in [ 'filenames', 'batch_label','fine_labels', 'coarse_labels']:
        print('k',k,len(d[k]),d[k][i], type(d[k][i]))

    data=d['data']    ; print('da',data.shape,data.dtype)
    flabels=np.array(d['fine_labels'],dtype='int32')
    clabels=np.array(d['coarse_labels'])
    
    length = len(flabels)

    #data, labels = shuffle_data(data, labels)
    return data.reshape(length, 3, 32, 32), flabels, clabels


#=================================
#=================================
#  M A I N 
#=================================
#=================================

print("")
dataPath='./'

tarBall="cifar-100-python.tar.gz"; dataRaw='cifar-100-python/'  
fullBall=dataPath+tarBall

dataH5=dataPath+'cifar-100-h5/'

print("Downloading... to "+fullBall)

if not os.path.exists(fullBall):
    call(
        "wget http://www.cs.toronto.edu/~kriz/"+tarBall,
        shell=True
    )
    print("Downloading done.\n")
else:
    print("Dataset already downloaded. Did not download twice.\n")


print("Extracting... to"+dataRaw)
cifar_python_directory = os.path.abspath(dataRaw)
if not os.path.exists(cifar_python_directory):
    call(
        "tar -zxvf "+fullBall,
        shell=True
    )
    print("Extracting successfully done to {}".format(cifar_python_directory))
else:
    print("Dataset already extracted. Did not extract twice.\n")


cifar_caffe_directory = os.path.abspath(dataH5)
print("Converting... from"+cifar_python_directory)

# cifar 100:
X, fy,cy = load_data_100(  "train")

Xt, fyt,cyt = load_data_100("test")

print("INFO: each dataset's element are of shape 3*32*32:")
print('"print(X.shape)" --> "{}"\n'.format(X.shape))
print("From the Caffe documentation: ")
print("The conventional blob dimensions for batches of image data "
      "are number N x channel K x height H x width W.\n")

if not os.path.exists(cifar_caffe_directory):
    os.makedirs(cifar_caffe_directory)
else:
    print('M:Warn - h5 dir already exist')


print("Data is fully loaded, now truly converting.")
print('example X', X.shape, X.dtype,X[0])
print('example fine_Y', fy.shape, fy.dtype,fy[:10])
print('example coarse_Y', cy.shape, cy.dtype,cy[:10])


train_filename = os.path.join(cifar_caffe_directory, 'cifar100_train.h5')
test_filename = os.path.join(cifar_caffe_directory, 'cifar100_test.h5')

comp_kwargs = {'compression': 'gzip', 'compression_opts': 1}
# Train
with h5py.File(train_filename, 'w') as f:
    f.create_dataset('data', data=X, **comp_kwargs)
    f.create_dataset('fine_label', data=fy.astype(np.int_), **comp_kwargs)
    f.create_dataset('coarse_label', data=cy.astype(np.int_), **comp_kwargs)

# Test
with h5py.File(test_filename, 'w') as f:
    f.create_dataset('data', data=Xt, **comp_kwargs)
    f.create_dataset('fine_label', data=fyt.astype(np.int_), **comp_kwargs)
    f.create_dataset('coarse_label', data=cy.astype(np.int_), **comp_kwargs)

print('Conversion successfully done to "{}".\n'.format(cifar_caffe_directory))
