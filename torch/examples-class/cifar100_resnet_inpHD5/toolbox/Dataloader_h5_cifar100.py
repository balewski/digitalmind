__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
reads all data at once and serves them from RAM
- optimized for mult-GPU training
- only used block of data  from each H5-file
- reads data from multiple files
- allows for in-fly transformation

    Shuffle: only  all samples after compleated read

'''

import time,  os
import random
import h5py
import numpy as np

import copy
from torch.utils.data import Dataset, DataLoader
import torch 
# this data loader reads all data upon start, there is no distributed sampler

import logging

#...!...!..................
def get_data_loader(params, is_distributed, is_train, verb=1):

  #print('GDL:ini', dataPath, distributed, is_train)
  # GDL:ini /global/cscratch1/sd/balewski/tmp_digitalMind/torchData True True

  cf={}   
  cf['dataPath']=params.data_path
  cf['domain']='train' if is_train else 'test'
  cf['h5nameTemplate']='cifar100_*.h5'
  cf['myRank']=params.world_rank
  cf['cifar100_mean']=params.cifar100_mean
  cf['cifar100_std']=params.cifar100_std
  cf['numRanks']=params.world_size
  cf['localBS']=params.local_batch_size
  dataset=  Dataset_h5_cifar100(cf,verb)

  shuffle=is_train  # use False for reproducibility
  dataloader = DataLoader(dataset,
                          batch_size=int(params.local_batch_size) if is_train else int(params.valid_batch_size_per_gpu),
                          num_workers=params.num_data_workers,
                          shuffle=shuffle,
                          drop_last=True,
                          pin_memory=torch.cuda.is_available())

  return dataloader

#-------------------
#-------------------
#-------------------
class Dataset_h5_cifar100(Dataset):
    
    def __init__(self, conf0,verb=1):
        self.conf=copy.deepcopy(conf0)  # the input conf0 is reused later in the upper level code
        self.verb=verb

        self.openH5()
        if self.verb and 0:
            print('\nDS-cnst name=%s  shuffle=%r BS=%d steps=%d myRank=%d numSampl/hd5=%d'%(self.conf['name'],self.conf['shuffle'],self.localBS,self.__len__(),self.conf['myRank'],self.conf['numSamplesPerH5']),'H5-path=',self.conf['dataPath'])
        assert self.numLocFrames>0
        assert self.conf['myRank']>=0
 
        if self.verb :
            logging.info(' DS:load-end numLocSamp=%d'%(self.numLocFrames))
            #print(' DS:Xall',self.data_frames.shape,self.data_frames.dtype)
            #print(' DS:Yall',self.data_labels.shape,self.data_labels.dtype)
            

#...!...!..................
    def Xsanity(self,localBS):
        stepPerEpoch=int(np.floor( self.numLocFrames/ localBS))
        if  stepPerEpoch <1:
            print('\nDS:ABORT, Have you requested too few samples per rank?, numLocFrames=%d, BS=%d  name=%s'%(self.numLocFrames, localBS,self.conf['name']))
            exit(67)
        # all looks good
        
#...!...!..................
    def openH5(self):
        cf=self.conf
        fnameTmpl=cf['dataPath']+cf['h5nameTemplate']
        inpF=fnameTmpl.replace('*',cf['domain'])
     
        if self.verb>0 : logging.info('DS:fileH5 %s  rank %d of %d '%(inpF,cf['myRank'],cf['numRanks']))
        
        if not os.path.exists(inpF):
            print('FAILD, missing HD5',inpF)
            exit(22)

        startTm0 = time.time()

        # = = = READING HD5  start
        h5f = h5py.File(inpF, 'r')
        Xshape=h5f['data'].shape
        totSamp=Xshape[0]
        locStep=int(totSamp/cf['numRanks']/cf['localBS'])
        locSamp=locStep*cf['localBS']
        #print('totSamp=%d locStep=%d'%(totSamp,locStep))
        assert locStep>0
        maxShard= totSamp// locSamp
        assert maxShard>=cf['numRanks']
                    
        # chosen shard is rank dependent, wraps up if not sufficient number of ranks
        myShard=self.conf['myRank'] %maxShard
        sampIdxOff=myShard*locSamp

        if self.verb: logging.info('DS:file dom=%s myShard=%d, maxShard=%d, sampIdxOff=%d allXshape=%s'%(cf['domain'],myShard,maxShard,sampIdxOff,str(Xshape)))
        # data reading starts
        self.data_frames=h5f['data'][sampIdxOff:sampIdxOff+locSamp].astype('float32')
        self.data_labels=h5f['fine_label'][sampIdxOff:sampIdxOff+locSamp]#.astype('int32')
        h5f.close()
        # = = = READING HD5  done

        # normalize data   here
        self.data_frames-=cf['cifar100_mean']
        self.data_frames/=cf['cifar100_std']

        if 0: # check normalization
            xm=np.mean(self.data_frames)
            xs=np.std(self.data_frames)
            print('xm',xm,xs,myShard,cf['domain'])
            ok99
        
        self.numLocFrames=locSamp

    def __len__(self):        
        return self.numLocFrames


    def __getitem__(self, idx):
        # print('DSI:',idx,self.conf['name'],self.cnt); self.cnt+=1
        assert idx>=0
        assert idx< self.numLocFrames
        X=self.data_frames[idx]
        Y=self.data_labels[idx]
        return (X,Y)


