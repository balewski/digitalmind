#!/usr/bin/env python
'''
Not running on CPUs !

export MASTER_ADDR=`hostname --ip-address`
echo use MASTER_ADDR=$MASTER_ADDR

Golden settings:
 localBS=128, nGpu=4, lr=0.28 --> 40 epochs in 7 min, INFO - train acc1=1.0000, valid acc1=0.5352

Runs   1 GPU:  srun -n1 ./train.py --design=bs1024-cori
Speed: Avg img/sec: 3706.3
rm -rf /global/cscratch1/sd/balewski/tmp_digitalMind/torchOut/bs1024-opt


Run on 4 GPUs on 1 node
 salloc -N1 -C gpu  -c 10 --gpus-per-task=1 --ntasks-per-node=4  -t4:00:00 

Full node
 salloc -N1  -C gpu  -c 80   --gpus-per-task=8  --ntasks-per-node=1 --exclusive  -t4:00:00   


'''

import sys,os
sys.path.append(os.path.abspath("toolbox"))

import argparse
from pprint import pprint

#from TrainerP import Trainer  # contaain NVTX-profiler
from TrainerS import Trainer  # profiler code was gutted out for simplicity of the code
from YParams import YParams
import  logging

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument("--hpar_config", default='./hpar_cifar100.yaml', type=str)
  parser.add_argument("--design", default='bs1024-cori', type=str)
  parser.add_argument("--out_path", default=None, type=str)
  parser.add_argument("--facility", default='corigpu', type=str)
  args = parser.parse_args()
  params = YParams(os.path.abspath(args.hpar_config), args.design)

  os.environ['MASTER_PORT'] = "8885"
  
  if args.facility=='summit':
    import subprocess
    get_master = "echo $(cat {} | sort | uniq | grep -v batch | grep -v login | head -1)".format(os.environ['LSB_DJOB_HOSTFILE'])
    os.environ['MASTER_ADDR'] = str(subprocess.check_output(get_master, shell=True))[2:-3]
    os.environ['WORLD_SIZE'] = os.environ['OMPI_COMM_WORLD_SIZE']
    os.environ['RANK'] = os.environ['OMPI_COMM_WORLD_RANK']
    params['local_rank'] = int(os.environ['OMPI_COMM_WORLD_LOCAL_RANK'])
  else:
    os.environ['RANK'] = os.environ['SLURM_PROCID']
    os.environ['WORLD_SIZE'] = os.environ['SLURM_NTASKS']
    params['local_rank'] = int(os.environ['SLURM_LOCALID'])

  params['world_size'] = int(os.environ['WORLD_SIZE'])

  if params['local_rank']==0:
    logging.info('M: MASTER_ADDR=%s WORLD_SIZE=%s RANK=%s'%(os.environ['MASTER_ADDR'] ,os.environ['WORLD_SIZE'], os.environ['RANK'] ))

  # setup output directory 
  if args.out_path==None:
    expDir = os.path.join(params.out_path, args.design)
  else:
    expDir = args.out_path
  params['out_path']=expDir

  #params['testJan']=11  # good way
  #params.update_params({'testJan':44})  # good way
  #params.testJan=22  # bad way - never assign a value this way, but you can alwasy do x=params.testJan
  
  trainer = Trainer(params)
  trainer.train()
