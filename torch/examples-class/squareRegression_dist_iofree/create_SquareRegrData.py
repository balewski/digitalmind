#!/usr/bin/env python
""" 
Generates 2D images , saves data as HD5
image_X, phys_Y, unit_Y, Aux
 mapped to : X,U --> Z for training
 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import sys,os
sys.path.append(os.path.relpath("toolbox/"))

from Plotter_SquareRegr import Plotter_SquareRegr
from Util_IOfunc import write_yaml, read_yaml,  write_data_hdf5
from Util_SquareRegr import create_frames

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-d", "--dataPath",  default='data',
                        help="output HD5+TFrec path")

    parser.add_argument("-N", "--name",  default='data0',
                        help="core name of produced file")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-n", "--events", default=500, type=int,
                        help="events for training, use 0 for all")

    parser.add_argument("--noise", default=0.01, type=float,
                        help="pepper noise probability per pixel")

    args = parser.parse_args()
    args.prjName='squareRegr'

    if args.outPath=='same': args.outPath=args.dataPath

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
geomF='misc/geom.'+args.prjName+'.yaml'

geomD=read_yaml(geomF)
print('M:geom',geomD)

dataD,metaD=create_frames(geomD,args)
# save meta-data file
metaF=args.dataPath+'/tmpmeta.'+args.prjName+'.yaml'
write_yaml(metaD,metaF)


outF5=args.dataPath+'/'+args.prjName+'.'+args.name+'.h5'
write_data_hdf5(dataD,outF5)

plot=Plotter_SquareRegr(args,{'createInfo':metaD} )

plot.frames(dataD,'test')
plot.display_all('gener')

