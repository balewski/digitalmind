#!/usr/bin/env python3
""" 
testing performance of dataLoader
read test data from HD5 file stored in data2/

It needs valid config: data2/meta.squareRegr_december2.yaml

testDataLoader_SquareRegr.py 

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
from pprint import pprint

import sys,os
sys.path.append(os.path.abspath("toolbox"))
from Deep_SquareRegr import Deep_SquareRegr
from Dataset_SquareRegr  import Dataset_SquareRegr
from torch.utils.data import  DataLoader

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-d", "--dataPath",default='data3/',help="path to input")

    parser.add_argument("--dataTag",  default='december5',help="data config name")

    parser.add_argument("--dom",default='train', help="domain is the dataset for which predictions are made",choices=['train','test','val'])

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")

    parser.add_argument("-n", "--localSamples", type=int, default=2000, help="samples to read")


    args = parser.parse_args()
    args.prjName='squareRegr'
    args.dataPath+='/'
    args.outPath+='/'
    args.modelDesign='cnn1'
    args.designPath='./'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args



#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_SquareRegr.predictor(args)
dom=args.dom
dstConf={}
for x in [ 'dataList','h5nameTemplate','inputShape','outputSize']:
    dstConf[x]=deep.metaD['dataInfo'][x]
dstConf['numLocalSamples']=args.localSamples
dstConf['dataPath']=deep.dataPath
dstConf['shuffle']=True  # use False for reproducibility
dstConf['x_y_aux']=True # it will produce 3 np-arrays per call
dstConf['name']='DST-'+dom
dstConf['domain']=dom
dstConf['myRank']=12

print('\nM:Create torch-Dataset instance')
pprint(dstConf)
inpDst=Dataset_SquareRegr(dstConf,verb=2)
localBS=8
numLocalCPU=2  # -c10 locks 5 physical cpu cores
inpDst.sanity(localBS)


print('\nCreate torch-DataLoader instance & test it')
trainLdr = DataLoader(inpDst, batch_size=localBS, shuffle=True, num_workers=numLocalCPU, pin_memory=True)
xx, yy,aux = next(iter(trainLdr))
print('test_dataLoader 1 batch: X:',xx.shape,'Y:',yy.shape)
print('Y[:,]',yy[:,0])

steps=len(trainLdr)

print('\nM:start processing dom=',dom,' localSamples=',args.localSamples,' steps=',steps, 'localBS=',localBS)

time.sleep(5) # too see memory usage, tmp
startT0 = time.time()
uL=[]
for i in range(steps+1):
    x,u,aux =next(iter(trainLdr))
    #if i%100==0 :  print('M:%d step of %d done, shapes u,z:'%(i,steps),u.shape)
    uL.append(u)

predTime=time.time() - startT0
print('RAM-read done, samples/k=%d, elaT=%.3f sec,'%(args.localSamples/1000.,predTime))
print('batch shape X:',x.shape,'U:',u.shape)

# add all steps 
U=np.concatenate(tuple(uL),axis=0)

avrU=np.mean(U,axis=0)
stdU=np.std(U,axis=0)
parNL=deep.metaD['dataInfo']['outputName']
print('got U avr,std:',U.shape)
for i in range(avrU.shape[0]):
    print("idx=%d  avr=%.3f  std=%.3f  %s"%(i, avrU[i],stdU[i],parNL[i]))









