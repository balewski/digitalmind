#!/usr/bin/env python
"""
Runs on CPU,  1 GPU, or multipl GPUs
Uses TCP/IP for communication

 read input hd5 tensors
uses self.layer = nn.ModuleList()
uses  LambdaLayer, ReduceLROnPlateau
train net
write net + weights as HD5

Tip from Steve F.: add this line to see NCCL debugging  
export NCCL_DEBUG=info

SEE ALSO README !!!


Interactive 2x2 GPU:
   salloc -N2  -C gpu  -c 10  --gres=gpu:4 --ntasks-per-node=4  -t4:00:00 
2 full nodes (16 GPUs)
  salloc -N 2  -c 10  --gres=gpu:8  --ntasks-per-node=8  --exclusive -C gpu  -t 3:59:00 


   line=`ifconfig |grep 'ib1:' -A1 |tail -n1`
   headIp=`echo $line | cut -f2 -d\ `
   export MASTER_ADDR=$headIp
   export MASTER_PORT=8888
   echo headIP=$headIp


srun -n2 -l python -u ./train_SquareRegr.py --device cuda --localSamples 64000 --localBS 512 --epochs 10  --dataPath data3/

   run on 1-8 GPUs preserving total samples and global BS:


Monitor GPU load:
srun -n 2   bash -c ' if [ ${SLURM_LOCALID} -eq 0 ] ; then   echo locRank0 on `hostname`;  nvidia-smi -l 10 >&out/L.smi_`hostname` ; fi  &  python -u  ./train_SquareRegr.py --device cuda -n20000 --localBS  256  -e 30 '


Predict:

      srun -n1 -l python -u ./predict_SquareRegr.py --device cuda ??

You can copare this code w/ Mustafa example:
https://github.com/MustafaMustafa/ssl-sky-surveys/blob/main/train.py#L151-L155

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from pprint import pprint 

import sys,os
sys.path.append(os.path.relpath("toolbox/"))
from Plotter_SquareRegr import Plotter_SquareRegr
from Deep_SquareRegr import Deep_SquareRegr
from Util_IOfunc import  read_yaml,write_yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--design', dest='modelDesign',  default='cnn1', help=" model design of the ML-network")
    parser.add_argument("--dataTag",  default='december5',help="data config name")
    parser.add_argument('--device',  default='cpu', choices=['cpu','cuda'],  help=' hardware to be used, multi-gpus not supported')
    parser.add_argument("--seedWeights", default=None, help="seed weights only, after model is created") # not tested

    parser.add_argument("--designPath",help="location of hpar.yaml defining the model",  default='./')
    parser.add_argument("--dataPath",  default='data6/',help="training data path")

    parser.add_argument("--outPath", default='out',help="output path for plots and tables")

    parser.add_argument("-n", "--localSamples", type=int, default=2000, help="samples to read")

    parser.add_argument("-e", "--epochs", type=int, default=6, help="fitting epoch")
    parser.add_argument("-b", "--localBS", type=int, default=None, help="change localBS, default --> hparams")
    parser.add_argument("--dropFrac", type=float, default=None, help="drop fraction at all FC layers, default --> hparams")
    parser.add_argument("-H", "--historyPeriod", type=int, default=5, help="in epochs, compute & save train+val loss.")

    parser.add_argument('-X', "--noXterm", dest='noXterm',  action='store_true', default=False, help="disables X-term for batch mode")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],  help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-j","--jobId", default=None, help="optional, aux info to be stored w/ summary")


    args = parser.parse_args()
    args.prjName='squareRegr'
    if args.device!='cuda':
        for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.seedWeights=='same' :  args.seedWeights=args.outPath

deep=Deep_SquareRegr.trainer(args)
deep.init_dataLoaders()

if deep.myRank==0 : 
    plot=Plotter_SquareRegr(args ,deep.metaD )
    dom='val'
    xx, yy, _ = next(iter(deep.dataLoader[dom]))
    dataA={dom+'_image_X':xx,dom+'_unit_Y':yy}
    plot.frames(dataA,dom)
    #plot.display_all('train')
    
deep.build_model()

if args.seedWeights: deep.load_model_state()
deep.train_model()
print('M:done training rank=',deep.myRank)

if deep.myRank!=0:  exit(0)  # stop all other ranks here

deep.save_model_full() 
write_yaml(deep.sumRec, deep.outPath+'/'+deep.prjName2+'.sum_train.yaml')

if args.verb>1: pprint(deep.sumRec)

plot.train_history(deep,args)
print('M:done plotting rank=',deep.myRank)
plot.display_all('train')
