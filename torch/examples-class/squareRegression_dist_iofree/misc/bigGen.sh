#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

outPath=/global/cscratch1/sd/balewski/test1/squareRegrData6/
echo path=$outPath
mkdir -p $outPath

numEve=100000

for k in `seq 1 20`; do
     time ./create_SquareRegrData.py --noise 0.70 --events $numEve --name data_c$k --dataPath $outPath -X
    #exit
done

# note, writing hd5 takes longer than data generation
