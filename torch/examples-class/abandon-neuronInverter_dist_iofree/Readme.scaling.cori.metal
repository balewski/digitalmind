= = = = = = = =

Data: witness2c, 0.98M train samples
4 workers/loader, localBS=512

- - - - - Interactive on 1 GPU
srun -n1 ./train_dist.py   --cellName witness2c   --design  hpar_gcref   --outPath ./out --epochs 3

INFO - T:rank 0 of 1, data loader initialized
INFO - T:train-data: 1875 steps, localBS=512, globalBS=512
INFO - T:valid-data: 234 steps

INFO - Epoch:  2, step: 1250, Avg samp/msec/gpu: 46.0
INFO - Epoch 2 took 25.6 sec, avr=25.4 +/-0.1 sec/epoch, elaT=77.1 sec, nGpu=1, LR=3.00e-04, Loss: train=0.0549, val=0.0486

- - - - - Interactive on 2 GPUs
INFO - T:rank 0 of 2, data loader initialized
INFO - T:train-data: 937 steps, localBS=512, globalBS=1024
INFO - T:valid-data: 117 steps
.
INFO - Epoch:  2, step: 936, Avg samp/msec/gpu: 37.1
INFO - Epoch 2 took 15.7 sec, avr=15.7 +/-0.0 sec/epoch, elaT=48.9 sec, nGpu=2, LR=3.00e-04, Loss: train=0.0565, val=0.0488


= = = = = = = =



= = = = = = = =





Cori-GPU, 1 node, -- exclusive 
one-cell training: bbp153, localBS=512, 8 epochs

***1 GPU:
INFO - Epoch:  8, step: 672, Avg img/sec/gpu: 50740.8
INFO - Time taken for epoch 8 is 13.5 sec, elaT=106.0 sec
INFO - train loss=0.0470, valid loss=0.0519


***2 GPUs
INFO - T:rank 0 of 2, data loader initialized
INFO - T:train-data: 504 steps, localBS=512, globalBS=1024
INFO - T:valid-data: 63 steps

INFO - Epoch:  8, step: 336, Avg img/sec/gpu: 40923.8
INFO - Time taken for epoch 8 is 8.8 sec, elaT=71.4 sec
INFO - train loss=0.0472, valid loss=0.0481


***4 GPUs:
INFO - T:rank 0 of 4, data loader initialized
INFO - T:train-data: 252 steps, localBS=512, globalBS=2048
INFO - T:valid-data: 31 steps

INFO - Epoch:  8, step: 168, Avg img/sec/gpu: 39515.5
INFO - Time taken for epoch 8 is 4.6 sec, elaT=39.1 sec
INFO - train loss=0.0487, valid loss=0.0531


***8 GPUs:
INFO - T:rank 0 of 8, data loader initialized
INFO - T:train-data: 126 steps, localBS=512, globalBS=4096
INFO - T:valid-data: 15 steps

INFO - Epoch:  8, step:  84, Avg img/sec/gpu: 33093.6
INFO - save_checkpoint for epoch 8 , val-loss=0.0494
INFO - Time taken for epoch 8 is 2.9 sec, elaT=24.3 sec
INFO - train loss=0.0534, valid loss=0.0494

*****************
DO full training on 8 GPUs w/ 4 probes , 80 epoch

INFO - Epoch: 80, step:  84, Avg img/sec/gpu: 33288.2
INFO - Time taken for epoch 80 is 2.9 sec, elaT=228.9 sec
INFO - train loss=0.0263, valid loss=0.0326

*****************
DO full training on 2x4 GPUs w/ 1 probe , 80 epoch
NFO - save_checkpoint for epoch 2 , val-loss=0.0653
INFO - Time taken for epoch 2 is 18.9 sec, elaT=38.3 sec


