#!/bin/bash -l
# common job script for CoriGpu and Perlmutter using Shifter
#SBATCH --time=2:50:00  -J ni_h5
#SBATCH -C gpu
#SBATCH --image=nersc/pytorch:ngc-21.08-v1
#SBATCH -o out/slurm-%j.out
#-SBATCH  -x cgpu08 # block sick nodes
#---CORI_GPU---
#-SBATCH  -N1 --ntasks-per-node=1  --gpus-per-task=1  --cpus-per-task=10
#SBATCH -N8 --ntasks-per-node=8 --gpus-per-task=1 --cpus-per-task=10 --exclusive
#---PERLMUTTER---
#-SBATCH -N4 --ntasks-per-node=4 --gpus-per-task=1 --cpus-per-task=32 --exclusive 
# - - - E N D    O F    SLURM    C O M M A N D S

#cellName=practice10c  # has 4.8M training samples
cellName=witness2c # has 1M training samples
#cellName=bbp153  # has 0.5M training samples
design=hpar_gcref  # reference jobs for Graphcore

nprocspn=${SLURM_NTASKS_PER_NODE}
#nprocspn=1  # special case for partial use of full node

N=${SLURM_NNODES}
G=$[ $N * $nprocspn ]
export MASTER_ADDR=`hostname`
echo S: job=${SLURM_JOBID} MASTER_ADDR=$MASTER_ADDR G=$G  N=$N 
nodeList=$(scontrol show hostname $SLURM_NODELIST)
echo S:node-list $nodeList

# grab some variables from environment - if defined
[[ -z "${NEUINV_INIT_LR}" ]] && initLRstr=" --initLR 1e-2 " || initLRstr=" --initLR ${NEUINV_INIT_LR} "
[[ -z "${NEUINV_WRK_SUFIX}" ]] && wrkSufix=$SLURM_JOBID || wrkSufix="${NEUINV_WRK_SUFIX}"
[[ -z "${NEUINV_JOBID}" ]] && jobId=$SLURM_JOBID || jobId="G${G}_${NEUINV_JOBID}"
env |grep NEUINV

wrkSufix=scanLR/G${G}_lr${NEUINV_INIT_LR}

if [[  $NERSC_HOST == cori ]]   ; then
    echo "on Cori-GPU"
    facility=corigpu

elif [[  $NERSC_HOST == perlmutter ]]   ; then
    echo "on Perlmutter-fixme"
    facility=perlmutter
    # bash -c 'printf "#include <stdio.h>\nint main() {  cudaFree(0);printf(\"cudaFree-done\"); }" > dummy.cu && nvcc -o dummy.exe dummy.cu'
    #  opening and closing a GPU context on each node to reset GPUs
    time srun -n$N -l --ntasks-per-node=1 toolbox/dummy.exe
fi


wrkDir0=$SCRATCH/tmp_digitalMind/neuInv/september/
wrkDir=$wrkDir0/$wrkSufix

echo "S:cellName=$cellName  initLRstr=$initLRstr jobId=$jobId  wrkSufix=$wrkSufix wrkDir=$wrkDir" 
date

export CMD=" python -u   train_dist.py --cellName $cellName   --facility $facility  --outPath ./out --design $design --jobId $jobId  $initLRstr "

echo CMD=$CMD

codeList="  train_dist.py  driveOneTrain.sh  toolbox/ batchShifter.slr  $design.yaml  "

outPath=$wrkDir/out
mkdir -p $outPath
cp -rp $codeList  $wrkDir
cd  $wrkDir
echo lsfPWD=`pwd`

echo "starting  jobId=$jobId neurInv 2021-08 " `date` " outPath= $outPath"

time srun -n $G  shifter  bash  toolbox/driveOneTrain.sh  >& log.train

wait
echo S:done
date
