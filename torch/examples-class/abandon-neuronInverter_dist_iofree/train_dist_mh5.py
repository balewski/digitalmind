#!/usr/bin/env python
'''
Not running on CPUs !

1 or many GPU training
one or many input HD5 files split in shards evenly across all ranks (aka GPUs)

export MASTER_ADDR=`hostname --ip-address`
echo use MASTER_ADDR=$MASTER_ADDR

Runs   1 GPU:  srun -n1 train_dist_mh5.py 
or  srun -n2 train_dist_mh5.py 

Run on 4 GPUs on 1 node
 salloc -N1 -C gpu  -c 10 --gpus-per-task=1 --ntasks-per-node=4  -t4:00:00 

Full node
 salloc -N1  -C gpu  -c 10   --gpus-per-task=1  --ntasks-per-node=8 --exclusive  -t4:00:00   

'''

import sys,os
sys.path.append(os.path.abspath("toolbox"))
from Util_IOfunc import read_yaml, write_yaml

import argparse
from pprint import pprint
from Trainer import Trainer 
import logging
import torch
import torch.distributed as dist

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == '__main__':
  logging.basicConfig(format='%(levelname)s - %(message)s', level=logging.INFO)
  parser = argparse.ArgumentParser()
  parser.add_argument("--design", default='hpar_ontra.yaml', type=str)
  parser.add_argument("-o","--outPath", default='out/', type=str)
  parser.add_argument("--facility", default='corigpu', type=str)
  parser.add_argument("--cellName", type=str, default=['bbp153','bbp102'],nargs="+", help="cell shortName, can be  list ")
  parser.add_argument("--numInpChan",default=None, type=int, help="if defined, reduces num of input channels")
  parser.add_argument("--globalTrainSamples",default=500*1024, type=int, help="if defined, reduces num of input samples")
  args = parser.parse_args()
  
  params = read_yaml( args.design)
  params['design']=args.design
  
  os.environ['MASTER_PORT'] = "8884"

  #print('M:faci',args.facility)
  if args.facility=='summit':
    import subprocess
    get_master = "echo $(cat {} | sort | uniq | grep -v batch | grep -v login | head -1)".format(os.environ['LSB_DJOB_HOSTFILE'])
    os.environ['MASTER_ADDR'] = str(subprocess.check_output(get_master, shell=True))[2:-3]
    os.environ['WORLD_SIZE'] = os.environ['OMPI_COMM_WORLD_SIZE']
    os.environ['RANK'] = os.environ['OMPI_COMM_WORLD_RANK']
    params['local_rank'] = int(os.environ['OMPI_COMM_WORLD_LOCAL_RANK'])
  else:
    os.environ['RANK'] = os.environ['SLURM_PROCID']
    os.environ['WORLD_SIZE'] = os.environ['SLURM_NTASKS']
    params['local_rank'] = int(os.environ['SLURM_LOCALID'])

  params['world_size'] = int(os.environ['WORLD_SIZE'])

  if params['local_rank']==0:
    logging.info('M: MASTER_ADDR=%s:%s WORLD_SIZE=%s RANK=%s'%(os.environ['MASTER_ADDR'],os.environ['MASTER_PORT'] ,os.environ['WORLD_SIZE'], os.environ['RANK'] ))

  logging.info('M:locRank: %d of %d, facility=%s'%(params['local_rank'],params['world_size'],args.facility))
    
  
  # refine multi-gpu configuration
  tmp_batch_size=params.pop('batch_size')
  if params['const_local_batch']: # faster but LR changes w/ num GPUs
    params['local_batch_size'] =tmp_batch_size 
    params['global_batch_size'] =tmp_batch_size*params['world_size']
  else:
    params['local_batch_size'] = int(tmp_batch_size//params['world_size'])
    params['global_batch_size'] = tmp_batch_size

  params['world_rank'] = 0
  if params['world_size'] > 1:  # multi-GPU training
    torch.cuda.set_device(params['local_rank'])
    dist.init_process_group(backend='nccl', init_method='env://')
    params['world_rank'] = dist.get_rank()
    print('M:locRank:',params['local_rank'])#?,'rndSeed=',torch.seed())

  # capture other args values
  params['cell_name']=args.cellName
  params['num_inp_chan']=args.numInpChan
  # want local samples to be multiplicity of localBS, clip globSamp as needed
  locStep=args.globalTrainSamples//params['world_size']//params['local_batch_size'] 
  params['globalTrainSamples']=locStep*params['world_size']*params['local_batch_size']
  params['data_path']=params['data_path'][args.facility]
  # setup output directory 
  if args.outPath!=None:
    params['out_path']=args.outPath

  trainer = Trainer(params)
  
  trainer.train()

  if params['world_rank'] == 0:
    sumF=args.outPath+'/sum_train.yaml'
    write_yaml(trainer.sumRec, sumF) # to be able to predict while training continus

    print("M:done")
