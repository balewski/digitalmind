#!/usr/bin/env python
'''
Not running on CPUs !

only 1  GPU configuraton
 
 salloc -N1 -C gpu  -c 10 --gpus-per-task=1 --ntasks-per-node=1  -t4:00:00 

 srun -n1 train_one.py
'''

import sys,os
sys.path.append(os.path.abspath("toolbox"))
from Util_IOfunc import read_yaml, write_yaml

import argparse
from pprint import pprint
from Trainer import Trainer 
import  logging
import torch
import torch.distributed as dist

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == '__main__':
  logging.basicConfig(format='%(levelname)s - %(message)s', level=logging.INFO)
  parser = argparse.ArgumentParser()
  parser.add_argument("--design", default='hpar_ontra.yaml', type=str)
  parser.add_argument("-o","--outPath", default='out/', type=str)
  parser.add_argument("--facility", default='corigpu', type=str)
  parser.add_argument("--cellName", type=str, default='bbp153', help="cell shortName ")
  parser.add_argument("--numInpChan",default=None, type=int, help="if defined, reduces num of input channels")

  args = parser.parse_args()
  params = read_yaml( args.design)
  params['design']=args.design
  params['world_size'] = 1
  params['world_rank'] = 0
  params['local_rank'] = 0
  params['cell_name']=args.cellName
  params['num_inp_chan']=args.numInpChan
  tmp_batch_size=params.pop('batch_size')
  params['local_batch_size'] =tmp_batch_size
  params['global_batch_size'] =tmp_batch_size

  # setup output directory 
  if args.outPath!=None:
    params['out_path']=args.outPath
  params['data_path']=params['data_path'][args.facility]

  trainer = Trainer(params)
  trainer.train()

  sumF=args.outPath+'/sum_train.yaml'
  write_yaml(trainer.sumRec, sumF) # to be able to predict while training continus
  
  print("M:done")
