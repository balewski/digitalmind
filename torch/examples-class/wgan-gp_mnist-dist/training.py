import imageio
import numpy as np
import torch
import torch.nn as nn
from torchvision.utils import make_grid
from torch.autograd import Variable
from torch.autograd import grad as torch_grad
from util import waterMarkImg
from time import time

import logging
import torch.distributed as dist

#...!...!..................
def average_gradients(model):
    world_size=dist.get_world_size()
    for param in model.parameters():
        dist.all_reduce(param.grad.data, op=dist.ReduceOp.SUM)
        param.grad.data /= world_size


#-------------------
#-------------------
#-------------------
class Trainer():
    def __init__(self, generator, discriminator, gen_optimizer, dis_optimizer,
                 params,gp_weight=10, critic_iterations=5, print_every=400):
        self.G = generator.cuda()
        self.G_opt = gen_optimizer
        self.D = discriminator.cuda()
        self.D_opt = dis_optimizer
        self.losses = {'G': [], 'D': [], 'GP': [], 'GL2': [],'Tsec':[0.]}
        self.num_steps = 0        
        self.gp_weight = gp_weight
        self.rank=params['world_rank']
        self.params=params
        self.verb=self.rank==0
        
        self.critic_iterations = critic_iterations
        self.print_every = print_every
        self.timeStart=time()


#...!...!..................
    def _critic_train_iteration(self, data):
        # Get generated data
        batch_size = data.size()[0]
        generated_data = self.sample_generator(batch_size)
        # Calculate probabilities on real and generated data
        data = Variable(data).cuda()
        d_real = self.D(data)
        d_generated = self.D(generated_data)
        # Get gradient penalty
        gradient_penalty = self._gradient_penalty(data, generated_data)
        self.losses['GP'].append(gradient_penalty.data)

        # Create total loss and optimize
        self.D_opt.zero_grad()
        d_loss = d_generated.mean() - d_real.mean() + gradient_penalty
        d_loss.backward()
        average_gradients(self.D) # important

        self.D_opt.step()

        # Record loss
        self.losses['D'].append(d_loss.data)

#...!...!..................
    def _generator_train_iteration(self, data):
        
        self.G_opt.zero_grad()

        # Get generated data
        batch_size = data.size()[0]  # [1] are MNIST labels - ignored for this problem
        generated_data = self.sample_generator(batch_size)

        # Calculate loss and optimize
        d_generated = self.D(generated_data)
        #print('GTI:dg',type(d_generated),d_generated.shape)
        g_loss = - d_generated.mean()
        #print('GTI:dl',type(g_loss),g_loss.shape,g_loss)
        g_loss.backward()
        average_gradients(self.G) # important
        self.G_opt.step()

        # Record loss
        self.losses['G'].append(g_loss.data)
        #print('GTI:loss',g_loss.data); ok90

#...!...!..................
    def _gradient_penalty(self, real_data, generated_data):
        batch_size = real_data.size()[0]

        # Calculate interpolation
        alpha = torch.rand(batch_size, 1, 1, 1)
        alpha = alpha.expand_as(real_data).cuda()
        interpolated = alpha * real_data.data + (1 - alpha) * generated_data.data
        interpolated = Variable(interpolated, requires_grad=True).cuda()

        # Calculate probability of interpolated examples
        prob_interpolated = self.D(interpolated)

        # Calculate gradients of probabilities with respect to examples
        gradients = torch_grad(outputs=prob_interpolated, inputs=interpolated,
                               grad_outputs=torch.ones(prob_interpolated.size()).cuda() ,
                               create_graph=True, retain_graph=True)[0]

        # Gradients have shape (batch_size, num_channels, img_width, img_height),
        # so flatten to easily take norm per example in batch
        gradients = gradients.view(batch_size, -1)
        gn2=gradients.norm(2, dim=1).mean().data
        #print('ooo',obj1,type(obj1),obj1.data)
        self.losses['GL2'].append(gn2)

        # Derivatives of the gradient close to 0 can cause problems because of
        # the square root, so manually calculate norm and add epsilon
        gradients_norm = torch.sqrt(torch.sum(gradients ** 2, dim=1) + 1e-12)

        # Return gradient penalty
        return self.gp_weight * ((gradients_norm - 1) ** 2).mean()




#...!...!..................
    def _train_epoch(self, data_loader):
        for i, data in enumerate(data_loader):  # i*BS = 900*32=58k samples
            self.G.train(); self.D.train();  # do I need it?
            
            if self.num_steps==0 and self.verb:
                print('TE:data i=',i,len(data),'X:',data[0].shape,'Y:',data[1].shape)
            self.num_steps += 1  
            self._critic_train_iteration(data[0])
            # Only update generator every |critic_iterations| iterations
            if (self.num_steps % self.critic_iterations) == 0:
                self._generator_train_iteration(data[0])

            self.losses['Tsec'].append(time()-self.timeStart)
 
            if i % self.print_every == 0 and self.verb:
                txt='  iter=%4d losses D:%.2f  GP:%.2f  GL2:%.2f rank=%d'%(i,self.losses['D'][-1],self.losses['GP'][-1],self.losses['GL2'][-1],self.rank)

                if self.num_steps > self.critic_iterations:
                    txt+="  G: %.2f"%(self.losses['G'][-1])

                print(txt)
                #break  # cut-off events earlier for testing
                
#...!...!..................
    def train(self, data_loader, epochs, save_training_gif=True):
        if save_training_gif:
            # Fix latents to see how image generation improves during training
            fixed_latents = Variable(self.G.sample_latent(64))
            print('T: one latent:',fixed_latents[0].shape)
            #data=self.G(fixed_latents).cpu().data
            #print('dd',data.shape)
            #waterMarkImg(data,13,4); ok22

            fixed_latents = fixed_latents.cuda()
            training_progress_images = []

        for epoch in range(epochs):
            dist.barrier()

            if self.verb :
                print("Epoch %d  begin elaT=%.2f min"%(epoch ,self.losses['Tsec'][-1]/60.))
                
            self._train_epoch(data_loader)

            if save_training_gif and  (epoch % 5) == 0:
                # Generate batch of images and convert to grid
                data=self.G(fixed_latents).cpu().data
                waterMarkImg(data,epoch,5)
                img_grid = make_grid(data)
                # Convert to numpy and transpose axes to fit imageio convention
                # i.e. (width, height, channels)
                img_grid = np.transpose(img_grid.numpy(), (1, 2, 0))
                # Add image grid to training progress
                training_progress_images.append(img_grid)
                # fix: Lossy conversion from float32 to uint8. Range [0, 1]. Convert image to uint8 prior to saving to suppress this warning.

        if save_training_gif:
            kargs = { 'duration': 0.20, 'loop':1 }
            outF=self.params['out_path']+'/training_%dG_%dep.gif'%(self.params['world_size'],epochs)
            print('saving %s ...'%outF)
            imageio.mimsave(outF, training_progress_images,  **kargs)


#...!...!..................
    def sample_generator(self, num_samples):
        latent_samples = Variable(self.G.sample_latent(num_samples)).cuda()
        generated_data = self.G(latent_samples)
        return generated_data


#...!...!..................
    def sample(self, num_samples):
        generated_data = self.sample_generator(num_samples)
        # Remove color channel
        return generated_data.data.cpu().numpy()[:, 0, :, :]
