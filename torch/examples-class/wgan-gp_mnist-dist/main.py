#!/usr/bin/env python3
# source https://github.com/EmilienDupont/wgan-gp
#
# Cori-GPU:  time srun -n1  python -u  ./main.py
# time srun -n 1   bash -c  '   nvidia-smi -l 10 >&L.smi_`hostname` & python3 -u main.py  '
#

'''
Not running on CPUs !

   module load cgpu
   module load pytorch/1.7.0-gpu

export MASTER_ADDR=`hostname --ip-address`
echo S:use MASTER_ADDR=$MASTER_ADDR

Golden settings:
 ???localBS=128, nGpu=4, lr=0.28 --> 40 epochs in 7 min, INFO - train acc1=1.0000, valid acc1=0.5352

Runs   1 GPU:  srun -n1 ./main.py
Speed: Avg img/sec: 3706.3
rm -rf /global/cscratch1/sd/balewski/tmp_digitalMind/torchOut/bs1024-opt


Run on 4 GPUs on 2 nodes
salloc -N2 -C gpu  -c 10 --gpus-per-task=1 --ntasks-per-node=2  -t4:00:00 

Run on 4 GPUs on 1 node
 salloc -N1 -C gpu  -c 10 --gpus-per-task=1 --ntasks-per-node=4  -t4:00:00 

Full node
 salloc -N1  -C gpu  -c 80   --gpus-per-task=8  --ntasks-per-node=1 --exclusive  -t4:00:00   


'''

import torch
import torch.optim as optim
from dataloaders import get_mnist_dataloaders
from models import Generator, Discriminator
from training import Trainer
from util import write_yaml, MyEmpty
import torch.distributed as dist

import socket  # for hostname
import subprocess
import logging
logging.basicConfig(format='%(levelname)s - %(message)s', level=logging.INFO)

import sys,os
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--globalBS', type=int, default=64,help='global batch size')
parser.add_argument('--epochs', type=int, default=5,help='global batch size')

parser.add_argument('--num-warmup-batches', type=int, default=10,  help='number of warm-up batches that don\'t count towards benchmark')

parser.add_argument("--facility", default='corigpu', type=str)
parser.add_argument("--out_path", default='out/', type=str)
parser.add_argument("--learning_rate", default=1e-4, type=float)


#=================================
#=================================
#  M A I N 
#=================================
#=================================

args = parser.parse_args()
assert torch.cuda.is_available()

params={}
params['data_path']='/global/cscratch1/sd/balewski/tmp_digitalMind/torchData/mnist-gz/'
params['out_path']=args.out_path
deep=MyEmpty
os.environ['MASTER_PORT'] = "8884"

if args.facility=='summit' :
    get_master = "echo $(cat {} | sort | uniq | grep -v batch | grep -v login | head -1)".format(os.environ['LSB_DJOB_HOSTFILE'])
    os.environ['MASTER_ADDR'] = str(subprocess.check_output(get_master, shell=True))[2:-3]
    os.environ['WORLD_SIZE'] = os.environ['OMPI_COMM_WORLD_SIZE']
    os.environ['RANK'] = os.environ['OMPI_COMM_WORLD_RANK']
    params['local_rank'] = int(os.environ['OMPI_COMM_WORLD_LOCAL_RANK'])
else:

    os.environ['RANK'] = os.environ['SLURM_PROCID']
    os.environ['WORLD_SIZE'] = os.environ['SLURM_NTASKS']
    params['local_rank'] = int(os.environ['SLURM_LOCALID'])
    
logging.info('M:ini  host=%s myRank=%s of %s  useIP=%s'%(socket.gethostname(), os.environ['RANK'] ,os.environ['WORLD_SIZE'],os.environ['MASTER_ADDR']))

assert  int(os.environ['WORLD_SIZE'])  >=1

dist.init_process_group(backend='nccl', init_method='env://')
#,world_size=int(os.environ['WORLD_SIZE']), rank=int(os.environ['RANK']))

params['world_rank'] = dist.get_rank()
params['world_size'] = dist.get_world_size()
params['localBS']=args.globalBS//params['world_size']
assert params['localBS']>=1
assert params['world_size']  ==  int(os.environ['SLURM_NTASKS'])

torch.cuda.set_device(params['local_rank'])
deep.device = torch.cuda.current_device()
deep.verb= params['world_rank'] ==0

logging.info('M:dist world rank %d of %d, local rank %d, host=%s  device=%s useIP=%s'%(params['world_rank'],params['world_size'],params['local_rank'],socket.gethostname(),str(deep.device),os.environ['MASTER_ADDR']))


# wait for all finish downloading the data - lets keep some order
assert dist.is_initialized()
dist.barrier()

# enable anomaly detection to find the operation that failed to compute its gradient
#torch.autograd.set_detect_anomaly(True)

# it assumes data are downloaded so there is no delay for rank>0
data_loader, _ = get_mnist_dataloaders(params)

'''
print( 'test data loader')
for i, data in enumerate(data_loader):
    print('DL i',i,data[0].shape)
exit(0)
'''
img_size = (32, 32, 1)  # this is not natural MNIST size, resize was done at loading
# Last number indicates number of channels, e.g. 1 for grayscale or 3 for RGB

generator = Generator(img_size=img_size, latent_dim=30, dim=16)
discriminator = Discriminator(img_size=img_size, dim=16)

if deep.verb:
    print(generator)
    print(discriminator)

# Initialize optimizers
lr = args.learning_rate
betas = (.9, .99)
G_opt = optim.Adam(generator.parameters(), lr=lr, betas=betas)
D_opt = optim.Adam(discriminator.parameters(), lr=lr, betas=betas)

# Train model


if deep.verb: logging.info('M: train start, epochs=%d, localBS=%d numGPU=%d'%(args.epochs,params['localBS'],params['world_size']))

trainer = Trainer(generator, discriminator, G_opt, D_opt,params)
trainer.train(data_loader, args.epochs, save_training_gif=deep.verb)

if deep.verb:
    print('M: train stop')
    # Save models
    name = 'mnist_model'
    torch.save(trainer.G.state_dict(), './gen_' + name + '.pt')
    torch.save(trainer.D.state_dict(), './dis_' + name + '.pt')
    
    #Save training history - dirty
    outF='train_hist.yaml'
    write_yaml(trainer.losses,outF)
    print('M: saved')
    
dist.barrier()
 
