import numpy as np
import torch
import ruamel.yaml  as yaml
import os

class MyEmpty:  pass # helper object - needed for some hacks

#...!...!..................
def waterMarkImg(data,epoch,freq=10):
    img_size=tuple(data.shape[2:4])
    #print('WM:',img_size,epoch)
    empty=np.zeros(img_size).flatten()
    #print('kk',empty.shape)
    for i in range(epoch):
        j=i+2*(i//freq)
        if j>=empty.shape[0]: break
        empty[j]=1.
    empty=empty.reshape(img_size)
    #print('tt',type(data)==torch.Tensor)
    if type(data)==torch.Tensor: empty=torch.FloatTensor(empty)
    data[0,0,:,:]=empty[:,:]


#...!...!..................
def read_yaml(ymlFn,verb=1):
        if verb: print('  read  yaml:',ymlFn,end='')
        start = time.time()
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.CLoader)

        ymlFd.close()
        #  
        #print('\nee2',type(bulk),'size=%d'%len(bulk))
        #print(bulk)
        if verb: print(' done  elaT=%.1f sec'%(time.time() - start))
        return bulk

#...!...!..................
def write_yaml(rec,ymlFn,verb=1):
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1024
        if verb:
                print('  closed  yaml:',ymlFn,' size=%.1f kB'%xx)


    
