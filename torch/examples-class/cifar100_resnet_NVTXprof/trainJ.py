#!/usr/bin/env python
'''
Not running on CPUs !

Runs   1 GPU:  srun -n1 ./trainS.py --config=bs1024-opt
Speed: Avg img/sec: 3706.3
rm -rf /global/cscratch1/sd/balewski/tmp_digitalMind/torchOut/bs1024-opt

Run on many GPU on one! node
nGPU_per_node=$SLURM_GPUS_PER_TASK 
srun  -n 1 python -m torch.distributed.launch --nproc_per_node=$nGPU_per_node   trainS.py --config=bs1024-opt

INFO - train-data: 48 steps, localBS=1024, globalBS=4096
Speed: Avg img/sec: 3542.7

INFO - train-data: 195 steps, localBS=256, globalBS=1024

More Speed data at
https://docs.google.com/spreadsheets/d/1_c3bHuUs555N10_A5B0LqTNHvcVIojNyxIEieq2QzZs/edit?usp=sharing


Or runs on multiple GPUs, one task per node: python -m torch.distributed.launch train.py <-- use batchTrainMulti.slr for full configuration

Run on 4 GPUs on 1 node
 salloc -N1 -C gpu  -c 40 --gpus-per-task=4 --ntasks-per-node=1  -t4:00:00 

Full node
 salloc -N1  -C gpu  -c 80   --gpus-per-task=8  --ntasks-per-node=1 --exclusive  -t4:00:00   


'''

import sys,os
sys.path.append(os.path.abspath("toolbox"))

import argparse
from pprint import pprint

from TrainerJ import Trainer
from YParams import YParams
import  logging

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument("--local_rank", default=0,  type=int, help='will be changed by torch/distributed/launch.py' )
  parser.add_argument("--yaml_config", default='./hpar_cifar100.yaml', type=str)
  parser.add_argument("--config", default='bs512', type=str)
  parser.add_argument("--out_path", default=None, type=str)
  parser.add_argument("--facility", default='corigpu', type=str)
  args = parser.parse_args()
  params = YParams(os.path.abspath(args.yaml_config), args.config)
   
  if args.facility=='summit':
    import subprocess
    get_master = "echo $(cat {} | sort | uniq | grep -v batch | grep -v login | head -1)".format(os.environ['LSB_DJOB_HOSTFILE'])
    os.environ['MASTER_ADDR'] = str(subprocess.check_output(get_master, shell=True))[2:-3]
    os.environ['MASTER_PORT'] = "23456"
    os.environ['WORLD_SIZE'] = os.environ['OMPI_COMM_WORLD_SIZE']
    os.environ['RANK'] = os.environ['OMPI_COMM_WORLD_RANK']
    params['local_rank'] = int(os.environ['OMPI_COMM_WORLD_LOCAL_RANK'])
    if params['local_rank']==0:
       logging.info('M: MASTER_ADDR=%s WORLD_SIZE=%s RANK=%s'%(os.environ['MASTER_ADDR'] ,os.environ['WORLD_SIZE'], os.environ['RANK'] ))
  else:
    #params['local_rank'] = args.local_rank
    os.environ['RANK'] = os.environ['SLURM_PROCID']
    os.environ['WORLD_SIZE'] = os.environ['SLURM_NTASKS']
    params['local_rank'] = int(os.environ['SLURM_LOCALID'])

  # setup distributed training variables and intialize cluster if using
  #params['world_size'] = 1
  #if 'WORLD_SIZE' in os.environ:
  params['world_size'] = int(os.environ['WORLD_SIZE'])

  # setup output directory
  if args.out_path==None:
    expDir = os.path.join(params.out_path, args.config)
  else:
    expDir = args.out_path
  params['out_path']=expDir


  #params['testJan']=11  # good way
  #params.update_params({'testJan':44})  # good way
  #params.testJan=22  # bad way - never assign value this way, but you can alwasy do x=params.testJan
  #print('M:mm  lr=%d, wr=%d alr=%d'%(params.local_rank,params.world_rank, args.local_rank1))
  
  
  trainer = Trainer(params)
  trainer.train()
