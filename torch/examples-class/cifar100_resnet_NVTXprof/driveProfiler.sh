#!/bin/bash
echo 'D: use NVTX profiler for each node'
# note, you must also use hpar-config which enables NVTX profiling, e.g. config=bs256-prof 
suffix=$(bash -c 'echo $SLURM_TASK_PID')
APP=$1
echo D:app=${APP}=suffix=${suffix}=

export NSYS_NVTX_PROFILER_REGISTER_ONLY=0
nsys profile -o result-${suffix} --trace=cuda,nvtx --capture-range=nvtx --nvtx-capture=PROFILE --force-overwrite true  --stats=true  ${APP}

# spare:
