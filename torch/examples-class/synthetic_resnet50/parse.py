import re,glob
import matplotlib.pyplot as plt
from itertools import cycle 

font = {'family': 'sans-serif',
        'color':  'black',
        'weight': 'normal',
        'size': 14,
        }
fig, ax = plt.subplots(figsize=(6,6))
lines = ["-","--","-.",":"]
markers = ['^','s','h','o']
next_line = cycle(lines)
next_marker = cycle(markers)
mode = ('nccl','mpi','horovod','ddl')
l = re.compile('Total.*')
p = re.compile('[0-9.]*\ \+\-[0-9.]*')
for m in mode: 
  speed =[]; err = []; node = []
  files = glob.glob("logs/log.%s*"%m)
  for f in files: 
    for line in open(f, 'r'): 
      results = re.search(l, line)
      if results is not None: 
        s, e = re.search(p, results.group()).group().split('+-')
        speed.append(float(s)/1000)
        err.append(float(e))
        node.append(f.split(m)[1])
  speed, err, node = zip(*sorted(zip(speed,err,node)))
  ax.plot(node, speed, linestyle=next(next_line), marker=next(next_marker), label=m)

ax.plot()
ax.set_xlabel('# Summit nodes',fontdict=font)
ax.set_ylabel('Images/s (k)', fontdict=font) 
ax.tick_params(axis='both', labelsize=12)
leg = ax.legend(fontsize=12)
leg.get_frame().set_linewidth(0.0)

#plt.show()
fig.savefig('pytorch_comm_batch32.png',dpi=300)
 
