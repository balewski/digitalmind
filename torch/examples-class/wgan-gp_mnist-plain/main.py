#!/usr/bin/env python3
# source https://github.com/EmilienDupont/wgan-gp
#
# Cori-GPU:  time srun -n1  python -u  ./main.py
# time srun -n 1   bash -c  '   nvidia-smi -l 10 >&L.smi_`hostname` & python3 -u main.py  '
#

import torch
import torch.optim as optim
from dataloaders import get_mnist_dataloaders, get_lsun_dataloader
from models import Generator, Discriminator
from training import Trainer
from util import write_yaml

data_loader, _ = get_mnist_dataloaders(batch_size=64)
img_size = (32, 32, 1)  # this is not natural MNIST size, resize was done at loading
# Last number indicates number of channels, e.g. 1 for grayscale or 3 for RGB

generator = Generator(img_size=img_size, latent_dim=30, dim=16)
discriminator = Discriminator(img_size=img_size, dim=16)

print(generator)
print(discriminator)

# Initialize optimizers
lr = 1e-4
betas = (.9, .99)
G_optimizer = optim.Adam(generator.parameters(), lr=lr, betas=betas)
D_optimizer = optim.Adam(discriminator.parameters(), lr=lr, betas=betas)

# Train model
epochs =10 
use_cuda=torch.cuda.is_available()
print('M: use_cuda:',use_cuda)
trainer = Trainer(generator, discriminator, G_optimizer, D_optimizer,
                  use_cuda=use_cuda)
trainer.train(data_loader, epochs, save_training_gif=True)

# Save models
name = 'mnist_model'
torch.save(trainer.G.state_dict(), './gen_' + name + '.pt')
torch.save(trainer.D.state_dict(), './dis_' + name + '.pt')

#Save training history - dirty
outF='train_hist.yaml'
write_yaml(trainer.losses,outF)
