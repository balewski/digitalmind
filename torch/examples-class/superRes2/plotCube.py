#!/usr/bin/env python
""" 
read 1 cube and plot selected slices
Uses DataLoader - to test it
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import torch

import  time
import sys,os
sys.path.append(os.path.relpath("toolbox/"))
#from Plotter import Plotter_NeuronInverter
from Dataloader_H5 import get_data_loader
from Util_IOfunc import read_yaml, write_yaml
import  logging
from pprint import pprint
import argparse

#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
  
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False, help="disable X-term for batch mode")

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath", default='/global/cfs/cdirs/m3363/balewski/superRes-packed_h5/',help="formated data location")
    parser.add_argument("--design", default='hpar_superRes1.yaml', type=str)

    parser.add_argument("--dataName",default="2021_05-Yueying-disp_4c.cpair.h5",help="name of inp h5 file")
    parser.add_argument("-o","--outPath", default='out',help="output path for plots and tables")
    

    args = parser.parse_args()
    args.prjName='cosmoCube'
   
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    logging.basicConfig(format='%(levelname)s - %(message)s', level=logging.INFO)
    return args
 
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':
    args=get_parser()

    domain='val'
    #domain='train'
    cf={}   
    cf['dataPath']=args.dataPath
    cf['domain']=domain
    cf['h5name']=args.dataPath+args.dataName
    cf['world_rank']=0
    cf['world_size']=1
    cf['local_batch_size']=2
    cf['shuffle']=True    
    
    blob=read_yaml( args.design,verb=args.verb)
    cf.update(blob)
    
    startT=time.time()
    dataloader = get_data_loader(cf, verb=1)
    inpMD=cf['inpMD']
    print('\nM:meta-data from h5:'); pprint(inpMD)
    print('M:data loader len:',len(dataloader))
    dataD=next(iter(dataloader))
    print('M:one sample=',list(dataD))
    for x in ['input', 'target']:
        y=dataD[x]
        print(x,type(y),y.shape)

    print('inp raw example:',dataD['input'][0,:,0,0,:])
    inp_shape=dataD['input'].shape
    tgt_shape=dataD['target'].shape
    

    ok00
    '''
      loss,U,Z=model_infer(model,data_loader,sumMD)
      predTime=time.time()-startT
      print('M: infer : Average loss: %.4f  events=%d , elaT=%.2f min\n'% (loss,  Z.shape[0],predTime/60.))

      sumRec={}
      sumRec['domain']=domain
      sumRec[domain+'LossMSE']=float(loss)
      sumRec['predTime']=predTime
      sumRec['numSamples']=U.shape[0]
      sumRec['lossThrHi']=0.50  # for tagging plots
      sumRec['inpShape']=sumMD['train_params']['model']['inputShape']
      sumRec['short_name']=sumMD['train_params']['cell_name']
      sumRec['modelDesign']=sumMD['train_params']['model']['myId']
      sumRec['trainRanks']=sumMD['train_params']['world_size']
      sumRec['trainTime']=sumMD['trainTime_sec']
      sumRec['loss_valid']= sumMD['loss_valid']

      #
      #  - - - -  only plotting code is below - - - - -
      #
      plot=Plotter_NeuronInverter(args,inpMD  )

      plot.param_residua2D(U,Z,sumRec)

      write_yaml(sumRec, args.outPath+'/sum_pred.yaml')

      #1plot.params1D(U,'true U',figId=7)
      #1plot.params1D(Z,'pred Z',figId=8)

      if 0: 
        print('input data example, it will plot waveforms')
        dlit=iter(data_loader)
        xx, yy = next(dlit)
        #1xx, yy = next(dlit) #another sample
        print('batch, X,Y;',xx.shape,xx.dtype,yy.shape,yy.dtype)
        print('Y[:2]',yy[:2])
        plot.frames_vsTime(xx,yy,9)


      plot.display_all('predict')  

    '''
