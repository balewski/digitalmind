#!/usr/bin/env python
'''
This code has been derived from
D+G models by Yueying (yueyingn@andrew.cmu.edu) 
https://github.com/eelregit/map2map/blob/master/map2map/models/srsgan.py


Not running on CPUs !

1 or many GPU configuraton is completed in this file


Runs   1 GPU: 
 srun -n1 train_dist.py  --design dev
 srun -n2 train_dist.py --outPath out2g 

Run on 1 GPUs on 1 node
 salloc -N1 -C gpu  -c 10 --gpus-per-task=1  -t4:00:00 --ntasks-per-node=2 

Full node
 salloc  --ntasks-per-node=8  -C gpu  -c 10   --gpus-per-task=1 --exclusive  -t4:00:00  -N1  

Quick test:
srun -n 1 -l ./train_dist.py  -n4

Production job
srun -n 2 -l ./train_dist.py --dataName 2021_05-Yueying-disp_17c --design supRes2 


Prefered order of plots in TB
lossGD
D noise
LR
... time ...
epoch time (sec)
glob_speed (samp:sec)

Large scale Perlmutter job: 240 GPUs
salloc  -C gpu -t4:00:00  --gpus-per-task=1  --ntasks-per-node=4  --exclusive  -N60
 srun -n240  ./train_dist.py   --facility perlmutter  --design  supRes2 --dataName 2021_05-Yueying-disp_17c  --epochs 300 --outPath */240Gx  


'''

import sys,os
sys.path.append(os.path.abspath("toolbox"))
from Util_IOfunc import read_yaml, write_yaml


from pprint import pprint
from Trainer import Trainer 
import  logging
import torch
import torch.distributed as dist
logging.basicConfig(format='%(levelname)s - %(message)s', level=logging.INFO)
import time

import argparse
#...!...!..................
def get_parser():
  parser = argparse.ArgumentParser()
  parser.add_argument("--design", default='dev', help='[.hpar.yaml] configuration of model and training')

  parser.add_argument("--dataName",default="2021_05-Yueying-disp_4c",help="[.cpair.h5] name data  file")
  parser.add_argument("--outPath", default='*/manual/', help=' all outputs, also TB, optional "*/" defined in hpar.yaml')

  parser.add_argument("--facility", default='corigpu', choices=['corigpu','summit','perlmutter'],help='computing facility where code is executed')  
  parser.add_argument("--jobId", default=None, help="optional, aux info to be stored w/ summary")
  parser.add_argument("-n", "--numSamp", type=int, default=None, help="(optional) cut off num samples per epoch")
  parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')

  parser.add_argument("--epochs",default=None, type=int, help="if defined, replaces max_epochs from hpar")

  args = parser.parse_args()
  return args

  
#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == '__main__':
    args=get_parser()
    if args.verb>2: # extreme debugging
      for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    
    os.environ['MASTER_PORT'] = "8886"

    params ={}
    #print('M:facility:',args.facility)
    if args.facility=='summit':
      import subprocess
      get_master = "echo $(cat {} | sort | uniq | grep -v batch | grep -v login | head -1)".format(os.environ['LSB_DJOB_HOSTFILE'])
      os.environ['MASTER_ADDR'] = str(subprocess.check_output(get_master, shell=True))[2:-3]
      os.environ['WORLD_SIZE'] = os.environ['OMPI_COMM_WORLD_SIZE']
      os.environ['RANK'] = os.environ['OMPI_COMM_WORLD_RANK']
      params['local_rank'] = int(os.environ['OMPI_COMM_WORLD_LOCAL_RANK'])
    else:
      #os.environ['MASTER_ADDR'] = os.environ['SLURM_LAUNCH_NODE_IPADDR']
      os.environ['RANK'] = os.environ['SLURM_PROCID']
      os.environ['WORLD_SIZE'] = os.environ['SLURM_NTASKS']
      params['local_rank'] = int(os.environ['SLURM_LOCALID'])

    params['world_size'] = int(os.environ['WORLD_SIZE'])
    params['world_rank'] = 0
    if params['world_size'] > 1:  # multi-GPU training
      torch.cuda.set_device(params['local_rank'])
      try:
        dist.init_process_group(backend='nccl', init_method='env://')
      except:
        print('NCCL crash, sleep for 2h',flush=True);  time.sleep(2*3600)
        
          
      params['world_rank'] = dist.get_rank()
      #print('M:locRank:',params['local_rank'],'rndSeed=',torch.seed())
    params['verb'] =args.verb * (params['world_rank'] == 0)

    if params['verb']:
      logging.info('M:MASTER_ADDR=%s WORLD_SIZE=%s RANK=%s  pytorch:%s'%(os.environ['MASTER_ADDR'] ,os.environ['WORLD_SIZE'], os.environ['RANK'],torch.__version__ ))
      for arg in vars(args):  logging.info('M:arg %s:%s'%(arg, str(getattr(args, arg))))

    blob=read_yaml( args.design+'.hpar.yaml',verb=params['verb'], logger=True)
    params.update(blob)
    params['design']=args.design
 
    # refine BS for multi-gpu configuration
    tmp_batch_size=params.pop('batch_size')
    if params['const_local_batch']: # faster but LR changes w/ num GPUs
      params['local_batch_size'] =tmp_batch_size 
      params['global_batch_size'] =tmp_batch_size*params['world_size']
    else:
      params['local_batch_size'] = int(tmp_batch_size//params['world_size'])
      params['global_batch_size'] = tmp_batch_size

    cube2rankD=read_yaml('cube2rank.conf.yaml',verb=params['verb'], logger=True)
    #pprint(cube2rankD)
    
    # capture other args values
    params['h5_name']=params['data_path'][args.facility]+args.dataName+'.cpair.h5'
    params['job_id']=args.jobId
    if '*/' in args.outPath:
      params['out_path']=args.outPath.replace('*/',params['out_base'][args.facility])
    else:
      params['out_path']=args.outPath # use as-is
    params['clique_conf']=cube2rankD[args.dataName]
    params['facility']=args.facility
    assert params['world_size'] in params['clique_conf']['rank_split']
    
    if args.numSamp!=None:  # reduce num steps/epoch - code testing
        params['max_samples_per_epoch']=args.numSamp
    if args.epochs!=None:
        params['max_epochs']= args.epochs

    
    trainer = Trainer(params)
    #except:
    #  print('ML constructor crash, sleep for 2h',flush=True);   time.sleep(2*3600)

    
    trainer.train()
    #except:
      #print('train_one_epoch crash, sleep for 2h',flush=True); time.sleep(2*3600)
                
    if params['world_rank'] == 0:
      sumF= params['out_path']+'/sum_train.yaml'
      write_yaml(trainer.sumRec, sumF) # to be able to predict while training continus

      print("M:done rank=",params['world_rank'])
