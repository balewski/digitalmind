#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
 tool generating  HPAR for SuperRes

?save: hpar_XX.conf.yaml and hpar_XX.sum.yaml 

./genHPar_v1.py --numTrials 5

'''

import socket  # for hostname
import os, time,sys
import secrets,copy
import warnings

from pprint import pprint

import numpy as np
sys.path.append(os.path.abspath("../toolbox"))
from Util_IOfunc import read_yaml, write_yaml

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--genConf', default='gen_v2.conf.yaml',help="constrants for HPAR generation")
    parser.add_argument('-b','--baseHpar', default='../dev.hpar.yaml',help="base HPAR conf to be varied")
    parser.add_argument("-v","--verb",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-o","--outPath",default='./',help="output path for HPAR")

    args = parser.parse_args()
    np.random.seed() #  set the seed to a random number obtained from /dev/urandom 
    for i in range(int(50*np.random.rand())):
        np.random.rand()  # now rnd is burned in

    for arg in vars(args):  
        print( 'myArg:',arg, getattr(args, arg))
    print('seed test:',np.random.rand(4))
    return args

                
#...!...!....................
def uni_exp(AB,fact=1.):
    [A,B]=AB
    assert A>0
    assert A<=B
    a=np.log(A)
    b=np.log(B)            
    u=np.random.uniform(a,b)
    v= np.exp(u)*fact
    return float('%.3g'%v)


#...!...!....................
def uni_lin(AB):
    [A,B]=AB
    assert A<=B
    u=np.random.uniform(A,B)
    return float('%.3g'%u)

#...!...!....................
def lucky_D( cf,thp):
    thp['D_delay_epochs']=int(uni_lin(cf['delay_range']))
    thp['D_noise']['cooldown_epochs']=int(uni_lin(cf['noise_cool_range']))
    out=thp['D_LR']
    out['init']=uni_exp(cf['LR']['init_range'])
    out['gamma']=uni_exp(cf['LR']['reduce_range'])
    out['decay_epochs']=int(uni_lin(cf['LR']['decay_range']))

#...!...!....................
def lucky_G( cf,thp):
    d_hp=thp['D_LR']
    g_hp=thp['G_LR']

    g_hp['init']=  uni_exp(cf['init_fact'],fact=d_hp['init'])
    g_hp['gamma']=uni_exp(cf['reduce_fact'],fact=d_hp['gamma'])
    g_hp['decay_epochs']=int(uni_exp(cf['decay_fact'],fact=d_hp['decay_epochs']))
   
   
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

conf=read_yaml(args.genConf)
base=read_yaml(args.baseHpar)

pprint(conf)
if 1:
    hpar=copy.deepcopy(base)
    myId=secrets.token_hex(nbytes=4)
    hpar['myId']=myId
    print('\n = = = = = = = = = = M:new  ',myId)
    lucky_D(conf['D'],hpar['train_conf'])
    lucky_G(conf['G_LR'],hpar['train_conf'])
      
    # GP
    cf=conf['GP']
    out=hpar['train_conf']
    out['gp_update_interval_steps']=int(np.random.choice( cf['step_choice']))
    out['gp_scale']=uni_exp(cf['scale_range'])
    propF=args.outPath+'/supRes_rnd.conf.yaml'
    write_yaml(hpar,propF)

print('M: genHpar done ')
