import os,time
from pprint import pprint,pformat
import socket  # for hostname
import numpy as np
import torch

import logging  # to screen

from Dataloader_H5 import get_data_loader  # 1-cell (code is simpler)
from Util_IOfunc import read_yaml
from Model import G,D
from wasserstein import wasserstein_distance_loss 
import torch.optim as optim
from resample import resample
from narrow import narrow_cast
from adversary import grad_penalty_reg
from figures import ( plt_slices, plt_power )
from TBSwriter import TBSwriter

#...!...!..................
def set_requires_grad(module, requires_grad=False):
    for param in module.parameters():
        param.requires_grad = requires_grad

#...!...!..................
def get_grads(model):
    """gradients of the weights of the first and the last layer
    """
    grads = list(p.grad for n, p in model.named_parameters()
                 if '.weight' in n)
    grads = [grads[0], grads[-1]]
    grads = [g.detach().norm() for g in grads]
    return grads


#............................
#............................
#............................
class Trainer(TBSwriter):
#...!...!..................
    def __init__(self, params):
        
        assert torch.cuda.is_available() 
        self.params = params
        self.verb=params['verb']
        self.isRank0=params['world_rank']==0
        self.device = torch.cuda.current_device()        

        if params['world_rank']<20:
            logging.info('T:ini world rank %d of %d, local rank %d, host=%s  see device=%d'%(params['world_rank'],params['world_size'],params['local_rank'],socket.gethostname(),self.device))

        expDir=params['out_path']
        if self.isRank0:
            TBSwriter.__init__(self,expDir)
            expDir2=os.path.join(expDir, 'checkpoints')
            if not os.path.isdir(expDir2):  os.makedirs(expDir2)

        
        params['checkpoint_path'] = os.path.join(expDir, 'checkpoints/ckpt.pth')
        #params['resuming'] =  params['resume_checkpoint'] and os.path.isfile(params['checkpoint_path'])
        
        optTorch=params['opt_pytorch']
        # EXTRA: enable cuDNN autotuning.
        torch.backends.cudnn.benchmark = optTorch['autotune']
        torch.autograd.set_detect_anomaly(optTorch['detect_anomaly'])

        if self.verb: logging.info('T:params %s'%pformat(params))

        # ...... END OF CONFIGURATION .........    
        if self.verb:
          logging.info('T:imported PyTorch ver:%s verb=%d'%(torch.__version__,self.verb))
          logging.info('T:rank %d of %d, prime data loaders'%(params['world_rank'],params['world_size']))


        self.train_loader = get_data_loader(params, 'train',verb=self.verb)
        #assert params['validation_period'][1]==0  # validation not implemented
        #1self.valid_loader = get_data_loader(params, 'val',verb=self.verb) 
        
        #
        inpMD=params['inpMD']
        if self.verb:
          logging.info('T:rank %d of %d, data loaders initialized'%(params['world_rank'],params['world_size']))
          logging.info('T:train-data: %d steps, localBS=%d, globalBS=%d'%(len(self.train_loader),self.train_loader.batch_size,params['global_batch_size']))
          
          #1logging.info('T:valid-data: %d steps'%(len(self.valid_loader)))        
          logging.info('T:meta-data from h5: %s'%pformat(inpMD))
          
        if params['world_rank']==0 :  # for now needed for cub-shapes
            someLoader=self.train_loader
            print('TI:dataset example,train loader len=numsteps:',len(someLoader))
            dataD=next(iter(someLoader))

            print('TI:one sample=',list(dataD))
            for x in ['input', 'target']:
                y=dataD[x]
                print(x,type(y),y.shape)

            # needed for Summary()
            seen_inp_shape=dataD['input'].shape[1:] # skip batch dimension
            tgt_shape=dataD['target'].shape[1:]

            
        if params['world_size']>1:
            import torch.distributed as dist
            from torch.nn.parallel import DistributedDataParallel
            assert dist.is_initialized()
            dist.barrier()
            self.dist=dist # needed by this class member methods
            # wait for all ranks to finish downloading the data - lets keep some order
            
        if self.verb:
            # must know the number of steps to decided how often to print
            params['text_log_interval_steps']=max(1,len(self.train_loader)//params['text_log_freq_per_epoch'])
            #logging.info('T:logger iterD: %s'%str(self.iterD))
                         
        if self.verb:
            logging.info('T:assemble G+D models')

        #self.model=MyModel(params['model'], verb=self.verb).to(self.device)
        m2mCf=params['map2map_conf']

        inp_chan=inpMD['lowResShape'][0]
        out_chan=inpMD['highResShape'][0]
        scale_fact=m2mCf['scale_factor']
        self.G_model = G(inp_chan,out_chan, scale_factor=scale_fact,verb=self.verb)
        self.D_model = D(inp_chan,out_chan, scale_factor=scale_fact,verb=self.verb)

        # add models to TB - takes 1 min
        if self.isRank0:
            gr2tb=params['model_conf']['tb_add_graph']
            dataD=next(iter(self.train_loader))            
            if 'G'==gr2tb:
                t1=time.time()
                self.TBSwriter.add_graph(self.G_model,dataD['input'].to('cpu'))
                t2=time.time()
                print('TB G-graph done elaT=%.1f'%(t2-t1))
            if 'D'==gr2tb:
                t1=time.time()
                self.TBSwriter.add_graph(self.D_model,dataD['target'].to('cpu'))
                t2=time.time()
                print('TB D-graph done elaT=%.1f'%(t2-t1),gr2tb)

        self.G_model=self.G_model.to(self.device)
        self.D_model=self.D_model.to(self.device)
        
        trCf=params['train_conf']
        self.G_opt = optim.Adam(self.G_model.parameters(), lr=trCf['G_LR']['init'])
        self.D_opt = optim.Adam(self.D_model.parameters(), lr=trCf['D_LR']['init'])


        self.G_sched = torch.optim.lr_scheduler.StepLR(self.G_opt, trCf['G_LR']['decay_epochs'], gamma=trCf['G_LR']['gamma'], verbose=self.verb)
        self.D_sched = torch.optim.lr_scheduler.StepLR(self.D_opt, trCf['D_LR']['decay_epochs'], gamma=trCf['D_LR']['gamma'], verbose=self.verb)


        self.G_criterion =torch.nn.MSELoss().to(self.device) # Mean Squared Loss
        self.D_criterion =wasserstein_distance_loss 


        if self.verb:
            logging.info('T: D+G models created, seen inp_shape: %s',str(seen_inp_shape))
            logging.info(self.G_model.summary())
            logging.info(self.D_model.summary())

            [Gpr,Dpr]=params['model_conf']['print_summary']
            if Dpr+Gpr>0: from torchsummary import summary  # not visible if loaded earlier

            if Gpr & 1:  logging.info('T:generator layers %s'%pformat(self.G_model))
            if Gpr & 2:  logging.info('T:generator summary %s'%pformat(summary(self.G_model,tuple(seen_inp_shape))))
            if Dpr & 1:  logging.info('T:discriminator layers %s'%pformat(self.D_model))
            if Dpr & 2:  logging.info('T:discriminator summary %s'%pformat(summary(self.D_model,tuple(tgt_shape))))
            
        if params['world_size']>1:
            self.G_model = DistributedDataParallel(self.G_model,
                              device_ids=[params['local_rank']],output_device=[params['local_rank']])
            self.D_model = DistributedDataParallel(self.D_model,
                              device_ids=[params['local_rank']],output_device=[params['local_rank']])
        # note, using DDP assures the same as average_gradients(self.model), no need to do it manually
        

        self.startEpoch = 0
        self.epoch = self.startEpoch

        if self.isRank0:  # create summary record
            tot_train_samp= params['world_size']*len(self.train_loader)
            self.sumRec={'train_params':params,
                         'hostName' : socket.gethostname(),
                         'numRanks': params['world_size'],
                         'state': 'model_build',
                         'input_meta':inpMD,
                         'total_train_samp':tot_train_samp,
                         'trainTime_sec':-1,
                         'loss_valid':-1,
                         'pytorch': torch.__version__,
                         'epoch_start': int(self.startEpoch),
            }

      
#...!...!..................
    def train(self):
        if self.verb:
            logging.info("Starting Training Loop..., myRank=%d "%(self.params['world_rank']))

        bestLoss=1e20  
        startTrain = time.time()
        TperEpoch=[]

        m2mCf=self.params['map2map_conf']
        trCf=self.params['train_conf']
        
        self.iterD={'gcnt':0} # local variables for counting
        
        #. . . . . . .  epoch loop start . . . . . . . . 
        for epoch in range(self.startEpoch, self.params['max_epochs']):
            self.epoch=epoch
            self.doDpass=epoch>= trCf['D_delay_epochs']

            # Apply learning rate warmup     
            if epoch < trCf['warmup_epochs']:
                lr=trCf['G_LR']['init']*(epoch+1.)/trCf['warmup_epochs']
                self.G_opt.param_groups[0]['lr'] = lr
                lr=trCf['D_LR']['init']*(epoch+1.)/trCf['warmup_epochs']
                self.D_opt.param_groups[0]['lr'] = lr

            if self.verb:
                numLocStep=len(self.train_loader)
                logging.info('T:Begin epoch=%d num-train-steps=%d doDpass=%r'%(epoch,numLocStep,self.doDpass))
               
            startE = time.time()
            
            lossV=self.train_one_epoch()  # ********           
        
            if self.doDpass : TperEpoch.append(time.time()-startE)
            if epoch >=  trCf['warmup_epochs']:
                self.G_sched.step()
                self.D_sched.step()
            

            ''' REVISIT  THIS  CODE LATER
            valid_time, valid_logs = self.validate_one_epoch()
            '''
            
            if self.verb:
              tV=np.array(TperEpoch)
              if len(tV)>1:
                tAvr=np.mean(tV); tStd=np.std(tV)/np.sqrt(tV.shape[0]-1)
              else:
                tAvr=tStd=-1
              logging.info('Epoch %d, took %.1f sec, avr=%.2f +/-%.2f sec/epoch, train elaT=%.1f sec, loss G=%.3g  D=%.2g'%(epoch , time.time()-startE, tAvr,tStd,time.time()-startTrain, lossV[0],lossV[1]) )
              

        #. . . . . . .  epoch loop end . . . . . . . .
        
        if self.isRank0:  # create summary record
          # add info to summary
          if 1:
            rec={'epoch_stop':epoch, 'state':'model_trained'} 
            rec['trainTime_sec']=time.time()-startTrain
            rec['timePerEpoch_sec']=[float('%.2f'%x) for x in [tAvr,tStd] ]
            self.sumRec.update(rec)
            return
 

#...!...!..................
    def train_one_epoch(self):
        torch.cuda.synchronize()
        self.G_model.train()
        self.D_model.train()  # not used if epoch<D_delay_epochs

        tbeg=time.time()
        report_time = time.time()
        report_samp = 0
        optTorch=self.params['opt_pytorch']
        m2mCf=self.params['map2map_conf']
        trCf=self.params['train_conf']
        
        device=self.device
        # loss, D_loss, adv_loss, adv_loss_fake, adv_loss_real
        # loss: generator (model) supervised loss
        # D_loss: generator (model) adversarial loss
        # adv_loss: discriminator (adv_model) loss
        epoch_lossV = torch.zeros(5, dtype=torch.float64, device=device)
        '''epoch_lossV  content: 
           0:G_loss= mse(outCube,tgtCube)
           1:D_loss= D_criterion(score_out, real)
           2:adv_loss_tot=3+4
           3:adv_loss_fake=D_criterion(score_out, fake)
           4:adv_loss_real=D_criterion(score_out, adv_real=label_smooth=1)
        '''
        
        # below are labels:
        fake = torch.zeros([1], dtype=torch.float32, device=device) # is label
        real = torch.ones([1], dtype=torch.float32, device=device)  # is label
        adv_real = torch.full([1], trCf['label_smoothing'], dtype=torch.float32,device=device)

        # Loop over training data batches
        for ist, data in enumerate(self.train_loader):
            self.iterD['lcnt']=ist
            self.iterD['gcnt']+=1
            self.iterD['TXlog']=0
            if self.verb:                
                self.iterD['TXlog']=ist % self.params['text_log_interval_steps']==0
            # additional computations are needed on *all* ranks before posting on TB
            self.iterD['TBcompute']=ist % self.params['tb_log_interval_steps']==0
            # only rank0 will do the logging to TB
            self.iterD['TBlog']= self.iterD['TBcompute'] and self.isRank0
                                    
            inpCube=data['input'].to(self.device, non_blocking=True)
            tgtCube=data['target'].to(self.device, non_blocking=True)
            
            if optTorch['zerograd']:
                # EXTRA: Use set_to_none option to avoid slow memsets to zero
                self.G_model.zero_grad(set_to_none=True)
                self.D_model.zero_grad(set_to_none=True) 
            else:
                self.model.zero_grad()

            outCube = self.G_model(inpCube)  # forward(.)

            #print('A1cube:',inpCube.shape,outCube.shape,tgtCube.shape)
            if self.verb>1:
                print('\nMT:##### iter=%d epoch=%d,  gen-model took %.3f sec'%(self.iters,self.epoch,t2-t1),self.iters % len(self.train_loader))
            
            inpCube = resample(inpCube,m2mCf['scale_factor'], narrow=False)
            inpCube, outCube, tgtCube = narrow_cast(inpCube, outCube, tgtCube)
            #print('A2cube:',inpCube.shape,outCube.shape,tgtCube.shape)
            #A1cube: torch.Size([1, 3, 16, 16, 16]) torch.Size([1, 3, 86, 86, 86]) torch.Size([1, 3, 128, 128, 128])
            #A2cube: torch.Size([1, 3, 86, 86, 86]) torch.Size([1, 3, 86, 86, 86]) torch.Size([1, 3, 86, 86, 86])

            
            G_loss = self.G_criterion(outCube, tgtCube)  # unit: (kpc/h)^2 for displacement
            epoch_lossV[0] += G_loss.detach()
                        
            if self.doDpass:
                D_loss,grads=self.D_train_one_step(outCube,tgtCube,epoch_lossV,fake,real,adv_real)
                #note: D_loss=wassertstine dist
                
            else:  # D-model not used yet
                self.G_opt.zero_grad()
                G_loss.backward()
                self.G_opt.step()
                grads = get_grads(self.G_model)
                            
            # .... below is only text-logging, TB-logging  and reporting .....
            # .... measure processing speed ....
            report_samp += inpCube.shape[0]
            if self.iterD['TXlog']:
                torch.cuda.synchronize()
                if self.verb: logging.info('Epoch: %2d, step: %3d, Avg samp/sec/gpu: %.2f'%(self.epoch, ist, report_samp / (time.time() - report_time)))
                report_time = time.time()
                report_samp = 0
                
            # .... TensorBoard reporting
            # additional computations are needed on all ranks before posting on TB
            if self.iterD['TBcompute'] and 0:  # off '5-'
                if self.params['world_size']>1:
                    self.dist.all_reduce(G_loss)
                    G_loss /= self.params['world_size']                    
                if self.iterD['TBlog']:
                    self.TBlog_G_step_train(G_loss,grads) 

        # . . . . .   end of loop over steps .........
        tred=time.time()
        if self.params['world_size']>1:
           self.dist.all_reduce(epoch_lossV)
        epoch_lossV /= len(self.train_loader) * self.params['world_size']
        tend=time.time()
        
        timeD={'train tot':tend-tbeg,'train all-reduce':tend-tred}
        # .... compute speed
        locTotTrainSamp=len(self.train_loader)*self.train_loader.batch_size
        kfac=self.params['world_size']
        speedD={'train':locTotTrainSamp*kfac/timeD['train tot']}  # train samp/sec
        
        if self.isRank0: # always do TB at the end of the epoch
            self.TBlog_epoch_train(epoch_lossV,inpCube,outCube, tgtCube,timeD,speedD)

        return epoch_lossV

  
#...!...!..................
    def D_train_one_step(self,outCube,tgtCube,epoch_loss,fake,real,adv_real):
        m2mCf=self.params['map2map_conf']
        trCf=self.params['train_conf']
        noiseCf=trCf['D_noise']
        
        # add noise at the early training stage
        self.D_noise_std=0.
        D_epoch=self.epoch-trCf['D_delay_epochs']
        if D_epoch < noiseCf['cooldown_epochs']:
            noise_std=noiseCf['std_dev'] * (1.-D_epoch/noiseCf['cooldown_epochs'])
            #1if self.verb: logging.info('noise D_epoch=%d std=%.3f'%(D_epoch,noise_std))
            noise = noise_std * torch.randn_like(outCube)
            outCube = outCube + noise
            noise = noise_std * torch.randn_like(tgtCube)
            tgtCube = tgtCube + noise
            self.D_noise_std=noise_std
            del noise

        # discriminator
        set_requires_grad(self.D_model, True)

        score_out = self.D_model(outCube.detach()) 
        adv_loss_fake = self.D_criterion(score_out, fake.expand_as(score_out))
        epoch_loss[3] += adv_loss_fake.detach()
        #print('SSS1',score_out.shape, outCube.shape)
        #SSS1 torch.Size([1, 1, 7, 7, 7]) torch.Size([1, 3, 86, 86, 86])
        
        self.D_opt.zero_grad()
        adv_loss_fake.backward()

        score_tgt = self.D_model(tgtCube) # forward(.)
        adv_loss_real = self.D_criterion(score_tgt, adv_real.expand_as(score_tgt))
        epoch_loss[4] += adv_loss_real.detach()

        adv_loss_real.backward()
        
        adv_loss_tot = adv_loss_fake + adv_loss_real
        epoch_loss[2] += adv_loss_tot.detach()
        
        if self.iterD['gcnt'] % trCf['gp_update_interval_steps']==0:
            score_tgt = self.D_model(tgtCube.requires_grad_(True))
            gp_loss = grad_penalty_reg(score_tgt, tgtCube)

            gp_loss_ = (
                gp_loss * trCf['gp_update_interval_steps'] * trCf['gp_scale']
                + 0 * score_tgt.sum()  # hack to trigger DDP allreduce hooks
            )

            gp_loss_.backward()
            if self.iterD['TBlog'] : 
                self.TBSwriter.add_scalar( 'gp_loss', gp_loss.item()/ trCf['gp_scale'], global_step=self.iterD['gcnt'])

        self.D_opt.step()
        adv_grads = get_grads(self.D_model)

        # generator adversarial loss
        set_requires_grad(self.D_model, False)
        score_out = self.D_model(outCube)
        D_loss = self.D_criterion(score_out, real.expand_as(score_out))
        epoch_loss[1] += D_loss.detach()
        
        self.G_opt.zero_grad()
        
        D_loss.backward()  
        
        self.G_opt.step()
        grads = get_grads(self.G_model)
        if self.iterD['TBlog'] and 0 :  # off '4-' and '3-'
            missing_sync_over_ranks
            self.TBlog_D_train_step(D_loss,adv_loss_tot,adv_loss_fake,adv_loss_real, adv_grads )
        return D_loss,grads
        
  
#...!...!..................
    def save_checkpoint(self, checkpoint_path, model=None):
        """ We intentionally require a checkpoint_dir to be passed
            in order to allow Ray Tune to use this function """

        if not model:
            model = self.model

        torch.save({'iters': self.iters, 'epoch': self.epoch, 'model_state': model.state_dict(),
                'optimizer_state_dict': self.optimizer.state_dict()}, checkpoint_path)

#...!...!..................
    def restore_checkpoint(self, checkpoint_path):
        '''We intentionally require a checkpoint_dir to be passed
          in order to allow Ray Tune to use this function '''
        checkpoint = torch.load(checkpoint_path, map_location='cuda:{}'.format(self.params['local_rank']))
        self.model.load_state_dict(checkpoint['model_state'])
        self.iters = checkpoint['iters']
        self.startEpoch = checkpoint['epoch'] + 1
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])

