__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
this data loader reads a subset of cubes  from a common h5-file upon start, there is NO distributed sampler

reads all data at once and serves them from RAM
- optimized for mult-GPU training, 
- - different cubes are read by different ranks
- - cropping of cubes is local on each GPU (aka rank)
- reads data from common single H5 file for all ranks
- the train/valid data split is done based on big cube index, is predictable, the same for all ranks the same
- allows for in-fly transformation
- constraints:
- -  at least 1 cube per rank for validation is needed
- -  there is no test data subset, only train or validation
- -  for multi-gpu training some cubes may be left out
Shuffle: only  all samples after read is compleated

'''

import time,  os
import random
import h5py
import numpy as np
import copy
import json
from pprint import pprint,pformat
from torch.utils.data import Dataset, DataLoader
import torch 
import logging

#...!...!..................
def get_data_loader(params,domain, verb=1):
    conf=copy.deepcopy(params)  # the input is reused later in the upper level code
    conf['domain']=domain
    
    # decide rank --> clique_id and clique_rank
    if domain=='train':
        numClique=conf['clique_conf']['rank_split'][conf['world_size']]
    else:
        numClique=1 # keep it simple: always all cubes are spread over all tranks
        
    cubeList=conf['clique_conf']['cube_split'][domain]
    totCube=len(cubeList)

    conf['big_cube_list']=cubeList
    conf['clique_size']=conf['world_size'] // numClique
    conf['clique_id']=conf['world_rank'] // conf['clique_size'] # want consecutve ranks together
    conf['clique_rank']=conf['world_rank'] % conf['clique_size'] 
    conf['cubes_per_clique']=totCube // numClique  # --> idxStep
    conf['cube_idx_offset']=conf['clique_id']*conf['cubes_per_clique'] 

    #if domain=='train' or 1:
    if verb:
        xtxD={ x:conf[x] for x in [ 'domain','world_size','world_rank','clique_id','clique_rank','clique_size','cubes_per_clique','cube_idx_offset','local_batch_size','big_cube_list']}
        logging.info('DL:conf %s'%str(xtxD))
       
    dataset=  Dataset_h5_field3D(conf,verb)

    # append more info 
    params['inpMD']=dataset.inpMD
    
    if 'max_samples_per_epoch' in params:
        max_samp= params['max_samples_per_epoch']
        logging.warn('GDL: dom=%s is using shorter max_samples=%d (was %d)'%(domain,max_samp,dataset.nsample))
        dataset.nsample=min(max_samp,dataset.nsample)

    dataloader = DataLoader(dataset,
                            batch_size=int(conf['local_batch_size']),
                            num_workers=conf['num_data_workers'],
                            shuffle=True,
                            drop_last=True,
                            pin_memory=torch.cuda.is_available())

    return dataloader

#-------------------
#-------------------
#-------------------

class Dataset_h5_field3D(Dataset):
    """
    Dataset of lists of fields.

    Oryginal code from Yueying (yueyingn@andrew.cmu.edu)
    https://github.com/eelregit/map2map

    Modiffied by Jan Balewski, NERSC
    1) -------- INPUT HD5 
    INPUT data are stored in a single HD5-file, both low & high resolution cubes. No separation into training/validation subsets is in the HD5 file. The cubes are assigned to be used in train/valid pass in-fly based on index in the input and rank (for multi-GPU training). This data split is done once at the start of the program. The cube index for the low & high res cubes must be matched. The cube index is assumed to be shuffled before writing HD5. 
The cubes are assumed to be normalized before writeing to HD5.
 
    The is a typical input data shape, 17 cubes, as stored in HD5  
    $h5ls 2021_05-Yueying-disp_17c.h5  ( 26 GB on disc)
        highResCube              Dataset {17, 3, 512, 512, 512}
        lowResCube               Dataset {17, 3, 64, 64, 64}
        meta.JSON                Dataset {1}

    The meta-data contains sumplementary information need by dataloader.
    The HD5 data are loaded once at the start and kept in CPU RAM. No disc IO is needed for each epoch. Each GPU (aka rank) holds full big cubes, before cropping is applied.
    In the following text, the low and high resolution cubes are referred as 'input' and 'target' fields. In the code 'inp' and 'tgt', respectively

    NOT-YET: Each pattern in the list is a new field.
    NOTE?? that vector fields are assumed if numbers of channels and dimensions are equal.

    Scalar and vector fields can be augmented by flipping and permutating the axes.
    In 3D these form the full octahedral symmetry, the Oh group of order 48.
    NOT-TESTED: In 2D this is the dihedral group D4 of order 8.
    1D is not supported, but can be done easily by preprocessing.
    Fields can be augmented by random shift by a few pixels, useful for models that treat neighboring pixels differently, e.g. with strided convolutions.
    Additive and multiplicative augmentation are also possible, but with all fields added or multiplied by the same factor.

    Input and target fields can be cropped, to return multiple slices of size    `crop` from each field. 
    The crop anchors are controlled by `crop_start`, `crop_stop`, and `crop_step`.
    Input (but not target) fields can be padded beyond the crop size assuming
    periodic boundary condition.
    Jan: crop_step is input cube size. The  target cube size will be crop_step*scale_factor? May be not, see below.

    Setting integer `scale_factor` greater than 1 will crop target bigger than the input for super-resolution, in which case `crop` and `pad` are sizes of  the input resolution.
    Jan: do not understand it yet.

    """

#...!...!..................    
    def __init__(self, conf,verb=1):
        self.conf=conf
        self.verb=verb
        assert self.conf['world_rank']>=0
        assert self.conf['world_size']>self.conf['world_rank']
        self.openH5()
        
        if self.verb :
            logging.info('DS:init dom=%s  localBS=%d  tgtData/hd5=%s myRank=%d '%(self.conf['domain'],self.conf['local_batch_size'],str(self.data_tgt.shape),self.conf['world_rank']))
            
        # superRes speciffic
        inpSh=self.data_inp.shape
        tgtSh=self.data_tgt.shape
        self.ndim=self.inpMD['field_dim']
        
        assert self.inpMD['tensor_format']=='NCHW'
        self.size=np.asarray(inpSh[-self.ndim:]) # NCHW is assumed 
        self.inp_chan=[inpSh[-self.ndim-1]]
        self.tgt_chan=[tgtSh[-self.ndim-1]]
        #print('DS:dims inp_chan tgt_chan size ndim',self.inp_chan,self.tgt_chan,self.size,self.ndim)
        # expected: DS:dims in_chan=[3] tgt_chan= [3] size [64 64 64] ndim=3

        m2mCf=self.conf['map2map_conf']
        
        # ... Augment config 
        self.augment = m2mCf['augment']
        if self.augment:
          assert 1==2 # augmenting not tested
          if self.ndim == 1 and self.augment:
            raise ValueError('cannot augment 1D fields')          
          
        #J clean it up:
        self.aug_shift = np.broadcast_to(m2mCf['aug_shift'], (self.ndim,))
        self.aug_add = m2mCf['aug_add']
        self.aug_mul = m2mCf['aug_mul']
          
        # .... cropping config
        if m2mCf['inp_crop'] is None:
            self.crop = self.size
        else:
            self.crop = np.broadcast_to(m2mCf['inp_crop'], (self.ndim,))

        if m2mCf['crop_start'] is None:
            crop_start = np.zeros_like(self.size)
        else:
            crop_start = np.broadcast_to(m2mCf['crop_start'], (self.ndim,))

        if m2mCf['crop_stop'] is None:
            crop_stop = self.size
        else:
            crop_stop = np.broadcast_to(m2mCf['crop_stop'], (self.ndim,))

        if m2mCf['crop_step'] is None:
            crop_step = self.crop
        else:
            crop_step = np.broadcast_to(m2mCf['crop_step'], (self.ndim,))
        self.crop_step = m2mCf['crop_step']

        self.anchors = np.stack(np.mgrid[tuple(
            slice(crop_start[d], crop_stop[d], crop_step[d])
            for d in range(self.ndim)
        )], axis=-1).reshape(-1, self.ndim)
        self.ncrop = len(self.anchors)
        #print('DS:crop',self.crop[-1],self.crop_step,self.ncrop)
        
        #...!...!..................    
        def format_pad(pad, ndim):
            if isinstance(pad, int):
                pad = np.broadcast_to(pad, ndim * 2)
            elif isinstance(pad, tuple) and len(pad) == ndim:
                pad = np.repeat(pad, 2)
            elif isinstance(pad, tuple) and len(pad) == ndim * 2:
                pad = np.array(pad)
            else:
                raise ValueError('pad and ndim mismatch')
            return pad.reshape(ndim, 2)
          
        self.inp_pad = format_pad( m2mCf['inp_pad'], self.ndim)
        self.tgt_pad = format_pad( m2mCf['tgt_pad'], self.ndim)
        scale_factor =m2mCf['scale_factor']
        #print('FDS: inp_pad',self.inp_pad, 'sf=', scale_factor )
        if scale_factor != 1:
            tgt_size = np.asarray(tgtSh[-self.ndim:]) # NCHW is assumed 
            if any(self.size * scale_factor != tgt_size):
                raise ValueError('input size x scale factor != target size')
        self.scale_factor = scale_factor

        self.num_clique_sample =  self.ncrop*inpSh[0]
        self.nsample =  self.num_clique_sample//self.conf['clique_size']
        self.idx_offset=self.conf['clique_rank']* self.nsample
        
        if self.verb :
            logging.info('DS:ready dom=%s locSamp=%d localBS=%d myRank=%d of %d\n'%(self.conf['domain'],len(self),self.conf['local_batch_size'],self.conf['world_rank'],self.conf['world_size']))
        assert len(self)>0  # too many ranks or too few data?
        assert len(self)%self.conf['local_batch_size']==0 
        


#...!...!..................
    def openH5(self):
        cf=self.conf
        inpF=cf['h5_name']        
        dom=cf['domain']
        idxOff=cf['cube_idx_offset']
        idxStep=cf['cubes_per_clique']
        idxL=[ cf['big_cube_list'][i+idxOff] for i in range(idxStep)]
        if self.verb>0 : logging.info('DS:open fileH5 %s  dom=%s rank %d of %d idxStep=%d, idxL=%s'%(inpF,dom,cf['world_rank'],cf['world_size'],idxStep,str(idxL)))
        
        if not os.path.exists(inpF):
            print('FAILED, missing HD5',inpF)
            exit(22)

        startTm0 = time.time()

        # = = = READING HD5  start
        h5f = h5py.File(inpF, 'r')
        metaJ=h5f['meta.JSON'][0]
        inpMD=json.loads(metaJ)
        #print('DL:recovered meta-data with %d keys dom=%s'%(len(inpMD),cf['domain']))
        #pprint(inpMD)
    
        self.inpMD=inpMD # will be needad later too
                
        if self.verb : logging.info('DS:file h5Cubes=%d, dom=%s cubes: idxOff=%d idxStep=%d, rank=%d '%(inpMD['numSamples'],cf['domain'],idxOff,idxStep,cf['world_rank']))
        
        assert idxStep>0  # too few cubes OR too many ranks
                
        # data reading starts ....
        self.data_inp=h5f['lowResCube'][idxL]
        self.data_tgt=h5f['highResCube'][idxL]
        
        h5f.close()
        # = = = READING HD5  done
        
        if self.verb>0 :
            startTm1 = time.time()
            if self.verb: logging.info('DS: hd5 read time=%.2f(sec) dom=%s '%(startTm1 - startTm0,dom))
            
        if 0: # check normalization
            xm=np.mean(self.data_tgt)
            xs=np.std(self.data_tgt)
            print('xm',xm,xs,cf['domain'],self.data_tgt.shape)
            ok99
        
 
#...!...!..................
    def __len__(self):        
        return self.nsample

      
#...!...!..................
    def __getitem__(self, idx0):
        idx=idx0+self.idx_offset
        #print('DS:__getitem__',idx, self.ncrop,self.conf['domain'],self.conf['clique_rank'],self.idx_offset)
        icube, icrop = divmod(idx, self.ncrop) # returns quotient and remainder
        #print('DS:GIT idx, icube, icrop:',idx,icube, icrop)
        anchor = self.anchors[icrop]
        inp_fields=self.data_inp[icube]
        tgt_fields=self.data_tgt[icube]
        
        for d, shift in enumerate(self.aug_shift):
            #print('qqww',d,shift)
            if shift is not None:
                anchor[d] += torch.randint(int(shift), (1,))
        #print('qqww pass, inp_fields.shape:',inp_fields.shape,flush=True)
        # crop and pad are for the shapes after perm()
        # so before that they themselves need perm() in the opposite ways
        if self.augment:
            # let i and j index axes before and after perm()
            # then perm_axes is i_j, whose argsort is j_i
            # the latter is needed to index crop and pad for opposite perm()
            perm_axes = perm([], None, self.ndim)
            argsort_perm_axes = np.argsort(perm_axes.numpy())
        else:
            argsort_perm_axes = slice(None)

        #J: inefficient?, re-computes indices for every sample and avery crop
        inp_fields=m2m_crop(inp_fields, anchor,
                            self.crop[argsort_perm_axes],
                            self.inp_pad[argsort_perm_axes])
        #print('back88, inp_fields.shape:',inp_fields.shape,flush=True)
        
        tgt_fields= m2m_crop(tgt_fields, anchor * self.scale_factor,
             self.crop[argsort_perm_axes] * self.scale_factor,
             self.tgt_pad[argsort_perm_axes])
        #print('back89, tgt_fields.shape:',tgt_fields.shape,flush=True)
                
        #print('DS:crop shape in:',len(inp_fields),inp_fields[0].shape,'tgt:',tgt_fields[0].shape,flush=True)
        
        if self.augment:
            flip_axes = m2m_flip(in_fields, None, self.ndim)
            flip_axes = m2m_flip(tgt_fields, flip_axes, self.ndim)

            perm_axes = m2m_perm(in_fields, perm_axes, self.ndim)
            perm_axes = m2m_perm(tgt_fields, perm_axes, self.ndim)

        if self.aug_add is not None:
            add_fac = m2m_add(in_fields, None, self.aug_add)
            add_fac = m2m_add(tgt_fields, add_fac, self.aug_add)

        if self.aug_mul is not None:
            mul_fac = m2m_mul(in_fields, None, self.aug_mul)
            mul_fac = m2m_mul(tgt_fields, mul_fac, self.aug_mul)

        
        #print('DS:fin0 shape inp:',inp_fields.shape,'tgt:',tgt_fields.shape,flush=True)

        #print('DS:fin shape inp:',inp_fields.shape,'tgt:',tgt_fields.shape,flush=True)
        return {
            'input': inp_fields,
            'target': tgt_fields
        }

#...!...!..................    
    def assemble(self, label, chan, patches, paths): #J: not used yet
        """Assemble and write whole fields from patches, each being the end
        result from a cropped field by `__getitem__`.

        Repeat feeding spatially ordered field patches.
        After filled, the whole fields are assembled and saved to relative
        paths specified by `paths` and `label`.
        `chan` is used to split the channels to undo `cat` in `__getitem__`.

        As an example, patches of shape `(1, 4, X, Y, Z)`, `label='_out'`
        and `chan=[1, 3]`, with `paths=[['d/scalar.npy'], ['d/vector.npy']]`
        will write to `'d/scalar_out.npy'` and `'d/vector_out.npy'`.

        Note that `paths` assumes transposed shape due to pytorch auto batching
        """
        if self.scale_factor != 1:
            raise NotImplementedError

        if isinstance(patches, torch.Tensor):
            patches = patches.detach().cpu().numpy()

        assert patches.ndim == 2 + self.ndim, 'ndim mismatch'
        if any(self.crop_step > patches.shape[2:]):
            raise RuntimeError('patch too small to tile')

        # the batched paths are a list of lists with shape (channel, batch)
        # since pytorch default_collate batches list of strings transposedly
        # therefore we transpose below back to (batch, channel)
        assert patches.shape[1] == sum(chan), 'number of channels mismatch'
        assert len(paths) == len(chan), 'number of fields mismatch'
        paths = list(zip(* paths))
        assert patches.shape[0] == len(paths), 'batch size mismatch'

        patches = list(patches)
        if label in self.assembly_line:
            self.assembly_line[label] += patches
            self.assembly_line[label + 'path'] += paths
        else:
            self.assembly_line[label] = patches
            self.assembly_line[label + 'path'] = paths

        del patches, paths
        patches = self.assembly_line[label]
        paths = self.assembly_line[label + 'path']

        # NOTE anchor positioning assumes sufficient target padding and
        # symmetric narrowing (more on the right if odd) see `models/narrow.py`
        narrow = self.crop + self.tgt_pad.sum(axis=1) - patches[0].shape[1:]
        anchors = self.anchors - self.tgt_pad[:, 0] + narrow // 2

        while len(patches) >= self.ncrop:
            fields = np.zeros(patches[0].shape[:1] + tuple(self.size),
                              patches[0].dtype)

            for patch, anchor in zip(patches, anchors):
                fill(fields, patch, anchor)

            for field, path in zip(
                    np.split(fields, np.cumsum(chan), axis=0),
                    paths[0]):
                pathlib.Path(os.path.dirname(path)).mkdir(parents=True,
                                                          exist_ok=True)

                path = label.join(os.path.splitext(path))
                np.save(path, field)

            del patches[:self.ncrop], paths[:self.ncrop]


#-------------------
#-------------------
#-------------------
#  UTIL func


#...!...!..................    
def m2m_fill(field, patch, anchor): #J: not tested
    ndim = len(anchor)
    assert field.ndim == patch.ndim == 1 + ndim, 'ndim mismatch'

    ind = [slice(None)]
    for d, (p, a, s) in enumerate(zip(
            patch.shape[1:], anchor, field.shape[1:])):
        i = np.arange(a, a + p)
        i %= s
        i = i.reshape((-1,) + (1,) * (ndim - d - 1))
        ind.append(i)
    ind = tuple(ind)

    field[ind] = patch


def m2m_crop(fields, anchor, crop, pad): #J: now is non-distructive
    # designed to work with one sample, fields is one  cube: CHW
    assert all(x.shape == fields[0].shape for x in fields), 'shape mismatch'
    size = fields.shape[1:]
    ndim = len(size)
    #print('m2m_crop:a', ndim ,len(anchor) , len(crop) , len(pad),flush=True)
    assert ndim == len(anchor) == len(crop) == len(pad), 'ndim mismatch'

    ind = [slice(None)]
    for d, (a, c, (p0, p1), s) in enumerate(zip(anchor, crop, pad, size)):
        i = np.arange(a - p0, a + c + p1)
        i %= s
        i = i.reshape((-1,) + (1,) * (ndim - d - 1))
        ind.append(i)
    ind = tuple(ind)  # index of voxels belonging to selected crop-box
    fields_crop = fields[ind]
    #print('m2m_crop:c',  fields_crop.shape,flush=True) #fields_crop[0,:,0],
    return fields_crop

    '''
    #  J: I do not cpmpletley understand how this 3D cropping works, but it may be very efficient for 3D
    
    print('m2m_crop:b', len(ind),ind[0], np.array(ind[1]).shape,fields.shape,flush=True)

    for i in range(1,4):
       print('m2m_crop:i',i, np.array(ind[i]).shape,flush=True)
    print('m2m_crop:c',  fields_crop.shape,flush=True)

    m2m_crop:b 4 slice(None, None, None) (16, 1, 1) (3, 64, 64, 64)
    m2m_crop:i 1 (16, 1, 1)
    m2m_crop:i 2 (16, 1)
    m2m_crop:i 3 (16,)
    
    '''


def m2m_flip(fields, axes, ndim):
    assert ndim > 1, 'flipping is ambiguous for 1D scalars/vectors'

    if axes is None:
        axes = torch.randint(2, (ndim,), dtype=torch.bool)
        axes = torch.arange(ndim)[axes]

    for i, x in enumerate(fields):
        if x.shape[0] == ndim:  # flip vector components
            x[axes] = - x[axes]

        shifted_axes = (1 + axes).tolist()
        x = torch.flip(x, shifted_axes)

        fields[i] = x

    return axes


def m2m_perm(fields, axes, ndim):
    assert ndim > 1, 'permutation is not necessary for 1D fields'

    if axes is None:
        axes = torch.randperm(ndim)

    for i, x in enumerate(fields):
        if x.shape[0] == ndim:  # permutate vector components
            x = x[axes]

        shifted_axes = [0] + (1 + axes).tolist()
        x = x.permute(shifted_axes)

        fields[i] = x

    return axes


def m2m_add(fields, fac, std):
    if fac is None:
        x = fields[0]
        fac = torch.zeros((x.shape[0],) + (1,) * (x.dim() - 1))
        fac.normal_(mean=0, std=std)

    for x in fields:
        x += fac

    return fac


def m2m_mul(fields, fac, std):
    if fac is None:
        x = fields[0]
        fac = torch.ones((x.shape[0],) + (1,) * (x.dim() - 1))
        fac.log_normal_(mean=0, std=std)

    for x in fields:
        x *= fac

    return fac


