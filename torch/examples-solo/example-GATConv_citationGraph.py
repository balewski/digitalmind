#!/usr/bin/env python
"""
Example taken from
https://github.com/rusty1s/pytorch_geometric/blob/master/examples/gat.py

Dataset represents Citations
Nodes represent documents and edges represent citation links.
    Training, validation and test splits are given by binary masks.

details here https://github.com/rusty1s/pytorch_geometric/blob/master/torch_geometric/datasets/planetoid.py

The structure of dataset is defined here
https://pytorch-geometric.readthedocs.io/en/latest/modules/data.html


"""
import os.path as osp

import torch
import torch.nn.functional as F
from torch_geometric.datasets import Planetoid
import torch_geometric.transforms as T
from torch_geometric.nn import GATConv

'''
dataset = 'Cora'
path = osp.join(osp.dirname(osp.realpath(__file__)), './', 'data', dataset)
dataset = Planetoid(path, dataset, T.NormalizeFeatures())
'''
dataset = Planetoid('data/','Cora', T.NormalizeFeatures())
data = dataset[0]
print('M: Cora data set is loaded, dataset:',type(dataset),'\n 1data:',type(data))

print('M:dataset  features:',dataset.num_features,'classes:',dataset.num_classes)
print('1 data:',data) # <class 'torch_geometric.data.data.Data'>
# Data(edge_index=[2, 10556], test_mask=[2708], train_mask=[2708], val_mask=[2708], x=[2708, 1433], y=[2708])      
print('\ndata .x:',data.x.shape,'.y:',data.y.shape,'.edge_index:',data.edge_index.shape,'.edge_attr:',data.edge_attr,'.pos:',data.pos,'.norm:',data.norm,'.face:',data.face)

print('data.keys',data.keys)
print('data[x].shape',data['x'].shape)
print('edges:',data.num_edges,'data.num_node_features:',data.num_node_features,'\ndata.contains_isolated_nodes()=',data.contains_isolated_nodes(),'data.contains_self_loops()=',data.contains_self_loops(),'data.is_directed()=',data.is_directed())

class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.firts=1
        out_chan=8
        #class GATConv(in_channels, out_channels, heads=1, concat=True, negative_slope=0.2, dropout=0, bias=True, **kwargs)
        self.conv1 = GATConv(dataset.num_features, out_chan, heads=8, dropout=0.6)
        # On the Pubmed dataset, use heads=8 in conv2.
        self.conv2 = GATConv(
            8 * 8, dataset.num_classes, heads=1, concat=True, dropout=0.6)

    def forward(self):
        x = F.dropout(data.x, p=0.6, training=self.training)
        x = F.elu(self.conv1(x, data.edge_index))
        x = F.dropout(x, p=0.6, training=self.training)
        x = self.conv2(x, data.edge_index)
        return F.log_softmax(x, dim=1)


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model, data = Net().to(device), data.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.005, weight_decay=5e-4)

print('M: model & optimizer is created')
print(model)
''' not working - model needs 2 inputs
print('\n\nM: torchsummary.summary(model):'); 
from torchsummary import summary
input_size=(2708, 1433)
summary(model, input_size)
'''

def train():
    model.train()
    optimizer.zero_grad()
    F.nll_loss(model()[data.train_mask], data.y[data.train_mask]).backward()
    optimizer.step()


def test():
    model.eval()
    logits, accs = model(), []
    for _, mask in data('train_mask', 'val_mask', 'test_mask'):
        pred = logits[mask].max(1)[1]
        acc = pred.eq(data.y[mask]).sum().item() / mask.sum().item()
        accs.append(acc)
    return accs


for epoch in range(1, 20):
    train()
    log = 'Epoch: {:03d}, Train: {:.4f}, Val: {:.4f}, Test: {:.4f}'
    print(log.format(epoch, *test()))
