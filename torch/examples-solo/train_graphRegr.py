#!/usr/bin/env python
"""
example of end-to-end regression of 5-node graph using GATConv layers

# run on 1 Cori w/ GPU-load monitor
module load  pytorch/v1.2.0-gpu
module load cuda

salloc  -C gpu -n10 --gres=gpu:1 -Adasrepo -t4:00:00 --exclusive
 srun -n1 bash -c '  nvidia-smi '

 salloc  -C gpu -c80 -N1 --gres=gpu:8 -Adasrepo -t4:00:00 --exclusive

Run job on GPU interactively

srun -n1 -c10  bash -c '  nvidia-smi -l 3 >&L.smi & python -u  ./train_graphRegr.py '

"""
import time
import numpy as np
import torch
import torch.nn as nn
import torch_geometric.data as geom_data  # us it for DataLoader & Dataset as well or else ...
from torch_geometric.nn import GATConv
import torch.nn.functional as F
from torch.optim.lr_scheduler import ReduceLROnPlateau

import os, sys # optional, to quit treaining earlier
#1sys.path.append(os.path.relpath("../jan_utils/"))
#1from EarlyStopping_Jan import EarlyStopping


#...!...!..................
def print_one_graph(data):
    print('\nprint_one_graph(), data:',data)
    print('data.keys',data.keys)
    print('data[x].shape',data['x'].shape)
    print('data[y].shape',data['y'].shape)
    print('M:data  features:',data.num_features,', edges:',data.num_edges,', data.num_node_features:',data.num_node_features, '\ndata.contains_isolated_nodes()=',data.contains_isolated_nodes(),', data.contains_self_loops()=',data.contains_self_loops(),', data.is_directed()=',data.is_directed())
    print('dump x',data['x'])
        

#...!...!..................
def create_data(nEve,codeType=0):
    print('\nCreate grap dataset as list for nEve=',nEve,', codeType=',codeType)
    assert x_dim>y_dim
    dataL=[]
    for i in range(nEve):
        # generate  params normalized to unity - there is no correlation
        xV=np.random.uniform(-1,1,size=(num_node,x_dim))
        yV=np.random.uniform(-1,1,size=(num_node,y_dim))

        tV=yV*0.3#+np.random.normal(loc=0, scale=0.001, size=yV.shape)
        if codeType==1:  # inject Y to X so the training can converge
            xV[:,:y_dim]=tV
        if codeType==2:
            for n2n in moveCycle:
                #print('copy n2n:',n2n)
                xV[n2n[1],:y_dim]=tV[n2n[0]]
                
        # encode some info in xV
        #xV[0,x_dim-1]=1+i/1000. # graphId stored in one node
        
        x= torch.tensor(xV, dtype=torch.float)
        y= torch.tensor(yV, dtype=torch.float)
        dataL.append( geom_data.Data( x=x, y=y, edge_index=edge_index) )
    return dataL


#...!...!..................
#...!...!..................
#...!...!..................
class NetG(nn.Module):
    def __init__(self,inp_chan,out_chan,hide_dim=20,heads=1,dropFrac=0.1):
        super(NetG, self).__init__()
        self.first=1
        self.dropFrac=dropFrac
        #heads: Number of multi-head-attentions.
        #droput :  probability of the normalized attention coefficients 
        #see more at: https://pytorch-geometric.readthedocs.io/en/latest/modules/nn.html#torch_geometric.nn.conv.GATConv
        #class GATConv(in_channels, out_channels, heads=1, concat=True, negative_slope=0.2, dropout=0, bias=True, **kwargs)
        self.conv1 = GATConv(inp_chan, hide_dim, heads=heads, dropout=dropFrac)
        self.conv2 = GATConv(hide_dim*heads, hide_dim, heads=heads, dropout=dropFrac)
        self.conv3 = GATConv(hide_dim*heads, out_chan, heads=1, dropout=dropFrac)

    def forward(self,data):
        if self.first:
                print('\nin-forward()')
                #1 print_one_graph(data)
                print('J: in x',data.x.shape)
        x = F.dropout(data.x, p=self.dropFrac, training=self.training)
        x = F.elu(self.conv1(x, data.edge_index))
        if self.first: print('J: conv1 x:',x.shape)
        
        x = F.dropout(x, p=self.dropFrac, training=self.training)
        x = self.conv2(x, data.edge_index)
        if self.first: print('J: conv2 x:',x.shape)
                
        x = F.dropout(x, p=self.dropFrac, training=self.training)
        x = self.conv3(x, data.edge_index)
        if self.first: print('J: y',x.shape)
        self.first=0
        return x

#...!...!..................
    def summary(self):
        numLayer=sum(1 for p in self.parameters())
        numParams=sum(p.numel() for p in self.parameters())
        return {'modelWeightCnt':numParams,'trainedLayerCnt':numLayer,'modelClass':self.__class__.__name__}


# = = = = = = = = = = = = = = = 
# UTIL FUNCTIONS
# = = = = = = = = = = = = = = = 
#...!...!..................
def model_train( model, device,train_loader, loss_func, optimizer):
    model.train()
    train_loss=0
    for batch_idx, graphBatch in enumerate(train_loader):
        graphBatch = graphBatch.to(device)
        optimizer.zero_grad()
        output = model(graphBatch)
        #print('output',output.shape)
        lossOp = loss_func(output, graphBatch.y)
        lossOp.backward()
        optimizer.step()
        train_loss +=lossOp.item()
    train_loss /= len(train_loader)
    return train_loss


#=================================
#=================================
#  M A I N 
#=================================
#=================================

epochs=100
batch_size=128
steps=300
num_workers=2 # to load the data in parallel 
device ='cpu' ;device='cuda'
learning_rate = 1e-3
hide_dim=200
gat_heads=3
dropFrac=1.e-3
num_eve=steps*batch_size

# Graph properties:
x_dim=6 # input channles/node
y_dim=2 # output channles/node
codeType=1 #  0=X random, 1= Y added to X  to the same node,
# 2=  Y added to X to next node on the edge
num_node=5 # do not chenge, w/o adjacency matrix below

'''
the 5-node graph has only those nontrivial edges
edge: (2, 4)
edge: (4, 1)
edge: (1, 0)
edge: (1, 3)
other edges are added to make it bi-directioanl and have self_loops
'''
edgePairs= [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4],  # self-loops
            [2, 4], [4, 2], # bi-directional edges
            [4, 1], [1, 4],
            [1, 0], [0, 1],
            [1, 3], [3, 1]]
moveCycle=[(4, 2),(1, 4),(0, 1),(3,3)] # node 3 is left alone

# this is bi-directional list of edges formatted as per torch requirement:
edge_index = torch.tensor(edgePairs, dtype=torch.long).t().contiguous()

data_list=create_data(num_eve,codeType)
data1=data_list[4]; print_one_graph(data1) # check if graphId is visible

loader=geom_data.DataLoader(data_list, batch_size=batch_size,shuffle=True,num_workers=num_workers)
print('\n print a fraction one batch of data , size=',batch_size)
dataIter=iter(loader)
bb = next(dataIter)

print('1 batch dump:',bb,', len=',',num_graphs=',bb.num_graphs)
print('batch keys:',bb.keys, type(bb))
#1 for key in bb.keys: print('key=',key,' bb[key]=',bb[key])


print('\n Initialize model') 
model = NetG(x_dim,y_dim,hide_dim=hide_dim, heads=gat_heads,dropFrac=dropFrac)
print(model)
print(model.summary())

# define loss function
loss_fn = nn.MSELoss()# Mean Squared Loss

# Initialize optimizer
lossDelta=1e-4
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
scheduler = ReduceLROnPlateau(optimizer, mode='min', factor=0.3, patience=5, cooldown=3, verbose=True,threshold_mode='abs',threshold=lossDelta)

# optional: initialize the early_stopping object
#1earlyStop = EarlyStopping(patience=10, delta=lossDelta, verbose=True, warmup=10)

if device =='cuda' : assert  torch.cuda.is_available()
model=model.to(device) # re-cast model on device, data will be cast later

print('optimizer=',optimizer)
print('Train for epochs=',epochs)

startT=time.time()
for epoch in range(epochs):
    loss=model_train( model, device, loader, loss_fn, optimizer)
    bestLoss=loss
    doMon= (epoch<4) or ( epoch%10==0)  or  (epoch==epochs-1)
    currLR=optimizer.param_groups[0]['lr']
    '''
    if  earlyStop(loss, model) :
        bestLoss=-earlyStop.best_score
        doMon=True
        print('Early stopping, patience=%d, best loss=%.3g'%(earlyStop.counter,bestLoss))
    '''
    if doMon: print('M: epoch=%d, LR=%.3g, end-loss=%.3g, elaT=%d sec'%(epoch,currLR ,loss,time.time()-startT))

    scheduler.step(loss)  # Decay Learning Rate
    #if  earlyStop.early_stop: break

print('avr residue=%.3g'%np.sqrt(bestLoss))
