#!/usr/bin/env python

'''
 example of simple FC 
 trains
saves model
reads model
predicts

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time
import copy
import warnings

startT0 = time.time()
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader

import numpy as np
import matplotlib.pyplot as plt
print('imported PyTorch ver:',torch.__version__,' elaT=%.1f sec,'%(time.time() - startT0))


# Define PyTorch model
#-------------------
#-------------------
#-------------------
class JanModel(nn.Module):  #  CNN+FC, MNIST-like input
    def __init__(self):
        super(JanModel, self).__init__()
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        self.fc1 = nn.Linear(4*4*50, 500)
        self.fc2 = nn.Linear(500, 10)
        
    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1,np.prod(x.shape[1:]))
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        y = F.tanh(x)
        return y

#-------------------
#-------------------
#-------------------
class JanDataset(Dataset):
    def __init__(self, npX, npY):
        self.X=npX
        self.Y=npY
        self.num_eve=Y.shape[0]

    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.X[idx]
        y=self.Y[idx]
        sample =(x,y)
        return sample

# = = = = = = = = = = = = = = = 
def model_train(model,device, train_loader,loss_func, optimizer):
    train_loss=0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        lossOp = loss_func(output, target)
        lossOp.backward()
        optimizer.step()
        train_loss +=lossOp.item()
    train_loss /= len(train_loader)
    return train_loss


# = = = = = = = = = = = = = = = 
def model_infer(model, device, test_loader,loss_func,outType=0):
    test_loss = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            lossOp=loss_func(output, target)
            test_loss += lossOp.item()
    test_loss /= len(test_loader)
    return test_loss
    if outType==0:
        return test_loss
    if outType==1: # returns just last batch
        return test_loss,data,target, output


# = = = = = = = = = = = = = = = 
def save_model_full(outF,model,optimizer,currEpoch,currLoss):
    torch.save({'epoch': currEpoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': currLoss,}, outF)
    xx=os.path.getsize(outF)/1048576
    print('  closed PyTorch chkpt:',outF,' size=%.2f MB'%xx)

    
# = = = = = = = = = = = = = = = 
def load_model_full(inpF,model,optimizer):
    print(' load PyTorch chkpt:',inpF)
    checkpoint = torch.load(inpF)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    lastEpoch = checkpoint['epoch']
    lastLoss = checkpoint['loss']
    return lastEpoch,lastLoss

#=================================
#=================================
#  M A I N 
#=================================
#=================================
inp_dim=28
fc_dim=15
out_dim=10

epochs=5
batch_size=16
steps=10
num_eve=steps*batch_size
num_workers=2 #Load the data in parallel 

outPath='out/'
# Initialize model 
model = JanModel()

# Print model's state_dict
print("M:Model's state_dict:")
for param_tensor in model.state_dict():
    print(param_tensor, "\t", model.state_dict()[param_tensor].size())

# Initialize optimizer
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# Print optimizer's state_dict
print("M:Optimizer's state_dict:")
for var_name in optimizer.state_dict():
    print(var_name, "\t", optimizer.state_dict()[var_name])

print('\n\nM: print(model):'); print(model)


print('\n\nM: torchsummary.summary(model):')
from torchsummary import summary
inpShape=(1,inp_dim,inp_dim)  # CNN expect format: Chan,W,H
summary(model,inpShape)

print('\nM: generate data and train, num_eve=',num_eve)

startT = time.time()
Y=np.random.uniform(0,1, size=(num_eve,out_dim)).astype('f')
print('M: done Y shape',Y.shape,', one:',Y[0])

X=np.random.uniform(-0.8,0.8, size=(num_eve,)+inpShape).astype('f')
print('M: done X shape',X.shape,' elaT=%.1f sec,'%(time.time() - startT))

# inject Y to X so training can converge
#X[:,0,:out_dim,0]=Y*0.3+np.random.normal(loc=0, scale=0.001, size=(num_eve,out_dim))

print('\nCreate pth-Dataset instance & test it')
trainDst=JanDataset(X,Y)
for i in range(len(trainDst)):
    (x,y) = trainDst[i]
    print('i=',i, 'shapes x,y:', x.shape,y.shape)
    print('x[:,2]',x[:,2])
    print('y',y)
    if i >0:        break

print('\nCreate pth-DataLoader instance & test it')
    
trainLoader = torch.utils.data.DataLoader(trainDst, batch_size=batch_size,
                                    shuffle=True, num_workers=num_workers)

print('\n Get a batch of training data from loader')
xx, yy = next(iter(trainLoader))
print(xx.shape,yy.shape)
print('Y[:,]',yy[:,0])

print('\n= = = = Prepare for the treaining = = =\n')
loss_fn = nn.MSELoss()# Mean Squared Loss
device='cpu'
print('optimizer=',optimizer)
print('loss=',loss_fn)



print('\n- - - - - - - TrainA for epochs=',epochs)
model=model.to(device) # re-cast model on device, data will be also casted
for epoch in range( epochs ):
    loss=model_train(model,device, trainLoader,loss_fn, optimizer)
    print('epoch=%d loss=%.3f'%(epoch,loss))


    
print("\n-----------  Save model *state* as pth-dict for inference ")
outF=outPath+'janToy.state.pth'
torch.save(model.state_dict(), outF)
xx=os.path.getsize(outF)/1048576
print('  closed  pth:',outF,' size=%.2f MB'%xx)


print("\n-----------  Read model *state* from %s -----------"%outF)
model5 = JanModel() # must create model first
model5.load_state_dict(torch.load(outF))



print("\n-----------  predict ----")
model5.eval() #must  to set dropout and batch normalization layers to evaluation mode before running inference.
loss=model_infer(model5,device, trainLoader,loss_fn)
print('infer loss=',loss)




outF=outPath+'janToy.model.tar'
print("\n----------- Saving & Loading a General Checkpoint for Inference and/or Resuming Training \n from/to: %s "%outF)

#To save multiple components, organize them in a dictionary and use torch.save() to serialize the dictionary.
save_model_full(outF,model,optimizer,epoch,loss)

print("\n-----------  Load model for re-training")
model6 = JanModel() # must create model first
optimizer6 = torch.optim.Adam(model.parameters(), lr=learning_rate)

load_model_full(outF,model6,optimizer6)
print('model6:',model6)
epochs=3
model6=model6.to(device) 
print('TrainB for epochs=',epochs)
model.train()
for epoch in range( epochs ):
    loss=model_train(model6,device, trainLoader,loss_fn, optimizer6)
    print('epoch=%d loss=%.3f'%(epoch,loss))



# - - - - - - - - - - - - - - - 
print("\nTo save on GPU, Load on CPU  --> use  map_location=device")
# see: https://pytorch.org/tutorials/beginner/saving_loading_models.html

