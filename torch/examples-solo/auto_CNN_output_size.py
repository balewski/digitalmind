#!/usr/bin/env python

#  Automatically compute the size of the output of CNN+Pool portion of the model, needed as input to the first FC layer 

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
import numpy as np

# - - - - - - - -
# - - - - - - - -
# - - - - - - - -
class NetJ(nn.Module):
    def __init__(self,input_size):
        super(NetJ, self).__init__()
        
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        with torch.no_grad():
            # process 1 example through the CNN portion of model
            x1=torch.tensor(np.zeros((1,)+input_size), dtype=torch.float32)
            y1=self.forwardCnnOnly(x1)
            self.flat_dim=np.prod(y1.shape[1:]) 
            print('myNet flat_dim=',self.flat_dim)
        
        self.fc1 = nn.Linear(self.flat_dim, 500)
        self.fc2 = nn.Linear(500, 10)
        self.first=1
        print('J: Net cstr')

    def forwardCnnOnly(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        return x
    
    def forward(self, x):
        x=self.forwardCnnOnly(x)
        x = x.view(-1,self.flat_dim)
        if  self.first: print('J: f',x.shape)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        self.first=0
        return F.log_softmax(x, dim=1)

#=================================
#=================================
#  M A I N
#=================================
#=================================

def main():
    device = 'cpu'
    input_size=(1,28,28)  # needs fc_inp=800
    #input_size=(1,26,26)  # needs fc_inp=450
    input_size=(1,128,228)  # needs fc_inp=800

    model = NetJ(input_size).to(device)
    
    print('\n\nM: torchsummary.summary(model):'); print(model)
    from torchsummary import summary
    summary(model, input_size)
         
if __name__ == '__main__':
    main()
