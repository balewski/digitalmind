#!/bin/bash 

if [ ${SLURM_PROCID} -eq 0 ] ; then
    echo smi-on-`hostname`
    echo S2: proc=$SLURM_PROCID `hostname` isShifter=`env|grep  SHIFTER_RUNTIME` 
    python -V
    python -c 'import torch; print("S2: pytorch:",torch.__version__)'
    nvidia-smi -l 5 >L.smi_${SLURM_JOBID} &
fi

# starts the training task
${CMD}
echo D:done rank ${SLURM_PROCID}
