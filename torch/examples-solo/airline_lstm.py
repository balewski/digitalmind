#!/usr/bin/env python3


# airline_lstm.py
# PyTorch 1.7.0-CPU Anaconda3-2020.02  Python 3.7.6
# copied from https://jamesmccaffrey.wordpress.com/2020/12/10/time-series-regression-using-a-pytorch-lstm-network-2/
''' Jan's notes
seq_len=4 
x=shape (seq_len, bat_sz, input_size==inp_chan)
BS=1: Fr:x torch.Size([4, 1, 1])
BS=3: Fr:x torch.Size([4, 3, 1])

PROBLEMS: this code is not resetting LSTM hidden state and is not working correctly for BS>1


'''


import numpy as np
import torch as T
device = T.device("cpu")  # apply to Tensor or Module
import time

# -----------------------------------------------------------

# bat_size = 1 strongly recommended
# seq_len = 4 should divide into 120 evenly

class AirlineDataset(T.utils.data.Dataset):
  # serve up data for training
  # "1949-01",112
  # . .
  # "1958-12",337
  # train = 120 rows, test = 24 rows


  def __init__(self, src_file, seq_len):
    all_data = np.loadtxt(src_file, usecols=1, 
      delimiter=",", skiprows=0, dtype=np.float32)
    all_data /= 100.0        # normalize
    L = len(all_data)        # 120 for train, 24 for test
    num_items = L - seq_len -1 # 120 - 4 = 166 input sequences

    tmp_x = []; tmp_y = []
    for i in range(num_items):
      seq = all_data[i:i+seq_len]   # fetch 4 values
      nxt = all_data[i+seq_len]     # next value is the target
      tmp_x.append(seq)
      tmp_y.append(nxt)

    self.x_data = T.tensor(tmp_x, dtype=T.float32).to(device)
    self.y_data = T.tensor(tmp_y, dtype=T.float32).to(device)

  def __len__(self):
    return len(self.x_data)

  def __getitem__(self, idx):
    seq_in = self.x_data[idx] 
    target = self.y_data[idx]
    sample = { 'seq_in' : seq_in, 'target' : target }
    return sample

# -----------------------------------------------------------

class MyLSTM(T.nn.Module):
  def __init__(self):
    super(MyLSTM, self).__init__()
    self.lstm = T.nn.LSTM(1, 10)   # 1 input_size, state size
    self.linear1 = T.nn.Linear(10, 5)  # intermediate layer
    self.linear2 = T.nn.Linear(5, 1)   # output layer

  def forward(self, x):
    # x must have shape (seq_len, bat_sz, input_size)
    # seq_len = 4 (values in one sequence)
    # batch = ?, input_size = 1 (analogous to embed dim)
    (all_outs, (final_oupt,final_state)) = self.lstm(x)

    # print(final_oupt.shape)    # shape [1,bat_sz,10] 
    # print(all_outs[-1].shape)  # shape [bat_sz,10] 
    # input()

    oupt = self.linear1(all_outs[-1])
    oupt = self.linear2(oupt)    # shape [bat_sz,1]
    return oupt

# -----------------------------------------------------------

def accuracy(model, ds, pct):
  # assumes model.eval()
  # percent correct within pct of true income
  # process 1 item at a time
  n_correct = 0; n_wrong = 0

  for i in range(len(ds)):
    X = ds[i]['seq_in']        # [4]
    X = X.reshape(4,1,1)
    Y = ds[i]['target']        # [1]
    with T.no_grad():
      oupt = model(X)          # [1,1,1]
    oupt = oupt.reshape(1)

    delta = np.abs(oupt.item() - Y.item())
    allowable = np.abs(pct * Y.item())
    if delta <= allowable:
      n_correct += 1
    else:
      n_wrong += 1

    # print values to make a graph
    # print("actual  predicted")
    # print("%0.2f %0.4f" % (Y.item(), oupt.item()))

  acc = (n_correct * 1.0) / (n_correct + n_wrong)
  return acc

#...!...!..................
def create_dataset(num_sampl, outF, t0=0, period=10):
    tax=[t+t0 for t in range(num_sampl)]
    x=np.array(tax)/period*2*np.pi
    y=50*(1.+np.sin(x))
    #y=tax
    fd=open(outF,'w')
    for i in range(num_sampl):        
        fd.write('"abc-%d",%d\n'%(i,int(y[i])))
    fd.close()
                 
                 
#=================================
#=================================
#  M A I N 
#=================================
#=================================

def main():
  # 0. get started
  print('create data sets as sin-waves w/ noise')
  train_file = "airline_train.csv"
  create_dataset(121,train_file,t0=0)
  test_file = "airline_test.csv"
  create_dataset(25,test_file,t0=13)
    
  print("\nBegin PyTorch LSTM airline time series \n")
  T.manual_seed(0)
  np.random.seed(0)

  # 1. create DataLoader objects
  print("Creating Airline Dataset objects ")
  
  train_ds = AirlineDataset(train_file, 4)  # seq_len = 4
  test_ds = AirlineDataset(test_file, 4)    # all 24 rows
  print('DS train size=',len(train_ds))
  
  bat_sz = 1   # batch size for TSR is tricky
  
  train_ldr = T.utils.data.DataLoader(train_ds,batch_size=bat_sz, shuffle=True)  
  # add drop_last=True if needed
  # use shuffle=False if bat_sz != 1 but risk of over-fitting

  '''
   BS=1: 
       bulk= {'seq_in': tensor([[0.7000, 0.4000, 0.2200, 0.2200]]), 'target': tensor([0.4000])}
  BS=2:
       bulk= {'seq_in': tensor([[0.7000, 0.9900, 1.1700, 1.1700],
        [0.9900, 1.1700, 1.1700, 0.9900]]), 'target': tensor([0.9900, 0.7000])}
  '''
  
  if 0:
    print('\n print one batch of training data BS=',bat_sz)
    bulk= next(iter(train_ldr))
    print('bulk=',bulk)
    #print('X:',xx.shape,'Y:',yy.shape)
    #print('Y[:,]',yy[:,0])
    # - - - -  DATA  READY - - - - -
    ok01

  # 2. create network
  lstm_tsr = MyLSTM().to(device)

  # 3. train model
  max_epochs = 100
  ep_log_interval = 10
  lrn_rate = 0.01

  loss_func = T.nn.MSELoss()
  optimizer = T.optim.SGD(lstm_tsr.parameters(), lr=lrn_rate)

  print("\nbat_size = %3d " % bat_sz)
  print("loss = " + str(loss_func))
  print("optimizer = SGD")
  print("max_epochs = %3d " % max_epochs)
  print("lrn_rate = %0.3f " % lrn_rate)

  
  print("\nStarting training")
  startT = time.time()
  lstm_tsr = lstm_tsr.train()
  for epoch in range(0, max_epochs):
    epoch_loss = 0  # for one full epoch

    for (batch_idx, batch) in enumerate(train_ldr):
      X = batch['seq_in']        # shape [bat_sz,4]
      X = X.reshape(4,bat_sz,1)  # seq_len, bat_sz, "input_len"
      Y = batch['target']        # shape [1] via Dataset
      Y = Y.reshape(bat_sz,1)    # shape [bat_sz, 1]

      optimizer.zero_grad()
      oupt = lstm_tsr(X)      # oupt has shape [bat_sz,1]

      loss_val = loss_func(oupt, Y)  # a tensor
      epoch_loss += loss_val.item()  # accumulate
      loss_val.backward()
      optimizer.step()

    if epoch % ep_log_interval == 0:
      print("epoch = %4d   loss = %0.4f" % (epoch, epoch_loss))

  print("Done ")

  # 4. evaluate model accuracy
  print("\nComputing model accuracy (within 15%)")
  lstm_tsr = lstm_tsr.eval()
  acc_train = accuracy(lstm_tsr, train_ds, 0.15)  # item-by-item
  print("Accuracy on training data = %0.4f" % acc_train)

  acc_test = accuracy(lstm_tsr, test_ds, 0.15)  # item-by-item
  print("Accuracy on test data = %0.4f" % acc_test)

  # 5. extrapolate 6 months ahead via bootstrapping
  print("\nExtrapolating 6 months past all known data ")
  v = T.tensor([5.08, 4.61, 3.90, 4.32],
    dtype=T.float32).to(device)
  for i in range(6):
    X = v.reshape(4,1,1)  # one at a time
    with T.no_grad():
      oupt = lstm_tsr(X)
    oupt = oupt.reshape(1)
    print(oupt)
    for j in range(3):
      v[j] = v[j+1]
    v[3] = oupt.item()
     
  print("\nEnd LSTM airline time series ")

if __name__ == "__main__":
  main()
