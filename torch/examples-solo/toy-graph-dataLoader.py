#!/usr/bin/env python
"""
example from 
https://pytorch-geometric.readthedocs.io/en/latest/notes/introduction.html

simple graph w/ 3 nodes and 2 edges

combine it with dataLoader
https://pytorch-geometric.readthedocs.io/en/latest/notes/create_dataset.html

data_list = [Data(...), ..., Data(...)]
loader = DataLoader(data_list, batch_size=32)
See 2nd question in https://pytorch-geometric.readthedocs.io/en/latest/notes/create_dataset.html#frequently-asked-questions. 

Now, if you were wondering how it does batches with graphs (because graph sizes could be different like in our Chaya case), it employs a  clever trick, which is to arrange each adjacency matrix of a graph along the diagonal of a larger sparse matrix. What this allows it to do is to consider a batch of graphs as a single giant graph.  It adds no performance overhead as these are represented as compressed sparse graphs, so no wasted computations over zero non-digonal elements. He has a good write-up here https://pytorch-geometric.readthedocs.io/en/latest/notes/introduction.html#mini-batches.

"""
import torch
import torch_geometric.data as geom_data  # us it for DataLoader & Dataset as well or else ...

# this is bi-directional list of edges for 3 nodes, read it as columns
edge_index = torch.tensor([[0, 1, 1, 2],
                           [1, 0, 2, 1]], dtype=torch.long)
x = torch.tensor([[-1], [0], [1]], dtype=torch.float)
y = torch.tensor([[1,2], [3,4], [5,6]], dtype=torch.float)

data1 = geom_data.Data( x=x, y=y, edge_index=edge_index )
print('data1',data1)
print('data.keys',data1.keys)
print('data[x].shape',data1['x'].shape)
print('M:data  features:',data1.num_features,'edges:',data1.num_edges,'data.num_node_features:',data1.num_node_features,'\ndata.contains_isolated_nodes()=',data1.contains_isolated_nodes(),'data.contains_self_loops()=',data1.contains_self_loops(),'data.is_directed()=',data1.is_directed())

print('\n create and test data loader')
numEve=128
data_list = [data1.clone() for i in range(numEve)] # just duplicate the same graph
loader = geom_data.DataLoader(data_list, batch_size=4)

print('\n print a fraction one batch of data ')
dataIter=iter(loader)
bb = next(dataIter)

print('1 batch dump:',bb,', len=',len(bb), type(bb),',num_graphs=',bb.num_graphs)
print('batch keys:',bb.keys)
for key in bb.keys:
        print('key=',key,' bb[key]=',bb[key])

'''        BAD CODE
for i in range(bb.num_graphs):
    print('\nsample i=',i)
    for key in bb.keys:
         print('key=',key)#,' bb[key][i]=',bb[key][i])   
        
    sample=bb()[i] # THIS IS WORNG
    print('sample i=',i,type(sample),next(sample))
    for key in sample:
        print('key=',key,' val=',sample[key])



        sample=bb["x"][i] 
'''
