#!/usr/bin/env python
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchsummary import summary

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        # use fixed objects
        #self.fc1 = nn.Linear(360, 50)
        #self.fc2 = nn.Linear(50, 10)
        
        # use dynamic objects
        # https://discuss.pytorch.org/t/when-should-i-use-nn-modulelist-and-when-should-i-use-nn-sequential/5463
        self.fcx = nn.ModuleList()
        self.fcx.append(nn.Linear(360, 80))
        self.fcx.append(nn.Linear(80, 10))


    def forward(self, x):
        print('J: in',x.shape)
        x = F.relu(F.max_pool2d(self.conv1(x), 2)) ;  print('J: a',x.shape)
        x = F.relu(F.max_pool2d(x, 2)) ;  print('J: b',x.shape)
        x = x.view(-1,360)  ;  print('J: c',x.shape)
        # use fixed objects
        #x = F.relu(self.fc1(x))  ;  print('J: d',x.shape)
        #x = self.fc2(x)    ; print('J: y',x.shape)
        
        # use dynamic objects
        x = F.relu(self.fcx[0](x))  ;  print('J: d',x.shape)
        x = self.fcx[1](x)    ; print('J: y',x.shape)
        return F.log_softmax(x, dim=1)

device='cpu'
model = Net().to(device)
print(model)
# input_size=(channels, H, W)) if CNN is first
summary(model, (1, 28, 28))
