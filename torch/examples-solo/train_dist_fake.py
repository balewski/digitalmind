#!/usr/bin/env python
'''  Verify torch.dist works on multipl GPUs


*****  Perlmutter
 salloc -C gpu -q interactive -t4:00:00 --gpus-per-task=1 --image=nersc/pytorch:ngc-21.08-v2 -A m3363_g --ntasks-per-node=4 -N 4

export MASTER_ADDR=`hostname` 
#export MASTER_PORT=8888
export NCCL_DEBUG=info

 srun -n16 -l shifter python3 -u ./train_dist_fake.py

***** Crusher
 salloc -A AST153_crusher -p batch  -t 2:30:00  -N 1 --x11 --ntasks-per-node=8 --gpus-per-task=1 --cpus-per-task=8 --exclusive

export MASTER_ADDR=`hostname` 
#export MASTER_PORT=8888

SIF=/gpfs/alpine/ast153/scratch/balewski/crusher_amd64/rocm4.5-crusher-torch.v3.sif 

 srun -n8 -l singularity exec  $SIF python3 -u  ./train_dist_fake.py


'''
import os,time
import torch
import torch.distributed as dist
import socket  # for worker name
import sys

#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == '__main__':
    
    os.environ['MASTER_PORT'] = "8886"

    os.environ['RANK'] = os.environ['SLURM_PROCID']
    os.environ['WORLD_SIZE'] = os.environ['SLURM_NTASKS']
    
    params={} # to be used by Trainer & DataLoader
    params['hostname']=os.environ['MASTER_ADDR']
    if 'crusher' in params['hostname']:
        params['local_rank'] =0
    else:
        params['local_rank'] = int(os.environ['SLURM_LOCALID'])  # needed by DistributedDataParallel
    params['world_size'] = int(os.environ['WORLD_SIZE'])
    params['world_rank'] =int(os.environ['RANK'])
    params['worker_name']=socket.gethostname()
    print('M:params',params)
    if params['world_rank']==0:
        print('M:python:',sys.version,'torch:',torch.__version__)
    
    if params['world_size'] > 1:  # multi-GPU training
      torch.cuda.set_device(params['local_rank'])
      dist.init_process_group(backend='nccl', init_method='env://')
      assert params['world_rank'] == dist.get_rank()
      #print('M:locRank:',params['local_rank'],'rndSeed=',torch.seed())
    
    if params['world_rank'] == 0:
      print("M:start rank=%d of %d"%(params['world_rank'],params['world_size']))


    # test all_reduce on GPUs
    device=torch.cuda.current_device()
    x=1001+params['world_rank']
    x=torch.tensor([x], device=device)
    print("M:before rank=%d device=%d x="%(params['world_rank'],device),x)
    dist.barrier()
    dist.all_reduce(x)
    nG=params['world_size']
    xTrue=1000*nG+nG*(nG+1)//2
    print("M:after rank=%d x=%.1f  truth=%.1f"%(params['world_rank'],x,xTrue))
    assert x==xTrue
    dist.barrier()
    time.sleep(1)
    if params['world_rank'] == 0:
      print("M:done rank=%d of %d  SUCCESS"%(params['world_rank'],params['world_size']))
