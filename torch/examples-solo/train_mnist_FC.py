#!/usr/bin/env python

'''
 example  from
https://github.com/pytorch/examples/blob/master/mnist/main.py

NET: BS=64
J: a torch.Size([64, 1, 28, 28])
J: b torch.Size([64, 20, 24, 24])
J: c torch.Size([64, 20, 12, 12])
J: d torch.Size([64, 50, 8, 8])
J: e torch.Size([64, 50, 4, 4])
J: f torch.Size([64, 800])
J: g torch.Size([64, 500])
J: h torch.Size([64, 10])

'''

import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms

# - - - - - - - -
# - - - - - - - -
# - - - - - - - -
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        self.fc1 = nn.Linear(4*4*50, 500)
        self.fc2 = nn.Linear(500, 10)
        self.first=1
        print('J: Net cstr')
        

    def forward(self, x):
        if  self.first: print('J: a',x.shape)
        x = F.relu(self.conv1(x))
        if  self.first: print('J: b',x.shape)
        x = F.max_pool2d(x, 2, 2)
        if  self.first: print('J: c',x.shape)
        x = F.relu(self.conv2(x))
        if  self.first: print('J: d',x.shape)
        x = F.max_pool2d(x, 2, 2)
        if  self.first: print('J: e',x.shape)
        x = x.view(-1, 4*4*50)
        if  self.first: print('J: f',x.shape)
        x = F.relu(self.fc1(x))
        if  self.first: print('J: g',x.shape)
        x = self.fc2(x)
        if  self.first: print('J: h',x.shape)
        self.first=0
        return F.log_softmax(x, dim=1)

# - - - - - - - -
def train(args, model, device, train_loader, optimizer, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        #print('MNIST train batch shape:',data.shape,target.shape); ok56
        
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))

# - - - - - - - -
def test(args, model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))

#=================================
#=================================
#  M A I N
#=================================
#=================================

def main():
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=3, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=50, metavar='N',
                        help='how many batches to wait before logging training status')

    parser.add_argument("-d","--dataPath",help="path to input",
                        default='data/')

    parser.add_argument("-o", "--outPath",
                    default='./',help="output path for plots and tables")

    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the current Model')

    #=================================
    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(args.seed)

    device = torch.device("cuda" if use_cuda else "cpu")
    print('use device=',device)

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}

    
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST(args.dataPath, train=True, download=True,
                       transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)


    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST(args.dataPath, train=False, transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.test_batch_size, shuffle=True, **kwargs)


    model = Net().to(device)
    print('\n= = = = Prepare for the treaining = = =\n')
    print('\n\nM: torchsummary.summary(model):'); print(model)
    from torchsummary import summary
    summary(model,(1,28,28), batch_size=15) #, device='cuda')
    
    optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)

    for epoch in range(1, args.epochs + 1):
        train(args, model, device, train_loader, optimizer, epoch)
        test(args, model, device, test_loader)

    if (args.save_model):
        torch.save(model.state_dict(),args.outPath+"/mnist_cnn.pth")
        
if __name__ == '__main__':
    main()
