#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

#Example of driving distributed Torch training w/o Slurm
# Note, the script must be executed inside srun -n1 command  because this is the only way to access GPUs:
# Note 2 - there will be no prompt !!! inside teh shell

#  salloc -N1  -C gpu  -c 10  --gres=gpu:4 --ntasks-per-node=4  -t4:00:00 
#  srun -n1 bash
#  ls
#  ./noSlurm_dist_drive.py


# limitation - can't cross node boundary, but could be extended to do so with ssh-loop over worker nodes

# 1-GPU code:
execCode=./train_FC-distr_gpu.py    # example 1
# example 2 for 4 GPUs
#execCode="./train_SquareRegr.py --device cuda -n10000 --localBS  128  -e 30"

# setup IP/port  the 'normal-way'
line=`ifconfig |grep 'ib1:' -A1 |tail -n1` # cori-gpu node
headIp=`echo $line | cut -f2 -d\ `
export MASTER_ADDR=$headIp
export MASTER_PORT=8888
echo headIP=$headIp


# decide how many GPU to use
num_gpu=4
max_gpu=`nvidia-smi|grep V100|wc -l`
echo  list visible GPUs
nvidia-smi|grep V100
if [ $max_gpu -lt $num_gpu ]; then
    echo avaliable  $max_gpu GPUs is lower than requested $num_gpu, abort
    exit 22
fi

echo run on $num_gpu GPUs  vs. possible $max_gpu

for j in $(seq 1 $num_gpu ); do
    i=$[ $j -1 ]
    echo start task $j of $num_gpu
    # overwrite SLURM env used to assign the rank & GPU ID for each task
    SLURM_PROCID=$i SLURM_LOCALID=$i SLURM_NTASKS=$num_gpu   $execCode &
done

echo S:submitted $num_gpu tasks in bg
top ibn1
echo remember there is no no shell prompt visible, you have now shell control back

exit 0


   




outPath=/global/cscratch1/sd/balewski/ontra3/predPerCell/b


# per cell  design=smt190927_a
trainPath0=/global/cscratch1/sd/balewski/ontra3/1181540/
#Cori, BBP019 HPO design=smt190927_a
trainPathOntra3=~/prjn/neuronBBP-outHPO-8kHz/ontra3-packSetD/1179761_1_bbp019/out

numEve=50000
taskList=witnessOntra3.list

echo path=$outPath  numEve=$numEve
mkdir -p $outPath



for k in `seq 16 17`; do
    cellName=`head -n $[ $k ] $taskList |tail -n 1`
    echo M: k=$k cellName=$cellName

    modelPath=${trainPath0}/${k}/out
    echo $k  $modelPath 

    #...... per cell:
    prefix=${cellName}_perCell
    log=$outPath/log.${prefix}
    ./predict_CellSpike.py --cellName $cellName -n $numEve --seedModel $modelPath  -X >& $log
    grep "global lossMSE" $log
    mv out/cellSpike_predict_test_f9.png $outPath/${prefix}.cellSpike_predict_witness_f9.png
    mv out/cellSpike.sum_pred_test.yaml  $outPath/${prefix}.cellSpike.sum_pred_witness.yaml
    
    #...... Ontra3:
    modelPath=$trainPathOntra3
    prefix=${cellName}_ontra3
    log=$outPath/log.${prefix}
    ./predict_CellSpike.py --cellName $cellName -n $numEve --seedModel $modelPath  -X >& $log
    grep  "global lossMSE" $log
    mv out/cellSpike_predict_test_f9.png $outPath/${prefix}.cellSpike_predict_witness_f9.png
    mv out/cellSpike.sum_pred_test.yaml  $outPath/${prefix}.cellSpike.sum_pred_witness.yaml
    
    
    #exit
done

exit


# per cell  design=a2f791f3a	
trainPath0=/global/homes/b/balewski/prjn/neuronBBP-outHPO-8kHz/ontra3-packSetD/1149899_perCell
#Summit Ontra3 HPO design=a2f791f3a	
trainPathOntra3=/global/homes/b/balewski/prjn/neuronBBP-outHPO-8kHz/ontra3-packSetD/459230_6_out/


