#!/usr/bin/env python3

'''
 example of simple RNN +regression
 a noisy Y is added to X so training  converges 
 uses DataLoader (aka generator) for input

based on 
https://jamesmccaffrey.wordpress.com/2020/12/10/time-series-regression-using-a-pytorch-lstm-network-2/
Reference speed

CoriGPU, srun -n1
module load pytorch/1.7.0-gpu

 srun -n1 ./train_LSTM-one_gpu.py


epochs=12
steps=200
batch_size=16
num_layers = 1 #number of stacked lstm layers

=============================================================================
Layer (type:depth-idx)                   Output Shape              Param #
=============================================================================
JanModel2                                --                        --
├─LSTM: 1-1                              [16, 280, 12]             720
├─ReLU: 1-2                              [16, 12]                  --
├─Linear: 1-3                            [16, 8]                   104
├─ReLU: 1-4                              [16, 8]                   --
├─Linear: 1-5                            [16, 3]                   27
=============================================================================
Total params: 851
Trainable params: 851
Non-trainable params: 0
Total mult-adds (M): 3.23


GPU V100:
Train for epochs= 12  device= cuda X torch.Size([3200, 280, 1])
M: epoch=0 LR=0.001 loss=1.03, elaT=1.9 sec
M: epoch=1 LR=0.001 loss=0.706, elaT=3.7 sec
M: epoch=2 LR=0.001 loss=0.65, elaT=5.8 sec
M: epoch=3 LR=0.001 loss=0.531, elaT=7.6 sec
M: epoch=4 LR=0.001 loss=0.353, elaT=9.6 sec
M: epoch=5 LR=0.001 loss=0.165, elaT=11.7 sec
M: epoch=6 LR=0.001 loss=0.0671, elaT=13.5 sec
M: epoch=7 LR=0.001 loss=0.038, elaT=15.3 sec
M: epoch=8 LR=0.001 loss=0.0218, elaT=17.4 sec
M: epoch=9 LR=0.001 loss=0.0127, elaT=19.2 sec
M: epoch=10 LR=0.001 loss=0.011, elaT=21.1 sec
M: epoch=11 LR=0.001 loss=0.00696, elaT=22.9 sec


CPU:
TRAIN for epochs= 12  device= cpu X torch.Size([3200, 280, 1])
M: epoch=0 LR=0.001 loss=1.01, elaT=23.9 sec
M: epoch=1 LR=0.001 loss=0.625, elaT=48.1 sec
M: epoch=2 LR=0.001 loss=0.458, elaT=72.3 sec
M: epoch=3 LR=0.001 loss=0.227, elaT=96.6 sec
M: epoch=4 LR=0.001 loss=0.113, elaT=120.6 sec
M: epoch=5 LR=0.001 loss=0.0714, elaT=139.1 sec
M: epoch=6 LR=0.001 loss=0.0506, elaT=157.7 sec
M: epoch=7 LR=0.001 loss=0.0338, elaT=180.0 sec
M: epoch=8 LR=0.001 loss=0.0229, elaT=198.6 sec
M: epoch=9 LR=0.001 loss=0.0195, elaT=217.3 sec
M: epoch=10 LR=0.001 loss=0.0118, elaT=245.4 sec
M: epoch=11 LR=0.001 loss=0.00939, elaT=273.4 sec


'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time, sys
import copy

startT0 = time.time()
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
# BAD from torchsummary import summary
from torchinfo import summary #  pip install --user torch-summary
from torch.autograd import Variable  #can be differentiated

print('imported PyTorch ver:',torch.__version__,' elaT=%.1f sec,'%(time.time() - startT0))


#-------------------
#-------------------
#-------------------
class JanModel2(nn.Module):
#...!...!..................
    def __init__(self, input_size, output_size, hidden_size,num_layers,fc_dim):
        super(JanModel2, self).__init__()

        self.num_layers = num_layers #number of layers
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(input_size=input_size, hidden_size=hidden_size,
                            num_layers=num_layers, batch_first=True)
        self.fc1 = nn.Linear(hidden_size,fc_dim )
        self.fc2 = nn.Linear(fc_dim, output_size)
        self.relu = nn.ReLU()

    def forward(self, x): 
        #x=shape (BS,seq_len,inp_chan)
        #print('J: in',x.shape)
        bs=x.size(0)
        h_0 = Variable(torch.zeros(self.num_layers, bs, self.hidden_size)).to(device) #hidden state
        c_0 = Variable(torch.zeros(self.num_layers, bs, self.hidden_size)).to(device) #internal state
       # Propagate input through LSTM
        output, (hn, cn) = self.lstm(x, (h_0, c_0)) #lstm with input, hidden, and internal state
        hn = hn.view(-1, self.hidden_size) #reshaping the data for Dense layer next
        #print('J: hn',hn.shape,h_0.shape)
        out = self.relu(hn)
        out = self.fc1(out) #first Dense
        out = self.relu(out) #relu
        out = self.fc2(out) #Final Output
        return out

#-------------------
#-------------------
#-------------------
class JanDataset(Dataset): # from input np-arrays
    def __init__(self, npX, npY):
        self.X=npX
        self.Y=npY
        self.num_eve=Y.shape[0]

    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.X[idx]
        y=self.Y[idx]
        sample =(x,y)
        return sample

    
# UTIL FUNCTIONS

#...!...!..................
def model_train( model, device,train_loader, criterion, optimizer):
    model.train()
    train_loss=0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        lossOp =  criterion(output, target)
        lossOp.backward()
        optimizer.step()
        train_loss +=lossOp.item()
    train_loss /= len(train_loader)
    return train_loss


#...!...!..................
def model_infer( model, device, test_loader,loss_func):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            lossOp=loss_func(output, target)
            #print('qq',lossOp,len(test_loader.dataset),len(test_loader)); ok55
            test_loss += lossOp.item()
    test_loss /= len(test_loader)
    return test_loss,data,target.cpu(), output.cpu()


#...!...!..................
def create_waveform_dataset(num_train,seq_len,inp_chan,out_chan):
    ''' The input data: 
    X: chan=0: f(t): sine-wave snippet with random phase0 & ampl
    X: chan>0: white noise
    Y: f(tend+phi0), 2*f(tend+phi1), -3*f(tend+phi2)
    '''
    assert inp_chan==1 # for now
    assert out_chan in [1,2,3]
    startT = time.time()
    seq2=2*seq_len
    t=np.linspace(0,4*np.pi,num=seq2)
    t0=np.random.uniform(2*np.pi,size=num_train)
    A=np.random.uniform(0.1,1.2,size=num_train)

    X=np.zeros((num_train,seq2,inp_chan),dtype=np.float32)
    Y=np.zeros((num_train,out_dim),dtype=np.float32)
    for i in range(num_train):
        X[i,:,0]=np.sin(t+t0[i])*A[i]
    
    Y[:,0]=X[:,int(seq_len*1.1),0]
    if out_chan>1: Y[:,1]=2*X[:,int(seq_len*1.2),0]
    if out_chan>2: Y[:,2]=-3*X[:,int(seq_len*1.3),0]

    # clip not needed values of X
    X=X[:,:seq_len]
    print('DS: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))
    print('DS: done Y shape',Y.shape,Y.dtype,Y[0])

    return torch.from_numpy(X), torch.from_numpy(Y)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':

    device='cpu'
    device='cuda'

    if device=='cuda':
        assert torch.cuda.is_available() 

    if device=='cpu':  # it matters for summary(.)
        assert not torch.cuda.is_available() 

    seq_len=280
    inp_chan=1
    num_layers = 1 #number of stacked lstm layers
    fc_dim=8
    out_dim=3

    epochs=12
    steps=200;  batch_size=16  # 2 sec/epoch on GPU V100
    #steps=5*250;  batch_size=256*4  #  17 sec/epoch  on GPU V100
    #steps=5*250;  batch_size=256*4*4  # 42 sec/epoch  on GPU V100
    
    num_eve=steps*batch_size
    num_workers=4 # to load the data in parallel ??

    # - - - -  DATA  PERP - - - - - 
    print('\nM: generate data and train, num_eve=',num_eve)
    X,Y=create_waveform_dataset(num_eve,seq_len,inp_chan,out_dim)
    print('\nCreate pth-Dataset instance')
    trainDst=JanDataset(X,Y)

    # my input shape: BS=4, seq-len=280, inp_size=1

    model = JanModel2(input_size=1, output_size=out_dim, hidden_size=12,num_layers=num_layers,fc_dim=fc_dim)
    model = model.to(device)

    # Initialize optimizer
    learning_rate = 1e-3
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    # define loss function
    criterion = nn.MSELoss().to(device)# Mean Squared Loss

    print('\nCreate pt-DataLoader instance & test it')
    trainLdr = torch.utils.data.DataLoader(trainDst, batch_size=batch_size, shuffle=True, num_workers=num_workers)

    if 0:
        print('\n print one batch of training data ')
        xx, yy = next(iter(trainLdr))
        #x=shape (BS,seq_len,inp_chan)
        print('X:',xx.shape,'Y:',yy.shape)
        print('Y[0]',yy[0])
        print('X[0]',xx[0].T)
    # - - - -  DATA  READY - - - - - 

    print('\n= = = = Prepare for the treaining = = =\n')
    print('\n\nM: torchsummary.summary(model):'); print(model)

    if 1:
        inp_size=(batch_size,seq_len,inp_chan) 
        summary(model,inp_size) 


    print('optimizer=',optimizer)
    print('Train for %d epochs BS=%d'%(epochs,batch_size),' device=',device,'X',X.shape)

    startT=time.time()
    for epoch in range(epochs):
        loss=model_train( model, device, trainLdr, criterion, optimizer)
        currLR=optimizer.param_groups[0]['lr']
        #scheduler.step(loss)  # Decay Learning Rate
        if epoch%1==0: print('M: epoch=%d LR=%.3g loss=%.3g, elaT=%.1f sec'%(epoch,currLR ,loss,time.time()-startT))

    print('\nInfer for train data:')

    loss,X1,Y1,Z1=model_infer( model, device, trainLdr,  criterion)
    print('infer : Average loss: %.4f  events=%d '% (loss,  len(trainLdr.dataset)))
    print('sample predictions:')
    for i in range(batch_size):
        print('y=',Y1[i].numpy(),'   diff=',(Y1[i]-Z1[i]).numpy()) #,'   z=',Z1[i].numpy()
