#!/usr/bin/env python3

'''
This example runs on CPU, 1 GPU, many GPUs
On CoriGpu or on Summit - see hardcoded swithc: facility

 example of simple FC +regression
 a noisy Y is added to X so training  converges 
 uses DataLoader (aka generator) for input

Distributed training on multiple GPUs using srun
GPU are saturaged at 10-50% IF steps=320

Use IP-based synchronization of those 2 env variables are set
   export MASTER_ADDR=10.101.105.103  <-- must be the head node
   export MASTER_PORT=8888
or it will do file-based synchronization as an alternative

Use  file-based synchornization following
The official NERSC tutorial is using nccl & csrtach based synchronization  
https://github.com/NERSC/pytorch-examples/blob/master/utils/distributed.py
def init_workers_nccl_file()

Tip from Steve F.: add this line to see NCCL debugging  
export NCCL_DEBUG=info

Testing:
Interactive 2x2 GPU:
   salloc -N2  -C gpu  -c 10  --gres=gpu:2 --ntasks-per-node=2  -t4:00:00 

   line=`ifconfig |grep 'ib1:' -A1 |tail -n1` # cori-gpu node
   # line=`ifconfig |grep 'ib8:' -A1 |tail -n1`  # cori-dgx
   #  line=`ifconfig |grep ib0: -A1|tail -n1`  # Summit
   headIp=`echo $line | cut -f2 -d\ `
   export MASTER_ADDR=$headIp
   export MASTER_PORT=8888
   echo headIP=$headIp

Limitation:  -n >=-N

Monitor GPU load:
srun -n 4   bash -c ' if [ ${SLURM_LOCALID} -eq 0 ] ; then   echo locRank0 on `hostname`;  nvidia-smi -l 10 >&out/L.smi_`hostname` ; fi  &  python -u   ./train_CNN-distr-hpar_gpu.py '


   run on 4 GPUs:
      srun -n4 -l python -u ./train_CNN-distr-hpar_gpu.py 


2 full nodes (16 GPUs)
  salloc -N 2  -c 10  --gres=gpu:8  --ntasks-per-node=8  --exclusive -C gpu  -t 3:59:00 


To run on CPU , whole node
   export MASTER_ADDR=127.0.0.1 
   export MASTER_PORT=8888
   export SLURM_PROCID=0 ; export SLURM_NTASKS=1 ;export SLURM_LOCALID=0 (if on login node)
  ./train_FC-distr_gpu.py

srun -n4 -l shifter python3 -u ./train_FC-distr_gpu.py

code change: device='cpu' , main(), line 228
change port to 8889 if you see RuntimeError: Address already in use

Method: lunches 1 python taks/GPU using srun
Scaling:  globalBS=const, global samples=const, LR=const
local BS=globalBS/world_size, localSamples=global samples/world_size

Example output for cori-GPU

1 GPU:
0: local BS=16384 world_size=1, LR=0.02,  local samples=262144
0: epoch=5  loss=0.00998, elapsed T=7.6 sec

4 GPUs:
0: local BS=4096 world_size=4, LR=0.02,  local samples=65536
0: epoch=4  loss=0.0134, elapsed T=3.1 sec

See also:
https://docs.google.com/spreadsheets/d/1SVcuzJ15K7SzwOEDsGhx8QJMlApG12ToxxOCPLdJz0o/edit?usp=sharing

For Summit vs. CoriGpu see
https://docs.google.com/document/d/1QAUU_jg6DPzrnLcqWArbyFO2Z4m6UYJkJEQcvrQY3Ck/edit?usp=sharing


Cori-login node, uses ~5 CPUs ????  data size was different, I think
local BS=65536 world_size=1, LR=0.02,  local samples=1048576
M: epoch=0  loss=0.279, elapsed T=23.7 sec
M: epoch=4  loss=0.0105, elapsed T=111.1 sec

1 KNL node  ???
local BS=65536 world_size=1, LR=0.02,  local samples=1048576
BAR 0
M: epoch=0  loss=0.279, elapsed T=110.4 sec
M: epoch=1  loss=0.105, elapsed T=219.8 sec


***** Crusher
 salloc -A AST153_crusher -p batch  -t 2:30:00 --x11 --ntasks-per-node=8 --gpus-per-task=1 --cpus-per-task=8 --exclusive  -N 2

export MASTER_ADDR=`hostname` 
export MASTER_PORT=8888
export NCCL_DEBUG=info
SIF=/gpfs/alpine/ast153/scratch/balewski/crusher_amd64/rocm4.5-crusher-torch.v4.sif 

 srun -n2 -l singularity exec  $SIF python3 -u ./train_FC-distr_gpu.py

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time, sys
import copy

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torchsummary import summary

# distr-train
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
import socket  # for hostname

#...!...!..................
def try_barrier():
    """Attempt a barrier but ignore any exceptions"""
    print('BAR %d'%rank)

    try:
        dist.barrier()
    except:
        pass

#-------------------
#-------------------
#-------------------
class JanModel(nn.Module):
#...!...!..................
    def __init__(self,inp_dim,fc_dim,out_dim):
        super(JanModel, self).__init__()
        self.fc1 = nn.Linear(inp_dim,fc_dim)
        self.fc2 = nn.Linear(fc_dim,fc_dim)
        self.fc3 = nn.Linear(fc_dim,out_dim)
        
    def forward(self, x):
        x = F.relu(self.fc1(x)) 
        x = F.relu(self.fc2(x))
        x = self.fc3(x)         
        x=torch.tanh(x)         
        return x

#-------------------
#-------------------
#-------------------
class JanDataset(Dataset): # input np-arrays
#...!...!..................
    def __init__(self, npX, npY):
        self.X=npX
        self.Y=npY
        self.num_eve=Y.shape[0]

    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.X[idx]
        y=self.Y[idx]
        sample =(x,y)
        return sample

    
# UTIL FUNCTIONS
#...!...!..................
def model_train( model, device,train_loader, loss_func,optimizer):
    model.train()
    train_loss=0
    for batch_idx, (data, label) in enumerate(train_loader):
        if device=='cuda':
            data, label = data.to(device), label.to(device)
        # Forward pass
        output = model(data)
        lossOp = loss_func(output, label)

        # Backward and optimize
        #optimizer.zero_grad() # this is expensive
        for x in model.parameters(): x.grad=None  # it makes no difference in this example
               
        lossOp.backward()
        optimizer.step()
        train_loss +=lossOp.item()
    train_loss /= len(train_loader)
    return train_loss

#...!...!..................
def model_infer( model, device, test_loader,loss_func):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for data, label in test_loader:
            if device=='cuda':
                data, label = data.to(device), label.to(device)
            output = model(data)
            lossOp=loss_func(output, label)
            test_loss += lossOp.item()
    test_loss /= len(test_loader)
    return test_loss,data,label, output


#...!...!..................
def create_dataset(num_train,inp_dim,out_dim,verb):
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-1,1, size=(num_train,out_dim)).astype('float32')

    X=np.random.uniform(-0.8,0.8, size=(num_train,inp_dim)).astype('float32')
    if verb: 
        print('CDS: done Y shape',Y.shape,Y.dtype,Y[0])
        print('CDS: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))

    # inject Y to X so training can converge
    X[:,:out_dim]=Y*0.8+np.random.normal(loc=0, scale=0.05, size=(num_train,out_dim)).astype('float32')

    return X,Y


#=================================
#=================================
#  M A I N 
#=================================
#=================================

#device='cpu'  # remember to:  export MASTER_ADDR=127.0.0.1 ; export MASTER_PORT=8888

# hardcoded setup:
device='cuda'
facility=['corigpu','summit','crusher'][2]

if device=='cuda':
    if facility=='corigpu'  :
        rank = int(os.environ['SLURM_PROCID'])
        world_size = int(os.environ['SLURM_NTASKS'])
        locRank=int(os.environ['SLURM_LOCALID'])
    if facility=='summit':
        rank = int(os.environ['PMIX_RANK'])
        world_size = int(os.environ['WORLD_SIZE'])
        locRank=rank%6
    if facility=='crusher' :
        rank = int(os.environ['SLURM_PROCID'])
        world_size = int(os.environ['SLURM_NTASKS'])
        locRank=0
else:
    rank=0;  world_size = 1; locRank=0 

host=socket.gethostname()
masterIP=os.getenv('MASTER_ADDR')
verb=rank==0
print('M:myRank=',rank,'world_size =',world_size,'verb=',verb,host,'locRank=',locRank,'masterIP=',masterIP )

masterPort=os.getenv('MASTER_PORT')
if verb: print('use masterIP',masterIP,masterPort)
assert masterPort!=None

if verb:
    print('imported PyTorch ver:',torch.__version__)

#Limitation:  -n >=-N
dist.init_process_group(backend='nccl', init_method='env://', world_size=world_size, rank=rank)
print("M:after dist.init_process_group, globRank=%d of %d, localRank=%d"%(rank,world_size,locRank))
assert dist.is_initialized()
      
      
inp_dim=280*2  # be aware of Cpu RAM limit/GPU
fc_dim=20
out_dim=15
epochs=10
steps=16*20   # note: use 16 steps for strong scaling & 320 steps for weak scaling

batch_size=16*1024//world_size  # local batch size

num_eve=steps*batch_size
learning_rate = 0.02

num_cpus=5 # to load the data in parallel , -c10 locks 5 phys cores

# Initialize model 
torch.manual_seed(0)
model = JanModel(inp_dim,fc_dim,out_dim)
if device=='cuda':
    torch.cuda.set_device(locRank)
    model.cuda(locRank)

# define loss function
loss_fn = nn.MSELoss().cuda(locRank)

# Initialize optimizer
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# Wrap the model
# This reproduces the model onto the GPU for the process.
if device=='cuda':
    model = nn.parallel.DistributedDataParallel(model, device_ids=[locRank])
    
# - - - -  DATA  PERP - - - - -
if verb:  print('\nM: generate data and train, local num_eve=',num_eve)
X,Y=create_dataset(num_eve,inp_dim,out_dim,verb)

if verb: print('\nCreate torch-Dataset instance')
trainDst=JanDataset(X,Y)

if verb: print('\nCreate torch-DataLoader instance & test it, num_cpus=',num_cpus)
trainLdr = DataLoader(trainDst, batch_size=batch_size, shuffle=True, num_workers=num_cpus,pin_memory=True)
# - - - -  DATA  READY - - - - - 
# Note, intentionally I do not use torch.utils.data.distributed.DistributedSampler(..)
# because I want to controll manually what data will be sent to which GPU - here data are generated on CPU matched to GPU

if verb:
    print('\n print one batch of training data ')
    xx, yy = next(iter(trainLdr))
    print('test_dataLoader: X:',xx.shape,'Y:',yy.shape)
    print('Y[:,]',yy[:,0])

    print('\n= = = = Prepare for the treaining = = =\n')
    print('\n\nM: torchsummary.summary(model):'); print(model)

inp_size=(inp_dim,) # input_size=(channels, H, W)) if CNN is first
if device=='cuda':
    model=model.to(device) # re-cast model on device, data will be cast later
    model = DDP(model, device_ids=[locRank],output_device=[locRank])

if verb:
    #summary(model,inp_size)    
    print('optimizer=',optimizer)
    print('Train for epochs=%d, local BS=%d world_size=%d, LR=%.3g,  local samples=%d'%(epochs,batch_size,world_size,learning_rate,num_eve) )
    startT=time.time()

if device=='cuda':  try_barrier()

for epoch in range(1,epochs+1):
    loss=model_train( model, device, trainLdr, loss_fn,optimizer)
    if epoch%1==0 and verb: print('M: epoch=%d  loss=%.3g, elapsed T=%.1f sec'%(epoch ,loss,time.time()-startT))

dist.barrier()

if verb==0: exit(0)
print('M:done training rank=%d of %d'%(rank,world_size))    
print('\nM:Infer for train data:')

loss,X1,Y1,Z1=model_infer( model, device, trainLdr,loss_fn)
print('infer : Average loss: %.4f  events=%d '% (loss,  len(trainLdr.dataset)))
X1=X1.cpu()
Y1=Y1.cpu()
Z1=Z1.cpu()
print('sample predictions:')
for i in range(batch_size):
    print(i,'loss=',loss)
    #print('y=',Y1[i].numpy(),'   z=',Z1[i].numpy(),'   diff=',(Y1[i]-Z1[i]).numpy())
    break
print('M:done inference rank=',rank)
