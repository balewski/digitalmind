#!/usr/bin/env python
'''
 python -c 'import torch; print(torch.__version__)'
 python -c 'import tensorboard; print(tensorboard.__version__)'


 example tensor board
To display TensorBoard on your laptop do

From Graphcore:
ssh  -i ~/.ssh/azure.pem -A  -Y -L localhost:9998:localhost:9998 balewskij@lr66-poplar3.usclt-pod1.graphcloud.ai 
source ~/digitalMind/graphcore/mySetup.source 

cd digitalMind/graphcore/torch/neuronInverter_dist_iofree/

From Cori-Gpu
ssh -A -Y -L localhost:9997:localhost:9997  cori.nersc.gov
source ~/digitalMind/torch/examples-class/superRes/mySetup.source
cd ~/digitalMind/torch/examples-solo

tensorboard --port 9600 --samples_per_plugin images=100  --logdir=out/


Now open http://0.0.0.0:9998/ in your browser on your laptop

'''

import sys,os

import argparse
from pprint import pprint
import numpy as np

import torch
import torchvision
from torch.utils.tensorboard import SummaryWriter
from torchvision import datasets, transforms

#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-o","--outPath", default='out/',help='TB output')
    args = parser.parse_args()    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

 
#=================================
#=================================
#  M A I N 
#=================================
#=================================

if __name__ == '__main__':
    
    args=get_parser()

    # Writer will output to ./runs/ directory by default
    TBSwriter = SummaryWriter(args.outPath)

    #... simple implementation of resnet
    transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))])
    trainset = datasets.MNIST('mnist_train', train=True, download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=64, shuffle=True)
    model = torchvision.models.resnet50(False)
    # Have ResNet model take in grayscale rather than RGB
    model.conv1 = torch.nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
    dataIter=iter(trainloader)

    # .... post info on TB
    
    images, labels = next(dataIter)
    TBSwriter.add_graph(model, images)


    for epoch in range(20):
        # ... post some images per epoch
        images, labels = next(dataIter)        
        grid = torchvision.utils.make_grid(images)
        TBSwriter.add_image('4-images', grid, epoch)

    for step in range(3,200,5):
        # ... post pair of losses per step
        loss1= 10/step
        loss2=10/np.sqrt(step)
        loss3=10/np.log(step)
        rec={'train':loss1, 'val':loss2}
        TBSwriter.add_scalar('2/step loss3',loss3,global_step=step)
        TBSwriter.add_scalars('3/step multi-loss',rec,global_step=step)

    r = 5
    for i in range(100):
        TBSwriter.add_scalars('run_14h', {'xsinx':i*np.sin(i/r),
                                          'xcosx':i*np.cos(i/r),
                                          'tanx': np.tan(i/r)}, i)

    for i in range(10):
        x = np.random.random(1000)
        TBSwriter.add_histogram('distribution centers', x + i, i)

        
    for i in range(5):
        TBSwriter.add_hparams({'lr': 0.1*i, 'bsize': i},
                      {'hparam/accuracy': 10*i, 'hparam/loss': 10*i})
    print("M:done, see ",args.outPath)
