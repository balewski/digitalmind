#!/usr/bin/env python3

'''
This example runs on CPU, 1 GPU, many GPUs

 example of complex CNN1D+FC+regression  defined in hpar-yaml 
 a noisy Y is added to X so training  converges 
 uses DataLoader (aka generator) for input
 uses  EarlyStopping &  Decay Learning Rate

Distributed training on multiple GPUs using srun
GPUs are  loaded at 90%

Use IP-based synchronization of those two env variables are set
   export MASTER_ADDR=10.101.105.103  <-- must be the head node IP
   export MASTER_PORT=8888
or it will do file-based synchronization as an alternative


Tip from Steve F.: add this line to see NCCL debugging  
export NCCL_DEBUG=info

Testing:
Interactive 2x2 GPU:
   salloc -N2  -C gpu  -c 10  --gres=gpu:2 --ntasks-per-node=2  -t4:00:00 

   line=`ifconfig |grep 'ib1:' -A1 |tail -n1` # cori-gpu node
   # line=`ifconfig |grep 'ib8:' -A1 |tail -n1`  # cori-dgx
   headIp=`echo $line | cut -f2 -d\ `
   export MASTER_ADDR=$headIp
   export MASTER_PORT=8888
   echo headIP=$headIp

Limitation:  -n >=-N

Monitor GPU load:
srun -n 4   bash -c ' if [ ${SLURM_LOCALID} -eq 0 ] ; then   echo locRank0 on `hostname`;  nvidia-smi -l 10 >&out/L.smi_`hostname` ; fi  &  python -u   ./train_CNN-distr-hpar_gpu.py '


   run on 4 GPUs:
      srun -n4 -l python -u ./train_CNN-distr_gpu.py  

2 full nodes (16 GPUs)
  salloc -N 2  -c 10  --gres=gpu:8  --ntasks-per-node=8  --exclusive -C gpu  -t 3:59:00 


To run on CPU , whole node
   export MASTER_ADDR=127.0.0.1 
   export MASTER_PORT=8888
   export SLURM_PROCID=0 ; export SLURM_NTASKS=1 ;export SLURM_LOCALID=0 (if on login node)
  ./train_CNN-distr-hpar_gpu.py

code change: device='cpu' , main(), line 228
change port to 8889 if you see RuntimeError: Address already in use

Method: lunches 1 python taks/GPU using srun
Scaling:  globalBS=const, global samples=const, LR=const
local BS=globalBS/world_size, localSamples=global samples/world_size


Example output for cori-GPU

See: https://docs.google.com/spreadsheets/d/1SVcuzJ15K7SzwOEDsGhx8QJMlApG12ToxxOCPLdJz0o/edit?usp=sharing 

1 GPU:
0: M:myRank= 0 world_size = 1 verb= True cgpu15 locRank= 0 device= cuda
Train for epochs=100,  world_size=1, local samples=491520 ,  localBS= 1536
0: M:Early stopping, patience=20, best loss=0.2325
0: M: epoch=57, LR=1.03e-06, end-loss=0.231, elaT=896 sec


6 GPUs:
0: Train for epochs=100,  world_size=6, local samples=81920, localBS= 256
0: M:Early stopping, patience=20, best loss=0.2609
0: M: epoch=54, LR=1.03e-06, end-loss=0.259, elaT=233 sec


'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time, sys
import copy

#startT0 = time.time()
import numpy as np
import ruamel.yaml  as yaml
from pprint import pprint
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torchsummary import summary

# distr-train
import torch.distributed as dist
import socket  # for hostname

#...!...!..................
def try_barrier():
    """Attempt a barrier but ignore any exceptions"""
    print('BAR %d'%rank)

    try:
        dist.barrier()
    except:
        pass

#-------------------
#-------------------
#-------------------
class EarlyStopping:
    """
    Early stops the training if validation loss doesn't improve after a given patience.
    Author: Bjarte Mehus Sunde
    source: https://github.com/Bjarten/early-stopping-pytorch/blob/master/pytorchtools.py
    modiffied by Jan Balewski
    """
    def __init__(self, patience=7, verbose=False, delta=0, warmup=5, chkptFile=None):
        """
        Args:
            patience (int): How long to wait after last time validation loss improved.
            verbose (bool): If True, prints a message for each validation loss improvement. 
            delta (float): Minimum change in the monitored quantity to qualify as an improvement.
            warmup (int): initial sleep period to avoid frequent chkpt
            chktpFile (string): full chkpt file name
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf
        self.delta = delta
        self.warmup=warmup
        self.chkptFile=chkptFile
        self.epochCnt=0
        if self.verbose: print('EarlyStopping cstr, chkptFile=',chkptFile,', patience=%d, delta=%.3g'%(patience,delta))

    def __call__(self, val_loss, model):
        score = val_loss
        self.epochCnt+=1
        if self.best_score is None:  # jan- drop it later
            self.best_score = score
            self.save_checkpoint(val_loss, model)
        elif score > self.best_score - self.delta:  # no improvement
            self.counter += 1
            #print('  EarlyStopping.call: patience=%d,  worse loss, old:  %.3g  --> new %.3g  counter=%d '%(self.counter,self.val_loss_min,val_loss,self.counter))
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
            self.counter = 0
 
        return self.early_stop

    def save_checkpoint(self, val_loss, model):
        '''Saves model when validation loss decrease.'''
        if self.verbose:
            print('  EarlyStopping.SCP: patience=%d,  val-loss decreased:  %.4g  -->  %.4g '%(self.counter,self.val_loss_min,val_loss))
        if self.epochCnt > self.warmup and self.chkptFile!=None:
            if self.verbose: print('  EarlyStopping: saving state ...',self.chkptFile)
            torch.save(model.state_dict(), self.chkptFile)
        self.val_loss_min = val_loss



#-------------------
#-------------------
#-------------------
class CNNandFC_Model(nn.Module):
    ''' Automatically   compute the size of the output of CNN+Pool portion of the model,  needed as input to the first FC layer 
    '''
#...!...!..................
    def __init__(self,hpar,verb=0):
        super(CNNandFC_Model, self).__init__()
        if verb: print('CNNandFC_Model hpar=',hpar)
        #
        self.inp_shape=tuple(hpar['inputShape']) # 2DCNN needs feature dimension speciffied
        inp_time,inp_chan=self.inp_shape
        
        self.verb=verb
        if verb: print('CNNandFC_Model inp_shape=',self.inp_shape,', verb=%d'%(self.verb))
        
        self.cnn_block = nn.ModuleList()
        self.fc_block  = nn.ModuleList()
        cnnD=hpar['cnn_block']
        fcD=hpar['fc_block']
        
        # .....  CNN layers
        for out_chan in cnnD['filter']:
            self.cnn_block.append( nn.Conv1d(inp_chan, out_chan, cnnD['kernel'],cnnD['strides'] ))
            self.cnn_block.append( nn.MaxPool1d(cnnD['pool_size']))
            self.cnn_block.append( nn.ReLU())
            inp_chan=out_chan
        self.cnn_block.append( nn.Flatten() )

        #print('fff',
        # compute FC_block input size
        with torch.no_grad():
            # process 2 fake examples through the CNN portion of model
            #x1=torch.tensor(np.zeros((2,)+self.inp_shape), dtype=torch.float32)
            # must pust inp_chan as the 1st dim of a frame
            inp_time,inp_chan=self.inp_shape
            x1=torch.tensor(np.zeros((2,inp_chan,inp_time)), dtype=torch.float32)
            if self.verb>1: print('J:x1 ',x1.shape)
            y1=self.forwardCnnOnly(x1)
            self.flat_dim=np.prod(y1.shape[1:]) 
            if verb>1: print('myNet flat_dim=',self.flat_dim)

        # .... add FC  layers
        inp_dim=self.flat_dim                
        if hpar['batch_norm_flat']: # With Learnable Parameters
            self.fc_block.append( nn.BatchNorm1d(inp_dim) )            
        for i,dim in enumerate(fcD['units']):
            self.fc_block.append( nn.Linear(inp_dim,dim))
            inp_dim=dim
            self.fc_block.append( nn.SELU())
            if fcD['dropFrac']>0 : self.fc_block.append( nn.Dropout(p= fcD['dropFrac']))

        #.... the last FC layer will have different activation and no Dropout
        self.fc_block.append(nn.Linear(inp_dim,hpar['outputSize']))

        # add last layer with proper activation
        outAmpl=1.2
        self.fc_block.append(nn.Tanh())
        self.fc_block.append(LambdaLayer(lambda x: x*outAmpl))

#...!...!..................
    def forwardCnnOnly(self, x):
        if self.verb>1: print('J0: inp2cnn',x.shape)
        
        for i,lyr in enumerate(self.cnn_block):
            if self.verb>1: print('Jcnn: ',i,x.shape,lyr)
            x=lyr(x)

        return x
        
#...!...!..................
    def forward(self, x):
        if self.verb>1: print('JF: inF',x.shape,'numLayers CNN=',len(self.cnn_block),'FC=',len(self.fc_block))
        x=self.forwardCnnOnly(x)
        #?x = x.view(-1,self.flat_dim)
        if self.verb>1: print('JF1:',x.shape)
        for i,lyr in enumerate(self.fc_block):
            x=lyr(x)
            if self.verb>1: print('Jfc: ',i,x.shape)
        if self.verb>1: print('JF: y',x.shape)
        return x

#...!...!..................
    def summary(self):
        numLayer=sum(1 for p in self.parameters())
        numParams=sum(p.numel() for p in self.parameters())
        return {'modelWeightCnt':numParams,'trainedLayerCnt':numLayer,'modelClass':self.__class__.__name__}


#-------------------
#-------------------
#-------------------
class LambdaLayer(nn.Module):
    def __init__(self, lambd):
        super(LambdaLayer, self).__init__()
        self.lambd = lambd
    def forward(self, x):
        return self.lambd(x)




#-------------------
#-------------------
#-------------------
class JanDataset(Dataset): # input np-arrays
#...!...!..................
    def __init__(self, npX, npY):
        self.X=npX
        self.Y=npY
        self.num_eve=Y.shape[0]

    def __len__(self):
        return self.num_eve

    def __getitem__(self, idx):
        assert idx>=0
        assert idx< self.num_eve
        x=self.X[idx]
        y=self.Y[idx]
        sample =(x,y)
        return sample

    
# UTIL FUNCTIONS
#...!...!..................
def model_train( model, device,train_loader, loss_func,optimizer):
    model.train()
    train_loss=0
    for batch_idx, (data, label) in enumerate(train_loader):
        if device=='cuda':
            data, label = data.to(device), label.to(device)
        # Forward pass
        output = model(data)
        lossOp = loss_func(output, label)

        # Backward and optimize
        #optimizer.zero_grad() # this is expensive
        for x in model.parameters(): x.grad=None  # it makes no difference in this example ?
       
        
        lossOp.backward()
        optimizer.step()
        train_loss +=lossOp.item()
    train_loss /= len(train_loader)
    return train_loss

#...!...!..................
def model_infer( model, device, test_loader,loss_func):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for data, label in test_loader:
            if device=='cuda':
                data, label = data.to(device), label.to(device)
            output = model(data)
            lossOp=loss_func(output, label)
            test_loss += lossOp.item()
    test_loss /= len(test_loader)
    return test_loss,data,label, output


#...!...!..................
def create_dataset(num_train,hpar,verb):
    # The input data and labels
    startT = time.time()
    inpShp=hparams['inputShape'] # data must be opened 1st
 
    out_dim=hpar['outputSize']
    Y=np.random.uniform(-1,1, size=(num_train,out_dim)).astype('float32')
    if verb: print('CDS: done Y shape',Y.shape,Y.dtype,Y[0])

    # channel must be 1st in Torch
    X=np.random.uniform(-0.8,0.8, size=(num_train,inpShp[1],inpShp[0])).astype('float32')
    if verb: print('CDS: done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))

    # inject Y to X so training can converge
    X[:,0,:out_dim]=Y*0.8+np.random.normal(loc=0, scale=0.05, size=(num_train,out_dim)).astype('float32')

    return X,Y


#=================================
#=================================
#  M A I N 
#=================================
#=================================

device='cpu'  # remember to:  export MASTER_ADDR=127.0.0.1 ; export MASTER_PORT=8888

device='cuda'

if device=='cuda':
    rank = int(os.environ['SLURM_PROCID'])
    world_size = int(os.environ['SLURM_NTASKS'])
    locRank=int(os.environ['SLURM_LOCALID'])
else:
    rank=0;  world_size = 1; locRank=0 

host=socket.gethostname()
verb=rank==0
print('M:myRank=',rank,'world_size =',world_size,'verb=',verb,host,'locRank=',locRank ,'device=',device)


masterIP=os.getenv('MASTER_ADDR')
sync_file='env://'
masterPort=os.getenv('MASTER_PORT')
if verb: print('use masterIP',masterIP,masterPort)
assert masterPort!=None
dist.init_process_group(backend='nccl', init_method=sync_file, world_size=world_size, rank=rank)
print("M:after dist.init_process_group, myRank=",rank)

if verb:
    print('M: imported PyTorch ver:',torch.__version__)


# load HPARs
hparF='hpar_cnn_e5bb4b43.yaml'
ymlFd = open(hparF, 'r')
hparams=yaml.load( ymlFd, Loader=yaml.CLoader)
ymlFd.close()
if verb:
    print('HPAR defined in',hparF)
    pprint(hparams)
prjName2=hparF.replace('.yaml','')

epochs=100
localBS=hparams['train_conf']['globalBS']//world_size  # local batch size
steps=16*20  # use 320
num_eve=steps*localBS

# Initialize model 
torch.manual_seed(rank)
model = CNNandFC_Model(hparams,verb=0)
# define loss function
loss_fn = nn.MSELoss().cuda(locRank)

if device=='cuda':
    torch.cuda.set_device(locRank)
    model.cuda(locRank)

if rank==0:
    # only printout code below
    print('loss=',loss_fn)
    inpShp=hparams['inputShape'] # data must be opened 1st
    
    inpShp=[inpShp[1],inpShp[0]] # channel must be 1st in Torch
    print('\n\nM: torchsummary.summary(model), inputShape=',inpShp)
    summary(model,tuple(inpShp), device=device)
    model.verb=0  #??

# Wrap the model
# This reproduces the model onto the GPU for the process.
if device=='cuda':
    model = nn.parallel.DistributedDataParallel(model, device_ids=[locRank])

trainD=hparams['train_conf']
num_cpus=trainD['numLocalCPU']
# Initialize optimizer
optName, initLR=trainD['optimizer']

if optName=='adam' :
    optimizer=torch.optim.Adam(model.parameters(),lr=initLR)
else:
    print('invalid Opt:',optName); exit(99)
if rank==0: print('opt:',optimizer)

lrSched=None
if 'ReduceLROnPlateau' in trainD:
    patx,rdelx,facx,coolx=trainD['ReduceLROnPlateau']
    lrSched=ReduceLROnPlateau(optimizer, mode='min', factor=facx, patience=patx, cooldown=coolx, verbose=verb,threshold =rdelx)
    if rank==0: print('enable ReduceLROnPlateau:',patx,rdelx,facx,coolx)
    # https://pytorch.org/docs/stable/optim.html#torch.optim.lr_scheduler.ReduceLROnPlateau
    
earlyStop=None
if 'EarlyStopping' in trainD:
    paty, adely, wary, chkpty= trainD['EarlyStopping']
    chkptFile=None
    if chkpty: chkptFile=prjName2+'.state_best.pt'
    earlyStop=EarlyStopping(patience=paty, verbose=rank==0,  delta=adely,warmup=wary, chkptFile=chkptFile)
    if rank==0: print('enable EarlyStopping:',paty, adely, wary, chkpty)
           

# - - - -  DATA  PERP - - - - -
if verb:  print('\nM: generate data and train, num_eve=',num_eve)
X,Y=create_dataset(num_eve,hparams,verb)

if verb: print('\nCreate torch-Dataset instance, localBS=',localBS)
trainDst=JanDataset(X,Y)

if verb: print('\nCreate torch-DataLoader instance & test it, num_cpus=',num_cpus)
trainLdr = DataLoader(trainDst, batch_size=localBS, shuffle=True, num_workers=num_cpus,pin_memory=True)
# - - - -  DATA  READY - - - - - 
# Note, intentionally I do not use torch.utils.data.distributed.DistributedSampler(..)
# because I want to manually control what portion of data will be sent to which GPU - here data are generated on CPU matched to GPU

if verb:
    print('\n print one batch of training data ')
    xx, yy = next(iter(trainLdr))
    print('test_dataLoader: X:',xx.shape,'Y:',yy.shape)
    print('Y[::10,]',yy[::10,0])

    print('\n= = = = Prepared for the treaining = = =\n')


#inp_size=(inp_dim,) # input_size=(channels, H, W)) if CNN is first
if device=='cuda':
    model=model.to(device) # re-cast model on device, data will be cast later

if verb:
    print('optimizer=',optimizer)
    print('Train for epochs=%d,  world_size=%d, local samples=%d trainConf:'%(epochs,world_size,num_eve),trainD )
    startT=time.time()

if device=='cuda':  try_barrier()

for epoch in range(epochs):
    loss=model_train( model, device, trainLdr, loss_fn,optimizer)
    
    #bestLoss=loss
    doMon= (epoch<4) or ( epoch%10==0)  or  (epoch==epochs-1)
    doMon *=verb
    currLR=optimizer.param_groups[0]['lr']
    #print('M:T',epoch,loss,earlyStop.best_score,earlyStop.early_stop,rank,type(loss))
    lossGlob=torch.tensor(loss).to(device)
    dist.all_reduce(lossGlob.detach()) # sum losses from all ranks in-place
    lossGlobAvr=float(lossGlob)/world_size
    #print('M:L',lossGlobAvr, loss,rank,epoch)
    
    if 'EarlyStopping' in trainD and  earlyStop(lossGlobAvr, model) :
        bestLoss=earlyStop.best_score
        doMon=True*verb
        if doMon: print('M:Early stopping, patience=%d, best loss=%.4g'%(earlyStop.counter,bestLoss))

    if doMon: print('M: epoch=%d, LR=%.3g, end-loss=%.4g, elaT=%d sec'%(epoch,currLR ,loss,time.time()-startT))
    if lrSched: lrSched.step(lossGlobAvr)  # Decay Learning Rate
    if  earlyStop.early_stop: break

    if epoch%10==0 and verb: print('M: epoch=%d  loss=%.4g, elapsed T=%.1f sec'%(epoch ,loss,time.time()-startT))
    

print('M:done training rank=',rank)
if verb==0: exit(0)
    
print('\nInfer for train data:')

loss,X1,Y1,Z1=model_infer( model, device, trainLdr,loss_fn)
print('infer : Average loss: %.4f  events=%d '% (loss,  len(trainLdr.dataset)))
X1=X1.cpu()
Y1=Y1.cpu()
Z1=Z1.cpu()
print('sample predictions:')
for i in range(localBS):
    print(i,'loss=',loss)
    #print('y=',Y1[i].numpy(),'   z=',Z1[i].numpy(),'   diff=',(Y1[i]-Z1[i]).numpy())
    break
print('M:done inference rank=',rank)
