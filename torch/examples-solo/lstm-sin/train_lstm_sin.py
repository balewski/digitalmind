#!/usr/bin/env python
'''
 code from : https://github.com/pytorch/examples/tree/master/time_sequence_prediction

M: data (100, 1000)
M: train torch.Size([97, 999]) torch.Size([97, 999])

STEP:  0
Model:
J: input torch.Size([97, 999])
J: h_t torch.Size([97, 51])
J: c_t torch.Size([97, 51])
J: h_t2 torch.Size([97, 51])
J: c_t2 torch.Size([97, 51])
J: outputs torch.Size([97, 999])

'''

import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt

class Sequence(nn.Module):
    def __init__(self):
        super(Sequence, self).__init__()
        self.lstm1 = nn.LSTMCell(1, 51)
        self.lstm2 = nn.LSTMCell(51, 51)
        self.linear = nn.Linear(51, 1)
        self.first=1
        print('J: Net cstr')

    def forward(self, input, future = 0):
        outputs = []
        #if  self.first: print('J: ',.shape)
        if  self.first: print('J: input',input.shape)
        h_t = torch.zeros(input.size(0), 51, dtype=torch.double)
        c_t = torch.zeros(input.size(0), 51, dtype=torch.double)
        h_t2 = torch.zeros(input.size(0), 51, dtype=torch.double)
        c_t2 = torch.zeros(input.size(0), 51, dtype=torch.double)
        if  self.first: print('J: h_t',h_t.shape)
        if  self.first: print('J: c_t',c_t.shape)
        if  self.first: print('J: h_t2',h_t2.shape)
        if  self.first: print('J: c_t2',c_t2.shape)

        for i, input_t in enumerate(input.chunk(input.size(1), dim=1)):
            h_t, c_t = self.lstm1(input_t, (h_t, c_t))
            h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
            output = self.linear(h_t2)
            outputs += [output]
        for i in range(future):# if we should predict the future
            h_t, c_t = self.lstm1(output, (h_t, c_t))
            h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
            output = self.linear(h_t2)
            outputs += [output]
        outputs = torch.stack(outputs, 1).squeeze(2)
        if  self.first: print('J: outputs',outputs.shape)
        self.first=0
        return outputs

#=================================
#=================================
#  M A I N
#=================================
#=================================

if __name__ == '__main__':
    #plt.ion()   # interactive mode
    
    # set random seed to 0
    np.random.seed(0)
    torch.manual_seed(0)
    outPath='out/'
    
    # load data and make training set
    data = torch.load('../data/sin_traindata.pt')
    print('M: data',data.shape)
    input = torch.from_numpy(data[3:, :-1])
    target = torch.from_numpy(data[3:, 1:])
    print('M: train',input.shape,target.shape)
    
    test_input = torch.from_numpy(data[:3, :-1])
    test_target = torch.from_numpy(data[:3, 1:])

    # build the model
    model = Sequence()
    model.double()
    criterion = nn.MSELoss()
    
    # use LBFGS as optimizer since we can load the whole data to train
    optimizer = optim.LBFGS(model.parameters(), lr=0.8)
    
    print('\nbegin to train...')
    for i in range(2):
        print('\nSTEP: ', i)
        
        def closure():
            optimizer.zero_grad()
            out = model(input)
            loss = criterion(out, target)
            print('loss:', loss.item())
            loss.backward()
            return loss
        
        optimizer.step(closure)
        
        print('\n begin to predict') 
        with torch.no_grad(): #  no need to track gradient here
            future = 1000
            pred = model(test_input, future=future)
            loss = criterion(pred[:, :-future], test_target)
            print('test loss:', loss.item())
            y = pred.detach().numpy()

        print('\n draw the result, step=',i)
        plt.figure(figsize=(10,8))
        plt.title('Predict future (Dashlines), step=%d'%i)#, fontsize=30)
        plt.xlabel('x')
        plt.ylabel('y')
        plt.xticks()
        plt.yticks()
        def draw(yi, color):
            plt.plot(np.arange(input.size(1)), yi[:input.size(1)], color, linewidth = 2.0)
            plt.plot(np.arange(input.size(1), input.size(1) + future), yi[input.size(1):], color + ':', linewidth = 2.0)
        draw(y[0], 'r')
        draw(y[1], 'g')
        draw(y[2], 'b')
        plt.savefig(outPath+'predict%d.pdf'%i)
        #plt.close()
        print('kill graph to proceed'); plt.show()

    print('end22')
