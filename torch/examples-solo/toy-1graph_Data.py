#!/usr/bin/env python
"""
example from 
https://pytorch-geometric.readthedocs.io/en/latest/notes/introduction.html

simple graph w/ 3 nodes and 2 edges
"""
import torch
from torch_geometric.data import Data

# this is bi-directional list of edges for 3 nodes, read it as columns
edge_index = torch.tensor([[0, 1, 1, 2],
                           [1, 0, 2, 1]], dtype=torch.long)
x = torch.tensor([[-1], [0], [1]], dtype=torch.float)

data = Data( x=x, edge_index=edge_index )
print('data',data)

# alternative (easier?) definition of adjeciency matrix
# Note, call contiguous on it before passing them to the data constructor

edge_index2 = torch.tensor([[0, 1],
                           [1, 0],
                           [1, 2],
                           [2, 1]], dtype=torch.long)
x2 = torch.tensor([[-1], [0], [1]], dtype=torch.float)

data2 = Data( x=x2, edge_index=edge_index2.t().contiguous() )
print('data2',data2)

# Although the graph has only two edges, we need to define four index tuples to account for both directions of a edge.

print('data.keys',data.keys)
print('data[x].shape',data['x'].shape)
print('M:data  features:',data.num_features,'edges:',data.num_edges,'data.num_node_features:',data.num_node_features,'\ndata.contains_isolated_nodes()=',data.contains_isolated_nodes(),'data.contains_self_loops()=',data.contains_self_loops(),'data.is_directed()=',data.is_directed())

