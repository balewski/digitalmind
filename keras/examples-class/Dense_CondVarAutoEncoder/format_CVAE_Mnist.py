#!/usr/bin/env python
"""  read raw input data
sanitize, randomize, pad/clip/1hot, 
write 9 tensors: (train,val,test) * (X,Y,Yhot) in hd5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_CVAE_Mnist import Plotter_CVAE_Mnist
from Deep_CVAE_Mnist import Deep_CVAE_Mnist

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Mnist data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("--project",
                        default='cvae_mnist',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")
 
    args = parser.parse_args()

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_CVAE_Mnist(args )

deep=Deep_CVAE_Mnist(args)
deep.split_raw_input(0.7)
deep.print_input('val',2)
deep.save_input_hdf5()
ppp.plot_digits(deep,'val',range(3),0)

ppp.display_all(args,'form')
