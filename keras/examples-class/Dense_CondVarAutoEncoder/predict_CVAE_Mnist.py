#!/usr/bin/env python
""" read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_CVAE_Mnist import Plotter_CVAE_Mnist
from Deep_CVAE_Mnist import Deep_CVAE_Mnist

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Mnist data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")
 
    parser.add_argument("--project",
                        default='cvae_mnist',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    parser.add_argument("-y", type=int, default=5,
                        help="label to be single out")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_CVAE_Mnist(args )
deep=Deep_CVAE_Mnist(args)

deep.load_input_hdf5(['test'])
deep.input_filter_label('test') 
#dee.print_input('test',1)
ncol=3
ppp.plot_digits(deep,'test',range(ncol*3),0)

deep.load_model_full() 
ppp.plot_latent_z(deep,'test')
ppp.plot_decoded_z(deep)
ppp.display_all(args,'predict')




badL,predL=deep.model_predict('test',400) 
if len(badL)>=ncol:
    ppp.plot_digits(deep,'test',badL[:ncol],1,auxL=predL,ncol=ncol)
if len(badL)>=2*ncol:
    ppp.plot_digits(deep,'test',badL[ncol:2*ncol],2,auxL=predL[ncol:2*ncol],ncol=ncol)
ppp.display_all(args,'predict')



