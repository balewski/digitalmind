import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()

from keras.datasets import mnist
from keras import utils as np_utils

from keras import backend as K
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout, concatenate
from keras.models import Model,  load_model

import numpy as np
import h5py

#from sklearn.metrics import confusion_matrix

print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
def NegLogLikeh(y_true, y_pred):
    """ Negative log likelihood (Bernoulli). """
    # keras.losses.binary_crossentropy gives the mean
    # over the last axis. we require the sum
    return K.sum(K.binary_crossentropy(y_true, y_pred), axis=-1)


class KLDivergComp(Layer):
    """ Identity transform layer that adds KL divergence
    to the final model loss.
    """
    def __init__(self, *args, **kwargs):
        self.is_placeholder = True
        super(KLDivergComp, self).__init__(*args, **kwargs)
    def call(self, inputs): # it computes loss due to latent vars per mini-batch
        mu, log_var = inputs
        kl_batch=-0.5 * K.sum(1 +log_var -K.square(mu) -K.exp(log_var), axis=-1)
        self.add_loss(K.mean(kl_batch), inputs=inputs)
        return inputs  # pass by

#............................
#............................
#............................
class Deep_CVAE_Mnist(object):
    """Contains all operations specific for this project"""

    def __init__(self,args):
        self.outPath=args.outPath
        self.dataPath=args.dataPath
        self.name=args.prjName
        self.label0=args.y
        print(self.__class__.__name__,'TF ver:', K.tf.__version__,', prj:',self.name)

#............................
    def split_raw_input(self,frac):
        print('read raw data')
        # Load pre-shuffled MNIST data into train and test sets
        (X, Y), (Xt, Yt) = mnist.load_data()
        img_dim=X.shape[1]*X.shape[2]
        X = X.reshape(-1, img_dim) / 255.
        Xt = Xt.reshape(-1, img_dim) / 255.

        nDig=np.unique(Y).shape[0] # there are 10 image classes
        print('raw data shape X:',X.shape,' Y:',Y.shape,' nDig=',nDig)
        nTrain=int(len(X) *frac)
        self.data={}

        self.data['X_val']=X[nTrain:].astype('float32')
        self.data['Y_val']=Y[nTrain:]
        self.data['Yhot_val']=np_utils.to_categorical(self.data['Y_val'],nDig)

        self.data['X_train']=X[:nTrain].astype('float32')
        self.data['Y_train']=Y[:nTrain]
        self.data['Yhot_train']=np_utils.to_categorical(self.data['Y_train'],nDig)

        self.data['X_test']=Xt.astype('float32')
        self.data['Y_test']=Yt
        self.data['Yhot_test']=np_utils.to_categorical(self.data['Y_test'],nDig)

        print('formated data shapes:',end='')
        for x in self.data:
            if 'Y' in x: print(x,self.data[x].shape,', ',end='')
        print()

#............................
    def save_input_hdf5(self):
        outF=self.dataPath+'/'+self.name+'.data.h5'
        print('save data as hdf5:',outF)
        h5f = h5py.File(outF, 'w')
        for x in self.data:
            xobj=self.data[x]
            print('h5-write ',x,type(xobj),xobj.shape,xobj.dtype);
            h5f.create_dataset(x, data=self.data[x])
        #for x in h5f.keys(): 
        #    print(x,self.data[x].shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%xx)

#............................
    def load_input_hdf5(self,dataL):
        inpF5=self.dataPath+'/'+self.name+'.data.h5'
        print('clear old, load new data from hdf5:',inpF5)
        h5f = h5py.File(inpF5, 'r')
        self.data={}
        for x in ['X','Y','Yhot']:
            for y in dataL:
                ddN=x+'_'+y
                self.data[ddN] = h5f[ddN][:]
                print('read ',ddN,self.data[ddN].shape)
        h5f.close()

    #............................
    def input_filter_label(self,dom):        
        X=self.data['X_'+dom]
        Y=self.data['Y_'+dom]
        Yhot=self.data['Yhot_'+dom]
        X1=[];Y1=[]; Yhot1=[]
        for x,y,yhot in zip(X,Y,Yhot):
            if y!=self.label0: continue
            X1.append(x)
            Y1.append(y)
            Yhot1.append(yhot)
        print('survived %d samples of %d for lab=%d'%(len(X1),len(X),self.label0))
        self.data['X_'+dom]=np.array(X1)
        self.data['Y_'+dom]=np.array(Y1)
        self.data['Yhot_'+dom]=np.array(Yhot1)
        

    #............................
    def print_input(self,name,k):
        nameX='X_'+name
        
        assert nameX in self.data
        X=self.data[nameX]
        Y=self.data['Y_'+name]
        print('sample of ',k,nameX)
        for i in range(k):
            print('\nidx=%d digit=%d, X-data:'%(i,Y[i]))
            print(X[i][10*28:13*28])
            print('\nY-data:', Y[i])

    #............................
    def build_model(self,args):
        # based on http://louistiao.me/posts/implementing-variational-autoencoders-in-keras-beyond-the-quickstart-tutorial/

        # fixed parameters of model
        latent_dim=2
        hiden_dim = 256
        epsilon_std = 1.0
        dropFr=args.dropFrac

        img_dim=self.data['X_train'].shape[1]
        lab_dim=self.data['Yhot_train'].shape[1]

        # - - - Assembling model 

        x1 = Input(shape=(img_dim,),name='inp_img')
        x2 = Input(shape=(lab_dim,),name='inp_lab')
        print('build_model inp1:',x1.get_shape(),' inp2:',x2.get_shape())

        x12 = concatenate([x1,x2],name='concat_12')
        print('x12=>',x12.get_shape())

        h = Dense(hiden_dim, activation='relu',name='1h_%d'%hiden_dim)(x12) 
        h = Dropout(dropFr,name='1frac_%.2f'%dropFr)(h)
        h = Dense(hiden_dim, activation='relu',name='2h_%d'%hiden_dim)(h)
        h = Dropout(dropFr,name='2frac_%.2f'%dropFr)(h)
        
        z_mu0 = Dense(latent_dim,name='z_mu0')(h)
        z_log_var0 = Dense(latent_dim,name='z_var_log0')(h)

        z_mu, z_log_var = KLDivergComp(name='add2loss')([z_mu0, z_log_var0])
        
        z_sigma = Lambda(lambda t: K.exp(.5*t), name='z_sig')(z_log_var0) 

        eps_gen=K.random_normal(stddev=epsilon_std,shape=(K.shape(x1)[0], latent_dim))
        eps = Input(tensor=eps_gen,  name='eps_gen')
        z_eps = Multiply(name='stoch_sig')([ eps,z_sigma])
        z = Add(name='stoch_z')([ z_mu,z_eps])

        # "decoder" 
        zx2 = concatenate([z,x2],name='concat_zx2')

        h=Dense(hiden_dim, input_dim=latent_dim, activation='relu',name='3h_%d'%hiden_dim)(zx2)
        h=Dropout(dropFr,name='3frac_%.2f'%dropFr)(h)

        h=Dense(hiden_dim, input_dim=latent_dim, activation='relu',name='4h_%d'%hiden_dim)(h)
        h=Dropout(dropFr,name='4frac_%.2f'%dropFr)(h)

        decoded =  Dense(img_dim, activation='sigmoid',name='out_img')(h)

        # full model
        cvaeM = Model(inputs=[x1,x2, eps], outputs=decoded)
        cvaeM.compile(optimizer='rmsprop', loss=NegLogLikeh)
        #? ,optimizer='adam'

        # sub-models share waights with the full VAE model 
        encoderM = Model(inputs=[x1,x2], outputs=[z_mu0,z_sigma])

        # create a placeholder for an encoded (zip-dimensional) input
        tmp_z = Input(shape=(latent_dim,),name='inp_z')
        tzx2 = concatenate([tmp_z,x2],name='concat_tzx2')

        # retrieve last 5 layers of the autoencoder to reach latent inputs
        kBack=5
        net = cvaeM.layers[-kBack](tzx2)
        for k in range(1,kBack):
            j=k-kBack
            #print('add',kBack,k,j)
            net = cvaeM.layers[j](net)

        # this model maps encoded representation to input image
        decoderM = Model(inputs=[tmp_z,x2], outputs=net) #net
        self.model=cvaeM
        self.encoderM=encoderM
        self.decoderM=decoderM
        # - - -  Model completed

        print('\nThis is full CVAE:')
        cvaeM.summary() # will print
        print('\nThis is Encoder:')
        encoderM.summary() # will print
        print('\nThis is Decoder:')
        decoderM.summary() # will print


    #............................
    def train_model(self,args):
        X=self.data['X_train']; Y=self.data['Yhot_train']
        X_val=self.data['X_val']; Y_val=self.data['Yhot_val']
 
        if args.events>0:
            if len(Y) > args.events : 
                X=X[:args.events]
                Y=Y[:args.events]
                print('reduced traing to %d events'%len(Y))
        if args.verb==0:
            print('train silently epochs:',args.epochs)
     
        callbacks_list = []
        if args.earlyStopOn:
            earlyStop=EarlyStopping(monitor='val_loss', patience=2+args.arrIdx, verbose=1, mode='auto')
            callbacks_list.append(earlyStop)

        if args.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.name+'.weights.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1)
            callbacks_list.append(ckpt)

        print('\nTrain_model X:',X.shape, ' earlyStop=',args.earlyStopOn,' epochs=',args.epochs,' batch=',args.batch_size)
        startTm = time.time()
        hir=self.model.fit([X,Y],X, callbacks=callbacks_list,
                 validation_data=([X_val,Y_val],X_val),  shuffle=True,
                 batch_size=args.batch_size, nb_epoch=args.epochs, 
                 verbose=args.verb)
        self.train_hirD=hir.history
        #evaluate performance for the last epoch
        loss=self.model.evaluate([X_val,Y_val],X_val)
        fitTime=time.time() - startTm
        print('\n End Validation  Loss:%.3f'%loss,', fit time=%.1f sec'%(fitTime))
        self.train_sec=fitTime


    #............................
    def model_evaluate(self,name):
        nameX='X_'+name
        print('model_evaluate ',nameX,' using current model')
        score = self.model.evaluate(self.data[nameX],self.data[nameX], verbose=0)
        print('score loss, acc=',score)


    #............................
    def save_model_full(self):
        fname=self.outPath+'/'+self.name
        model=self.model
        for i in range(3):
            if i==1:
                fname=self.outPath+'/'+self.name+'_encoder'
                model=self.encoderM
            if i==2:
                fname=self.outPath+'/'+self.name+'_decoder'
                model=self.decoderM
                            
            fname+='.model.h5'        
            print('save model full to',fname)
            model.save(fname)

 
    #............................
    def load_model_full(self):
        try:
            del self.model
            del self.encoderM
            del self.decoderM
            print('delte old model')
        except:
            a=1

        subModelL=['', '_encoder', '_decoder']  
        fname=self.outPath+'/'+self.name
        for i in range(3):
            fname=self.outPath+'/'+self.name+subModelL[i]
            fname+='.model.h5'
            print('load model full from',fname)
            model=load_model(fname,custom_objects={'KLDivergComp':KLDivergComp,'NegLogLikeh':NegLogLikeh}) # creates model from HDF5
            model.summary()
            
            if i==0: self.model=model
            if i==1: self.encoderM=model
            if i==2: self.decoderM=model
     

  
