import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()

#from keras import utils as np_utils
from Util_Func import multiSpike_generator

from keras import backend as K
from keras import losses, optimizers 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout,UpSampling1D, Reshape,Conv1D,Flatten,MaxPool1D
from keras.models import Model,  load_model

from keras.layers.advanced_activations import LeakyReLU # breaks model_save unless it is a separate layer

import numpy as np
import h5py

from sklearn.metrics import mean_absolute_error,  mean_squared_error

print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
def NegLogLikeh(y_true, y_pred):
    """ for continuous features use MSE """
    # keras.losses.mean_squared_errorgives the mean
    # over the last axis. we require the sum
    #return K.sum(losses.mean_squared_error(y_true, y_pred), axis=-1)
    return K.sum(losses.mean_absolute_error(y_true, y_pred), axis=-1)
    '''
    neve=K.shape(y_true0)[0]
    ndim=K.ndim(y_true0)
    print('KKK1',y_true0.shape,' neve=',neve,ndim)
    y_true=K.reshape(y_true0,shape=(neve,-1))
    y_pred=K.reshape(y_pred0,shape=(neve,-1))
    #y_true=y_true0
    #y_pred=y_pred0
    print('KKK2',y_true.shape)
    '''


#............................
#............................
#............................
class KLDivergDVAE(Layer):
    """ Identity transform layer that adds 'partial' KL divergence
    to the final model loss.
    """
    def __init__(self,strength=1., *args, **kwargs):
        self.is_placeholder = True
        self.strength=strength  # the smaller it gets the closer VAE-->AE
        super(KLDivergDVAE, self).__init__(*args, **kwargs)
        
    def call(self, inputs): # it computes loss due to latent vars per mini-batch
        mu, drive, log_var = inputs       
        kl_batch=-0.5 * K.sum(1 +log_var  -K.square(mu-drive)  -K.exp(log_var), axis=-1)
        self.add_loss(K.mean(self.strength*kl_batch), inputs=inputs)
        return [mu, log_var]  # pass by the standard VAE output 


#............................
#............................
#............................
class KLDivergVAE(Layer):
    """ Identity transform layer that adds 'partial' KL divergence
    to the final model loss.
    """
    def __init__(self,strength=1., *args, **kwargs):
        self.is_placeholder = True
        self.strength=strength  # the smaller it gets the closer VAE-->AE
        super(KLDivergVAE, self).__init__(*args, **kwargs)
        
    def call(self, inputs): # it computes loss due to latent vars per mini-batch
        mu,log_var = inputs       
        kl_batch=-0.5 * K.sum(1 +log_var  -K.square(mu)  -K.exp(log_var), axis=-1)
        self.add_loss(K.mean(self.strength*kl_batch), inputs=inputs)
        return [mu, log_var]  # pass by the standard VAE output 



#............................
#............................
#............................
class Deep_DVAE(object):
    """Contains all operations specific for this project"""

    def __init__(self,args):
        self.outPath=args.outPath
        self.dataPath=args.dataPath
        self.name=args.prjName
        self.events=args.events
        self.verb=args.verb
        self.modelDesign=args.modelDesign
        self.data={}
        self.epsLayerExist=False
        print(self.__class__.__name__,'TF ver:', K.tf.__version__,', prj:',self.name)

#............................
    def generate_pulses(self,args):
        domL={'train':0.8,'val':0.9,'test':1.01}
        print('split frames goal:',domL)
        
        dataD={}
        for dom in domL:
            dataD[dom]={'X':[],'U':[]}

        start = time.time()
        for i in range(args.events):
            r=np.random.uniform() # train/val/test
            funcV,parmV=multiSpike_generator()
            if i%1000==0:
                print('gen i=',i,' pars',parmV)

            # assign domain
            for dom in domL:
                if r >domL[dom] : continue
                dataD[dom]['X'].append(funcV)
                dataD[dom]['U'].append(parmV)
                break

        for dom in domL:
            Xi=dataD[dom]['X']
            Ui=dataD[dom]['U']
            self.data[dom]=[np.array(Xi),np.array(Ui)]

        print('%d frames generated , elaT=%.1f sec'%(args.events,(time.time() - start)))


#............................
    def save_data_hdf5(self):

        for dom in self.data:
            outF=self.dataPath+'/f2f_'+dom+'.frames.yml'
            print('save data as hdf5:',outF)
            h5f = h5py.File(outF, 'w')
            X,Y=self.data[dom]
            print('h5-write X',dom,X.shape);
            h5f.create_dataset('X', data=X)
            print('h5-write Y',dom,Y.shape);
            h5f.create_dataset('Y', data=Y)
            h5f.close()
            xx=os.path.getsize(outF)/1048576
            print('closed  hdf5:',outF,' size=%.2f MB'%xx)


    #............................
    def load_data_hdf5(self,dom):
        inpF=self.dataPath+'/f2f_'+dom+'.frames.yml'

        h5f = h5py.File(inpF, 'r')
        xyL=[]
        for x in ['X','Y']:            
            obj=h5f[x][:]
            if x=='X' and self.modelDesign=='lstm':
                neve=obj.shape[0]
                obj=obj.reshape(neve,-1,1)
            print('read ',dom,x,obj.shape)
            if self.events>0 and obj.shape[0] > self.events : 
                obj=obj[:self.events]                
                print('reduced traing to %d events'%obj.shape[0])
            xyL.append(obj)
        self.data[dom]=xyL
        h5f.close()
        print('loaded ',self.data.keys())


    #............................
    def print_frames(self,dom,numFrm=1):
        X,Y=self.data[dom]
        print('sample of ',numFrm,' from dom=',dom)
        for i in range(numFrm):            
            print('i=',i,'par:',Y[i],'frm:',X[i,::5],'::5')
            
    #............................
    def build_model(self,args): 
        if args.modelDesign=='fcDVAE':
            self.build_model_fcDVAE(args)
        elif args.modelDesign=='fcVAE':
            self.build_model_fcVAE(args)
        else:
            print('unknow design:',args.modelDesign)
            bad_bad17

        # - - -  Model assembled and compiled
        self.epsLayerExist=True # to correct input if model is only loade
        
        for mm in self.model:
            mmobj=self.model[mm]
            print('\nSummary model=%s  layers=%d , params=%.1f K, inputs:'%(mm,len(mmobj.layers),mmobj.count_params()/1000.),mmobj.input_shape)
            if args.verb>1 or mm=='auto':
                mmobj.summary() # will print


    #............................
    def build_model_fcDVAE(self,args): 
        X,U=self.data['train']
        time_dim=X.shape[1]        
        latent_dim=U.shape[1]        
        dense_dim = 50
        dense_layer=2
        dropFrac=args.dropFrac
        epsilon_std = 0.99
        KLD_strength=0.99

        # - - - Assembling model 
        x = Input(shape=(time_dim,),name='inp_X')
        u = Input(shape=(latent_dim,),name='inp_U')
        print('build_model_fc inpX:',x.get_shape(),'  inpU:',u.get_shape(),'  KLD_strength=', KLD_strength, ' dense_layer=',dense_layer)
        h=x    
        for i in range(dense_layer):
            h = Dense(dense_dim,name='enc%d'%i)(h) 
            h =LeakyReLU()(h)
            h = Dropout(dropFrac)(h) 

        #.... encoded  has 2 output tensors: meanZ and sigmaZ

        z_mu0 = Dense(latent_dim, activation='linear',name='z_mu_%d'%latent_dim)(h) 
        z_log_var0 = Dense(latent_dim, activation='linear',name='z_var_log')(h)
        z_mu, z_log_var = KLDivergDVAE(strength=KLD_strength,name='dvae_loss')([z_mu0, u, z_log_var0])
        
        z_sigma = Lambda(lambda t: K.exp(.5*t), name='z_sig')(z_log_var0) 
        eps_gen=K.random_normal(stddev=epsilon_std,shape=(K.shape(x)[0], latent_dim))
        eps = Input(tensor=eps_gen,  name='eps_gen')
        z_eps = Multiply(name='stoch_sig')([ eps,z_sigma])
        z = Add(name='stoch_z')([ z_mu,z_eps])
        print('build_model latent z+eps:',z.get_shape())
        # .... decoder: 
        h=z
        for i in range(dense_layer):
            h = Dense(dense_dim,name='dec%d'%i)(h) 
            h =LeakyReLU()(h)
            h = Dropout(dropFrac)(h) 
        decoded = Dense(time_dim, activation='linear',name='out_img')(h)

        print('build_model decoded:',decoded.get_shape())
        # full model
        vaeM = Model(inputs=[x, u, eps], outputs=decoded)
  
        vaeM.compile(optimizer='adam', loss=NegLogLikeh)

        # sub-models share waights with the full VAE model 
        encoderM = Model(inputs=[x, u], outputs=[z_mu0,z_sigma])

        # create a placeholder for an encoded (zip-dimensional) input
        tmp_z = Input(shape=(latent_dim,),name='inp_z')

        #vaeM.summary() # will print
        # retrieve last layer of the autoencoder to reach latent inputs
        numLay=len(vaeM.layers)
        print('vaeM: numLayers',numLay)
        jj=0; nBack=-1
        for lay in vaeM.layers:
            jjInv=numLay-jj-1
            print('lay:',jj,jjInv,lay.name)#,lay.get_shape())
            if lay.name=='stoch_z': nBack=jjInv
            jj+=1
        assert nBack>0


        h = vaeM.layers[-nBack](tmp_z)
        for jj in range(1,nBack):
            h = vaeM.layers[-nBack+jj](h)

        # this model maps encoded representation to input image
        decoderM = Model(inputs=tmp_z, outputs=h) #net
        self.model={'auto':vaeM, 'encoder':encoderM, 'decoder':decoderM}

    #............................
    def build_model_fcVAE(self,args): 
        X,U=self.data['train']
        time_dim=X.shape[1]        
        latent_dim=U.shape[1]        
        dens_dim = 50
        dropFrac=args.dropFrac
        epsilon_std = 1.0
        KLD_strength=0.4

        # - - - Assembling model 
        x = Input(shape=(time_dim,),name='inp_X')
        print('build_model_fc inpX:',x.get_shape(),'  KLD_strength=', KLD_strength)
    
        h = Dense(dens_dim, activation='relu',name='enc1')(x)
        h = Dropout(dropFrac,name='drop_%.2f'%dropFrac)(h) 
        h = Dense(dens_dim, activation='relu',name='enc2')(h)
        #.... encoded  has 2 output tensors: meanZ and sigmaZ
        z_mu0 = Dense(latent_dim, activation='linear',name='z_mu_%d'%latent_dim)(h) 
        z_log_var0 = Dense(latent_dim, activation='linear',name='z_var_log')(h)
        z_mu, z_log_var = KLDivergVAE(strength=KLD_strength,name='vae_loss')([z_mu0, z_log_var0])
        
        z_sigma = Lambda(lambda t: K.exp(.5*t), name='z_sig')(z_log_var0) 
        eps_gen=K.random_normal(stddev=epsilon_std,shape=(K.shape(x)[0], latent_dim))
        eps = Input(tensor=eps_gen,  name='eps_gen')
        z_eps = Multiply(name='stoch_sig')([ eps,z_sigma])
        z = Add(name='stoch_z')([ z_mu,z_eps])
        print('build_model latent z+eps:',z.get_shape())
        # .... decoder: 
        h = Dense(dens_dim, activation='relu',name='dec1')(z)
        h = Dropout(dropFrac)(h)
        h = Dense(dens_dim, activation='relu',name='dec2')(h)
        decoded = Dense(time_dim, activation='linear',name='out_img')(h)

        print('build_model decoded:',decoded.get_shape())
        # full model
        vaeM = Model(inputs=[x, eps], outputs=decoded)
  
        vaeM.compile(optimizer='adam', loss=NegLogLikeh)

        # sub-models share waights with the full VAE model 
        encoderM = Model(inputs=x, outputs=[z_mu0,z_sigma])

        # create a placeholder for an encoded (zip-dimensional) input
        tmp_z = Input(shape=(latent_dim,),name='inp_z')

        # retrieve last layer of the autoencoder to reach latent inputs
        nBack=4
        h = vaeM.layers[-nBack](tmp_z)
        for jj in range(1,nBack):
            h = vaeM.layers[-nBack+jj](h)

        # this model maps encoded representation to input image
        decoderM = Model(inputs=tmp_z, outputs=h) #net
        self.model={'auto':vaeM, 'encoder':encoderM, 'decoder':decoderM}
 
    #............................
    def train_model(self,args):
        X,U=self.data['train']
        X_val,U_val=self.data['val']
        
        if args.verb==0:
            print('train silently epochs:',args.epochs)
     
        callbacks_list = []
        if args.earlyStopOn:
            earlyStop=EarlyStopping(monitor='val_loss', patience=2+args.arrIdx, verbose=1, mode='auto')
            callbacks_list.append(earlyStop)

        if args.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.name+'.weights.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1)
            callbacks_list.append(ckpt)

        print('\nTrain_model X:',X.shape, ' earlyStop=',args.earlyStopOn,' epochs=',args.epochs,' batch=',args.batch_size,'  modelDesign=', self.modelDesign)
        startTm = time.time()
        
        if 'fcVAE' in self.modelDesign:
            Xin=X; Xin_val=X_val
        else:
            Xin=[X,U]; Xin_val=[X_val,U_val]
        hir=self.model['auto'].fit(Xin,X,callbacks=callbacks_list,
                 validation_data=(Xin_val,X_val),  shuffle=True,
                 batch_size=args.batch_size, nb_epoch=args.epochs, 
                 verbose=args.verb)
        self.train_hirD=hir.history
        #evaluate performance for the last epoch
        loss=self.train_hirD['val_loss'][-1]
        fitTime=time.time() - startTm
        print('\n End Validation  Loss:%.3f'%loss,', fit time=%.1f min'%(fitTime/60.))
        self.train_sec=fitTime

    #............................
    def predict_modelF(self,dom,scope):
        X,U=self.data[dom]

        model=self.model[scope]
        print('\npredict[%s] inputs:'%scope,model.input_shape,type(model.input_shape),', outputs:',model.output_shape)
        
        if scope=='encoder':
            if type(model.input_shape)==type(list()) and 'fcDVAE' in self.modelDesign:
                out=model.predict([X,U])
            else:
                out=model.predict(X)
            return out

        if scope=='auto':
            if 'fcVAE' in self.modelDesign:
                Xin=[X]
            else:
                Xin=[X,U]
            if not self.epsLayerExist:
                inpShpL=model.input_shape
                epsshp=inpShpL[-1]
                eps_inp=np.zeros((U.shape[0],epsshp[1]))
                Xin.append(eps_inp)
            Xout=model.predict(Xin)
            return Xout
            
        elif scope=='decoder':
            a=1
            bad_22
        else:
            print('abort predicton, unexpected scope=',scope)
            bad_bad2
    #............................
    def save_model_full(self):
        modelA=self.model
        for name in modelA:
            fname=self.outPath+'/'+self.name+'_'+name+'.model.h5'     
            print('save model  to',fname)
            modelA[name].save(fname)


   #............................
    def load_model_full(self):
        custObj={'KLDivergDVAE':KLDivergDVAE,'KLDivergVAE':KLDivergVAE,'NegLogLikeh':NegLogLikeh}
        self.model={}
        for name in ['auto', 'encoder', 'decoder']:
            fname=self.outPath+'/'+self.name+'_'+name+'.model.h5'     
            print('load model from ',fname)
            model=load_model(fname,custom_objects=custObj) # creates model from HDF5
            if  self.verb>1 or name=='auto':  model.summary()
            self.model[name]=model


   #............................
    def eval_dom_loss(self,dom):
        print('eval_dom_loss dom=',dom)

        Xp,Up=self.data[dom]
        scope='auto'
        Xq=self.predict_modelF(dom,scope)
        Xp=Xp.reshape(Xp.shape[0],-1)
        Xq=Xq.reshape(Xp.shape[0],-1)
        print('ev-dom-loss',type(Xq),Xp.shape,Xq.shape)
        
        mseV=[]; maeV=[]
        i=0
        fac=100
        for p,q in zip(Xp,Xq):
            i+=1
            mse=fac*mean_squared_error(p,q)
            mae=fac*mean_absolute_error(p,q)
            mseV.append(mse)
            maeV.append(mae)
            if i<5:
                print('i',i,len(p),len(q),'mse,mae:',mse,mae)

        self.eval={'Xp':Xp,'Up':Up,'Xq':Xq,'dom':dom,'scope':scope} #
        self.eval['MSE']=np.array(mseV)
        self.eval['MAE']=np.array(maeV)


    #............................
    def reveal_myFunc(self,dom,frId):
        print('reveal myFunc for dom=%s frameId=%d'%(dom,frId) )
        X=self.data['X_'+dom][frId]
        Y=self.data['Y_'+dom][frId]
        Z=np.reshape(Y,(-1,2))

        print('oz',Z.shape,Z)

        U=self.decoderM.predict(Z)
        print('oo',U.shape)
        U=U.flatten()
        print('oo-flat',U.shape)
        #print('ood',out1)
        
        for i in range(U.shape[0]):
            x=X[i][0]
            u=U[i]
            d=x-u
            print(i,x,u,d)
  

        
