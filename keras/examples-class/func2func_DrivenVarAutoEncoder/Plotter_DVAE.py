import os
import numpy as np
from matplotlib import cm as cmap
import socket  # for hostname
from tensorflow.contrib.keras.api.keras.utils import plot_model

import time
from sklearn.manifold import TSNE
#from Util_Func import func_val_norm, func_par_norm

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_DVAE(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args):
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        
        if not os.path.exists(self.outPath):
            print('Aborting on start, missing output dir',args.outPath)
            exit(22)

    #............................
    def display_all(self,args,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/idx%d_%s_%s_f%d'%(self.outPath,args.arrIdx,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()

#............................
    def plot_model(self,deep,flag=0):
        #print('plot_model broken in TF=1.4, skip it'); return

        if 'cori' not in socket.gethostname(): return  # software not installed
        modelA=deep.model
        for name in modelA:
            fname=self.outPath+'/'+deep.name+'_'+name+'_graph.svg'
            plot_model(modelA[name], to_file=fname, show_layer_names=flag>0, show_shapes=flag>1)
            print('Graph saved as ',fname,' flag=',flag)

#............................
    def plot_data_vsTime(self,dataA,dom,idxL,figId=7):
        self.figL.append(figId)
        X,U=dataA[dom]
        fig=self.plt.figure(figId,facecolor='white', figsize=(9,5))
       
        nrow,ncol=2,3
        print('plot input for idx=',idxL)
        j=0
        for idx in idxL:
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            amplV=X[idx]
            ax = self.plt.subplot(nrow, ncol, 1+j)
            j+=1
            tit='%d '%(idx)
            ptxtL=[ '%.2f'%x for x in U[idx]]
            tit+=' par:'+','.join(ptxtL)
            ax.set(title=tit[:25]+'...',ylabel='ampl', xlabel='time')
            ax.plot(amplV)

            ax.axhline(0, color='blue', linestyle='--')
            ax.set_ylim(-0.2,1)
            #ax.set_xlim(200,800)

            ax.grid()
            


#............................
    def plot_train_history(self,deep,args,figId=10):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,5))
        
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((2,4), (1,0), colspan=3 )
        
        DL=deep.train_hirD
        nSkip=1
        valLoss=DL['val_loss'][-1]
        tit1='%s, train %.1f min, drop=%.2f, end-val=%.3f'%(deep.name,deep.train_sec/60.,args.dropFrac,valLoss)
        ax1.set(ylabel='loss',title=tit1,xlabel='epochs')            # use Y.shape
        ax1.plot(DL['loss'],'.-.',label='train n=%d'%deep.data['train'][1].shape[0])
        ax1.plot(DL['val_loss'],'.-',label='val n=%d'%deep.data['val'][1].shape[0])
        ax1.legend(loc='best')
        #ax1.set_ylim(0,DL['loss'][nSkip])
        #ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')
        
    
#............................
    def plot_func_residua(self,evalD,tit0,idxL,figId=8):
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(11,6))

        Xp=evalD['Xp']
        Xq=evalD['Xq']

        Up=evalD['Up']
        #print('ttff',type(Xq),Xp.shape,Xq.shape)

        t_bin=range(Xp.shape[1])

        nrow,ncol=2,4
        print('plot input for idx=',idxL)
        j=0
        for i in idxL:            
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j)
            j+=1

            tit='i:%d U=%.2f , %.2f'%(i,Up[i][0],Up[i][1])            
            ax.set(title=tit)
            xp=Xp[i].flatten()
            xq=Xq[i].flatten()
            ax.plot(xp,label='inp')
            ax.plot(xq,label='out')
            ax.fill_between(t_bin,xq-xp,label='diff',facecolor='red')
            #ax.set_ylim(-.5,0.9)
            ax.grid()
            ax.legend(loc='top left')
            ax.set_ylim(-0.2,1)
            ax.axhline(0, color='blue', linestyle='--')
            if j==0:  ax.text(200,0.4,tit0,rotation=45)


            
#............................
    def plot_dom_loss(self,deep,figId=11):
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,6))
        nameL=['MSE','MAE'] 
        text1=deep.eval['scope']+' '+deep.eval['dom']
        nrow,ncol=2,2
        print('plot dom loss, num frames',deep.eval['MSE'].shape)
        j=1
        for name in nameL:
            ax = self.plt.subplot(nrow, ncol, j)
            dataV=deep.eval[name]
            col='b'
            if name=='MAE' : col='g'
            n, bins, patches=ax.hist(dataV, 50,facecolor=col)
            ax.grid()
            #ax.set_yscale('log')
            avr=dataV.mean()
            std=dataV.std()
            txt='%s  avr=%.2f+/-%.2f'%(text1,avr,std)
            ax.set(title=txt,xlabel='frame loss=100*'+name,ylabel='frames')
            j+=1
        #return    
        ax = self.plt.subplot(nrow, ncol, j)
        hb = ax.hist2d(deep.eval['MAE'],deep.eval['MSE'] , bins=20,  cmin=1, cmap=cmap.rainbow)
        cb = fig.colorbar(hb[3], ax=ax)
        ax.grid()
        ax.set(title='loss correlation',xlabel='100*mae',ylabel='100*mse')

    #............................
    def plot_worst_loss(self,deep,maeThr=0.2,figId=12):

        frameA=deep.eval['mae']
        # find index of worst frames
        idxL=np.nonzero(frameA >maeThr) 
        nBad=len(idxL[0])
        print('worst mseThr=',maeThr,' num frames=',nBad,' of ', frameA.shape)
        
        if nBad <2 : return
        worstD={}
        worstD['Xp']=deep.eval['Xp'][idxL]
        worstD['Yp']=deep.eval['Yp'][idxL]
        worstD['Xq']=deep.eval['Xq'][idxL]
        self.plot_func_residua(worstD,'maeThr >%.1f'%maeThr,range(8),figId=figId) 

        
#............................
    def plot_param_residua(self,U,Z,S,figId=9):
        nPar=Z.shape[1]
        nrow,ncol=2,nPar
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(2.5*nPar,5))
 

        assert U.shape[1]==Z.shape[1]
        j=0
        for iPar in range(0,nPar):
            ax=self.plt.subplot(nrow,ncol,j+1)
            u=U[:,iPar]
            z=Z[:,iPar]
            img=ax.scatter(u,z, alpha=.4, s=8)
            ax.set(title='U-avrZ iPar=%d'%iPar, xlabel='true U_%d'%iPar, ylabel='avrZ_%d'%iPar)
            
            ax=self.plt.subplot(nrow,ncol,j+1+ncol)
 
            s=S[:,iPar]
            img=ax.scatter(u,s, alpha=.4, s=8)
            ax.set(title='U-sigZ iPar=%d'%iPar, xlabel='true U_%d'%iPar, ylabel='sigZ_%d'%iPar)
            ax.set_ylim(0.,1.2)
            ax.grid()
            j+=1

        return

#............................
    def plot_latent_z(self,avrZ,sigZ,figId=16):
        nPar=avrZ.shape[1]
        nrow,ncol=2,int(nPar/2+0.5) # displays 2 pars per plot
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        if figId in self.figL:
            self.plt.figure(figId)
        else:
            self.figL.append(figId)
            self.plt.figure(figId,facecolor='white', figsize=(3.5*ncol,7))


        #print(type(zV),zV.shape,zV[0])

        # plot correlation between latent space variables - should be none
        j=0
        for iPar in range(0,nPar,2):
            iPar2=(iPar+1)%nPar
            y1=avrZ[:,iPar] ; y2=avrZ[:,iPar2]
            ax=self.plt.subplot(nrow,ncol,j+1)
            j+=1
            img=ax.scatter(y1,y2, alpha=.4, s=8, cmap='cool')
            ax.set(title='Z mean %d-%d'%(iPar,iPar2), xlabel='Z_%d'%iPar, ylabel='Z_%d'%(iPar2))

        # lower row is for sigmas
        for iPar in range(0,nPar,2):
            iPar2=(iPar+1)%nPar
            y1=sigZ[:,iPar] ; y2=sigZ[:,iPar2]
            ax=self.plt.subplot(nrow,ncol,j+1)
            j+=1
            img=ax.scatter(y1,y2, alpha=.4, s=8, cmap='cool')
            ax.set(title='Z sig', xlabel='sZ_%d'%iPar, ylabel='sZ_%d'%(iPar2))
            ax.set_xlim(0,1.2) ; ax.set_ylim(0,1.2)

        return
 
#............................
    def plot_latent_TSNE(self,zTens,Y,text1,figId=12):
        print('do TSNE, zTensor:',zTens.shape,' Y:',Y.shape)

        time_start = time.time()
        tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
        tsne_results = tsne.fit_transform(zTens)
        print ('t-SNE done1 ! Time elapsed: %.1f seconds, figId=%d'%(time.time()-time_start,figId))

        fig=self.plt.figure(figId,facecolor='white', figsize=(12,8))
        self.figL.append(figId)
        nrow,ncol=3,3

        df_x2 = tsne_results[:,0]
        df_y2 = tsne_results[:,1]

        ax=self.plt.subplot(nrow,ncol,1)
        img = ax.hexbin(df_x2, df_y2,cmap='Accent',gridsize=30)
    
        ax.set(title=text1+' %dD to TSNE colored by:'%(zTens.shape[1]))
        ax.set_axis_off()
        self.plt.colorbar(img).set_label('frame count')


        j=2
        for iPar in range(Y.shape[1]):
            df_lbl=Y[:,iPar]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            img=ax.scatter(df_x2,df_y2, alpha=.4, s=3**2 ,c=df_lbl, cmap='cool')

            self.plt.colorbar(img).set_label(' Y[%d]-parameter'%iPar)
            ax.set(title='%dD to TSNE colored by'%(zTens.shape[1]))
            ax.set_axis_off()

        return
 
#............................
    def plot_data_params(self,dataA,dom,figId=6):
        X,U=dataA[dom]
        nPar=U.shape[1]
        nrow,ncol=1,int(nPar/2+0.5) # displays 2 pars per plot
 
        fig=self.plt.figure(figId,facecolor='white', figsize=(3*ncol,3))
        self.figL.append(figId)
        j=0
        for iPar in range(0,U.shape[1],2):
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j+1)
            j+=1
            img=ax.scatter(y1,y2, alpha=.4, s=8, cmap='cool')
            ax.set(title='true params', xlabel='U_%d'%iPar, ylabel='U_%d'%(iPar+1))

        return
 
