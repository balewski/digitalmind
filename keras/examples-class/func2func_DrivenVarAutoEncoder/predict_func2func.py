#!/usr/bin/env python
""" read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_DVAE import Plotter_DVAE
from Deep_DVAE import Deep_DVAE

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Mnist data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign', choices=['fcDVAE','fcVAE'],
                         default='fcDVAE',
                         help=" model design of the network")

 
    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='f2f_'+args.modelDesign
    args.arrIdx=0
 
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
ppp=Plotter_DVAE (args )
deep=Deep_DVAE(args)
dom='test'

deep.load_data_hdf5(dom)
#1 ppp.plot_data_vsTime(deep.data,'test',range(6)) 
 
#4 deep.print_frames('test',numFrm=1)
deep.load_model_full()

X,U=deep.data[dom]
# Compute latent space
avrZ,sigZ= deep.predict_modelF(dom,'encoder')
ppp.plot_latent_z(avrZ,sigZ)
ppp.plot_param_residua(U,avrZ,sigZ)


#1 ppp.plot_latent_TSNE(Y,Y,'truth',figId=13)
#1 ppp.plot_latent_TSNE(zV,Y,'latent',figId=14)

deep.eval_dom_loss('test') # full pass
ppp.plot_dom_loss(deep)

ppp.plot_func_residua(deep.eval,'test-sample',range(8)) 
#ppp.plot_worst_loss(deep,maeThr=2.5)
ppp.display_all(args,'predict')



