__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Deep_Common  import Deep_Common

from keras import backend as K
from keras import losses 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout, LSTM
from keras.models import Model,  load_model

import numpy as np
#............................
#............................
#............................
def NegLogLikeh(y_true, y_pred):
    """ for continuous features use MSE """
    # keras.losses.mean_squared_errorgives the mean
    # over the last axis. we require the sum
    #return K.sum(losses.mean_squared_error(y_true, y_pred), axis=-1)
    return K.sum(losses.mean_absolute_error(y_true, y_pred), axis=-1)

#............................
#............................
#............................
class KLDivergComp(Layer):
    """ Identity transform layer that adds 'partial' KL divergence
    to the final model loss.
    """
    def __init__(self,strength=1., *args, **kwargs):
        self.is_placeholder = True
        self.strength=strength  # the smaller it gets the closer VAE-->AE
        super(KLDivergComp, self).__init__(*args, **kwargs)
    def call(self, inputs): # it computes loss due to latent vars per mini-batch
        mu, log_var = inputs
       
        kl_batch=-0.5 * K.sum(1 +log_var  -K.square(mu)  -K.exp(log_var), axis=-1)
        self.add_loss(K.mean(self.strength*kl_batch), inputs=inputs)
        return inputs  # pass by


#............................
#............................
#............................
class Deep_VAE_Func2Func(Deep_Common ):

    def __init__(self,args):
        Deep_Common.__init__(self,args)
        print('created',self.__class__.__name__)

    #............................
    def build_model(self,args): 
        X,Y=self.data['train']
        time_dim=X.shape[1]        
        latent_dim=4
        dens_dim = 50
        dropFrac=args.dropFrac
        epsilon_std = 1.0
        KLD_strength=0.1
        
        # - - - Assembling model 
        x = Input(shape=(time_dim,),name='inp_pulse')
        print('build_model inp1:',x.get_shape(),' KLD_strength=',KLD_strength)
        h = Dense(dens_dim, activation='relu',kernel_initializer='he_uniform',name='enc1')(x)
        h = Dropout(dropFrac,name='drop_%.2f'%dropFrac)(h) 
        h = Dense(dens_dim, activation='relu',name='enc2')(h)
        #.... encoded  has 2 pieces: mand and sigma
        z_mu0 = Dense(latent_dim, activation='linear',name='z_mu_%d'%latent_dim)(h) 
        z_log_var0 = Dense(latent_dim, activation='linear',name='z_var_log')(h)

        z_mu, z_log_var = KLDivergComp(strength=KLD_strength,name='add2loss')([z_mu0, z_log_var0])
        
        z_sigma = Lambda(lambda t: K.exp(.5*t), name='z_sig')(z_log_var0) 

        eps_gen=K.random_normal(stddev=epsilon_std,shape=(K.shape(x)[0], latent_dim))
        eps = Input(tensor=eps_gen,  name='eps_gen')
        z_eps = Multiply(name='stoch_sig')([ eps,z_sigma])
        z = Add(name='stoch_z')([ z_mu,z_eps])
        print('build_model z:',z.get_shape())
        # "decoder" 
        h = Dense(dens_dim, activation='relu',name='dec1')(z)
        h = Dropout(dropFrac)(h)
        h = Dense(dens_dim, activation='relu',name='dec2')(h)
        decoded = Dense(time_dim, activation='sigmoid',name='out_img')(h)

        print('build_model decoded:',decoded.get_shape())
        # full model
        vaeM = Model(inputs=[x, eps], outputs=decoded)
  
        vaeM.compile(optimizer='rmsprop', loss=NegLogLikeh)

        # sub-models share waights with the full VAE model 
        encoderM = Model(inputs=x, outputs=[z_mu0,z_sigma])

        # create a placeholder for an encoded (zip-dimensional) input
        tmp_z = Input(shape=(latent_dim,),name='inp_z')

        # retrieve last layer of the autoencoder to reach latent inputs
        h = vaeM.layers[-4](tmp_z)
        h = vaeM.layers[-3](h)
        h = vaeM.layers[-2](h)
        h = vaeM.layers[-1](h)

        # this model maps encoded representation to input image
        decoderM = Model(inputs=tmp_z, outputs=h) #net
        self.model={'auto':vaeM, 'encoder':encoderM, 'decoder':decoderM}
     
        # - - -  Model completed
        vaeM.summary() # will print
        print('\nFull auto-encoder  layers=%d , params=%.1f K'%(len(vaeM.layers),vaeM.count_params()/1000.))

        print('\nEncoder layers=%d , params=%.1f K'%(len(encoderM.layers),encoderM.count_params()/1000.))
        if args.verb>1:
            encoderM.summary() # will print
            
        print('\nDecoder layers=%d , params=%.1f K'%(len(decoderM.layers),decoderM.count_params()/1000.))
        if args.verb>1:
            decoderM.summary() # will print


    #............................
    def load_model_full(self):
       Deep_Common.load_model_full(self,custObj={'KLDivergComp':KLDivergComp,'NegLogLikeh':NegLogLikeh})

