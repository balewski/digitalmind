__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Common  import Plotter_Common
import numpy as np

#............................
#............................
#............................
class Plotter_AE_Func2Func(Plotter_Common ):

    def __init__(self,args):
        Plotter_Common.__init__(self,args)
        print('created',self.__class__.__name__)
            
#............................
    def plot_latent_z(self,deep,dom,figId=11):
        if figId in self.figL:
            self.plt.figure(figId)
        else:
            self.figL.append(figId)
            self.plt.figure(figId,facecolor='white', figsize=(6,3))
        X,Y=deep.data[dom]
       
        if Y.shape[0] > deep.events and deep.events>0:
            print('plot_latent_z: reduce events from %d to %d'%(Y.shape[0],deep.events))
            X=X[:deep.events,]
            Y=Y[:deep.events,]

        # extract & plot ground truth 2 params
        ax=self.plt.subplot(2,3,1)
        img=ax.scatter(Y[:,1],Y[:,5], alpha=.4, s=3**2)
        ax.set(title='ground truth',xlabel='par=t1',ylabel='par=RC2')

        # Compute latent space
        zVec= deep.model['encoder'].predict(X)
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax=self.plt.subplot(2,3,2)
        
        ztxt='zVar'
        img=ax.scatter(zVec[:, 0], zVec[:, 1], alpha=.4, s=3**2)
        ax.set(title='AE: '+ztxt+', dom='+dom, xlabel=ztxt+'_0',ylabel=ztxt+'_1')
        ax.grid()

        # - - - - -  Z2+Z3
        if zVec.shape[1]< 4: return
        ax=self.plt.subplot(2,3,3)

        img=ax.scatter(zVec[:, 2], zVec[:, 3], alpha=.4, s=3**2)
        ax.set(title='AE: '+ztxt+', dom='+dom, xlabel=ztxt+'_2',ylabel=ztxt+'_3')
        ax.grid()
        return zVec,Y
