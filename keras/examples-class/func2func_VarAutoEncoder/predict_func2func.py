#!/usr/bin/env python
""" read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Mnist data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign', choices=['vae','ae'],
                         default='vae',
                         help=" model design of the network")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='f2f_'+args.modelDesign
    args.arrIdx=0
 
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.modelDesign=='vae':
    from Plotter_VAE_Func2Func import Plotter_VAE_Func2Func as Plotter_F2F
    from Deep_VAE_Func2Func import Deep_VAE_Func2Func as Deep_F2F
elif args.modelDesign=='ae':
    from Plotter_AE_Func2Func import Plotter_AE_Func2Func as Plotter_F2F
    from Deep_AE_Func2Func import Deep_AE_Func2Func as Deep_F2F
else:
    assert 2==4 # implement it

ppp=Plotter_F2F (args )
deep=Deep_F2F(args)

deep.load_data_hdf5('test')
ppp.plot_data_vsTime(deep.data,'test',range(6),norm='2exp') 

deep.print_frames('test',numFrm=1)
deep.load_model_full() 
zTens,Y=ppp.plot_latent_z(deep,'test',figId=10)

#1 ppp.plot_latent_TSNE(Y,Y,figId=13)
ppp.plot_latent_TSNE(zTens,Y,figId=14)

deep.eval_dom_loss('test',-1) # full pass
ppp.plot_dom_loss(deep)
ppp.plot_dom_residua(deep.eval,'test-sample',range(8),norm='2exp') 

for k in range(2,5):
    #break
    xp=deep.eval['Xp']; y=deep.data['test'][1]; xq=deep.eval['Xq']
    ppp.plot_neuron_volt_speed([xp,y],k,norm='2exp',figId=100+k)
    ppp.plot_neuron_volt_speed([xq,y],k,norm='2exp',figId=200+k)

#4 ppp.plot_worst_loss(deep,maeThr=2.5)
ppp.display_all(args,'predict')



