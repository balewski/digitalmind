import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()

#from keras import utils as np_utils
from Util_Func2Func import func_norm, spikeFunc_expo2

from keras import backend as K
from keras import losses 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout, LSTM
from keras.models import Model,  load_model

import numpy as np
import h5py


print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Deep_Common(object):
    """Contains all operations specific for this project"""

    def __init__(self,args):
        self.outPath=args.outPath
        self.dataPath=args.dataPath
        self.name=args.prjName
        self.events=args.events
        self.data={}
        print(self.__class__.__name__,'TF ver:', K.tf.__version__,', prj:',self.name)

#............................
    def generate_pulses(self,args):
        domL={'train':0.8,'val':0.9,'test':1.01}
        print('split frames goal:',domL)
        
        rngT1=[5,50]
        rngRC2=[15,30]
        uoff=-70
        u0=0
        u1=80
        dt=10
        rc1=6

        # 2nd 'b' peak location
        rngT1b=[200,300]

        dataD={}
        for dom in domL:
            dataD['X_'+dom]=[];   dataD['Y_'+dom]=[]
        #print('aa',dataD.keys())
        start = time.time()
        for _ in range(args.events):
            r=np.random.uniform() # train/val/test
            # below are only function parameters
            t1=np.random.uniform(rngT1[0],rngT1[1])
            rc2=np.random.uniform(rngRC2[0],rngRC2[1])

            # 2nd peak
            t1b=np.random.uniform(rngT1b[0],rngT1b[1])
            rc2b=np.random.uniform(rngRC2[0],rngRC2[1])

            pV=np.array([u0,t1,u1,dt,rc1,rc2,t1b,rc2b]) # parameters
            fV=uoff+spikeFunc_expo2(u0,t1,u1,dt,rc1,rc2) #function
            fV+=spikeFunc_expo2(u0,t1b,u1*.7,dt,rc1,rc2b) #function-b

            fVn=func_norm(fV,norm='2log')
            # assign domain
            for dn in domL:
                if r >domL[dn] : continue
                dataD['X_'+dn].append(fVn)
                dataD['Y_'+dn].append(pV)
                break
        for dom in domL:
            self.data[dom]=[np.array(dataD['X_'+dom]),np.array(dataD['Y_'+dom])]

        print('%d frames generated , elaT=%.1f sec'%(args.events,(time.time() - start)))

#............................
    def save_data_hdf5(self):

        for dom in self.data:
            outF=self.dataPath+'/f2f_'+dom+'.frames.yml'
            print('save data as hdf5:',outF)
            h5f = h5py.File(outF, 'w')
            X,Y=self.data[dom]
            print('h5-write X',dom,X.shape);
            h5f.create_dataset('X', data=X)
            print('h5-write Y',dom,Y.shape);
            h5f.create_dataset('Y', data=Y)
            h5f.close()
            xx=os.path.getsize(outF)/1048576
            print('closed  hdf5:',outF,' size=%.2f MB'%xx)


    #............................
    def load_data_hdf5(self,dom):
        inpF=self.dataPath+'/f2f_'+dom+'.frames.yml'

        h5f = h5py.File(inpF, 'r')
        xyL=[]
        for x in ['X','Y']:            
            obj=h5f[x][:]
            xyL.append(obj)
            print('read ',dom,x,obj.shape)
        self.data[dom]=xyL
        h5f.close()
        print('loaded ',self.data.keys())


    #............................
    def print_frames(self,dom,numFrm=1):
        X,Y=self.data[dom]
        print('sample of ',numFrm,' from dom=',dom)
        for i in range(numFrm):            
            print('i=',i,'par:',Y[i],'frm:',X[i][::5],'::5')
            

    #............................
    def train_model(self,args):
        X,Y=self.data['train']
        X_val,Y_val=self.data['val']
        if args.events>0:
            if len(X) > args.events : 
                X=X[:args.events]                
                print('reduced traing to %d events'%len(X))

        if args.verb==0:
            print('train silently epochs:',args.epochs)
     
        callbacks_list = []
        if args.earlyStopOn:
            earlyStop=EarlyStopping(monitor='val_loss', patience=2+args.arrIdx, verbose=1, mode='auto')
            callbacks_list.append(earlyStop)

        if args.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.name+'.weights.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1)
            callbacks_list.append(ckpt)


        print('\nTrain_model X:',X.shape, ' earlyStop=',args.earlyStopOn,' epochs=',args.epochs,' batch=',args.batch_size)
        startTm = time.time()
        hir=self.model['auto'].fit(X,X, callbacks=callbacks_list,
                 validation_data=(X_val,X_val),  shuffle=True,
                 batch_size=args.batch_size, nb_epoch=args.epochs, 
                 verbose=args.verb)
        self.train_hirD=hir.history
        #evaluate performance for the last epoch
        loss=self.model['auto'].evaluate(X_val,X_val)
        fitTime=time.time() - startTm
        print('\n End Validation  Loss:%.3f'%loss,', fit time=%.1f min'%(fitTime/60.))
        self.train_sec=fitTime



    #............................
    def save_model_full(self):
        modelA=self.model
        for name in modelA:
            fname=self.outPath+'/'+self.name+'_'+name+'.model.h5'     
            print('save model  to',fname)
            modelA[name].save(fname)


   #............................
    def load_model_full(self,custObj={}):
        self.model={}
        #print('ll',custObj)
        for name in ['auto', 'encoder', 'decoder']:
            fname=self.outPath+'/'+self.name+'_'+name+'.model.h5'     
            print('load model from ',fname)
            model=load_model(fname,custom_objects=custObj) # creates model from HDF5
            if name=='auto':  model.summary()
            self.model[name]=model
            self.model['onlyLoaded']=True
 
  
   #............................
    def eval_dom_loss(self,dom,numFrm=10):
        print('eval_dom_loss dom=',dom)

        X,Y=self.data[dom]
        if numFrm<0: numFrm=X.shape[0]
        Xp=X[:numFrm]
        Yp=Y[:numFrm]

        if 'vae' in self.name and 'onlyLoaded' in self.model:
            # VAE loaded from hd5 need more inputs - not sure why I need it?
            xsh,epssh=self.model['auto'].input_shape
            print('VAE predict inputs:',xsh,epssh)
            eps_inp=np.zeros((numFrm,epssh[1]))
            Xq=self.model['auto'].predict([Xp,eps_inp])
        else:
            Xq=self.model['auto'].predict(Xp)

        from sklearn.metrics import mean_absolute_error,  mean_squared_error

        mseV=[]; maeV=[];kldV=[]
        i=0
        fac=100
        for p,q in zip(Xp,Xq):
            i+=1
            mse=fac*mean_squared_error(p,q)
            mae=fac*mean_absolute_error(p,q)
            mseV.append(mse)
            maeV.append(mae)
            if i<5:
                print('i',i,len(p),len(q),mse,mae)

        self.eval={'Xp':Xp,'Yp':Yp,'Xq':Xq,'dom':dom}
        self.eval['mse']=np.array(mseV)
        self.eval['mae']=np.array(maeV)


    #............................
    def reveal_myFunc(self,dom,frId):
        print('reveal myFunc for dom=%s frameId=%d'%(dom,frId) )
        X=self.data['X_'+dom][frId]
        Y=self.data['Y_'+dom][frId]
        Z=np.reshape(Y,(-1,2))

        print('oz',Z.shape,Z)

        U=self.decoderM.predict(Z)
        print('oo',U.shape)
        U=U.flatten()
        print('oo-flat',U.shape)
        #print('ood',out1)
        
        for i in range(U.shape[0]):
            x=X[i][0]
            u=U[i]
            d=x-u
            print(i,x,u,d)
  

        
