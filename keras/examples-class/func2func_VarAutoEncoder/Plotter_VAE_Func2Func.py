__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Common  import Plotter_Common
import numpy as np

#............................
#............................
#............................
class Plotter_VAE_Func2Func(Plotter_Common ):

    def __init__(self,args):
        Plotter_Common.__init__(self,args)
        print('created',self.__class__.__name__)

#............................
    def plot_latent_z(self,deep,dom,figId=11):
        if figId in self.figL:
            self.plt.figure(figId)
        else:
            self.figL.append(figId)
            self.plt.figure(figId,facecolor='white', figsize=(6,3))
        X,Y=deep.data[dom]
        
        # extract & plot ground truth 2 params
        ax=self.plt.subplot(2,3,1)
        img=ax.scatter(Y[:,1],Y[:,5], alpha=.4, s=3**2)
        ax.set(title='ground truth',xlabel='par=t1',ylabel='par=RC2')

        # Compute the latent space
        zA= deep.model['encoder'].predict(X)
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        zNameA=['Z_mu','Z_sig']
        for i in range(len(zA)):
            ax=self.plt.subplot(2,3,i+2)
            zVec=zA[i] 
            ztxt=zNameA[i]
            img=ax.scatter(zVec[:, 0], zVec[:, 1], alpha=.4, s=3**2)
            ax.set(title='AE: '+ztxt+', dom='+dom, xlabel=ztxt+'_0',ylabel=ztxt+'_1')
            if i==0:
                ax.set_xlim(-3,3);   ax.set_ylim(-3,3);
            else:
                ax.set_xlim(0,1.3);   ax.set_ylim(0,1.3);
            ax.grid()

        return zA[0],Y
