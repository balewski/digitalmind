import os
import numpy as np
from matplotlib import cm as cmap
import socket  # for hostname
from tensorflow.contrib.keras.api.keras.utils import plot_model

import time
from sklearn.manifold import TSNE
from Util_Func2Func import func_norm

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_Common(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args):
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        if not os.path.exists(self.outPath):
            print('Aborting on start, missing output dir',args.outPath)
            exit(22)

    #............................
    def display_all(self,args,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/idx%d_%s_%s_f%d'%(self.outPath,args.arrIdx,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()

#............................
    def plot_model(self,deep,flag=0):
        #print('plot_model broken in TF=1.4, skip it'); return

        if 'cori' not in socket.gethostname(): return  # software not installed
        modelA=deep.model
        for name in modelA:
            fname=self.outPath+'/'+deep.name+'_'+name+'_graph.svg'
            plot_model(modelA[name], to_file=fname, show_layer_names=flag>0, show_shapes=flag>1)
            print('Graph saved as ',fname,' flag=',flag)


#............................
    def plot_data_phaseSpace(self,dataA,dom,idxL,norm='as-is',figId=6):

        self.figL.append(figId)
        X,Y=dataA[dom]
        fig=self.plt.figure(figId,facecolor='white', figsize=(8,5))
        nrow,ncol=2,3
        print('plot input for idx=',idxL)
        j=0
        timeV=range(len(X[0])-1)
        for idx in idxL:            
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            amplV=func_norm(X[idx],norm=norm)

            velV=[ amplV[i+1]-amplV[i] for i in range(len(amplV)-1) ]
            amplV=amplV[:-1]

            ax = self.plt.subplot(nrow, ncol, 1+j)
            tit='idx:%d par=%.2f , %.2f'%(idx,Y[idx][1],Y[idx][5]) # t1,rc2            
            #ax.plot(timeV,amplV)
            ax.set(title=tit,ylabel='ampl', xlabel='speed')
            ax.plot(velV,amplV)
            ax.scatter(velV,amplV,c=timeV,cmap='Accent')
            #ax.axhline(0, color='blue', linestyle='--')
            #ax.set_ylim(-1,1)
            ax.grid()
            j+=1
#............................
    def plot_data_vsTime(self,dataA,dom,idxL,norm='as-is',figId=7):
        self.figL.append(figId)
        X,Y=dataA[dom]
        fig=self.plt.figure(figId,facecolor='white', figsize=(8,5))
       
        timeV=range(len(X[0]))
        nrow,ncol=2,3
        print('plot input for idx=',idxL)
        j=0
        for idx in idxL:            
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            amplV=func_norm(X[idx],norm=norm)

            ax = self.plt.subplot(nrow, ncol, 1+j)
            tit='idx:%d par=%.2f , %.2f'%(idx,Y[idx][1],Y[idx][5]) # t1,rc2
            ax.set(title=tit,ylabel='ampl', xlabel='time')
            ax.plot(amplV)
            ax.scatter(timeV,amplV,c=timeV,cmap='Accent')
            ax.axhline(0, color='blue', linestyle='--')
            #ax.set_ylim(-1,1)
            
            ax.grid()
            j+=1

#............................
    def plot_neuron_volt_speed(self,XY,idx,norm='as-is',figId=0):
        if figId==0: figId=100+idx
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(8,4))

        amplV=func_norm(XY[0][idx],norm=norm)

        Y=XY[1][idx]
        nrow,ncol=1,2
        print('plot neuron for ...',idx)

        timeV=range(len(amplV))
        strOut='%d ypar: '%len(Y)
        for y in Y:
            strOut+='%.2g,'%y
            if len(strOut) >80: break
            tit=strOut
            #    print('strOut',strOut)

        # ..........  ampl-time
        #  grid is (yN,xN) - y=0 is at the top,  so dumm        
        ax = self.plt.subplot(nrow, ncol, 1)

        ax.set(title=tit,ylabel='ampl', xlabel='time')
        ax.plot(timeV,amplV,label='inp')
        ax.scatter(timeV,amplV,c=timeV,cmap='Accent')
        ax.grid()
        ax.legend(loc='best')

        #......... phasespace
        ax = self.plt.subplot(nrow, ncol, 2)
        velV=[ amplV[i+1]-amplV[i] for i in range(len(amplV)-1) ]
        amplV=amplV[:-1]
        timeV=timeV[:-1]
        ax.set(title='phasepace',ylabel='ampl', xlabel='speed')
        ax.plot(velV,amplV)
        ax.scatter(velV,amplV,c=timeV,cmap='Accent')
        ax.grid()

#............................
    def plot_train_history(self,deep,args,figId=10):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(7,5))
        
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((2,3), (1,0), colspan=3 )
        
        DL=deep.train_hirD

        tit1='%s, train %.1f min, drop=%.2f'%(deep.name,deep.train_sec/60.,args.dropFrac)
        ax1.set(ylabel='loss',title=tit1,xlabel='epochs')
        ax1.plot(DL['loss'],'.-.',label='train n=%d'%deep.data['train'][0].shape[0])
        ax1.plot(DL['val_loss'],'.-',label='val n=%d'%deep.data['val'][0].shape[0])
        ax1.legend(loc='best')
        #ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')
        
    
#............................
    def plot_dom_residua(self,evalD,tit0,idxL,norm='as-is',figId=8):
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(11,6))


        Xp=func_norm(evalD['Xp'],norm=norm)
        Xq=func_norm(evalD['Xq'],norm=norm)
        Yp=evalD['Yp']

        t_bin=range(Xp.shape[1])

        nrow,ncol=2,4
        print('plot input for idx=',idxL)
        j=0
        for i in idxL:            
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j)
            tit='i:%d Yp=%.2f , %.2f'%(i,Yp[i][1],Yp[i][5])            
            ax.set(title=tit)
            ax.plot(Xp[i],label='inp')
            ax.plot(Xq[i],label='out')
            ax.fill_between(t_bin,Xp[i]-Xq[i],label='diff',facecolor='red')
            #ax.set_ylim(-.5,0.9)
            ax.grid()
            ax.legend(loc='top left')
            ax.axhline(0, color='blue', linestyle='--')
            if j==0:  ax.text(1,0.1,tit0,rotation=45)
            j+=1


#............................
    def plot_dom_loss(self,deep,figId=11):
        self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,5))
        nameL=['mse','mae'] # 'kld',
        nrow,ncol=2,2
        print('plot dom loss, num frames',deep.eval['mse'].shape)
        j=1
        for name in nameL:
            ax = self.plt.subplot(nrow, ncol, j)
            ax.set(title='frame loss=100*'+name,ylabel='frames')
            n, bins, patches=ax.hist(deep.eval[name], 50)
            ax.grid()
            ax.set_yscale('log')

            #ax.axhline(0, color='blue', linestyle='--')
            j+=1
        #return    
        ax = self.plt.subplot(nrow, ncol, j)  
        hb = ax.hist2d(deep.eval['mae'],deep.eval['mse'] , bins=20,  cmin=1, cmap=cmap.rainbow)
        cb = fig.colorbar(hb[3], ax=ax)
        ax.grid()
        ax.set(title='loss correlation',xlabel='100*mae',ylabel='100*mse')

    #............................
    def plot_worst_loss(self,deep,maeThr=0.2,figId=12):

        frameA=deep.eval['mae']
        # find index of worst frames
        idxL=np.nonzero(frameA >maeThr) 
        nBad=len(idxL[0])
        print('worst mseThr=',maeThr,' num frames=',nBad,' of ', frameA.shape)
        
        if nBad <2 : return
        worstD={}
        worstD['Xp']=deep.eval['Xp'][idxL]
        worstD['Yp']=deep.eval['Yp'][idxL]
        worstD['Xq']=deep.eval['Xq'][idxL]
        self.plot_dom_residua(worstD,'maeThr >%.1f'%maeThr,range(8),figId=figId) 

        


#............................
    def plot_latent_TSNE(self,zTens,Y,figId=12):
        print('do TSNE, zTensor:',zTens.shape,' Y:',Y.shape)

        time_start = time.time()
        tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
        tsne_results = tsne.fit_transform(zTens)
        print ('t-SNE done1 ! Time elapsed: %.1f seconds, figId=%d'%(time.time()-time_start,figId))

        fig=self.plt.figure(figId,facecolor='white', figsize=(12,8))
        self.figL.append(figId)
        nrow,ncol=3,3

        df_x2 = tsne_results[:,0]
        df_y2 = tsne_results[:,1]

        ax=self.plt.subplot(nrow,ncol,1)
        img = ax.hexbin(df_x2, df_y2,cmap='Accent',gridsize=30)
    
        ax.set(title='%dD to TSNE colored by:'%(zTens.shape[1]))
        ax.set_axis_off()
        self.plt.colorbar(img).set_label('frame count')


        parIdxL=[1,5,6,7]
        j=2
        for iPar in parIdxL:
            df_lbl=Y[:,iPar]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            img=ax.scatter(df_x2,df_y2, alpha=.4, s=3**2 ,c=df_lbl, cmap='cool')

            self.plt.colorbar(img).set_label(' Y[%d]-parameter'%iPar)
            ax.set(title='%dD to TSNE colored by'%(zTens.shape[1]))
            ax.set_axis_off()

        return
 
