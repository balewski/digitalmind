import numpy as np

#............................
def func_norm(fV,norm='fixMe'):
        #  amplitude range: [-70,+20] :exp<-->log: [0.1,0.9]
        
        off=72.
        fac=5
        
        if norm=='2log':
            f1=fV+off
            f2=np.log(f1)
            f3=f2/fac
            #print('aa-log',f1.shape,f3[0],f2[0],f1[0],fV[0])
        elif norm=='2exp':
            f1=fV*fac
            f2=np.exp(f1)
            f3=f2-off
            #print('aa-exp',f1.shape,f3[0],f2[0],f1[0],fV[0])
        elif norm=='as-is':
            f3=fV
        else:
            print('bad func transform',end)
            bad_bad
        return f3
        


#............................
def spikeFunc_expo2(u0,t1,u1,dt,rc1,rc2,tBins=range(402)):
        """ RC charge/discharge model """
        nt=len(tBins)

        #print('spikeFunc pars:',t1,u1,dt,rc1,rc2,' nt=',nt)
        t2=t1+dt
        assert u0<u1
        assert t1>0 ;   assert dt>0 ;
        assert t2<nt*.8 ; # allow enugh time to see the decay
        assert rc1>0; assert rc2>0
        assert u1>0

        f0=np.exp(-dt/rc1)
        fact= u1/(1-f0)
        #print('f0:',f0,' fact:',fact)
        uc=[]
        for t in tBins:
            if t<t1 :
                u=u0                            
            elif t < t2:
                f1= np.exp( (t - t1-dt)/rc1)
                u=u0+fact*(f1-f0)
            else:
                u=u0+u1*np.exp(-(t - t2)/rc2)
            uc.append(u)
        
        #print('uc len',len(uc))
        # smooth with simple kernel
        ucs=[]
        for i in range(1,nt-1):            
            u=uc[i-1]*0.2 + uc[i]*0.6 + uc[i+1]*0.2
            ucs.append(u)
        #print('Uc:',uc)
        return np.array(ucs)
