__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Deep_Common  import Deep_Common

from keras import backend as K
from keras import losses 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout, LSTM
from keras.models import Model,  load_model

import numpy as np

#............................
#............................
#............................
class Deep_AE_Func2Func(Deep_Common ):

    def __init__(self,args):
        Deep_Common.__init__(self,args)
        print('created',self.__class__.__name__)

    #............................
    def build_model(self,args):  
        X,Y=self.data['train']
        time_dim=X.shape[1]        
        latent_dim=4
        dens_dim = 50
        dropFrac=args.dropFrac
        
        # - - - Assembling model 
        x = Input(shape=(time_dim,),name='inp_pulse')
        print('build_model inp1:',x.get_shape())
        h = Dense(dens_dim, activation='relu',name='enc1')(x)
        h=Dropout(dropFrac,name='drop_%.2f'%dropFrac)(h)
        h = Dense(dens_dim, activation='relu',name='enc2')(h)
        encoded = Dense(latent_dim, activation='linear',name='zip')(h)

        # "decoded" is the lossy reconstruction of the input
        h = Dense(dens_dim, activation='relu',name='dec1')(encoded)
        h = Dropout(dropFrac)(h)
        h = Dense(dens_dim, activation='relu',name='dec2')(h)
        decoded = Dense(time_dim, activation='sigmoid',name='out_img')(h)

        #1) this model maps an input to its reconstruction
        autoencoderM = Model(x, decoded)

        #2) this model maps an input to its encoded representation
        encoderM = Model(x, encoded)

        # create a placeholder for an encoded (zip-dimensional) input
        z = Input(shape=(latent_dim,),name='inp_z')

        # retrieve last  layer of the autoencoder model
        h = autoencoderM.layers[-4](z)
        h = autoencoderM.layers[-3](h)
        h = autoencoderM.layers[-2](h)
        h = autoencoderM.layers[-1](h)

        #3) this model maps encoded representation to input image
        decoderM = Model(z, h)

        #myLoss='mean_absolute_error'
        myLoss='mae'
        autoencoderM.compile(optimizer='adadelta', loss=myLoss)
        self.model={'auto':autoencoderM, 'encoder':encoderM, 'decoder':decoderM}

        # - - -  Model completed
        autoencoderM.summary() # will print
        print('\nFull auto-encoder loss=%s layers=%d , params=%.1f K'%(myLoss,len(autoencoderM.layers),autoencoderM.count_params()/1000.))

        print('\nEncoder layers=%d , params=%.1f K'%(len(encoderM.layers),encoderM.count_params()/1000.))
        if args.verb>1:
            encoderM.summary() # will print
            
        print('\nDecoder layers=%d , params=%.1f K'%(len(decoderM.layers),decoderM.count_params()/1000.))
        if args.verb>1:
            decoderM.summary() # will print

