#!/usr/bin/env python
""" 
produce training data, save as hd5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Seq2Func data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument('--design', dest='modelDesign', choices=['vae','ae'],
                         default='ae',
                         help=" model design of the network")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")


    parser.add_argument("-n", "--events", type=int, default=100,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='f2f_'+args.modelDesign
    args.arrIdx=0
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.modelDesign=='vae':
    from Plotter_VAE_Func2Func import Plotter_VAE_Func2Func as Plotter_F2F
    from Deep_VAE_Func2Func import Deep_VAE_Func2Func as Deep_F2F
elif args.modelDesign=='ae':
    from Plotter_AE_Func2Func import Plotter_AE_Func2Func as Plotter_F2F
    from Deep_AE_Func2Func import Deep_AE_Func2Func as Deep_F2F
else:
    assert 2==4 # implement it

ppp=Plotter_F2F (args )
deep=Deep_F2F(args)

deep.generate_pulses(args)
deep.print_frames('train',numFrm=1)
deep.save_data_hdf5()
plotNorm='2exp'
ppp.plot_data_vsTime(deep.data,'train',range(6),norm=plotNorm)
ppp.plot_data_phaseSpace(deep.data,'train',range(6),norm=plotNorm)

ppp.plot_neuron_volt_speed(deep.data['train'],5,norm=plotNorm)
ppp.display_all(args,'form')
