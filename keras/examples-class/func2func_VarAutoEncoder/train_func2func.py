#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train func2func',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('--design', dest='modelDesign', choices=['vae','ae'],
                         default='vae',
                         help=" model design of the network")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="fitting epoch")

    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.05,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", dest='earlyStopOn',
                         action='store_true',default=False,help="enable early stop")
    parser.add_argument( "--checkOn", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    args = parser.parse_args()
    args.prjName='f2f_'+args.modelDesign
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

if args.modelDesign=='vae':
    from Plotter_VAE_Func2Func import Plotter_VAE_Func2Func as Plotter_F2F
    from Deep_VAE_Func2Func import Deep_VAE_Func2Func as Deep_F2F
elif args.modelDesign=='ae':
    from Plotter_AE_Func2Func import Plotter_AE_Func2Func as Plotter_F2F
    from Deep_AE_Func2Func import Deep_AE_Func2Func as Deep_F2F
else:
    assert 2==4 # implement it

ppp=Plotter_F2F (args )
deep=Deep_F2F(args)
deep.load_data_hdf5('train')
deep.load_data_hdf5('val')
deep.print_frames('train',numFrm=1)

#ppp.plot_data(deep.data,'train',range(6)) ; ppp.display_all(args,'xx')

deep.build_model(args)

#deep.reveal_myFunc('train', frId=2)  # dumps one input frame and the decoder output

ppp.plot_model(deep,1) # depth:0,1,2

deep.train_model(args) 
deep.save_model_full() 

deep.eval_dom_loss('val',-1) # full pass
ppp.plot_dom_residua(deep.eval,'val-sample',range(8)) 
ppp.plot_dom_loss(deep)

ppp.plot_train_history(deep,args,figId=10)
ppp.plot_latent_z(deep,'val',figId=10)

ppp.display_all(args,'train')


