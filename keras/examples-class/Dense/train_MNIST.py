#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_MNIST import Plotter_MNIST
from Deep_MNIST import Deep_MNIST

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of MNIST data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
 
    parser.add_argument("--project",
                        default='jan_mnist1',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="fitting epoch")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", dest='earlyStopOn',
                         action='store_true',default=False,help="enable early stop")
    parser.add_argument( "--checkOn", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

# CPUs are used via a "device" which is just a threadpool
if args.nCpu>0:
    import tensorflow as tf
    tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
    print('M: restrict CPU count to ',args.nCpu)


ppp=Plotter_MNIST(args )
deep=Deep_MNIST(args)
deep.load_input_hdf5(['train','val'])
deep.print_input('val',1)
ppp.plot_digits(deep,'val',range(3),0)

deep.build_model(args) 
deep.train_model(args) 
deep.save_model_full() 
deep.model_evaluate('val')  # kind of useless - too little info 
deep.model_predict('val')

ppp.plot_train_history(deep,args)
ppp.plot_confusionMatrix(deep,'val')
ppp.display_all(args,'train')


