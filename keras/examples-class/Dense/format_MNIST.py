#!/usr/bin/env python
"""  read raw input data
sanitize, randomize, pad/clip/1hot, 
write 9 tensors: (train,val,test) * (X,Y,Yhot) in hd5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_MNIST import Plotter_MNIST
from Deep_MNIST import Deep_MNIST

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of MNIST data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("--project",
                        default='jan_mnist1',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument( "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_MNIST(args )

deep=Deep_MNIST(args)
deep.split_raw_input(0.7)
deep.print_input('val',2)
deep.save_input_hdf5()
ppp.plot_digits(deep,'val',range(3),0)

ppp.display_all(args,'form')
