import os
import numpy as np
from matplotlib import cm as cmap


__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_MNIST(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args,xinch=6,yinch=7):
        args.outPath='./'
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        if not os.path.exists(self.outPath):
            print('Aborting on start, missing output dir',args.outPath)
            exit(22)

    #............................
    def display_all(self,args,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/idx%d_%s_%s_f%d'%(self.outPath,args.arrIdx,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()


#............................
    def plot_digits(self,deep,name,idxL,irow,ncol=3, auxL=[],figId=7):
        assert 'X_'+name in deep.data

        if figId not in self.figL:
            self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,6))


        X=deep.data['X_'+name]
        Y=deep.data['Y_'+name]       
        nrow=3
        print('plot input for idx=',idxL)
        j=0
        for i in idxL:            
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j+irow*ncol)
            tit='i:%d dig=%d'%(i,Y[i])
            if len(auxL)>0 : tit+=' pred=%d'%auxL[j]
            ax.set(title=tit)
            ax.imshow(X[i], cmap=self.plt.get_cmap('gray'))
            j+=1

#............................
    def plot_train_history(self,dee,args,figId=10):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(13,6))
        
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((3,3), (1,0), colspan=2 )
        ax2 = self.plt.subplot2grid((3,3), (2,0), colspan=2,sharex=ax1 )

        DL=dee.train_hirD

        tit1='%s, train %.1f min, nCpu=%d, drop=%.1f'%(dee.name,dee.train_sec/60.,args.nCpu,args.dropFrac)
        tit2='arrIdx=%d, earlyStop=%d, end val. acc=%.3f'%(args.arrIdx,args.earlyStopOn,dee.acc)
        

        ax1.set(ylabel='loss',title=tit1)
        ax1.plot(DL['loss'],'.-.',label='train')
        ax1.plot(DL['val_loss'],'.-',label='valid')
        ax1.legend(loc='best')
        #ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')
        
        ax2.set(xlabel='epochs',ylabel='accuracy',title=tit2)
        ax2.plot(DL['acc'],'.-',label='train')
        ax2.plot(DL['val_acc'],'.-',label='valid')
        ax2.legend(loc='bottom right')
        ax2.grid(color='brown', linestyle='--',which='both')
        
  #............................
    def plot_confusionMatrix(self,deep,dom,figId=11):
       self.figL.append(figId)
       fig=self.plt.figure(figId,facecolor='white', figsize=(10,5))
       nrow,ncol=1,2
       #ax = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=2 )
       ax = self.plt.subplot(nrow, ncol, 1)
       ax1 = self.plt.subplot(nrow, ncol, 2)
       catgL= deep.catgL
       cm=deep.CM[dom]
       cm1=np.sum(cm,axis=0)
       cm2=np.sum(cm,axis=1)
       cmSum=sum(cm1)
       print(cmSum,'got cm')

       print('cm1',cm1)
       print('cm2',cm2)

       tick_marks = np.arange(len(catgL))

       ax1.bar(tick_marks,cm1,align='center',label='prediction',color='salmon')
       ax1.plot(tick_marks,cm2,'*b',label='truth',markersize=10.0)
       ax1.legend(loc='lower center')
 
       ax1.set(xlabel='Label',ylabel='events', title='dom=%s  %.1fk '%(dom,cmSum/1000.))
       ax1.set_xticks(tick_marks)
       ax1.set_xticklabels(catgL)
       ax1.grid(True)

       #-----
       cm=len(catgL)*cm/cmSum
       #print('got norm cm',cm)
       title='Confusion Mtrx (sum=%d)'%len(catgL)
       #catgL=['%d'%x for x in deep.primeL]

       ax.imshow(cm,origin='lower', cmap=cmap.cool)  #cool Wistia
       ax.set(title=title,ylabel='True label', xlabel='Predicted label')

 
       print('catgL',catgL,tick_marks)
       ax.set_xticks(tick_marks)
       ax.set_yticks(tick_marks )
       ax.set_xticklabels(catgL)
       ax.set_yticklabels(catgL)

       fmt = '.2f'
       #fmt = 'd' 
       thresh = cm.max() / 2.
       for i in range(cm.shape[0]):
           for j in  range(cm.shape[1]):
               ax.text(j, i, format(cm[i, j], fmt),
                       horizontalalignment="center",
                       color="white" if cm[i, j] > thresh else "black")

