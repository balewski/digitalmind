import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()

from keras.datasets import mnist
from keras import utils as np_utils
from keras.models import Sequential, load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint #ProgbarLogger
from keras.layers import Dense, Dropout, Flatten

import numpy as np
import h5py

from sklearn.metrics import confusion_matrix

print('deep-libs imported elaT=%.1f sec'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Deep_MNIST(object):
    """Contains all operations specific for this project"""

    def __init__(self,args):
        self.name=args.prjName
        print(self.__class__.__name__,', prj:',self.name)

#............................
    def split_raw_input(self,frac):
        print('read raw data')
        # Load pre-shuffled MNIST data into train and test sets
        (X, Y), (Xt, Yt) = mnist.load_data()
        nDig=np.unique(Y).shape[0] # there are 10 image classes
        print('raw data shape X:',X.shape,' Y:',Y.shape,' nDig=',nDig)
        nTrain=int(len(X) *frac)
        self.data={}

        self.data['X_val']=X[nTrain:].astype('float32')/256.
        self.data['Y_val']=Y[nTrain:]
        self.data['Yhot_val']=np_utils.to_categorical(self.data['Y_val'],nDig)
        

        self.data['X_train']=X[:nTrain].astype('float32')/256.
        self.data['Y_train']=Y[:nTrain]
        self.data['Yhot_train']=np_utils.to_categorical(self.data['Y_train'],nDig)

        self.data['X_test']=Xt.astype('float32')/256.
        self.data['Y_test']=Yt
        self.data['Yhot_test']=np_utils.to_categorical(self.data['Y_test'],nDig)
        print('formated data shapes:',end='')
        for x in self.data:
            if 'Y' in x: print(x,self.data[x].shape,', ',end='')
        print()

#............................
    def save_input_hdf5(self):
        outF=self.name+'.data.h5'
        print('save data as hdf5:',outF)
        h5f = h5py.File(outF, 'w')
        for x in self.data:
            xobj=self.data[x]
            print('h5-write ',x,type(xobj),xobj.shape,xobj.dtype);
            h5f.create_dataset(x, data=self.data[x])
        #for x in h5f.keys(): 
        #    print(x,self.data[x].shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%xx)

#............................
    def load_input_hdf5(self,dataL):
        inpF5=self.name+'.data.h5'
        print('clear old, load new data from hdf5:',inpF5)
        h5f = h5py.File(inpF5, 'r')
        self.data={}
        for x in ['X','Y','Yhot']:
            for y in dataL:
                ddN=x+'_'+y
                self.data[ddN] = h5f[ddN][:]
                print('read ',ddN,self.data[ddN].shape)
        h5f.close()

    #............................
    def print_input(self,name,k):
        nameX='X_'+name
        
        assert nameX in self.data
        X=self.data[nameX]
        Y=self.data['Y_'+name]
        print('sample of ',nameX)
        for i in range(k):
            print('\nidx=%d digit=%d, X-data:'%(i,Y[i]))
            print(X[i][10:13])
            print('\nY-data:', Y[i])
            print('\nYhot-data:', self.data['Yhot_'+name][i])


    #............................
    def build_model(self,args):
        print('build_model:')
        model = Sequential()
        model.add(Dense(32, activation='relu', input_shape=(28,28,)))
        #print (model.output_shape) 
        model.add(Dropout(args.dropFrac))
        model.add(Dense(16, activation='relu'))
        model.add(Dropout(args.dropFrac))
        model.add(Flatten()) # sth is messed up with dims, ignore for now
        model.add(Dense(10, activation='softmax'))
        model.summary()
 
        model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
        self.model=model


    #............................
    def train_model(self,args):
        X=self.data['X_train']; Y= self.data['Yhot_train']
        X_val=self.data['X_val']; Y_val= self.data['Yhot_val']
        nDig=Y[0].shape[0]
        self.catgL=range(nDig)

        if args.events>0:
            if len(Y) > args.events : 
                X=X[:args.events]
                Y=Y[:args.events]
                print('reduced traing to %d events'%len(Y))
        if args.verb==0:
            print('train silently epochs:',args.epochs)
     
        callbacks_list = []
        if args.earlyStopOn:
            earlyStop=EarlyStopping(monitor='val_loss', patience=2+args.arrIdx, verbose=1, mode='auto')
            callbacks_list.append(earlyStop)

        if args.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.name+'.weights.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1)
            callbacks_list.append(ckpt)


        print('\nTrain_model X:',X.shape, ' earlyStop=',args.earlyStopOn,' epochs=',args.epochs,' batch=',args.batch_size)
        startTm = time.time()
        hir=self.model.fit(X,Y, callbacks=callbacks_list,
                 validation_data=(X_val,Y_val),  shuffle=True,
                 batch_size=args.batch_size, nb_epoch=args.epochs, 
                 verbose=args.verb)
        self.train_hirD=hir.history
        #evaluate performance for the last epoch
        [loss,acc]=self.model.evaluate(X_val,Y_val)
        fitTime=time.time() - startTm
        print('\n End Validation Accuracy:%.3f'%acc, ', Loss:%.3f'%loss,', fit time=%.1f sec'%(fitTime))
        self.train_sec=fitTime
        self.acc=acc

    #............................
    def model_evaluate(self,name):
        nameX='X_'+name
        nameY='Yhot_'+name        
        print('model_evaluate ',nameX,' using current model')
        score = self.model.evaluate(self.data[nameX],self.data[nameY], verbose=0)
        print('score loss, acc=',score)

    #............................
    def model_predict(self,name,k=100):
        nameX='X_'+name
        nameY='Y_'+name        
        print('predict data',nameX,' using current model')
        YscoreCatg = self.model.predict(self.data[nameX], verbose=0)
        Ypred=np.argmax(YscoreCatg,axis=1)
        print('ScoreCatg',YscoreCatg.shape,Ypred.shape, 'list only bad pred:')
        Ytrue = self.data[nameY]
        CM=confusion_matrix(Ytrue,Ypred)
        print('ConfMtrx\n',CM)
        self.CM={name:CM}
        
        mL=[]
        badL=[]
        predL=[]
        nok=0
        for i in range(k):
            m=Ytrue[i]-Ypred[i]
            if m==0: 
                nok+=1
            else:
                badL.append(i)
                predL.append(Ypred[i])
                #print(i,Ytrue[i],Ypred[i],m,nok)
            mL.append(m)
            
        print(len(badL),'bad of ',k,', bad:predL=',end='')
        for x in zip(badL,predL): print(x,end=',  ')
        print()

        return badL,predL

    #............................
    def save_model_full(self):
        outF5m=self.name+'.model.h5'
        print('save model full to',outF5m)
        self.model.save(outF5m)
        return
        outF5w=self.name+'.weights.h5'
        print('save weights to',outF5w)
        self.model.save_weights(outF5w)
        print('save hdf5 done')
 
    #............................
    def load_model_full(self):
        try:
            del self.model
            print('delte old model')
        except:
            a=1

        outF5m=self.name+'.model.h5'
        print('load model full from',outF5m)
        self.model=load_model(outF5m) # creates mode from HDF5
        self.model.summary()
        return
        outF5w=self.name+'.weights.h5'
        print('load weights from',outF5w)
        self.model.load_weights(outF5w)
        print('save hdf5 done')
 

  
