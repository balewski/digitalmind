import numpy as np
from scipy import signal # for gauss function
from scipy.stats import stats # for Pearson correlation

nSpike=2 # select how many spikes you want
amplR=[0.5,0.4] # - avr,half-range of amplitudes
timeR=50 # half-range of time spread
spikeM=[ 100+120*i for i in range(nSpike)]
print('Util: spikeM:',spikeM,'amplR:',amplR,'timeR:',timeR,' nSpike=',nSpike)

#............................
def multiSpike_generator():

        # generate  params normalized to unity
        U=np.random.uniform(-0.5,0.5,size=2*nSpike)
         
        sla=0.08; slb=0.03
        nPar=2
        for j in range(nSpike):
                idx=j*nPar
                #print('iii',j,idx,U[idx:idx+nPar])
                fV1=oneSpikeFunc(U[idx:idx+nPar],spikeM[j],sla,slb) #,tBins=range(2200)) #FF
                if j==0: 
                        X=fV1
                else:
                        X+=fV1
        return X,U 
        

#............................
def oneSpikeFunc(U,tm,sla,slb,tBins=range(200+100*nSpike)): 
        """ asymetric trangle : u[0]-max ampl, u[1]-time center fact, sla,b-slopes"""
        nt=len(tBins)

        #print('oneSpikeFunc pars:',U[0],U[1],tm,sla,slb,' nt=',nt)

        F=[]

        u1=amplR[0] + amplR[1]*U[0]
        t1=tm+  timeR*U[1]
        dta=1./sla
        ta=t1-dta  ; assert ta>0
        dtb=1./slb
        tb=t1+dtb  ; assert tb<nt-1
        #print('ff',u1,ta,t1,tb)
        for t in tBins:
            #... spike component
            if t<ta :
                f=0                            
            elif t < t1:
                f= (t-ta)*sla 
            elif t < tb:
                f= (tb-t)*slb                
            else:
                f=0
            #print(t,f)
            F.append(f*u1)
        # smooth with simple kernel
        Fs=[]
        for i in range(1,nt-1):            
            u=F[i-1]*0.2 + F[i]*0.6 + F[i+1]*0.2
            Fs.append(u)
  
        #print('Fs:',Fs)
        
        return np.array(Fs)

#............................
def get_correlation(aV,bV):
        n=len(aV)
        assert n >2
        assert n == len(bV)
        rho,p_val=stats.pearsonr(aV,bV)
        #print(n,rho,p_val)
        # based on  https://stats.stackexchange.com/questions/226380/derivation-of-the-standard-error-for-pearsons-correlation-coefficient

        sigRho=np.sqrt( (1-rho*rho)/(n-2))
        #print('rho=',rho,'sigRho=',sigRho)
        return rho,sigRho


