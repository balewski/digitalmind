#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_4Dregr import Plotter_4Dregr
from Deep_4Dregr import Deep_4Dregr

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train func_invert',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('--design', dest='modelDesign', choices=['fc','fcX'],
                         default='fc',
                         help=" model design of the network")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-k", "--kfoldOffset", type=int, default=0,
                        help="decides which segments merge for training")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=15,
                        help="fitting epoch")

    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.20,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", dest='earlyStopOn',
                         action='store_true',default=False,help="enable early stop")
    parser.add_argument( "--checkOn", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    args = parser.parse_args()
    
    args.prjName='funcRegr_'+args.modelDesign
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_4Dregr(args)

deep.prep_labeled_input('val')
deep.prep_labeled_input('train')

ppp=Plotter_4Dregr (args,deep.metaD  )

deep.build_model(args)
ppp.plot_model(deep,1) # depth:0,1,2

deep.train_model(args)

deep.save_model_full()

ppp.plot_train_history(deep,args,figId=10)

# Compute latent space
dom='val'
X,U,Aux=deep.data[dom]
Z= deep.model.predict(X)
#ppp.plot_latent_z(Z,figId=11)
if U.shape[1]>1: # makes no sense for 2+ param regression
    deep.get_correl(U-Z)

ppp.plot_data_fparams(Z,'reco Z',figId=6)

tit='dom=%s idx=%d'%(dom,args.arrIdx)
ppp.plot_param_residua(U,Z,tit=tit)


ppp.display_all(args,'train')


