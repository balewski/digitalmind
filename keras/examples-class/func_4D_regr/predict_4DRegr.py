#!/usr/bin/env python
""" read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_4Dregr import Plotter_4Dregr
from Deep_4Dregr import Deep_4Dregr

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Mnist data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign', choices=['fc','fcX'],
                         default='fc',
                         help=" model design of the network")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")
    parser.add_argument("-k", "--kfoldOffset", type=int, default=0,
                        help="decides which segments merge for training")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='funcRegr_'+args.modelDesign
    args.arrIdx=0
 
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_4Dregr(args)
dom='test'

deep.prep_labeled_input(dom)
ppp=Plotter_4Dregr (args,deep.metaD  )

#1 ppp.plot_data_vsTime(deep.data,'test',range(6))  
#4 deep.print_frames('test',numFrm=1)
deep.load_model_full()

X,U,Aux=deep.data[dom]
Z= deep.model.predict(X)
#ppp.plot_latent_z(Z,figId=11)
if U.shape[1]>1: # makes no sense for 2+ param regression
    deep.get_correl(U-Z)

ppp.plot_latent_z(Z)
tit='dom=%s idx=%d'%(dom,args.arrIdx)
ppp.plot_param_residua(U,Z,tit=tit)
ppp.plot_param_summary(tit=tit )# only after _residua

ppp.display_all(args,'predict')



