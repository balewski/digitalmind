'''
Purpose:  produce valid spice circuit file based on input graph and randomized resistors
'''
from networkx import nx
import numpy as np
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class Random_Spicer(object):
#...!...!..................    
    def __init__(self, G, nameG,verb=1):
        self.G=G
        numNode=nx.number_of_nodes(G)
        for i in G.nodes():
            if G.in_degree(i)==0:
                rootN=i
                break
            bad_place1 # should never reaich it
        self.rootN=rootN
        self.verb=verb
        self.metaD={}
        self.metaD['graph']={'edges':nx.number_of_edges(G),'nodes':numNode,'rootNode':rootN,'name':nameG}
        self.metaD['circ']={'Vstim':30,'Re':100.,'Rx_range':[200.,500.]}
        print('Random_Spicer input graph w/:',self.metaD['graph'])
        print(' gen params:',self.metaD['circ'])

        # extract edges of the graph
        #self.graphEdges=np.array(G.edges())
        #print( 'edges.shape=',self.graphEdges.shape, 'dump:',self.graphEdges)
        #print(G.edges(data=True))
        #ok44
        np.random.seed() #  set the seed to a random number obtained from /dev/urandom 
        for i in range(int(50*np.random.rand())):
            np.random.rand()
            # now rnd is burned in
        print('seed test:',np.random.rand(4))
        

#...!...!..................    
    def addEdge(self,i,e,fd):
        fd.write('RE%d V%d V%d %.1f\n'%(i,e[0],e[1],self.metaD['circ']['Re']))

#...!...!..................    
    def addStimulusNode(self, n,Rx,fd):
        fd.write('VSTIM  VS  Gnd %.1f \n'%(self.metaD['circ']['Vstim']))
        fd.write('RS  V%d  VS  %.1f\n'%(n,Rx))

#...!...!..................    
    def addDendriteNode(self, n,Rx,fd):
        fd.write('RX%d V%d GND %.1f\n'%(n,n,Rx))

#...!...!..................    
    def genCircuit(self, spiceF,circId):
        G=self.G
        numNode=self.metaD['graph']['nodes']

        if self.verb>0: print('gen spice file=',spiceF)
        fd=open(spiceF, 'w')
        
        Rxr=self.metaD['circ']['Rx_range']
        dR=(Rxr[1]-Rxr[0])/2.
        aR=(Rxr[1]+Rxr[0])/2.
        RxUnit=np.random.uniform(-1,1, size=numNode)
        RxPhys=RxUnit*dR + aR
        
        if self.verb>1: 
            print('gen RX-U',RxUnit.shape,RxUnit)
            print('gen RX-P',RxPhys.shape,RxPhys)
                        
        # circ-preamble
        fd.write('.title circId=%d edges=%d nodes=%d\n'%(circId, nx.number_of_edges(G), nx.number_of_nodes(G)))

        
        for i,e in  enumerate(G.edges()):
            self.addEdge(i,e,fd)

        for n in G.nodes():
            if  G.in_degree(n)==0:
                self.addStimulusNode( n,RxPhys[n],fd)
            else:
                self.addDendriteNode(n,RxPhys[n],fd)
        
        # circ-closing+measure
        fd.write('.control \n op \n')
        measL=['V%d'%i for i in range(numNode)]
        measS=' '.join(measL)
        #print('measS',measS)
        fd.write('print %s \n'%measS)
        fd.write('quit\n.endc \n')

        fd.close()
        if self.verb>1: print('Spice circuit was created,\n$   ngspice -b %s\n'%spiceF)
        return RxPhys,RxUnit
