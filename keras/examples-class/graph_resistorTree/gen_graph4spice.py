#!/usr/bin/env python
import sys
import numpy as np

''' 
Create random tree of resistors  

uses graph generators
https://networkx.github.io/documentation/stable/reference/generators.html

Oct-2019 tested with 
module load tensorflow/gpu-1.15.0-rc1-py37

'''
import matplotlib.pyplot as plt
from networkx import nx
from networkx.drawing.nx_agraph import graphviz_layout

# - - - - - -  PARSER 
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-t", "--random_tree", type=int, default=None, help=" size, e.g.: 9")
parser.add_argument("-b", "--balanced_tree", type=int, nargs='+', default=[2,3] , help=" Branching factor + Height of the tree, e.g.: 2 2")
parser.add_argument("-f", "--full_rary_tree", type=int, nargs='+', default=None, help="tree w/ all non-leaf vertices have exactly r children, e.g.: 3 15  ")
parser.add_argument("-p", "--random_powerlaw_tree", type=float, nargs='+', default=None, help="n (int)  number of nodes + gamma (float)  Exponent of the power law, e.g.: 10 2.5 ")

parser.add_argument("-g", "--graph_name",  default='circTest', help="name of the graph")
parser.add_argument("-o", "--outPath",
                    default='out',help="output path for plots and tables")
parser.add_argument("-r", "--rootNode", type=int, default=2 , help=" where stimulus is applied")

args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
# - - - - - -  PARSER  END

#= = = = = = = = = =
'''
Spice code [parameters]
RA[x]  N[m] V[x]  100.
RB[x]  N[m] GND   500.
RX[x]  V[X] N[d]  200.
'''

#= = = = = = = = = =
def digraph2spice(G,rootN,fd):
    is_dg=nx.is_directed(G)
    numEdge=nx.number_of_edges(G)
    numNode=nx.number_of_nodes(G)

    print('\ndigraph2spice, is directed=',is_dg,' num edges=',numEdge,' nodes=',numNode, ' rootN=',rootN)
    assert is_dg

    Re=100
    Rs=50
    # preamble
    fd.write('.title graph:%s, edges:%d nodes:%d\n'%(args.graph_name,numEdge, nx.number_of_nodes(G)))
    

    for i,e in  enumerate(G.edges()):
        fd.write('RE%d V%d V%d %.1f\n'%(i,e[0],e[1],Re))

    for n in G.nodes():
        if  G.in_degree(n)==0:
            fd.write('VSTIM  VS  Gnd 10.0 \n')
            fd.write('RS  VS  V%d  %.1f\n'%(rootN,Rs))
        else:
            Rx=200+n*20
            fd.write('RX%d V%d GND %d\n'%(n,n,Rx))
            
    # closing
    fd.write('.control \n op \n')
    measL=['V%d'%i for i in range(numNode)]
    measS=' '.join(measL)
    #print('measS',measS)
    fd.write('print %s \n'%measS)
    fd.write('quit\n.endc \n')

        
G=None
if args.random_tree:
    G = nx.random_tree(args.random_tree)
elif args.full_rary_tree:
    inp=args.full_rary_tree
    assert len(inp)==2
    G=nx.full_rary_tree( inp[0],inp[1])
elif args.random_powerlaw_tree:
    inp=args.random_powerlaw_tree
    assert len(inp)==2
    G=nx.random_powerlaw_tree( int(inp[0]),inp[1], tries=1000)
elif args.balanced_tree:
    inp=args.balanced_tree
    assert len(inp)==2
    G=nx.balanced_tree( inp[0],inp[1])

if G==None :
    parser.print_help()
    exit(0)


# update graph name
args.graph_name+='_%dn'%nx.number_of_nodes(G)

print("source vertex {target:length, }")
v=0
spl = dict(nx.single_source_shortest_path_length(G, v))
print('v=',v, 'spl=',spl)

print("radius: %d" % nx.radius(G))
print("diameter: %d" % nx.diameter(G))
print("center: %s" % nx.center(G))
print("is_tree: %s" % nx.is_tree(G))

print('make graph directed ')
assert nx.number_of_nodes(G) >args.rootNode
G=nx.bfs_tree(G,args.rootNode)  # orient edges breadth-first-search starting at root

edgeL=G.edges()

#print('oriented edges',edgeL)
spiceF=args.outPath+'/'+args.graph_name+'.cir'

print('write spice file=',spiceF)
spFd=open(spiceF, 'w')
digraph2spice(G,args.rootNode,spFd)
spFd.close()
print('Spice circuit was created,\n$   ngspice -b %s\n'%spiceF)

graphF=args.outPath+'/'+args.graph_name+'.grf'
print(' write edgelist to:',graphF)
nx.write_edgelist(G, path=graphF, delimiter=":",data=['Ri'])


# plotting
pos=graphviz_layout(G)
nx.draw(G, with_labels=True,pos=pos)

edge_labels = nx.get_edge_attributes(G,'Ri')
#print('EL',edge_labels)
#nx.draw_networkx_edge_labels(G, pos,labels = edge_labels) # edge attribute+ID
nx.draw_networkx_edge_labels(G, pos, edge_labels)  # only edgeID

plotF=args.outPath+'/'+args.graph_name+'.png'
plt.savefig(plotF)

print('plot.show, saved to',plotF)
plt.show()
