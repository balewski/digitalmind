import os, time
import warnings
import socket  # for hostname
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
from Util_Func import write_yaml, read_yaml,  light_data_generator_dense,  light_data_generator_graph
from spektral.layers import GraphConv
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint , ReduceLROnPlateau,  Callback
from keras.layers import Dense, Dropout,   Input, concatenate, Lambda
from keras.layers import LeakyReLU 

from keras.optimizers import Adam

import keras.backend as K
import tensorflow as tf

import numpy as np
import h5py
import socket  # for hostname

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


#............................
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr)
        self.hir.append(float(lr))

#............................
#............................
#............................
class Deep_ResistorTree(object):
    #for multiple, independent "constructors" provide different class-methods, see below 

    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)

        for xx in [  self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

        print(self.__class__.__name__,'TF ver:', tf.__version__,', prj:',self.prjName)


    # alternative constructors
    @classmethod #........................
    def formater(cls, args):
        print('Cnst:form')
        obj=cls(**vars(args))
        obj.metaD={} # add empty 
        return obj

    @classmethod #........................
    def trainer(cls, args):
        print('Cnst:train')
        obj=cls(**vars(args))
        obj.read_metaInp(obj.dataSplit)  # . . .  load metaD

        obj.train_hirD={'acc': [],'loss': [],'lr': [],'val_acc': [],'val_loss': []}
        # . . . load hyperparameters
        hparF='./hpar_'+obj.prjName+'.yaml'
        bulk=read_yaml(hparF)
        obj.hparams=bulk

        obj.refine_input(args.dataSource)
        return obj

    @classmethod #........................
    def predictor(cls, args):
        print('Cnst:pred')
        obj=cls(**vars(args))
        obj.read_metaInp(obj.dataSplit)  # . . .  load metaD
        obj.refine_input(args.dataSource)
        
        return obj

#............................
    def read_metaInp(self,metaF):
        self.metaD=read_yaml(metaF)
        print('read metaInp:',metaF,len(self.metaD))
        # update prjName if not 'format'
        self.prjName+='_'+self.modelDesign
        # expand names of params for plotter
        self.metaD['parNames']=['R%d'%i for i in range(self.metaD['numNode']) ]
        # add placholder for generator
        self.metaD['gen']={}
        
#............................
    def refine_input(self,dataSource):
        if dataSource not in  self.metaD['sourceDir']:
            inpDir=dataSource
            print('use user defined data path:',inpDir)
        else:  # use shortcut stored in yaml file
            inpDir=self.metaD['sourceDir'][dataSource]
        self.inpDir=inpDir

#............................
    def build_model(self):
        hpar=self.hparams
        if self.verb: print('build_model hpar:',hpar)
        if 'dense' in self.modelDesign :
            model=self.build_model_dense(hpar)
        elif 'graph' in self.modelDesign :
            model=self.build_model_graph(hpar)
        else:
            print('ABORT, unimplemented model design:', self.modelDesign)
            exit(55)

        optName, initLR=hpar['optimizer']
        if self.verb: print('Opt + initLR',optName,initLR)

        if optName=='adam' :
            opt=Adam(lr=initLR)
        elif optName=='adadelta' :
            opt = Adadelta(lr=initLR)
            #Adadelta.__init__(lr=1.0,rho=0.95,epsilon=None,decay=0.0,**kwargs)
        else:
            print('invalid Opt:',optName)
            bad67

        model.compile(optimizer=opt, loss=hpar['lossName'])
        self.model=model
        # - - -  Model assembled and compiled

        print('\nSummary design=%s, layers=%d, params=%.1fk, loss=%s, inputs:'%(self.modelDesign, len(model.layers),model.count_params()/1000.,hpar['lossName']),model.input_shape)
        model.summary() # will print

        # create summary record
        rec={'hyperParams':hpar,
             'modelWeightCnt':int(model.count_params()),
             'modelLayerCnt':len(model.layers),
             'modelDesign' : self.modelDesign,
             'hostName' : socket.gethostname(),
        }
        self.sumRec=rec

#............................
    def build_model_graph(self,hpar):
        #numEdge=self.metaD['numEdge']
        N=self.metaD['numNode']
        F=1 # input features count is 1 per node
        dropFrac=self.dropFrac

        # - - - Assembling Graph CNN model
        nf_in = Input(shape=(N, F),name='nodeF')
        adj_in = Input(shape=(N, N),name='adjM')
        #nf_in = Input(shape=( F,),name='nodeF')
        #adj_in = Input(shape=(N, ),name='adjM')

        print('build_model nf_in:',nf_in.shape,' adj_in:',adj_in.shape)
        h=nf_in
        if dropFrac>0:  h = Dropout(dropFrac,name='in_drop')(h)

        '''
        Input:

        Node features of shape (n_nodes, n_features) (with optional batch dimension);
        Normalized Laplacian of shape (n_nodes, n_nodes) (with optional batch dimension); see spektral.utils.convolution.localpooling_filter.
        '''
        
        # .... GCNN  layers
        for i,dim in enumerate(hpar['gcnn_dims']):
            h = GraphConv(dim, activation='linear',name='fc%d'%i,
                          #kernel_regularizer=l2(l2_regrt)
                          )([h, adj_in])
            h =LeakyReLU(name='agc%d'%(i))(h)
            if dropFrac>0:  h = Dropout(dropFrac,name='drop%d'%i)(h)
            print('h_i :',i,h.get_shape())

        ho = GraphConv(1, activation='tanh',
                       #kernel_regularizer=l2(l2_regrt)
                       )([h, adj_in] )
        print('ho :',ho.get_shape())
        # Build model
        model = Model(inputs=[nf_in, adj_in], outputs=ho)
        return model

#............................
    def build_model_dense(self,hpar):
        numPar=self.metaD['numNode']

        # FC params
        dropFrac=self.dropFrac

        # - - - Assembling model
        xA = Input(shape=(numPar,),name='inpA')

        if self.verb: print('build_model  inpA:',xA.get_shape())#,' inpB:',xB.get_shape(), ' dropFrac=',dropFrac)

        h=xA
        if dropFrac>0:  h = Dropout(dropFrac,name='fdrop')(h)

        # .... FC  layers
        for i,dim in enumerate(hpar['fc_dims']):
            h = Dense(dim,activation='linear',name='fc%d'%i)(h)
            h =LeakyReLU(name='afc%d'%(i))(h)
            if dropFrac>0:  h = Dropout(dropFrac,name='drop%d'%i)(h)

        y= Dense(numPar, activation=hpar['lastAct'],name='out_%s'%hpar['lastAct'])(h)
        if hpar['lastAct']=='tanh':
            y = Lambda(lambda val: val*hpar['outAmpl'], name='scaleAmpl_%.1f'%hpar['outAmpl'])(y)

        if self.verb:  print('build_model: loss=',hpar['lossName'],' optName+LR=',hpar['optimizer'],' out:',y.get_shape())
        # full model
        model = Model(inputs=xA, outputs=y)
        return model

         
  #............................
    def train_model(self):
        callbacks_list = []
        batch_size=self.batch_size
        if self.verb: print('train_model assembly Generators, inpDir=%s, batch_size=%d'%(self.inpDir, batch_size))

        inpGenD={}
        for dom in ['train','val']:
            fnameL=self.metaD[dom]
            if 'dense' in self.modelDesign :
                inpGen=light_data_generator_dense(dom, self)
            elif 'graph' in self.modelDesign :
                inpGen=light_data_generator_graph(dom, self)
            else:
                print('ABORT, unimplemented generator for design:', self.modelDesign)
                exit(66)
            inpGenD[dom]=inpGen

        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)

        if self.earlyStopPatience>0:
            earlyStop=EarlyStopping(monitor='val_loss', patience=self.earlyStopPatience, verbose=self.verb, min_delta=2.e-4, mode='auto')
            callbacks_list.append(earlyStop)
            if self.verb: print('enabled EarlyStopping, patience=',self.earlyStopPatience)

        if self.checkPtOn==0:
            outF5w=self.outPath+'/'+self.prjName+'.weights_best.h5'
            chkPer=1
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=self.verb,period=chkPer)
            callbacks_list.append(ckpt)
            if self.verb: print('enabled ModelCheckpoint, period=',chkPer)

        if self.reduceLRPatience>0:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=self.hparams['reduceLR_factor'], patience=self.reduceLRPatience, min_lr=0.0, verbose=self.verb,min_delta=0.003)
            callbacks_list.append(redu_lr)
            if self.verb: print('enabled ReduceLROnPlateau, patience=',self.reduceLRPatience,',factor=',self.hparams['reduceLR_factor'])

        #batch_size=self.hparams['batch_size']
        epochs=self.epochs
        if self.verb: print('\nTrain_model numNode:',self.metaD['numNode'],' batch=',batch_size,'  modelDesign=', self.modelDesign,' epochs=',epochs)

        fitVerb=1   # prints live:  [=========>....] - ETA: xx s 
        if self.verb==2: fitVerb=1
        if self.verb==0: fitVerb=2  # prints 1-line summary at epoch end

        val_steps=max(1,self.steps//8)
        startTm = time.time()
        hir=self.model.fit_generator(
                inpGenD['train'],callbacks=callbacks_list, 
                epochs=epochs, max_queue_size=32,
                steps_per_epoch=self.steps,
                workers=1, use_multiprocessing=False,  
                shuffle=False, verbose=fitVerb,
                validation_data=inpGenD['val'],
                validation_steps=val_steps
        )

        fitTime=time.time() - startTm
        hir=hir.history
        usedEpochs=len(hir['loss'])
        earlyStopOccured=usedEpochs<epochs
        
        #print('FFF',earlyStop,epochs2, maxEpochs,totEpochs)

        for obs in hir:
            rec=[ float(x) for x in hir[obs] ]
            self.train_hirD[obs].extend(rec)

        # this is a hack, 'lr' is returned by fit only when --reduceLr is used
        if 'lr' not in  hir:
            self.train_hirD['lr'].extend(lrCb.hir)

        #self.train_hirD['stepTime']=inpGenD['train'].stepTime
        #self.train_hirD['steps_per_epoch']=inpGenD['train'].__len__()

        #report performance for the last epoch
        lossT=self.train_hirD['loss'][-1]
        lossV=self.train_hirD['val_loss'][-1]
        lossVbest=min(self.train_hirD['val_loss'])

        hpar=self.hparams
        if self.verb:
            print('\n End Val Loss=%s:%.3f, best:%.3f'%(hpar['lossName'],lossV,lossVbest),', %d usedEpochs, fit=%.1f min'%(usedEpochs,fitTime/60.),' hpar:',hpar)
        
        self.train_sec=fitTime

        # add info to summary
        rec=self.sumRec
        rec['earlyStopOccured']=int(earlyStopOccured)
        rec['fitMin']=fitTime/60.
        rec['usedEpochs']=usedEpochs
        rec['train_loss']=float(lossT)
        rec['val_loss']=float(lossV)
        rec['steps_per_epoch']=self.steps #train_hirD['steps_per_epoch']

    #............................
    def save_model_full(self):
        model=self.model
        fname=self.outPath+'/'+self.prjName+'.model.h5'     
        print('save model  to',fname)
        model.save(fname)
        return fname


   #............................
    def load_model_full(self,path='',verb=1):
        fname=path+'/'+self.prjName+'.model.h5'     
        print('load model from ',fname)
        self.model=load_model(fname,custom_objects={'GraphConv':GraphConv}) # creates model from HDF5) # creates model from existing HDF5
        if verb:  self.model.summary()


    #............................
    def load_weights_only(self,path='.'):
        start = time.time()
        inpF5m=path+'/'+self.prjName+'.weights_best.h5'  #_best
        print('load  weights  from',inpF5m,end='... ')
        self.model.load_weights(inpF5m) # restores weights from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))

 
