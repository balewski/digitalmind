#!/usr/bin/env python
""" 
read input hd5 tensors
train net
write net + weights as HD5

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_ResistorTree import Plotter_ResistorTree
from Deep_ResistorTree import Deep_ResistorTree
from Util_Func import write_yaml, test_generator

import argparse
def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign', default='dense1',help=" model design of the network")

    parser.add_argument("--seedWeights",
                        default=None,
                        help="seed weights only, after model is created")

    parser.add_argument("-o","--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("--dataSource",type=str,
                        default='data',help="path to data vault: cscratch, BB, local, etc")

    parser.add_argument("--dataSplit", default='dataSplit_circR_2e.yaml',
                        help="how to split data onto train/val/test")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-e", "--epochs", type=int, default=3,
                        help="num epochs")

    parser.add_argument("-b", "--batch_size", type=int, default=32,
                        help="training batch_size, default:hpar[]")

    parser.add_argument( "--steps", type=int, default=10,
                        help="overwtite natural steps per training epoch, default:hpar[]")

    parser.add_argument("--dropFrac", type=float, default=0.05,
                        help="drop fraction at all FC layers")

    parser.add_argument( "-s","--earlyStop", type=int,
                         dest='earlyStopPatience', default=10,
                         help="early stop:  epochs w/o improvement (aka patience), 0=off")
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLR", dest='reduceLRPatience', type=int, default=10,help="reduce learning patience at plateau")

    args = parser.parse_args()
    args.prjName='resistorTree'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_ResistorTree.trainer(args)
#1test_generator(deep)  ; ok66

gra=Plotter_ResistorTree(args,deep.metaD  )

#1 gra.plot_data_fparams(deep.data['val'][1],'true U',figId=6)

deep.build_model()

#1 ppp.plot_model(deep,1) # depth:0,1,2
if args.epochs >5 : deep.save_model_full() # just to get instant binary dump of the model definition

if args.seedWeights=='same' :  args.seedWeights=args.outPath
if args.seedWeights:
    deep.load_weights_only(path=args.seedWeights)

deep.train_model() 

gra.plot_train_history(deep,args,figId=10)

deep.save_model_full()
write_yaml(deep.sumRec, deep.outPath+'/'+deep.prjName+'.sum_train.yaml')


gra.display_all(args,'train')

# make predictions on CPU, in a separate code

