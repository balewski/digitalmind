#!/usr/bin/env python
'''
 generate random data matching  desired number of graph edges

'''

import ruamel.yaml  as yaml
import numpy as np
import time

from Util_Func import read_graph, write_yaml, shell_runSpice, write_data_hdf5
from Random_Spicer    import Random_Spicer

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-v","--verb",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

parser.add_argument("-g","--graph_name", 
                        default='circR_2e',help="input graph (edges)")
parser.add_argument("-o","--outPath",
                        default='out',help="output path for plots and tables")
parser.add_argument("-n", "--events", type=int, default=1,
                        help="number of (X,Y) pairs to be generated for fixed graph topology")

args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))



#=================================
#=================================
#  M A I N 
#=================================
#=================================
G=read_graph('data/'+args.graph_name+'.grf')
rasp=Random_Spicer(G,args.graph_name,verb=args.verb)

# overwrigth graph_name  (in 2 places)
args.graph_name='fake3_lin_2e'
rasp.metaD['graph']['name']=args.graph_name

# proceed w/ generation
spiceDataId=int(np.random.randint(0,4294967295, dtype=np.uint32))

numEdge=rasp.metaD['graph']['edges']
#numAux=2

#...Thorsten trick to not re-create large objects
RxUnitH5=np.zeros([args.events,numEdge],dtype=np.float32)
RxPhysH5=np.copy(RxUnitH5)  # not needed
VoltsH5=np.copy(RxUnitH5)
AuxL=[]

print('gen_spiceData for %d events, Y-shape:'%args.events,RxUnitH5.shape,'...\n')
startTm = time.time()
for iEve in range(args.events):
    circId=np.random.randint(0,4294967295, dtype=np.uint32)
    Vstim=1.1
    RxUnit=np.random.uniform(-1,1, size=numEdge)    
    Vtrue=RxUnit+np.random.normal(loc=0.0, scale=0.1, size=numEdge)
    Volts=np.zeros(numEdge,dtype=np.float32)
    Volts[0]=Vtrue[1];  Volts[1]=np.random.uniform(-1,1)
    Vhead=1.2
    Ihead=1.3
    # store data for hd5
    #print('RxUnit',RxUnit.shape,RxUnit)
    RxUnitH5[iEve]=RxUnit[:]
    VoltsH5[iEve]=Volts[:]
    AuxL.append([ Vhead, Ihead,circId,Vstim])

spiceErr=False
runTime=time.time()-startTm
AuxH5=np.array(AuxL)
numEve=AuxH5.shape[0]
print('Done running spice, spiceErr=',spiceErr,' num eve=%d elaT=%.1f min'%(numEve,runTime/60.))

rasp.metaD['spice']={'dataId':spiceDataId,'numEve':numEve,'spiceTime':runTime,'spiceFaled':spiceErr}
metaF=args.outPath+'/meta_%d_'%spiceDataId+args.graph_name+'.yaml'
write_yaml(rasp.metaD,metaF)

if AuxH5.shape[0]!=RxUnitH5.shape[0]:
    print('\n Warn: partial spice crash, shrink all HD5 to ',numEve)
    RxUnitH5=RxUnitH5[:numEve]
    RxPhysH5=RxPhysH5[:numEve]
    VoltsH5=VoltsH5[:numEve]

hd5F=args.outPath+'/spiceData_%d_'%spiceDataId+args.graph_name+'.hd5'
dataD={'RxUnit':RxUnitH5,'RxPhys':RxPhysH5,'Volts':VoltsH5,'Aux':AuxH5,'graphEdges':rasp.graphEdges}
write_data_hdf5(dataD,hd5F)

#print('RxPhysH5',RxPhysH5.shape,RxPhysH5)
#print('RxUnitH5',RxUnitH5.shape,RxUnitH5)
#print('graphEdges',rasp.graphEdges)
