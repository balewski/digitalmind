#!/usr/bin/env python
'''
randomizes resistors for pre-defined circuit graph
'''

import ruamel.yaml  as yaml
import numpy as np
import time

from Util_Func import read_graph, write_yaml, shell_runSpice, write_data_hdf5,  get_ZmU_correl
from Random_Spicer    import Random_Spicer

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-v","--verb",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

parser.add_argument("-g","--graph_name", 
                        default='circTest_6e',help="input graph (edges)")
parser.add_argument("-d","--dataPath",
                        default='data/',help=" path for graph+hd5 data files")
parser.add_argument("-o","--outPath",
                        default='out',help="output path for plots and tables")
parser.add_argument("-n", "--events", type=int, default=1,
                        help="number of (X,Y) pairs to be generated for fixed graph topology")

args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))



#=================================
#=================================
#  M A I N 
#=================================
#=================================
G=read_graph(args.dataPath+'/'+args.graph_name+'.grf')
rasp=Random_Spicer(G,args.graph_name,verb=args.verb)
spiceDataId=int(np.random.randint(0,4294967295, dtype=np.uint32))
Vstim=rasp.metaD['circ']['Vstim']

#Thorsten trick to not re-create large objects
numNode=rasp.metaD['graph']['nodes']
RxUnitH5=np.zeros([args.events,numNode],dtype=np.float32)
RxPhysH5=np.copy(RxUnitH5)
VoltsH5=np.copy(RxUnitH5)
AuxL=[]

print('gen_spiceData for %d events, Y-shape:'%args.events,RxUnitH5.shape,'...\n')
startTm = time.time()
for iEve in range(args.events):
    circId=np.random.randint(0,4294967295, dtype=np.uint32)
    spiceF=args.outPath+'/'+args.graph_name+'_id%d.cir'%circId
    RxPhys,RxUnit=rasp.genCircuit(spiceF,circId)
    spiceErr,Volts=shell_runSpice(spiceF,numNode, verb=args.verb-1,cleanup=args.verb==0 )
    if iEve%1000==0: print('work in %d of %d, elaT=%.1f min'%(iEve,args.events,(time.time()-startTm)/60.))
    if spiceErr: break
    # store data for hd5
    #print('RxUnit',RxUnit.shape,RxUnit)
    #print('GSD: Volts:',Volts)
    RxUnitH5[iEve]=RxUnit[:]
    RxPhysH5[iEve]=RxPhys[:]
    VoltsH5[iEve]=Volts[:]
    AuxL.append([ circId,Vstim])

runTime=time.time()-startTm
AuxH5=np.array(AuxL)
numEve=AuxH5.shape[0]
print('Done running spice, spiceErr=',spiceErr,' num eve=%d elaT=%.1f min'%(numEve,runTime/60.))

rasp.metaD['spice']={'dataId':spiceDataId,'numEve':numEve,'spiceTime':runTime,'spiceFaled':spiceErr}
metaF=args.dataPath+'/meta_%d_'%spiceDataId+args.graph_name+'.yaml'
write_yaml(rasp.metaD,metaF)

if AuxH5.shape[0]!=RxUnitH5.shape[0]:
    print('\n Warn: partial spice crash, shrink all HD5 to ',numEve)
    RxUnitH5=RxUnitH5[:numEve]
    RxPhysH5=RxPhysH5[:numEve]
    VoltsH5=VoltsH5[:numEve]

# re-scale Voltages
avrV=np.mean(VoltsH5,axis=0)
stdV=np.std(VoltsH5,axis=0)
print('avrV shape=',avrV.shape,'\navr=',avrV,'\nstd=',stdV)
#print('pre-V',VoltsH5)
VoltsH5=(VoltsH5-avrV)/stdV

#print('post-V',VoltsH5)
#avrV=np.mean(VoltsH5,axis=0)
#stdV=np.std(VoltsH5,axis=0)
#print('avrV shape=',avrV.shape,'values=',avrV,'std=',stdV)

# analyze cov matrix between Volatges (input X)

if args.events >1:
    assert numNode==VoltsH5.shape[1]
    nameL=[ 'V%d'%i for i in range(numNode)]
    print('nameL=',nameL)
    get_ZmU_correl(VoltsH5,'VoltsH5',nameL)

#metaD2={'parNames':nameL,'numEdge':numEdge,'autoScale':True}
#gra.plot_data_fparams(VoltsH5,'inp VoltsH5',figId=6,metaD=metaD2)


hd5F=args.dataPath+'/spiceData_%d_'%spiceDataId+args.graph_name+'.hd5'
dataD={'RxUnit':RxUnitH5,'RxPhys':RxPhysH5,'Volts':VoltsH5,'Aux':AuxH5,'avrVolts':avrV,'stdVolts':stdV} #,'graphEdges':rasp.graphEdges}
write_data_hdf5(dataD,hd5F)

#print('RxPhysH5',RxPhysH5.shape,RxPhysH5)
#print('RxUnitH5',RxUnitH5.shape,RxUnitH5)
#print('graphEdges',rasp.graphEdges)
