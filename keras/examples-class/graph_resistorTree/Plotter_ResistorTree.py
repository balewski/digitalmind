import numpy as np

from matplotlib import cm as cmap
from Util_Func import get_correlation

from Plotter_Backbone import Plotter_Backbone

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_ResistorTree(Plotter_Backbone):
    def __init__(self, args,metaD):
        Plotter_Backbone.__init__(self,args)

        self.maxU=1.4
        self.metaD=metaD

#............................
    def plot_model(self,deep,flag=0):
        #print('plot_model broken in TF=1.4, skip it'); return

        if 'cori' not in socket.gethostname(): return  # software not installed
        model=deep.model
        fname=self.outPath+'/'+deep.prjName+'_graph.svg'
        plot_model(model, to_file=fname, show_layer_names=flag>0, show_shapes=flag>1)
        print('Graph saved as ',fname,' flag=',flag)

            
#............................
    def plot_train_history(self,deep,args,figId=10):
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,4))
        
        nrow,ncol=3,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2, rowspan=2 )
        
        DL=deep.train_hirD
        nEpochs=len(DL['val_loss'])
        epoV=[i for i in range(1,nEpochs+1)]

        valLoss=DL['val_loss'][-1]
        tit1='%s, train %.1f min, end-val=%.3g'%(deep.prjName,deep.train_sec/60.,valLoss)
        ax1.set(ylabel='loss = %s'%deep.hparams['lossName'],title=tit1,xlabel='fake epochs, earlyStopOccured=%d'%deep.sumRec['earlyStopOccured']) 
        ax1.plot(epoV,DL['loss'],'b:',label='train fit')
        ax1.plot(epoV,DL['val_loss'],'r.-',label='val ')

        if 'train_loss' in DL:
            ax1.plot(epoV,DL['train_loss'],'b--',label='train epochEnd')    
        ax1.legend(loc='best')
        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')

        if 'lr' not in DL: return

        ax3 = self.plt.subplot2grid((nrow,ncol), (2,1), colspan=2,sharex=ax1 )

        ax3.plot(epoV,DL['lr'][-nEpochs:],'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate')    


        if 1: return # TMP
        # time per frame
        ax2  = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=1, rowspan=2 )
        stepTL=DL['stepTime']
        delTL=[]
        [t0,fCnt0, eCnt0]=stepTL[0]
        batch_size=deep.hparams['batch_size']
        numRanks=deep.sumRec['numRanks']
        glob_epoch_size=batch_size * deep.sumRec['steps_per_epoch']*numRanks
        print('DDD0',t0 ,fCnt0, eCnt0,'BS=',batch_size,'gl_ep_sz=',glob_epoch_size,numRanks)
        for t,fCnt, eCnt in stepTL[1:]:            
            delT_step=t-t0
            t0=t
            '''
            if fCnt0!=fCnt:
                #print('new file opened', t,fCnt, pCnt, delT_step)
                fCnt0=fCnt
                continue
            '''
            #print('DDD', t,fCnt, pCnt, delT_step)
            frm2sec=glob_epoch_size/delT_step
            #frm2sec=delT_step
            delTL.append(frm2sec)
            
        ax2.hist(delTL,bins=30)

        zmu=np.array(delTL)
        resM=float(zmu.mean())
        resS=float(zmu.std())
        deep.sumRec['glob_frames_per_sec']=[ resM, resS ]
        numOpenFiles=fCnt*numRanks
        ax2.set(title='train avr %.1f+/-%.1f'%(resM,resS), xlabel='global frames/sec ')
        x0=0.2
        ax2.text(x0,0.85,'BS=%d'%batch_size,transform=ax2.transAxes)
        ax2.text(x0,0.75,'steps=%d'%DL['steps_per_epoch'],transform=ax2.transAxes)
        ax2.text(x0,0.65,'myRank=%d of %d'%(deep.myRank,deep.numRanks),transform=ax2.transAxes)
        ax2.text(x0,0.55,'epochs=%d'%zmu.shape[0],transform=ax2.transAxes)
        ax2.text(x0,0.45,'gl op fil=%d'%numOpenFiles,transform=ax2.transAxes)
        
   
#............................
    def plot_physParam_residu(self,U,Z, figId=9,tit='',tit2=''):

        colMap=cmap.rainbow            
        parName=self.metaD['parNames']
        nPar=self.metaD['numNode']

        nrow,ncol=3,nPar
        breakPar=0
        if nPar>15:
            breakPar=int(np.ceil(nPar/2.) )          
            nrow,ncol=4,breakPar
        
        #  grid is (yN,xN) - y=0 is at the top,  so dum
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(ncol*3.2,nrow*2.6))
        assert U.shape[1]==Z.shape[1]
        
        j=0
        for iPar in range(0,nPar):
            j=iPar
            jOff=1
            if iPar >=breakPar:
                jOff=2*breakPar
                j=iPar-breakPar+1
            print('dd',iPar,j,jOff)
            ax1 = self.plt.subplot(nrow,ncol, j+jOff); ax1.grid()
            ax2 = self.plt.subplot(nrow,ncol, j+ncol*1+jOff); ax2.grid()

            u=U[:,iPar] 
            z=Z[:,iPar]
            unitStr='(a.u.)'
            mm2=self.maxU
            mm1=-mm2
            mm3=self.maxU/1.  # adjust here of you want narrow range for 1D residues
            binsX=np.linspace(mm1,mm2,30)
            rho,sigRho=get_correlation(u,z)
            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX, cmin=1,
                                              cmap = colMap)
            # beutification
            self.plt.colorbar(img, ax=ax1)
            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=1,transform=ax1.transAxes) #diagonal
            # 

            ax1.set(title='%d:%s'%(iPar,parName[iPar]), xlabel='pred %s'%unitStr, ylabel='true')
            ax1.text(0.2,0.1,'rho=%.3f +/- %.3f'%(rho,sigRho),transform=ax1.transAxes)

            x0=0.07
            if iPar==0: ax1.text(x0,0.82,tit,transform=ax1.transAxes)
            if iPar==1: ax1.text(x0,0.92,'n=%d'%(u.shape[0]),transform=ax1.transAxes)
            if iPar==0: ax1.text(x0,0.92,tit2,transform=ax1.transAxes)

            # .... compute residue
            zmu=z-u
            resM=zmu.mean()
            resS=zmu.std()
 
            # ..... 1D residue
            binsU= np.linspace(-mm3,mm3,50)
            ax2.hist(zmu,bins=binsU)

            ax2.set(title='res: %.2g+/-%.2g'%(resM,resS), xlabel='pred-true %s'%unitStr, ylabel='traces')
            ax2.text(0.15,0.8,parName[iPar],transform=ax2.transAxes)

            if breakPar>0: continue
            # ......  2D residue
            ax3 = self.plt.subplot(nrow,ncol, j+ncol*2+jOff); ax3.grid()
            zsum,xbins,ybins,img = ax3.hist2d(z,zmu,bins=[binsX,binsX/2], cmin=1,cmap = colMap)
            self.plt.colorbar(img, ax=ax3)
            ax3.set( xlabel='pred %s'%unitStr, ylabel='pred-true %s'%unitStr)
            ax3.axhline(0, color='green', linestyle='--')
            ax3.set_xlim(mm1,mm2) ; ax3.set_ylim(mm1,mm2)

  
#............................
    def plot_data_fparams(self,U,tit,figId=6,tit2='', metaD=None):

        if metaD==None:
            metaD=self.metaD
        else:
            print('PF: use metaD=',metaD)
        parName=metaD['parNames']
        nPar=metaD['numNode']
        autoScale = 'autoScale' in metaD
        print('PF:',tit,' autoScale=',autoScale)
        #nrow,ncol=2,3
        #nrow,ncol=7,7
        nrow,ncol=8,8
        
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(2.7*ncol,2.2*nrow))
        unitStr=' (a.u.)'
        mm2=mm=self.maxU
        mm1=-mm2
        
        if autoScale:
            binsX=30
            binsY=50        
        else:
            binsY=np.linspace(mm1,mm2,50)
            binsX= np.linspace(mm1,mm2,30)

        j=1
        for i in range(0,nPar,2):
            iPar=min(i,U.shape[1]-2) # account for odd num of params
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            self.plt.colorbar(img, ax=ax)
            ax.set(title=tit+unitStr, xlabel='%d: %s'%(iPar,parName[iPar]), ylabel='%d: %s'%(iPar+1,parName[iPar+1]))
            ax.grid()
            if i==0: ax.text(0.1,0.85,tit2,transform=ax.transAxes)
            if self.maxU >1.3:
                rect1 = self.plt.Rectangle((-1,-1),2,2,fill=False,linestyle='--',edgecolor='r')
                ax.add_patch(rect1)

        for iPar in range(0,nPar):
            z=U[:,iPar]
            ax=self.plt.subplot(nrow,ncol,j) ; j+=1
            ax.hist(z,binsY)
            ax.set(xlabel='%d: %s%s'%(iPar,parName[iPar],unitStr),ylabel='traces')
            if iPar==0:  ax.set_title('n=%d'%z.shape[0])
            ax.grid()

