#!/usr/bin/env python
""" 
read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_ResistorTree import Plotter_ResistorTree
from Deep_ResistorTree import Deep_ResistorTree
from Util_Func import  read_yaml, light_data_generator_dense, light_data_generator_graph , get_ZmU_correl, write_yaml
import numpy as np
import time

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='predict Cell-HH data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign', default='dense1',help=" model design of the network")

#    parser.add_argument("-d", "--dataPath",help="path to input",
#                        default='data')

    parser.add_argument("--seedModel",default='out/',
                        help="trained model and weights")

    parser.add_argument("--seedWeights",
                        default=None,
                        help="seed weights only, after model is created")

    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
    parser.add_argument("--dataSource",type=str,
                        default='data',help="path to data vault: cscratch or BB")
    parser.add_argument("--dataSplit", default='circR_2e_dataSplit.yaml',
                        help="how to split data onto train/val/test")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='resistorTree'
    args.batch_size=1

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_ResistorTree.predictor(args)

#tmp, for hparams['gcnnK']
# . . . load hyperparameters
hparF='./hpar_'+deep.prjName+'.yaml'
bulk=read_yaml(hparF)
deep.hparams=bulk



dom='test'
#dom='val'
#dom='blind'

gra=Plotter_ResistorTree(args,deep.metaD  )

# deep.print_frames('test',numFrm=1)
if args.seedModel=='same' :  args.seedModel=args.outPath
deep.load_model_full(args.seedModel)
if args.seedWeights:
    if args.seedWeights=='same' : args.seedWeights=args.seedModel
    deep.load_weights_only(path=args.seedWeights)

if 'dense' in deep.modelDesign :
    inpGen=light_data_generator_dense(dom, deep)
    isGraph=False
elif 'graph' in deep.modelDesign :
    inpGen=light_data_generator_graph(dom, deep)
    isGraph=True
else:
    print('ABORT, unimplemented generator for design:', self.modelDesign)
    exit(66)

next(inpGen) # trigger file openning
numPredEve= deep.metaD['gen'][dom]['events']
if args.events>0: numPredEve=min(numPredEve,args.events)
    
print("\n   numPredEve=%d, making predictions ..."%numPredEve)

startT0 = time.time()

U=[];Z=[];X=[]
for i in range(numPredEve):
    x,u = next(inpGen)    
    #print(i,'x sh',x.shape,u)
    z= deep.model.predict(x)
    if isGraph:
        X.append(x[0].flatten())
        U.append(u[0,:,0]) 
        Z.append(z[0,:,0])
    else:
        X.append(x.flatten())
        U.append(u[0])
        Z.append(z[0])

U=np.array(U)
Z=np.array(Z)
X=np.array(X)
predTime=time.time() - startT0
print('pred-done, elaT=%.1f sec,'%(predTime))

print('got predicted Z, shape:',Z.shape,' X.shape=',X.shape)
print('Z sample:',Z[:5])
tit='dom=%s '%(dom)
lossMSE=get_ZmU_correl(Z-U,dom,deep.metaD['parNames'])
gra.plot_physParam_residu(U,Z, figId=9,tit=tit, tit2='loseMSE=%.3f'%lossMSE)


gra.plot_data_fparams(Z,'pred Z',figId=8)
#1gra.plot_data_fparams(U,'true U',figId=7)

numNode=X.shape[1]
nameL=[ 'V%d'%i for i in range(numNode)]
print('XX nameL=',nameL)
metaD2={'parNames':nameL,'numNode':numNode,'autoScale':True}
#1gra.plot_data_fparams(X,'inp X',figId=6,metaD=metaD2)


sumRec={}
sumRec[dom+'LossMSE']=float(lossMSE)
sumRec['predTimeMnt']=predTime/60.
sumRec['numSamples']=numPredEve
sumRec['design']=deep.modelDesign

write_yaml(sumRec, deep.outPath+'/'+deep.prjName+'.sum_pred.yaml')

gra.display_all(args,'predict')  







