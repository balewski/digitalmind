#!/usr/bin/env python
import numpy as np

''' 
graph operations, based on
https://networkx.github.io/documentation/stable/tutorial.html

Examples:
https://networkx.github.io/documentation/stable/auto_examples/index.html

'''


import sys

import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
from Util_Func import read_graph, write_yaml,write_data_hdf5
from spektral.utils import  nx_to_numpy

inpF="data_c7/circR7_6n.grf"
G=read_graph(inpF)

# plotting
plt.figure(10)
pos=graphviz_layout(G)
nx.draw(G, with_labels=True,pos=pos)
edge_labels = nx.get_edge_attributes(G,'R')
#print('EL',edge_labels)
#nx.draw_networkx_edge_labels(G, pos,labels = edge_labels) # edge attribute+ID
nx.draw_networkx_edge_labels(G, pos, edge_labels)  # only edgeID
print('input  graph w/:  %d edges, %d nodes,'%(nx.number_of_edges(G),nx.number_of_nodes(G)))

L = nx.line_graph(G)
plt.figure(13)
pos=graphviz_layout(L)
nx.draw(L, with_labels=True,pos=pos)
print('output line  graph w/:  %d edges, %d nodes,'%(nx.number_of_edges(L),nx.number_of_nodes(L)))

# extract G-edges of the graph
graphEdges=np.array(G.edges())
print( 'G edges.shape=',graphEdges.shape)#, 'dump:',graphEdges)
print(G.edges(data=True))

adj1,nf1,ef1=nx_to_numpy(G, auto_pad=False, self_loops=True)#,nf_keys=['sd'])
print(type(adj1),type(nf1),type(ef1))
print('template ADJ',adj1.shape,type(adj1),adj1[0])
dataD={'adj_mx':adj1}
hd5F=inpF.replace('.grf','.hd5')
write_data_hdf5(dataD,hd5F)


print('plot.show')
plt.show()
