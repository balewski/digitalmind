import time,  os
import h5py
import ruamel.yaml  as yaml
import numpy as np
import networkx as nx
from ShellCmdwTimeout import ShellCmdwTimeout
from scipy.stats import stats # for Pearson correlation
from spektral.utils.convolution import localpooling_filter

#...!...!..................
def light_data_generator_dense(dom,deep):
    # it will use only 1st file from the list: deep.metaD[dom]
    inpPath=deep.inpDir
    fnameL=deep.metaD[dom]
    bs=deep.batch_size
    numNode=deep.metaD['numNode']

    # open the HD5 file for reading
    inpF=inpPath+'/'+fnameL[0]
    print('light data gener dom=',dom,',inpF=',inpF,',BS=',bs,'numNode=',numNode)
    blob=read_data_hdf5(inpF)
    j=0

    XAA=blob['Volts']
    YY=blob['RxUnit']
    deep.metaD['gen'][dom]={'events':blob['Volts'].shape[0], 'epochs':0}
    
    mxJ=XAA.shape[0]
    assert mxJ >bs
    assert numNode==XAA.shape[1]

    # loop indefinitely
    while True:
        if j>=mxJ-1-bs:
            j=0 # reset pointer if not enough for next batch
            deep.metaD['gen'][dom]['epochs']+=1
        XA=XAA[j:j+bs]                        
        Y=YY[j:j+bs]
        j+=bs
        yield (XA,Y)

#...!...!..................
def light_data_generator_graph(dom,deep=2):
    # it will use only 1st file from the list: deep.metaD[dom]
    inpPath=deep.inpDir
    fnameL=deep.metaD[dom]
    bs=deep.batch_size
    numNode=deep.metaD['numNode']
    gcnnK=deep.hparams['gcnnK']

    # open the HD5 file for reading
    inpF=inpPath+'/'+fnameL[0]
    print('light data gener dom=',dom,',inpF=',inpF,',BS=',bs,'numNode=',numNode,'gcnnK=',gcnnK)
    blob=read_data_hdf5(inpF)

    # add new axis for number of features - here 1 feature per node
    XAA=np.expand_dims(blob['Volts'],axis=-1)
    YY=np.expand_dims(blob['RxUnit'],axis=-1)
    print('XAA shape=',XAA.shape,'YY shape=',YY.shape)

    # remap nodes - is it needed?
    nodeMap=np.array([0,3,2,5,1,4]) # inverse
    #nodeMap=np.array([0,4,2,1,5,3])
    XAA=XAA[:,nodeMap,:]
    
    deep.metaD['gen'][dom]={'events':blob['Volts'].shape[0], 'epochs':0}
    
    mxJ=XAA.shape[0]
    assert mxJ >bs
    assert numNode==XAA.shape[1]

    # read in graph
    inpF=deep.inpDir+'/'+deep.metaD['graphName']+'.hd5'
    blob=read_data_hdf5(inpF)
    adj1=blob['adj_mx'][0]
    print('template ADJ',adj1.shape,type(adj1))

    #symetrize ADJ by adding reversed edges
    for i in range(numNode):
        for j in range(i,numNode):
            #print(i,j,adj1[i,j], adj1[j,i])
            if adj1[i,j]>0 or  adj1[j,i]>0:
              adj1[i,j]=adj1[j,i]=1
            
    # Preprocessing operations
    fltr1 = localpooling_filter(adj1)
    #fltr1 =adj1
    
    print('fltr1',fltr1.shape,type(fltr1))
    # Pre-compute propagation : Output matrix is worng
    for i in range(gcnnK - 1):
        fltr1 = fltr1.dot(fltr1)
        print('fltr i',i,fltr1.shape,type(fltr1))
    X2=[ fltr1 for  i in range(bs)]
    XB=np.array(X2)  # fixed for all events
    print('adj-matrix XB:',XB.shape)

 
    j=0  # pointer on data  record 
    # loop indefinitely
    while True:
        if j>=mxJ-1-bs:
            j=0 # reset pointer if not enough for next batch
            deep.metaD['gen'][dom]['epochs']+=1
        XA=XAA[j:j+bs]                        
        Y=YY[j:j+bs]
        j+=bs # advance to next batch here
        yield ([XA,XB],Y)

#- - - - - - - -
def test_generator(deep):
    print(' test generator, design=',deep.modelDesign)
    
    if 'dense' in deep.modelDesign :
        gen=light_data_generator_dense('train',deep)
        X,Y = next(gen)
        print(X.shape,Y.shape)
        print('X=',X[0],'\nY=',Y[0])
 
    elif 'graph' in deep.modelDesign :
        gen=light_data_generator_graph('train',deep)
        [XA,XB],Y = next(gen)
        print(XA.shape,XB.shape,Y.shape)
        print('XA=',XA[0],'XB=\n',XB[0],'\nY=',Y[0])

    else:
        exit(61)
    print('test_generator DONE')
    exit(62)
    

#...!...!..................
def read_graph(inpF):
        print(' read edgelist from:',inpF)
        # read edgelist from grid.edgelist
        G = nx.read_edgelist(path=inpF, delimiter=":",
                             create_using =nx.DiGraph(),
                             nodetype = int,
                             #data=(('R',str),)
        )

        print('Got graph w/:  %d edges, %d nodes,'%(nx.number_of_edges(G),nx.number_of_nodes(G)))
        is_dg=nx.is_directed(G)
        assert is_dg
        return G


#...!...!..................
def read_yaml(ymlFn):
        print('  read  yaml1:',ymlFn)#,' ',end='')
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.Loader)
        ymlFd.close()
        #print(' done, size=%d'%len(bulk))
        return bulk

#...!...!..................
def write_yaml(rec,ymlFn,verb=1):
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1048576
        if verb:
                print('  closed  yaml:',ymlFn,' size=%.2f MB'%xx)

#...!...!..................
def write_data_hdf5(dataD,outF):
        h5f = h5py.File(outF, 'w')
        print('save data as hdf5:',outF)
        for item in dataD:
           rec=dataD[item]
           h5f.create_dataset(item, data=rec)
           print('h5-write :',item, rec.shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%(xx))

#...!...!..................
def read_data_hdf5(inpF):
        print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD
    

################################
#     execute shell command 'cmd'
################################

def shell_runSpice(spiceF,numEdge, cleanup=True, verb=1 ):
    cmd='time ngspice -b %s\n'%spiceF
    if verb>-1: print ('do command1:',cmd)
    task=ShellCmdwTimeout(cmd, timeout = 10) # in seconds
    if task.ret:
        # execution failed or timed out
        return True,None

    VoltA=np.zeros(numEdge) # for collecting output
    if verb>0: print ('cmd: stdErr=')
    for line in task.stderr:
         if 'elapsed' not in line  : continue
         if verb>0: print (line, len(line) )
         elapse=line
         
    if verb>0: print ('cmd: stdOut=')
    nV=0
    for line in task.stdout:
        #print (line, len(line) )
        if len(line)<2:
            continue;
        if 'Circuit:' in line : tit=line[7:]
        if line[0]!='v' : continue
        xL=line.split()
        if verb>0: print(xL[0],xL[2])
        #print(xL)
        i=int(xL[0][1:])
        
        VoltA[i]=xL[2]
        nV+=1

    isOK=nV==numEdge
    if isOK and cleanup:
         cmd='rm %s\n'%spiceF
         if verb>0: print ('do command2:',cmd)
         ShellCmdwTimeout(cmd, timeout = 10) # in seconds

    return  not isOK,VoltA



#...!...!..................
def get_ZmU_correl(ZmU,dom,parNameL):
        print('ZmU dims:',ZmU.shape,'\n dump a:')

        # compute cov matrix
        c=np.cov(ZmU,rowvar=False)
        nv=c.shape[0]
        nvL=range(nv)
        #print('cov M:')
        #pprint(c)
        c1=np.diagonal(c)
        rmseU=np.sqrt(c1)
        print('one RMSE for ZmU:',rmseU)

        avreU=ZmU.mean(axis=0)
        print('ZmU shape',ZmU.shape,avreU.shape)
        
        avrSig=rmseU.mean()
        print('global RMS avr=%.3f , var=%.3f '%(avrSig, avrSig*avrSig))

        print('\nresidue',end='')
        [ print(' %2d:%s  '%(i,parNameL[i]),end='') for i in nvL]

        print('\n avr: ',end='')
        [ print(' U%d-> %5.2f  '%(i,avreU[i]),end='') for i in nvL ]

        print('\n rms: ',end='')
        [ print(' U%d-> %4.1f%s  '%(i,100*rmseU[i],chr(37)),end='') for i in nvL ]

        lossMAE=np.absolute(ZmU).mean()

        lossMSE=c1.mean()
        print('\n global lossMSE=%.4f , lossMAE=%.3f (computed), dom=%s'%( lossMSE,lossMAE,dom))

        #print('\ncorrelation matrix for %d dim vector'%nv)
        r=np.corrcoef(ZmU,rowvar=False)
        #pprint(r)

        print('\nnames  ',end='')
        [ print(' %2d:%s'%(i,parNameL[i]),end='') for i in nvL]
        print('\ncorrM     ',end='')
        [ print(' __var%1d__  '%i,end='') for i in nvL]
        for k in nvL:
            print('\n var%1d '%k,end='')
            [ print('     %6.3f'% r[i][k],end='') for i in range(k+1)]
            #print('      #  '*(nv-k),end='')
        print()
        return lossMSE


#............................
def get_correlation(aV,bV):
        n=len(aV)
        assert n >2
        assert n == len(bV)
        rho,p_val=stats.pearsonr(aV,bV)
        #print(n,rho,p_val)
        # based on  https://stats.stackexchange.com/questions/226380/derivation-of-the-standard-error-for-pearsons-correlation-coefficient

        sigRho=np.sqrt( (1-rho*rho)/(n-2))
        #print('rho=',rho,'sigRho=',sigRho)
        return rho,sigRho

