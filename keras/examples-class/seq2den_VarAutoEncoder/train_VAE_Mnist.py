#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_VAE_Mnist import Plotter_VAE_Mnist
from Deep_VAE_Mnist import Deep_VAE_Mnist

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Mnist data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("--project",
                        default='vae_mnist',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="fitting epoch")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", dest='earlyStopOn',
                         action='store_true',default=False,help="enable early stop")
    parser.add_argument( "--checkOn", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_VAE_Mnist(args )
deep=Deep_VAE_Mnist(args)
deep.load_input_hdf5(['train','val'])
#ppp.plot_digits(deep,'val',range(6),0)

deep.build_model(args) 
ppp.plot_model(deep)

deep.train_model(args) 
deep.save_model_full() 

ppp.plot_train_history(deep,args)
ppp.plot_latent_z(deep,'val')

ppp.display_all(args,'train')


