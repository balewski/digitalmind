# for AUC of ROC
from sklearn.metrics import roc_curve, auc
import numpy as np
from keras.utils import plot_model

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_catdog(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args,nr_nc=(3,3)):
        if args.noXterm:
            print('diasable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print('Graphics started')
        self.plt=plt
        self.nr_nc=nr_nc
        self.kz=8  # divider for displaying latent data
        self.figL=[]
            

#............................
    def pause(self,args,ext,pdf=1):
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
            
                figName='idx%d_%s_%s_f%d'%(args.arrIdx,args.prjName,ext,fid)  
                print('Graphics displayed, saving %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()
  

#............................
    def plot_model(self,deep,netN):
        fname=deep.name+'.'+netN+'.graph.svg'
        plot_model(deep.model[netN], to_file=fname, show_shapes=True, show_layer_names=True)
        print('Graph saved as ',fname)


#............................
    def plot_autoen_eval(self,deep,args,figId):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(12,6))
        nrow,ncol=(4,6)
        print('see len eval_out',len(deep.eval['out']))
        assert ncol <= len(deep.eval['out'])
        for i in range(ncol):
            # display original
            ax = self.plt.subplot( nrow,ncol, i + 1)
            img =deep.eval['inp'][i]
            ax.imshow(img)
            print('\ninp img',img.shape, 'mean=%.2f'%img.mean())
            
            if i==0 :ax.set(ylabel='input')
            ax.get_xaxis().set_visible(False)
            
            # display outputs
            ax = self.plt.subplot( nrow,ncol, i + 1+ncol)
            img =deep.eval['out'][i]
            print('out img',img.shape,  'mean=%.2f'%img.mean())
            ax.imshow(img)
            if i==0 : ax.set(ylabel='output')
            ax.get_xaxis().set_visible(False)

            if deep.eval['zip']==None :
                print("no zip data, skip plotting")
                continue
            # display zips
            ax = self.plt.subplot( nrow,ncol, i + 1+ncol*2)
            img=deep.eval['zip'][i]
            ax.imshow(img.reshape(-1,self.kz))
            if i==0 : ax.set(title='zipped')

            # histo zips
            ax = self.plt.subplot( nrow,ncol, i + 1+ncol*3)
            n, bins, patches = ax.hist(img, 50, facecolor='green', alpha=0.75)
            zip_mean=img.mean()
            print('zip img', img.shape,' mean=%.2f'%zip_mean)
            ax.set(ylabel='zipped',xlabel='ampli', title='mean=%.2f'%zip_mean)
            #ax.set_yscale('log')
                
#............................
    def plot_autoen_input(self,deep):
        sample=next(deep.generator['train']) # will produce whole batch
        self.plt.figure(10,facecolor='white', figsize=(10,6))
        print('plot train sample shape',sample.shape)
        nrow,ncol=self.nr_nc
        imax=nrow*ncol-1
        for i in range(sample.shape[0]):
            #print(i,x.shape)
            ax = self.plt.subplot(nrow, ncol, 1+i)
            tit='i:%d %s'%(i,deep.name) ; ax.set(title=tit)
            ax.imshow(sample[i])
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            if i >= imax: break
 
#............................
    def plot_classif_input(self,deep,figId):
        ([imgA,catsA,dogsA],yA)=next(deep.classifier_generator('train'))
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(12,6))
        print('plot train sample max len',len(yA))
        nrow,ncol=(3,5)
        imax=nrow*ncol-1
        for i in range(ncol):
            print('img=',i,'y=',yA[i])
            ax = self.plt.subplot(nrow, ncol, 1+i)
            tit='i:%d y=%d'%(i,yA[i]) ; ax.set(title=tit)
            ax.imshow(imgA[i])
            
            ax = self.plt.subplot( nrow,ncol, i + 1+ncol)
            img=catsA[i]
            ax.imshow(img.reshape(-1,self.kz))
            if i==0 : ax.set(ylabel='as cat')

            ax = self.plt.subplot( nrow,ncol, i + 1+ncol*2)
            img=dogsA[i]
            ax.imshow(img.reshape(-1,self.kz))
            if i==0 : ax.set(ylabel='as dog')
 
#............................
    def plot_train_hir(self,deep,args,figId): 
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(10,6))
        nrow,ncol=self.nr_nc
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (1,0), colspan=2 )

        DL=deep.train_hirD
        loss=DL['val_loss'][-1]
        
        tit1='%s, train %.2f h, zipDim=%d, end-val-loss=%.3f'%(deep.name,deep.train_sec/3600.,args.zipDim,loss)
        
        ax1.set(ylabel='loss',title=tit1)
        ax1.plot(DL['loss'],'.-.',label='train')
        ax1.plot(DL['val_loss'],'.-',label='valid')
        ax1.legend(loc='best')
        ax1.grid(color='brown', linestyle='--',which='both')
            
        if 'acc' not in DL : 
            ax1.set_yscale('log')
            return

        # this part is only for classifier
        acc=DL['val_acc'][-1]
            
        tit2='arrIdx=%d, end-val-acc=%.3f'%(args.arrIdx,acc)
        if 'use_encoder'  in dir(deep):
            tit2='encoder=%d, '%deep.use_encoder+tit2
        ax2 = self.plt.subplot2grid((nrow,ncol), (2,0), colspan=2,sharex=ax1)
        ax2.set(xlabel='epochs',ylabel='accuracy',title=tit2)
        ax2.plot(DL['acc'],'.-',label='train')
        ax2.plot(DL['val_acc'],'.-',label='valid')
        ax2.legend(loc='bottom right')
        ax2.grid(color='brown', linestyle='--',which='both')

        if 'lr' not in DL: return

        ax3 = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=2,sharex=ax1 )
        ax3.plot(DL['lr'],'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate')

  
    #............................
    def plot_AUC(self,name,deep,args,nEve):
        self.plt.figure(10)
        nrow,ncol=self.nr_nc

        print('accumulate %d number of events for ROC...'%nEve)
        num=0
        k=0
        while num <nEve:
            k+=1
            ([imgA,catA,dogA],yA)=next(deep.classifier_generator(name))
            if num==0:
                imgAS=imgA; catAS=catA; dogAS=dogA; yAS=yA
            else:
                imgAS=np.concatenate((imgAS, imgA), axis=0)
                catAS=np.concatenate((catAS, catA), axis=0)
                dogAS=np.concatenate((dogAS, dogA), axis=0)
                yAS=np.concatenate((yAS, yA), axis=0)
            num=yAS.shape[0]

        print('AUC: collected batches=',k,', img shape',imgAS.shape)
        y_true=yAS

        # produce AUC of ROC
        y_score = deep.model['classif'].predict([imgAS,catAS,dogAS])
        print('\ny_score shape',y_score.shape,y_score[:5])
        print('Y shape',y_true.shape,y_true[:5])

        fpr, tpr, _ = roc_curve(y_true, y_score)
        roc_auc = auc(fpr, tpr)

        LRP=np.divide(tpr,fpr)
        fpr_cut=0.10
        for x,y in zip(fpr,LRP):
            if x <fpr_cut :continue
            print('found fpr=',x, 'LP+=',y)
            break

        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        #ax1 = self.plt.subplot(nrow, ncol, nrow*ncol-ncol)
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,2), rowspan=2)
        ax1.plot(fpr, tpr, label='ROC',color='seagreen' )
        ax1.plot([0, 1], [0, 1], 'k--', label='coin flip')
        ax1.axvline(x=x,linewidth=1, color='blue')
        ax1.set(xlabel='False Positive Rate',ylabel='True Positive Rate',title='ROC , area = %0.3f' % roc_auc)
        ax1.legend(loc='lower right',title=name+'-data')
        ax1.grid(color='brown', linestyle='--',which='both')

        ax2 = self.plt.subplot(nrow, ncol, nrow*ncol)
        ax2.plot(fpr,LRP, label='ROC', color='teal')
        ax2.plot([0, 1], [1, 1], 'k--',label='coin flip')
        ax2.set(ylabel='Pos. Likelih. Ratio',xlabel='False Positive Rate',title='LR+(FPR=%.2f)=%.1f'%(x,y))
        ax2.set_xlim([0,fpr_cut+0.05])

        ax2.axvline(x=x,linewidth=1, color='blue')
        ax2.legend(loc='upper right')
        ax2.grid(color='brown', linestyle='--',which='both')

        print('AUC: %f' % roc_auc)
    
