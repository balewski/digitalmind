import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
from keras import utils as np_utils
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.layers import Dense, Input, concatenate,Conv2D,Flatten,Reshape,Conv2DTranspose, Dropout
from keras.preprocessing.image import ImageDataGenerator
from keras import regularizers

import numpy as np
import h5py

print('deep-libs imported elaT=%.1f sec'%(time.time() - start))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
from keras.callbacks import Callback
import keras.backend as K
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr)
        self.hir.append(lr)

#............................
#............................
#............................
class Deep_catdog(object):
    def __init__(self,args):
        self.name=args.prjName+'_%s_idx%d'%(args.petType,args.arrIdx)
        #self.regularizerStrength=1./2e4 /50./(1+args.arrIdx%3)
        self.verb=args.verb
        self.img_shape=(144,144,3) # matched to autoencoder net topology
        self.model={}
        self.use_encoder=True

        print(self.__class__.__name__,', prj:',self.name)
        #,' regularizerStrength=%.1e'%self.regularizerStrength)

        # CPUs are used via a "device" which is just a threadpool
        if 'nCpu' in vars(args) and args.nCpu>0:
            import tensorflow as tf
            tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
            print('M: restrict CPU count to ',args.nCpu)


    #............................
    def prep_image_generator(self,args, nPets):
        if nPets==1: 
            petL=[args.petType]
            class_mode=None
        else:
            petL=['cats','dogs']
            class_mode='binary'

        batch_size=32 
        if 'batch_size' in vars(args): batch_size=args.batch_size

        print('image_generator setup for',petL,' class_mode=',class_mode,' batch_size=',batch_size)
        # based on https://github.com/fchollet/keras/issues/3923
        # https://keras.io/preprocessing/image/
        img_path='/global/homes/b/balewski/prj/catdog-data/'  # 10k images
        train_data_dir = img_path+'train/'
        val_data_dir = img_path+'validation/'
        img_dims=self.img_shape[:-1]
        print('localize image data, dims:',img_dims,' path=',img_path,' pet=',petL)

        # this is the augmentation configuration we will use for training
        datagen_train = ImageDataGenerator( rescale=1./255,
            shear_range=0.2, zoom_range=0.2,horizontal_flip=True)
        datagen_val = ImageDataGenerator(rescale=1./255)

        # this is a generator that will read pictures found in
        # subfolers of 'data/train', and indefinitely generate
        # batches of augmented image data
        self.generator={}

        self.generator['train'] = datagen_train.flow_from_directory(
            train_data_dir, target_size=img_dims, 
            batch_size=batch_size, classes=petL, class_mode=class_mode)

        self.generator['val'] = datagen_val.flow_from_directory(
            val_data_dir, target_size=img_dims, 
            batch_size=batch_size, classes=petL, class_mode=class_mode)

        # code below is needed for classifier - not sure why

        sample=next(self.generator['train']) # will produce whole batch
        print('train sample len',len(sample),' X-shape=',sample[0].shape)
        if nPets>1 : 
            print(' and Y-labels shape=',sample[1].shape)

        if nPets==1 :         
            sample1,sample2=next(self.autoencoder_generator('train')) # will produce whole batch
            print('sample1,2 shapes',sample1.shape,sample2.shape)

        if nPets==2 :   
            print("sample classifier generator, use_encoder=",self.use_encoder)
            ([imgA,catsA,dogsA],yA)=next(self.classifier_generator('train')) 
            print('yA',yA)

    #............................
    def autoencoder_generator(self,name):
        for batch in self.generator[name]:
            yield (batch, batch)

    #............................
    def classifier_generator(self,name):
        for batch in self.generator[name]:
            #print('cg',len(batch),batch[0].shape,batch[1].shape)
            #print('Ys',batch[1])
            encoded_cat = self.model['cats'].predict(batch[0])
            encoded_dog = self.model['dogs'].predict(batch[0])
            if not self.use_encoder:
                #print(encoded_cat.dtype)
                encoded_cat=np.zeros(encoded_cat.shape, encoded_cat.dtype)
                encoded_dog=np.zeros(encoded_dog.shape, encoded_dog.dtype)
                
            #print('cats',encoded_cat.shape)
            #print('dogs',encoded_dog.shape)
            yield ([batch[0],encoded_cat,encoded_dog],batch[1])

   #............................
    def build_compile_classifier_model(self,args):
        zipDim=args.zipDim # expect 8,16,24,...
        #partDim=60
        #mergeDim=24

        start = time.time()
        input_img = Input(shape=self.img_shape,name='inp_img')
        print('n0 ',input_img.get_shape())

        # build conv*3 net
        net=Conv2D(zipDim, (3, 3), strides=2, padding='same', activation='relu')(input_img)
        net=Conv2D(zipDim, (3, 3), strides=2, padding='same', activation='relu')(net)
        net=Conv2D(2*zipDim, (3, 3), strides=2, padding='same', activation='relu')(net)
        print('n01 ',net.get_shape())
        net=Flatten()(net)  # this converts our 3D feature maps to 1D feature vectors
        net=Dropout(args.dropFrac)(net)
        net_img=Dense(zipDim, activation='relu', name='dense_img')(net)

        print('n02 ',net_img.get_shape())
        #testM = Model(input_img, net_img); testM.summary()

        # build pet-nets
        petL=['cats','dogs']
        input_pet={}
        net_pet={}
        for pet in petL:
            shape1=self.model[pet].output_shape
            print(pet,'shape', shape1)
            input_pet[pet]=Input(shape=(shape1[1],), name='inp_'+pet)

        # merge nets
        net = concatenate([net_img, input_pet['cats'],input_pet['dogs']],name='merge')
        net=Dropout(args.dropFrac)(net)
        net=Dense(1, activation='sigmoid',name='cat_or_dog')(net)
        model = Model([input_img,  input_pet['cats'], input_pet['dogs']], net)
        print('\nnet assembled as Model:',model.count_params())
        model.summary() # will print
        self.model['classif']=model

        model.compile(loss='binary_crossentropy',
                      optimizer='rmsprop',
                      # ???                 optimizer='adam', # worse
                      metrics=['accuracy'])
        
    #............................
    def build_compile_autoencoder_model(self,args):
        ''' based on:
        https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html
        https://blog.keras.io/building-autoencoders-in-keras.html
        and:  https://keras.io/getting-started/functional-api-guide/
        '''
        zipDim=args.zipDim
        
        start = time.time()
        input_img = Input(shape=self.img_shape,name='inp_img')

        net=Conv2D(zipDim, (3, 3), strides=2, padding='same', activation='relu')(input_img)
        net=Conv2D(zipDim, (3, 3), strides=2, padding='same', activation='relu')(net)
        net=Conv2D(2*zipDim, (3, 3), strides=2, padding='same', activation='relu')(net)

        zipT=net.get_shape()
        zip3d=(int(zipT[1]),int(zipT[2]),int(zipT[3]))
        zipDim3d=zip3d[0]*zip3d[1]*zip3d[2]
        print('zip3d shape=',zip3d, ' prod=',zipDim3d)

        # the model so far outputs 3D feature maps (height, width, features), now we converge to single discriminator

        net=Flatten()(net)  # this converts our 3D feature maps to 1D feature vectors
        net=Dropout(args.dropFrac)(net)
        #acRg=None
        #activity_regularizer=regularizers.l2(1e-7)
        #,activity_regularizer=acRg,
        encoded=Dense(zipDim, activation='tanh', name='zip_out')(net)
        # now latent layer values are bracketed [-1,1]

        encoderM = Model(input_img, encoded)
        print('\nEncoder layers=%d , params=%.1f K'%(len(encoderM.layers),encoderM.count_params()/1000.))

        if self.verb>1:
            encoderM.summary() # will print
        print('Encoder output shape',encoderM.output_shape)

        input_zip = Input(shape=(encoderM.output_shape[1],),name='inp_zip')

        # "decoded" is the lossy reconstruction of the input
        net=Dense(zipDim3d, activation='relu')(encoded)
        net = Reshape(zip3d)(net)
        net = Conv2DTranspose(zipDim, (3, 3),  strides=(2, 2), padding='same', activation='relu')(net)
        net = Conv2DTranspose(zipDim, (3, 3),  strides=(2, 2), padding='same', activation='relu')(net)
        decoded = Conv2DTranspose(self.img_shape[2], (3, 3),  strides=(2, 2), padding='same', activation='sigmoid')(net)
        #old decoded = Conv2D(self.img_shape[2], (3, 3), activation='sigmoid', padding='same')(net)
        autoencoderM = Model(input_img, decoded)
        print('\nFull auto-encoder layers=%d , params=%.1f K'%(len(autoencoderM.layers),autoencoderM.count_params()/1000.))
        if self.verb>1:
            autoencoderM.summary() # will print

        kOff=len(autoencoderM.layers) - len(encoderM.layers)
        print('Autoencoder - Encoder layers len diff=',kOff)

        # decoder from autoencoder
        # retrieve last K layers of the autoencoder model
        net = autoencoderM.layers[-kOff](input_zip)
        for i in range(-kOff+1, 0,1):
            net = autoencoderM.layers[i](net)
        decoderM = Model(input_zip, net)
        if self.verb>1:
            decoderM.summary() # will print

        self.model={}
        self.model['autoen']=autoencoderM
        self.model['encoder']=encoderM
        self.model['decoder']=decoderM

        self.compile_autoencoder()
        print('model autoEn size=%.1fK compiled elaT=%.1f sec'%(autoencoderM.count_params()/1000.,time.time() - start))


   #............................
    def compile_autoencoder(self):
        optimizer='rmsprop' # can be used for regression
        optimizer='adadelta'
        optimizer='adam'
        loss='mean_squared_error' # aka L2
        #loss='mean_absolute_error' # aka L1

        self.model['autoen'].compile(optimizer=optimizer, loss=loss)
        print('\nCompiled auto-encoder  , params=%.1f K, optimizer=%s  loss=%s'%(self.model['autoen'].count_params()/1000.,optimizer,loss))


    #............................
    def train_autoencoder(self,args):
        nb_train_samples = args.batches
        nb_validation_samples = int(nb_train_samples*.2)+1
 
        if args.verb==0:
            print('train silently epochs:',args.epochs)

        callbacks_list = []

        if args.earlyStopPatience>0:
            # spare: 
            earlyStop=EarlyStopping(monitor='val_loss',min_delta=1.e-4, patience=args.earlyStopPatience, verbose=1, mode='auto')
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping, patience=',args.earlyStopPatience)

        if args.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.name+'-autoen.weights_best.h5'
            chkPer=4
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=chkPer)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint, period=',chkPer)

        print('\nTrain_model batch_size=',args.batch_size,' num_batches=',args.batches,' epochs=',args.epochs,' earlyStop=',args.earlyStopPatience)
        startTm = time.time()

        hir=self.model['autoen'].fit_generator(
            self.autoencoder_generator('train'),
            samples_per_epoch=nb_train_samples,
            nb_epoch=args.epochs, verbose=args.verb>0,
            callbacks=callbacks_list,
            validation_data=self.autoencoder_generator('val'),
            nb_val_samples=nb_validation_samples)

        self.train_hirD=hir.history

        fitTime=time.time() - start
       
        loss=self.train_hirD['val_loss'][-1]
        print('\n End Validation  Loss:%.3f'%loss,', fit time=%.2f min'%(fitTime/60.))
        if args.verb>1:
            print('Dump fit history')
            print(self.train_hirD)
        self.train_sec=fitTime

 
    #............................
    def train_classifier(self,args):
        nb_train_samples = args.batches
        nb_validation_samples = int(nb_train_samples*.2)+1
 
        if args.verb==0:
            print('train silently epochs:',args.epochs)

        callbacks_list = []

        if args.earlyStopPatience>0:
            # spare: 
            earlyStop=EarlyStopping(monitor='val_loss', patience=args.earlyStopPatience, verbose=1, min_delta=1.e-4, mode='auto')
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping, patience=',args.earlyStopPatience)

        if args.checkPtOn:
            outF5w=self.name+'-classif.weights_best.h5'
            chkPer=4
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=chkPer)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint, period=',chkPer)

        if args.reduceLearn:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=5, min_lr=0.0, verbose=1,epsilon=0.01)
            callbacks_list.append(redu_lr)
            lrCb=MyLearningTracker()
            callbacks_list.append(lrCb)  # just for plotting
            print('enabled ReduceLROnPlateau')


        print('\nTrain Classifier batch_size=',args.batch_size,' num_batches=',args.batches,' epochs=',args.epochs,' earlyStop=',args.earlyStopPatience)
        startTm = time.time()

        hir=self.model['classif'].fit_generator(
            self.classifier_generator('train'),
            samples_per_epoch=nb_train_samples,
            nb_epoch=args.epochs, verbose=args.verb>0,
            callbacks=callbacks_list,
            validation_data=self.classifier_generator('val'),
            nb_val_samples=nb_validation_samples)

        self.train_hirD=hir.history
        if args.reduceLearn:
            self.train_hirD['lr']=lrCb.hir

        fitTime=time.time() - start
       
        loss=self.train_hirD['val_loss'][-1]
        acc=self.train_hirD['val_acc'][-1]
        print('\n End Validation  Acc=%.3f Loss:%.4f'%(acc,loss),', fit time=%.1f min'%(fitTime/60.))
        if args.verb>1:
            print('Dump fit history')
            print(self.train_hirD)
        self.train_sec=fitTime

 
    #............................
    def predict_encoder_decoder(self,n=6):
        sample=next(self.generator['val']) # will produce whole batch
        sample=sample[:n]
        print('val sample shape',sample.shape)
        encoded_imgs = self.model['encoder'].predict(sample)
        decoded_imgs = self.model['decoder'].predict(encoded_imgs)

        zip_mean=encoded_imgs.mean()
        zip_stddev=encoded_imgs.std()
        print('eval encoder output: mean=%.3f  stddev=%.3f'%(zip_mean,zip_stddev))
        print('auuIn', sample.mean())
        print('auuOu', decoded_imgs.mean())
        self.eval={}
        self.eval['inp']=sample
        self.eval['zip']=encoded_imgs
        self.eval['out']=decoded_imgs
        
    #............................
    def predict_autoencoder(self,n=6):
        sample=next(self.generator['val']) # will produce whole batch
        sample=sample[:n]
        print('val sample shape',sample.shape)
        #encoded_imgs = self.model['encoder'].predict(sample)
        decoded_imgs = self.model['autoen'].predict(sample)
        self.eval={}
        self.eval['inp']=sample
        self.eval['zip']=None
        self.eval['out']=decoded_imgs
    #............................
    def eval_autoencoder(self):
        sample=next(self.generator['val']) # will produce whole batch        
        #print('val Xsample shape',sample.shape)
        pred_imgs = self.model['autoen'].predict(sample)
        score=self.model['autoen'].evaluate(pred_imgs,sample, batch_size=32, verbose=0)
        print('score=%.4f, batch_size=%d'%(score,sample.shape[0]))
        return score
        
    #............................
    def save_model(self,modelL):
        for modelN in modelL:
            outF=self.name+'.%s.model.h5'%modelN
            print('save model full to',outF)
            self.model[modelN].save(outF)
            xx=os.path.getsize(outF)/1048576
            print('  closed  hdf5:',outF,' size=%.2f MB'%xx)
 
    #............................
    def load_model(self,modelL,path='.',trainN=''):
        if len(trainN)<2: trainN=self.name
        for modelN in modelL:
            try:
                del self.model[modelN]
                print('delte old model',modelN)
            except:
                a=1
            start = time.time()

            outF=path+'/'+trainN+'.'+modelN+'.model.h5'
            print('load model and weights  from',outF,'  ... ')
            self.model[modelN]=load_model(outF) # creates mode from HDF5
            if 'autoen'==modelN:  self.model[modelN].summary()
            print(' model loaded, elaT=%.1f sec'%(time.time() - start))

    #............................
    def load_encoder_models(self,petL,path='.'):
        trainL=[ x for x in petL]
        print('bbb',trainL)
        for petN in petL:   
            try:
                del self.model[petN]
                print('delte old model',petN)
            except:
                a=1
            outF=path+'/petAuto_%s_idx1.encoder.model.h5'%petN
            print('load encoder model and weights  from',outF,'  ... ')
            self.model[petN]=load_model(outF) # creates mode from HDF5
 
    #............................
    def load_autoen_weights(self,path='.'):
        start = time.time()
        outF5m=path+'/'+self.name+'-autoen.weights_best.h5'
        print('load  weights  from',outF5m,end='... ')
        self.model['autoen'].load_weights(outF5m) # creates mode from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))
