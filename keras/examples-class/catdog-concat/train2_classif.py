#!/usr/bin/env python
""" read input cat/dog from Kagel data set
train net
write net + weights as HD5
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_catdog import Plotter_catdog
from Deep_catdog import Deep_catdog

import socket  # for hostname

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train autoencoder for single animal: cat or dog',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='petClsf',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("--seedEncoder",
                        default='ok-v10-idx1',
                        help="seed encoder model and weights")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="fitting epoch")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=10,
                        help="fit batch_size")

    parser.add_argument("-z", "--zipDim", type=int, default=32,
                        help="size of our encoded representations")

    parser.add_argument("-n", "--batches", type=int, default=10,
                        help="num batches per epoch")
    parser.add_argument("--dropFrac", type=float, default=0.3,
                        help="drop fraction at all layers")

    parser.add_argument( "-s","--earlyStop", type=int,
                         dest='earlyStopPatience', default=20,
                         help="early stop:  epochs w/o improvement (aka patience), 0=off")
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLr", dest='reduceLearn',
                         action='store_true',default=False,help="reduce learning at plateau")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
petL=['cats','dogs']

args=get_parser()
args.petType='both'

ppp=Plotter_catdog(args )
deep=Deep_catdog(args)
if args.arrIdx!=1:
    deep.use_encoder=False

deep.load_encoder_models(petL,path=args.seedEncoder)
deep.prep_image_generator(args,2)
ppp.plot_classif_input(deep,12)

deep.build_compile_classifier_model(args) 
if not args.noXterm and 'edison' not in socket.gethostname():
    ppp.plot_model(deep,'classif')
if args.epochs>20: deep.save_model(['classif']) 

deep.train_classifier(args) 

deep.save_model(['classif']) 
ppp.plot_train_hir(deep,args,10)
ppp.plot_AUC('val',deep,args,500)
ppp.pause(args,'train2') # list of figures




