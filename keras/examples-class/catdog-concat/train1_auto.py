#!/usr/bin/env python
""" read input cat/dog from Kagel data set
train net
write net + weights as HD5
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_catdog import Plotter_catdog
from Deep_catdog import Deep_catdog

import socket  # for hostname

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train autoencoder for single animal: cat or dog',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='petAuto',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument('-p',"--pet",
                        default='cats',dest='petType',
                        help="animal to be trained for")
  
    parser.add_argument("--seedModel",
                        default='none',
                        help="seed model and weights")

    parser.add_argument("--seedWeights",
                        default='none',
                        help="seed weights only, after model is loaded, 'same' is allowed")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="fitting epoch")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=10,
                        help="fit batch_size")

    parser.add_argument("-z", "--zipDim", type=int, default=32,
                        help="size of our encoded representations")

    parser.add_argument("-n", "--batches", type=int, default=10,
                        help="num batches per epoch")

    parser.add_argument("--dropFrac", type=float, default=0.3,
                        help="drop fraction at all layers")

    parser.add_argument( "-s","--earlyStop", type=int,
                         dest='earlyStopPatience', default=20,
                         help="early stop:  epochs w/o improvement (aka patience), 0=off")
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_catdog(args )
deep=Deep_catdog(args)
deep.prep_image_generator(args,1)
ppp.plot_autoen_input(deep)

trainFreezN='' 
if '9cats' in args.seedModel : trainFreezN='petAuto_cats_idx%d'%args.arrIdx
if '9dogs' in args.seedModel : trainFreezN='petAuto_dogs_idx%d'%args.arrIdx

modelL=['autoen','encoder','decoder']

if args.seedModel=='none':
    deep.build_compile_autoencoder_model(args) 
else:
    print('continue training, ignore zipDim')
    deep.load_model(modelL,path=args.seedModel,trainN=trainFreezN)
    if args.seedWeights!='none':
        if args.seedWeights=='same':
            deep.load_autoen_weights(args.seedModel)
        else:
            deep.load_autoen_weights(path=args.seedWeights)
    deep.compile_autoencoder()

if not args.noXterm and 'edison' not in socket.gethostname():
    for modelN in modelL:
        ppp.plot_model(deep,modelN)

if args.epochs>20: deep.save_model(modelL) 

deep.train_autoencoder(args) 

deep.save_model(modelL) 

ppp.plot_train_hir(deep,args,10)

deep.predict_encoder_decoder() 
ppp.plot_autoen_eval(deep,args,11)
ppp.pause(args,'train1') # list of figures




