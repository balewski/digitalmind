#!/usr/bin/env python
""" read input cat/dog from Kagel data set
read weights of train net
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_catdog import Plotter_catdog
from Deep_catdog import Deep_catdog
import socket
import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train autoencoder for single animal: cat or dog',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='petAuto',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument('-p',"--pet",
                        default='cats',dest='petType',
                        help="animal to be trained for")

    parser.add_argument("-b", "--batch_size", type=int, default=500,
                        help="fit batch_size")

    parser.add_argument("--seedModel",
                        default='./',
                        help="seed model and weights")
    parser.add_argument("--seedWeights",
                        default='none',
                        help="seed weights only, after model is loaded, 'same' is allowed")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
modelL=['encoder','decoder','autoen']

ppp=Plotter_catdog(args )
deep=Deep_catdog(args)

deep.load_model(modelL,path=args.seedModel)
if args.seedWeights!='none':
    if args.seedWeights=='same':
        dee.load_autoen_weights(args.seedModel)
    else:
        dee.load_autoen_weights(path=args.seedWeights)

goalPet='cats'
goalPet='dogs'

args.petType=goalPet  # overwrite for ross checking
deep.prep_image_generator(args,1)

# evaluate the model
nTry=3
print('pet=',goalPet,', model=',args.seedModel,' nTry=',nTry)
sum=0.
for j in range(nTry):
    sum+=deep.eval_autoencoder() 
print('avr loss=%.4f'%(sum/nTry))

deep.predict_encoder_decoder() 
ppp.plot_autoen_eval(deep,args,11)
ppp.pause(args,'pred1')






