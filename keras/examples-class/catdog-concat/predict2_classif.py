#!/usr/bin/env python
""" read input cat/dog from Kagel data set
read weights of train net
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_catdog import Plotter_catdog
from Deep_catdog import Deep_catdog
import socket
import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train autoencoder for single animal: cat or dog',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb') 
    parser.add_argument("--project",
                        default='petClsf',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("--seedEncoder",
                        default='ok-zip32',
                        help="seed encoder model and weights")

    parser.add_argument("--seedClassif",
                        default='./',
                        help="seed classif model and weights")


    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
petL=['cats','dogs']

args=get_parser()
args.petType='both'

ppp=Plotter_catdog(args )
deep=Deep_catdog(args)

deep.load_encoder_models(petL,path=args.seedEncoder)
deep.prep_image_generator(args,2)
ppp.plot_classif_input(deep,12)
#ppp.plt.show()

deep.load_model(['classif'],path=args.seedClassif)

if not args.noXterm  and 'edison' not in socket.gethostname():
    ppp.plot_model(deep,'classif')

ppp.plot_AUC('val',deep,args,500)
ppp.pause(args,'pred2')




