#!/usr/bin/env python
""" 
Generates 2D sets of data point in the shape of moon or sun,  saves X,Y as HD5 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_MoonPermute import Plotter_MoonPermute
from Util_Func import write_yaml, read_yaml, produce_frames,write_data_hdf5

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("-d","--dataPath",  default='data',
                        help="output HD5+TFrec path")

    parser.add_argument("-N", "--name",  default='frames',
                        help="core name of produced file")

    parser.add_argument("-o","--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-n", "--events", default=500, type=int,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='moonPermute'

    if args.outPath=='same': args.outPath=args.dataPath

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
contentF=args.dataPath+'/content.'+args.prjName+'.yaml'

contentD=read_yaml(contentF)
print('M:con',contentD)

dataD,metaD=produce_frames(contentD,args)
# save meta-data file
metaF=args.dataPath+'/meta.'+args.prjName+'.yaml'
write_yaml(metaD,metaF)


outF5=args.dataPath+'/'+args.prjName+'_'+args.name+'.hd5'
write_data_hdf5(dataD,outF5)

gra=Plotter_MoonPermute(args,metaD )

gra.plot_frames(dataD,8)
gra.display_all(args,'gener')

