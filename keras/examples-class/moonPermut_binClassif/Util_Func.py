import numpy as np
import time,  os
import h5py
#import tensorflow as tf
import ruamel.yaml  as yaml

#...!...!..................
def read_yaml(ymlFn):
        print('  read  yaml:',ymlFn,end='')
        start = time.time()
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.Loader)
        ymlFd.close()
        print(' done, size=%d'%len(bulk),'  elaT=%.1f sec'%(time.time() - start))
        return bulk

#...!...!..................
def write_yaml(rec,ymlFn,verb=1):
        start = time.time()
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1048576
        if verb:
                print('  closed  yaml:',ymlFn,' size=%.2f MB'%xx,'  elaT=%.1f sec'%(time.time() - start))

#...!...!..................
def write_data_hdf5(dataD,outF):
        h5f = h5py.File(outF, 'w')
        print('save data as hdf5:',outF)
        for item in dataD:
           rec=dataD[item]
           h5f.create_dataset(item, data=rec)
        print('h5-write :',item, rec.shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB, frames=%d'%(xx,rec.shape[0]))

#...!...!..................
def read_data_hdf5(inpF):
        print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD

#...!...!..................
def produce_oneFrame(parD):
        mx_x=parD['max_x']
        mx_y=parD['max_y']
        radius=parD['radius']
        width=parD['width']
        numPoint=parD['numPoint']

        base_a=width+radius # distance to edges
        # generate  params normalized to unity
        U=np.random.uniform(-1,1,size=3)
        # phys center of circle, symbol do not touches the frame
        pos_x=(U[0]+1.)/2* (mx_x -2.05*base_a) + base_a
        pos_y=(U[1]+1.)/2* (mx_y -2.05*base_a) + base_a
        ang0=U[2]*np.pi
        P=[pos_x,pos_y,ang0]  # center + cut location (for moon)
 
        outL=[]
        lab=np.random.randint(0,2)
       
        for i in range(numPoint):
                d,l=np.random.uniform(0,1,size=2)
                l*=2*np.pi               
                if lab==1:  l*=0.6   # moon or sun                           
                ang=ang0+l
                tx=(radius+width*d)* np.sin(ang)
                ty=(radius+width*d)* np.cos(ang) 
                x=pos_x+tx
                y=pos_y+ty
                outL.append([x,y])
        return P,outL,lab

#...!...!..................
def produce_frames(parD,args):        
        out={'X':[],'Y':[],'AUX':[]}

        # fill meta-data record
        metaD={}
        for x in ['max_y','max_x','numPoint']:
            metaD[x]=parD[x]
        
        start = time.time()
        for i in range(args.events):
            P,frame,Y=produce_oneFrame(parD)
            if i%500==0:
                print('gen i=',i,'P,Y',P,Y)

            out['Y'].append(Y)
            out['X'].append(frame)
            out['AUX'].append(P)

        for item in out:
            out[item]=np.array(out[item]).astype(np.float32)
            if item=='X':
                    out[item]=np.swapaxes(out[item],0,1)
            print('gen ',item,out[item].shape)
        print('%d frames produced, elaT=%.1f sec'%(args.events,(time.time() - start)))
        return out, metaD
