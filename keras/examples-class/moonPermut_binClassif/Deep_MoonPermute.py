import os, time, sys
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
from tensorflow.keras.layers import Dense, maximum, Input, Dropout, Conv1D,MaxPool1D,Flatten
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint , ReduceLROnPlateau

from Util_Func import write_yaml, read_yaml
import numpy as np
import h5py

# this sepecail keras layer:
sys.path.append(os.path.abspath("../../superkeras-chanchana/"))
from permutational_layer import PairwiseModel, PermutationalEncoder,PermutationalLayer, move_item
from layers import repeat_layers

print('deep-libs imported elaT=%.1f sec'%(time.time() - start))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
from tensorflow.python.keras.callbacks import Callback
import tensorflow.keras.backend as K
import tensorflow as tf
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]   
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        #lr = K.eval(optimizer.lr * (1. / (1. + optimizer.decay * optimizer.iterations)))
        lr = K.eval(optimizer.lr)
        self.hir.append(lr)


#............................
#............................
#............................
class Deep_MoonPermute(object):

    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)
        print(self.__class__.__name__,'TF ver:', tf.__version__, 'prj:',self.prjName)
        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

        self.prjName2='%s-%s'%(self.prjName,self.modelDesign)
        # load meta-data file
        metaF=self.dataPath+'/meta.'+self.prjName+'.yaml'
        self.metaD=read_yaml(metaF)

        self.hparams={'lossName':'binary_crossentropy', 
                      'optimizerName': 'adam',
                      'reduceLR_factor':0.3} 

        self.data={}
        ''' data container holds only X-values
        data[dom][X,Y], where dom=train/val/test, X=func, Y=0/1
        '''


#............................
    def save_nput_hdf5(self):
        for dom in self.data:
            outF=self.dataPath+'/'+self.prjName+'_%s.hd5'%dom
            #print('save data as hdf5:',outF)
            h5f = h5py.File(outF, 'w')
            for xy in self.data[dom]:
                xobj=self.data[dom][xy]
                h5f.create_dataset(xy, data=self.data[dom][xy])
                #for x in h5f.keys(): 
                #    print(x,self.data[x].shape)
            h5f.close()
            xx=os.path.getsize(outF)/1048576
            print('closed  hdf5:',outF,' size=%.2f MB'%xx)

#............................
    def load_input_hdf5(self,domL):
        for dom in domL:
            inpF=self.dataPath+'/'+self.prjName+'_%s.hd5'%dom
            print('load hdf5:',inpF)
            h5f = h5py.File(inpF, 'r')
            self.data[dom]={}
            for xy in h5f.keys(): 
                npA=h5f[xy][:]
                if self.events>0:
                    mxe=self.events 
                    if dom!='train':  mxe=int(mxe/5) 
                    pres=int(npA.shape[0]/ mxe)
                    if pres>1: npA=npA[::pres]
                    print('reduced ',dom,xy,' to %d events'%npA.shape[0])
                self.data[dom][xy] = npA 
                print(' done',dom,xy,self.data[dom][xy].shape)

                if xy=='X': # repack X as list of arrays , TMP
                    if 'perm' in self.modelDesign:
                        self.data[dom][xy] =[ npA[i] for i in range(npA.shape[0])]
                        print(' re-done',dom,xy,len(self.data[dom][xy]))
                    elif self.modelDesign=='cnn':
                        self.data[dom][xy]=np.swapaxes(npA,0,1)
                        print(' re-done',dom,xy,self.data[dom][xy].shape)
                    else:
                        print('unknow design=%s, ABORTING-1'%self.modelDesign)
                        exit(99)
              
            h5f.close()

        print('load_input_hdf5 done, elaT=%.1f sec'%(time.time() - start))



#...!...!..................
    def build_perm_model(self,X):
        one_shape=(X[0].shape[1],)
        num_inputs=len(X)
        perm_pool=maximum

        print('\nPER step-A - - - - - - - -')
        print('PER: 1 point_shape=',one_shape,' num_inputs=',num_inputs)
        pairwise_op = PairwiseModel(
            one_shape, repeat_layers(Dense, [20,10,5], name="pairFC"), name="pairwise_op"
            )
        mod=pairwise_op
        print('PER: PairOP  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))
        mod.summary()

        if self.verb>1:
            outF='pairwise_op.graph.svg'
            plot_model(pairwise_op, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M: saved1 ',outF)

        print('\nPER step-B - - - - - - - ')
        encoderRow_op = PermutationalEncoder(pairwise_op,num_inputs , name="encoder_row",encode_identical=True)

        mod=encoderRow_op
        print('PER: EncoderRow  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if self.verb>1:
            outF='encoderRow_op.graph.svg'
            plot_model(encoderRow_op, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved2 ',outF)


        print('\nPER step-C - - - - - - ')
        perm_layer = PermutationalLayer(encoderRow_op, name="permutational_layer", pooling=perm_pool)

        mod=perm_layer
        print('PER: perm_layer  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers), 'pool=',type(perm_pool))
        if self.verb>1:
            outF='permLyr.graph.svg'
            plot_model(perm_layer, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved3 ',outF)

        print('\nPER step-D - - - - - - \n add few common FC layers --> sigmoid')
        # append sigmoid layer
        model1=perm_layer
        h=model1.layers[-1].output # the last layer
        h= Dense(3, activation='relu',name='common')(h)
        outputs= Dense(1, activation='sigmoid',name='sigmoid')(h)
        model2 = Model(model1.inputs, outputs)
        self.model=model2


#...!...!..................
    def build_model(self):
        X=self.data['train']['X']
        Y=self.data['train']['Y']
        
        num_outputs=1
        assert Y.ndim==num_outputs

        if self.modelDesign=='perm':
            self.build_perm_model(X)
        elif self.modelDesign=='cnn':
            self.build_cnn_model(X)
            self.model.summary()
        else:
            print('unknow design=%s, ABORTING-2'%self.modelDesign)
            exit(99)

        outF='%s/%s.graph.svg'%(self.outPath,self.prjName2)
        plot_model(self.model, to_file=outF, show_shapes=True, show_layer_names=True)
        print('build_model saved: ',outF)

 
#...!...!..................
    def build_cnn_model(self,X):
        dropFrac=self.dropFrac

        sh1=X.shape
       
        xa = Input(shape=(sh1[1],sh1[2]),name='inp1')        
        print('build_cnn_model inp:',sh1,'design=',self.modelDesign)
        kernel = 3
        pool_len = 3 # how much time_bins get reduced per pooling
        cnnDim=[8,12]
        cnnDim=[]
        numCnn=len(cnnDim)
        print(' cnnDim:',cnnDim)
        h=xa
        for i in range(numCnn):
            dim=cnnDim[i]
            h= Conv1D(dim,kernel,activation='relu', padding='valid',name='cnn%d_d%d_k%d'%(i,dim,kernel))(h)
            h= MaxPool1D(pool_size=pool_len, name='pool_%d'%(i))(h)
            print('cnn',i,h.get_shape())

        h=Flatten(name='to_1d')(h)

        print('end FC=>',h.get_shape())
        h = Dropout(dropFrac,name='dropFC')(h)

        # .... FC  layers  COMMON 
        fcDim=[10,5]; numFC=len(fcDim)

        for i in range(numFC):
            dim = fcDim[i]
            h = Dense(dim,activation='relu',name='fc%d'%i)(h)
            h = Dropout(dropFrac,name='drop%d'%i)(h)
            print('fc',i,h.get_shape())

        y= Dense(1, activation='sigmoid',name='sigmoid')(h)
        # full model
        model = Model(inputs=xa, outputs=y)
        self.model=model



    #............................
    def compile_model(self): 
        print('compile_model: loss=',self.hparams['lossName'],' optName=',self.hparams['optimizerName'])
        self.model.compile(optimizer=self.hparams['optimizerName'], loss=self.hparams['lossName'], metrics=['accuracy'])

        print('model size=%.1fK compiled elaT=%.1f sec'%(self.model.count_params()/1000.,time.time() - start))
        


    #............................
    def train_model(self):
        X=self.data['train']['X']
        Y=self.data['train']['Y']
        X_val=self.data['val']['X']
        Y_val=self.data['val']['Y']

        callbacks_list = []
        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)

        if self.earlyStopPatience>0:
            earlyStop=EarlyStopping(monitor='val_loss', patience=self.earlyStopPatience, verbose=1, min_delta=2.e-4, mode='auto')
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping, patience=',self.earlyStopPatience)
        

        if self.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.outPath+'/'+self.prjName2+'.weights_best.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=4)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint')

        if self.reduceLRPatience>0:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=self.hparams['reduceLR_factor'], patience=self.reduceLRPatience, min_lr=0.0, verbose=1,min_delta=0.003)
            callbacks_list.append(redu_lr)
            print('enabled ReduceLROnPlateau, patience=',self.reduceLRPatience,',factor=',self.hparams['reduceLR_factor'])


        print('\nTrain_model X:',len(X), ' epochs=',self.epochs,' batch=',self.batch_size)
        startTm = time.time()

        model=self.model

        hir=model.fit(X,Y, callbacks=callbacks_list,
                      validation_data=(X_val,Y_val),
                      shuffle=True,
                      batch_size=self.batch_size, 
                      epochs=self.epochs, verbose=1)

        self.train_hirD=hir.history
        self.train_hirD['lr']=lrCb.hir
        #print('TgotA', self.train_hirD.keys())
        #print('TgotA2',lrCb.hir)

        #evaluate performance for the last epoch
        loss=self.train_hirD['val_loss'][-1]
        fitTime=time.time() - start
        print('\n End Validation  Loss:%.3f'%loss,', fit time=%.1f sec'%(fitTime))
        self.train_sec=fitTime


#...!...!..................
    def make_prediction(self,dom):
        X=self.data[dom]['X']
        Yhot=self.data[dom]['Y']

        if 'perm' in self.modelDesign:
            nX=X[0].shape[0]
        elif self.modelDesign=='cnn':
            nX=X.shape[0]
        print('make_prediction, dom=',dom,' nX=',nX)
        Yprob = self.model.predict(X).flatten()
        print('Yprob',Yprob.shape)
        return X,Yhot,Yprob

 
#...!...!..................
    def save_model_full(self):
        outF=self.outPath+'/'+self.prjName2+'.model.h5'
        print('save model full to',outF)
        self.model.save(outF)
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%xx)
 
    #............................
    def load_model_full(self,path='',verb=1):
        fname=path+'/'+self.prjName2+'.model.h5'
        print('load model from ',fname)
        self.model=load_model(fname) # creates model from HDF5
        if verb and  'perm'  not in self.modelDesign:
            self.model.summary()

    #............................
    def load_weights(self,name):
        start = time.time()
        outF5m=self.prjName2+'.%s.h5'%name
        print('load  weights  from',outF5m,end='... ')
        self.model.load_weights(outF5m) # creates mode from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))

