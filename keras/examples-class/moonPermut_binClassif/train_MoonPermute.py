#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_MoonPermute import Plotter_MoonPermute
from Deep_MoonPermute import Deep_MoonPermute
import tensorflow as tf

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign',  default='perm',
                        choices=['cnn','perm'], help=" model design of the network")

    parser.add_argument("-d", "--dataPath",help="output path",  default='data')
    parser.add_argument("-o", "--outPath",
                        default='out',help="output path for plots and tables")


    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    parser.add_argument("-e", "--epochs", type=int, default=5,
                        help="fitting epoch")
    parser.add_argument("-b", "--batch_size", type=int, default=128,
                        help="fit batch_size")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", type=int,
                         dest='earlyStopPatience', default=10,
                         help="early stop:  epochs w/o improvement (aka patience), 0=off")
 
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLR", dest='reduceLRPatience', type=int, default=5,help="reduce learning at plateau, patience")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disables X-term for batch mode")

    args = parser.parse_args()
    args.prjName='moonPermute'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_MoonPermute(**vars(args))
gra=Plotter_MoonPermute(args,deep.metaD)
deep.load_input_hdf5(['train','val'])

''' #dump 1 input
xx=deep.data['val']['X']
yy=deep.data['val']['Y']
num_inputs=len(xx)
ieve=7
print('dump eve=%d, y=%d'%(ieve,yy[ieve]))
for i in range(num_inputs):
    print('i, pt:',i,xx[i][ieve])
'''


#gra.plot_frames(deep.data['val'],6)

deep.build_model() 
#gra.plot_model(deep)

deep.compile_model()
deep.train_model()

deep.save_model_full() 
X,Yhot,Yprob=deep.make_prediction('val')


gra.plot_train_history(deep,args)
gra.plot_labeled_scores(Yhot,Yprob,'val',figId=-10)
gra.plot_AUC(Yhot,Yprob,'val',args,figId=-10)

gra.display_all(args,'train')    


