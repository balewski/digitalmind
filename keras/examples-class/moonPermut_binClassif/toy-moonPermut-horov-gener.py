#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
https://www.pyimagesearch.com/2018/12/24/how-to-use-keras-fit-and-fit_generator-a-hands-on-tutorial/

 example of simple FC+horovod +generator(file)
 trains
saves model
reads model
predicts
uses generator for input

See examples of data generators:
https://github.com/keras-team/keras/issues/7729

****Run on Cori GPUs
ssh cori 
module load escori 
module load cuda
module load tensorflow/gpu-1.13.1-py36 

salloc -C gpu -n 80 --gres=gpu:8 -Adasrepo -t4:00:00

# run training w/o GPU-load monitor

srun  -n 1 bash -c '  python -u  toy-moonPermut-horov-gener.py -G'
# run with 1 GPU-load monitor
srun  -n 2   bash -c ' hostname & if [ $SLURM_PROCID -eq 0 ] ; then  nvidia-smi -l >&L.smi ; fi &  python -u toy-moonPermut-horov-gener.py'

srun bash -c '  nvidia-smi '

******  Run on Cori CPUs , all on 1 node as single task
ssh cori
module load tensorflow/intel-1.12.0-py36
./toy-moonPermut-horov-gener  --noHorovod     --steps 5

If you see: HD5 OSError: Unable to create file (unable to lock file, errno = 524..
 export HDF5_USE_FILE_LOCKING=FALSE 

'''

import socket  # for hostname
import os, time, sys
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import backend as K
from tensorflow.keras.layers import  Dense, Input, maximum,Conv1D,MaxPool1D,Flatten,Dropout
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.utils import plot_model
import horovod.tensorflow.keras as hvd 
import h5py

sys.path.append(os.path.abspath("/global/homes/b/balewski/digitalMind/keras/superkeras-chanchana/"))
from permutational_layer import PairwiseModel, PermutationalEncoder,PermutationalLayer, move_item
from layers import repeat_layers

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

import argparse

parser = argparse.ArgumentParser()
parser.add_argument( "--noHorovod", dest='useHorovod',  action='store_false',
                     default=True, help="disable Horovod to run on CPU")
#parser.add_argument( "-G","--useGen", dest='',  action='store_true',
#                     default=False, help="create data as file, use generator to read & feed training")
parser.add_argument("-b", "--batch_size", type=int, default=4096,
                        help="training batch_size")
parser.add_argument("-e", "--epochs", type=int, default=16,
                        help="training epochs")
parser.add_argument("-s", "--steps", type=int, default=80,
                        help="steps per training epochs")
parser.add_argument("--seedWeights",default=None,
                    help="seed weights only, after model is created")
parser.add_argument("--lr", type=float, default=1.,
                    help="learning rate for Adadelta optimizer")
parser.add_argument("--dropFrac", type=float, default=0.02,
                    help="drop fraction at all layers")
parser.add_argument("-d","--dataPath",help="path to input",
                        default='data/')
parser.add_argument("-o", "--outPath",
                    default='out/',help="output path for plots and tables")


#...!...!..................
def moonPermut_inputGeneratorFnc(inputPath, bs):    
    # open the HD5 file for reading
    blob=read_data_hdf5(inputPath)
    j=0
    mxJ=blob['Y'].shape[0]
    protSetSize=blob['X'].shape[0]
    # loop indefinitely
    while True:
        if j>=mxJ-1-bs: j=0 # reset pointer if not enough for next batch
        Xall=blob['X'][:,j:j+bs]
        Y=blob['Y'][j:j+bs]
        j+=bs
        #print('ww',Xall.shape)
        X=[ Xall[i] for i in range(protSetSize) ]
        yield (X,Y)

#...!...!..................
def read_data_hdf5(inpF):
        print('\nread data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD

#...!...!..................
def build_permu_model(one_shape,num_inputs,dropFrac ):
        print('\nPER step-A - - - - - - - -')
        print('PER: 1 point_shape=',one_shape,' num_inputs=',num_inputs)
        perm_pool=maximum

        print('\nPER step-A - - - - - - - -')
        pairwise_op = PairwiseModel(
            one_shape, repeat_layers(Dense, [20,10,5], name="pairFC"), name="pairwise_op"
            )
        mod=pairwise_op
        print('PER: PairOP  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))
        mod.summary()

        if verb>1:
            outF='pairwise_op.graph.svg'
            plot_model(pairwise_op, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M: saved1 ',outF)

        print('\nPER step-B - - - - - - - ')
        encoderRow_op = PermutationalEncoder(pairwise_op,num_inputs , name="encoder_row",encode_identical=True)

        mod=encoderRow_op
        print('PER: EncoderRow  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

        if verb>1:
            outF='encoderRow_op.graph.svg'
            plot_model(encoderRow_op, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved2 ',outF)


        print('\nPER step-C - - - - - - ')
        perm_layer = PermutationalLayer(encoderRow_op, name="permutational_layer", pooling=perm_pool)

        mod=perm_layer
        print('PER: perm_layer  inp=',len(mod.input_shape),',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers), 'pool=',type(perm_pool))
        if verb>1:
            outF='permLyr.graph.svg'
            plot_model(perm_layer, to_file=outF, show_shapes=True, show_layer_names=True)
            print('M:saved3 ',outF)

        print('\nPER step-D - - - - - - \n add few common FC layers --> sigmoid')
        # append sigmoid layer
        model1=perm_layer
        h=model1.layers[-1].output # the last layer
        h= Dense(3, activation='relu',name='common')(h)
        outputs= Dense(1, activation='sigmoid',name='sigmoid')(h)
        model2 = Model(model1.inputs, outputs)
        return model2

        
#...!...!..................


#=================================
#=================================
#  M A I N 
#=================================
#=================================

args = parser.parse_args()
args.useGenerator=True
verb=1
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
for xx in [args.dataPath,args.outPath]:
        assert  os.path.exists(xx)

if args.useHorovod:
    hvd.init()
    gpu_count= hvd.size()
    myRank= hvd.rank()
    print(' Horovod: initialize Horovod, myRank=%d of %d'%(myRank,gpu_count))
    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = str(hvd.local_rank())
    K.set_session(tf.Session(config=config))
else:
    gpu_count= 1
    myRank=0
    print('Horovod disabled, gpu_count=',gpu_count)
    

batch_size=16*2

if args.useGenerator:
    print(' initialize both the training and testing image generators')
    inpF=args.dataPath+'moonPermute_train.hd5'
    trainGen = moonPermut_inputGeneratorFnc(inpF, batch_size)
    inpF=args.dataPath+'moonPermute_val.hd5'
    valGen = moonPermut_inputGeneratorFnc(inpF, batch_size)
    xx,yy =next(valGen)  # X=[ np(batch,one_shape) ]_item

    one_shape=xx[0][0].shape  
    num_inputs=len(xx)
    assert batch_size == xx[0].shape[0]
    print('input generator activated, X num_inputs=',num_inputs,' one_shape=',one_shape)
    ''' #dump 1 input
    ieve=7
    print('dump eve=%d, y=%d'%(ieve,yy[ieve]))
    for i in range(num_inputs):
          print('i, pt:',i,xx[i][ieve])
    '''

steps=150
epochs=20
if myRank>0 : verb==0

model= build_permu_model(one_shape,num_inputs,args.dropFrac)


myCallbacks = []
LR=args.lr
if args.useHorovod:
    #LR=1 *gpu_count
    opt = keras.optimizers.Adadelta(lr=LR)
    #Adadelta.__init__(lr=1.0,rho=0.95,epsilon=None,decay=0.0,**kwargs)

    #LR/1000.; opt = keras.optimizers.Adam(lr=LR)  # explodes for nGpu>1
    print('Horovod: adjust learning rate to',LR)
    print(opt)

    # Horovod: add Horovod Distributed Optimizer.
    myOpt = hvd.DistributedOptimizer(opt)
    # Horovod: broadcast initial variable states from rank 0 to all other processes.
    # This is necessary to ensure consistent initialization of all workers when
    # training is started with random weights or restored from a checkpoint.
    myCallbacks.append(hvd.callbacks.BroadcastGlobalVariablesCallback(0))
else:
    myOpt = keras.optimizers.Adadelta(lr=LR)
    #myOpt='adam'

print('compile rank=%d LR=%.2f ...'%(myRank,LR))
model.compile(optimizer=myOpt, loss='binary_crossentropy', metrics=['accuracy'])

#if myRank == 0: model.summary() # will print

if args.seedWeights=='same' :  args.seedWeights=args.outPath
if args.seedWeights:
    inpF5=args.seedWeights+'/jan-FC-ok0.model.h5' # val_loss: 0.25
    #inpF5=args.seedWeights+'/jan-FC-ok1.model.h5' # val_loss: 0.0138
    print(myRank,'restore weights from HDF5:',inpF5)
    model.load_weights(inpF5) 
    

# Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
#if hvd.rank() == 0:
#    callbacks.append(keras.callbacks.ModelCheckpoint(outPath+'/checkpoint-{epoch}.h5'))


print('fit...rank=',myRank)
startTm = time.time()

assert  args.useGenerator
model.fit_generator(trainGen, callbacks=myCallbacks, epochs=epochs, steps_per_epoch=steps ,validation_data=valGen, validation_steps=steps//8)


fitTime=time.time() - startTm
print('M:fit done  elaT=%.1f sec, BS=%d epochs=%d'%(fitTime,batch_size,epochs))

if myRank > 0: exit(0)
# no need to repeat it for all ranks

print("-----------  Save model as HD5 -----------")
outF=args.outPath+'jan-FC.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from HD5 -----------")
model5=load_model(outF)

print('M: fit done, predicting')

if args.useGenerator:
    Zval=model5.predict_generator(valGen,steps=3)
else:
    Zval=model5.predict(Xval)
print('M: done, Z:',Zval.shape,Yval.shape)
ZmY=Zval-Yval
# now compute MSE
sqErr=np.power(ZmY,2)
print('ss',sqErr.shape)
sqErr=sqErr.mean(axis=1)
print('ss',sqErr.shape)
msqErr=sqErr.mean()
ssqErr=sqErr.std()
print('computed segment MSE mean=%.4f +/- %.4f'%(msqErr,ssqErr))



