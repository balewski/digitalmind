#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_SquareRegr import Plotter_SquareRegr
from Deep_SquareRegr import Deep_SquareRegr
import tensorflow as tf

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--design', dest='modelDesign',  default='cnn',
                        choices=['fc','cnn','lstm'], help=" model design of the network")

    parser.add_argument("--dataPath",help="output path",  default='data')
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-g","--nGPU", type=int, default=0,
                        help="num GPU >=2 activates keras.multi_gpu_model, use 0   for 1 GPU or CPU")
    parser.add_argument("--use-generator", dest='useGenerator',
                         action='store_true', default=False,
                         help="feeds data thrugh generator, slower on GPUs")

    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    parser.add_argument("-e", "--epochs", type=int, default=3,
                        help="fitting epoch")
    parser.add_argument("-b", "--batch_size", type=int, default=128,
                        help="fit batch_size")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", dest='earlyStopOn',
                         action='store_true',default=False,help="enable early stop")
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLr", dest='reduceLearn',
                         action='store_true',default=False,help="reduce learning at plateau")
    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disables X-term for batch mode")

    args = parser.parse_args()
    args.prjName='squareRegr'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

gra=Plotter_SquareRegr(args)
deep=Deep_SquareRegr(**vars(args))
deep.load_input_hdf5(['train','val'])

#gra.plot_input_digit(deep.data['val'],range(4))

#1, instantiate your base model on a cpu

if args.nGPU!=1: 
    with tf.device("/cpu:0"):
        deep.build_model() 
else: # looks strange, for 1GPU no tf.divice()
    deep.build_model() 

if args.nGPU==0: 
    print('use only CPU for training, no GPU')
    gra.plot_model(deep)
    
#2, put your model to multiple gpus
if args.nGPU>1:
    from tensorflow.python.keras.utils import multi_gpu_model
    cpu_relocation=True # merge on CPU using cpu_relocation (makes no differrence on speed)
    print('clone model for %d GPUs'%args.nGPU,'cpu_relocation=',cpu_relocation )
    deep.multi_model = multi_gpu_model(deep.model, gpus=args.nGPU,cpu_relocation=cpu_relocation)

#3, compile both models
deep.compile_model()
if args.nGPU>1:
    deep.multi_model.compile(loss=deep.lossName, optimizer=deep.optimizerName)
    print('multi_model.compile done')



deep.train_model()


deep.save_model_full() 

gra.plot_train_history(deep,args)
gra.display_all(args,'train')


