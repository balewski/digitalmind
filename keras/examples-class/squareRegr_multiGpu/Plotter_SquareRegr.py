import os
import numpy as np
from matplotlib import cm as cmap
from tensorflow.python.keras.utils import plot_model

import time

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_SquareRegr(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args):
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        self.maxU=1.2
        
        for xx in [ self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

    #............................
    def display_all(self,args,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()                
                figName='%s/%s_%s_f%d'%(self.outPath,args.prjName,ext,fid)
                print('Graphics saving to %s  ...'%figName)
                self.plt.savefig(figName+'.png')
        self.plt.show()

# figId=self.smart_append(figId)
#...!...!....................
    def smart_append(self,id): # increment id if re-used
        while id in self.figL: id+=1
        self.figL.append(id)
        return id
#............................
    def plot_model(self,deep):

        fname='%s/%s.graph.svg'%(self.outPath,deep.prjName)
        plot_model(deep.model, to_file=fname, show_shapes=True, show_layer_names=True)
        print('Graph saved as ',fname)


#............................
    def plot_frames(self,dataA,nFr,idxL=[],pred=0,figId=7):
        figId=self.smart_append(figId)
        X=dataA['X']
        Y=dataA['Y']
        Aux=dataA['AUX']
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,6))

        nPar=len(Y[0])
        addPred= type(pred)==type(np.zeros(1))

        if addPred:
            phys_range=self.metaD['phys_range']
        
            print('pprr',phys_range)
            physZoff=[]; physZfac=[]        
            for rec in phys_range:
                print('rec',rec)
                physZoff.append((rec[1]+rec[0])/2.)
                physZfac.append((rec[1]-rec[0])/2.)

            print(physZoff,physZfac)
            physZoff=np.array(physZoff)
            physZfac=np.array(physZfac)

        if len(idxL)==0:
            mxFr=len(X) #X.shape[0]
            idxL=np.random.randint(mxFr,size=(nFr))

        nFr=min(len(idxL),nFr)
        nrow,ncol=2,2
        print('plot input for trace idx=',idxL, type(pred))
        j=0
        for it in idxL[:nFr]:
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j)
            j+=1
            frame=X[it]
            P=Aux[it]
            print('P=',P,frame.shape)
            ax.imshow(frame, vmax=1.1,cmap="YlGnBu", interpolation='nearest',origin='lower')

            ax.scatter(P[0],P[1],c='r') #s=20,
            
            if addPred:
                Z=pred[it]
                #print('u Z=',Z)
                Z=Z*physZfac+physZoff
                #print('p Z=',Z)
                #print('t P=',P)
                ax.scatter(Z[0],Z[1],c='b',marker='X')#,linewidth=0.3)


#............................
    def plot_train_history(self,deep,args,figId=10):
        figId=self.smart_append(figId)
        
        self.plt.figure(figId,facecolor='white', figsize=(9,4))
        
        nrow,ncol=3,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2, rowspan=2 )
        
        DL=deep.train_hirD
        nEpochs=len(DL['val_loss'])
        epoV=[i for i in range(1,nEpochs+1)]

        valLoss=DL['val_loss'][-1]
        tit1='%s, train %.1f min, end-val=%.3g'%(deep.prjName,deep.train_sec/60.,valLoss)
        ax1.set(ylabel='loss = %s'%deep.lossName,title=tit1,xlabel='epochs')            # use Y.shape
        ax1.plot(epoV,DL['loss'],'.-.',label='train n=%d'%deep.data['train']['X'].shape[0])
        ax1.plot(epoV,DL['val_loss'],'.-',label='val n=%d'%deep.data['val']['X'].shape[0])
        ax1.legend(loc='best')
        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')
        
        if 'lr' not in DL: return
        #print('sss2',len(DL['lr']))
        #if 2*nEpochs==len(DL['lr']): DL['lr']=DL['lr'][:nEpochs] #tmp hack

        ax3 = self.plt.subplot2grid((nrow,ncol), (2,1), colspan=2,sharex=ax1 )

        ax3.plot(epoV,DL['lr'][-nEpochs:],'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate')    

            
#............................
    def plot_fparam_residua(self,U,Z,figId=9,tit=''):
        figId=self.smart_append(figId)


        parName=self.metaD['parName']
        nPar=self.metaD['numPar']

        nrow,ncol=3,nPar
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        inSz=2.5
        self.plt.figure(figId,facecolor='white', figsize=(inSz*nPar*1.2,nrow*inSz))

        assert U.shape[1]==Z.shape[1]
        self.parSum=[]

        j=0
        mm=self.maxU
        binsU= np.linspace(-mm*1.5,mm*1.5,90)
        binsX= np.linspace(-mm,mm,30)
        binsY= np.linspace(-mm/2,mm/2,30)

        for iPar in range(0,nPar):
            ax1 = self.plt.subplot(nrow,ncol, j+1)
            ax3 = self.plt.subplot(nrow,ncol, j+1+ncol*2)
            ax2 = self.plt.subplot(nrow,ncol, j+1+ncol*1)
            ax1.grid(); ax2.grid(); ax3.grid()

            u=U[:,iPar]
            z=Z[:,iPar]

            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            # beutification
            self.plt.colorbar(img, ax=ax1)
            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=1,transform=ax1.transAxes) #diagonal
            # 

            ax1.set(title='corr %s'%parName[iPar], xlabel='pred Z_%d'%iPar, ylabel='true U_%d'%iPar)
            
            if iPar==0: ax1.text(0.1,0.9,tit,transform=ax1.transAxes)
            if iPar==1: ax1.text(0.1,0.9,'n=%d'%(u.shape[0]),transform=ax1.transAxes)
                 
                        
            # .... compute residua 
            zmu=z-u
            resM=zmu.mean()
            resS=zmu.std()

            # ......  2D residua
            zsum,xbins,ybins,img = ax3.hist2d(z,zmu,bins=[binsX,binsY], cmin=1,
                                   cmap = self.plt.cm.rainbow)
            self.plt.colorbar(img, ax=ax3)
            ax3.set( xlabel='pred %s'%parName[iPar], ylabel='residue Z-U')
            ax3.axhline(0, color='green', linestyle='--')
            ax3.set_xlim(-mm,mm) ; ax3.set_ylim(-mm/2,mm/2)

            # ..... 1D residua
            ax2.hist(zmu,bins=binsU)

            ax2.set(title='avr res%d: %.2f+/-%.2f'%(iPar,resM,resS), xlabel='residue %d: %s'%(iPar,parName[iPar]), ylabel='traces')

            ax2.set_xlim(-mm/2,mm/2)

            j+=1

        return

  
#............................
    def plot_data_fparams(self,U,tit,figId=6):
        figId=self.smart_append(figId)
        parName=self.metaD['parName']
        nPar=self.metaD['numPar']
        
        nrow,ncol=2,int(nPar/2+1.5) # displays 2 pars per plot
 
        fig=self.plt.figure(figId,facecolor='white', figsize=(3.2*ncol,2.5*nrow))

        mm=self.maxU
        binsX= np.linspace(-mm,mm,30)

        j=1
        for i in range(0,nPar,2):
            iPar=min(i,U.shape[1]-2) # account for odd num of params
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            self.plt.colorbar(img, ax=ax)
            ax.set(title=tit, xlabel='%d: %s'%(iPar,parName[iPar]), ylabel='%d: %s'%(iPar+1,parName[iPar+1]))
            ax.grid()

        for i in range(0,nPar):
            y1=U[:,i]
            ax=self.plt.subplot(nrow,ncol,j) ; j+=1
            ax.hist(y1,bins=50)
            ax.set(xlabel='%d: %s'%(i,parName[i]))
            ax.set_xlim(-mm,mm)
            ax.grid()
