import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()

from tensorflow.python.keras.models import Model, load_model
from tensorflow.python.keras.callbacks import EarlyStopping, ModelCheckpoint , ReduceLROnPlateau
from tensorflow.python.keras.layers import Dense, Dropout,   Input, Conv2D,MaxPool2D,Flatten,Reshape,LSTM, Lambda

import numpy as np
import h5py
print('deep-libs imported elaT=%.1f sec'%(time.time() - start))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
from tensorflow.python.keras.callbacks import Callback
import tensorflow.keras.backend as K
import tensorflow as tf
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]   
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        #lr = K.eval(optimizer.lr * (1. / (1. + optimizer.decay * optimizer.iterations)))
        lr = K.eval(optimizer.lr)
        self.hir.append(lr)


#............................
#............................
#............................
class Deep_SquareRegr(object):

    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)
        print(self.__class__.__name__,'TF ver:', tf.__version__, 'prj:',self.prjName)

        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

        self.data={}
        ''' data container holds only X-values
        data[dom][X,Y], where dom=train/val/test, X=func, Y=0/1
        '''


#............................
    def save_input_hdf5(self):
        for dom in self.data:
            outF=self.dataPath+'/'+self.prjName+'_%s.hd5'%dom
            #print('save data as hdf5:',outF)
            h5f = h5py.File(outF, 'w')
            for xy in self.data[dom]:
                xobj=self.data[dom][xy]
                h5f.create_dataset(xy, data=self.data[dom][xy])
                #for x in h5f.keys(): 
                #    print(x,self.data[x].shape)
            h5f.close()
            xx=os.path.getsize(outF)/1048576
            print('closed  hdf5:',outF,' size=%.2f MB'%xx)

#............................
    def load_input_hdf5(self,domL):
        for dom in domL:
            inpF=self.dataPath+'/'+self.prjName+'_%s.hd5'%dom
            print('load hdf5:',inpF)
            h5f = h5py.File(inpF, 'r')
            self.data[dom]={}
            for xy in h5f.keys(): 
                npA=h5f[xy][:]
                if self.events>0:
                    mxe=self.events 
                    if dom!='train':  mxe=int(mxe/5) 
                    pres=int(npA.shape[0]/ mxe)
                    if pres>1: npA=npA[::pres]
                    print('reduced ',dom,xy,' to %d events'%npA.shape[0])
                self.data[dom][xy] = npA 
                print(' done',dom,xy,self.data[dom][xy].shape)
            h5f.close()
        print('load_input_hdf5 done, elaT=%.1f sec'%(time.time() - start))


    #............................
    def build_model(self):
        start = time.time()
        dropFrac=self.dropFrac
        sh1=self.data['train']['X'].shape
        out_dim=self.data['train']['Y'].shape[1]

        xa = Input(shape=(sh1[1],sh1[2]),name='inp1')        
        print('build_model inp1:',sh1,'out_dim:',out_dim,'design=',self.modelDesign)

        if self.modelDesign=='cnn': # start . . . . . . . . . . . . . . . 
            h=Reshape((sh1[1],sh1[2],1))(xa)
            kernel = 5
            pool_len = 3 # how much time_bins get reduced per pooling
            cnnDim=[2,4]
            cnnDim=[4,8,12]
            numCnn=len(cnnDim)
            print(' cnnDim:',cnnDim)
            for i in range(numCnn):
                dim=cnnDim[i]
                h= Conv2D(dim,kernel,activation='relu', padding='valid',name='cnn%d_d%d_k%d'%(i,dim,kernel))(h)
                h= MaxPool2D(pool_size=pool_len, name='pool_%d'%(i))(h)
                print('cnn',i,h.get_shape())

            h=Flatten(name='to_1d')(h)

        if self.modelDesign=='fc': # start . . . . . . . . . . . . . . . 
            h=Flatten(name='to_1d')(xa)
            fcDim=[200,100,100,100]; numFc=len(fcDim)
            print(' fcDim:',fcDim, 'inpDim=',h.get_shape())            
            for i in range(numFc):
                dim=fcDim[i]
                h = Dense(dim,activation='relu',name='afc%d'%i)(h)
                h = Dropout(dropFrac,name='adrop%d'%i)(h)

        if self.modelDesign=='lstm': # start . . . . . . . . . . . . . . . 
            h=xa
            lstmDim=32; numLstm=10
            recDropFrac=0.5*dropFrac
            print(' lstmDim:',lstmDim,'numLstm:',numLstm,'h_in:',h.get_shape())
            for j in range(numLstm-1):
                h= LSTM(lstmDim, activation='tanh',recurrent_dropout=recDropFrac,dropout=dropFrac,name='lstm%c_%d'%(65+j,lstmDim),return_sequences=True) (h)

            h= LSTM(lstmDim, activation='tanh',recurrent_dropout=recDropFrac,dropout=dropFrac,name='lstm%c_%d'%(65+j+1,lstmDim),return_sequences=False) (h)


        print('end FC=>',h.get_shape())
        h = Dropout(dropFrac,name='dropFC')(h)

        # .... FC  layers  COMMON 
        fcDim=[20,20]; numFC=len(fcDim)

        for i in range(numFC):
            dim = fcDim[i]
            h = Dense(dim,activation='relu',name='fc%d'%i)(h)
            h = Dropout(dropFrac,name='drop%d'%i)(h)
            print('fc',i,h.get_shape())

        lastAct='tanh'
        outAmplFact=1.2
        y= Dense(out_dim, activation=lastAct,name='out_%s'%lastAct)(h)
        y = Lambda(lambda val: val*outAmplFact, name='scaleAmpl_%.1f'%outAmplFact)(y)
        #,' out:',y.get_shape()
        # full model
        model = Model(inputs=xa, outputs=y)
        self.model=model

    #............................
    def compile_model(self):
        self.lossName='mse'
        self.optimizerName='adam'

        print('compile_model: loss=',self.lossName,' optName=',self.optimizerName)
        self.model.compile(optimizer=self.optimizerName, loss=self.lossName)

        self.model.summary() # will print
        print('model size=%.1fK compiled elaT=%.1f sec'%(self.model.count_params()/1000.,time.time() - start))
        


    #............................
    def train_model(self):
        X=self.data['train']['X']
        Y=self.data['train']['Y']
        X_val=self.data['val']['X']
        Y_val=self.data['val']['Y']

        callbacks_list = []
        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)
        
        if self.earlyStopOn:
            earlyStop=EarlyStopping(monitor='val_loss', patience=10, verbose=1, mode='auto',min_delta=0.001)
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping')

        if self.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.pejName+'.weights_best.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=4)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint')

        if self.reduceLearn:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=5, min_lr=0.0, verbose=1,epsilon=0.01)
            callbacks_list.append(redu_lr)
            print('enabled ReduceLROnPlateau')

        print('\nTrain_model X:',X.shape, ' earlyStop=',self.earlyStopOn,' epochs=',self.epochs,' batch=',self.batch_size,' nGPU=',self.nGPU)
        startTm = time.time()

        if self.nGPU<2:
            model=self.model
        else:
            model=self.multi_model

        if not self.useGenerator:
            print('model.fit(np.array), step is 1 event')
            hir=model.fit(X,Y, callbacks=callbacks_list,
                          validation_data=(X_val,Y_val),
                          shuffle=True,
                          batch_size=self.batch_size, 
                          epochs=self.epochs, verbose=1)
        else:
            print('model.fit_generator(), step is 1 batch')
            from Util_Func import np_to_generator
            steps=int(X.shape[0]/self.batch_size)
            steps_val=int(X_val.shape[0]/self.batch_size)
            print('fit_generator(..), steps=',steps,'epoch_size=',steps*self.batch_size,' no Shuffle',' nGPU=',self.nGPU,'steps_val=',steps_val)
            hir=model.fit_generator(
                np_to_generator(X,Y,self.batch_size),
                validation_data=np_to_generator(X_val,Y_val,self.batch_size), 
                validation_steps=steps_val,callbacks=callbacks_list,
                epochs=self.epochs, steps_per_epoch=steps, verbose=1)

        self.train_hirD=hir.history
        self.train_hirD['lr']=lrCb.hir
        print('gotA', self.train_hirD.keys())
        print('gotA2',lrCb.hir)

        #evaluate performance for the last epoch
        loss=self.train_hirD['val_loss'][-1]
        fitTime=time.time() - start
        print('\n End Validation  Loss:%.3f'%loss,', fit time=%.1f sec'%(fitTime))
        self.train_sec=fitTime

 
    #............................
    def save_model_full(self):
        outF=self.outPath+'/'+self.prjName+'.'+self.modelDesign+'.model_full.h5'
        print('save model full to',outF)
        self.model.save(outF)
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%xx)
 
    #............................
    def load_model_full(self):
        try:
            del self.model
            print('delte old model')
        except:
            a=1
        start = time.time()
        outF5m=self.outPath+'/'+self.prjName+'.'+self.modelDesign+'.model_full.h5'
 
        print('load model and weights  from',outF5m,'  ... ')
        self.model=load_model(outF5m) # creates mode from HDF5
        self.model.summary()
        print(' model loaded, elaT=%.1f sec'%(time.time() - start))

    #............................
    def load_weights(self,name):
        start = time.time()
        outF5m=self.prjName+'.%s.h5'%name
        print('load  weights  from',outF5m,end='... ')
        self.model.load_weights(outF5m) # creates mode from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))

#............................
    def get_correl(self,a,metaD):
        #print('a dims:',a.shape,'\n dump a:')

        # compute cov matrix
        c=np.cov(a,rowvar=False)
        nv=c.shape[0]
        nvL=range(nv)
        #print('cov M:')
        #pprint(c)
        c1=np.diagonal(c)
        c2=np.sqrt(c1)
        print('one std dev:',c2)
        avrSig=c2.mean()
        print('global  residua avr=%.3f  var=%.3f '%(avrSig, avrSig*avrSig))

        parNameL=metaD['parName']
        print('\nnames ',end='')
        [ print('     %s'%parNameL[i],end='') for i in nvL]

        print('\nresidu: ',end='')
        [ print(' U%d-> %4.1f%s  '%(i,100*c2[i],chr(37)),end='') for i in nvL ]

        lossMAE=np.absolute(c2).mean()
        c3=np.power(c2,2);    lossMSE=c3.mean()
        print('\n global lossMSE=%.4f , lossMAE=%.3f (computed)'%( lossMSE,lossMAE))

        #print('\ncorrelation matrix for %d dim vector'%nv)
        r=np.corrcoef(a,rowvar=False)
        #pprint(r)

        print('\nnames  ',end='')
        [ print('   %s '%parNameL[i],end='') for i in nvL]
        print('\ncorrM  ',end='')
        [ print('  __var%1d__ '%i,end='') for i in nvL]
        for k in nvL:
            print('\n var%1d '%k,end='')
            [ print('     %6.3f'% r[i][k],end='') for i in range(k+1)]
            #print('      #  '*(nv-k),end='')
        print()

