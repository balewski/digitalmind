#!/usr/bin/env python
""" read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_SquareRegr import Plotter_SquareRegr
from Deep_SquareRegr import Deep_SquareRegr
from Util_Func import  read_yaml

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--design', dest='modelDesign',  default='2Dcnn',
                        choices=['fc','cnn'], help=" model design of the network")

    parser.add_argument("--dataPath",help="output path",  default='data')
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    args = parser.parse_args()
    args.prjName='squareRegr'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

gra=Plotter_SquareRegr(args)
deep=Deep_SquareRegr(**vars(args))
# read meta-data file
metaF=args.dataPath+'/meta.'+args.prjName+'.yaml'
gra.metaD=read_yaml(metaF)
print('ddd',gra.metaD)

dom='test'
#dom='val'

deep.load_input_hdf5([dom])
deep.load_model_full() 

#deep.load_weights('weights_best') 
#deep.make_prediction(dom)


X=deep.data[dom]['X']
U=deep.data[dom]['Y']
print('X',X.shape)
Z= deep.model.predict(X)

deep.get_correl(U-Z,gra.metaD)

tit='dom=%s '%(dom,)
gra.plot_fparam_residua(U,Z,tit=tit)

gra.plot_data_fparams(U,'true U',figId=6)
gra.plot_data_fparams(Z,'pred Z',figId=5)

gra.plot_frames(deep.data[dom],4,pred=Z)

gra.display_all(args,'predict')



