#!/usr/bin/env python
""" 
Generates 2D images , saves X,Y as HD5 or TFRec files (see if:0 )
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_SquareRegr import Plotter_SquareRegr
from Util_Func import write_yaml, read_yaml, produce_frames,write_data_hdf5,np_to_tfrecord,tfrecord_to_np
from Util_Func import generate_batches_from_hdf5

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("--dataPath",  default='data',
                        help="output HD5+TFrec path")

    parser.add_argument("-N", "--name",  default='frames',
                        help="core name of produced file")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-n", "--events", default=500, type=int,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='squareRegr'

    if args.outPath=='same': args.outPath=args.dataPath

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
contentF='content.'+args.prjName+'.yaml'

contentD=read_yaml(contentF)
print('M:con',contentD)

dataD,metaD=produce_frames(contentD,args)
# save meta-data file
metaF=args.dataPath+'/meta.'+args.prjName+'.yaml'
write_yaml(metaD,metaF)


if 1: # only for HD5
    outF5=args.dataPath+'/'+args.prjName+'_'+args.name+'.hd5'
    write_data_hdf5(dataD,outF5)

if 0: # only forTFRec
    outF=args.dataPath+'/'+args.name+'_'+args.prjName+'.tfrec'
    np_to_tfrecord(dataD,outF)

if 0 :  # only for testing IO for TFrecords  
    Xdims=(-1,contentD['constPar']['max_y'],contentD['constPar']['max_x'])
    print('test-read TFrec',outF,'Xdims=',Xdims)
    dataD2=tfrecord_to_np(outF,Xdims)
    dataD=dataD2

if 0 :  # only for 
    print('testing IO for Hd5 to streamer')
    batch_size=4
    Xdims=(batch_size,contentD['constPar']['max_y'],contentD['constPar']['max_x'])
    Ydims=(batch_size,len(contentD['varPar']))
    for item in generate_batches_from_hdf5(outF5,batch_size,Xdims,Ydims):
        print('gener item len X:',item[0].shape, 'Y:',item[1].shape)
        break


gra=Plotter_SquareRegr(args )

gra.plot_frames(dataD,4)
gra.display_all(args,'gener')

