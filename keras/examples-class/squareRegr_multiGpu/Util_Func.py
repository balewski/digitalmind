import numpy as np
import time,  os
import h5py
import tensorflow as tf
import ruamel.yaml  as yaml

#...!...!..................
def read_yaml(ymlFn):
        print('  read  yaml:',ymlFn,end='')
        start = time.time()
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.Loader)
        ymlFd.close()
        print(' done, size=%d'%len(bulk),'  elaT=%.1f sec'%(time.time() - start))
        return bulk

#...!...!..................
def write_yaml(rec,ymlFn,verb=1):
        start = time.time()
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1048576
        if verb:
                print('  closed  yaml:',ymlFn,' size=%.2f MB'%xx,'  elaT=%.1f sec'%(time.time() - start))

#...!...!..................
def write_data_hdf5(dataD,outF):
        h5f = h5py.File(outF, 'w')
        print('save data as hdf5:',outF)
        for item in dataD:
           rec=dataD[item]
           h5f.create_dataset(item, data=rec)
        print('h5-write :',item, rec.shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB, frames=%d'%(xx,rec.shape[0]))

#...!...!..................
def read_data_hdf5(inpF):
        print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD

#...!...!..................
def np_to_generator(X_data, y_data, batch_size):

  samples_per_epoch = X_data.shape[0]
  number_of_batches = samples_per_epoch/batch_size
  counter=0

  while 1:
    X_batch = np.array(X_data[batch_size*counter:batch_size*(counter+1)]).astype('float32')
    y_batch = np.array(y_data[batch_size*counter:batch_size*(counter+1)]).astype('float32')
    counter += 1
    yield X_batch,y_batch

    #restart counter to yeild data in the next epoch as well
    if counter >= number_of_batches:
        counter = 0




#...!...!..................
def generate_batches_from_hdf5(filepath, batchsize,Xdims,Ydims):
    """
    Generator that returns batches of images ('xs') and params('ys') from a h5 file.
    :param string filepath: Full filepath of the input h5 file, e.g. '/path/to/file/file.h5'.
    :param int batchsize: Size of the batches that should be generated.
    :Xdims : (batchsize, nx, ny, nFeatures)
    :Ydims : (batchsize, nParams) 
    :return: (ndarray, ndarray) (xs, ys): Yields a tuple which contains a full batch of images and params.
    """
    
    while 1: # the hd5 file will be opened all the time
        f = h5py.File(filepath, "r")
        filesize = len(f['Y'])

        # count how many entries we have read
        n_entries = 0
        # as long as we haven't read all entries from the file: keep reading
        while n_entries < (filesize - batchsize):
            # start the next batch at index 0
            # create numpy arrays of input data (features)
            xs = f['X'][n_entries : n_entries + batchsize]
            xs = np.reshape(xs, Xdims).astype('float32')

            # and label info. Contains more than one label in my case, e.g. is_dog, is_cat, fur_color,...
            y_values = f['Y'][n_entries:n_entries+batchsize]
            ys = np.reshape(y_values, Ydims).astype('float32')

            # we have read one more batch from this file
            n_entries += batchsize
            yield (xs, ys)
        f.close()



#...!...!..................
def np_to_tfrecord( dataA, result_tf_file, verbose=True):
    objNL=list(dataA.keys())
    nEve=dataA[objNL[0]].shape[0]
    print('np_to_tfrec() inp nEve=',nEve,objNL,' outFN=',result_tf_file)
    writer = tf.python_io.TFRecordWriter(result_tf_file)

    def _bytes_feature(value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    for idx in range(nEve):
        features = {}
        for objN in objNL:
            features[objN] = _bytes_feature(dataA[objN][idx].tostring())

        example = tf.train.Example(features=tf.train.Features(feature=features))
        serialized = example.SerializeToString()
        writer.write(serialized)
    writer.close()

#...!...!..................
def tfrecord_to_np(tfr_fname,Xdims):
    # note - wild cards are not accepted by 
    print('\n Open %s \nand  convert it into in-memory np.array'%tfr_fname)

    outD={}
    for ser_item in tf.python_io.tf_record_iterator(tfr_fname):
        tftUtil = tf.train.Example()
        myLen=tftUtil.ParseFromString(ser_item)  # must be called for each frame
        if len(outD)==0:  # first record
            for x in tftUtil.features.feature:
                outD[x]=[]                
                #print('gene',x)
            print('first frame len/kB=%.1f,'%(myLen/1024),outD.keys())
        for gene in outD:            
            _x = np.fromstring(tftUtil.features.feature[gene].bytes_list.value[0], dtype=np.float32)
            outD[gene].append(_x)

    for gene in outD:
        outD[gene]=np.array(outD[gene])
        if gene=='X' : outD[gene]=outD[gene].reshape(Xdims)
        print('recoverd', gene,outD[gene].shape)
    return outD



#...!...!..................
def insert_ghost(base_a,pos_x,pos_y,frame):
        off=int(base_a/4)
        for i in range(base_a):
           #if i<10: print(i,pos_y,pos_x+i)
           # remember, plotter flips the Y-axis
           frame[pos_y+off,pos_x+i]=1  # bottom
           frame[pos_y+i,pos_x+off]=1  # left
           frame[pos_y+i,pos_x+base_a-off]=1  # right
           frame[pos_y+base_a-off,pos_x+i]=1  # top

#...!...!..................
def insert_symbol(base_a,pos_x,pos_y,frame):
        for i in range(base_a):
           #if i<10: print(i,pos_y,pos_x+i)
           # remember, plotter flips the Y-axis
           frame[pos_y,pos_x+i]=1  # bottom
           frame[pos_y+i,pos_x]=1  # left
           frame[pos_y+i,pos_x+base_a-1]=1  # right
           frame[pos_y+base_a-1,pos_x+i]=1  # top

#...!...!..................
def produce_oneFrame(parD):
        nPar=len(parD['varPar'])
        mx_x=parD['constPar']['max_x']
        mx_y=parD['constPar']['max_y']
        base_a=parD['constPar']['base_a']

        # generate  params normalized to unity
        U=np.random.uniform(-1,1,size=nPar)
        # phys params restricted, symbol do not touches the frame
        pos_x=(U[0]+1.)/2* (mx_x -2.05*base_a) + base_a
        pos_y=(U[1]+1.)/2* (mx_y -2.05*base_a) + base_a
        P=[pos_x,pos_y]  # lower-left corner
        frame=np.zeros( (mx_y,mx_x) )
        insert_symbol(base_a,int(pos_x),int(pos_y),frame)
        insert_ghost(base_a,int(mx_x-pos_x),int(mx_y-pos_y),frame)

        return U,P,frame

#...!...!..................
def produce_frames(parD,args):        
        out={'X':[],'Y':[],'AUX':[]}

        # fill meta-data record
        metaD={}
        mx_x=parD['constPar']['max_x']
        mx_y=parD['constPar']['max_y']
        metaD['phys_range']=[[0,mx_x], [0,mx_y]]
        metaD['parName']=parD['varPar']
        metaD['numPar']=len(parD['varPar'])

        start = time.time()
        for i in range(args.events):
            U,P,frame=produce_oneFrame(parD)

            if i%500==0:
                print('gen i=',i,' parsU,P',U,P)

            out['Y'].append(U)
            out['X'].append(frame)
            out['AUX'].append(P)

        for item in out:
            out[item]=np.array(out[item]).astype(np.float32)
 
        print('%d frames produced, elaT=%.1f sec'%(args.events,(time.time() - start)))
        return out, metaD
