import os
import numpy as np
from matplotlib import cm as cmap
import socket  # for hostname
from tensorflow.contrib.keras.api.keras.utils import plot_model

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_VAE_Mnist(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args):
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        if not os.path.exists(self.outPath):
            print('Aborting on start, missing output dir',args.outPath)
            exit(22)

    #............................
    def display_all(self,args,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        self.plt.tight_layout()
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/idx%d_%s_%s_f%d'%(self.outPath,args.arrIdx,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()

#............................
    def plot_model(self,deep):
        #print('plot_model broken in TF=1.4, skip it')
        #return
        if 'cori' not in socket.gethostname(): return  # software not installed
        fname=self.outPath+'/'+deep.name+'_graph.svg'
        model=deep.model
        for i in range(3):
            if i==1:
                fname=self.outPath+'/'+deep.name+'_encoder_graph.svg'
                model=deep.encoderM
            if i==2:
                fname=self.outPath+'/'+deep.name+'_decoder_graph.svg'
                model=deep.decoderM
                            
            plot_model(model, to_file=fname, show_layer_names=True)#, show_shapes=True)
            print('Graph saved as ',fname)



#............................
    def plot_digits(self,deep,name,idxL,irow,ncol=3, auxL=[],figId=7):
        assert 'X_'+name in deep.data

        if figId not in self.figL:
            self.figL.append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,6))


        X=deep.data['X_'+name]
        X=X.reshape(-1,28,28) #un-do flattening
        Y=deep.data['Y_'+name]       
        nrow=3
        print('plot input for idx=',idxL)
        j=0
        for i in idxL:            
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j+irow*ncol)
            tit='i:%d dig=%d'%(i,Y[i])
            if len(auxL)>0 : tit+=' pred=%d'%auxL[j]
            ax.set(title=tit)
            ax.imshow(X[i], cmap=self.plt.get_cmap('gray'))
            j+=1

#............................
    def plot_train_history(self,dee,args,figId=10):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(10,5))
        
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((2,3), (0,0), colspan=2 )
        ax2 = self.plt.subplot2grid((2,3), (1,0), colspan=2,sharex=ax1 )

        DL=dee.train_hirD

        tit1='%s, train %.1f min, nCpu=%d, drop=%.1f'%(dee.name,dee.train_sec/60.,args.nCpu,args.dropFrac)
        

        ax1.set(ylabel='loss',title=tit1)
        ax1.plot(DL['loss'],'.-.',label='train')
        ax1.plot(DL['val_loss'],'.-',label='valid')
        ax1.legend(loc='best')
        #ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')
        

#............................
    def plot_latent_z(self,deep,dom,figId=11):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,4))
        X=deep.data['X_'+dom]
        Y=deep.data['Y_'+dom]

        # display a 2D plot of the digit classes in the latent space
        zA= deep.encoderM.predict(X)

        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        zNameA=['Z_mu','Z_sig']
        for i in range(len(zA)):
            ax=self.plt.subplot(1,2,i+1)
            zVec=zA[i] 
            ztxt=zNameA[i]
            print('dd',i,zVec.shape)
            img=ax.scatter(zVec[:, 0], zVec[:, 1], c=Y,alpha=.4, s=3**2, cmap='tab10')
            ax.set(title=ztxt+' latent space, dom='+dom, xlabel='latent Z_0',ylabel='latent Z_1')
            ax.grid()
            self.plt.colorbar(img).set_label('10 MNIST digits')


#............................
    def plot_decoded_z(self,deep,figId=12):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(11,6))
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        # linearly spaced coordinates on the unit square were transformed
        zmx=4; n=12; digit_size=28
        u_grid = np.dstack(np.meshgrid(np.linspace(-zmx, zmx, n),
                               np.linspace(-zmx, zmx, n)))
        z_grid=u_grid
        print('z_grid:',z_grid.shape)
        for i in range(2):
            ax=self.plt.subplot2grid((2,3), (i,0) )
            img=ax.imshow( z_grid[:,:,i], cmap='cool')
            ax.set(title='Z_%d latent space'%i)
            self.plt.colorbar(img)

        x_decoded = deep.decoderM.predict(z_grid.reshape(n*n, 2))
        img_decoded = x_decoded.reshape(n, n, digit_size, digit_size)

        ax= self.plt.subplot2grid((2,3), (0,1), colspan=2, rowspan=2 )

        ax.imshow(np.block(list(map(list, img_decoded))), cmap='gray')
        ax.set_title(' 2D manifold of the digits in latent space')

