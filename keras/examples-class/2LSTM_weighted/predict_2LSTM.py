#!/usr/bin/env python
""" read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_2LSTM import Plotter_2LSTM
from Deep_2LSTM import Deep_2LSTM

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of 2LSTM data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
 
    parser.add_argument("--project",
                        default='jan_2LSTM',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument( "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

mpl=Plotter_2LSTM(args,xinch=10 )
dee=Deep_2LSTM(args)

dee.load_input_hdf5(['test'])
#mpl.plot_input_pairs(dee,'test',0,sel=1)
dee.load_model_full() 
dee.eval_data('test') 

dee.load_weights('weights_best') 
dee.eval_data('test') 

mpl.plot_AUC('test',dee,args)
mpl.pause(args,'predict')


