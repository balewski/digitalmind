# for AUC of ROC
from sklearn.metrics import roc_curve, auc
import numpy as np
from keras.utils import plot_model


__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_2LSTM(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args,xinch=8,yinch=7):
        if args.noXterm:
            print('diasable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print('Graphics started')
        plt.figure(facecolor='white', figsize=(xinch,yinch))
        self.plt=plt
        self.nr_nc=(3,3)

#............................
    def pause(self,args,ext,pdf=1):
        self.plt.tight_layout()
        if pdf:
            figName='idx%d_%s_%s'%(args.arrIdx,ext,args.prjName)  
            print('Graphics displayed, saving %s ...'%figName)
            self.plt.savefig(figName+'.pdf')
        self.plt.show()
  

#............................
    def plot_model(self,deep):
        fname=deep.name+'.graph.svg'
        plot_model(deep.model, to_file=fname, show_shapes=True, show_layer_names=True)
        print('Graph saved as ',fname)

#............................
    def plot_input_raw(self,deep,idxL,irow):
        X=deep.raw['Xorg']
        Y=deep.raw['Yorg']
        nrow,ncol=self.nr_nc
        print('plot input raw for idx=',idxL)
        j=0
        for i in idxL:            
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j+irow*ncol)
            tit='i:%d dig=%d'%(i,Y[i])           
            ax.set(title=tit)
            ax.imshow(X[i], cmap=self.plt.get_cmap('gray'))
            j+=1

#............................
    def plot_input_pairs(self,deep,name,irow,sel=1):
        X1=deep.data['X1_'+name]
        X2=deep.data['X2_'+name]
        Y=deep.data['Y_'+name]
        nrow,ncol=self.nr_nc
        print('plot input pairs for label=',sel)
        j=0
        for i in range(100):  
            if Y[i]!= sel: continue
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow*2, ncol, 1+j+irow*ncol*2)
            tit='i:%d label=%d'%(i,Y[i])           
            ax.set(title=tit)
            ax.imshow(X1[i], cmap=self.plt.get_cmap('gray'))

            ax = self.plt.subplot(nrow*2, ncol, 1+j+irow*ncol*2+ncol, sharex=ax)
            ax.imshow(X2[i], cmap=self.plt.get_cmap('gray'))
            j+=1
            if j>= ncol: break


#............................
    def plot_train_hir(self,dee,args): 
        nrow,ncol=self.nr_nc
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (1,0), colspan=2 )
        ax2 = self.plt.subplot2grid((nrow,ncol), (2,0), colspan=2,sharex=ax1 )
        ax3 = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=2,sharex=ax1 )

        DL=dee.train_hirD
        val_acc=DL['val_acc'][-1]

        tit1='%s, train %.1f min, nCpu=%d, drop=%.1f'%(dee.name,dee.train_sec/60.,args.nCpu,args.dropFrac)
        tit2='arrIdx=%d, earlyStop=%d, end val_acc=%.3f,balance=%.1f'%(args.arrIdx,args.earlyStopOn,val_acc,dee.data['w_train'][1])

        
        ax1.set(ylabel='loss',title=tit1)
        ax1.plot(DL['loss'],'.-.',label='train')
        ax1.plot(DL['val_loss'],'.-',label='valid')
        ax1.legend(loc='best')
        ax1.grid(color='brown', linestyle='--',which='both')
        
        ax2.set(xlabel='epochs',ylabel='accuracy',title=tit2)
        ax2.plot(DL['acc'],'.-',label='train')
        ax2.plot(DL['val_acc'],'.-',label='valid')
        ax2.legend(loc='bottom right')
        ax2.grid(color='brown', linestyle='--',which='both')

        ax3.plot(DL['lr'],'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate')

    #............................
    def plot_AUC(self,name,deep,args):
        nrow,ncol=self.nr_nc
        X1=deep.data['X1_'+name]
        X2=deep.data['X2_'+name]
        y_true=deep.data['Y_'+name]

        # produce AUC of ROC
        '''
        With the Model class, you can use the predict method which will give you a vector of probabilities and then get the argmax of this vector (with np.argmax(y_pred1,axis=1)).
        '''
        # here I have only 1 class - so I can skip the armax step
        y_score = deep.model.predict([X1,X2])
        print('\ny_score shape',y_score.shape,y_score[:5])
        print('Y shape',y_true.shape,y_true[:5])

        fpr, tpr, _ = roc_curve(y_true, y_score)
        roc_auc = auc(fpr, tpr)

        LRP=np.divide(tpr,fpr)
        fpr_cut=0.08
        for x,y in zip(fpr,LRP):
            if x <fpr_cut :continue
            print('found fpr=',x, 'LP+=',y)
            break

        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot(nrow, ncol, nrow*ncol-ncol)
        ax1.plot(fpr, tpr, label='ROC',color='seagreen' )
        ax1.plot([0, 1], [0, 1], 'k--', label='coin flip')
        ax1.axvline(x=x,linewidth=1, color='blue')
        ax1.set(xlabel='False Positive Rate',ylabel='True Positive Rate',title='ROC , area = %0.3f' % roc_auc)
        ax1.legend(loc='lower right',title=name+'-data')
        ax1.grid(color='brown', linestyle='--',which='both')

        ax2 = self.plt.subplot(nrow, ncol, nrow*ncol)
        ax2.plot(fpr,LRP, label='ROC', color='teal')
        ax2.plot([0, 1], [1, 1], 'k--',label='coin flip')
        ax2.set(ylabel='Pos. Likelih. Ratio',xlabel='False Positive Rate',title='LR+(FPR=%.2f)=%.1f'%(x,y))
        ax2.set_xlim([0,.1])

        ax2.axvline(x=x,linewidth=1, color='blue')
        ax2.legend(loc='upper right')
        ax2.grid(color='brown', linestyle='--',which='both')

        print('AUC: %f' % roc_auc)
