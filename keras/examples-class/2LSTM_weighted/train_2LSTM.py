#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""
#start = time.time()
#print('xxx elaT=%.1f sec'%(time.time() - start))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_2LSTM import Plotter_2LSTM
from Deep_2LSTM import Deep_2LSTM

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='trainer for  2LSTM ',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
 
    parser.add_argument("--project",
                        default='jan_2LSTM',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument('-X', "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=3,
                        help="fitting epoch")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", dest='earlyStopOn',
                         action='store_true',default=False,help="enable early stop")
    parser.add_argument( "--checkPt", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    parser.add_argument( "--reduceLr", dest='reduceLearn',
                         action='store_true',default=False,help="reduce learning at plateau")

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

mpl=Plotter_2LSTM(args,xinch=10 )
dee=Deep_2LSTM(args)
dee.load_input_hdf5(['train','val'])
#mpl.plot_input_pairs(dee,'train',0,sel=1)

dee.build_model(args) 
mpl.plot_model(dee)
dee.train_model(args) 
dee.save_model_full() 
dee.eval_data('val') 

mpl.plot_train_hir(dee,args)
mpl.plot_AUC('val',dee,args)

mpl.pause(args,'train')

