import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()

from keras.datasets import mnist
from keras import utils as np_utils
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint , ReduceLROnPlateau
from keras.layers import Dense, Dropout,   LSTM, Input, concatenate

import numpy as np
import h5py
#import yaml
print('deep-libs imported elaT=%.1f sec'%(time.time() - start))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
from keras.callbacks import Callback
import keras.backend as K
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]   
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        #lr = K.eval(optimizer.lr * (1. / (1. + optimizer.decay * optimizer.iterations)))
        lr = K.eval(optimizer.lr)
        self.hir.append(lr)


#............................
#............................
#............................
class Deep_2LSTM(object):
    """Contains all operations specific for this project"""

    def __init__(self,args):
        self.name=args.prjName
        print(self.__class__.__name__,', prj:',self.name)
        # CPUs are used via a "device" which is just a threadpool
        if args.nCpu>0:
            import tensorflow as tf
            tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
            print('restrict CPU count to ',args.nCpu)

#............................
    def read_mnist_raw(self):
        print('read raw data')
        # Load pre-shuffled MNIST data into train and test sets
        (X, Y), (Xt, Yt) = mnist.load_data()
        self.raw={}
        self.raw['Xorg']=np.concatenate((X,Xt))
        self.raw['Yorg']=np.concatenate((Y,Yt))
        print('raw MNIST Xorg sum',self.raw['Xorg'].shape)
    
    #............................
    def slice_frames(self,nA):
        X=self.raw['Xorg']
        self.raw['Xa']=X[:,:nA,:]
        self.raw['Xb']=X[:,nA:,:]
        print('sliced shapes Xa,Xb:',self.raw['Xa'].shape,self.raw['Xb'].shape)


    #............................
    def make_good_bad_pairs(self,bala):
        Xa=self.raw['Xa']
        Xb=self.raw['Xb']
        nd=Xa.shape[0]
        assert nd==Xb.shape[0]

        okL=[]
        X1=[]; X2=[]; Y=[]
        nM=0
        for i in range(nd):
            X1.append(Xa[i])
            rnd=np.random.uniform()
            if rnd <bala:
                okL.append(i) # to not reuse it for miss-matches
                X2.append(Xb[i])
                Y.append(1)
                nM+=1
            else:
                while True:
                    j=np.random.randint(nd)
                    if j not in okL: break
                X2.append(Xb[j])
                Y.append(0)
        print('made_pairs  balance=%.2f  any=%d'%(bala,nd),'nMatch=',nM, 'sizes X1,X2,Y:',len(X1), len(X2),len(Y))

        # normaliz4
        self.raw['X1']=np.asarray(X1)/256.
        self.raw['X2']=np.asarray(X2)/256.
        self.raw['Y']=np.asarray(Y)


    #............................
    def split_input(self,trainFrac=0.7, valFrac=0.1):
        self.data={}
        
        mxeve=len(self.raw['X1'])
        idxL=[i for i in range(mxeve)]
        np.random.shuffle(idxL)
        print(mxeve,'total, example shuffling',idxL[:10])

        idx={}
        partL=['train','val','test']
        for ss in partL:
            idx[ss]=[]

        prob2=trainFrac+valFrac
        assert prob2 <1
        assert trainFrac <prob2

        for i in idxL:
            x=np.random.uniform()
            if x <trainFrac :
                idx['train'].append(i)
            elif x <prob2 :
                idx['val'].append(i)
            else:
                idx['test'].append(i)
        print('split pairs: ',end='')
        for ss in partL:
            print(ss, len(idx[ss]), end=', ')
        print()

        for ss in partL:
            print('Assemble  final input tensors [X1,X2], Y for:',ss,len(idx[ss]))
            X1=[];X2=[];Y=[]
            for i in idx[ss]:
                X1.append(self.raw['X1'][i])
                X2.append(self.raw['X2'][i])
                Y.append(self.raw['Y'][i])
            self.data['X1_'+ss]=np.array(X1).astype(np.float32)
            self.data['X2_'+ss]=np.array(X2).astype(np.float32)
            self.data['Y_'+ss]=np.array(Y).astype(np.float32)
            print('X1 shape', self.data['X1_'+ss].shape)

#............................
    def save_input_hdf5(self):
        outF=self.name+'.data.h5'
        print('save data as hdf5:',outF)
        h5f = h5py.File(outF, 'w')
        for x in self.data:
            xobj=self.data[x]
            print('h5-write ',x,type(xobj),xobj.shape,xobj.dtype);
            h5f.create_dataset(x, data=self.data[x])
        #for x in h5f.keys(): 
        #    print(x,self.data[x].shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%xx)

#............................
    def load_input_hdf5(self,dataL):
        inpF5=self.name+'.data.h5'
        start = time.time()
        print('clear old, load new data from hdf5:',inpF5)
        self.data={}
        h5f = h5py.File(inpF5, 'r')
        for x in h5f.keys(): 
            for y in dataL:
                if y not in x: continue 
                print('load:',x,end='')
                self.data[x] = h5f[x][:]
                print(' done',self.data[x].shape,type( h5f[x]))
                if 'Y' in  x:
                    sum=np.sum(h5f[x])
                    frac=sum/len(h5f[x])
                    print('  %s balance=%.2f'%(x,frac))
                    if 'train' in x: # just a dict w/ 2 weights per labels
                        self.data['w_train']={0: 1, 1: 1./frac}
                    if 'val' in x: # full array of weights per event
                        xV=h5f[x]* (1./frac-1)
                        self.data['w_val']=xV+1
                        #print(1/frac,'aa',self.data['w_val'][:100])
                        
        h5f.close()
        print('load_input_hdf5 done, elaT=%.1f sec'%(time.time() - start))

    #............................
    def print_input(self,name,k=2):
        for x in self.data:
            if name not in x:  continue
            xobj=self.data[x]
            print('\nsample of ',x, xobj.shape)
            for i in range(k):
                if 'Y' in x:
                    print('\nidx=%d digit=%d, X-data:'%(i,xobj[i]))
                else:
                    print('\n%d data:'%i)
                    print(xobj[i][5:7])

    #............................
    def build_model(self,args):
        # based  https://keras.io/getting-started/functional-api-guide/
        start = time.time()
        sh1=self.data['X1_train'].shape
        sh2=self.data['X2_train'].shape
        input1 = Input(shape=(sh1[1],sh1[2]), name='inp1')
        input2 = Input(shape=(sh2[1],sh2[2]), name='inp2')
        print('build_model inp1:',input1.get_shape(),' inp2:',input2.get_shape())
        lstm_n1=20; lstm_n2=20

        dens_n1=20; dens_n2=10

        net1= LSTM(lstm_n1,dropout=args.dropFrac,return_sequences=True) (input1)
        net1= LSTM(lstm_n1,dropout=args.dropFrac) (net1)

        net2= LSTM(lstm_n2,dropout=args.dropFrac,return_sequences=True) (input2)
        net2= LSTM(lstm_n2,dropout=args.dropFrac) (net2)
        print('net2=>',net2.get_shape())

        net = concatenate([net1,net2],name='concat-jan')
        print('net3=>',net.get_shape())
        net=Dropout(args.dropFrac)(net)
        net=Dense(dens_n1, activation='relu')(net)
        net=Dropout(args.dropFrac)(net)
        net=Dense(dens_n2, activation='relu')(net)
        net=Dropout(args.dropFrac)(net)
        outputs=Dense(1, activation='sigmoid')(net) # predicts only 0/1 
        model = Model(inputs=[input1,input2], outputs=outputs)
        # Compile model
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        model.summary() # will print
        print('model size=%.1fK compiled elaT=%.1f sec'%(model.count_params()/1000.,time.time() - start))
        self.model=model


    #............................
    def train_model(self,args):
        X1=self.data['X1_train']
        X2=self.data['X2_train']
        Y= self.data['Y_train']
        X1_val=self.data['X1_val']
        X2_val=self.data['X2_val']
        Y_val= self.data['Y_val']
        class_weight = self.data['w_train']
        W_val = self.data['w_val']

        if args.events>0:
            if len(Y) > args.events :
                X1=X1[:args.events]
                X2=X2[:args.events]
                Y=Y[:args.events]
                print('reduced traing to %d events'%len(Y))
        if args.verb==0:
            print('train silently epochs:',args.epochs)

        callbacks_list = []
        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)
        
        if args.earlyStopOn:
            earlyStop=EarlyStopping(monitor='val_loss', patience=10, verbose=1, mode='auto',min_delta=0.001)
            callbacks_list.append(earlyStop)
            print('enabled EarlyStopping')

        if args.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.name+'.weights_best.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1,period=4)
            callbacks_list.append(ckpt)
            print('enabled ModelCheckpoint')

        if args.reduceLearn:
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=5, min_lr=0.0, verbose=1,epsilon=0.01)
            callbacks_list.append(redu_lr)
            print('enabled ReduceLROnPlateau')

        print('\nTrain_model X1:',X1.shape, ' earlyStop=',args.earlyStopOn,' epochs=',args.epochs,' batch=',args.batch_size,' weights=',class_weight)
        startTm = time.time()
        hir=self.model.fit([X1,X2],Y, callbacks=callbacks_list,
                 class_weight = class_weight,
                 validation_data=([X1_val,X2_val],Y_val,W_val),
                 shuffle=True,
                 batch_size=args.batch_size, nb_epoch=args.epochs,
                 verbose=args.verb)
        self.train_hirD=hir.history
        self.train_hirD['lr']=lrCb.hir
    

        #evaluate performance for the last epoch
        acc=self.train_hirD['val_acc'][-1]
        loss=self.train_hirD['val_loss'][-1]
        fitTime=time.time() - start
        print('\n End Validation Accuracy:%.3f'%acc, ', Loss:%.3f'%loss,', fit time=%.1f sec'%(fitTime))
        self.train_sec=fitTime


    #............................
    def eval_data(self,name):
        nameX1='X1_'+name
        nameX2='X2_'+name
        nameY='Y_'+name        
        print('eval data',nameX1,' using current model')
        score = self.model.evaluate(
            [self.data[nameX1],self.data[nameX2]],self.data[nameY], verbose=0)
        print('score=',score)

    #............................
    def predict_data(self,name,k=10):
        print('predict data  not implemented, see Plotter for example')

 
    #............................
    def save_model_full(self):
        outF=self.name+'.model_full.h5'
        print('save model full to',outF)
        self.model.save(outF)
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%xx)
 
    #............................
    def load_model_full(self):
        try:
            del self.model
            print('delte old model')
        except:
            a=1
        start = time.time()
        outF5m=self.name+'.model_full.h5'
        print('load model and weights  from',outF5m,'  ... ')
        self.model=load_model(outF5m) # creates mode from HDF5
        self.model.summary()
        print(' model loaded, elaT=%.1f sec'%(time.time() - start))

    #............................
    def load_weights(self,name):
        start = time.time()
        outF5m=self.name+'.%s.h5'%name
        print('load  weights  from',outF5m,end='... ')
        self.model.load_weights(outF5m) # creates mode from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))
        
'''

mmm1 ['name', 'supports_masking', 'trainable', 'inputs', 'outputs', 'input_layers', 'input_layers_node_indices', 'input_layers_tensor_indices', 'output_layers', 'output_layers_node_indices', 'output_layers_tensor_indices', 'layers', '_output_mask_cache', '_output_tensor_cache', '_output_shape_cache', 'input_names', 'output_names', '_feed_input_names', '_feed_inputs', '_feed_input_shapes', 'internal_input_shapes', 'internal_output_shapes', 'layers_by_depth', 'container_nodes', 'nodes_by_depth', 'outbound_nodes', 'inbound_nodes', 'built', 'optimizer', 'sample_weight_mode', 'loss', 'loss_weights', 'loss_functions', '_feed_outputs', '_feed_output_names', '_feed_output_shapes', '_feed_loss_fns', 'sample_weight_modes', '_feed_sample_weight_modes', 'targets', '_feed_targets', 'metrics', 'metrics_names', 'metrics_tensors', 'total_loss', 'sample_weights', '_feed_sample_weights', '_function_kwargs', 'train_function', 'test_function', 'predict_function', '_collected_trainable_weights', 'history', 'stop_training']

mmm2 ['updates', 'weights', 'iterations', 'lr', 'beta_1', 'beta_2', 'epsilon', 'decay', 'initial_decay']
lr= <tf.Variable 'lr:0' shape=() dtype=float32_ref>

'''
