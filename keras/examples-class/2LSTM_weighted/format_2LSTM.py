#!/usr/bin/env python
"""  read raw input MNIST data
sanitize, randomize, pad/clip/1hot, 
Can produce imbalance data sets -->  make_good_bad_pairs(bala=0.3)
write 9 tensors: (train,val,test) * (X,Y,Yhot) in hd5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_2LSTM import Plotter_2LSTM
from Deep_2LSTM import Deep_2LSTM

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of MNIST data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("--project",
                        default='jan_2LSTM',dest='prjName',
                        help="core name used to store outputs")
    parser.add_argument( "--no-Xterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.nCpu=0	
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

nA=10 # time bins for 1-LSTM, 28-nA will be used by 2-LSTM

mpl=Plotter_2LSTM(args )

dee=Deep_2LSTM(args)
dee.read_mnist_raw()
mpl.plot_input_raw(dee,range(4),0)
dee.slice_frames(nA)
dee.make_good_bad_pairs(bala=0.03)
dee.split_input()

mpl.plot_input_pairs(dee,'val',1,sel=0)
mpl.plot_input_pairs(dee,'val',2,sel=1)
dee.print_input('val',2)
dee.save_input_hdf5()
mpl.pause(args,'form')
