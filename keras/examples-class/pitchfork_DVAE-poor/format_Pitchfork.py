#!/usr/bin/env python
""" 
produce training data, save as hd5
Example VAE with diven inputs

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Pitchfork import Plotter_Pitchfork
from Deep_Pitchfork import Deep_Pitchfork

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Seq2Func data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")


    parser.add_argument("-n", "--events", type=int, default=100,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='f2f'
    args.arrIdx=0
    args.modelDesign='bad12'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_Pitchfork (args )
deep=Deep_Pitchfork(args)

deep.generate_pulses(args)
deep.print_frames('train',numFrm=1)
deep.save_data_hdf5()
plotNorm='2restore'
#plotNorm='as-is'

ppp.plot_data_vsTime(deep.data,'train',range(6),norm=plotNorm)
#ppp.plot_data_params(deep.data,'train')

ppp.display_all(args,'form')
