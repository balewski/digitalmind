import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()

#from keras import utils as np_utils
from Util_Func import func_val_norm, func_par_norm,spikeFunc_expo2

from keras import backend as K
from keras import losses, optimizers 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout,UpSampling1D, Reshape,Conv1D,Flatten,MaxPool1D,Activation
from keras.models import Model,  load_model

from keras.layers.advanced_activations import LeakyReLU # breaks model_save unless it is a separate layer
from keras.constraints import max_norm

import numpy as np
import h5py

from sklearn.metrics import mean_absolute_error,  mean_squared_error

print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
def NegLogLikeh(y_true0, y_pred0):
    """ for continuous features use MSE """
    # keras.losses.mean_squared_errorgives the mean
    # over the last axis. we require the sum
    
    neve=K.shape(y_true0)[0]
    ndim=K.ndim(y_true0)
    y_true=K.reshape(y_true0,shape=(neve,-1))
    y_pred=K.reshape(y_pred0,shape=(neve,-1))
    #return K.sum(losses.mean_squared_error(y_true, y_pred), axis=-1)
    return K.sum(losses.mean_absolute_error(y_true, y_pred), axis=-1)

#............................
#............................
#............................
class KLDivergComp(Layer):
    """ Identity transform layer that adds 'partial' KL divergence
    to the final model loss.
    """
    def __init__(self,strength=1., *args, **kwargs):
        self.is_placeholder = True
        self.strength=strength  # the smaller it gets the closer VAE-->AE
        super(KLDivergComp, self).__init__(*args, **kwargs)
        
    def call(self, inputs): # it computes loss due to latent vars per mini-batch
        mu, drive, log_var = inputs       
        kl_batch=-0.5 * K.sum(1 +log_var  -K.square(mu-drive)  -K.exp(log_var), axis=-1)
        self.add_loss(K.mean(self.strength*kl_batch), inputs=inputs)
        return [mu, log_var]  # pass by the standard VAE output 



#............................
#............................
#............................
class Deep_Pitchfork(object):
    """Contains all operations specific for this project"""

    def __init__(self,args):
        self.outPath=args.outPath
        self.dataPath=args.dataPath
        self.name=args.prjName
        self.events=args.events
        self.verb=args.verb
        self.modelDesign=args.modelDesign
        self.data={}
        self.epsLayerExist=False
        print(self.__class__.__name__,'TF ver:', K.tf.__version__,', prj:',self.name)

#............................
    def generate_pulses(self,args):
        domL={'train':0.8,'val':0.9,'test':1.01}
        print('split frames goal:',domL)
        
        rngT1=[50,150]
        rngRC2=[16,30]
        uoff=-70
        u0=0
        u1=80
        dt=10
        rc1=6

        # 2nd 'b' peak location
        rngT1b=[250,350]

        dataD={}
        for dom in domL:
            dataD['X_'+dom]=[];   dataD['Y_'+dom]=[]
        #print('aa',dataD.keys())
        start = time.time()
        for _ in range(args.events):
            r=np.random.uniform() # train/val/test
            # below are only function parameters
            t1=np.random.uniform(rngT1[0],rngT1[1])
            rc2=np.random.uniform(rngRC2[0],rngRC2[1])

            # 2nd peak
            t1b=np.random.uniform(rngT1b[0],rngT1b[1])
            rc2b=np.random.uniform(rngRC2[0],rngRC2[1])

            pV=np.array([t1,rc2,t1b,rc2b]) # parameters
            pVn=func_par_norm(pV,norm='2unit')
                        
            fV=uoff+spikeFunc_expo2(u0,t1,u1,dt,rc1,rc2)  #function
            fV+=spikeFunc_expo2(u0,t1b,u1*.7,dt,rc1,rc2b) #function-b

            fVn=func_val_norm(fV,norm='2unit')
            # assign domain
            for dn in domL:
                if r >domL[dn] : continue
                dataD['X_'+dn].append(fVn)
                dataD['Y_'+dn].append(pVn)
                break
        for dom in domL:
            self.data[dom]=[np.array(dataD['X_'+dom]),np.array(dataD['Y_'+dom])]

        print('%d frames generated , elaT=%.1f sec'%(args.events,(time.time() - start)))

#............................
    def save_data_hdf5(self):

        for dom in self.data:
            outF=self.dataPath+'/f2f_'+dom+'.frames.yml'
            print('save data as hdf5:',outF)
            h5f = h5py.File(outF, 'w')
            X,Y=self.data[dom]
            print('h5-write X',dom,X.shape);
            h5f.create_dataset('X', data=X)
            print('h5-write Y',dom,Y.shape);
            h5f.create_dataset('Y', data=Y)
            h5f.close()
            xx=os.path.getsize(outF)/1048576
            print('closed  hdf5:',outF,' size=%.2f MB'%xx)


    #............................
    def load_data_hdf5(self,dom):
        inpF=self.dataPath+'/f2f_'+dom+'.frames.yml'

        h5f = h5py.File(inpF, 'r')
        xyL=[]
        for x in ['X','Y']:            
            obj=h5f[x][:]
            print('read ',dom,x,obj.shape)
            if self.events>0 and obj.shape[0] > self.events : 
                obj=obj[:self.events]                
                print('reduced traing to %d events'%obj.shape[0])
            xyL.append(obj)
        self.data[dom]=xyL
        h5f.close()
        print('loaded ',self.data.keys())


    #............................
    def print_frames(self,dom,numFrm=1):
        X,Y=self.data[dom]
        print('sample of ',numFrm,' from dom=',dom)
        for i in range(numFrm):            
            print('i=',i,'par:',Y[i],'frm:',X[i,::5],'::5')
            
    #............................
    def build_model(self,args): 
        if args.modelDesign=='fc':
            self.build_model_fc(args)
        elif args.modelDesign=='cnn1d':
            self.build_model_cnn1d(args)
        else:
            print('unknow design:',args.modelDesign)
            bad_bad17

        # - - -  Model assembled and compiled
        self.epsLayerExist=True # to correct input if model is only loade
        
        for mm in self.model:
            mmobj=self.model[mm]
            print('\nSummary model=%s  layers=%d , params=%.1f K, inputs:'%(mm,len(mmobj.layers),mmobj.count_params()/1000.),mmobj.input_shape)
            if args.verb>1 or mm=='auto':
                mmobj.summary() # will print



    #............................
    def build_model_fc(self,args): 
        not_implemente_234

    #............................
    def build_model_cnn1d(self,args): 

        X,Y=self.data['train']        
        time_dim=X.shape[1]
        latent_dim=Y.shape[1]        
        dropFrac=args.dropFrac
        epsilon_std = 0.99  # was 1.0
        KLD_strength=0.99
        
        # CNN params
        filterA = [4,6,8,10,12]
        kernel = 7
        pool_len   = 3 # how much time_bins get reduced per pooling
        numConv_lr=len(filterA)

        # - - - Assembling model 
        #. . . . . . .  encoder 
        x = Input(shape=(time_dim,),name='inp_X')
        u = Input(shape=(latent_dim,),name='inp_U')
        print('build_model_cnn1d inpX:',x.get_shape(),'  inpU:',u.get_shape(),'  KLD_strength=', KLD_strength)
        
        h = Reshape((-1, 1), name='pass0') (x) # because Conv1D wants rank-3 tensor 
        print('resh0=',h.get_shape())    
        pool_lr =h
        for i in range(numConv_lr):
            dim=filterA[i]
            print('qqq',i,dim)
            conv_lr = Conv1D(dim,kernel, border_mode='same', W_constraint=max_norm(3),name='enc%d_d%d_k%d'%(i,dim,kernel))(pool_lr)
            act_lr =LeakyReLU(name='en_act%d'%(i))(conv_lr)
            pool_lr = MaxPool1D(pool_length=pool_len, name='mxp_%d'%(i))(act_lr)

        ufc_goal=int(pool_lr.get_shape()[1]* pool_lr.get_shape()[2])    
        print('pool out',pool_lr.get_shape(), ufc_goal)

        h_lr=Flatten(name='pass1')(pool_lr) 
        print('flat=',h_lr.get_shape(),ufc_goal,type(ufc_goal))
        
        h=Dropout(dropFrac,name='df_%.2f'%dropFrac)(h_lr)

        hfc_goal=40
        h=Dense(hfc_goal , name='den1')(h)
        print('fc4=',h.get_shape())
        h=Dropout(dropFrac)(h)
        #.... encoded  has 2 pieces: mand and sigma
        z_mu0 = Dense(latent_dim, activation='linear',name='z_mu_%d'%latent_dim)(h) 
        z_log_var0 = Dense(latent_dim, activation='linear',name='z_var_log')(h)
        z_mu, z_log_var = KLDivergComp(strength=KLD_strength,name='add2loss')([z_mu0, z_log_var0,u])
        
        z_sigma = Lambda(lambda t: K.exp(.5*t), name='z_sig')(z_log_var0) 
        eps_gen=K.random_normal(stddev=epsilon_std,shape=(K.shape(x)[0], latent_dim))
        eps = Input(tensor=eps_gen,  name='eps_gen')
        z_eps = Multiply(name='stoch_sig')([ eps,z_sigma])
        z_lr = Add(name='stoch_z')([ z_mu,z_eps])
        print('build_model latent z+eps:',z_lr.get_shape())

        # .... decoder: 
        print('\nufc_goal',ufc_goal,'\n')
        z_ufc=Dense(ufc_goal,name='z_ufc' )(z_lr)
        print('z_ufc=',z_ufc.get_shape())  
        h =LeakyReLU(name='z_act')(z_ufc)  
        h=Dropout(dropFrac)(h)
        h= Dense(ufc_goal, activation='relu',name='den2')(h)
        h=Dropout(dropFrac)(h)
        print('ufc1=',h.get_shape())

        ufc_3d = Reshape((-1,filterA[-1] ), name='ufc_3d') (h) # because Conv1D wants rank 3 tensor 
        print('z_3d=',ufc_3d.get_shape())    
        pool_lr=ufc_3d
        for i0 in range(numConv_lr):
            i=numConv_lr -i0-1
            dim=filterA[i]
            print('ppp',i,dim)
            conv_lr = Conv1D(dim,kernel, border_mode='same', W_constraint=max_norm(3),name='dec%d_d%d_k%d'%(i,dim,kernel))(pool_lr)
            act_lr =LeakyReLU(name='de_act%d'%(i))(conv_lr)
            print('conv out',conv_lr.get_shape())
            pool_lr = UpSampling1D(size=pool_len, name='ups_%d'%(i))(act_lr)

        print('out_lr=',pool_lr.get_shape())
        conv_lr = Conv1D(1,kernel, border_mode='same', activation='tanh', W_constraint=max_norm(3),name='tanh')(pool_lr)
        decoded = Reshape((-1, ), name='out_x') (conv_lr) # restore rank 2 tensor
        print('dec=',decoded.get_shape())
        print('build_model decoded:',decoded.get_shape())

        # check for AE equal size of input vs. output
        if x.shape[1:] != decoded.shape[1:]: 
            print('Alert: in/out dimension mismatch !!!', x.shape,decoded.shape)
            fix_me12

        # full model
        vaeM = Model(inputs=[x, u, eps], outputs=decoded)
        numLay=len(vaeM.layers)
        print('vaeM: numLayers',numLay)

        jj=0; nBack=-1
        for lay in vaeM.layers:
            jjInv=numLay-jj-1
            print('lay:',jj,jjInv,lay.name)#,lay.get_shape())
            if lay.name=='stoch_z': nBack=jjInv
            jj+=1
        assert nBack>0

        # optimizer='adadelta', optimizer='rmsprop'
        vaeM.compile(optimizer='adadelta', loss=NegLogLikeh)

        # sub-models share waights with the full VAE model 
        encoderM = Model(inputs=[x, u], outputs=[z_mu0,z_sigma])

        # create a placeholder for an encoded (zip-dimensional) input
        tmp_z = Input(shape=(latent_dim,),name='inp_z')

        # retrieve last layer of the autoencoder to reach latent inputs
        h = vaeM.layers[-nBack](tmp_z)
        for jj in range(1,nBack):
            print('jj=',jj, -nBack+jj)
            h = vaeM.layers[-nBack+jj](h)

        # this model maps encoded representation to input image
        decoderM = Model(inputs=tmp_z, outputs=h) #net
        self.model={'auto':vaeM, 'encoder':encoderM, 'decoder':decoderM}
 
    #............................
    def train_model(self,args):
        X,Y=self.data['train']
        X_val,Y_val=self.data['val']
        
        if args.verb==0:
            print('train silently epochs:',args.epochs)
     
        callbacks_list = []
        if args.earlyStopOn:
            earlyStop=EarlyStopping(monitor='val_loss', patience=2+args.arrIdx, verbose=1, mode='auto')
            callbacks_list.append(earlyStop)

        if args.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.name+'.weights.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1)
            callbacks_list.append(ckpt)

        
        print('\nTrain_model X:',X.shape, ' earlyStop=',args.earlyStopOn,' epochs=',args.epochs,' batch=',args.batch_size)
        startTm = time.time()
        
        hir=self.model['auto'].fit([X,Y],X,callbacks=callbacks_list,
                 validation_data=([X_val,Y_val],X_val),  shuffle=True,
                 batch_size=args.batch_size, nb_epoch=args.epochs, 
                 verbose=args.verb)
        self.train_hirD=hir.history
        #evaluate performance for the last epoch
        loss=self.train_hirD['val_loss'][-1]
        fitTime=time.time() - startTm
        print('\n End Validation  Loss:%.3f'%loss,', fit time=%.1f min'%(fitTime/60.))
        self.train_sec=fitTime

    #............................
    def predict_model(self,dom,scope='auto'):
        X,Y=self.data[dom]

        model=self.model[scope]
        print('predict[%s] inputs:'%scope,model.input_shape,type(model.input_shape),', outputs:',model.output_shape)
        
        if scope=='encoder':
            if type(model.input_shape)==type(list()):
                out=model.predict([X,Y])
            else:
                out=model.predict(X)
            return out

        elif scope=='auto':
            if self.epsLayerExist:
                Xin=[X,Y]
            else:
                xshp,ushp,epsshp=self.model['auto'].input_shape
                eps_inp=np.zeros((Y.shape[0],epsshp[1]))
                Xin=[X,Y,eps_inp]
            out=model.predict(Xin)
            return out
            
        elif scope=='decoder':
            a=1
            
        else:
            print('abort predicton, unexpected scope=',scope)
            bad_bad2
    #............................
    def save_model_full(self):
        modelA=self.model
        for name in modelA:
            fname=self.outPath+'/'+self.name+'_'+name+'.model.h5'     
            print('save model  to',fname)
            modelA[name].save(fname)


   #............................
    def load_model_full(self):
        custObj={'KLDivergComp':KLDivergComp,'NegLogLikeh':NegLogLikeh}
        self.model={}
        for name in ['auto', 'encoder', 'decoder']:
            fname=self.outPath+'/'+self.name+'_'+name+'.model.h5'     
            print('load model from ',fname)
            model=load_model(fname,custom_objects=custObj) # creates model from HDF5
            if  self.verb>1 or name=='auto':  model.summary()
            self.model[name]=model


   #............................
    def eval_dom_loss(self,dom):
        print('eval_dom_loss dom=',dom)

        Xp,Yp=self.data[dom]
        Xq=self.predict_model(dom,'auto')
        Xp=Xp.reshape(Xp.shape[0],-1)
        Xq=Xq.reshape(Xp.shape[0],-1)
        print('ttll',type(Xq),Xp.shape,Xq.shape)
        
        mseV=[]; maeV=[];kldV=[]
        i=0
        fac=100
        for p,q in zip(Xp,Xq):
            i+=1
            mse=fac*mean_squared_error(p,q)
            mae=fac*mean_absolute_error(p,q)
            mseV.append(mse)
            maeV.append(mae)
            if i<5:
                print('i',i,len(p),len(q),mse,mae)

        self.eval={'Xp':Xp,'Yp':Yp,'Xq':Xq,'dom':dom}
        self.eval['mse']=np.array(mseV)
        self.eval['mae']=np.array(maeV)


    #............................
    def reveal_myFunc(self,dom,frId):
        print('reveal myFunc for dom=%s frameId=%d'%(dom,frId) )
        X=self.data['X_'+dom][frId]
        Y=self.data['Y_'+dom][frId]
        Z=np.reshape(Y,(-1,2))

        print('oz',Z.shape,Z)

        U=self.decoderM.predict(Z)
        print('oo',U.shape)
        U=U.flatten()
        print('oo-flat',U.shape)
        #print('ood',out1)
        
        for i in range(U.shape[0]):
            x=X[i][0]
            u=U[i]
            d=x-u
            print(i,x,u,d)
  

        
