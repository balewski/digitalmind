#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Pitchfork import Plotter_Pitchfork
from Deep_Pitchfork import Deep_Pitchfork

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train func2func',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('--design', dest='modelDesign', choices=['fc','cnn1d'],
                         default='cnn1d',
                         help=" model design of the network")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="fitting epoch")

    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.10,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", dest='earlyStopOn',
                         action='store_true',default=False,help="enable early stop")
    parser.add_argument( "--checkOn", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    args = parser.parse_args()
    args.prjName='pitchfork1_'+args.modelDesign
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
ppp=Plotter_Pitchfork (args )
deep=Deep_Pitchfork(args)

deep.load_data_hdf5('train')
deep.load_data_hdf5('val')
#deep.print_frames('train',numFrm=1)

#ppp.plot_data(deep.data,'train',range(6)) ; ppp.display_all(args,'xx')

deep.build_model(args)

#deep.reveal_myFunc('train', frId=2)  # dumps one input frame and the decoder output

ppp.plot_model(deep,1) # depth:0,1,2

deep.train_model(args) 
deep.save_model_full()

deep.eval_dom_loss('val') # full pass
ppp.plot_func_residua(deep.eval,'val-sample',range(6)) 
ppp.plot_dom_loss(deep,'val')

ppp.plot_train_history(deep,args,figId=10)

# Compute latent space
zV,sigV= deep.predict_model('val','encoder')
ppp.plot_latent_z(zV,sigV)
X,Y=deep.data['val']
ppp.plot_param_residua(Y,zV)


ppp.display_all(args,'train')


