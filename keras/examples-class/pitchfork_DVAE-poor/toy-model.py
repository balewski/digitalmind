#!/usr/bin/env python

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np
from keras.layers import Input, Dense, UpSampling1D, Reshape,Conv1D,Flatten,MaxPool1D
from keras.layers.advanced_activations import LeakyReLU # breaks model_save unless it is a separate layer
from keras import backend as K
from keras.models import Model
from keras.constraints import max_norm
print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))
#-------------------
def build_model(time_dim,z_dim ): # this has big input

    filterA = [4,6,8,10,12]
    kernel = 7
    pool_len   = 3 # how much time_bins get reduced per pooling
    numConv_lr=len(filterA)

    #. . . . . . .  encoder 
    input_shape=(time_dim,)
    x_input = Input(shape=input_shape, name='inp_x')
    print('build_model X_inp:',x_input.get_shape())
    x_3d = Reshape((-1, 1), name='pass0') (x_input) # because Conv1D wants rank 3 tensor 
    print('resh0=',x_3d.get_shape())    
    pool_lr =x_3d
    for i in range(numConv_lr):
        dim=filterA[i]
        print('qqq',i,dim)
        conv_lr = Conv1D(dim,kernel, border_mode='same', W_constraint=max_norm(3),name='enc%d_d%d_k%d'%(i,dim,kernel))(pool_lr)
        act_lr =LeakyReLU(name='en_act%d'%(i))(conv_lr)
        pool_lr = MaxPool1D(pool_length=pool_len)(act_lr)
        pool_lr = MaxPool1D(pool_length=pool_len, name='mxp_%d'%(i))(conv_lr)

    ufc_goal=int(pool_lr.get_shape()[1]* pool_lr.get_shape()[2])    
    print('pool out',pool_lr.get_shape(), ufc_goal)

    h_lr=Flatten(name='pass1')(pool_lr) 
    print('flat=',h_lr.get_shape(),ufc_goal,type(ufc_goal))

    z_lr = Dense(z_dim, activation="softmax",name='z_space')(h_lr)
    print('z_lr=',z_lr.get_shape())    

    # . . . . . . . . decoder
    print('\nufc_goal',ufc_goal,'\n')
    z_ufc=Dense(ufc_goal )(z_lr)
    print('z_ufc=',z_ufc.get_shape())    
    act_lr =LeakyReLU(name='z_act')(z_ufc)

    ufc_3d = Reshape((-1,filterA[-1] ), name='ufc_3d') (act_lr) # because Conv1D wants rank 3 tensor 
    print('z_3d=',ufc_3d.get_shape())    
    pool_lr=ufc_3d
    for i0 in range(numConv_lr):
        i=numConv_lr -i0-1
        dim=filterA[i]
        print('ppp',i,dim)
        conv_lr = Conv1D(dim,kernel, border_mode='same', W_constraint=max_norm(3),name='dec%d_d%d_k%d'%(i,dim,kernel))(pool_lr)
        act_lr =LeakyReLU(name='de_act%d'%(i))(conv_lr)
        pool_lr = MaxPool1D(pool_length=pool_len)(act_lr)
        print('conv out',conv_lr.get_shape())
        pool_lr = UpSampling1D(size=pool_len, name='ups_%d'%(i))(conv_lr)

    print('out_lr=',pool_lr.get_shape())
    conv_lr = Conv1D(1,kernel, border_mode='same', activation='tanh', W_constraint=max_norm(3),name='tanh')(pool_lr)
    decoded = Reshape((-1, ), name='out_x') (conv_lr) # restore rank 2 tensor
    print('dec=',decoded.get_shape())
    print('build_model decoded:',decoded.get_shape())

    # full model
    model = Model(input=x_input, output=decoded)
    myLoss='mean_squared_error'
    model.compile(loss = myLoss,optimizer = 'adam')

    model.summary() # will print

    print('\nFull  loss=%s layers=%d , params=%.1f K'%(myLoss,len(model.layers),model.count_params()/1000.))
    # check for AE equal size of input vs. output
    if x_input.shape[1:] != decoded.shape[1:]: 
        print('Alert: in/out dimension mismatch !!!', x_input.shape,decoded.shape)
        fix_me12

    return model

#=================================
#=================================
#  M A I N 
#=================================
#=================================
time_dim=1944  # good solutions w/ 5*pool: 8019 , 1944
z_dim=4
model=build_model(time_dim,z_dim)

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outG='out/cnn1d-AE.graph.svg'
    plot_model(model, to_file=outG, show_shapes=True, show_layer_names=True)  # works only on Cori
    print('saved model graph as ',outG)

outM='cnn1d-AE.model.h5'
model.save(outM)
print('saved h5 model as ',outM)


print('\ngenerate data and train')
num_eve=1000
X=np.random.normal( size=(num_eve,time_dim))

model.fit(X,X, batch_size=100, nb_epoch=2,verbose=1)
print('fit done, predicting')
Xnew=model.predict(X)
print('done, Xnew:',Xnew.shape)

from keras.models import load_model
model2=load_model(outM)
print('load h5 model h5 from ',outM)


