import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()

#from keras import utils as np_utils
from Util_Func import multiSpike_generator

from keras import backend as K
from keras import losses, optimizers 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout,UpSampling1D, Reshape,Conv1D,Flatten,MaxPool1D
from keras.models import Model,  load_model

from keras.layers.advanced_activations import LeakyReLU # breaks model_save unless it is a separate layer

import numpy as np
import h5py

from sklearn.metrics import mean_absolute_error,  mean_squared_error

print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


#............................
#............................
#............................
class Deep_ActiveLearn(object):
    """Contains all operations specific for this project"""

    def __init__(self,args):
        self.outPath=args.outPath
        self.dataPath=args.dataPath
        self.name=args.prjName
        self.events=args.events
        self.verb=args.verb
        self.modelDesign=args.modelDesign
        self.numSegm=20  #  K-fold partition of data frames
        self.kfoldOffset=args.kfoldOffset
        self.noiseAmpl=0.4 # added to traces
        self.actSegOff=0
        self.data={} # domains:train/val/test or segments:0/1/.../K
        self.train_hirD={'acc': [],'loss': [],'lr': [],'val_acc': [],'val_loss': []}

        print(self.__class__.__name__,'TF ver:', K.tf.__version__,', prj:',self.name)
        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            add_missing_dir999

#............................
    def generate_traces(self,args):
        out={}
        numSegm=self.numSegm

        print('split traces K-fold=',numSegm,' noise=',self.noiseAmpl)
        for seg in range(numSegm):
            out[seg]={'X':[],'Y':[],'AUX':[]}

        start = time.time()
        for i in range(args.events):
            funcV,fparmV=multiSpike_generator()
            if i%1000==0:
                print('gen i=',i,' pars',fparmV)

            #.... add noise to the traces
            noiseV= np.random.uniform( 0, self.noiseAmpl,size=funcV.shape)
            #print('nnn',noiseV[:10], self.noiseAmpl)
            funcV=np.add(funcV,noiseV)
            AuxV=[funcV.sum(),self.noiseAmpl]

            #.... assign segment
            sg=np.random.randint(numSegm)
            out[sg]['X'].append(funcV)
            out[sg]['Y'].append(fparmV)  
            out[sg]['AUX'].append(AuxV)

        print('%d frames generated , elaT=%.1f sec'%(args.events,(time.time() - start)))
        print('  achieved split to segments:',[ ('seg=',sg,len(out[sg]['Y'])) for sg in out ])
        return out
        
#............................
    def save_data_hdf5(self,segD):
       for sg in segD:
            X=np.array(segD[sg]['X'])
            Y=np.array(segD[sg]['Y'])
            Aux=np.array(segD[sg]['AUX'])
            self.data[sg]=[X,Y,Aux] # for plotting only
            outF=self.dataPath+'/actLrn_seg%d.frames.hd5'%sg

            h5f = h5py.File(outF, 'w')
            if sg==0 :
                print('save data as hdf5:',outF)
                print('h5-write X',sg,X.shape,'  Y',Y.shape,' AUX',Aux.shape)
            h5f.create_dataset('X', data=X)
            h5f.create_dataset('Y', data=Y)
            h5f.create_dataset('AUX',data=Aux)

            h5f.close()

            xx=os.path.getsize(outF)/1048576
            print('closed  hdf5:',outF,' size=%.2f MB, frames=%d'%(xx,Y.shape[0]))

    #............................
    def prep_labeled_input(self,dom):
        numTrainSegm=2 # must be the same for all calls
        start=time.time()

        numSegm=self.numSegm
        print('\nprep_labeled scaffolds for dom:',dom, ',max num segments:',numSegm)
        assert numSegm>=numTrainSegm+2  # makes no sense to split to train/eval/test
        kOff=self.kfoldOffset

        kL=[]
        if dom=='val':
            jL=(kOff+numTrainSegm)%numSegm # one element                
            kL=[jL]

        if dom=='test':
            jL=(kOff+numTrainSegm+1)%numSegm # one element 
            kL=[jL]

        if dom=='nextAct':
            assert(self.actSegOff< numSegm - (numTrainSegm+2)) # avoid reusing segements
            jL=(kOff+numTrainSegm+2+self.actSegOff)%numSegm # one element
            self.actSegOff+=1 # move segment pointer by one
            kL=[jL]

        if dom=='train':
            kL=[ (kOff+j)%numSegm for j in range(numTrainSegm)]

        print('segL:',kL,' dom:',dom,', kfoldOffset=', self.kfoldOffset)
        assert len(kL)>0 

        assert len(kL)>0

        xyL= ['X','Y','AUX']
        domD={ x:[]  for x in xyL }
        for k in kL:
            objD=self.load_segment_hdf5(k,k==kL[0])
            for x in xyL:  domD[x].append(objD[x]) # first stack np-arrays in a list

        #print('qq1',type(domD['X']))
        #print('qq2',type(domD['X'][0]))
        self.data[dom]=[ np.concatenate(domD[x],axis=0) for x in xyL ]

        print('add %d segments, sizes X,Y:'%len(kL),self.data[dom][0].shape,self.data[dom][1].shape)

        print('prep:',dom,' completed, elaT=%.1f sec'%(time.time() - start))

    #............................
    def load_segment_hdf5(self,sg,flag=0):
        inpF=self.dataPath+'/actLrn_seg%d.frames.hd5'%sg
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            if flag or x=='X': print('read ',x,obj.shape,' seg=',sg)
            objD[x]=obj

        h5f.close()
        return objD


 

    #............................
    def print_frames(self,dom,numFrm=1):
        X,Y,Aux=self.data[dom]
        print('sample of ',numFrm,' frames from dom=',dom)
        print(' types',type(X))
        print(' shapes',X.shape,Y.shape)
        for it in range(numFrm):
            print('it:',it,'par:',Y[it],'\n frmRbg:',X[it,::20],' Aux:',Aux[it])

            
    #............................
    def build_model(self,args): 
        if args.modelDesign=='fc':
            self.build_model_fc(args)
        else:
            print('unknow design:',args.modelDesign)
            bad_bad17

        # - - -  Model assembled and compiled
        
        mmobj=self.model
        print('\nSummary   layers=%d , params=%.1f K, inputs:'%(len(mmobj.layers),mmobj.count_params()/1000.),mmobj.input_shape)
        
        mmobj.summary() # will print


    #............................
    def build_model_fc(self,args): 
        X,Y,AUX=self.data['train']
        time_dim=X.shape[1]        
        out_dim=Y.shape[1]        
        dense_dim = 20
        dense_layer=2
        dropFrac=args.dropFrac

        # - - - Assembling model 
        x = Input(shape=(time_dim,),name='inp_X')

        print('build_model_fc inpX:',x.get_shape(),' dense_layer=',dense_layer)
        h=x    
        for i in range(dense_layer):
            h = Dense(dense_dim,name='enc%d'%i)(h) 
            h = LeakyReLU(name='act%d'%i)(h)
            h = Dropout(dropFrac,name='drop%d'%i)(h) 

        y= Dense(out_dim, activation='linear',name='out_%d'%out_dim)(h) 

        print('build_model_fc:',y.get_shape())
        # full model
        model = Model(inputs=x, outputs=y)
  
        model.compile(optimizer='adam', loss='mse')
        self.model=model

  #............................
    def train_model(self,args):
        X,Y,Aux=self.data['train']
        X_val,Y_val,Aux_val=self.data['val']

        if args.verb==0:
            print('train silently epochs:',args.epochs)

        callbacks_list = []
        if args.earlyStopOn:
            earlyStop=EarlyStopping(monitor='val_loss', patience=2+args.arrIdx, verbose=1, mode='auto')
            callbacks_list.append(earlyStop)

        if args.checkPtOn:
            #outFw='weights.{epoch:02d}-{val_loss:.2f}.h5'
            outF5w=self.name+'.weights.h5'
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=1)
            callbacks_list.append(ckpt)

        print('\nTrain_model X:',X.shape, ' earlyStop=',args.earlyStopOn,' epochs=',args.epochs,' batch=',args.batch_size,'  modelDesign=', self.modelDesign)
        startTm = time.time()

        fitVerb=1   # prints live:  [=========>....] - ETA: xx s 
        if args.verb==2: fitVerb=1
        if args.verb==0: fitVerb=2  # prints 1-line summary at epoch end

        hir=self.model.fit(X,Y,callbacks=callbacks_list,
                 validation_data=(X_val,Y_val),  shuffle=True,
                 batch_size=args.batch_size, nb_epoch=args.epochs,
                           verbose=fitVerb)
        # allows appending history 
        hir=hir.history
        for obs in hir:
            rec=[ float(x) for x in hir[obs] ]
            self.train_hirD[obs].extend(rec)

        #evaluate performance for the last epoch
        loss=self.train_hirD['val_loss'][-1]
        fitTime=time.time() - startTm
        print('\n End Validation  Loss:%.3f'%loss,', fit time=%.1f min'%(fitTime/60.))
        self.train_sec=fitTime

    #............................
    def save_model_full(self):
        model=self.model
        fname=self.outPath+'/'+self.name+'.model.h5'     
        print('save model  to',fname)
        model.save(fname)


   #............................
    def load_model_full(self):

        fname=self.outPath+'/'+self.name+'.model.h5'     
        print('load model from ',fname)
        self.model=load_model(fname) # creates model from HDF5
        self.model.summary()


 
#............................
    def get_correl(self,A):
        #print('a dims:',a.shape,'\n dump a:')

        # compute cov matrix
        c=np.cov(A,rowvar=False)
        nv=c.shape[0]
        nvL=range(nv)
        #print('cov M:')
        #pprint(c)
        c1=np.diagonal(c)
        c2=np.sqrt(c1)
        print('one std dev:',c2)
        avrSig=c2.mean()
        print('global  residua avr=%.3f  var=%.3f '%(avrSig, avrSig*avrSig))
        parNameL=['par_%d'%i for i in nvL]
        magU=0.5

        print('\nnames ',end='')
        [ print('      %s'%parNameL[i],end='') for i in nvL]

        print('\nresidua/%.1f: '%magU,end='')
        [ print('U%d-> %4.1f%s  '%(i,100*c2[i]/magU,chr(37)),end='') for i in nvL ]

        # hack-guessed loss assuming U4 was x2
        # c2[4]*=2; - loss(U4) was weigher x4  during training
        lossMAE=np.absolute(c2).mean()
        c3=np.power(c2,2);    lossMSE=c3.mean()
        print('\n global lossMSE=%.4f , lossMAE=%.4f (computed)'%( lossMSE,lossMAE))


        #print('\ncorrelation matrix for %d dim vector'%nv)
        r=np.corrcoef(A,rowvar=False)
        #pprint(r)

        print('\nnames  ',end='')
        [ print('   %s '%parNameL[i],end='') for i in nvL]
        print('\ncorrM  ',end='')
        [ print('  __var%1d__ '%i,end='') for i in nvL]
        for k in nvL:
            print('\n var%1d '%k,end='')
            [ print('     %6.3f'% r[i][k],end='') for i in range(k+1)]
            #print('      #  '*(nv-k),end='')
        print()

    #............................
    def active_grow_train_data(self,nSigThr): # nSig>mean
        print('\n *******\n Active learning, actSegOff=',self.actSegOff,' nSigThr=',nSigThr)
        dom='nextAct'
        self.prep_labeled_input(dom)
        X,U,Aux=self.data[dom]
        Z= self.model.predict(X)
        
        # compute variance per trace
        nPar=U.shape[1]
        ZmU=Z-U
        var2d=np.power(ZmU,2)
        print('var2d shape',var2d.shape)
        Var=np.sum(var2d,axis=1)
        print('Var shape',Var.shape)
        mse=Var.mean()/nPar
        mseErr=Var.std()/nPar
        varThr=nPar*(mse+nSigThr*mseErr) # variance is summed w/o division by nPar
        
        print('seg MSE=',mse,' +/-',mseErr, ' apply varThr=',varThr)
    
        #filter out good predictions
        
        
        Un=[]; Zn=[]; Xn=[]; Auxn=[]
        for i in range(U.shape[0]):
            if Var[i]<varThr : continue
            #if var2d[i][2]<varThr : continue  #testing only
            Xn.append(X[i])
            Un.append(U[i])
            Auxn.append(Aux[i])
            Zn.append(Z[i])

        n0=U.shape[0]
        n1=len(Un)
        print('survived %d traces, frac=%.3f'%(n1,n1/n0))

        # append new traces to training data
        m0=self.data['train'][1].shape[0]
        #print('sh0=',self.data['train'][0].shape)
        out=[]
        for obj,ext in zip(self.data['train'], [Xn,Un,Auxn]):
            #print('sum',obj.shape,len(ext))
            obj=np.append(obj,ext,axis=0)
            #print('done',obj.shape)
            out.append(obj)
        self.data['train']=out # replace list with new longer version
        print('sh1=',self.data['train'][0].shape)

        return

        # overwrite old variables
        Xn=np.array(Xn)
        Un=np.array(Un)
        Auxn=np.array(Auxn)                   
        Zn=np.array(Zn)

        self.get_correl(Z-U)

        return U,Z
