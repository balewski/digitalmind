#!/usr/bin/env python
""" 
produce training data, save as hd5
Example VAE with diven inputs

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_ActiveLearn import Plotter_ActiveLearn
from Deep_ActiveLearn import Deep_ActiveLearn

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Formater of VAE_Seq2Func data',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")


    parser.add_argument("-n", "--events", type=int, default=100,
                        help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='activeLearn'
    args.arrIdx=0
    args.modelDesign='bad12'
    args.kfoldOffset=0 # not used in formatting

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

ppp=Plotter_ActiveLearn (args )
deep=Deep_ActiveLearn(args)

segD=deep.generate_traces(args)
deep.save_data_hdf5(segD)
seg=3
deep.print_frames(seg,numFrm=1)

ppp.plot_data_vsTime(deep.data,seg,range(6))
ppp.plot_data_fparams(deep.data[seg][1],'true U')

ppp.display_all(args,'form')
