import os
import numpy as np
from matplotlib import cm as cmap
import socket  # for hostname
from tensorflow.contrib.keras.api.keras.utils import plot_model

import time
from sklearn.manifold import TSNE
from Util_Func import get_correlation

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_ActiveLearn(object):
    """Graphic ops related to formatin,training, eval of deep net """

    def __init__(self, args):
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        self.maxU=0.8


    #............................
    def display_all(self,args,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()
                figName='%s/idx%d_%s_%s_f%d'%(self.outPath,args.arrIdx,args.prjName,ext,fid)
                print('Graphics saving to %s PDF ...'%figName)
                self.plt.savefig(figName+'.pdf')
        self.plt.show()

#............................
    def plot_model(self,deep,flag=0):
        #print('plot_model broken in TF=1.4, skip it'); return

        if 'cori' not in socket.gethostname(): return  # software not installed
        model=deep.model
        fname=self.outPath+'/'+deep.name+'_graph.svg'
        plot_model(model, to_file=fname, show_layer_names=flag>0, show_shapes=flag>1)
        print('Graph saved as ',fname,' flag=',flag)

#............................
    def plot_data_vsTime(self,dataA,dom,idxL,figId=7):
        self.figL.append(figId)
        X,Y,Aux=dataA[dom]
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,5))
       
        nrow,ncol=2,3
        print('plot input for idx=',idxL)
        j=0
        for idx in idxL:
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            amplV=X[idx]
            ax = self.plt.subplot(nrow, ncol, 1+j)
            j+=1
            tit='%d '%(idx)
            ptxtL=[ '%.2f'%x for x in Y[idx]]
            tit+=' par:'+','.join(ptxtL)
            ax.set(title=tit[:30],ylabel='ampl', xlabel='time')
            ax.plot(amplV)

            ax.axhline(0, color='blue', linestyle='--')
            ax.set_ylim(-0.2,1)
            #ax.set_xlim(200,800)

            ax.grid()
            
    #............................
    def plot_train_history(self,deep,args,figId=10):
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,3))

        nrow,ncol=1,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2 )

        DL=deep.train_hirD
        nSkip=1
        valLoss=DL['val_loss'][-1]
        tit1='%s, train %.1f min, idx=%d, end-val=%.3g'%(deep.name,deep.train_sec/60.,args.arrIdx,valLoss)
        ax1.set(ylabel='loss ',title=tit1,xlabel='epochs')            # use Y.shape
        ax1.plot(DL['loss'],'.-.',label='train n=%d'%deep.data['train'][1].shape[0])
        ax1.plot(DL['val_loss'],'.-',label='val n=%d'%deep.data['val'][1].shape[0])
        ax1.legend(loc='best')

        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')


  
        
#............................
    def plot_param_residua(self,U,Z,figId=9):
        nPar=Z.shape[1]
        nrow,ncol=1,nPar
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(2.5*nPar,2.5))
 

        assert U.shape[1]==Z.shape[1]
        j=0
        for iPar in range(0,nPar):
            ax=self.plt.subplot(nrow,ncol,j+1)
            u=U[:,iPar]
            z=Z[:,iPar]
            img=ax.scatter(u,z, alpha=.4, s=8)
            ax.set(title='U-Z iPar=%d'%iPar, xlabel='true U_%d'%iPar, ylabel='Z_%d'%iPar)
            j+=1
 
        return

#............................
    def plot_latent_z(self,Z,figId=11):
        nPar=Z.shape[1]
        nrow,ncol=1,int(nPar/2+0.5) # displays 2 pars per plot
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        if figId in self.figL: # append this plot to training history
            self.plt.figure(figId)
            nrow,ncol=1,3
        else:
            self.figL.append(figId)
            self.plt.figure(figId,facecolor='white', figsize=(2.5*ncol,2.5))

        # plot correlation between latent space variables - should be none
        j=0
        for iPar in range(0,ncol):
            iPar2=(iPar+ncol)%nPar
            y1=Z[:,iPar] ; y2=Z[:,iPar2]
            ax=self.plt.subplot(nrow,ncol,j+1)
            j+=1
            img=ax.scatter(y1,y2, alpha=.4, s=8, cmap='cool')
            ax.set(title='pred Z %d-%d'%(iPar,iPar2), xlabel='Z_%d'%iPar, ylabel='Z_%d'%(iPar2))
 
#............................
    def plot_data_fparams(self,U,tit,figId=6):
        nPar=U.shape[1]

        nrow,ncol=2,int(nPar/2+1.5) # displays 2 pars per plot

        fig=self.plt.figure(figId,facecolor='white', figsize=(3.*ncol,2.6*nrow))
        self.figL.append(figId)
        mm=self.maxU
        binsX= np.linspace(-mm,mm,30)

        j=1
        for i in range(0,nPar,2):
            iPar=min(i,U.shape[1]-2) # account for odd num of params
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            self.plt.colorbar(img, ax=ax)
            ax.set(title=tit, xlabel='par_%d'%iPar, ylabel='par_%d'%(iPar+1))
            ax.grid()

        for i in range(0,nPar):
            y1=U[:,i]
            ax=self.plt.subplot(nrow,ncol,j) ; j+=1
            ax.hist(y1,bins=50)
            ax.set(xlabel='par_%d'%(i))
            ax.set_xlim(-mm,mm)
            ax.grid()

#............................
    def plot_param_residua(self,U,Z,figId=9,tit=''):
        nPar=Z.shape[1]
        nrow,ncol=3,nPar
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        self.figL.append(figId)
        inSz=2.5
        self.plt.figure(figId,facecolor='white', figsize=(inSz*nPar*1.2,nrow*inSz))

        assert U.shape[1]==Z.shape[1]
        self.parSum=[]

        j=0
        mm=self.maxU
        binsU= np.linspace(-mm*1.5,mm*1.5,90)
        binsX= np.linspace(-mm,mm,30)
        binsY= np.linspace(-mm/2,mm/2,30)

        for iPar in range(0,nPar):
            ax1 = self.plt.subplot(nrow,ncol, j+1)
            ax2 = self.plt.subplot(nrow,ncol, j+1+ncol*2)
            ax3 = self.plt.subplot(nrow,ncol, j+1+ncol*1)
            ax1.grid(); ax2.grid(); ax3.grid()

            u=U[:,iPar]
            z=Z[:,iPar]
            rho,sigRho=get_correlation(u,z)

            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            # beutification
            self.plt.colorbar(img, ax=ax1)
            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=1,transform=ax1.transAxes) #diagonal
            # 

            ax1.set(title='corr iPar=%d'%iPar, xlabel='pred Z_%d'%iPar, ylabel='true U_%d'%iPar)
            ax1.text(0.2,0.1,'rho=%.3f +/- %.3f'%(rho,sigRho),transform=ax1.transAxes)

            if iPar==0: ax1.text(0.1,0.9,tit,transform=ax1.transAxes)
            if iPar==1: ax1.text(0.1,0.9,'n=%d'%(u.shape[0]),transform=ax1.transAxes)


            # .... compute residua 
            zmu=z-u
            resM=zmu.mean()
            resS=zmu.std()
            sum={'resM':resM,'res-std-dev':resS,'rho':rho,'sigRho':sigRho}
            self.parSum.append(sum)

            # ......  2D residua
            zsum,xbins,ybins,img = ax3.hist2d(z,zmu,bins=[binsX,binsY], cmin=1,
                                   cmap = self.plt.cm.rainbow)
            self.plt.colorbar(img, ax=ax3)
            ax3.set( xlabel='pred Z_%d'%iPar, ylabel='residue Z-U')
            ax3.axhline(0, color='green', linestyle='--')
            ax3.set_xlim(-mm,mm) ; ax3.set_ylim(-mm/2,mm/2)

            # ..... 1D residua
            ax2.hist(zmu,bins=binsU)

            ax2.set(title='avr res%d: %.2f+/-%.2f'%(iPar,resM,resS), xlabel='residue: Z_%d - U_%d'%(iPar,iPar), ylabel='traces')

            ax2.set_xlim(-mm/2,mm/2)

            j+=1

#............................
    def plot_param_summary(self,figId=8,tit=''):
        nrow,ncol=1,2
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        self.figL.append(figId)
        inSz=3.0
        self.plt.figure(figId,facecolor='white', figsize=(inSz*ncol,nrow*inSz))

        j=0
        for vName in ['res-std-dev','rho']:
            ax = self.plt.subplot(nrow,ncol, j+1)
            j+=1
            valL=[ x[vName] for x in self.parSum]
            #print(vName,valL)

            sym1='ro'
            if j==2 : sym1='bs'
            ax.plot(valL,sym1)
            ax.set(title=tit, xlabel='iParam',ylabel=vName)
            #if j==1: ax.set_ylim(0.4,0.9)
            #if j==2: ax.set_ylim(0.7,1.1)

            ax.grid()

            avr1=np.array(valL).mean()
            ax.axhline(avr1, color='green', linestyle='--')
            ax.text(1,avr1+0.05,'avr=%.3f'%avr1)
