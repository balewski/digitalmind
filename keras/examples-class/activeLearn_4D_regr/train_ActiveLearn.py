#!/usr/bin/env python
""" read input hd5 tensors
train net
write net + weights as HD5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_ActiveLearn import Plotter_ActiveLearn
from Deep_ActiveLearn import Deep_ActiveLearn

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='train func_invert',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--arrIdx", type=int, default=1,
                        help="slurm array index")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument('--design', dest='modelDesign', choices=['fc','fcX'],
                         default='fc',
                         help=" model design of the network")

    parser.add_argument("--dataPath",
                        default='data',help="path to input/output")
    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("-k", "--kfoldOffset", type=int, default=0,
                        help="decides which segments merge for training")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument("-e", "--epochs", type=int, default=2,
                        help="fitting epoch")

    parser.add_argument("-b", "--batch_size", type=int, default=100,
                        help="fit batch_size")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.30,
                        help="drop fraction at all layers")
    parser.add_argument( "-s","--earlyStop", dest='earlyStopOn',
                         action='store_true',default=False,help="enable early stop")
    parser.add_argument( "--checkOn", dest='checkPtOn',
                         action='store_true',default=False,help="enable check points for weights")

    args = parser.parse_args()
    args.prjName='activeLearn_'+args.modelDesign
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
ppp=Plotter_ActiveLearn (args )
deep=Deep_ActiveLearn(args)

deep.prep_labeled_input('val')

deep.prep_labeled_input('train')
ppp.plot_data_vsTime(deep.data,'val',range(6))
#ppp.plot_data_fparams(deep.data['val'][1],'true U',figId=6); ppp.display_all(args,'xx')

deep.build_model(args)

ppp.plot_model(deep,1) # depth:0,1,2

deep.train_model(args)
for aa in range(12):
    deep.active_grow_train_data(0.5) # chi2> mean+nSig
    deep.train_model(args)

#U,Z=
#tit='nextAct %d'%(deep.actSegOff)
#ppp.plot_param_residua(U,Z,tit=tit,figId=20+deep.actSegOff)
#ppp.display_all(args,'train')
#okok22
 
deep.save_model_full()

ppp.plot_train_history(deep,args,figId=10)

# Compute latent space
dom='val'
X,U,Aux=deep.data[dom]
Z= deep.model.predict(X)
ppp.plot_latent_z(Z)
deep.get_correl(U-Z)

tit='dom=%s idx=%d'%(dom,args.arrIdx)
ppp.plot_param_residua(U,Z,tit=tit)
#1 ppp.plot_param_summary(tit=tit )# only after _residua

ppp.display_all(args,'train')


