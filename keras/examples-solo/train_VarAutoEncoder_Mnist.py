#!/usr/bin/env python

#Toy Variational Autoencoder on MNIST data

# based on http://louistiao.me/posts/implementing-variational-autoencoders-in-keras-beyond-the-quickstart-tutorial/
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

import warnings,os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

from keras import backend as K
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply
from keras.models import Model, Sequential
from keras.datasets import mnist


original_dim = 784
intermediate_dim = 256
latent_dim = 2
batch_size = 100
epochs = 30
epsilon_std = 1.0


def NegLogLikeh(y_true, y_pred):
    """ Negative log likelihood (Bernoulli). """
    # keras.losses.binary_crossentropy gives the mean
    # over the last axis. we require the sum
    return K.sum(K.binary_crossentropy(y_true, y_pred), axis=-1)


class KLDivergComp(Layer):
    """ Identity transform layer that adds KL divergence
    to the final model loss.
    """
    def __init__(self, *args, **kwargs):
        self.is_placeholder = True
        super(KLDivergComp, self).__init__(*args, **kwargs)
    def call(self, inputs): # it computes loss due to latent vars per mini-batch
        mu, log_var = inputs
        kl_batch=-0.5 * K.sum(1 +log_var -K.square(mu) -K.exp(log_var), axis=-1)
        self.add_loss(K.mean(kl_batch), inputs=inputs)
        return inputs  # pass by

# - - - Assembling model 

x = Input(shape=(original_dim,),name='inp_img')
h = Dense(intermediate_dim, activation='relu',name='hid_encode')(x)

z_mu0 = Dense(latent_dim,name='z_mu0')(h)
z_log_var0 = Dense(latent_dim,name='z_var_log0')(h)

z_mu, z_log_var = KLDivergComp(name='add2loss')([z_mu0, z_log_var0])

z_sigma = Lambda(lambda t: K.exp(.5*t), name='z_sig')(z_log_var0) 

eps_gen=K.random_normal(stddev=epsilon_std,shape=(K.shape(x)[0], latent_dim))
eps = Input(tensor=eps_gen,  name='eps_gen')
z_eps = Multiply(name='stoch_sig')([ eps,z_sigma])
z = Add(name='stoch_z')([ z_mu,z_eps])

# "decoder" 
h2=Dense(intermediate_dim, input_dim=latent_dim, activation='relu',name='hid_decode')(z)
decoded =  Dense(original_dim, activation='sigmoid',name='out_img')(h2)

# full model
vaeM = Model(inputs=[x, eps], outputs=decoded)
vaeM.compile(optimizer='rmsprop', loss=NegLogLikeh)

# sub-models share waights with the full VAE model 
encoderM = Model(inputs=x, outputs=[z_mu0,z_sigma])

# create a placeholder for an encoded (zip-dimensional) input
tmp_z = Input(shape=(latent_dim,),name='inp_z')

# retrieve last 2 layers of the autoencoder to reach latent inputs
net = vaeM.layers[-2](tmp_z)
net = vaeM.layers[-1](net)
# this model maps encoded representation to input image
decoderM = Model(inputs=tmp_z, outputs=net)


# - - -  Model completed

print('\nThis is full VAE:')
vaeM.summary() # will print
print('\nThis is Encoder:')
encoderM.summary() # will print
print('\nThis is Decoder:')
decoderM.summary() # will print

print('save models as SVG')
from tensorflow.contrib.keras.api.keras.utils import plot_model
plot_model(vaeM, to_file='vae_full.graph.svg', show_shapes=True, show_layer_names=True)  # owrks only on Cori
plot_model(encoderM, to_file='vae_encoder.graph.svg', show_shapes=True, show_layer_names=True)  
plot_model(decoderM, to_file='vae_decoder.graph.svg', show_shapes=True, show_layer_names=True) 

# train the VAE on MNIST digits
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train = x_train.reshape(-1, original_dim) / 255.
x_test = x_test.reshape(-1, original_dim) / 255.

vaeM.fit(x_train, x_train, shuffle=True, epochs=epochs, batch_size=batch_size,
        validation_data=(x_test, x_test))

print('training done, next show latent space')


# display a 2D plot of the digit classes in the latent space
z_mu_test,z_sig_test = encoderM.predict(x_test, batch_size=batch_size*2)
plt.figure(figsize=(5.5, 4.5))
plt.scatter(z_mu_test[:, 0], z_mu_test[:, 1], c=y_test, alpha=.4, s=3**2, cmap='tab10')
plt.xlabel('latent mu-vector')
plt.xlabel('latent Z_0')
plt.ylabel('latent Z_1')
cb=plt.colorbar()
cb.set_label('10 MNIST digits')

plt.figure(figsize=(10, 4))
for i in range(2):
    ax=plt.subplot(1,2,i+1)
    zVec=z_mu_test ; ztxt='Z_mu'
    if i : 
        zVec=z_sig_test
        ztxt='Z_sig'
    img=ax.scatter(zVec[:, 0], zVec[:, 1], c=y_test, alpha=.4, s=3**2, cmap='tab10')
    ax.set(title=ztxt+' latent space', xlabel='latent Z_0',ylabel='latent Z_1')
    ax.grid()
    plt.colorbar(img).set_label('10 MNIST digits')


# display a 2D manifold of the digits
n = 15  # figure with 15x15 digits
digit_size = 28

# linearly spaced coordinates on the unit square were transformed
zmx=4
u_grid = np.dstack(np.meshgrid(np.linspace(-zmx, zmx, n),
                               np.linspace(-zmx, zmx, n)))
z_grid=u_grid
print('z_grid:',z_grid.shape)
plt.figure(figsize=(8, 4))
for i in range(2):
    ax=plt.subplot(1,2,i+1)
    img=ax.imshow( z_grid[:,:,i], cmap='cool')
    ax.set(title='Z_%d latent space'%i)
    plt.colorbar(img)

x_decoded = decoderM.predict(z_grid.reshape(n*n, 2))
img_decoded = x_decoded.reshape(n, n, digit_size, digit_size)

plt.figure(figsize=(7, 7))
plt.imshow(np.block(list(map(list, img_decoded))), cmap='gray')
plt.title(' 2D manifold of the digits in latent space')
plt.tight_layout()
plt.show()
