#!/usr/bin/env python
from __future__ import print_function
""" 
generic Keras

Example:  regression of 1D variable
true model X=F(U)
 U range [-0.5,0.5] 
 X= A*U+B + gauss(0,eps)

U.shape=(-1,1)
X.shape=(-1,tBins)

ML model: find Y given X

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Recognize MNIST  using LSTM',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--project",
                        default='regr1d',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-e", "--epochs", type=int, default=5,
                        help="fitting epoch")
    parser.add_argument("-b", "--batch_size", type=int, default=200,
                        help="batch_size for training")
    parser.add_argument("-n", "--events", type=int, default=100000,
                        help="events for training, use 0 for all")
    parser.add_argument("--dropFrac", type=float, default=0.2,
                        help="drop fraction at all layers")
    parser.add_argument( "--noXterm", dest='noXterm', action='store_true',
                         default=False,help="disable X-term for batch mode")
    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
 
    return args

# - - - - - - - - -   actual  code - - - - - - - - - 
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np


from keras.layers import Input, Dense , Dropout
from keras.models import Model
#from keras.layers.advanced_activations import LeakyReLU 
from keras.constraints import maxnorm
import tensorflow as tf

print('libs imported, TF.ver=%s elaT=%.1f sec'%(tf.__version__,(time.time() - start)))


#-------------------
def  plot_fitHir(DL,tit='abc',figId=10):
       nrow,ncol=1,1
       #  grid is (yN,xN) - y=0 is at the top,  so dumm
       plt.figure(figId,facecolor='white', figsize=(8,4))

       ax1 = plt.subplot(nrow,ncol,1)
       valLoss=DL['val_loss'][-1]

       ax1.set(ylabel='loss ',title=tit,xlabel='epochs')            # use Y.shape
       ax1.plot(DL['loss'],'.-.',label='train')
       ax1.plot(DL['val_loss'],'.-',label='val')
       ax1.legend(loc='best')
       
       ax1.set_yscale('log')
       ax1.grid(color='brown', linestyle='--',which='both')
       
#............................
#............................
def plot_data_vsTime(X,Y,idxL,figId=7):
        plt.figure(figId,facecolor='white', figsize=(12,5))

        nrow,ncol=2,3
        print('plot input for idx=',idxL)
        j=0
        for idx in idxL:
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            amplV=X[idx]
            ax = plt.subplot(nrow, ncol, 1+j)
            j+=1
            tit='%d '%(idx)
            ptxtL=[ '%.2f'%x for x in Y[idx]]
            tit+=' par:'+','.join(ptxtL)
            ax.set(title=tit[:30],ylabel='ampl', xlabel='time')
            ax.plot(amplV,'o')
            #img=ax.scatter(amplV, alpha=.4, s=8)


            ax.axhline(0, color='blue', linestyle='--')
            ax.set_ylim(-1.,1.2)
            #ax.set_xlim(200,800)

            ax.grid()
            plt.tight_layout()

#............................
def plot_param_residua(U,Z,tit='',figId=9):
        nPar=Z.shape[1]
        nrow,ncol=1,5
        #  grid is (yN,xN) - y=0 is at the top,  so dumm

        inSz=2.5
        plt.figure(figId,facecolor='white', figsize=(inSz*ncol*1.2,nrow*inSz))

        figName='out/1dReg_%d'%figId
        
        plt.savefig(figName+'.pdf')

        assert U.shape[1]==Z.shape[1]

        j=0
        mm1=-0.55; mm2=0.55;
        #mm1=-0.05; mm2=1.05;
        dmm=0.3
        binsU= np.linspace(mm1*1.5,mm2*1.5,90)
        binsX= np.linspace(mm1,mm2,40)
        binsY= np.linspace(-dmm,dmm,40)

        for iPar in range(0,nPar):
            ax1 = plt.subplot(nrow,ncol, j+1)
            ax2 = plt.subplot(nrow,ncol, j+3)
            ax3 = plt.subplot(nrow,ncol, j+2)
            ax4 = plt.subplot(nrow,ncol, j+4)
            ax5 = plt.subplot(nrow,ncol, j+5)
            ax1.grid(); ax2.grid(); ax3.grid(); ax4.grid(); ax5.grid()

            u=U[:,iPar]
            z=Z[:,iPar]

            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            # beutification
            plt.colorbar(img, ax=ax1)
            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=1,transform=ax1.transAxes) #diagonal
            # 

            ax1.set(title='corr iPar=%d'%iPar, xlabel='pred Z_%d'%iPar, ylabel='true U_%d'%iPar)

            if iPar==0: ax1.text(0.1,0.9,tit,transform=ax1.transAxes)

            # .... compute residua 
            zmu=z-u
            resM=zmu.mean()
            resS=zmu.std()

            # ......  2D residua
            zsum,xbins,ybins,img = ax3.hist2d(z,zmu,bins=[binsX,binsY], cmin=1,
                                   cmap = plt.cm.rainbow)
            plt.colorbar(img, ax=ax3)
            ax3.set( xlabel='pred Z_%d'%iPar, ylabel='residue Z-U')
            ax3.axhline(0, color='green', linestyle='--')
            #ax3.set_xlim(mm1,mm2) ; ax3.set_ylim(mm1/2,mm2/2)

            # ..... 1D residua
            ax2.hist(zmu,bins=binsU)

            ax2.set(title='res%d: %.3f+/-%.3f'%(iPar,resM,resS), xlabel='residue: Z_%d - U_%d'%(iPar,iPar), ylabel='traces')

            ax2.set_xlim(-dmm,dmm)

            # .... Z-histo            
            kk=9
            hsum,xbins,img=ax4.hist(z,bins=binsX,color='blue')
            hh=hsum[kk:-kk]
            #print('aa',xbins[kk],xbins[-kk],binsX)
            avr=hh.mean(); err=hh.std(); tit='avr=%.1f+/-%.1f'%(avr,err)

            x1=(xbins[kk]-xbins[0])/(xbins[-1] - xbins[0])
            x2=(xbins[-kk]-xbins[0])/(xbins[-1] - xbins[0])
            #x1=0.2; x2=0.8
            print(figId,'ax4 kk',kk,x1,x2,tit)
            ax4.set( xlabel='pred Z_%d'%iPar,title=tit)
            ax4.axhline(avr, xmin=x1,xmax=x2, color='red', linestyle='--')


            # .... U-histo
            hsum,xbins,img=ax5.hist(u,bins=binsX,color='green')
            hh=hsum[kk:-kk]
            avr=hh.mean(); err=hh.std(); tit='avr=%.1f+/-%.1f'%(avr,err)
            print('ax5 kk',kk,tit)
            ax5.set( xlabel='true U_%d'%iPar,title=tit)
            ax5.axhline(avr, xmin=x1,xmax=x2, color='red', linestyle='--')

            j+=1


        plt.tight_layout()

#-------------------
def build_model(X,Y):
    time_dim=X.shape[1]        
    out_dim=Y.shape[1]        
    dense_dim = 4
    dense_layer=2
    dropFrac=args.dropFrac
    
    hiddenAct='sigmoid'
    #hiddenAct='relu'
    hiddenAct='selu'
    hiddenAct='tanh'

    lastAct='tanh'
    lastAct='linear'

    # - - - Assembling model 
    x = Input(shape=(time_dim,),name='inp_X')    
    print('build_model_fc inpX:',x.get_shape(),' dense_layer=',dense_layer,' hiddenAct=',hiddenAct,' lastAct=',lastAct)
        
    h=x            

    for i in range(dense_layer):
        h = Dense(dense_dim,kernel_constraint=maxnorm(3), activation=hiddenAct,name='enc%d'%i)(h) 
        #h = LeakyReLU(name='act%d'%i)(h)
        h = Dropout(dropFrac,name='drop%d'%i)(h) 
            
    y= Dense(out_dim, activation=lastAct,name='out_%d'%out_dim)(h) 
    # activation='linear'
    
    print('build_model_fc:',y.get_shape())
    # full model
    model = Model(inputs=x, outputs=y)
    
    model.compile(optimizer='adam', loss='mse')
    model.summary()  # will print

    return model
  
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()


if args.noXterm:
    print('diasable Xterm')
    import matplotlib as mpl
    mpl.use('Agg')  # to plot w/ X-server
import matplotlib.pyplot as plt
from matplotlib import cm as cmap

nTrain=args.events
nVal=int(nTrain/8)
tBins=10
U=np.random.uniform(-0.5,0.5,size=(nTrain,1))
U_val=np.random.uniform(-0.5,0.5,size=(nVal,1))

def func1(u): # vecotrized
    return 1.3*u +0.3  + np.random.normal(0,0.05,size=(u.shape[0],tBins))


X=func1(U)
X_val=func1(U_val)

print('Us',U[:10])
#print('Xs',X[:10])
print('shapes X:',X.shape,' Y:',U.shape)

# add offset to param
#U+=0.5; U_val+=0.5
# enlarge params
#U*=1.2; U_val*=1.2

plot_data_vsTime(X,U,range(6),figId=1)

model=build_model(X,U)

hirD={'loss': [],'val_loss': []}
startTm = time.time()
for k in range(2):
    # . . . . . .   TRAINING  . . . . 
    hir=model.fit(X,U, validation_data=(X_val,U_val),  shuffle=True,
                  batch_size=args.batch_size , epochs=args.epochs, verbose=1)

    hir=hir.history # allows appending history 
    for obs in hir:
        rec=[ float(x) for x in hir[obs] ]
        hirD[obs].extend(rec)

    # . . . . . . . . Compute predictions  . . . . 
    Z_val= model.predict(X_val)
    plot_param_residua(U_val,Z_val,figId=(k+1)*args.epochs)

fitTime=time.time() - start
loss=hirD['val_loss'][-1]
print('\n End Validation  Loss:%.3f'%loss,', fit time=%.1f sec'%(fitTime))

plot_fitHir(hirD,figId=2)



plt.show()

