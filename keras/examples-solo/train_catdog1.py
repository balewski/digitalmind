#!/usr/bin/env python

""" 
This examples is build based on Kagle competition
"Classifying pictures of cats and dogs with Keras"
https://gggdomi.github.io/keras-workshop/notebook.html

The data are need to be downloaded from
https://www.kaggle.com/c/dogs-vs-cats/data

read cat/dog data from Kagle competition
train net
write net + weights as HD5

Best result:  drop=0.5, epoch=15
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Classifying pictures of cats and dogs , demo',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--project",
                        default='catdog1',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-e", "--epochs", type=int, default=4,
                        help="fitting epoch")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=16,
                        help="batch_size for training")
    parser.add_argument("--dropFrac", type=float, default=0.5,
                        help="drop fraction at all layers")
    parser.add_argument( "--no-Xterm", dest='noXterm', action='store_true',
                         default=False,help="disable X-term for batch mode")
    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

# - - - - - - - - -   actual  code - - - - - - - - - 
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np

from keras.preprocessing.image import ImageDataGenerator, load_img

from keras.layers import Input, Dense,Conv2D, MaxPooling2D, Flatten, Dropout
from keras.models import Model
from keras.utils import plot_model

print('libs imported elaT=%.1f sec'%(time.time() - start))


#-------------------
def plot_input(path):
    ''' plain image reader
img = load_img('data/train/cats/cat.0.jpg')  # this is a PIL image
x = img_to_array(img)  # this is a Numpy array with shape (3, 150, 150)

# the .flow() command below generates batches of randomly transformed images
# and saves the results to the `preview/` directory
i = 0
for batch in datagen.flow(x, batch_size=1,
                          save_to_dir='preview', save_prefix='cat', save_format='jpeg'):
    i += 1
    if i > 20:
        break  # otherwise the generator would loop indefinitely
    '''
    n=8
    plt.figure(facecolor='white', figsize=(10,4))
    for i in range(n):
        plt.subplot(2,4,i+1)
        name=path+'/cats/cat.%d.jpg'%(1002+i)
        if i>3 :
            name=path+'/dogs/dog.%d.jpg'%(1000+i)
        img = load_img(name)
        plt.imshow(img)
    # plt.show()     # show the plot later


#----------------------------------
def plot_fitHir(DL,tit='fit summary'):
    #print('DL',DL)
    # Two subplots, unpack the axes array immediately
    f, (ax2, ax1) = plt.subplots(2,  sharex=True,
                                 facecolor='white', figsize=(6,4))

    ax1.set(xlabel='epochs',ylabel='loss')
    ax1.plot(DL['loss'],'.--',label='train')
    ax1.plot(DL['val_loss'],'.-',label='valid')
    ax1.legend(loc='best')
    #ax1.set_yscale('log')
    ax1.grid(color='brown', linestyle='--',which='both')

    ax2.set(ylabel='accuracy',title=tit)
    ax2.plot(DL['acc'],'.-',label='train')
    ax2.plot(DL['val_acc'],'.-',label='valid')
    ax2.legend(loc='upper left')
    ax2.grid(color='brown', linestyle='--',which='both')
 
    f.subplots_adjust(hspace=0.05)
    figName=args.prjName+'_fit'
    plt.savefig(figName+'.pdf')


#-------------------
def build_model():
    ''' based on:
    https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html
    and:  https://keras.io/getting-started/functional-api-guide/
    '''
    zipDim=32
    input_img = Input(shape=(img_width,img_height,3),name='inp_img')
    
    net=Conv2D(zipDim, (3, 3), strides=2, padding='same', activation='relu')(input_img)
    net=Conv2D(zipDim, (3, 3), strides=2, padding='same', activation='relu')(net)
    net=Conv2D(2*zipDim, (3, 3), strides=2, padding='same', activation='relu')(net)

    # the model so far outputs 3D feature maps (height, width, features), now we converge to single discriminator

    net=Flatten()(net)  # this converts our 3D feature maps to 1D feature vectors
    net=Dense(2*zipDim, activation='relu')(net)
    net=Dropout(args.dropFrac)(net)
    net=Dense(1, activation='sigmoid')(net)

    model = Model(input_img, net)
    print('\nnet assembled as Model:',model.count_params())
    model.summary() # will print

    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop', 
#                  optimizer='adam', # worse
                  metrics=['accuracy'])

    return model

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

if args.noXterm:
    print('diasable Xterm')
    import matplotlib as mpl
    mpl.use('Agg')  # to plot w/ X-server
import matplotlib.pyplot as plt

# CPUs are used via a "device" which is just a threadpool
if args.nCpu>0:
    import tensorflow as tf
    tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
    print('M: restrict CPU count to ',args.nCpu)

# dimensions of our images.
img_width=150 ; img_height = 150
img_path='/global/homes/b/balewski/prj/catdog-data/'  # 10k images

img_dims=(img_width,img_height)
train_data_dir = img_path+'train'
validation_data_dir = img_path+'validation'
print('localize image data, dims:',img_dims,' path=',img_path)

plot_input(train_data_dir)

#retrieve images and their classes for train and validation sets
    
datagen = ImageDataGenerator(rescale=1./255) # add more image ops here

train_generator = datagen.flow_from_directory(
    train_data_dir, batch_size=args.batch_size,
    target_size=img_dims,class_mode='binary')

validation_generator = datagen.flow_from_directory(
    validation_data_dir, batch_size=args.batch_size,
    target_size=img_dims,class_mode='binary')

model=build_model()

plot_model(model, to_file=args.prjName+'.graph.svg', show_shapes=True, show_layer_names=True)

print('star training for epochs=',args.epochs,' ...')
startTm = time.time()
hir=model.fit_generator(
        train_generator,verbose=args.verb,
        steps_per_epoch=800,
        epochs=args.epochs,
        validation_data=validation_generator,
        validation_steps=200)
outF=args.prjName+"_e%d.model.h5"%args.epochs
model.save_weights(outF)  # always save your weights after training or during training

fitTime=time.time() - start
plot_fitHir(hir.history,tit='catdog1, fit %.1f min, dropFrac=%.2f'%(fitTime/60.,args.dropFrac))

plt.show()

