#!/usr/bin/env python

# Run `pip install sigopt` to download the python API client
from sigopt import Connection
import sys,os,yaml
sys.path.append(os.path.abspath("/global/homes/b/balewski/janNersc/pdsfVaria/"))
from ShellCmdwTimeout import ShellCmdwTimeout
from Util_Func import read_yaml

if 0: # test execution of training         
        cmd="./train_CellHH.py -X -v0 -e2  "
        print("cmd=",cmd)
        res=ShellCmdwTimeout(cmd, timeout = 100)  # in seconds
        print('str.out=',res.stdout)
        print('str.err=',res.stderr)
        assert res.ret==0  # crash if no valid output
        okok1

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np


#............................
def my2dFunc(mid,x,y): 
    #print('mid=%d hyper values'%mid,x,y)
    maxSec=900; outPath='out2/'
    # note - epoch are ignored, now run for fixed run time
    sumF='step%d.yml'%mid
    prjN='step%d'%mid
    cmd="./train_CellHH.py -X -v0 --summary %s  --hyper %d %d --project %s --outPath %s"%(sumF,x,y,prjN,outPath)
    print("exec cmd=",cmd)
    res=ShellCmdwTimeout(cmd, timeout = maxSec) 
    #print('str.out=',res.stdout)
    #print('str.err=',res.stderr)
    assert res.ret==0  # makse no sense to proceed w/o node list

    bulk=read_yaml(outPath+sumF)
    print('SO: got',bulk)
    val=-bulk['train_loss']
    return val

# unit test:
#mid=4;x=12; y=13 ; Z=my2dFunc(mid,x,y)


#............................
#............................
#............................

fig = plt.figure()
ax = fig.gca(projection='3d')


#=================================
#=================================
#  M A I N 
#=================================
#=================================

conn = Connection(client_token="QPKFASCNKKJNMWWTBIJMJYLCTRJLBMDSJSULSLQOJRKDUFNG")

experiment = conn.experiments().create(
    name='Gauss2D Optimization (Python)',
    parameters=[
            dict(name='x', type='int', bounds=dict(min=2, max=7)),
            dict(name='y', type='int', bounds=dict(min=1, max=4)),
    ],
)
print("Created experiment: https://sigopt.com/experiment/" + experiment.id)

# Evaluate your model with the suggested parameter assignments

def evaluate_model(mid,assignments):    
    return my2dFunc(mid,assignments['x'], assignments['y'])

px=[];py=[];pz=[]
# Run the Optimization Loop between 10x - 20x the number of parameters
c1=-1e100
for j in range(6):
    suggestion = conn.experiments(experiment.id).suggestions().create()
    value = evaluate_model(j,suggestion.assignments)
    conn.experiments(experiment.id).observations().create(
        suggestion=suggestion.id,
        value=value,        
    )
    mm=''
    if c1<value : c1=value; mm='*'
    print('done j',j,value,mm) #,suggestion["assignments"]

    pp=suggestion.assignments
    px.append(pp['x'])
    py.append(pp['y'])
    pz.append(value)

print('end dump trajectory')
c1=pz[0]; j=0   
for a,b,c, in zip (px,py,pz):
    mm=''
    if c1<c : c1=c; mm='*'
    print(j,'x,y: %d, %d --> func=%.3g %s'%(a,b,c,mm))
    j+=1

ax.plot(px, py, pz, label='search trajectory')
ax.set(xlabel='nCNN',ylabel='nFC',zlabel='val acc')
ax.legend()
plt.savefig('SO.pdf')
plt.show()
