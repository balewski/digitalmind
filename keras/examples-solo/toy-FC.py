#!/usr/bin/env python

'''
 example of simple FC 
 linear decay of learning rate
 trains FC
saves model
reads model
predicts
can use generator for input

See examples of data generators:
https://github.com/keras-team/keras/issues/7729

See example for linear decay learnibng rate
https://www.pyimagesearch.com/2019/07/22/keras-learning-rate-schedules-and-decay

Run on Cori CPU:
export HDF5_USE_FILE_LOCKING=FALSE

module load tensorflow/gpu-1.14.0-py37


'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np

import tensorflow as tf
from tensorflow.contrib.keras.api.keras.layers import Dense, Input
from tensorflow.contrib.keras.api.keras.models import Model, load_model

# for LR schedule:
from tensorflow.keras.callbacks import LearningRateScheduler
from tensorflow.keras.callbacks import  Callback
from tensorflow.keras import backend as K

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

#............................
class PolynomialDecayJan():
    def __init__(self, maxEpochs=100, initAlpha=0.01, power=1.0):
        # store the maximum number of epochs, base learning rate,
        # and power of the polynomial
        self.maxEpochs = maxEpochs
        self.initAlpha = initAlpha
        self.power = power
    def __call__(self, epoch):
        # compute the new learning rate based on polynomial decay
        decay = (1 - (epoch / float(self.maxEpochs))) ** self.power
        decay=max(1e-8, decay) # to protect against negative decay
        alpha = self.initAlpha * decay
        return float(alpha) # return the new learning rate

#............................
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[] 
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr)
        print('\n*****MYL: lr',lr)
        self.hir.append(float(lr))

#-------------------
def build_model(inp_dim,out_dim,fc_dim):
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' outs:',out_dim)
    h = Dense(fc_dim, activation='relu',name='calcium')(x_img)
   
    h=Dense(fc_dim, activation='relu',name='sodium')(h)
    ho=Dense(out_dim, activation='tanh',name='potas')(h)


    print('build_model w/ outputs :',ho.get_shape())
    model=Model(inputs=x_img, outputs=ho)
    
    # We compile the model and assign a weight of 0.2 to the 2nd loss=mae.
    model.compile(optimizer='adam', loss='mse')
    model.summary() # will print

    print('\nBuild multi-out model, layers=%d , params=%.1f K'%(len(model.layers),model.count_params()/1000.))

    return model


# - - - - - - - -  
def my_generator(X_data, y_data, batch_size):

  samples_per_epoch = X_data.shape[0]
  number_of_batches = samples_per_epoch/batch_size
  counter=0
  print('GEN:',X_data.shape, y_data.shape)

  while 1:
    X_batch = np.array(X_data[batch_size*counter:batch_size*(counter+1)]).astype('float32')
    y_batch = np.array(y_data[batch_size*counter:batch_size*(counter+1)]).astype('float32')
    counter += 1
    #print('myGen cnt=',counter)
    yield X_batch,y_batch

    #restart counter to yeild data in the next epoch as well
    if counter >= number_of_batches:
        counter = 0


#=================================
#=================================
#  M A I N 
#=================================
#=================================
mFact=1  # model size expansion factor
nFact=1 # number of events expansion factor

inp_dim=2000*mFact
fc_dim=15*mFact*mFact
out_dim=3*mFact

epochs=10*2
batch_size=32
steps=150*nFact
num_eve=steps*batch_size

model=build_model(inp_dim,out_dim,fc_dim)

if 'cori99'  in socket.gethostname():
    # this software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outF='simpleFC.graph.svg'
    plot_model(model, to_file=outF, show_shapes=True, show_layer_names=True)  # owrks only on Cori
    print('saved ',outF)

print('\nM: generate data and train, num_eve=',num_eve,', mFact=%d, nFact=%d'%(mFact,nFact))

startT = time.time()

Y=np.random.uniform(-0.2,0.8, size=(num_eve,out_dim))
print('M: done Y shape',Y.shape,)
#X=np.random.normal(loc=u1, scale=u2, size=(num_eve,inp_dim))
X=np.random.uniform(-0.8,0.8, size=(num_eve,inp_dim))
print('M: done X shape',X.shape,' elaT=%.1f sec,'%(time.time() - startT))

# inject Y to X so training can converge
X[:,:out_dim]=Y*0.3+np.random.normal(loc=0, scale=0.001, size=(num_eve,out_dim))

print("M: using 'linear' learning rate decay...")
lr_sched = PolynomialDecayJan(maxEpochs=epochs, initAlpha=1e-3, power=1)
myCallbacks=[LearningRateScheduler(lr_sched),MyLearningTracker()]

# input as np.arrrays
#model.fit(X,Y, batch_size=batch_size, nb_epoch=epochs,verbose=1) 
# OR input from generator
model.fit_generator(my_generator(X,Y,batch_size), epochs=epochs,steps_per_epoch = steps,verbose=1, callbacks=myCallbacks)
print("-----------  training completed -----------")


print("-----------  Save model as hd5 -----------")
outF='jan.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from YAML -----------")
model5=load_model(outF)


print('M: fit done, predicting')
Z=model5.predict(X)
print('M: done, Z:',Z.shape)

import matplotlib.pyplot as plt
plt.figure()
jN=1
plt.scatter(Z[:,jN], Y[:,jN], alpha=0.3)
#plt.plot(range(-1, 1), range(-1, 1))
plt.xlabel('Predicted jN=%d'%jN)
plt.ylabel('Actual')
plt.savefig('pred_v_true.png')
plt.show()


