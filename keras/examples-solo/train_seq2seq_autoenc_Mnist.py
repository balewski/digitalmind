#!/usr/bin/env python

""" 
generic Keras

Example:  MNIST data set w/ Autoencoder and Keras API
use LSTMS so it is sequence to sequence (seq2seq)
Based on https://blog.keras.io/building-autoencoders-in-keras.html

"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Recognize MNIST  using Autoencoder',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--project",
                        default='seq2seq_AE',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-e", "--epochs", type=int, default=10,
                        help="fitting epoch")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=200,
                        help="batch_size for training")

    parser.add_argument("-z", "--zipDim", type=int, default=32, choices=[ 24, 32,48],
                        help="size of our encoded representations")
    parser.add_argument("-n", "--events", type=int, default=0,
                        help="events for training, use 0 for all")
    parser.add_argument("--noise", type=float, default=0.2,
                        help="noise added [0,1]")

    parser.add_argument( "--no-Xterm", dest='noXterm', action='store_true',
                         default=False,help="disable X-term for batch mode")
    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

# - - - - - - - - -   actual  code - - - - - - - - - 
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np
from keras.datasets import mnist
from keras.layers import Input, Dense, LSTM, RepeatVector
from keras.models import Model
from keras.utils import plot_model
print('libs imported elaT=%.1f sec'%(time.time() - start))


#-------------------
def plot_input(X):
    for i in range(4):
        plt.subplot(2,2,i+1)
        plt.imshow(X[i], cmap=plt.get_cmap('gray'))
    # show the plot


#----------------------------------
def plot_fitHir(DL,tit='fit summary'):
    #print('DL',DL)
    # Two subplots, unpack the axes array immediately
    f, (ax1, ax2) = plt.subplots(2,  sharex=True,
                                 facecolor='white', figsize=(8,6))

    ax1.set(ylabel='loss',title=tit)
    ax1.plot(DL['loss'],'.-',label='train')
    ax1.plot(DL['val_loss'],'.-',label='valid')
    ax1.legend(loc='best')
    #ax1.set_yscale('log')
    ax1.grid(color='brown', linestyle='--',which='both')

    ax2.set(xlabel='epochs',ylabel='accuracy')
    ax2.plot(DL['acc'],'.-',label='train')
    ax2.plot(DL['val_acc'],'.-',label='valid')
    ax2.legend(loc='upper left')
    ax2.grid(color='brown', linestyle='--',which='both')
 
    f.subplots_adjust(hspace=0.05)
    figName=args.prjName+'_fit'
    plt.savefig(figName+'.pdf')


#----------------------------------
def plot_comparison(Xinp,Xzip,Xout,n):
    kz=int(args.zipDim/4)
    plt.figure(figsize=(13, 4.5))
    
    for i in range(n):
        # display original
        ax = plt.subplot(3, n, i + 1)
        plt.imshow(Xinp[i].reshape(28, 28))
        plt.gray()
        if i==0 :plt.title('zipDim %d'%args.zipDim)
        if i==1 :plt.title('epochs %d'%args.epochs)
        if i==2 :plt.title('seq2seq')
        
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)


        # display reconstruction
        ax = plt.subplot(3, n, i + 1 + n)
        plt.imshow(Xout[i].reshape(28, 28))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # display zips
        ax = plt.subplot(3, n, i + 1 + n*2)
        plt.imshow(Xzip[i].reshape(4, kz).T)
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)


#-------------------
def add_noise(xa,noise_factor = 0.3):
    print('add noise %.2f to '%args.noise,xa.shape,' ... ')
    xa = xa + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=xa.shape) 
    xa = np.clip(xa, 0., 1.)
    return xa
#-------------------
def build_model():
    ''' based on  https://blog.keras.io/building-autoencoders-in-keras.html
    '''
    #Typically happens is that the hidden layer is learning an approximation of PCA (principal component analysis).
    #from keras import regularizers
    #actReg=regularizers.l1(1.e-7)
    #actReg=None
    #,activity_regularizer=actReg
    
    dropFrac=0.1
    recDropFrac=dropFrac/2.

    input_shape=(28,28)
    time_dim=input_shape[0]
    feature_dim=input_shape[1]
    lstm_dim=50
    zip_dim = args.zipDim
    x = Input(shape=input_shape, name='inp28x28')
    print('build_model inp:',x.get_shape())

    h=  LSTM(lstm_dim,dropout=dropFrac, activation='relu',recurrent_dropout=recDropFrac,name='r1_enc_%d'%lstm_dim) (x)

    encoded = Dense(zip_dim, activation='relu',name='zip_data')(h)
    
    # "decoded" is the lossy reconstruction of the input
    h2= Dense(lstm_dim, activation='relu',name='zip2img')(encoded)
    h2=RepeatVector(time_dim,name='times_%d'%time_dim)(h2)

    decoded= LSTM(feature_dim,dropout=dropFrac, activation='relu',return_sequences=True,recurrent_dropout=recDropFrac,name='r2_dec_%d'%feature_dim) (h2)
 
    #1) this model maps an input to its reconstruction
    autoencoderM = Model(x, decoded)

    #2) this model maps an input to its encoded representation
    encoderM = Model(x, encoded)

    # create a placeholder for an encoded (zip-dimensional) input
    encoded_input = Input(shape=(zip_dim,),name='zip_input')
 
    # retrieve last ? layers of the autoencoder model
    net = autoencoderM.layers[-3](encoded_input)
    net = autoencoderM.layers[-2](net)
    net = autoencoderM.layers[-1](net)
    
    #3) this model maps encoded representation to input image
    decoderM = Model(encoded_input, net)
    #??? 'binary_crossentropy' works much worse vs. 'mse' - no clue why, may be wrong activation is used on the last LSTM layer?

    #myLoss='mean_absolute_error'
    myLoss='mean_squared_error'
    
    autoencoderM.compile(optimizer='adadelta', loss=myLoss, metrics=['accuracy'])

    
    autoencoderM.summary() # will print

    print('\nFull  seq2seq auto-encoder loss=%s layers=%d , params=%.1f K'%(myLoss,len(autoencoderM.layers),autoencoderM.count_params()/1000.))

    print('\nEncoder layers=%d , params=%.1f K'%(len(encoderM.layers),encoderM.count_params()/1000.))
    if args.verb>1:
       encoderM.summary() # will print
 
    print('\nDecoder layers=%d , params=%.1f K'%(len(decoderM.layers),decoderM.count_params()/1000.))
    if args.verb>1:
        decoderM.summary() # will print


    return autoencoderM,encoderM,decoderM

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

if args.noXterm:
    print('diasable Xterm')
    import matplotlib as mpl
    mpl.use('Agg')  # to plot w/ X-server
import matplotlib.pyplot as plt

# CPUs are used via a "device" which is just a threadpool
if args.nCpu>0:
    import tensorflow as tf
    tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
    print('M: restrict CPU count to ',args.nCpu)

# this is the size of our encoded representations

# The data, shuffled and split between train and test sets.
(X, _), (X_test, _) = mnist.load_data()

# normalize amplitudes
X = X.astype('float32') / 255.
X_test = X_test.astype('float32') / 255.

if  args.events>0 :
    neve=args.events
    if neve < X.shape[0]:  
        X=X[:neve]
        print('shrink input to ',neve)
if args.noise >0:
    X=add_noise(X,args.noise)
    X_test=add_noise(X_test,args.noise)

#plot_input(X) # plot 4 images as gray scale

#X1d = X.reshape((len(X), np.prod(X.shape[1:])))
#X1d_test = X_test.reshape((len(X_test), np.prod(X_test.shape[1:])))

#
print('shapes X:',X.shape)
autoM,encoM,decoM=build_model()

plot_model(autoM, to_file=args.prjName+'.graph.svg', show_shapes=True, show_layer_names=True)

print('star training for epochs=',args.epochs,' ...')
startTm = time.time()
# note, for autoencoder  output == input
hir=autoM.fit(X,X,shuffle=True, verbose=args.verb,
          validation_data=(X_test, X_test),
          batch_size=args.batch_size, nb_epoch=args.epochs )
fitTime=time.time() - start

tit='mnist-autoenc, fit %.1f min, zip_dim=%d, encoder:%.1fK params, noise=%.2f'%(fitTime/60.,args.zipDim,encoM.count_params()/1000.,args.noise)

if args.epochs>0: plot_fitHir(hir.history,tit=tit)

''' Visualize the reconstructed inputs and the encoded representations. 
encode and decode some digits
note that we take them from the *test* set
'''
encoded_imgs = encoM.predict(X_test)
decoded_imgs = decoM.predict(encoded_imgs)

zip_mean=encoded_imgs.mean()
zip_stddev=encoded_imgs.std()
print('encoder : mean=%.3f  stddev=%.3f'%(zip_mean,zip_stddev))

#encoder : mean=6.200  stddev=7.129

n = 12  # how many digits we will display
plot_comparison(X_test,encoded_imgs,decoded_imgs,n)

plt.tight_layout()
plt.show()

