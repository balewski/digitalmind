#!/usr/bin/env python

'''
 example of simple FC 

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np

import tensorflow as tf
from tensorflow.contrib.keras.api.keras.layers import Dense, Input
from tensorflow.contrib.keras.api.keras.models import Model, load_model

# for LR schedule:
from tensorflow.keras.callbacks import LearningRateScheduler
from tensorflow.keras.callbacks import  Callback
from tensorflow.keras import backend as K

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))


#-------------------
def build_model(inp_dim,out_dim,fc_dim):
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' outs:',out_dim)
    h = Dense(fc_dim, activation='relu',name='calcium')(x_img)
   
    h=Dense(fc_dim, activation='relu',name='sodium')(h)
    ho=Dense(out_dim, activation='tanh',name='potas')(h)


    print('build_model w/ outputs :',ho.get_shape())
    model=Model(inputs=x_img, outputs=ho)
    
    # We compile the model and assign a weight of 0.2 to the 2nd loss=mae.
    model.compile(optimizer='adam', loss='mse')
    model.summary() # will print

    print('\nBuild multi-out model, layers=%d , params=%.1f K'%(len(model.layers),model.count_params()/1000.))

    return model


#=================================
#=================================
#  M A I N 
#=================================
#=================================
mFact=1  # model size expansion factor
nFact=1 # number of events expansion factor

inp_dim=2000*mFact
fc_dim=15*mFact*mFact
out_dim=3*mFact

epochs=10*2
batch_size=32
steps=150*nFact
num_eve=steps*batch_size

model=build_model(inp_dim,out_dim,fc_dim)

print('\nM: generate data and train, num_eve=',num_eve,', mFact=%d, nFact=%d'%(mFact,nFact))

startT = time.time()

Y=np.random.uniform(-0.2,0.8, size=(num_eve,out_dim))
print('M: done Y shape',Y.shape,)

X=np.random.uniform(-0.8,0.8, size=(num_eve,inp_dim))
print('M: done X shape',X.shape,' elaT=%.1f sec,'%(time.time() - startT))

# inject Y to X so training can converge
X[:,:out_dim]=Y*0.3+np.random.normal(loc=0, scale=0.001, size=(num_eve,out_dim))


# input as np.arrrays
model.fit(X,Y, batch_size=batch_size, nb_epoch=epochs,verbose=1) 
