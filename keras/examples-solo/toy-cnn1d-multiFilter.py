#!/usr/bin/env python

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np
from keras.layers import Input, Dense, UpSampling1D, Reshape,Conv1D,Flatten,MaxPool1D,concatenate
from keras.layers.advanced_activations import LeakyReLU 
from keras.models import Model

from keras.constraints import max_norm

from keras.layers import Activation
from keras import backend as K
from keras.utils.generic_utils import get_custom_objects
from keras.layers.advanced_activations import LeakyReLU # breaks model_save unless it is a separate layer

print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

#-------------------
def build_model(time_dim,out_dim ): # this has big input

    filterA = [2, 4]
    kernelA = [3, 7]
    strides=2

    assert len(filterA)==len(kernelA)
    numFan=len(filterA)

    input_shape=(time_dim,)
    x_input = Input(shape=input_shape, name='inp_x')
    print('build_model X_inp:',x_input.get_shape())
    x_3d = Reshape((-1, 1), name='pass0') (x_input) # because Conv1D wants rank 3 tensor 
    print('resh0=',x_3d.get_shape())    

    fan_outL = []
    for i in range(numFan):
        pool_len=1+kernelA[i]
        # Conv1D(filters, kernel_size,strides=strides,
        conv_lr = Conv1D(filterA[i],kernelA[i],strides=strides, border_mode='same', W_constraint=max_norm(3),name='fan%d_f%d_k%d'%(i,filterA[i],kernelA[i]))(x_3d)
        
        act_lr =LeakyReLU(name='act%d'%(i))(conv_lr)
        pool_lr = MaxPool1D(pool_length=pool_len)(act_lr)
        fan_outL.append( Flatten()(pool_lr))
                         
    ''' What does a weight constraint of max_norm do?
    maxnorm(m) will, if the L2-Norm of your weights exceeds m, 
    scale your whole weight matrix by a factor that reduces the norm to m.
    '''

    merged_conv_outputs =  concatenate(fan_outL, name="concat_%d"%len(filterA))
    print('merg=',merged_conv_outputs.get_shape())    

    softmax = Dense(out_dim, activation="softmax")(merged_conv_outputs)
    model = Model(input=x_input, output=softmax)
    myLoss='mean_squared_error'
    model.compile(loss = myLoss,optimizer = 'adam')

    model.summary() # will print

    print('\nFull  loss=%s layers=%d , params=%.1f K'%(myLoss,len(model.layers),model.count_params()/1000.))

    return model

#=================================
#=================================
#  M A I N 
#=================================
#=================================
time_dim=50
out_dim=5
model=build_model(time_dim,out_dim)

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outG='cnn1d-AE.graph.svg'
    plot_model(model, to_file=outG, show_shapes=True, show_layer_names=True)  # works only on Cori
    print('saved model graph as ',outG)


outM='cnn1d-AE.model.h5'
model.save(outM)
print('saved h5 model as ',outM)

print('\ngenerate data and train')
num_eve=1000
X=np.random.normal( size=(num_eve,time_dim))
Y=np.random.normal( size=(num_eve,out_dim))
model.fit(X,Y, batch_size=100, nb_epoch=2,verbose=1)
print('fit done, predicting')
Xnew=model.predict(X)
print('done, Xnew:',Xnew.shape)

from keras.models import load_model
model2=load_model(outM)
print('load h5 model h5 from ',outM)
