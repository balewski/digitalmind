#!/usr/bin/env python3
import socket  # for hostname
import horovod.tensorflow.keras as hvd 

hvd.init()
numRanks= hvd.size()
myRank= hvd.rank()

print(' Horovod: initialize  myRank=%d of %d on %s '%(myRank,numRanks,socket.gethostname()))
