#!/usr/bin/env python

""" 
No graphics, no matplitlib

This examples is build based on Kagle competition
"Classifying pictures of cats and dogs with Keras"
https://gggdomi.github.io/keras-workshop/notebook.html

The data are need to be downloaded from
https://www.kaggle.com/c/dogs-vs-cats/data

read cat/dog data from Kagle competition
train net
write net + weights as HD5

Best result:  drop=0.5, epoch=15
"""
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import argparse
def get_parser():
    parser = argparse.ArgumentParser(
        description='Classifying pictures of cats and dogs , demo',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--project",
                        default='catdog1',dest='prjName',
                        help="core name used to store outputs")

    parser.add_argument("-e", "--epochs", type=int, default=4,
                        help="fitting epoch")
    parser.add_argument("--nCpu", type=int, default=0,
                        help="num CPUs used when fitting, use 0 for all resources")
    parser.add_argument("-b", "--batch_size", type=int, default=16,
                        help="batch_size for training")
    parser.add_argument("--dropFrac", type=float, default=0.5,
                        help="drop fraction at all layers")
    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

# - - - - - - - - -   actual  code - - - - - - - - - 
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np

from tensorflow.contrib.keras.api.keras.preprocessing.image import ImageDataGenerator, load_img

from tensorflow.contrib.keras.api.keras.layers import Input, Dense,Conv2D, MaxPooling2D, Flatten, Dropout
from tensorflow.contrib.keras.api.keras.models import Model
from tensorflow.contrib.keras.api.keras.utils import plot_model

print('libs imported elaT=%.1f sec'%(time.time() - start))


#-------------------
def build_model():
    ''' based on:
    https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html
    and:  https://keras.io/getting-started/functional-api-guide/
    '''
    zipDim=32
    input_img = Input(shape=(img_width,img_height,3),name='inp_img')
    
    net=Conv2D(zipDim, (3, 3), strides=2, padding='same', activation='relu')(input_img)
    net=Conv2D(zipDim, (3, 3), strides=2, padding='same', activation='relu')(net)
    net=Conv2D(2*zipDim, (3, 3), strides=2, padding='same', activation='relu')(net)

    # the model so far outputs 3D feature maps (height, width, features), now we converge to single discriminator

    net=Flatten()(net)  # this converts our 3D feature maps to 1D feature vectors
    net=Dense(2*zipDim, activation='relu')(net)
    net=Dropout(args.dropFrac)(net)
    net=Dense(1, activation='sigmoid')(net)

    model = Model(input_img, net)
    print('\nnet assembled as Model:',model.count_params())
    model.summary() # will print

    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop', 
#                  optimizer='adam', # worse
                  metrics=['accuracy'])

    return model

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

# CPUs are used via a "device" which is just a threadpool
if args.nCpu>0:
    import tensorflow as tf
    tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=args.nCpu))
    print('M: restrict CPU count to ',args.nCpu)

# dimensions of our images.
img_width=150 ; img_height = 150
img_path='/global/homes/b/balewski/prj/catdog-data/'  # 10k images

img_dims=(img_width,img_height)
train_data_dir = img_path+'train'
validation_data_dir = img_path+'validation'
print('localize image data, dims:',img_dims,' path=',img_path)

#retrieve images and their classes for train and validation sets
    
datagen = ImageDataGenerator(rescale=1./255) # add more image ops here

train_generator = datagen.flow_from_directory(
    train_data_dir, batch_size=args.batch_size,
    target_size=img_dims,class_mode='binary')

validation_generator = datagen.flow_from_directory(
    validation_data_dir, batch_size=args.batch_size,
    target_size=img_dims,class_mode='binary')

model=build_model()

#1 plot_model(model, to_file=args.prjName+'.graph.svg', show_shapes=True, show_layer_names=True)

print('star training for epochs=',args.epochs,' ...')
startTm = time.time()
hir=model.fit_generator(
        train_generator,verbose=args.verb,
        steps_per_epoch=800,
        epochs=args.epochs,
        validation_data=validation_generator,
        validation_steps=200)
outF=args.prjName+"_e%d.model.h5"%args.epochs
model.save_weights(outF)  # always save your weights after training or during training

fitTime=time.time() - start
print('fit time=',fitTime)
