#!/usr/bin/env python

# example of muti-output regression
# based on https://keras.io/getting-started/functional-api-guide/#multi-input-and-multi-output-models

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np
from keras import backend as K
from keras import losses 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout
from keras.models import Model
print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))



#-------------------
def build_model(inp_dim,out1_dim,out2_dim):
    fc_dim=15
       
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' out1',out1_dim , ' out2',out2_dim)
    hc = Dense(fc_dim, activation='relu',name='enc1')(x_img)
   
    # ......  1st output branchout
    h1=Dense(fc_dim,name='branch1')(hc)
    h1=Dense(out1_dim,name='out1')(h1)

    # ......  2nd output branchout
    h2=Dense(out2_dim,name='out2')(hc)
    
    print('build_model w/ 2 outputs h1:',h1.get_shape(),' h2:',h2.get_shape())
    model=Model(inputs=x_img, outputs=[h1,h2])
    
    # We compile the model and assign a weight of 0.2 to the 2nd loss=mae.
    model.compile(optimizer='adam', 
                  loss={'out1':'mse','out2':'mae'}, 
                  loss_weights={'out1':1,'out2':0.2} )
    model.summary() # will print

    print('\nBuild multi-out model, layers=%d , params=%.1f K'%(len(model.layers),model.count_params()/1000.))


    return model

#=================================
#=================================
#  M A I N 
#=================================
#=================================
inp_dim=50
out1_dim=2; out2_dim=1
model=build_model(inp_dim,out1_dim,out2_dim)

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outF='multi-out.graph.svg'
    plot_model(model, to_file=outF, show_shapes=True, show_layer_names=True)  # owrks only on Cori
    print('saved ',outF)

print('\nM: generate data and train')
num_eve=1000
u1=0.2; u2=7.0; u3=12
Y1=np.array([u1,u2]*num_eve).reshape(-1,out1_dim)
Y2=np.array([u3,]*num_eve).reshape(-1,out2_dim)
print('M: Y1 shape',Y1.shape, Y2.shape)
X=np.random.normal(loc=u1, scale=u2, size=(num_eve,inp_dim))
model.fit(X,[Y1,Y2], batch_size=100, nb_epoch=2,verbose=1)
print('M: fit done, predicting')
Z1,Z2=model.predict(X)
print('M: done, Zs:',Z1.shape,Z2.shape)


