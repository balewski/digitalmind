#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"
 
import os, time
import warnings
import socket  # for hostname

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

import yaml
from tensorflow.contrib.keras.api.keras.layers import Dense,  LSTM, Input, concatenate
from tensorflow.contrib.keras.api.keras.models import Model, load_model,  model_from_yaml

input1 = Input(shape=(10,11), name='inp1')
input2 = Input(shape=(20,22), name='inp2')
print('build_model inp1:',input1.get_shape(),' inp2:',input2.get_shape())
net1= LSTM(60) (input1)
net2= LSTM(40) (input2)
net = concatenate([net1,net2],name='concat-jan')
net=Dense(30, activation='relu')(net)
outputs=Dense(1, activation='sigmoid')(net) # predicts only 0/1 
model = Model(inputs=[input1,input2], outputs=outputs)
# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary() # will print


if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    plot_model(model, to_file='2LSTM-classif.graph.svg', show_shapes=True, show_layer_names=True)  # owrks only on Cori


print("-----------  Save model as YAML -----------")
yamlRec = model.to_yaml()
with open('jan.model.yml', 'w') as outfile:
    yaml.dump(yamlRec, outfile)


print("-----------  Read model from YAML -----------")
with open('jan.model.yml', 'r') as inpfile:
    yamlRec4=yaml.load(inpfile)
model4 = model_from_yaml(yamlRec4)
model4.summary() # will print


print("-----------  Save model as hd5 -----------")
import yaml
outF='jan.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from YAML -----------")
model5=load_model(outF)
