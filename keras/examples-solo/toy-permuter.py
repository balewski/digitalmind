#!/usr/bin/env python

'''
 example of using Permutational Layer
Modiffied from
https://github.com/off99555/superkeras/blob/master/permutational_layer.py
Oryginal by:  Chanchana Sornsoontorn (off99555)

An implementation of the Permutational Layers as described by the paper
"Permutation-equivariant neural networks applied to dynamics prediction"
Link to the paper: https://arxiv.org/pdf/1612.04530.pdf

Single permutation layer allows for 2-inputs correlation.

As you add more permutational layers, the rate of input relationship grows exponentially, e.g.: 2 perm-layer --> 4-inp-corr, 3 perm-layers --> 8-inp-corr, etc.

# Structure of the concepts from concrete to abstract
    properties -> object -> pairwise model -> permutational encoder -> permutational layer -> permutational module

# Concepts introduced in this code:
*) single input object represents 2D hard dics described by 4 variables: x,y,vx,vy
*) full input is a pair of discs : [ discA, discB] - but the order does not matter
*) pairwise-model:  input=2 objects, output=encoding

*) permutational encoder: The model that takes N objects, apply pairwise model N times, merge the encodings and return it.


 trains
saves model
reads model
predicts
Run on Cori CPU:

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time,sys
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np

import tensorflow as tf
from tensorflow.keras.layers import  Dense, Input, average
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.utils import plot_model

sys.path.append(os.path.abspath("../superkeras-chanchana/"))
from permutational_layer import PairwiseModel, PermutationalEncoder,PermutationalLayer, move_item
from layers import repeat_layers

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))


#...!...!..................
def build_1permu_model():
    print('\n step-A - - - - - - - -')
    print('M: 1 disc_shape=',one_shape,' num_disc=',num_inputs)
    pairwise_op = PairwiseModel(
        one_shape, repeat_layers(Dense, [32, 24,perm1_out], name="FCpair"), name="pairwise_op"
        )
    #j print("M: PairwiseModel summary") ; pairwise_model.summary()
    mod=pairwise_op
    print('M: PairOP  inp=',mod.input_shape,',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

    outF='pairwise_op.graph.svg'
    plot_model(pairwise_op, to_file=outF, show_shapes=True, show_layer_names=True)
    print('M: saved1 ',outF)


    print('\n step-B - - - - - - - ')
    encoderRow_op = PermutationalEncoder(pairwise_op,num_inputs , name="encoder_row",encode_identical=True)
    #j print("M: PermutationalEncoder summary"); perm_encoder.summary()
    mod=encoderRow_op
    print('M: EncoderRow  inp=',mod.input_shape,',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers))

    outF='encoderRow_op.graph.svg'
    plot_model(encoderRow_op, to_file=outF, show_shapes=True, show_layer_names=True)
    print('M:saved2 ',outF)


    print('\n step-C - - - - - - ')
    perm_layer = PermutationalLayer(encoderRow_op, name="permutational_layer", pooling=perm_pool)
    #j print("M: PermutationalLayer summary"); perm_layer.summary()

    mod=perm_layer
    print('M: perm_layer  inp=',mod.input_shape,',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers), 'pool=',type(perm_pool))
    outF='permLyr.graph.svg'
    plot_model(perm_layer, to_file=outF, show_shapes=True, show_layer_names=True)
    print('M:saved3 ',outF)

    return perm_layer

#...!...!..................
def build_3permu_model(perm_layer):
    a = Input(shape=one_shape, name="a")
    b = Input(shape=one_shape, name="b")
    c = Input(shape=one_shape, name="c")
    inputs = [a, b, c]

    outputs = perm_layer(inputs)
    #print('UU1',outputs)

    perm_layer2 = PermutationalLayer(
        PermutationalEncoder(
            PairwiseModel((perm1_out,), repeat_layers(Dense, [19, perm2_out], activation="relu")), num_inputs),  name="permu_layer2",
    )

    outputs = perm_layer2(outputs)
    #print('UU2',outputs)

    perm_layer3 = PermutationalLayer(
        PermutationalEncoder(
            PairwiseModel((perm2_out,), repeat_layers(Dense, [14, perm3_out], activation="tanh")), num_inputs), name="perm_layer3",
    )
    outputs = perm_layer3(outputs)
    #print('UU3',outputs)

    output = average(outputs)  # let's average the output for single tensor
    model = Model(inputs, output)
    print("# Multi-layer model summary")
    model.summary()
    mod=model
    print('M: perm3_layer  inp=',mod.input_shape,',out=',mod.output_shape,', params=',mod.count_params(),' layers=',len(mod.layers), 'pool=',type(perm_pool))
    outF='perm3Lyr.graph.svg'
    plot_model(mod, to_file=outF, show_shapes=True, show_layer_names=True)
    print('M:saved3 ',outF)

    return model


#...!...!..................





#=================================
#=================================
#  M A I N 
#=================================
#=================================
one_shape=(4,)
num_inputs=3  # how many discs to be permuted 
perm1_out=6  # how many features on output
perm_pool=None  # do you want many output tensors?
#perm_pool=average  # do you want 1 output tensor?

epochs=5
batch_size=16
steps=15
num_eve=steps*batch_size

# for multi-permuter model
perm2_out=7
perm3_out=5

model1= build_1permu_model()

if perm_pool==average:
    #''' fit-starts
    print('\nM:  - - - - - - - - generate data and train, num_eve=',num_eve)
    assert perm_pool==average  # want single vector output

    # Expects X as a list of num_inputs=3 arrays of shape=(num_eve,one_shape)
    X=[np.random.normal(loc=0.3, scale=0.7, size=(num_eve,one_shape[0])) for i in range(num_inputs) ]
    Y=np.random.uniform(size=(num_eve,perm1_out))
    print('M: Y shape',Y.shape, 'num_inputs=',num_inputs)

    # We compile the model , takes long time for large num_inputs
    model1.compile(optimizer='adam', loss='mse') 
    print('M: compiled')
    model1.fit(X,Y, batch_size=batch_size, nb_epoch=epochs,verbose=1) 

    # procure shorter version of inputX 
    X2=[ X[j][:5] for j in range(num_inputs) ]
    Y2=Y[:5]

    Z2=model1.predict(X2)
    print("M: OutputFit Features")
    for z,u in zip(Z2,Y2):    print('pred=',z,'truth=',u,'diff=',z-u)
    #''' # end-fit


if perm_pool!=None: exit(0)
# below this is demo for the oryginal code

print('\n step-D - - - - - - - ')

print('\nM:  model %s is defined \n'%(model1.name))
assert num_inputs==3
assert perm_pool==None
assert one_shape==(4,)
x5 = [[[1, 2, 3, 4]], [[4, 3, 2, 1]], [[1, 2, 3, 4]],[[5,6,7,8]],[[1,1,1,1]]]
x3 = [[[1, 2, 3, 4]], [[4, 3, 2, 1]], [[1, 2, 3, 4]]]
X=x3
predictions = model1.predict(X)
print("M: Input Data")
print(X)
print("M: Output Features")
for pred in predictions: print(pred)
print(
        "You will notice that output1 = output3 if input1 = input3. So the permutational layer is simply performing a function f(x) for each input x."
    )
print("It is similar to a shared model applied to each input.")
print(
        "The difference that makes this approach better is because f(x) is not standalone like shared model. It sees useful relationship between inputs."
    )


print("-----------  Save model as hd5 -----------")
outF='perm1.model.h5'
model1.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from YAML -----------")
model1b=load_model(outF)


print('M:  predicting-1b')
Z=model1b.predict(X)
print("M: Output5 Features")
for pred in Z: print(pred)


if 1:
    print('\n step-E - - - - - - - ')
    ## now let's make a model with multi permutational layers to show you that it's possible!
    model3=build_3permu_model(model1)
    # let's predict with the big mult layers model on some similar data
    print("# Multi-layer Prediction")
    print(
        "Because the layer is preserving permutation invariance. "
        "The output will be the same no matter how many times you permute the input order."
    )
    print("## Input/Output 1")
    x = [[[0, 0, 0, 0]], [[1, 2, 3, 4]], [[4, 3, 2, 1]]]
    print(x)
    print(model3.predict(x))

    print("## Input/Output 2")
    x = [[[1, 2, 3, 4]], [[0, 0, 0, 0]], [[4, 3, 2, 1]]]
    print(x)
    print(model3.predict(x))

    print("## Input/Output 3")
    x = [[[4, 3, 2, 1]], [[0, 0, 0, 0]], [[1, 2, 3, 4]]]
    print(x)
    print(model3.predict(x))
    print()
    print("Isn't this cool !?")
    print()
    print("After you understand the internal working of permutational layers,")
    print("you can use PermutationalModule() as a high-level function to construct")
    print("all the layer components quicker. Read its docstring to see how to use. It allows for 4-correlation or 8-correlation type of model")



