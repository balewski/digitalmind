#!/usr/bin/env python

'''
 example of Graph-layer from Spektral  (needs installation)
https://danielegrattarola.github.io/spektral/ 

Can train Graph or FC-based model (see switch: isGCNN
 trains
saves model
reads model - broken (but fixable)
predicts

See examples of data generators:
https://github.com/keras-team/keras/issues/7729

Run on Cori CPU:
export HDF5_USE_FILE_LOCKING=FALSE

'''

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np


import tensorflow as tf
'''
from tensorflow.contrib.keras.api.keras.layers import Dense, Input
from tensorflow.contrib.keras.api.keras.models import Model, load_model
'''
#OK: from tensorflow.contrib.keras.api.keras.layers import ConvLSTM2D

from keras.layers import Input, Dense, Dropout
from keras.models import Model, load_model
from keras.regularizers import l2
from keras.optimizers import Adam
from keras.layers.advanced_activations import LeakyReLU

from spektral.layers import GraphConv
from spektral.utils import  nx_to_numpy
from spektral.utils.convolution import localpooling_filter


import matplotlib.pyplot as plt

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

#-------------------
def build_modelFC(inp_dim,out_dim,fc_dim):
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' outs:',out_dim)
    h = Dense(fc_dim, activation='relu',name='calcium')(x_img)
   
    h=Dense(fc_dim, activation='relu',name='sodium')(h)
    ho=Dense(out_dim, activation='tanh',name='potas')(h)


    print('build_model w/ outputs :',ho.get_shape())
    model=Model(inputs=x_img, outputs=ho)
    
    # We compile the model and assign a weight of 0.2 to the 2nd loss=mae.
    model.compile(optimizer='adam', loss='mse')
    model.summary() # will print

    print('\nBuild multi-out model, layers=%d , params=%.1f K'%(len(model.layers),model.count_params()/1000.))

    return model


#-------------------
def build_modelGraphCNN(N,F,initLR):
    l2_regrt = 3e-5/10.
    dropFrac=0.01
    # - - - Assembling Graph CNN model
    nf_in = Input(shape=(N, F),name='nodeF')
    adj_in = Input(shape=(N, N),name='adjM')

    print('build_model nf_in:',nf_in.shape,' adj_in:',adj_in.shape)
    h=nf_in
    h=Dropout(dropFrac)( nf_in)

    for i in range(1):
        h = GraphConv(32, # activation='relu',
            kernel_regularizer=l2(l2_regrt)
            )([h, adj_in])
        h =LeakyReLU()(h)
        h=Dropout(dropFrac)(h)
        print('h_i :',i,h.get_shape())

    ho = GraphConv(1, activation='tanh',
                   kernel_regularizer=l2(l2_regrt)
                   )([h, adj_in] )
    print('ho :',ho.get_shape())
    # Build model
    model = Model(inputs=[nf_in, adj_in], outputs=ho)
    optimizer = Adam(lr=initLR)
    model.compile(optimizer=optimizer, loss='mse')
    model.summary() # will print

    print('\nBuild  model done, layers=%d , params=%d '%(len(model.layers),model.count_params()))

    return model


#=================================
#=================================
#  M A I N 
#=================================
#=================================
mFact=1  # model size expansion factor
nFact=1 # number of events expansion factor

inp_dim=7*mFact
fc_dim=15*mFact*mFact
out_dim=inp_dim

epochs=10*2
batch_size=32
steps=150*nFact
num_eve=steps*batch_size
isGCNN=1
initLR=1e-3
K = 1                         # Degree of propagation

if isGCNN:
    model=build_modelGraphCNN(inp_dim,1,initLR)
else:
    model=build_modelFC(inp_dim,out_dim,fc_dim)

myHost=socket.gethostname()
if 'cori'  in myHost or 'ubuntu' in myHost or 1:
    # this software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outF='simpleFC.graph.svg'
    plot_model(model, to_file=outF, show_shapes=True, show_layer_names=True)  # owrks only on Cori
    print('saved ',outF)

print('\nM: generate data and train, num_eve=',num_eve,', mFact=%d, nFact=%d'%(mFact,nFact))

startT = time.time()

Y=np.random.uniform(-0.9,0.9, size=(num_eve,out_dim))
print('M: done Y shape',Y.shape,)
#X=np.random.normal(loc=u1, scale=u2, size=(num_eve,inp_dim))
X=np.random.uniform(-0.8,0.8, size=(num_eve,inp_dim))
print('M: done X shape',X.shape,' elaT=%.1f sec,'%(time.time() - startT))

# inject Y to X so the training can converge
X[:,:out_dim]=Y*0.3+np.random.normal(loc=0, scale=0.001, size=(num_eve,out_dim))

if isGCNN:
    N=inp_dim
    F=1
    print('re-pack data for G-CNN, num nodes=',N,' features/node=',F)
    nodeL=[i for i in range(1,N+1)]
    edgeL=[ (1,2) , (1,3) , (1,4), (3,5), (3,6), (4,7)]
    for x,y in edgeL:
        assert x in nodeL
        assert y in nodeL
    # tmp using nx for sparse array generation
    from networkx import nx
    G = nx.Graph(day="Friday")
    for i in nodeL:
        G.add_node(i, sd=999.)  # place holder for Xs
        G.add_edge(i,i)
    G.add_edges_from(edgeL)
    #nx.draw(G, with_labels=True, alpha=0.8); print('G draw done')
    print('edges:',G.edges(),' nodes:',G.nodes())
    plt.show()
    adj1,nf1,ef1=nx_to_numpy(G, auto_pad=False, self_loops=False,nf_keys=['sd'])
    print(type(adj1),type(nf1),type(ef1))
    print('template X1, X2',nf1.shape,adj1.shape)
    # Preprocessing operations
    fltr1 = localpooling_filter(adj1[0])
    print('fltr1',fltr1.shape,type(fltr1))
    # Pre-compute propagation : Output matrix is worng
    for i in range(K - 1):
        fltr1 = fltr1.dot(fltr1)
        print('fltr i',i,fltr1.shape,type(fltr1))
    X1=X.reshape(num_eve,N,F) # node features
    print('node features X1:',X1.shape)
    #X2=np.tile(fltr1,(num_eve,1,1))
    X2=[ fltr1 for  i in range(num_eve)]
    X2=np.array(X2)
    print('adj-matrix X2:',X2.shape)
    Y=Y.reshape(num_eve,N,1)
    print('labels Y:',Y.shape)
    #print(X2[0],X2[1])
    X=[X1,X2] # repack input so it looks for FC-model



# input as np.arrrays
model.fit(X,Y, batch_size=batch_size, nb_epoch=epochs,verbose=1, validation_split=0.1,shuffle=False, ) 
print("-----------  training completed -----------")


print("-----------  Save model as hd5 -----------")
outF='jan.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

#print("-----------  Read model from YAML -----------")
#model5=load_model(outF)


print('M: fit done, predicting')
Z=model.predict(X)
print('M: done, Z:',Z.shape)

import matplotlib.pyplot as plt
plt.figure()
jN=4
plt.scatter(Z[:,jN], Y[:,jN], alpha=0.3)
#plt.plot(range(-1, 1), range(-1, 1))
plt.xlabel('Predicted jN=%d'%jN)
plt.ylabel('Actual')
plt.savefig('pred_v_true.png')
plt.show()


