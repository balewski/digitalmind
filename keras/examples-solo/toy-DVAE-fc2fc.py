#!/usr/bin/env python

# example of Driven Variational autoencoder converting image to image

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np
from keras import backend as K
from keras import losses 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout
from keras.models import Model
print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

#............................
#............................
#............................
def NegLogLikeh(y_true, y_pred):
    """ for continuous features use MSE """
    # keras.losses.mean_squared_errorgives the mean
    # over the last axis. we require the sum
    #return K.sum(losses.mean_squared_error(y_true, y_pred), axis=-1)
    return K.sum(losses.mean_absolute_error(y_true, y_pred), axis=-1)

#............................
#............................
#............................
class KLDivergComp(Layer):
    """ Identity transform layer that adds 'partial' KL divergence
    to the final model loss.
    """
    def __init__(self,strength=1., *args, **kwargs):
        self.is_placeholder = True
        self.strength=strength  # the smaller it gets the closer VAE-->AE
        super(KLDivergComp, self).__init__(*args, **kwargs)
        
    def call(self, inputs): # it computes loss due to latent vars per mini-batch
        mu, drive, log_var = inputs       
        kl_batch=-0.5 * K.sum(1 +log_var  -K.square(mu-drive)  -K.exp(log_var), axis=-1)
        self.add_loss(K.mean(self.strength*kl_batch), inputs=inputs)
        return [mu, log_var]  # pass by standard VAE output 



#-------------------
def build_model(img_dim,latent_dim):
    
    dens_dim = 50
    epsilon_std = 1.0
    KLD_strength=0.3
        
    # - - - Assembling model 
    x_img = Input(shape=(img_dim,),name='inp_x')
    u_img = Input(shape=(latent_dim,),name='inp_u')
    print('build_model inp1:',x_img.get_shape(),'  inp2:',u_img.get_shape(),'  KLD_strength=', KLD_strength )
    h = Dense(dens_dim, activation='relu',kernel_initializer='he_uniform',name='enc1')(x_img)
    
    #.... encoder  has 2 pieces: mand and sigma
    z_mu0 = Dense(latent_dim, activation='linear',name='z_mu_%d'%latent_dim)(h) 
    z_log_var0 = Dense(latent_dim, activation='linear',name='z_var_log')(h)

    z_mu, z_log_var = KLDivergComp(strength=KLD_strength,name='add2loss')([z_mu0, u_img,z_log_var0])
        
    z_sigma = Lambda(lambda t: K.exp(.5*t), name='z_sig')(z_log_var0) 

    eps_gen=K.random_normal(stddev=epsilon_std,shape=(K.shape(x_img)[0], latent_dim))
    eps = Input(tensor=eps_gen,  name='eps_gen')
    z_eps = Multiply(name='stoch_sig')([ eps,z_sigma])
    z = Add(name='stoch_z')([ z_mu,z_eps])
    
    print('build_model z:',z.get_shape())
    # .... decoder: 
    h = Dense(dens_dim, activation='relu',kernel_initializer='he_uniform',name='dec1')(z)
    decoded = Dense(img_dim, activation='sigmoid',name='out_img')(h)

    print('build_model decoded:',decoded.get_shape())
    # full model
    vaeM = Model(inputs=[x_img, u_img, eps], outputs=decoded)
    
    vaeM.compile(optimizer='rmsprop', loss=NegLogLikeh)
    vaeM.summary() # will print

    print('\nFull  seq2seq auto-encoder layers=%d , params=%.1f K'%(len(vaeM.layers),vaeM.count_params()/1000.))


    return vaeM

#=================================
#=================================
#  M A I N 
#=================================
#=================================
img_dim=50
latent_dim=2
model=build_model(img_dim,latent_dim)

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outF='dvae-f2f.graph.svg'
    plot_model(model, to_file=outF, show_shapes=True, show_layer_names=True)  # owrks only on Cori
    print('saved ',outF)

print('\ngenerate data and train')
num_eve=1000
u1=0.2; u2=7.0
U=np.array([u1,u2]*num_eve).reshape(-1,latent_dim)
print('driveX shape',U.shape)
X=np.random.normal(loc=u1, scale=u2, size=(num_eve,img_dim))
model.fit([X,U],X, batch_size=100, nb_epoch=2,verbose=1)
print('fit done, predicting')
Xnew=model.predict([X,U])
print('done, Xnew:',Xnew.shape)


