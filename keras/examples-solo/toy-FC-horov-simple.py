#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
 example of simple FC+horovod 
 trains
saves model
reads model
predicts
uses primitive generator for input (still data in RAM)

See examples of data generators:
https://github.com/keras-team/keras/issues/7729

****Run on Cori GPUs
ssh cori 
module load esslurm 
module load tensorflow/gpu-1.14.0-py37 

cd ~/digitalMind/keras/examples-solo
1 NODE:
salloc -C gpu -n 80 --gres=gpu:8  --exclusive -Adasrepo -t4:00:00


2 NODES (according to Chris)
salloc -C gpu -n 160 -N 2 --gres=gpu:8 -Adasrepo -t4:00:00 --exclusive
srun -n 8 --ntasks-per-node=4  ... to get uniform spread

# run training w/o GPU-load monitor

srun  -n 1 bash -c '  OMP_STACKSIZE=64M  python -u toy-FC-horov-simple.py'

To run on multiple nodes:
salloc -C gpu -n 16 --ntasks-per-node=8 -c 10 -N2  --gres=gpu:8 -Adasrepo -t4:00:00

srun  -n 16 bash -c '  OMP_STACKSIZE=64M  python -u toy-FC-horov-simple.py'


# run with 1 GPU-load monitor
srun  -n 2   bash -c ' hostname & if [ $SLURM_PROCID -eq 0 ] ; then  nvidia-smi -l >&L.smi ; fi &  python -u toy-FC-horov-simple.py'

srun bash -c '  nvidia-smi '

******  Run on Cori CPUs , all on 1 node as single task
ssh cori
module load tensorflow/intel-1.13.1-py36
./toy-FC-horov-simple.py  --noHorovod     --steps 5

If you see: HD5 OSError: Unable to create file (unable to lock file, errno = 524..
 export HDF5_USE_FILE_LOCKING=FALSE 

'''

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np

from tensorflow import keras 
from tensorflow.keras.datasets import mnist 
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input 
from tensorflow.keras import backend as K 
import math 
import tensorflow as tf 
import horovod.tensorflow.keras as hvd 

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

import argparse

parser = argparse.ArgumentParser()
parser.add_argument( "--noHorovod", dest='useHorovod',  action='store_false',
                     default=True, help="disable Horovod to run on CPU")
parser.add_argument("-b", "--batch_size", type=int, default=4096,
                        help="training batch_size")
parser.add_argument("-e", "--epochs", type=int, default=16,
                        help="training epochs")
parser.add_argument("-s", "--steps", type=int, default=80,
                        help="training epochs")
parser.add_argument("--seedWeights",default=None,
                    help="seed weights only, after model is created")
parser.add_argument("--lr", type=float, default=0.5,
                    help="learning rate for Adadelta optimizer")
parser.add_argument("--dropFrac", type=float, default=0.2,
                    help="drop fraction at all layers")
parser.add_argument("-o", "--outPath",
                    default='out/',help="output path for plots and tables")


#-------------------
def build_model(inp_dim,out_dim,fc_dim,dropFrac):
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' outs:',out_dim)
    h = Dense(fc_dim, activation='relu',name='calcium')(x_img)
   
    h=Dense(fc_dim, activation='relu',name='sodium1')(h)
    h = Dropout(dropFrac)(h) 
    h=Dense(fc_dim, activation='relu',name='sodium2')(h)
    h = Dropout(dropFrac)(h) 
    h=Dense(fc_dim, activation='relu',name='sodium3')(h)
    h = Dropout(dropFrac)(h) 
    h=Dense(fc_dim, activation='relu',name='sodium4')(h)
    h = Dropout(dropFrac)(h) 
    ho=Dense(out_dim, activation='linear',name='potas')(h)

    if verb>0: print('build_model w/ outputs :',ho.get_shape())
    model=Model(inputs=x_img, outputs=ho)
    
    if verb>0:
        print('model layers=%d , params=%.1f K, rank=%d of %d'%(len(model.layers),model.count_params()/1000.,myRank,gpu_count))

    return model



#=================================
#=================================
#  M A I N 
#=================================
#=================================

args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
assert  os.path.exists(args.outPath)

if args.useHorovod:
    hvd.init()
    gpu_count= hvd.size()
    myRank= hvd.rank()
    print(' Horovod: initialize  myRank=%d of %d on %s localRank=%d'%(myRank,gpu_count,socket.gethostname(),hvd.local_rank()))
    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = str(hvd.local_rank())
    K.set_session(tf.Session(config=config))
else:
    gpu_count= 1
    myRank=0
    print('Horovod disabled, gpu_count=',gpu_count)
verb=myRank==0
    
inp_dim=1000
out_dim=10
fc_dim=4000
epochs=args.epochs
batch_size=args.batch_size #//gpu_count
steps=args.steps

num_eve=steps*batch_size
num_eve2=int(1.2*num_eve)

model=build_model(inp_dim,out_dim,fc_dim,args.dropFrac)


myCallbacks = []
LR=args.lr
if args.useHorovod:
    #LR=1 *gpu_count
    opt = keras.optimizers.Adadelta(lr=LR)
    #Adadelta.__init__(lr=1.0,rho=0.95,epsilon=None,decay=0.0,**kwargs)

    #LR/1000.; opt = keras.optimizers.Adam(lr=LR)  # explodes for nGpu>1
    if verb>0: print('Horovod: adjust learning rate to',LR)
    print(opt)

    # Horovod: add Horovod Distributed Optimizer.
    myOpt = hvd.DistributedOptimizer(opt)
    # Horovod: broadcast initial variable states from rank 0 to all other processes.
    # This is necessary to ensure consistent initialization of all workers when
    # training is started with random weights or restored from a checkpoint.
    myCallbacks.append(hvd.callbacks.BroadcastGlobalVariablesCallback(0))
else:
    myOpt = keras.optimizers.Adadelta(lr=LR)

if verb>0: print('compile rank=%d LR=%.2f ...'%(myRank,LR))
model.compile(loss='mse', optimizer=myOpt)

if myRank == 0: model.summary() # will print

if args.seedWeights=='same' :  args.seedWeights=args.outPath
if args.seedWeights:
    inpF5=args.seedWeights+'/jan-FC-ok0.model.h5' # val_loss: 0.25
    #inpF5=args.seedWeights+'/jan-FC-ok1.model.h5' # val_loss: 0.0138
    if verb>0: print(myRank,'restore weights from HDF5:',inpF5)
    model.load_weights(inpF5) 
    

# Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
#if hvd.rank() == 0:
#    callbacks.append(keras.callbacks.ModelCheckpoint(outPath+'/checkpoint-{epoch}.h5'))

np.random.seed() #  set the seed to a random number obtained from /dev/urandom 
if verb>0:
    print('\nM: generate data train+val, num_eve/node=',num_eve,', gpu_count=',gpu_count, ', batch_size=',batch_size)
    print(myRank,'=rank, seed:',np.random.rand(4))
Y2=np.random.uniform(-1,1,(num_eve2,out_dim))
X2=np.random.normal(0, scale=1., size=(num_eve2,inp_dim))
# store info about Y in to X to regress 
for i in range(Y2.shape[0]):
    X2[i][0:out_dim]=Y2[i]

Y=Y2[:num_eve]
Yval=Y2[num_eve:]
X=X2[:num_eve]
Xval=X2[num_eve:]

if verb>0:
    print('M: Y2 shape',Y2.shape)
    print('M: split',Y.shape,Yval.shape)
    print('fit...',myRank,' verb',verb)
    
startTm = time.time()

model.fit(X,Y, batch_size=batch_size, callbacks=myCallbacks, epochs=epochs, verbose=verb, validation_data=(Xval, Yval))

fitTime=time.time() - startTm
if verb>0:
    print('fit done  elaT=%.1f sec, BS=%d epochs=%d'%(fitTime,batch_size,epochs))
    print(' Horovod: training completed  myRank=%d of %d on %s localRank=%d'%(myRank,gpu_count,socket.gethostname(),hvd.local_rank())) 

#exit(0)

if myRank > 0: exit(0)
# no need to repeat it for all ranks

print("-----------  Save model as hd5 -----------")
outF=args.outPath+'jan-FC.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from YAML -----------")
model5=load_model(outF)

print('M: fit done, predicting')
Zval=model5.predict(Xval)
print('M: done, Z:',Zval.shape,Yval.shape)
ZmY=Zval-Yval
# now compute MSE
sqErr=np.power(ZmY,2)
print('ss',sqErr.shape)
sqErr=sqErr.mean(axis=1)
print('ss',sqErr.shape)
msqErr=sqErr.mean()
ssqErr=sqErr.std()
print('computed segment MSE mean=%.4f +/- %.4f'%(msqErr,ssqErr))



