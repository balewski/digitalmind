#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
 example of simple FC 
 trains
saves model
reads model
predicts
uses generator for input

See examples of data generators:
https://github.com/keras-team/keras/issues/7729

****Run on Cori CPU:
set in code : gpu_count=0

module load tensorflow/gpu-1.12.0-py36
./toy-cosmoFlow-multiGPU.py

*** Run on KNL:
salloc --qos interactive -t 4:00:00 -C knl,quad,cache -N1 
module load tensorflow/intel-1.12.0-py36

****Run on Cori GPUs
set in code : gpu_count=2 or higher

module load tensorflow/gpu-1.12.0-py36
module load escori
salloc  -C gpu --gres=gpu:4 -Adasrepo -t2:00:00 --mem 30G

srun bash -c '  nvidia-smi '
srun bash -c '  nvidia-smi -l >&L.smi & python -u ./toy-cosmoFlow-multiGPU.py'

'''

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np

import tensorflow as tf
from tensorflow.contrib.keras.api.keras.layers import Dense, Input, Dropout,Conv3D,MaxPooling3D,Flatten
from tensorflow.contrib.keras.api.keras.models import Model, load_model
from tensorflow.python.keras.utils import multi_gpu_model
import tensorflow.keras.backend as K

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

#-------------------
def build_model(hparams):
    Xshape=hparams['Xshape'][1:]
    out_dim=hparams['Yshape'][1]
    dropFrac=hparams['dropFrac']
    padd='valid'

    # - - - Assembling model 
    x_img = Input(shape=Xshape,name='inp_x')
    print('build_model inp1:',x_img.get_shape(), hparams['data_format'],' outs:',out_dim)
    j=0 # global CNN-layer counter
    h=x_img
    # blockA - cnn stride is 1
    for filt,kern,stri in zip(hparams['convA_filters'] ,hparams['convA_kernels'] , hparams['poolA_strides']):
        j+=1
        cname='conv%d'%j
        print('assemby j=',j,cname,filt,stri)
        h=Conv3D(filt,kern,1,padding = padd,name=cname,activation='relu',data_format=hparams['data_format'])(h)
        # Conv3D(filters, kernel_size, strides=(1, 1, 1), padding='valid', data_format=None
        cname='pool%d'%j
        h=MaxPooling3D(pool_size=stri, padding = padd,name=cname,data_format=hparams['data_format'])(h)
        #MaxPooling3D(pool_size=(2, 2, 2), strides=None, padding='valid', data_format=None)
        print(cname,h.shape, 'pool_stride:',stri)

    h=Flatten(name='flat')(h)

    # FC block
    print('flatten',h.shape)
    j=0
    for dims  in hparams['dense_dims']:
        j+=1
        cname='fc%d'%j
        print('assemby j=',j,cname,dims)
        h = Dense(dims, activation='relu',name=cname)(h)
        h = Dropout( dropFrac)(h)

    ho=Dense(out_dim, activation='linear',name='ic_params')(h)

    print('build_model w/ outputs :',ho.get_shape())
    model=Model(inputs=x_img, outputs=ho)
    
    print('\nBuild multi-out model, layers=%d , params=%.1f K'%(len(model.layers),model.count_params()/1000.))

    return model

# - - - - - - - -  
def my_generator(X_data, y_data, batch_size):
  global resetCnt
  resetCnt=0
  samples_per_epoch = X_data.shape[0]
  assert samples_per_epoch>=batch_size
  assert samples_per_epoch%batch_size ==0
  number_of_batches = samples_per_epoch/batch_size
  counter=0
  #print('myGen cnt0')

  while 1:
    X_batch = np.array(X_data[batch_size*counter:batch_size*(counter+1)]).astype('float32')
    y_batch = np.array(y_data[batch_size*counter:batch_size*(counter+1)]).astype('float32')
    counter += 1
    #print('myGen cnt=',counter)
    yield X_batch,y_batch

    #restart counter to yield data in the next epoch as well
    if counter >= number_of_batches:
        counter = 0
        #print('myGen counter reset',resetCnt)
        resetCnt+=1


#=================================
#=================================
#  M A I N 
#=================================
#=================================
#box=128 #fixed for big model
box=128 #fixed  for small model

gpu_count=0
inpFeature=2
outFeature=4
epochs=10
steps=8
# scale batch size w/ number of GPUs
batch_size=max( 16, 16*gpu_count)
num_eve=batch_size*16
# 128^3 *2f , num eve=512 --> 18 GB CPU RAM


data_format='channels_last'
if gpu_count>0:
    data_format='channels_first'

hpar_big={ 
    # 3D cnn params
    'convA_filters': [16, 32, 64, 128, 256, 256, 256],
    'convA_kernels': [ 3,  4,  4,   3,   3,   2,   1],
    'poolA_strides': [ 2,  2,  2,   2,   1,   1,   1],
    # FC params
    'dense_dims': [2048, 256],
    'dropFrac': 0.2,
    'data_format': data_format,
    'design' : 'paper1'
}


hpar_small={ 
    # 3D cnn params
    'convA_filters': [16, 16, 16, 16, 16],
    'convA_kernels': [ 2,  2,  2,  2,  2],
    'poolA_strides': [ 2,  2,  2,  2,  2],
    # FC params
    'dense_dims': [128, 64],
    'dropFrac': 0.2,
    'data_format': data_format,
    'design' : 'tune1'
}

hpar=hpar_small
print('hpar:',hpar)

cpu_relocation=False # merge on CPU using cpu_relocation (~7% faster)

startT = time.time()
print('\nM: generate data and train, num_eve=',num_eve,', gpu_count=',gpu_count, ', batch_size=',batch_size,', inpFeature=', inpFeature)
u0=-2; u1=0.2; u2=7.0; u3=0.33
Y=np.array([u0,u1,u2,u3]*num_eve).reshape(-1,outFeature)
print('M: Y shape',Y.shape,)

if data_format=='channels_last':
    X=np.random.uniform( size=(num_eve,box,box,box,inpFeature))
else:
    X=np.random.uniform( size=(num_eve,inpFeature,box,box,box))
print('M: X shape',X.shape,)
print('data generated,  elaT=%.1f sec\n'%(time.time() - startT))

hpar['Xshape']=X.shape;  hpar['Yshape']=Y.shape


if gpu_count==0:  
    print('CPU-only  config')
    #config = tf.ConfigProto( intra_op_parallelism_threads=80)
    #K.set_session(tf.Session(config=config))

''' 
intra_op_parallelism_threads:  When training on CPU
      set to 0 to have the system pick the appropriate number or alternatively
      set it to the number of physical CPU cores.
'''

startT = time.time()


if gpu_count<2:  
    model=build_model(hpar)
else:
    #1, instantiate your base model on a cpu
    with tf.device("/cpu:0"):
        model=build_model(hpar)


if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outF='simpleFC.graph.svg'
    plot_model(model, to_file=outF, show_shapes=True, show_layer_names=True)  # owrks only on Cori
    print('saved ',outF)

#2, put your model to multiple gpus
if gpu_count>1:
    print('clone model for %d GPUs'%gpu_count)
    multi_model = multi_gpu_model(model, gpus=gpu_count,cpu_relocation=cpu_relocation)
    print('model clonned for %d GPUs,  elaT=%.1f sec,'%(gpu_count,time.time() - startT))

#3, compile both models
startT = time.time()
model.compile(optimizer='adam', loss='mse')
model.summary() # will print
if gpu_count>1:
    multi_model.compile(loss='mse', optimizer='adam')
    print('model compiled for %d GPUs, batch_size=%d elaT=%.1f sec,'%(gpu_count,batch_size,time.time() - startT))


#4, train the multi gpu model
if gpu_count<2:
    print('CPU or 1 GPU training')
    model.fit_generator(my_generator(X,Y,batch_size), epochs=epochs, steps_per_epoch=steps, verbose=1)
    #model.fit(X,Y, batch_size=batch_size, epochs=epochs,verbose=1) #?? batch size not working
else:
    multi_model.fit_generator(my_generator(X,Y,batch_size), epochs=epochs, steps_per_epoch=steps)


print("-----------  Save model as hd5 -----------")
outF='jan.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from YAML -----------")
model5=load_model(outF)

startT = time.time()
print('M: fit done, predicting on CPU')
Z=model5.predict(X)
print('M: done, Z:',Z.shape)
print('prediction done,  elaT=%.1f sec,'%(time.time() - startT))

