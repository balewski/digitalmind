#!/usr/bin/env python

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np
from keras.layers import Input, Dense, LSTM, RepeatVector
from keras.models import Model
print('libs imported elaT=%.1f sec'%(time.time() - start))

#-------------------
def build_model():
    time_dim=80
    feature_dim=10
     
    input_shape=(time_dim,feature_dim)
    lstm_dim=50
    zip_dim = 3
    x = Input(shape=input_shape, name='inp28x28')
    print('build_model inp:',x.get_shape())

    h=  LSTM(lstm_dim, activation='relu',name='rnn_enc_%d'%lstm_dim) (x)

    encoded = Dense(zip_dim, activation='relu',name='zip_out')(h)
    
    # "decoded" is the lossy reconstruction of the input
    h2= Dense(lstm_dim, activation='relu',name='zip_inp')(encoded)
    h2=RepeatVector(time_dim,name='times_%d'%time_dim)(h2)

    decoded= LSTM(feature_dim, activation='relu',return_sequences=True,name='rnn_dec_%d'%feature_dim) (h2)
 
    autoencoderM = Model(x, decoded)
    myLoss='mean_squared_error'
    autoencoderM.compile(optimizer='adadelta', loss=myLoss)

    autoencoderM.summary() # will print

    print('\nFull  seq2seq auto-encoder loss=%s layers=%d , params=%.1f K'%(myLoss,len(autoencoderM.layers),autoencoderM.count_params()/1000.))


    return autoencoderM

#=================================
#=================================
#  M A I N 
#=================================
#=================================
model=build_model()

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    plot_model(model, to_file='2LSTM-classif.graph.svg', show_shapes=True, show_layer_names=True)  # owrks only on Cori
