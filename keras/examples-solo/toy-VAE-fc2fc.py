#!/usr/bin/env python

# example of Variational autoencoder converting image to image

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np
from keras import backend as K
from keras import losses 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout
from keras.models import Model
print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

#............................
#............................
#............................
def NegLogLikeh(y_true, y_pred):
    """ for continuous features use MSE """
    # keras.losses.mean_squared_errorgives the mean
    # over the last axis. we require the sum
    #return K.sum(losses.mean_squared_error(y_true, y_pred), axis=-1)
    return K.sum(losses.mean_absolute_error(y_true, y_pred), axis=-1)

#............................
#............................
#............................
class KLDivergComp(Layer):
    """ Identity transform layer that adds 'partial' KL divergence
    to the final model loss.
    """
    def __init__(self, *args, **kwargs):
        self.is_placeholder = True
        self.myLambda=0.1  # the smaller it gets the closer VAE-->AE
        super(KLDivergComp, self).__init__(*args, **kwargs)
    def call(self, inputs): # it computes loss due to latent vars per mini-batch
        mu, log_var = inputs
       
        kl_batch=-0.5 * K.sum(1 +log_var  -K.square(mu)  -K.exp(log_var), axis=-1)
        self.add_loss(K.mean(self.myLambda*kl_batch), inputs=inputs)
        return inputs  # pass by



#-------------------
def build_model(img_dim):

    latent_dim=4
    dens_dim = 50
    epsilon_std = 1.0
        
    # - - - Assembling model 
    x = Input(shape=(img_dim,),name='inp_pulse')
    print('build_model inp1:',x.get_shape())
    h = Dense(dens_dim, activation='relu',name='enc1')(x)
    
    #.... encoder  has 2 pieces: mand and sigma
    z_mu0 = Dense(latent_dim, activation='linear',name='z_mu_%d'%latent_dim)(h) 
    z_log_var0 = Dense(latent_dim, activation='linear',name='z_var_log')(h)

    z_mu, z_log_var = KLDivergComp(name='add2loss')([z_mu0, z_log_var0])
        
    z_sigma = Lambda(lambda t: K.exp(.5*t), name='z_sig')(z_log_var0) 

    eps_gen=K.random_normal(stddev=epsilon_std,shape=(K.shape(x)[0], latent_dim))
    eps = Input(tensor=eps_gen,  name='eps_gen')
    z_eps = Multiply(name='stoch_sig')([ eps,z_sigma])
    z = Add(name='stoch_z')([ z_mu,z_eps])
    
    print('build_model z:',z.get_shape())
    # .... decoder: 
    h = Dense(dens_dim, activation='relu',name='dec1')(z)
    decoded = Dense(img_dim, activation='sigmoid',name='out_img')(h)

    print('build_model decoded:',decoded.get_shape())
    # full model
    vaeM = Model(inputs=[x, eps], outputs=decoded)
    
    vaeM.compile(optimizer='rmsprop', loss=NegLogLikeh)
    vaeM.summary() # will print

    print('\nFull  seq2seq auto-encoder layers=%d , params=%.1f K'%(len(vaeM.layers),vaeM.count_params()/1000.))


    return vaeM

#=================================
#=================================
#  M A I N 
#=================================
#=================================
img_dim=50
model=build_model(img_dim)

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    plot_model(model, to_file='2LSTM-classif.graph.svg', show_shapes=True, show_layer_names=True)  # owrks only on Cori

print('\ngenerate data and train')
num_eve=1000
X=np.random.normal(loc=0.0, scale=1.0, size=(num_eve,img_dim))
model.fit(X,X, batch_size=100, nb_epoch=3,verbose=1)
print('fit done, predicting')
Xnew=model.predict(X)
print('done, Xnew:',Xnew.shape)
    
