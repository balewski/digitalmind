#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
https://www.pyimagesearch.com/2018/12/24/how-to-use-keras-fit-and-fit_generator-a-hands-on-tutorial/

 example of simple FC+horovod +generator(file)
 linear decay of learning rate
 trains
saves model
reads model
predicts
uses generator for input

See examples of data generators:
https://github.com/keras-team/keras/issues/7729

****Run on Cori GPUs
ssh cori 
module load esslurm 
module load tensorflow/gpu-1.14.0-py37 

salloc -C gpu -n 80 --gres=gpu:8 --exclusive -Adasrepo -t4:00:00

# run training w/o GPU-load monitor

srun  -n 1 bash -c '  python -u toy-FC-horov-gen.py'
# run with 1 GPU-load monitor
srun  -n 2   bash -c ' hostname & if [ $SLURM_PROCID -eq 0 ] ; then  nvidia-smi -l >&L.smi ; fi &  python -u toy-FC-horov-gen.py -G -e4'

srun bash -c '  nvidia-smi '

******  Run on Cori CPUs , all on 1 node as single task
ssh cori
module load tensorflow/intel-1.12.0-py36
./toy-FC-horovod.py  --noHorovod     --steps 5

If you see: HD5 OSError: Unable to create file (unable to lock file, errno = 524..
 export HDF5_USE_FILE_LOCKING=FALSE 

'''

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np

from tensorflow import keras 
from tensorflow.keras.datasets import mnist 
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input 
from tensorflow.keras import backend as K 
import math 
import tensorflow as tf 
import horovod.tensorflow.keras as hvd 
import h5py

# for LR schedule:
from tensorflow.keras.callbacks import LearningRateScheduler
from tensorflow.keras.callbacks import  Callback
from tensorflow.keras import backend as K

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))

import argparse

parser = argparse.ArgumentParser()
parser.add_argument( "--noHorovod", dest='useHorovod',  action='store_false',
                     default=True, help="disable Horovod to run on CPU")
parser.add_argument( "-G","--useGenerator",  action='store_true',
                     default=False, help="create data as file, use generator to read & feed training")
parser.add_argument("-b", "--batch_size", type=int, default=4096,
                        help="training batch_size")
parser.add_argument("-e", "--epochs", type=int, default=16,
                        help="training epochs")
parser.add_argument("-s", "--steps", type=int, default=80,
                        help="steps per training epochs")
parser.add_argument("--seedWeights",default=None,
                    help="seed weights only, after model is created")
parser.add_argument("--initLR", type=float, default=0.5,
                    help="learning rate for Adadelta optimizer")
parser.add_argument("--linerLRsched", type=int, default=20,
                    help="max epochs for linear decay of learning rate, 0=none")

parser.add_argument("--dropFrac", type=float, default=0.2,
                    help="drop fraction at all layers")
parser.add_argument("-d","--dataPath",help="path to input",
                        default='data/')
parser.add_argument("-o", "--outPath",
                    default='out/',help="output path for plots and tables")


#............................
class MyPolynomialDecay():
    def __init__(self, maxEpochs=100, initLR=0.01, power=1.0):
        # store the maximum number of epochs, base learning rate,
        # and power of the polynomial
        self.maxEpochs = maxEpochs
        self.initAlpha = initLR
        self.power = power
        if rank0: print('\nMyPolynomialDecay activated, maxEpochs=%d, initLR=%.3g power=%.2f'%(maxEpochs,initLR,power))
    def __call__(self, epoch):
        # compute the new learning rate based on polynomial decay
        decay = (1 - (epoch / float(self.maxEpochs))) ** self.power
        decay=max(1e-8, decay) # to protect against negative decay
        alpha = self.initAlpha * decay
        return float(alpha) # return the new learning rate

#............................
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[] 
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr)
        if rank0: print('\n*****MYL: lr',lr)
        self.hir.append(float(lr))


#...!...!..................
def simple_image_generator(inputPath, bs,  mode="train"):
    # open the HD5 file for reading
    blob=read_data_hdf5(inputPath)
    j=0
    mxJ=blob['Y'].shape[0]
    # loop indefinitely
    while True:
        if j>=mxJ-1-bs: j=0 # reset pointer if not enough for next batch
        X=blob['X'][j:j+bs]
        Y=blob['Y'][j:j+bs]
        j+=bs
        yield (X,Y)


#...!...!..................
def write_data_hdf5(dataD,outF):
        h5f = h5py.File(outF, 'w')
        print('save data as hdf5:',outF)
        for item in dataD:
           rec=dataD[item]
           h5f.create_dataset(item, data=rec)
           print('h5-write :',item, rec.shape)
        h5f.close()
        xx=os.path.getsize(outF)/1048576
        print('closed  hdf5:',outF,' size=%.2f MB'%(xx))

#...!...!..................
def read_data_hdf5(inpF):
        if rank0: print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            if rank0: print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD



#-------------------
def build_model(inp_dim,out_dim,fc_dim,dropFrac):
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' outs:',out_dim)
    h = Dense(fc_dim, activation='relu',name='calcium')(x_img)
   
    h=Dense(fc_dim, activation='relu',name='sodium1')(h)
    h = Dropout(dropFrac)(h) 
    h=Dense(fc_dim, activation='relu',name='sodium2')(h)
    h = Dropout(dropFrac)(h) 
    h=Dense(fc_dim, activation='relu',name='sodium3')(h)
    h = Dropout(dropFrac)(h) 
    h=Dense(fc_dim, activation='relu',name='sodium4')(h)
    h = Dropout(dropFrac)(h) 
    ho=Dense(out_dim, activation='linear',name='potas')(h)

    if rank0: print('build_model w/ outputs :',ho.get_shape())
    model=Model(inputs=x_img, outputs=ho)
    
    if rank0: print('\nBuild multi-out model, layers=%d , params=%.1f K, myRank=%d of %d'%(len(model.layers),model.count_params()/1000.,myRank,gpu_count))

    return model

#=================================
#=================================
#  M A I N 
#=================================
#=================================

args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
for xx in [args.dataPath,args.outPath]:
        assert  os.path.exists(xx)

rank0=1
if args.useHorovod:
    hvd.init()
    gpu_count= hvd.size()
    myRank= hvd.rank()
    rank0= myRank==0
    print(' Horovod: initialize Horovod, myRank=%d of %d'%(myRank,gpu_count))
    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = str(hvd.local_rank())
    K.set_session(tf.Session(config=config))
else:
    gpu_count= 1
    myRank=0
    print('Horovod disabled, gpu_count=',gpu_count)
    
inp_dim=1000
out_dim=10
fc_dim=4000
epochs=args.epochs
batch_size=args.batch_size  # //gpu_count
steps=args.steps

num_eve=steps*batch_size
num_eve2=int(1.2*num_eve)

model=build_model(inp_dim,out_dim,fc_dim,args.dropFrac)

myCallbacks = []

if args.useHorovod:
    #LR=1 *gpu_count
    opt = keras.optimizers.Adadelta(learning_rate=args.initLR)
    #Adadelta.__init__(lr=1.0,rho=0.95,epsilon=None,decay=0.0,**kwargs)

    #LR/1000.; opt = keras.optimizers.Adam(lr=LR)  # explodes for nGpu>1
    #print('Horovod: adjust learning rate to',LR)
    
    # Horovod: add Horovod Distributed Optimizer.
    myOpt = hvd.DistributedOptimizer(opt)
    # Horovod: broadcast initial variable states from rank 0 to all other processes.
    # This is necessary to ensure consistent initialization of all workers when
    # training is started with random weights or restored from a checkpoint.
    myCallbacks.append(hvd.callbacks.BroadcastGlobalVariablesCallback(0))
    myCallbacks.append(hvd.callbacks.MetricAverageCallback()) # to gather loss across workers
else:
    myOpt = keras.optimizers.Adadelta(learning_rate=args.initLR)

if rank0: print('compile rank=%d initLR=%.2f, reduction schedule maxEpochs=%d ...'%(myRank,args.initLR,args.linerLRsched))

if args.linerLRsched>0:
    maxEpochs=args.linerLRsched
    assert maxEpochs > args.epochs
    if rank0:  print('add linear LR reduction, ')
    lr_sched = MyPolynomialDecay(maxEpochs=maxEpochs, initLR=args.initLR, power=1)
    myCallbacks.append(LearningRateScheduler(lr_sched))

# monitor LR regradless of scheduling
lr_track=MyLearningTracker()
myCallbacks.append(lr_track)

model.compile(loss='mse', optimizer=myOpt)

if rank0: model.summary() # will print

if args.seedWeights=='same' :  args.seedWeights=args.outPath
if args.seedWeights:
    inpF5=args.seedWeights+'/jan-FC-ok0.model.h5' # val_loss: 0.25
    #inpF5=args.seedWeights+'/jan-FC-ok1.model.h5' # val_loss: 0.0138
    print(myRank,'restore weights from HDF5:',inpF5)
    model.load_weights(inpF5) 
    

# Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
#if hvd.rank() == 0:
#    callbacks.append(keras.callbacks.ModelCheckpoint(outPath+'/checkpoint-{epoch}.h5'))

if rank0: print('\nM: generate data train+val, num_eve=',num_eve,', gpu_count=',gpu_count, ', batch_size=',batch_size)
np.random.seed() #  set the seed to a random number obtained from /dev/urandom 
print(myRank,'=rank, seed:',np.random.rand(4))
Y2=np.random.uniform(-1,1,(num_eve2,out_dim))
X2=np.random.normal(0, scale=1., size=(num_eve2,inp_dim))
# store info about Y in to X to regress 
for i in range(Y2.shape[0]):
    X2[i][0:out_dim]=Y2[i]

if rank0: print('M: Y2 shape',Y2.shape)
Y=Y2[:num_eve]
Yval=Y2[num_eve:]
X=X2[:num_eve]
Xval=X2[num_eve:]
if rank0: print('M: split',Y.shape,Yval.shape)

if args.useGenerator:
    for dom in ['train','val']:
        outF=args.dataPath+'cosmo5_%s_rank%d.h5'%(dom,myRank)
        if dom=='train':
            dataD={'X':X,'Y':Y}
        else:
            dataD={'X':Xval,'Y':Yval}
        write_data_hdf5(dataD,outF)

print('fit...',myRank)
startTm = time.time()

if not args.useGenerator:
    hir=model.fit(X,Y, batch_size=batch_size, callbacks=myCallbacks, epochs=epochs, verbose=rank0, validation_data=(Xval, Yval))
else:
    if rank0: print(' initialize both the training and testing image generators')
    assert steps>=8
    dom='train'
    inpF=args.dataPath+'cosmo5_%s_rank%d.h5'%(dom,myRank)
    trainGen = simple_image_generator(inpF, batch_size, mode=dom)
    dom='val'
    inpF=args.dataPath+'cosmo5_%s_rank%d.h5'%(dom,myRank)
    valGen = simple_image_generator(inpF, batch_size, mode=dom)
    hir=model.fit_generator(trainGen,steps_per_epoch=steps, callbacks=myCallbacks,
                            epochs=epochs,validation_data=valGen, verbose=rank0,
                            validation_steps=steps//5,
                            max_queue_size=32, workers=1, use_multiprocessing=False)


fitTime=time.time() - startTm
print('fit done  elaT=%.1f sec, BS=%d epochs=%d'%(fitTime,batch_size,epochs))

if myRank > 0: exit(0)
# no need to repeat it for all ranks
print('M: inspect call backs history:')
fit_hir=hir.history
print(fit_hir.keys(),len(lr_track.hir))
if 'lr' in fit_hir:
    print('LR per epoch from fit:',fit_hir['lr'])
else:
    print('LR per epoch from myTracker:',lr_track.hir)

print("-----------  Save model as hd5 -----------")
outF=args.outPath+'jan-FC.model.h5'
model.save(outF)
xx=os.path.getsize(outF)/1048576
print('  closed  hdf5:',outF,' size=%.2f MB'%xx)

print("-----------  Read model from YAML -----------")
model5=load_model(outF)

print('M: fit done, predicting')

if args.useGenerator:
    Zval=model5.predict_generator(valGen,steps=3)
else:
    Zval=model5.predict(Xval)
print('M: done, Z:',Zval.shape,Yval.shape)
ZmY=Zval-Yval
# now compute MSE
sqErr=np.power(ZmY,2)
print('ss',sqErr.shape)
sqErr=sqErr.mean(axis=1)
print('ss',sqErr.shape)
msqErr=sqErr.mean()
ssqErr=sqErr.std()
print('computed segment MSE mean=%.4f +/- %.4f'%(msqErr,ssqErr))



