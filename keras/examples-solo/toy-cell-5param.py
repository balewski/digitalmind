#!/usr/bin/env python

# example of muti-output regression
# based on https://keras.io/getting-started/functional-api-guide/#multi-input-and-multi-output-models

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import numpy as np
from keras import backend as K
from keras import losses 
from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply, Dropout
from keras.models import Model
print('deep-libs imported TF ver:',K.tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))



#-------------------
def build_model(inp_dim,dima,dimb,dimc,dimd):
    fc_dim=15
       
    # - - - Assembling model 
    x_img = Input(shape=(inp_dim,),name='inp_x')
    print('build_model inp1:',x_img.get_shape(), ' outs:',dima,dimb,dimc,dimd)
    ha_bcd = Dense(fc_dim, activation='relu',name='enc1')(x_img)
   
    # ......  1st branchout
    ha=Dense(fc_dim,name='sodium')(ha_bcd)
    hbcd=Dense(fc_dim,name='calc_potas')(ha_bcd)

    # ......  a-output
    ha=Dense(dima,name='na_node')(ha)
    
    # ......  2nd branchout
    hb=Dense(fc_dim,name='branch3')(hbcd)
    hc=Dense(fc_dim,name='branch4')(hbcd)
    hd=Dense(fc_dim,name='branch5')(hbcd)

    # ......  b,c-output
    hb=Dense(dimb,name='kca_soma')(hb)
    hc=Dense(dimc,name='ca_dend')(hc)

    # ......  3nd branchout
    hd=Dense(fc_dim,name='potasium')(hd)
    hd=Dense(dimd,name='kmden_kvsom')(hd)


    print('build_model w/ 4 outputs :',ha.get_shape(),hb.get_shape(),hc.get_shape(),hd.get_shape())
    model=Model(inputs=x_img, outputs=[ha,hb,hc,hd])
    
    # We compile the model and assign a weight of 0.2 to the 2nd loss=mae.
    model.compile(optimizer='adam', 
                  loss={'na_node':'mse','kca_soma':'mae','ca_dend':'mse','kmden_kvsom':'mse'}, 
                  loss_weights={'na_node':1.,'kca_soma':1.,'ca_dend':0.8,'kmden_kvsom':0.9}
              )
    model.summary() # will print

    print('\nBuild multi-out model, layers=%d , params=%.1f K'%(len(model.layers),model.count_params()/1000.))

    return model

#=================================
#=================================
#  M A I N 
#=================================
#=================================
inp_dim=2000
dima=1; dimb=1; dimc=1; dimd=2
model=build_model(inp_dim,dima,dimb,dimc,dimd)

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outF='cell-5param.graph.svg'
    plot_model(model, to_file=outF, show_shapes=True, show_layer_names=True)  # owrks only on Cori
    print('saved ',outF)

print('\nM: generate data and train')
num_eve=1000
u0=-2; u1=0.2; u2=7.0; u3=12; u4=3
Ya=np.array([u0,]*num_eve).reshape(-1,dima)
Yb=np.array([u4,]*num_eve).reshape(-1,dimb)
Yc=np.array([u2,]*num_eve).reshape(-1,dimc)
Yd=np.array([u1,u3]*num_eve).reshape(-1,dimd)

print('M: Ys shape',Ya.shape, Yb.shape,Yc.shape, Yd.shape)
X=np.random.normal(loc=u1, scale=u2, size=(num_eve,inp_dim))
model.fit(X,[Ya,Yb,Yc,Yd], batch_size=100, nb_epoch=2,verbose=1)
print('M: fit done, predicting')
Za,Zb,Zc,Zd=model.predict(X)
print('M: done, Zs:',Za.shape,Zb.shape,Zc.shape,Zd.shape)


