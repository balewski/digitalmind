#!/usr/bin/env python

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np

from keras import backend as K
from keras.layers import Input, Dense, LSTM, RepeatVector, Layer
from keras.models import Model
from keras.utils import plot_model
print('libs imported elaT=%.1f sec'%(time.time() - start))

#............................
#............................
#............................
class MySinFunc(Layer):
    """ Custom function layer, output size differs from input size
    goal output: f= A * sin( B*t )
    """
    def __init__(self, *args, **kwargs):
        self.is_placeholder = True
        super(MySinFunc, self).__init__(*args, **kwargs)
        self.output_dim=80
        t=K.arange(0,self.output_dim,dtype='float32')
        self.t_bins=K.expand_dims(t,axis=0)
        print(self.__class__.__name__,' out:',self.t_bins.shape)

    def call(self, inputs): 
        #print('inpABC',inputs.shape) 
        A=K.expand_dims(inputs[:,0]) # goal shape : (?,1)
        B=K.expand_dims(inputs[:,1]) 
        #print('parB',B.shape)        
        t=self.t_bins
        phi=K.dot(B,t)/90.
        func=A*K.sin(phi)
        #func=phi
        outV=K.expand_dims(func,axis=-1)
        #print('out Func',outV.shape)  
        return outV

       # If we are changing input dimensions we need to tell keras that we are doing so and what the output dimension is
    def compute_output_shape(self, input_shape):
           outsh=(input_shape[0], self.output_dim,1)
           print(self.__class__.__name__,'shapes inp:',input_shape,' out:',outsh) 
           return outsh


#-------------------
def build_model():
    time_dim=80
    feature_dim=1
     
    input_shape=(time_dim,feature_dim)
    lstm_dim=50
    zip_dim = 3
    x = Input(shape=input_shape, name='inp_seq')
    print('build_model inp:',x.get_shape())

    h=  LSTM(lstm_dim, activation='relu',name='rnn_enc_%d'%lstm_dim) (x)

    encoded = Dense(zip_dim, activation='relu',name='zip_out')(h)
    decoded = MySinFunc(name='user_func')(encoded)
    print('decoded:',decoded.get_shape())
   
    autoencoderM = Model(x, decoded)

    myLoss='mean_squared_error'
    autoencoderM.compile(optimizer='adadelta', loss=myLoss)

    autoencoderM.summary() # will print

    print('\nFull  seq2seq auto-encoder loss=%s layers=%d , params=%.1f K'%(myLoss,len(autoencoderM.layers),autoencoderM.count_params()/1000.))


    return autoencoderM

#=================================
#=================================
#  M A I N 
#=================================
#=================================
model=build_model()

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    plot_model(model, to_file='2LSTM-classif.graph.svg', show_shapes=True, show_layer_names=True)  # owrks only on Cori

#from keras.utils import plot_model

