#!/usr/bin/env python

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import socket  # for hostname
import os, time
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

start = time.time()
import numpy as np
from keras.layers import Input, Dense, UpSampling1D, Reshape,Conv1D,Flatten,MaxPool1D


from keras.models import Model
print('libs imported elaT=%.1f sec'%(time.time() - start))

#-------------------
def build_model(time_dim,   zip_dim ): # this has big input
    
    kernel_size=5
    strides=2
    pool_size = 4

    input_shape=(time_dim,)

    x = Input(shape=input_shape, name='inp_x')
    print('build_model inp:',x.get_shape())
    h = Reshape((-1, 1), name='pass0') (x) # because Conv1D wants rank 3 tensor 
    print('resh0=',h.get_shape())    

    # padding="same" results in padding the input such that the output has the same length as the original input
    # strades - how move move to the right
    h=Conv1D(3, kernel_size,strides=strides, name='con1',activation = 'relu', padding='same')(h)
    print('c1=',h.get_shape())
    h=MaxPool1D(pool_size , name='pool1')(h)

    print('m1=',h.get_shape())
    h=Conv1D(6, kernel_size,strides=strides, name='con2', activation = 'relu', padding='same')(h)
    print('c2=',h.get_shape())
    h=MaxPool1D(pool_size , name='pool2')(h)
    print('m2=',h.get_shape())

    h=Flatten(name='pass1')(h) 
    print('flat=',h.get_shape())

    encoded = Dense(zip_dim, name='to_zip',activation='relu')(h)
    print('enc=',encoded.get_shape())

    # "decoded" is the lossy reconstruction of the input
    h= Dense(42, activation='relu',name='from_zip')(encoded)
    print('d0=',h.get_shape())
    h = Reshape((-1, 6), name='pass2') (h)
    print('resh=',h.get_shape())    
    h = UpSampling1D(size=pool_size, name='ups1')(h)
    print('u1=',h.get_shape())
    h=Conv1D(3, kernel_size, name='con3', activation = 'relu', padding='same')(h)
    print('d2=',h.get_shape())
    h = UpSampling1D(size=pool_size, name='ups2')(h)
    print('u2=',h.get_shape())
    h=Conv1D(1, kernel_size, name='con4', activation = 'relu', padding='same')(h)
    print('d3=',h.get_shape())
    h = UpSampling1D(size=pool_size, name='ups3')(h)
    print('u3=',h.get_shape())
    decoded = Reshape((-1, ), name='out_x') (h) # restore rank 2 tensor
    print('dec=',decoded.get_shape())
 
    # check for equal size
    if x.shape[1:] != decoded.shape[1:]: print('alert: in/out dimension mismatch')
    autoencoderM = Model(x, decoded)
    myLoss='mean_squared_error'
    autoencoderM.compile(optimizer='adadelta', loss=myLoss)

    autoencoderM.summary() # will print

    print('\nFull  seq2seq auto-encoder loss=%s layers=%d , params=%.1f K'%(myLoss,len(autoencoderM.layers),autoencoderM.count_params()/1000.))


    return autoencoderM

#-------------------
def build_modelOK(time_dim,   zip_dim ): # small input (48,3)
    
    kernel_size=5
    strides=2
    pool_size = 4

    input_shape=(time_dim,)

    x = Input(shape=input_shape, name='inp_x')
    print('build_model inp:',x.get_shape())
    h = Reshape((-1, 1), name='pass0') (x) # because Conv1D wants rank 3 tensor 
    print('resh0=',h.get_shape())    

    # padding="same" results in padding the input such that the output has the same length as the original input
    # strades - how move move to the right
    h=Conv1D(3, kernel_size,strides=strides, name='con1',activation = 'relu', padding='same')(h)
    print('c1=',h.get_shape())
    h=MaxPool1D(pool_size , name='pool1')(h)

    print('m1=',h.get_shape())
    h=Conv1D(6, kernel_size,strides=strides, name='con2', activation = 'relu', padding='same')(h)
    print('c2=',h.get_shape())
    h=MaxPool1D(pool_size , name='pool2')(h)
    print('m2=',h.get_shape())

    h=Flatten(name='pass1')(h)
    print('flat=',h.get_shape())
    encoded = Dense(zip_dim, name='to_zip',activation='relu')(h)
    print('enc=',encoded.get_shape())

    # "decoded" is the lossy reconstruction of the input
    h= Dense(6, activation='relu',name='from_zip')(encoded)
    print('d0=',h.get_shape())
    h = Reshape((1, -1), name='pass2') (h)
    print('resh=',h.get_shape())    
    h = UpSampling1D(size=pool_size, name='ups1')(h)
    print('u1=',h.get_shape())
    h=Conv1D(3, kernel_size, name='con3', activation = 'relu', padding='same')(h)
    print('d2=',h.get_shape())
    h = UpSampling1D(size=pool_size, name='ups2')(h)
    print('u2=',h.get_shape())
    h=Conv1D(1, kernel_size, name='con4', activation = 'relu', padding='same')(h)
    print('d3=',h.get_shape())
    h = UpSampling1D(size=pool_size, name='ups3')(h)
    print('u3=',h.get_shape())
    decoded = Reshape((-1, ), name='out_x') (h) # restore rank 2 tensor
    print('resh0=',h.get_shape())    


    print('dec=',decoded.get_shape())
 
    # check for equal size
    if x.shape[1:] != decoded.shape[1:]: print('alert: in/out dimension mismatch')
    autoencoderM = Model(x, decoded)
    myLoss='mean_squared_error'
    autoencoderM.compile(optimizer='adadelta', loss=myLoss)

    autoencoderM.summary() # will print

    print('\nFull  seq2seq auto-encoder loss=%s layers=%d , params=%.1f K'%(myLoss,len(autoencoderM.layers),autoencoderM.count_params()/1000.))


    return autoencoderM

#=================================
#=================================
#  M A I N 
#=================================
#=================================
time_dim=448
latent_dim=4
model=build_model(time_dim,latent_dim)

if 'cori'  in socket.gethostname():
    # software not installed on other machines
    from tensorflow.contrib.keras.api.keras.utils import plot_model
    outF='AE-cnn1d.graph.svg'
    plot_model(model, to_file=outF, show_shapes=True, show_layer_names=True)  # works only on Cori
    print('saved model as ',outF)

print('\ngenerate data and train')
num_eve=1000
X=np.random.normal(loc=0, scale=0.5, size=(num_eve,time_dim))
model.fit(X,X, batch_size=100, nb_epoch=2,verbose=1)
print('fit done, predicting')
Xnew=model.predict(X)
print('done, Xnew:',Xnew.shape)
